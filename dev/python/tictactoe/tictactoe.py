"""
Created on Tue May  9 09:46:17 2017
tic tac toe
@author: lab301-user127
"""


# j1
def tictactoe():
    positions = ["__", "__", "__", "__", "__", "__", "__", "__", "__"]
    yep = ["YES", "yes", "Yes", "Y", "y"]
    nope = ["NO", "no", "No", "N", "n"]
    x = ["X", "x"]
    o = ["O", "o"]

    if input("Would you like to play tic tac toe? yes/no \n") in yep:

        print("      A    B    C")
        print("    ==============")
        print("1  |", positions[0], "|", positions[1], "|", positions[2], "|")
        print("2  |", positions[3], "|", positions[4], "|", positions[5], "|")
        print("3  |", positions[6], "|", positions[7], "|", positions[8], "|")
        print("    ==============")

        print("\nTo pick a square enter the coordinate of the square")
        print("for example, to pick the top left square enter A1.")

        player1 = ""
        player2 = ""
        # decides who is who
        while True:
            choice = input("\nPlayer 1 will you be playing as X or O ?\n")
            if choice in x:
                player1 = "x"
                player2 = "o"
                print("Player 1 is X \nPlayer 2 is O")
                break
            elif choice in o:
                player1 = "o"
                player2 = "x"
                print("Player 1 is O \nPlayer 2 is X")
                break
            else:
                print("Please enter your choice as either 'x', 'X' , 'o', or 'O'")


        ###################################################################################

        ###################################################################################
        # j2
        def square1():
            if player1 in ["x"]:
                if coords in ["A1", "a1"]:
                    if positions[0] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[0] = "><"

                elif coords in ["A2", "a2"]:
                    if positions[3] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[3] = "><"

                elif coords in ["A3", "a3"]:
                    if positions[6] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[6] = "><"

                elif coords in ["B1", "b1"]:
                    if positions[1] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[1] = "><"

                elif coords in ["B2", "b2"]:
                    if positions[4] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[4] = "><"

                elif coords in ["B3", "b3"]:
                    if positions[7] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[7] = "><"

                elif coords in ["C1", "c1"]:
                    if positions[2] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[2] = "><"

                elif coords in ["C2", "c2"]:
                    if positions[5] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[5] = "><"

                elif coords in ["C3", "c3"]:
                    if positions[8] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[8] = "><"

                        # j3
            elif player1 in ["o"]:
                if coords in ["A1", "a1"]:
                    if positions[0] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[0] = "<>"

                elif coords in ["A2", "a2"]:
                    if positions[3] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[3] = "<>"

                elif coords in ["A3", "a3"]:
                    if positions[6] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[6] = "<>"

                elif coords in ["B1", "b1"]:
                    if positions[1] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[1] = "<>"

                elif coords in ["B2", "b2"]:
                    if positions[4] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[4] = "<>"

                elif coords in ["B3", "b3"]:
                    if positions[7] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[7] = "<>"

                elif coords in ["C1", "c1"]:
                    if positions[2] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[2] = "<>"

                elif coords in ["C2", "c2"]:
                    if positions[5] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[5] = "<>"

                elif coords in ["C3", "c3"]:
                    if positions[8] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[8] = "<>"
                        ### for player two
                        # j4

        def square2():
            if player2 in ["x"]:
                if coords in ["A1", "a1"]:
                    if positions[0] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[0] = "><"

                elif coords in ["A2", "a2"]:
                    if positions[3] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[3] = "><"

                elif coords in ["A3", "a3"]:
                    if positions[6] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[6] = "><"

                elif coords in ["B1", "b1"]:
                    if positions[1] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[1] = "><"

                elif coords in ["B2", "b2"]:
                    if positions[4] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[4] = "><"

                elif coords in ["B3", "b3"]:
                    if positions[7] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[7] = "><"

                elif coords in ["C1", "c1"]:
                    if positions[2] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[2] = "><"

                elif coords in ["C2", "c2"]:
                    if positions[5] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[5] = "><"

                elif coords in ["C3", "c3"]:
                    if positions[8] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[8] = "><"

                        # j5

            elif player2 in ["o"]:
                if coords in ["A1", "a1"]:
                    if positions[0] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[0] = "<>"

                elif coords in ["A2", "a2"]:
                    if positions[3] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[3] = "<>"

                elif coords in ["A3", "a3"]:
                    if positions[6] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[6] = "<>"

                elif coords in ["B1", "b1"]:
                    if positions[1] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[1] = "<>"

                elif coords in ["B2", "b2"]:
                    if positions[4] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[4] = "<>"

                elif coords in ["B3", "b3"]:
                    if positions[7] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[7] = "<>"

                elif coords in ["C1", "c1"]:
                    if positions[2] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[2] = "<>"

                elif coords in ["C2", "c2"]:
                    if positions[5] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[5] = "<>"

                elif coords in ["C3", "c3"]:
                    if positions[8] not in ["__"]:
                        print("Sorry that space is taken")
                    else:
                        positions[8] = "<>"


                        ###################################################################################

                        ###################################################################################
                        # j6

    def board():
        print("      A    B    C")
        print("    ==============")
        print("1  |", positions[0], "|", positions[1], "|", positions[2], "|")
        print("2  |", positions[3], "|", positions[4], "|", positions[5], "|")
        print("3  |", positions[6], "|", positions[7], "|", positions[8], "|")
        print("    ==============")

    ###################################################################################

    ###################################################################################
    # code for other ways to win
    # j7
    def win(who_wins):
        if who_wins in ["><"] and player1 in ["x"]:
            print("Congratulations Player1, YOU WIN!!!")
            again = input("Would you like to play again?")
            if again in ["YES", "yes", "Yes", "Y", "y"]:
                tictactoe()
        elif who_wins in ["><"] and player1 in ["o"]:
            print("Congratulations Player2, YOU WIN!!!")
            again = input("Would you like to play again?")
            if again in ["YES", "yes", "Yes", "Y", "y"]:
                tictactoe()
        elif who_wins in ["<>"] and player2 in ["x"]:
            print("Congratulations Player1, YOU WIN!!!")
            again = input("Would you like to play again?")
            if again in ["YES", "yes", "Yes", "Y", "y"]:
                tictactoe()
        elif who_wins in ["<>"] and player2 in ["o"]:
            print("Congratulations Player2, YOU WIN!!!")
            again = input("Would you like to play again?")
            if again in ["YES", "yes", "Yes", "Y", "y"]:
                tictactoe()

    def check():
        if positions[0] is positions[1] and positions[1] is positions[2]:
            who_wins = positions[0]
            win(who_wins)

        if positions[3] is positions[4] and positions[4] is positions[5]:
            who_wins = positions[3]
            win(who_wins)

        if positions[6] is positions[7] and positions[7] is positions[8]:
            who_wins = positions[6]
            win(who_wins)

        if positions[0] is positions[3] and positions[3] is positions[6]:
            who_wins = positions[6]
            win(who_wins)

        if positions[1] is positions[4] and positions[4] is positions[7]:
            who_wins = positions[1]
            win(who_wins)

        if positions[2] is positions[5] and positions[5] is positions[8]:
            who_wins = positions[8]
            win(who_wins)

        if positions[0] is positions[4] and positions[4] is positions[8]:
            who_wins = positions[0]
            win(who_wins)

        if positions[3] is positions[4] and positions[4] is positions[6]:
            who_wins = positions[6]
            win(who_wins)






            ###################################################################################

            ###################################################################################
            # j8

    possabilities = ["a1","A1","a2","A2","a3","A3","b1","B1","b2","B2","b3","B3","c1","C1","c2","C2","c3","C3"]
    # LOOOP
    while True:
        turn1 = True
        while (turn1):
            coords = input("\nPlayer 1 please choose your square.\n")
            if coords in possabilities:
                square1()
                board()
                check()
                turn1 = False
            else:
                print("Please enter a valid coordinate.")

        turn2 = True
        while (turn2):
            coords = input("\nPlayer 2 please choose your square.\n")
            if coords in possabilities:
                square2()
                board()
                check()
                turn2 = False
            else:
                print("Please enter a valid coordinate.")


    else:
        print("Come back soon.")
    quit()


tictactoe()
