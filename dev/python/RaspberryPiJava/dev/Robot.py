# TODO: Update the remarks, especially at the top of the program
#       Start PIGPIO daemon on startup: sudo pigpiod
#       Add real logging
#       Add a config file
from time import sleep
from time import time
import os
import smbus
import math
import ConfigParser
import pigpio
import piplates.MOTORplate as MOTOR
import RPi.GPIO as GPIO

class Robot:
    'common base class for all robots'
    def __init__ (self, slowSpeedParm, mediumSpeedParm, fastSpeedParm, configFile):

        config = ConfigParser.RawConfigParser()
        config.read(configFile)

        self.debug = int(config.get('Debug','debug'))
        self.logDebug("__init__()")
        self.slowSpeed = slowSpeedParm
        self.mediumSpeed = mediumSpeedParm
        self.fastSpeed = fastSpeedParm
        self.slowAccelerate = float(config.get('RobotAccelerate', 'slowAccelerate'))
        self.mediumAccelerate = float(config.get('RobotAccelerate', 'mediumAccelerate'))
        self.fastAccelerate = float(config.get('RobotAccelerate', 'fastAccelerate'))
        self.hardTurnTime180 = float(config.get('RobotTurn', 'hardTurnTime180'))
        self.hardTurnTime360 = float(config.get('RobotTurn', 'hardTurnTime360'))
        self.sharpTurnTime = float(config.get('RobotTurn', 'sharpTurnTime'))
        self.distanceSamples = int(config.get('Distance','distanceSamples'))

        # setup the SR04 distance sensor
        GPIO.setmode(GPIO.BCM)
        self.distanceTriggerPin = int(config.get('Pins','distanceTriggerPin'))
        self.distanceEchoPin = int(config.get('Pins','distanceEchoPin'))
        GPIO.setup(self.distanceTriggerPin, GPIO.OUT)
        GPIO.setup(self.distanceEchoPin, GPIO.IN)

        self.motorAddress = int(config.get('Motors','motorAddress'))   # pi-plate address
        # return the MOTORplate to a known state. Stops and removes power to the motors.  Clears out all configuration settings
        self.off()
        self.bothLEDsOn()
        sleep(int(config.get('Distance','settleTime')))   # let the distance sensor settle

        # Initialize the motor variables
        self.motorFrontRight = int(config.get('Motors','motorFrontRight'))
        self.motorFrontLeft = int(config.get('Motors','motorFrontLeft'))
        self.motorRearRight = int(config.get('Motors','motorRearRight'))
        self.motorRearLeft = int(config.get('Motors','motorRearLeft'))

        self.forward = config.get('Direction','forward')
        self.backward = config.get('Direction','backward')

        # Initialize the servo
        self.servoHorizontalPin = int(config.get('Pins','servoHorizontalPin'))        # horizontal pan servo for distaince sensor
        self.servoVerticalPin = int(config.get('Pins','servoVerticalPin'))           # vertical pan servo for distance sensor
        self.servoWait = float(config.get('Servo', 'servoWait'))    # time in seconds to wait for servo to move
        
        self.servoLeftPulseWidth = int(config.get('Servo', 'servoLeftPulseWidth'))
        self.servoRightPulseWidth = int(config.get('Servo', 'servoRightPulseWidth'))
        self.servoHorizontalMidPulseWidth = int(config.get('Servo', 'servoHorizontalMidPulseWidth'))
        
        self.servoUpPulseWidth = int(config.get('Servo', 'servoUpPulseWidth'))
        self.servoDownPulseWidth = int(config.get('Servo', 'servoDownPulseWidth'))
        self.servoVerticalMidPulseWidth = int(config.get('Servo', 'servoVerticalMidPulseWidth'))

        # Turn off the LEDs
        self.bothLEDsOff()

        # log some info
        self.logDebug("Pi-Plate descriptor: " + str(MOTOR.getID(self.motorAddress)))
        self.logDebug("FW revision: " + str(MOTOR.getFWrev(self.motorAddress)))
        self.logDebug("HW revision: " + str(MOTOR.getHWrev(self.motorAddress)))
        self.logDebug("Pi-Plate Address: " + str(MOTOR.getADDR(self.motorAddress)))

        # initialize distance variables
        self.leftDistance = 0
        self.rightDistance = 0
        self.midDistance = 0

        # Motors off, direction forward
#        self.stopAllMotors()
#        self.setAllMotorsForward(self.mediumSpeed)

        # servo = PWM.Servo()
        self.pi1 = pigpio.pi()      # horizontal
        self.pi2 = pigpio.pi()      # vertical

    def __del__(self):
        self.logDebug("__del__()")
        self.off()
        
    def logDebug(self,message):
        if self.debug:
            print message

    def servoLeft(self):           # horizontal left
        self.logDebug("servoLeft()")
        self.pi1.set_servo_pulsewidth(self.servoHorizontalPin,self.servoLeftPulseWidth)
        sleep(self.servoWait)
        self.pi1.set_servo_pulsewidth(self.servoHorizontalPin,0)


    def servoRight(self):          # horizontal right
        self.logDebug("servoRight()")
        self.pi1.set_servo_pulsewidth(self.servoHorizontalPin,self.servoRightPulseWidth)
        sleep(self.servoWait)
        self.pi1.set_servo_pulsewidth(self.servoHorizontalPin,0)

    def servoHorizontalMid(self):            # horizontal mid
        self.logDebug("servoHorizontalMid()")
        self.pi1.set_servo_pulsewidth(self.servoHorizontalPin,self.servoHorizontalMidPulseWidth)
        sleep(self.servoWait)
        self.pi1.set_servo_pulsewidth(self.servoHorizontalPin,0)

    def servoHorizontalPan(self):            # horizontal pan
        self.logDebug("servoHorizontalPan()")
        self.bothLEDsOn()
        self.servoLeft()
        sleep(self.servoWait)
        self.leftDistance = self.readDistance()
        self.servoRight()
        sleep(self.servoWait)
        self.rightDistance = self.readDistance()
        self.servoHorizontalMid()
        sleep(self.servoWait)
        self.midDistance = self.readDistance()
        self.bothLEDsOff()

    # moves servo 2 to the left-most position
    def servoDown(self):           # vertical left
        self.logDebug("servoDown()")
        self.pi2.set_servo_pulsewidth(self.servoVerticalPin,self.servoDownPulseWidth)
        sleep(self.servoWait)
        self.pi2.set_servo_pulsewidth(self.servoVerticalPin,0)

    # moves servo 2 to the right-most position    
    def servoUp(self):          # vertical right
        self.logDebug("servoUp()")
        self.pi2.set_servo_pulsewidth(self.servoVerticalPin,self.servoUpPulseWidth)
        sleep(self.servoWait)
        self.pi2.set_servo_pulsewidth(self.servoVerticalPin,0)

    # moves servo 2 to the middle position
    def servoVerticalMid(self):            # vertical mic
        self.logDebug("servoVerticalMid()")
        self.pi2.set_servo_pulsewidth(self.servoVerticalPin,self.servoVerticalMidPulseWidth)
        sleep(self.servoWait)
        self.pi2.set_servo_pulsewidth(self.servoVerticalPin,0)

    def servoVerticalPan(self):            # vertical pan
        self.logDebug("servoVerticalPan()")
        self.bothLEDsOn()
        self.servoDown()
        self.leftDistance = self.readDistance()
        self.servoUp()
        self.rightDistance = self.readDistance()
        self.servoVerticalMid()
        self.midDistance = self.readDistance()
        self.bothLEDsOff()

    def getLeftDistance(self):
        self.logDebug("getLeftDistance()")
        self.logDebug("Left Distance: " + str(self.leftDistance))
        return self.leftDistance
    
    def getRightDistance(self):
        self.logDebug("getRigtDistance()")
        self.logDebug("Right Distance: " + str(self.rightDistance))
        return self.rightDistance
    
    def getMidDistance(self):
        self.logDebug("getMidDistance()")
        self.logDebug("Mid Distance: " + str(self.midDistance))
        return self.midDistance

    # Distance to nearest obstical via SR-04 ultrasonic range finder
    def readDistance(self):
        self.logDebug("readDistance()")
        sumDistance = 0.0
        maxDistance = 0.0
        minDistance = 1000000
        for i in range(0,self.distanceSamples):
            GPIO.output(self.distanceTriggerPin, True)
            sleep(0.00001)
            GPIO.output(self.distanceTriggerPin, False)
            while GPIO.input(self.distanceEchoPin) == 0:
                pulse_start = time()
            while GPIO.input(self.distanceEchoPin) == 1:
                pulse_end = time()
            pulse_duration = pulse_end - pulse_start
            distance = pulse_duration * 17150
            if distance > maxDistance:
                maxDistance = distance
            if distance < minDistance:
                minDistance = distance
            sumDistance += distance
            distance = round(distance, 2)
        sumDistance -= minDistance          # throw out the max distance
        sumDistance -= maxDistance          # throw out the min distance
        distance = sumDistance / (self.distanceSamples - 2)
        self.logDebug("Final Distance: " + str(distance) + " cm")
        return distance

    def leftLEDOn(self):
        self.logDebug("leftLEDOn()")
        MOTOR.setLED(self.motorAddress)

    def leftLEDOff(self):
        self.logDebug("leftLEDOff()")
        MOTOR.clrLED(self.motorAddress)

    def rightLEDOn(self):
        self.logDebug("rightLEDOn()")
        MOTOR.setLED(self.motorAddress)

    def rightLEDOff(self):
        self.logDebug("rightLEDOff()")
        MOTOR.clrLED(self.motorAddress)

    def bothLEDsOn(self):
        self.logDebug("bothLEDsOn()")
        MOTOR.setLED(self.motorAddress)

    def bothLEDsOff(self):
        self.logDebug("bothLEDsOff()")
        MOTOR.clrLED(self.motorAddress)

    def stopAllMotors(self):
        self.logDebug("stopAllMotors()")
        self.logDebug("Slowing...")
        MOTOR.dcSTOP(self.motorAddress,self.motorFrontLeft)
        MOTOR.dcSTOP(self.motorAddress,self.motorFrontRight)
        MOTOR.dcSTOP(self.motorAddress,self.motorRearLeft)
        MOTOR.dcSTOP(self.motorAddress,self.motorRearRight)
        self.off()
        self.logDebug("Stopped")

    def getDirection(self):
        self.logDebug("getDirection()")
        return self.direction

    def setAllMotorsForward(self,speed):
        self.logDebug("setAllMotorsForward()")
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontLeft,self.forward,speed,self.mediumAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontRight,self.forward,speed,self.mediumAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearLeft,self.forward,speed,self.mediumAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearRight,self.forward,speed,self.mediumAccelerate)
        self.logDebug("Set forward")

    def setAllMotorsReverse(self,speed):
        self.logDebug("setAllMotorsReverse()")
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontLeft,self.backward,speed,self.mediumAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontRight,self.backward,speed,self.mediumAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearLeft,self.backward,speed,self.mediumAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearRight,self.backward,speed,self.mediumAccelerate)
        self.logDebug("Set reverse")

    def startAllMotors(self):
        self.logDebug("startAllMotors()")
        MOTOR.dcSTART(self.motorAddress,self.motorFrontLeft)
        MOTOR.dcSTART(self.motorAddress,self.motorFrontRight)
        MOTOR.dcSTART(self.motorAddress,self.motorRearLeft)
        MOTOR.dcSTART(self.motorAddress,self.motorRearRight)
        self.logDebug("all motors started")

    def setAllMotorsLeft(self,speed):
        self.logDebug("startAllMotorsLeft()")
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontLeft,self.backward,speed,self.fastAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontRight,self.forward,speed,self.fastAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearLeft,self.backward,speed,self.fastAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearRight,self.forward,speed,self.fastAccelerate)
        self.logDebug("Set left")

    def setAllMotorsRight(self,speed):
        self.logDebug("startAllMotorsLeft()")
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontLeft,self.forward,speed,self.fastAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorFrontRight,self.backward,speed,self.fastAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearLeft,self.forward,speed,self.fastAccelerate)
        MOTOR.dcCONFIG(self.motorAddress,self.motorRearRight,self.backward,speed,self.fastAccelerate)
        self.logDebug("Set right")

    def goForward(self, seconds, speed):
        self.logDebug("goForward()")
        self.logDebug("Forward Seconds: " + str(seconds))
        self.setAllMotorsForward(speed)
        self.startAllMotors()
        sleep(seconds)
        self.stopAllMotors()

    def goReverse(self, seconds, speed):
        self.logDebug("goReverse()")
        self.logDebug("Reverse Seconds: " + str(seconds))
        self.setAllMotorsReverse(speed)
        self.startAllMotors()
        sleep(seconds)
        self.stopAllMotors()

    def goLeft(self, seconds, speed):
        self.logDebug("goLeft()")
        self.leftLEDOn()
        self.setAllMotorsLeft(speed)
        self.startAllMotors()
        sleep(seconds)
        self.stopAllMotors()
        self.leftLEDOff()

    def goRight(self, seconds, speed):
        self.logDebug("goRight()")
        self.leftLEDOn()
        self.setAllMotorsRight(speed)
        self.startAllMotors()
        sleep(seconds)
        self.stopAllMotors()
        self.rightLEDOff()

    def turnSharpLeft(self):
        self.logDebug("turnSharpLeft()")
        self.goLeft(self.sharpTurnTime, self.fastSpeed)

    def turnSharpRight(self):
        self.logDebug("turnSharpRight()")
        self.goRight(self.sharpTurnTime, self.fastSpeed)

    def spinHardRight180(self):
        self.logDebug("spinHardRight180()")
        self.goRight(self.hardTurnTime180, self.fastSpeed)

    def spinHardLeft180(self):
        self.logDebug("spinHardLeft180()")
        self.goLeft(self.hardTurnTime180, self.fastSpeed)

    def spinHardRight360(self):
        self.logDebug("spinHardRight360()")
        # set left motor forward, right motor backwards
        self.goRight(self.hardTurnTime360, self.fastSpeed)

    def spinHardLeft360(self):
        self.logDebug("spinHardLeft360()")
        self.goLeft(self.hardTurnTime360, self.fastSpeed)

    # turn everything off
    def off(self):
        self.logDebug("off()")
        MOTOR.RESET(self.motorAddress)

def main():
#    self.logDebug("main()")
    if len(args) != 2:
        print "pass the configuration file name as an argument"
        exit()
    print "using configuration file " + sys.argv[1]
    slowSpeed = 25.0
    mediumSpeed = 50.0
    fastSpeed = 100.0
    forwardTime = 2                     # seconds
    reverseTime = 2                     # seconds
    robot = Robot(slowSpeed, mediumSpeed, fastSpeed, sys.argv[1])
    robot.logDebug("1")
    robot.leftLEDOn();
    sleep(1)
    robot.logDebug("2")
    robot.leftLEDOff()
    sleep(1)
    robot.logDebug("3")
    robot.rightLEDOn()
    sleep(1)
    robot.logDebug("4")
    robot.rightLEDOff()
    sleep(1)
    robot.logDebug("5")
    robot.goForward(forwardTime,robot.fastSpeed)
    sleep(1)
    robot.logDebug("6")
    robot.goReverse(reverseTime,robot.fastSpeed)
    sleep(1)
    robot.logDebug("7")
    robot.goForward(forwardTime,robot.mediumSpeed)
    sleep(1)
    robot.logDebug("8")
    robot.goReverse(reverseTime,robot.mediumSpeed)
    sleep(1)
    robot.logDebug("9")
    robot.spinHardRight360()
    sleep(1)
    robot.logDebug("10")
    robot.spinHardLeft360()
    sleep(1)
    robot.logDebug("11")
    robot.spinHardRight180()
    sleep(1)
    robot.logDebug("12")
    robot.spinHardLeft180()
    sleep(1)
    robot.logDebug("13")
    robot.turnSharpLeft()
    sleep(1)
    robot.logDebug("14")
    robot.turnSharpRight()
    sleep(1)
    robot.logDebug("15")
    robot.goForward(6,robot.slowSpeed)
    sleep(1)
    robot.logDebug("16")
    robot.goReverse(6,robot.slowSpeed)
    sleep(1)
    robot.logDebug("17")
    robot.servoHorizontalMid()
    robot.servoVerticalMid()
    robot.servoLeft()
    robot.servoHorizontalMid()
    robot.servoRight()
    robot.servoHorizontalPan()
    robot.servoDown()
    robot.servoVerticalMid()
    robot.servoUp()
    robot.servoVerticalPan()
    print "left distance: ", robot.getLeftDistance()
    print "right distance: ", robot.getRightDistance()
    print "mid distance: ", robot.getMidDistance()
    robot.stopAllMotors()
    sleep(1)
    robot.off()
    print "Done"
    
if __name__ == "__main__": main()
