#!/usr/bin/python
import ConfigParser
import RPi.GPIO as GPIO
from time import sleep
import random
import sys
import ConfigParser
from Robot import Robot

def autonomy(configFile):

    config = ConfigParser.RawConfigParser()
    config.read(configFile)

    slowSpeed = float(config.get('RobotSpeed', 'slowSpeed'));
    mediumSpeed = float(config.get('RobotSpeed', 'mediumSpeed'));
    fastSpeed = float(config.get('RobotSpeed', 'fastSpeed'));
    forwardTime = float(config.get('RobotTime', 'forwardTime'));  # seconds
    reverseTime = float(config.get('RobotTime', 'reverseTime'));  # seconds
    minObsticalDistance = float(config.get('Distance', 'minObsticalDistance'));             # centimeters

    robot = Robot(slowSpeed, mediumSpeed, fastSpeed, sys.argv[1])
    robot.servoHorizontalMid()
    robot.servoHorizontalMid()
    robot.servoVerticalMid()
    robot.servoVerticalMid()

    while True:

        maxObsticalDistance, leftObstDist, rightObstDist, centerObstDist = getDistances(robot)
        
        while maxObsticalDistance > minObsticalDistance:

            if maxObsticalDistance == centerObstDist:
                print('We are going straight')
                robot.goForward(forwardTime, fastSpeed)
            elif maxObsticalDistance == leftObstDist:
                print('We are going left')
                robot.turnSharpLeft()
            elif maxObsticalDistance == rightObstDist:
                print('We are going right')
                robot.turnSharpRight()

            maxObsticalDistance, leftObstDist, rightObstDist, centerObstDist = getDistances(robot)

        print('We are going in reverse')
        robot.goReverse(reverseTime, mediumSpeed)
        print('We are doing a 180')
        robot.spinHardRight180();

def getDistances(robot):
    servoSleepTime = 0.10               # seconds
                                        # get left distance

    centerObstDist = robot.readDistance()

    robot.servoLeft()                   # move rangefinder left
    sleep(servoSleepTime)
    leftObstDist = robot.readDistance()

                                        # get right distance
    robot.servoRight()                  # move rangefinder right
    sleep(servoSleepTime)
    rightObstDist = robot.readDistance()

                                        # get mid distance
    robot.servoHorizontalMid()          # move rangefinder center
    sleep(servoSleepTime)
            
    maxObsticalDistance = max(leftObstDist, rightObstDist, centerObstDist)

    return maxObsticalDistance, leftObstDist, rightObstDist, centerObstDist
                    
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "pass the configuration file name as an argument"
        exit()
    print "using configuration file " + sys.argv[1]
    autonomy(sys.argv[1])
