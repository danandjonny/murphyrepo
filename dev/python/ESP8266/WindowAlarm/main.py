'''
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Jorge Aranda Moro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.

'''

#This part is to connect to the WiFi

WIFISSID='HOME-4D62'
WIFIPASS='basket2979fancy'

def do_connect():
    import network
    sta_if = network.WLAN(network.STA_IF)
#   ap_if = WLAN(network.AP_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        # activate the station interface
        sta_if.active(True)
        sta_if.connect(WIFISSID, WIFIPASS)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())
#   if ap_if.active():
#       ap_if.active(False)
        
#---End Wifi Config---

from machine import Pin

led = Pin(2, Pin.OUT, value=1)

#---MQTT Sending---

from time import sleep_ms
from ubinascii import hexlify
from machine import unique_id
#import socket
from umqtt import MQTTClient

SERVER = "10.0.0.3"
CLIENT_ID = hexlify(unique_id())
TOPIC1 = b"/security"
#TOPIC2 = b"/sensor1/hum"
TOPIC3 = b"/security_dev"

def envioMQTT(server=SERVER, topic="/security", dato=None):
    try:
        c = MQTTClient(CLIENT_ID, server)
        c.connect()
        c.publish(topic, dato)
        sleep_ms(200)
        c.disconnect()
        #led.value(1)
        print("Connected to %s, publishing topic %s, data %s" % (server, topic, dato))
    except Exception as e:
        pass
        #led.value(0)

state = 0

def sub_cb(topic, msg):
    global state
    print((topic, msg))
    if msg == b"on":
        led.value(0)
        state = 1
    elif msg == b"off":
        led.value(1)
        state = 0

def recepcionMQTT(server=SERVER, topic=TOPIC3):
    c = MQTTClient(CLIENT_ID, server)
    # Subscribed messages will be delivered to this callback
    c.set_callback(sub_cb)
    c.connect()
    c.subscribe(topic)
    print("Connected to %s, subscribed to %s topic" % (server, topic))
    try:
        # this blocks, waiting for a response
        c.wait_msg()
    finally:
        c.disconnect()

#---End MQTT Sending---

#---Reed Switch---

from machine import ADC

def readADC():
    adc = ADC(0)            # create ADC object on ADC pin
    adcValue = adc.read()              # read value, 0-1024
    return(adcValue)

#---End Reed Switch---

#---Main Program---
sleep_ms(10000)
do_connect()

while True:
    adcValue = readADC()
    envioMQTT(SERVER,TOPIC1, "red " + str(adcValue))
#   recepcionMQTT()
    sleep_ms(10000)

#---END Main Program---
