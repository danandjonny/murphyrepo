#/usr/bin/env python
# Copyright 2017 Daniel & Jonathan Murphy
#test for gitkraken.
#             _____
#            /     \
#           | o   o |
#           |   v   |
#           |  ___- |
#            \______/
#Import Modules
# History
# 2017-02-29    DJM Added the ability to shoot multiple lasers from the launcher
#
import time, random, os, pygame
from pygame.locals import *
from sys import exit
#import RPi.GPIO as GPIO

#test comment
#test comment by jonny
#test comment by Dan Murphy
if not pygame.font: print 'Warning, fonts disabled'
if not pygame.mixer: print 'Warning, sound disabled'


# functions to create our resources
def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    image = image.convert() #convert all images to the same format
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()


def load_sound(name):
    class NoneSound:
        def play(self): pass
    if not pygame.mixer or not pygame.mixer.get_init():
        return NoneSound()
    fullname = os.path.join('data', name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error, message:
        print 'Cannot load sound:', fullname
        raise SystemExit, message
    return sound
# Classes


class pyscope :
    screen = None;
    
    def __init__(self):
        "Initializes a new pygame screen using the framebuffer"
        # Based on "Python GUI in Linux frame buffer"
        # http://www.karoltomala.com/blog/?p=679
        disp_no = os.getenv("DISPLAY")
        if disp_no:
            print "I'm running under X display = {0}".format(disp_no)
        
        # Check which frame buffer drivers are available
        # Start with fbcon since directfb hangs with composite output
        drivers = ['fbcon', 'directfb', 'svgalib']
        found = False
        for driver in drivers:
            # Make sure that SDL_VIDEODRIVER is set
            if not os.getenv('SDL_VIDEODRIVER'):
                os.putenv('SDL_VIDEODRIVER', driver)
            try:
                pygame.display.init()
            except pygame.error:
                print 'Driver: {0} failed.'.format(driver)
                continue
            found = True
            break
    
        if not found:
            raise Exception('No suitable video driver found!')
        
        size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
        print "Framebuffer size: %d x %d" % (size[0], size[1])
        self.screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
        # Clear the screen to start
        self.screen.fill((0, 0, 0))        
        # Initialise font support
        pygame.font.init()
        # Render the screen
        pygame.display.update()
 
    def __del__(self):
        "Destructor to make sure pygame shuts down, etc."
 

class Launcher(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)  # call Sprite intializer
        self.image, self.rect = load_image('launcher4.jpeg', -1)
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.rect.topleft = 375, 476
        self.move = 0
        self.dizzy = 0

    def update(self):
        if self.dizzy:
            self._spin()
        else:
            launcher_newpos = self.rect.move((self.move, 0))
            if self.rect.left < self.area.left or \
                    self.rect.right > self.area.right:
                self.move = -self.move
                launcher_newpos = self.rect.move((self.move, 0))
                #self.image = pygame.transform.flip(self.image, 1, 0)
            self.rect = launcher_newpos

    def _spin(self):
        "spin the launcher image"
        center = self.rect.center
        self.dizzy += 12
        if self.dizzy >= 1440:
            self.dizzy = 0
            self.image = self.original
        else:
            rotate = pygame.transform.rotate
            self.image = rotate(self.original, self.dizzy)
        self.rect = self.image.get_rect(center=center)

    def shot(self):
        "this will cause the launcher to start spinning"
        if not self.dizzy:
            self.dizzy = 1
            self.original = self.image


class Spaceship(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #call Sprite intializer
        self.image, self.rect = load_image('spaceship4.jpeg', -1)
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.rect.topleft = 10, 10
        
        self.move = random.uniform(1,5)
        self.dizzy = 0

    def update(self):
        "walk or spin, depending on the spaceship's state"
        if self.dizzy:
            self._spin()
        else:
            self._walk()
        # decide if it's time to drop a bomb
        x = random.uniform(1, 300)
        if x > 50 and x < 51:
            for bomb in self.bomb_inventory:
                if bomb.dropping == 0:  # this bomb is not dropping
                    #print "dropping a bomb " + str(x)
                    bomb.dropping = 1  # drop the bomb
                    bomb.rect.topleft = self.rect.topleft #drop the bomb from the spaceship
                    break;
        

    def _walk(self):
        "move the spaceship across the screen, and turn at the ends"
        newpos = self.rect.move((self.move, self.bounce))
        if self.rect.left < self.area.left or \
           self.rect.right > self.area.right:
            self.move = -self.move
            newpos = self.rect.move((self.move, self.bounce))
            self.image = pygame.transform.flip(self.image, 1, 0)
        if self.rect.top < self.area.top or \
           self.rect.bottom > self.area.bottom - 200:
            self.bounce = -self.bounce
            newpos = self.rect.move((self.move, self.bounce))
        self.rect = newpos

    def _spin(self):
        "spin the spaceship image"
        center = self.rect.center
        self.dizzy = self.dizzy + 12
#       if self.dizzy >= 1440:
        if self.dizzy >= 360:
            self.dizzy = 0
            self.image = self.original
            # let the spaceship die here
            self.kill();
        else:
            rotate = pygame.transform.rotate
            self.image = rotate(self.original, self.dizzy)
        self.rect = self.image.get_rect(center=center)

    def shot(self):
        "this will cause the spaceship to start spinning"
        if not self.dizzy:
            self.dizzy = 1
            self.original = self.image

#            GPIO.output(LedPin, GPIO.LOW)  # led on

    def setBombInventory(self, bomb_inventory):
        self.bomb_inventory = bomb_inventory


class Laser(pygame.sprite.Sprite):
    # the laser is shot from the Gun
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)  # call Sprite initializer
        self.image, self.rect = load_image('bomb_drop2.jpg', -1)
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.rect.topleft = 375, 496
        self.move = 0
        self.shooting = 0
        self.shoot_sound = load_sound('ship_fire.wav')

    def update(self):
        "move the laser"
        if self.shooting:
            self.rect = self.rect.move(0, -10)  # move the laser sprite up
            hitbox = self.rect.inflate(-5, -5)
            i = 0
            while i < 10:
                if hitbox.colliderect(self.spaceship_list[i].rect): # we hit the spaceship
                    self.spaceship_list[i].shot() #make the spaceship spin
                    self.shooting = 0  #turn off 'shooting'
                i += 1
        else: #if we're not shooting
            laserx, lasery = self.launcher.rect.topleft
            self.rect.topleft = laserx + 26, lasery #laser follows the launcher 
            self.rect = self.rect.move((self.move, 0))
        if self.rect.top < self.area.top:
            self.shooting = 0 # we missed the spaceship

    def setLauncherAndSpaceship(self, theLauncher, theSpaceshipList): #tell the laser about the
                                                                      #spaceship and the launcher
        self.launcher = theLauncher
        self.spaceship_list = theSpaceshipList

    def shoot(self):
#        GPIO.output(LedPin, GPIO.HIGH) # led off
        self.shooting = 1
        self.shoot_sound.play() #shoot

class Bomb(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self) #call Sprite initializer
        self.image, self.rect = load_image('bomb_drop2.jpg', -1)
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.move = 0
        self.dropping = 0

    def update(self):
        "move the bomb"
        if self.dropping:
            self.rect = self.rect.move(0,5) # move the bomb sprite down
            hitbox = self.rect.inflate(-5, -5)
            if hitbox.colliderect(self.launcher.rect): # we hit the laser gun
                self.launcher.shot() # make the laser spin
                self.dropping = 0
            if self.rect.top > self.area.bottom:
                self.dropping = 0 # we missed the laser

    def setParentSpaceship(self, theSpaceship):
        self.rect.topleft = theSpaceship.topleft

    def setLauncher(self, theLauncher): # tell the bomb about the launcher
        self.launcher = theLauncher

    def drop(self):
        self.dropping = 1

def main():
    """this function is called when the program starts.
       it initializes everything it needs, then runs in
       a loop until the function returns."""
    #Initialize Everything
    pygame.init()

# Create an instance of the PyScope class
    scope = pyscope()
    # scope.screen.fill((255, 255, 0))  # we already did this in pyscope()
    scope.screen = pygame.display.set_mode((1000, 600))
    pygame.display.set_caption('Alien Invasion')
    pygame.mouse.set_visible(0)

    # Create The Backgound
    background = pygame.Surface(scope.screen.get_size())
    background = background.convert()
    background.fill((0, 0, 0)) #black background

    # Put Text On The Background, Centered
    if pygame.font:
        font = pygame.font.Font(None, 36)
        text = font.render("Hit The Spaceship", 1, (10, 10, 10))
        textpos = text.get_rect(centerx=background.get_width()/2)
        background.blit(text, textpos)

    # Display The Background
    scope.screen.blit(background, (0, 0))
    pygame.display.flip()

    # Prepare Game Objects
    clock = pygame.time.Clock()
    whiff_sound = load_sound('whiff.wav')

    #create the gun that fires
    launcher = Launcher()

    # fill your magazine with lasers
    #   laser = Laser()
    laser_clip = []
    k = 0
    while k < 5:
        laser = Laser()
        laser_clip.append(laser)
        k += 1

    # create an inventory of 100 bombs
    bomb_inventory = []
    j = 0
    while j < 100:
        bomb = Bomb()
        bomb.setLauncher(launcher)
        bomb_inventory.append(bomb)
        j += 1

    # create a fleet of space ships
    spaceship_list = []
    i = 0
    while i < 10:
        spaceship_list.append(Spaceship())
        spaceship_list[i].bounce = random.uniform(1,5)
        spaceship_list[i].setBombInventory(bomb_inventory)
        i += 1

    # tell the laser beam about the launcher and the fleet of spaceships.
#   laser.setLauncherAndSpaceship(launcher, spaceship_list)
    n = 0
    while n < 5:
        laser_clip[n].setLauncherAndSpaceship(launcher, spaceship_list)
        n +=1

    allsprites = pygame.sprite.Group((spaceship_list,launcher,laser_clip,bomb_inventory))

    #point to the next laser to fire
    round = 0

    #Main Loop
    while 1:
        clock.tick(60)

        #Handle Input Events
        for event in pygame.event.get():
            if event.type == KEYDOWN and event.key == K_SPACE:
                laser_clip[round].shoot()
                round += 1
                if round == 5:
                    round = 0
            elif  event.type == KEYDOWN and event.key == K_LEFT:
                launcher.move = -5
                laser_clip[round].move = launcher.move
            elif event.type == KEYDOWN and event.key == K_RIGHT:
                launcher.move = 5
                laser_clip[round].move = launcher.move
            elif event.type == KEYDOWN and event.key == K_DOWN:
                launcher.move = 0
                laser_clip[round].move = launcher.move
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                exit()
            elif event.type == QUIT:
                exit()

        allsprites.update()

        #Draw Everything
        scope.screen.blit(background, (0, 0))
        allsprites.draw(scope.screen)
        pygame.display.flip()

    #Game Over

#this calls the 'main' function when this script is executed

# intitialize gpio stuff
#LedPin = 11    # pin11
 
#GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
#GPIO.setup(LedPin, GPIO.OUT)   # Set LedPin's mode as output
#GPIO.output(LedPin, GPIO.HIGH) # Set LedPin as high(+3.3V) to turn off the led

if __name__ == '__main__': main()

