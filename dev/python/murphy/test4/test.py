import urllib.request
import urllib.parse
import re

url = 'https://www.google.com/finance?q=aapl&ei=4QYvWanOJ9CKe8LbiKAC'
values = {'s':'basics',
           'submit':'search'}
data = urllib.parse.urlencode(values)
data = data.encode('utf-8')
req = urllib.request.Request(url, data)
resp = urllib.request.urlopen(req)
respData = resp.read()

# print(respData)

paragraphs = re.findall(r'<span class="pr">(.+?)</span>', str(respData))

for eachP in paragraphs:
    print(eachP)
'''
<span class="pr">
<span id="ref_22144_l" class="unchanged"><span class="unchanged">153.18</span><span></span></span>
</span>
'''