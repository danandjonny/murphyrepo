from random import randint
import array

#establish answer and history array
x = (randint(1,100))
history=[]

print('I\'m thinking of a number 1 to 100')
guess = input('Can you guess what it is?\n')
history.append(int(guess))

while(guess!= x):
    if(int(guess) < x):
        guess = input('Your guess was too low, try again.\n')
        history.append(int(guess))

    if(int(guess) > x):
        guess = input('Your guess was too high, try again.\n')
        history.append(int(guess))

    if(int(guess) == x):
        print('Congratulations! you guessed correct.')

        print('Here is a printout of your guesses.')
        print(history)

        print('Here is the sum of your guesses.')
        print(sum(history))

        print('Here is the mean of your guesses.')
        print(round((sum(history)/len(history)),2))

        break