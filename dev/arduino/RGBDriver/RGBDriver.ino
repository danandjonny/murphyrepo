int redPin=11;  //set red LED pin to 11
int greenPin=10; //set green LED pin to 10
int bluePin=6; //set blue LED pin to 6
 
void setup() {
  Serial.begin(9600); //Turn on Serial port
  pinMode(redPin, OUTPUT); //Set redPin to be an output
  pinMode(greenPin, OUTPUT); //Set greenPin to be an output
  pinMode(bluePin, OUTPUT); //set bluePin to be an output
}
 
void loop() {
  for (int i = 1; i < 255; i++) {
    analogWrite(bluePin, i); //turn off red pin
    delay(25);
    for (int j = 1; j < 255; j++) {
      analogWrite(greenPin, j); //turn off green pin
      delay(25);
      for (int k = 1; k < 255; k++) {
        analogWrite(redPin, k); //write 100 (brightness) to blue pin
        delay(25);
      }
    }
  }
}

