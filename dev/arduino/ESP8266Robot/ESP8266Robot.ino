#include <ESP8266WiFi.h>                                                // include libraries
#include <ESP8266WebServer.h>
#include <Servo.h> 

#define motorPWMLeft  D1                                                // = PWMA Motor A              arduino pin 5
#define motorPWMRight D2                                                // = PWMB Motor B              arduino pin 4
#define motorDirLeft  D3                                                // = DA direction of motor A   arduino pin 0
#define motorDirRight D4                                                // = DB direction of motor B   arduino pin 2
#define dirBackwards  LOW
#define dirForwards   HIGH 
#define SERVOPIN      16                                                // = D0 pin to which the servo is attached
#define STRAIGHT      90                                                // 0 = left, 90 = straight. 180 = right    
#define OFFSET        0
#define HARDLEFT      45      
#define HARDRIGHT     115     
#define DIFFERENTIAL  500
#define STOP          0
#define TRUE          1
#define FALSE         0

boolean isLeft = FALSE;
boolean isRight = FALSE;
boolean isStraight = TRUE;
int vehicleSpeed = 1022;

Servo servo;                                                            // define variable for the Servo object
ESP8266WebServer server(80);                                            // configure server
const char *form = "<center><form action='/'>"
"<button name='dir' type='submit' value='4'>Forward</button><br>"
"<button name='dir' type='submit' value='1'>Left</button> "
"<button name='dir' type='submit' value='6'>Straight</button> "
"<button name='dir' type='submit' value='2'>Right</button><br>"
"<button name='dir' type='submit' value='3'>Reverse</button><p>"
"<button name='dir' type='submit' value='5'>Stop</button>"
"</form></center>";

int calcAngle(int myAngle) {
    return myAngle + OFFSET;
}

void stop(void) {
    analogWrite(motorPWMLeft,   STOP);
    analogWrite(motorPWMRight,  STOP);
}

void forward(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed);
    digitalWrite(motorDirLeft, dirForwards);
    digitalWrite(motorDirRight, dirForwards);
}

void backward(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed);
    digitalWrite(motorDirLeft, dirBackwards);
    digitalWrite(motorDirRight, dirBackwards);
}

void left(void) {
    analogWrite(motorPWMLeft, vehicleSpeed - DIFFERENTIAL);
    analogWrite(motorPWMRight, vehicleSpeed);
    digitalWrite(motorDirLeft, dirForwards);
    digitalWrite(motorDirRight, dirForwards);
    servo.write(calcAngle(HARDLEFT));
    isLeft      = TRUE;
    isRight     = FALSE;
    isStraight  = FALSE;
}

void right(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed - DIFFERENTIAL);
    digitalWrite(motorDirLeft, dirForwards);
    digitalWrite(motorDirRight, dirForwards);
    servo.write(calcAngle(HARDRIGHT));
    isLeft      = FALSE;
    isRight     = TRUE;
    isStraight  = FALSE;
}

void goStraight(void) {
    servo.write(calcAngle(STRAIGHT));
    if (isLeft) {
      servo.write(calcAngle(STRAIGHT));
    } else if (isRight) {
//    servo.write(calcAngle(STRAIGHT));
      servo.write(calcAngle(HARDLEFT)); // when pointing right it was not returning to STRAIGHT, so this workaround was
      delay(200);                       // implemented.  Regardless of power supply
      servo.write(calcAngle(STRAIGHT));
    }
    isLeft     = FALSE;
    isRight    = FALSE;
    isStraight = TRUE;
}

void handle_form() {
    // only move if we submitted the form
    if (server.arg("dir"))
    {
        // get the value of request argument "dir"
        int direction = server.arg("dir").toInt();
        // chose direction
        switch (direction)
        {
            case 1:
                left();
                break;
            case 2:
                right();
                break;
            case 3:
                backward();
                break;
            case 4:
                forward();
                break;
            case 5:
                stop();
                break;
            case 6:
                goStraight();
                break;
        }
        delay(300);                                                     // move for 300ms, gives chip time to update wifi also
    }
    server.send(200, "text/html", form);                                // in all cases send the response
}

void setup() {
    Serial.begin(115200);
    WiFi.begin("HOME-4D62", "basket2979fancy");                         // connect to wifi network
    WiFi.config(IPAddress(10,0,0,50), IPAddress(10,0,0,1), IPAddress(255,255,255,0)); // static ip, gateway, netmask
    while (WiFi.status() != WL_CONNECTED) { 
      Serial.print('.');
      delay(200); 
    }               // connect
    Serial.println ( "" );
    Serial.print ( "WiFi connected to " );
    Serial.println ( "HOME-4D62" );
    Serial.print ( "IP address: " );
    Serial.println ( WiFi.localIP() );
    server.on("/", handle_form);                                        // set up the callback for http server
    server.begin();                                                     // start the webserver
    pinMode(D1, OUTPUT);                                                // 5 1,2EN aka D1 pwm left
    pinMode(D2, OUTPUT);                                                // 4 3,4EN aka D2 pwm right
    pinMode(D3, OUTPUT);                                                // 0 1A,2A aka D3
    pinMode(D4, OUTPUT);                                                // 2 3A,4A aka D4
    servo.attach(SERVOPIN);  
    servo.write(calcAngle(STRAIGHT));                                   // point the vehicle straight ahead   
    goStraight();    
}

void loop() {
    server.handleClient();                                              // check for client connections
}
