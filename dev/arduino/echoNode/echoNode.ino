//************************************************************
// this is a simple example that uses the painlessMesh library and echos any
// messages it receives
//
//************************************************************
#include "painlessMesh.h"

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

// Prototypes
void receivedCallback( uint32_t from, String &msg );
// Prototypes
void receivedCallback(uint32_t from, String & msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback(); 
void nodeTimeAdjustedCallback(int32_t offset); 

painlessMesh  mesh;
SimpleList<uint32_t> nodes;

void setup() {
  Serial.begin(115200);
    
  mesh.setDebugMsgTypes( ERROR | STARTUP | CONNECTION );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

}

uint32_t deviceID = 3;
int previousSensorValue = 0;

void loop() {
  mesh.update();
  int sensorValue = analogRead(A0);                       // read the input on analog pin 0
  if (sensorValue != previousSensorValue) {
    //Serial.println(sensorValue);                        // print out the value you read
    char buffer[sizeof(int) * 4 + 1];
    sprintf(buffer, "%d", sensorValue);  
    String msg(buffer);
    //mesh.sendSingle(deviceID,msg);
    //mesh.sendBroadcast(msg);
  }
  previousSensorValue = sensorValue;
}

void receivedCallback( uint32_t from, String &msg ) {
  Serial.printf("echoNode: Received from %u msg=%s\n", from, msg.c_str());
  mesh.sendBroadcast(msg);
  //mesh.sendSingle(from, msg);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
 
  nodes = mesh.getNodeList();

  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");

  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end()) {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();
}

void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void newConnectionCallback(uint32_t nodeId) {
  // Reset blink task
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}
