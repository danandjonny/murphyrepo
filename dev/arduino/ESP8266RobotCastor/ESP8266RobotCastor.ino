/*
 *  This program controls a three wheeled robot; two rear wheels attached to dc motors and a front castor wheel.
 *  Dan Murphy, 2018-01-23
 */
#include <ESP8266WiFi.h>                                                // include libraries
#include <ESP8266WebServer.h>
//                            ARDUINO  ESP                      WIRE
//                            PIN      PIN PURPOSE  MOTOR SIDE  COLOR   
#define motorPWMLeft    4  // gpio4  = D2  PWM      A     LEFT  YELLOW  
#define motorPWMRight   5  // gpio5  = D1  PWM      B     RIGHT BLUE
#define motorDirLeft2   14 // gpio14 = D5  DIR      A     LEFT  MAGENTA
#define motorDirLeft1   12 // gpid12 = D6  DIR      A     LEFT  GREEN
#define motorDirRight2  2  // gpio2  = D4  DIR      B     RIGHT GREEN 
#define motorDirRight1  0  // gpio0  = D3  DIR      B     RIGHT BLUE

#define STOP            0
#define MAX_SPEED       1023
#define MIN_SPEED       255
#define SPEED_INCREMENT 128
#define BUILTIN_LED2    16  

int vehicleSpeed = 1023;

ESP8266WebServer server(80);                                            // configure server
const char *form = "<center><form action='/'>"
"<button name='dir' type='submit' value='4'>Forward</button><br>"
"<button name='dir' type='submit' value='1'>Left</button> "
"<button name='dir' type='submit' value='3'>Reverse</button> "
"<button name='dir' type='submit' value='2'>Right</button><p>"
"<button name='dir' type='submit' value='5'>Stop</button><br>"
"<button name='dir' type='submit' value='7'>Slower</button> "
"<button name='dir' type='submit' value='8'>Faster</button> "
"</form></center>";

void off(void) {
  digitalWrite(motorDirLeft1,   LOW);
  digitalWrite(motorDirLeft2,   LOW);  
  digitalWrite(motorDirRight1,  LOW);
  digitalWrite(motorDirRight2,  LOW);  
}

void leftMotorOnForward(void) {
  digitalWrite(motorDirLeft1, HIGH);
  digitalWrite(motorDirLeft2, LOW);
}

void rightMotorOnForward(void) {
  digitalWrite(motorDirRight1, HIGH);
  digitalWrite(motorDirRight2, LOW);
}

void leftMotorOnReverse(void) {
  digitalWrite(motorDirLeft1, LOW);
  digitalWrite(motorDirLeft2, HIGH);
}

void rightMotorOnReverse(void) {
  digitalWrite(motorDirRight1, LOW);
  digitalWrite(motorDirRight2, HIGH);
}

void faster(void) {
  vehicleSpeed += SPEED_INCREMENT;
  if (vehicleSpeed > MAX_SPEED) { vehicleSpeed = MAX_SPEED; }
}

void slower(void) {
  vehicleSpeed -= SPEED_INCREMENT;
  if (vehicleSpeed < MIN_SPEED) { vehicleSpeed = MIN_SPEED; }
}

void stop(void) {
    analogWrite(motorPWMLeft,   STOP);
    analogWrite(motorPWMRight,  STOP);
    off();
}

void changeSpeed(void) {
  analogWrite(motorPWMLeft, vehicleSpeed);
  analogWrite(motorPWMRight, vehicleSpeed);
}

void forward(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed);
    leftMotorOnForward();
    rightMotorOnForward();
}

void reverse(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed);
    leftMotorOnReverse();
    rightMotorOnReverse();
}

void left(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed);
    leftMotorOnReverse();
    rightMotorOnForward();
}

void right(void) {
    analogWrite(motorPWMLeft, vehicleSpeed);
    analogWrite(motorPWMRight, vehicleSpeed);
    leftMotorOnForward();
    rightMotorOnReverse();
}

void handle_form() {
    // only move if we submitted the form
    if (server.arg("dir"))
    {
        digitalWrite(BUILTIN_LED2, HIGH);
        // get the value of request argument "dir"
        int direction = server.arg("dir").toInt();
        // chose direction
        switch (direction)
        {
            case 1:
                left();
                break;
            case 2:
                right();
                break;
            case 3:
                reverse();
                break;
            case 4:
                forward();
                break;
            case 5:
                stop();
                break;
            case 7:
                slower();
                break;
            case 8:
                faster();
                break;
        }
        delay(300);                                                     // move for 300ms, gives chip time to update wifi also
        digitalWrite(BUILTIN_LED2, LOW);
    }
    server.send(200, "text/html", form);                                // in all cases send the response
}

void setup() {
    digitalWrite(BUILTIN_LED2, HIGH);
    Serial.begin(115200);
    WiFi.begin("HOME-4D62", "basket2979fancy");                         // connect to wifi network
    WiFi.config(IPAddress(10,0,0,50), IPAddress(10,0,0,1), IPAddress(255,255,255,0)); // static ip, gateway, netmask
    while (WiFi.status() != WL_CONNECTED) { 
      Serial.print('.');
      delay(200); 
    }               // connect
    Serial.println ( "" );
    Serial.print ( "WiFi connected to " );
    Serial.println ( "HOME-4D62" );
    Serial.print ( "IP address: " );
    Serial.println ( WiFi.localIP() );
    server.on("/", handle_form);                                        // set up the callback for http server
    server.begin();                                                     // start the webserver
    
    // set all the motor control pins to outputs
    pinMode(motorPWMLeft,   OUTPUT);                                           
    pinMode(motorPWMRight,  OUTPUT);                                           
    pinMode(motorDirLeft1,  OUTPUT);                                            
    pinMode(motorDirLeft2,  OUTPUT);                                            
    pinMode(motorDirRight1, OUTPUT);
    pinMode(motorDirRight2, OUTPUT);
    
    digitalWrite(BUILTIN_LED2, LOW);
}

void loop() {
    server.handleClient();                                              // check for client connections
}
