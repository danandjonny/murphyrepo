#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <SPI.h>

const int motor1Control = 2; // Right motor
const int motor2Control = 3; // Left motor
const int motor1Input1 = 4;
const int motor1Input2 = 5;
const int motor2Input1 = 8;
const int motor2Input2 = 9;
const int wifiLed = 10; // For indicating successful Wi-Fi connection

const char* ssid = "HOME-4D62";
const char* pass = "basket2979fancy";

int keyIndex = 0;          // Fill in your network key Index number (Optional - needed only for WEP)

int status = WL_IDLE_STATUS;
String readString;

//WiFiServer server(80); // Define the port of the server
ESP8266WebServer server ( 80 );

String speedMode = "fast";

void setup() {
  Serial.begin(115200);
  Serial.println("Starting");
  // Attempt to connect to Wifi network:
  Serial.print("Connecting to wifi...");
  status = WiFi.begin(ssid, pass);
  while ( WiFi.status() != WL_CONNECTED) { 
    Serial.print('.');
    delay(1000);
  }

  Serial.println ( "" );
  Serial.print ( "WiFi connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );

  // Begin the webserver
  server.begin();

  pinMode(motor1Control, OUTPUT);
  pinMode(motor2Control, OUTPUT);
  pinMode(motor1Input1, OUTPUT);
  pinMode(motor1Input2, OUTPUT);
  pinMode(motor2Input1, OUTPUT);
  pinMode(motor2Input2, OUTPUT);
  pinMode(wifiLed, OUTPUT);
  
  // Indicate that the server is running
  digitalWrite(wifiLed, HIGH);
}

void loop() {
  // Listen for incoming clients
//  WiFiClient client = server.available();
  WiFiClient client;
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        // If you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        
        if (readString.length() < 100) {
          //store characters to string
          readString += c;
        }
        
        if (c == '\n') {
          // send a standard http response header
          const char *form = "client.println("HTTP/1.1 200 OK")"
          "Content-Type: text/html"
          "<!DOCTYPE HTML>"
          "<html>"
          "<head>"
          "<style>"
          "a.button {-webkit-appearance: button;"
          "-moz-appearance: button;"
          "appearance: button;"
          "height:400px;"
          "line-height:400px;"
          "text-align:center;"
          "text-decoration: none;"
          "font-size: 100px;"
          "color: initial;}"
          "</style>"
          "</head>"
          "<body>"
          "<a href=\"/?moveForwards\" class=\"button\" style=\"width:100%;\"\">FORWARDS</a>"
          "<br />"
          "<a href=\"/?turnLeft\" class=\"button\" style=\"width:35%;\"\">LEFT</a>"
          "<a href=\"/?stopMoving\" class=\"button\" style=\"width:29%;\"\">STOP</a>"
          "<a href=\"/?turnRight\" class=\"button\" style=\"width:35%;\"\">RIGHT</a>"
          "<br />"
          "<a href=\"/?moveBackwards\" class=\"button\" style=\"width:100%;\"\">BACKWARDS</a>"
          "<br />"
          "<br />"
          "<h1 style=\"text-align:center;\">SPEED MODES</h1>"
          "<a href=\"/?fast\" class=\"button\" style=\"width:45%;\"\">FAST</a>"
          "<a href=\"/?slow\" class=\"button\" style=\"width:45%;\"\">SLOW</a>"
          "</body>"
          "</html>";
           break;
        }
        
        // Decide which button was clicked (if any)
        // Fast Mode button
        if (readString.indexOf("?fast") > 0){
          speedMode = "fast";
          // Clear the readString to be able to get the next command
          readString = "";
        }
        // Slow Mode button
        if (readString.indexOf("?slow") > 0){
          speedMode = "slow";
          // Clear the readString to be able to get the next command
          readString = "";
        }
        // Fast Mode
        if (speedMode == "fast"){
          if (readString.indexOf("?moveForwards") > 0){
            moveForwards();
            // Clear the readString to be able to get the next command
            readString = "";
          }
          if (readString.indexOf("?moveBackwards") > 0){
            moveBackwards();
            // Clear the readString to be able to get the next command
            readString = "";
          }
        } else if (speedMode == "slow"){
          // Slow Mode
          if (readString.indexOf("?moveForwards") > 0){
            moveForwardsSlow();
            // Clear the readString to be able to get the next command
            readString = "";
          }
          if (readString.indexOf("?moveBackwards") > 0){
            moveBackwardsSlow();
            // Clear the readString to be able to get the next command
            readString = "";
          }
        }
        if (readString.indexOf("?turnLeft") > 0){
          turnLeft();
          // Clear the readString to be able to get the next command
          readString = "";
        }
        if (readString.indexOf("?turnRight") > 0){
          turnRight();
          // Clear the readString to be able to get the next command
          readString = "";
        }
        if (readString.indexOf("?stopMoving") > 0){
          stopMoving();
          // Clear the readString to be able to get the next command
          readString = "";
        }
      }
    }
    // Give the web browser time to receive the data
    delay(1);
    // Close the connection:
    client.stop();
  }
}

// Commands for moving forwards
void moveForwards(){
  digitalWrite(motor1Control, HIGH);
  digitalWrite(motor2Control, HIGH);
  digitalWrite(motor1Input1, LOW);
  digitalWrite(motor1Input2, HIGH);
  digitalWrite(motor2Input1, LOW);
  digitalWrite(motor2Input2, HIGH);
}
// Commands for moving backwards
void moveBackwards(){
  digitalWrite(motor1Control, HIGH);
  digitalWrite(motor2Control, HIGH);
  digitalWrite(motor1Input1, HIGH);
  digitalWrite(motor1Input2, LOW);
  digitalWrite(motor2Input1, HIGH);
  digitalWrite(motor2Input2, LOW);
}
// Commands for turning right
void turnRight(){
  // Lower voltage is provided to turn slower - better control
  analogWrite(motor1Control, 0);
  analogWrite(motor2Control, 200);
  digitalWrite(motor1Input1, HIGH);
  digitalWrite(motor1Input2, LOW);
  digitalWrite(motor2Input1, LOW);
  digitalWrite(motor2Input2, HIGH);
}
// Commands for turning left
void turnLeft(){
  // Lower voltage is provided to turn slower - better control
  analogWrite(motor1Control, 200);
  analogWrite(motor2Control, 0);
  digitalWrite(motor1Input1, LOW);
  digitalWrite(motor1Input2, HIGH);
  digitalWrite(motor2Input1, HIGH);
  digitalWrite(motor2Input2, LOW);
}
// Commands for stopping the car
void stopMoving() {
  digitalWrite(motor1Control, LOW);
  digitalWrite(motor2Control, LOW);
  digitalWrite(motor1Input1, LOW);
  digitalWrite(motor1Input2, LOW);
  digitalWrite(motor2Input1, LOW);
  digitalWrite(motor2Input2, LOW);
}
// Commands for low speed forward moving
void moveForwardsSlow(){
  analogWrite(motor1Control, 200);
  analogWrite(motor2Control, 200);
  digitalWrite(motor1Input1, LOW);
  digitalWrite(motor1Input2, HIGH);
  digitalWrite(motor2Input1, LOW);
  digitalWrite(motor2Input2, HIGH);
}
// Commands for low speed backward moving
void moveBackwardsSlow(){
  analogWrite(motor1Control, 200);
  analogWrite(motor2Control, 200);
  digitalWrite(motor1Input1, HIGH);
  digitalWrite(motor1Input2, LOW);
  digitalWrite(motor2Input1, HIGH);
  digitalWrite(motor2Input2, LOW);
}
