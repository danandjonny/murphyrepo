#ifndef _PIN_CONFIG_H
#define _PIN_CONFIG_H

#include "prj.h"

#define SCL_PORT        PORTD   //PORTB
#define SCL_DIR         DDRD    //DDRB 
#define SCL_PIN         PIND    //PINB
#define SCL_BIT         0       //2   

#define SDA_PORT        PORTD   //PORTB
#define SDA_DIR         DDRD    //DDRB 
#define SDA_PIN         PIND    //PINB
#define SDA_BIT         1       //0           



#endif
