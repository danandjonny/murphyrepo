/*
 * Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL   
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 /* Modified on 2016-04-17 by Daniel Murphy
  *   - Plotting actual sensor values now, obtaining them via I2C 
  *   - Changed the size of the x and y axis
  *   - Added code to talk to trigger a text message when the sensor threshold
  *     is exceeded, via ThingHTTP and Twilio
  */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Wire.h>

#include "ThingSpeak.h"
#include <SPI.h>
//#include <WiFi.h>

const char* ssid = "HOME-4D62";
const char* password = "basket2979fancy";
int sensorArray[200];
int thresholdArray[200];
int readingNum = 0;
uint8_t loopCount = 0;
uint8_t sensorTurn = 1;
uint8_t sentText = 0;
int status = WL_IDLE_STATUS;

WiFiClient  client;
WiFiClient  clientGet;
IPAddress   serverGet(52,6,12,3);                                   // ThingSpeak 

ESP8266WebServer server ( 80 );

const int led = 16;

void drawGraph() {
  String out = "";
  char temp[100];
  out += "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"1400\" height=\"600\">\n";
  out += "<rect width=\"1400\" height=\"600\" fill=\"rgb(250, 230, 210)\" stroke-width=\"1\" stroke=\"rgb(0, 0, 0)\" />\n";
  out += "<g stroke=\"black\">\n";
  for (int x = 120; x > 0; x -= 1) {
    sprintf(temp, "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke-width=\"1\" />\n", ((120 - x) * 10) + 10, 560 - sensorArray[x-1], (120 - x) * 10, 560 - sensorArray[x]);
    out += temp;
  }
  out += "</g>\n</svg>\n";

  server.send ( 200, "image/svg+xml", out);
}

void handleRoot() {
  digitalWrite ( led, 1 );
  char temp[400];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  snprintf ( temp, 400,

"<html>\
  <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>ESP8266 Demo</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>Methane Sensor</h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
    <img src=\"/test.svg\" />\
  </body>\
</html>",

    hr, min % 60, sec % 60
  );
  server.send ( 200, "text/html", temp );
  digitalWrite ( led, 0 );
}

void handleNotFound() {
  digitalWrite ( led, 1 );
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }

  server.send ( 404, "text/plain", message );
  digitalWrite ( led, 0 );
}

void setup ( void ) {
  pinMode ( led, OUTPUT );
  digitalWrite ( led, 0 );
  Serial.begin ( 115200 );
  status = WiFi.begin ( ssid, password );
  Serial.println ( "" );

  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  Serial.println ( "" );
  Serial.print ( "WiFi connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );

  if ( MDNS.begin ( "esp8266" ) ) {
    Serial.println ( "MDNS responder started" );
  }

  server.on ( "/", handleRoot );
  server.on ( "/test.svg", drawGraph );
  server.on ( "/inline", []() {
    server.send ( 200, "text/plain", "this works as well" );
  } );
  server.onNotFound ( handleNotFound );
  server.begin();
  Serial.println ( "HTTP server started" );

  Wire.begin();                                                             // join i2c bus, default clock rate = 100kHz 

  ThingSpeak.begin(client);
}

void loop ( void ) {
  uint16_t sensorValue;
  uint16_t thresholdValue;
  
  loopCount++;
  
  server.handleClient();

  Wire.requestFrom(0x10, 4);                                                // request 4 bytes from slave device #8
  while (Wire.available()) {                                                // slave may send less than requested
    uint8_t highByteSensor = Wire.read();
    uint8_t lowByteSensor = Wire.read();
    sensorValue = (highByteSensor<<8)|lowByteSensor;
    Serial.println(sensorValue);
    uint8_t highByteThreshold = Wire.read();
    uint8_t lowByteThreshold = Wire.read();
    thresholdValue = (highByteThreshold<<8)|lowByteThreshold;
    Serial.println(thresholdValue);

    for (int i = 200; i > 0; i--) {                                         // shift all of the readings up one
      sensorArray[i] = sensorArray[i - 1];
//    thresholdArray[i] = thresholdArray[i - 1];
    }
    sensorArray[0] = sensorValue;
//  thresholdArray[0] = thresholdValue;
  }
  delay(500);                                                               // approx. 1 sensor read per second, 1 theshold read per second
                                                                            // TODO: make the threshold reads less frequent and sensor reads more frequent
  if ((sensorValue >= thresholdValue) and !sentText) {                      // Trigger a text message via ThingSpeak and Twilio if sensor value exceeds threshold value
    sentText = 1;                                                           // after 1 SMS is sent, ESP8266 must be reset before another SMS is sent
    Serial.println("Sensor value exceeds threshold value.");
    if (clientGet.connect(serverGet, 80)) {             
      Serial.println("connected"); 

      clientGet.print("GET /apps/thinghttp/send_request?api_key=");         // To trigger the text message via browser:  https://api.thingspeak.com/apps/thinghttp/send_request?api_key=YD4YRQ3ZHOX4E2X6
      clientGet.print("YD4YRQ3ZHOX4E2X6");
      clientGet.print("&number=");
      clientGet.print("9788090770");
      clientGet.print("&message=");
      clientGet.print("NoOp");                                              // This is ignored
      clientGet.println(" HTTP/1.1");
      clientGet.print("Host: ");
      clientGet.println("api.thingspeak.com");
      clientGet.println("Connection: close");
      clientGet.println();

      delay(10);
      while(clientGet.available()){                                         // Read all the lines of the reply from server and print them to Serial
        String line = clientGet.readStringUntil('\r');
        Serial.print(line);
      } 
      Serial.println();
      Serial.println("closing connection");
    } else {
      Serial.println("Connection to ThingSpeak for SMS failed.");
    }
  }
  
  if (loopCount > 40) {                                                     // @500ms / loop, this is true every 20 seconds
    loopCount = 0;
    if (sensorTurn) {
      sensorTurn = 0;
      ThingSpeak.writeField(107922, 1, sensorValue, "M9EMFPS6V1G1YN2D");    // Write sensor value to ThingSpeak
    } else {
      sensorTurn = 1;
      ThingSpeak.writeField(107922, 2, thresholdValue, "M9EMFPS6V1G1YN2D"); // Write threshold value to ThingSpeak
    }
  }

}
