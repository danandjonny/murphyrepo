

#include <AFMotor.h>

#define   DEBUG           1
#define   STEER           255
#define   FAST            255
#define   MEDIUM          191
#define   SLOW            128
#define   STOP_THRESHOLD  20  // expressed in centimeters.
#define   RAMP_UP         1 // typically 1
#define   RAMP_DOWN       1 // typically 1
#define   AVOID_DELAY     2000

/*
 * MOTOR12_64KHZ
 * MOTOR12_8KHZ
 * MOTOR12_2KHZ
 * MOTOR12_1KHZ
*/

AF_DCMotor motor(1, MOTOR12_1KHZ);
AF_DCMotor steer(2);

int currentSpeed;

void setup() {
  if (DEBUG) {
    Serial.begin(9600);           // set up Serial library at 9600 bps
    Serial.println("Motor test!");
  }
  Debug("setup");

  // steer
  steer.setSpeed(STEER);
  steer.run(RELEASE);

  motor.setSpeed(FAST);
  motor.run(RELEASE);

}

void loop() {
//  Debug("loop");
//  Test();

  int targetSpeed = FAST;

  if (MeasureDistance() <= STOP_THRESHOLD) {
    DecelerateForward(currentSpeed);
    Avoid(targetSpeed);
  }
  if (currentSpeed < targetSpeed) {
    AccelerateForward(targetSpeed);  
  }

}

void steerLeft() {
  Debug("steerLeft");
  steer.run(FORWARD);
  steer.setSpeed(STEER);
}

void steerRight() {
  Debug("steerRight");
  steer.run(BACKWARD);
  steer.setSpeed(STEER);
}

void steerStraight(){
  Debug("steerStraight");
  steer.run(RELEASE);
}

void AccelerateForward(int mySpeed) {
  Debug("AccelerateForward");
  if (mySpeed > 255) mySpeed = 255;
  if (mySpeed < 0) mySpeed = 0;
  motor.run(FORWARD);  
  for (int i=0; i<mySpeed; i+=RAMP_UP) {
    motor.setSpeed(i);  
    currentSpeed = i;
    delay(10);
  }
  currentSpeed = mySpeed;
}

void DecelerateForward(int mySpeed) {
  if (mySpeed > 255) mySpeed = 255;
  if (mySpeed < 0) mySpeed = 0;
  Debug("DecelerateForward");
  for (int i=mySpeed; i>0; i-=RAMP_DOWN) {
    motor.setSpeed(i);
    currentSpeed = i;  
    delay(10);
  }
  currentSpeed = 0;
}

void AccelerateBackwards(int mySpeed) {
  if (mySpeed > 255) mySpeed = 255;
  if (mySpeed < 0) mySpeed = 0;
  Debug("AccelerateBackwards");
  motor.run(BACKWARD);
  for (int i=0; i<mySpeed; i+=RAMP_UP) {
    motor.setSpeed(i);
    currentSpeed = i * -1;  
    delay(10);
 }
 currentSpeed = mySpeed;
}

void DecelerateBackwards(int mySpeed) {
  if (mySpeed > 255) mySpeed = 255;
  if (mySpeed < 0) mySpeed = 0;
  Debug("DecelerateBackwards");
  for (int i=mySpeed; i>0; i-=RAMP_DOWN) {
    motor.setSpeed(i);  
    currentSpeed = i * -1;
    delay(10);
 }
 currentSpeed = 0;
}

void StopMotor() {
  Debug("StopMotor");
  motor.run(RELEASE);
  currentSpeed = 0;
}

void Avoid(int myTargetSpeed) {
  Debug("Avoid");
  StopMotor();
  steerStraight();
  AccelerateBackwards(myTargetSpeed);
  delay(250);
  DecelerateBackwards(myTargetSpeed);
  StopMotor();
  steerLeft();
  AccelerateForward(myTargetSpeed);
  delay(AVOID_DELAY);
  //DecelerateForward();
  //StopMotor();
  steerStraight();
}

int MeasureDistance() {
  Debug("MeasureDistance");
  int time = -1;
  int distance;
  int avgTime;
  int sumTime = 0;
  int goodReads = 0;
  
  for (int i = 0; i < 5; i++) {    
    pinMode(A0,OUTPUT);
    digitalWrite(A0,HIGH);
    delayMicroseconds(10);
    digitalWrite(A0,LOW);
    pinMode(A1,INPUT);
    time=pulseIn(A1,HIGH);
    if (time > -1) {
      sumTime += time;
      goodReads += 1;    
    }
  }
  avgTime = sumTime / goodReads;
  distance=avgTime/58.2;
  if (distance < 0) {
    distance = 9999;
  }
  Serial.println(distance);  
  return distance;
}

void Test() {
  Debug("Test");
  int distance;
  uint8_t i;

  distance = MeasureDistance();
  Serial.println("tick");
  steerLeft();
  delay(500);
  steerRight();
  delay(500);
  steerStraight();
  currentSpeed = MEDIUM;
  AccelerateForward(currentSpeed);
  DecelerateForward(currentSpeed);
  Serial.println("tock");
  currentSpeed=MEDIUM;
  AccelerateBackwards(currentSpeed);
  DecelerateBackwards(currentSpeed);
  StopMotor();
  Serial.println("tech");
  delay(1000);
}

void Debug(char message[]) {
  if (DEBUG) {
    Serial.println(message);
  }
}

