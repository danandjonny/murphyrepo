//Libraries:
#include "TEA5767.h"
#include <Wire.h>

//Constants:
TEA5767 Radio; //Pinout SLC and SDA - Arduino uno pins A5 and A4


unsigned char frequencyH = 0;
unsigned char frequencyL = 0;

unsigned int frequencyB;
double frequency = 0;
double oldFrequency = 0;

void setup()
{
  Wire.begin();
  Serial.begin(9600);
  Serial.println("Starting");
  Radio.init();
  oldFrequency = 100.7;
//Radio.set_frequency(100.7); //On power on go to station 95.2
}

void loop()
{
  int reading = analogRead(0);
  Serial.print(reading);Serial.print("   ");
  //frequency = map((float)reading, 0.0, 1024.0, 87.5, 108.0);
 
  //frequency = ((double)reading * (108.0 - 87.5)) / 1024.0 + 87.5;
  frequency = ((double)reading * (108.0 - 87.5)) / 1013.0 + 87.6;
  frequency = ((int)(frequency * 10)) / 10.0;
  Serial.println(frequency);

  if (frequency != oldFrequency) {
    oldFrequency = frequency;
    Serial.println("Set Frequency");
    Radio.set_frequency(frequency);
  }
}


