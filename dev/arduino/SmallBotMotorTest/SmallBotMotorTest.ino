/*
Robot version 1
ATtiny85, 2 motors, LED, two sensors

Sensor stuff: 
  both white  : no prior  : random walk
        : prior   : turn in direction of last black
  
  one white : ideal situation, move straight
  
  both black  : no prior  : random walk
        : prior   : turn in direction of last white

This code was written by Eric Heisler (shlonkin)
It is in the public domain, so do what you will with it.
*/
#include "Arduino.h"

// Here are some parameters that you need to adjust for your setup
// Base speeds are the motor pwm values. Adjust them for desired motor speeds.
#define baseLeftSpeed 255
#define baseRightSpeed 255

// the pin definitions
#define lmotorpin 1 // PB1 pin 6
#define rmotorpin 0 // PB0 pin 5
#define lsensepin 3 //ADC3 pin 2
#define rsensepin 1 //ADC1 pin 7
#define ledpin 4 //PB4 pin 3

// variables
uint8_t lspd, rspd; // motor speeds
uint16_t lsenseval, rsenseval, lwhiteval, rwhiteval; // sensor values

void move(uint8_t lspeed, uint8_t rspeed);
void stop();
void senseInit();
void flashLED(uint8_t flashes);

// just for convenience and simplicity (HIGH is off)
#define ledoff PORTB |= 0b00010000
#define ledon PORTB &= 0b11101111

void setup(){
  // setup pins
  pinMode(lmotorpin, OUTPUT);
  pinMode(rmotorpin, OUTPUT);
  pinMode(2, INPUT); // these are the sensor pins, but the analog
  pinMode(3, INPUT); // and digital functions use different numbers
  ledoff;
  pinMode(ledpin, OUTPUT);
  analogWrite(lmotorpin, 0);
  analogWrite(rmotorpin, 0);
  
  lspd = baseLeftSpeed;
  rspd = baseRightSpeed;
  
  flashLED(4);
  delay(500);
  senseInit();
}

void loop(){
  runTest();
}

void runTest() {
  flashLED(3);
  delay(500);
  move(255,255);
  delay(3000);
  stop();
  delay(1000);
  move(0,255);
  delay(3000);
  stop();
  delay(1000);
  move(255,0);
  delay(3000);
  stop();

  delay(5000);
}
   
void move(uint8_t lspeed, uint8_t rspeed){
  analogWrite(lmotorpin, lspeed);
  analogWrite(rmotorpin, rspeed);
}

void stop(){
  analogWrite(lmotorpin, 0);
  analogWrite(rmotorpin, 0);
}

// stores the average of 16 readings as a white value
void senseInit(){
  lwhiteval = 0;
  rwhiteval = 0;
  ledon;
  delay(1);
  for(uint8_t i=0; i<16; i++){
    lwhiteval += analogRead(lsensepin);
    delay(1);
    rwhiteval += analogRead(rsensepin);
    delay(9);
  }
  lwhiteval >>= 4;
  rwhiteval >>= 4;
  ledoff;
}

void flashLED(uint8_t flashes){
  while(flashes){
    flashes--;
    ledon;
    delay(200);
    ledoff;
    if(flashes){ delay(500); }
  }
}
