/*
********************************************************************
  Name    : timeDisplaySlave_03
  Author  : Daniel Murphy
  Date    : October 25, 2015
  Version : 3
  Notes   : The purpose of this program is to function as an I2C slave
          : responsible for displaying numerals and characters on a 
          : series of 4 single character seven segment displays set up
          : in a circuit with S8550 PNP transistors for multiplexed
          : switching. A shift register is used to shuttle a value to 
          : all of the SS displays, and the transistor associated with
          : the character position for which the value is intended is
          : switched on.  When the next number comes through the shift
          : register, it's transistor is turned on, and so on.  Values
          : coming in to the slave from the master have already been 
          : mapped so as to light the correct segments on the display.
          : See Clock.h for the mapping.  The setup includes 8 330ohm
          : resistors, 4 single character seven segment displays, 4
          : S8550 transistors, a 74HC595 shift register, and 4 
          : resistors (brown, brown, black, red, red) for the transistor 
          : switching.
********************************************************************

7 Segment (Common Anode) Display Map: 

    D   E  5V   F   G
 ___|___|___|___|___|____
|                        |
|        F               |
|    E       G           |
|        D               |
|    A       C           |
|        B       H(Dot)  |
|________________________|
    |   |   |   |   |
    A   B  5V   C   H

74HC595 Map:
     _______
Q1  |1 *  16|  Vcc                  PINS 1-7, 15   Q0 - Q7   Output Pins
Q2  |2    15|  Q0                   PIN 8	   GND	     Ground, Vss
Q3  |3    14|  DS                   PIN 9	   Q7"	     Serial Out
Q4  |4    13|  OE                   PIN 10	   MR	     Master Reclear, active low
Q5  |5    12|  ST_CP                PIN 11	   SH_CP     Shift register clock pin
Q6  |6    11|  SH_CP                PIN 12	   ST_CP     Storage register clock pin (latch pin)
Q7  |7    10|  MR                   PIN 13	   OE	     Output enable, active low
GND |8_____9|  Q7"                  PIN 14	   DS	     Serial data input
                                    PIN 16	   Vcc	     Positive supply voltage
           _______
   LED Q1-|1 *  16|-5V
   LED Q2-|2    15|-LED Q0
   LED Q3-|3    14|-PIN 11
   LED Q4-|4    13|-GND
   LED Q5-|5    12|-PIN 8
   LED Q6-|6    11|-PIN 12 ; 1uF TO GND
   DOT Q7-|7    10|-5V
      GND-|8_____9|-NILL

The values passed in from the master map to characters as follows:

Array Value      Character
-----------      ---------
0                0
1                1
2                2
...
9                9
10               A
11               B
...
35               Z
(decimal point)  36
(space)          37

This mapping is controlled on the master in Clock.h.

Valid States    Pos  Descr
------------    ---  -----
BLINK_DISPLAY   1    The entier display blinks when set.
DECIMAL_ON      2    The rightmost decimal point is on when set.
DECIMAL_BLINK   3    The rightmost decimal point is blinking when set.
MILITARY_TIME   4    Shows time in military time when set, with 
                     leading 0 when appropriate.
*/
#include <Wire.h>

#define  BLINK_DISPLAY    0
#define  DECIMAL_ON       1
#define  DECIMAL_BLINK    2
#define  MILITARY_TIME    3
#define  CHAR_SPACE       0
#define  CHAR_DECIMAL     0
#define  BLINK_DURATION   500

unsigned long currentTime; // holds the time in milliseconds since the board started
unsigned long timeToSwitch;// holds the time in milliseconds when the decimal point will
                           // be turned on or off based on the presence of BLINK_DISPLAY
                           // in theTime[instructionPos].
boolean ledOn = true;

static const int latchPin       = 8;     //Pin connected to ST_CP of 74HC595
static const int clockPin       = 10;    //Pin connected to SH_CP of 74HC595
static const int dataPin        = 9;     //Pin connected to DS of 74HC595
static const int hourTensPin    = 5;    // switch to turn on tens position for hours
static const int hourOnesPin    = 4;    // switch to turn on ones position for hours
static const int minuteTensPin  = 3;    // switch to turn on tens position for minutes
static const int minuteOnesPin  = 2;    // switch to turn on ones position for minutes
static const int instructionPos = 0;
static const int hourTensPos    = 1;
static const int hourOnesPos    = 2;
static const int minuteTensPos  = 3;
static const int minuteOnesPos  = 4;
byte volatile hourTensVal = 0;
byte volatile hourOnesVal;
byte volatile minuteTensVal;
byte volatile minuteOnesVal;
static const int delayTime = 5;

static const int slaveAddress = 16;  // for some reason this slave is detected on address 32 when set to 16 here.

byte volatile theTime[4] = {0,0,0,0}; // make this 238 for 0 on the display

                                        // this is needed because we can be set to convert from military to civilian
const int ssCharMap[38] = { 238,130,220,214,178,118,126,194,254,246,
                            250,254,108,238,124,120,246,186,130,134,
                            186,44,26,234,238,248,238,250,118,104,
                            174,174,14,186,178,220,1,0  };
void setup() {     
  Serial.begin(9600);
  pinMode(latchPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  
  pinMode(hourTensPin, OUTPUT);
  pinMode(hourOnesPin, OUTPUT);
  pinMode(minuteTensPin, OUTPUT);
  pinMode(minuteOnesPin, OUTPUT);
    
  Wire.begin(slaveAddress);          // join i2c bus with address #32
  TWBR=32;                           // == 100kHz SCL frequency  
  Wire.onReceive(receiveEvent);      // register event
}

void loop() {
  hourTensVal = theTime[hourTensPos];
  hourOnesVal = theTime[hourOnesPos];
  minuteTensVal = theTime[minuteTensPos];
  minuteOnesVal = theTime[minuteOnesPos];

/*  // if we're not displaying military time subtract 12 from hours
if (!(theTime[instructionPos] & (1 << MILITARY_TIME))) {
    int hours = (hourTensVal * 10) + hourOnesVal;
    if (hours > 12) {
      hourTensVal = ssCharMap[hours / 10];
      if (hourTensVal < 1) {
        hourTensVal = CHAR_SPACE;
      }
      hourOnesVal = ssCharMap[hours - 12];
    }
  }
*/

  currentTime = millis();
  if (currentTime > timeToSwitch) {
    timeToSwitch = currentTime + BLINK_DURATION; 
    ledOn = !ledOn;
    if (!ledOn) {
      if (theTime[instructionPos] & (1 << BLINK_DISPLAY)) {
        // make all of the digits blank
        hourTensVal = CHAR_SPACE;
        hourOnesVal = CHAR_SPACE;
        minuteTensVal = CHAR_SPACE;
        minuteOnesVal = CHAR_SPACE;
      } else if (theTime[instructionPos] & (1 << DECIMAL_BLINK)) {
        minuteOnesVal = theTime[minuteOnesPos] | (1 << CHAR_DECIMAL);
      }
    } else if (theTime[instructionPos] & (1 << DECIMAL_BLINK)) {
        minuteOnesVal = theTime[minuteOnesPos] & ~(1 << CHAR_DECIMAL);
    }
  }
  
  if (!((1 << DECIMAL_BLINK) & theTime[instructionPos]) &&  // if we're not blinking the decimal and
     (theTime[instructionPos] & (1 << DECIMAL_ON))) {       // if the decimal point is on
    minuteOnesVal = theTime[minuteOnesPos] | (1 << CHAR_DECIMAL);  // add one to the number to send to the SS
  }

  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, hourTensVal);  
  digitalWrite(latchPin, HIGH);   // take the latch pin high so the LEDs will light up:
  digitalWrite(hourTensPin, LOW); // turn on the switch
  delay(delayTime);               // wait
  digitalWrite(hourTensPin, HIGH);// turn off the switch
  
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, hourOnesVal);  
  digitalWrite(latchPin, HIGH); //take the latch pin high so the LEDs will light up:
  digitalWrite(hourOnesPin, LOW);
  delay(delayTime);
  digitalWrite(hourOnesPin, HIGH);
  
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, minuteTensVal);  
  digitalWrite(latchPin, HIGH);   // take the latch pin high so the LEDs will light up:
  digitalWrite(minuteTensPin, LOW); // turn on the switch
  delay(delayTime);               // wait
  digitalWrite(minuteTensPin, HIGH);// turn off the switch
  
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, minuteOnesVal);  
  digitalWrite(latchPin, HIGH);   // take the latch                                                                   pin high so the LEDs will light up:
  digitalWrite(minuteOnesPin, LOW); // turn on the switch
  delay(delayTime);               // wait
  digitalWrite(minuteOnesPin, HIGH);// turn off the switch

}

// This function executes whenever data is received from The 
// master.  The function is registered as an event (see setup()).
void receiveEvent(int howMany) {
  int i = 0;
  while (1 <= Wire.available()) { // loop through all
    theTime[i] = Wire.read();
    Serial.println(theTime[i]);
    i++;
  }
}

