/*
 * Watchdog Sleep Example 
 * Demonstrate the Watchdog and Sleep Functions
 * LED on digital pin 0
 * 
 * KHM 2008 / Lab3/  Martin Nawrath nawrath@khm.de
 * Kunsthochschule fuer Medien Koeln
 * Academy of Media Arts Cologne
 *
 * Modified on 5 Feb 2011 by InsideGadgets (www.insidegadgets.com)
 * to suit the ATtiny85 and removed the cbi( MCUCR,SE ) section 
 * in setup() to match the Atmel datasheet recommendations
 */

#include <avr/sleep.h>
#include <avr/wdt.h>
 
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

int pinLed = PB3;
volatile boolean f_wdt = 1;
volatile int sleepCount = 0;
volatile int w;

void setup(){
  pinMode(pinLed,OUTPUT);
  setup_watchdog(9);                    // approximately 8 seconds sleep
  w = rand() % 44;

  // beep once when powered up
  PORTB |= 1 << PORTB3;                 // turn on PB3 (buzzer)
  delayus( 100 );                       // wait for PITCH_OF_BEEP * 100 microseconds
  PORTB &= ~( 1 << PORTB3 );            // turn off PB3 (buzzer)

}

void loop(){
  int lengthOfBeep;
  int pitchOfBeep;
  
  if (f_wdt==1) {                       // wait for timed out watchdog / flag is set when a watchdog timeout occurs
    f_wdt=0;                            // reset flag

    pinMode(pinLed,INPUT);              // set all used port to intput to save power
    system_sleep();
    pinMode(pinLed,OUTPUT);             // set all ports into state before sleep
    delayus(100);                       // wait a bit

    if (sleepCount > w) {
      sleepCount = 0;
      lengthOfBeep = rand() % 400;      // length of beep a random # microsec > 0 < 401
      pitchOfBeep = rand() % 10;
      for (int i = 0; i < lengthOfBeep; i++) {
        PORTB &= ~( 1 << PORTB3 );      // turn off PB3 (buzzer)
        delayus( pitchOfBeep );         // wait for PITCH_OF_BEEP * 100 microseconds
        PORTB |= 1 << PORTB3;           // turn on PB3 (buzzer)
        delayus( pitchOfBeep );         // wait for PITCH_OF_BEEP * 100 microseconds
      }
      PORTB &= ~( 1 << PORTB3 );        // clear the bit to turn off the beep
      w = rand() % 44;                  // set w to a random number of seconds up to 3 minutes
    }
  }
}

void delayus( uint16_t z ) {

  while ( z ) {
    _delay_us( 100 );                   // delay 100 microseconds of a second
    z--;
  }
}

// set system into the sleep state 
// system wakes up when wtchdog is timed out
void system_sleep() {
  cbi(ADCSRA,ADEN);                     // switch Analog to Digitalconverter OFF

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // sleep mode is set here
//  sleep_enable();

  sleep_mode();                         // System sleeps here

//  sleep_disable();                    // System continues execution here when watchdog timed out 
  sbi(ADCSRA,ADEN);                     // switch Analog to Digital converter ON
}

// 0=16ms, 1=32ms,2=64ms,3=128ms,4=250ms,5=500ms
// 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void setup_watchdog(int ii) {

  byte bb;
  int ww;
  if (ii > 9 ) ii=9;
  bb=ii & 7; // b00000111; b00001001; b00000001
  if (ii > 7) bb|= (1<<5);
  bb|= (1<<WDCE);
  ww=bb;

  MCUSR &= ~(1<<WDRF);
  // start timed sequence
  WDTCR |= (1<<WDCE) | (1<<WDE);
  // set new watchdog timeout value
  WDTCR = bb;
  WDTCR |= _BV(WDIE);
}
  
// Watchdog Interrupt Service / is executed when watchdog times out
ISR(WDT_vect) {
 WDTCR|=B01000000;
 sleepCount++;
 f_wdt=1;  // set global flag
}
