#define ESP8266_LED 5


void setup() 
{
  pinMode(ESP8266_LED, OUTPUT);
  pinMode(D0, OUTPUT);
}

void loop() 
{
  digitalWrite(ESP8266_LED, HIGH);
  digitalWrite(D0,HIGH);
  delay(500);
  digitalWrite(D0,LOW);
  digitalWrite(ESP8266_LED, LOW);
  delay(500);
}
