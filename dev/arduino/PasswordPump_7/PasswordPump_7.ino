/*
  PasswordPump_7.ino

  Project Name: PasswordPump | CredsPump | PasswordKeeper
  Version:      1.0
  Date:         2018/09/22 - 2018/11/26
  Device:       Arduino Pro Micro w/ ATmega32u4
  Language:     C
  Memory:       32kB flash, 2.5kB SRAM, 1kB EEPROM
  EEprom:       internal=1kB, 25LC256 external=32kB 
  Clock Speed:  16MHz
  Voltage:      5v
  Author:       Daniel J. Murphy
  Components:   RGB LED, 16x2 Liquid Crystal Display or 132x64 LED display,
                one momentaty push button, one rotary encoder, 2 4.7kohm 
                resistors for I2C, 3 220ohm resistors for the RGB, 2 25LC256
                external EEprom chips
  Purpose
  =======
  - To manage usernames and passwords and to type them in via keyboard/USB.

  Features
  ========
  - Authenticate with master password
  - Search for accounts
    - Send account name, username and password as if typed in keyboard
    - Add account name, username, password (generated or not)
    - Edit existing account name, username, password
    - Delete account
  - Store up to 169 sets of credentials
  - Backup all accounts to a text file
  x Backup all accounts to another external EEprom
  - Logout / de-authenticate via menu
  - Factory reset via menu (when authenticated)
  - Factory reset after 10 failed login attempts
  x Decoy password feature
  - Configurable password display on or off
  - Works w/ 16x2 LCD or 132x64 LED displays (precompiler directive)
  - Works w/ EEprom internal to AtMega32u4 (1kB) or 25LC256 (32kB). (precompiler
    directive).
  - all passwords (except master password) are encrypted; master password is 
    hashed.
  
  Known Defects
  =============  
    - = outstanding
    ? = fixed but needs testing
    x = fixed
  ? we are only encrypting the first 16 characters of account name, username and
    password.  The sha256 blocksize is 16.
  - Delete screws up the account count when it leaves a hole.  e.g. add AAA, 
    BBB, CCC; delete BBB, you'll only be able to "Find" AAA.
  - in the switch statement for EVENT_SINGLE_CLICK the case statements 
    are not in order. When they are in order it doesn't evaluate 
    correctly.
  x When using extended memory crash at line 1367 where the LCD output is 
    trashed. Suspect some conflict w/ the 25LC256 EEprom chip.  Hangs subsequent
    to line 1367. Search for "trashed".
  x Backup all isn't consistently printing carriage returns.
  - can't seem to send a <tab> character via the Keyboard.  Tried KEY_TAB, 
    TAB_KEY, 0x2b, 0xB3, '  '.
  - we are authenticated after blowing all the creds away after 10 failures
  - single character usernames and passwords are not working well
  ? single click after Reset brings you to alpha edit mode
  x passwords are not generating correctly
  x the LCD's backlight is not turning on, needed to operate in the dark
  x after add account the account isn't showing in find account
  x after hitting reset show password = 0
  x turning the rotary encoder fast doesn't scroll thru the menu fast [sending 
    output to the serial monitor corrects this] 
  x can't add a new account; can't scroll edit menu on first account add
  x long click seems too long
  x first account added doesn't immediatly show in find account after it's 
    added.
  x after reset, stuck in reset.
  x in LCD mode too many chars in "Show Passwrd OFF", freezes device.
  x Crash when scrolling up the menu after selecting 'add account'.
  x failures are not showing correctly because you need to blank out the line 
    first
  x after adding an account we can't find it
  
  TODO / Enhancements
  ===================
    - = unimplemented
    ? - tried to implement, ran into problems
    % - concerned there isn't enough memory left to implement
    x = implemented but not tested  
    * - implemented and tested
  x store the master password in internal EEprom and everything else in external
    EEprom.
  x encrypt the usernames
  x encrpt the account names
  % move the master password to EEprom
  - reconsider the organization of the menus
  - work on the workflow; which menu items are defaulted after which events.
  - learn how to set the lock bits
  - ground unused pins
  - remove the space character and the escape (/ or \) from enterable 
    characters?
  - implement a doubly linked list to keep the accounts in order
  * implement backup from external EEprom to external EEprom
  ? incorporate the use of an i2c EEprom chip to expand available memory
  ? add a feature whereby the unit factory resets after two triple clicks, even
    if not yet authenticated. (commented out, caused problems)
  ? add a feature whereby the unit logsout after two double clicks. (commented
    out, caused problems)
  % add a decoy password that executes a factory reset when the password plus
    the characters "FR" are supplied as the master password.
  % add the ability to pump a single tab or a single carrige return from the 
    menu.
  % make it possible to import creds via XML file
  x Delete account (account name, username, password)
  x substitute memcpy with for loops
  * make it possible to edit creds via keyboard
  * Encrypt all passwords (except the master password)
  * finish the video
    https://screencast-o-matic.com/
  * decide if the encrypted pw will be saved to EEprom
  * get ScreenCastOMatic to record the video for the project 
  * add function(s) to send error output to the display device (e.g. no external 
    EEprom.
  * Make it possible to send the account name, where a URL can be entered.
  * eliminate the need for the acctsArray to conserve memory.
  * make room for a next pointer and a previous pointer to maintain a doubly 
    linked list so that we can sort the accountnames.
  * decide if you should display the passwords.  Possibly make it configurable.
  * mask passwords on input
  * add a feature that dumps all account names, usernames and passwords out
    through the keyboard (like to be inserted into an editor as a backup).
  * have the unit factory reset after 10 failed attempts.  Store the failed
    attempts in EEprom.
  * Hash the master password
  * A master key generated from the user password is hashed using SHA-256, which 
    is subsequently used to encrypt the password database with AES-128. 
    Cryptography experts no longer recommend CBC for use in newer designs. It 
    was an important mode in the past but newer designs should be using 
    authenticated encryption with associated data (AEAD) instead.   
  * have the unit automatically logout after a period of inactivity, this will
    require use of the timer.

  Components
  ==========
  - I2C LCD display 16x2 characters, user interface or
  - I2C LED display 32x128 pixels.
  - RGB LED
  - Rotary Encoder
  - internal AtMega32u4 EEprom or
  - 2 25LC256 EEprom, one primary one backup
  - 3 220ohm resistors
  - 2 4.7kohm resistors (to hold i2c SDA and SCL lines high) / optional
  
  Warnings
  ========
  - Program memory is nearly full so be careful and watch out for flaky 
    behavior.  Once over 80% of global space you're in trouble. The LED library 
    is large.

  Suggestions
  ===========
  - Best viewed in an editor w/ 160 columns, most comments are at column 80
  - Please submit defects you find so I can improve the quality of the program
    and learn more about embedded programming.

  Copyright
  =========
  - Copyright �2018, �2019 Daniel Murphy <dan-murphy@comcast.net>
  
  Contributors
  ============
  smching: https://gist.github.com/smching/05261f11da11e0a5dc834f944afd5961 
  for EEPROMUtil.cpp.
  Source code has been pulled from all over the internet,
  it would be impossible for me to cite all contributors.
  Special thanks to Elliott Williams for his essential book
  "Make: AVR Programming", which is highly recommended. 

  License TODO: make this more restrictive.
  =======
  Daniel J. Murphy hereby disclaims all copyright interest in this
  program written by Daniel J. Murphy.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Libraries 
  =========
  - https://rweather.github.io/arduinolibs/index.html - AES and SHA library
  - https://github.com/LennartHennigs/Button2 - Used for the button on the 
    rotary encoder
  - https://github.com/brianlow/Rotary - Used for the rotary encoder
  - https://github.com/arduino-libraries/Keyboard - Used to send characters to 
    the keyboard as if typed by the user
  - https://www.arduinolibraries.info/libraries/hd44780 - For the LCD display
  - https://www.arduino.cc/en/Reference/EEPROM - Used for internal EEprom

  Hardware
  ========
  - 1 SparkFun Pro Micro (w/ ATMega32u4 microcontroller)
  - Data Sheet: https://www.pjrc.com/teensy/atmega32u4.pdf
    Number Name                 Connect To / Notes
    1       TX    D1    PD3     
    2       RX    D0    PD2     
    3       GND                 
    4       GND                 
    5       SDA   D2    PD1     SSD1306 SDA, 4.7k pullup
    6       SCL   D3    PD0     SSD1306 SCL, 4.7k pullup
    7       A6    D4    PD4     
    8             D5    PC6     
    9       A7    D6    PD7     pin 1 backup chip select for 25LC256
    10            D7    PE6     rotary button
    11      A8    D8    PB4     rotary pin 2
    12      A9    D9    PB5     rotary pin 1
    13      A10   D10   PB6     pin 1 primary chip select for 25LC256
    14      MOSI  D16   PB2     pin 5 primary 25LC256, backup 25LC256
    15      MISO  D14   PB3     pin 2 primary 25LC256, backup 25LC256
    16      SCLK  D15   PB1     pin 6 primary 25LC256, backup 25LC256
    17      A0    D18   PF7     must float    Used for random # generator
    18      A1    D19   PF6     red RGB pin
    19      A2    D20   PF5     green RGB pin
    20      A3    D21   PF4     blue RGB pin
    21      Vcc (+3V)           TODO: should 25LC256 VCC connect to pin 24 
                                instead (5 volts)?, SSD1306 Vcc?
    22      Reset               reset button
    23      GND                 GND RGB, GND SSD1306, GND rotary encoder button, 
                                GND 2 25LC256 chips
    24      RAW (+5V from USB)  should we be pulling power for the components 
                                from here?

  - 2 25LC256 (External EEprom)
    Tested Part: MICROCHIP - 25LC256-I/P - 256K SPI� Bus Serial EEPROM DIP8
  - Data Sheet: http://ww1.microchip.com/downloads/en/DeviceDoc/20005715A.pdf

    Number Name                 ConnectTo        Note
    1       CS    D10           pin 13 promicro  Chip Select Input
    2       SO    D14           pin 15 promicro  MISO - Serial Data Output
    3       WP    VCC           pin 21 promicro  Write Protect
    4       Vss   GND           pin 23 promicro  Ground
    5       SI    D16           pin 14 promicro  MOSI - Serial Data Input
    6       SCK   D15           pin 16 promicro  SCLK - Serial Clock Input
    7       HOLD  VCC           pin 21 promicro  Hold Input
    8       Vcc   VCC           pin 21 promicro  Supply Voltage (currently at 
                                                 3.3v, should this be 5v?)

  RGB Colors and Meanings
  =======================
  Green         Logged in
  Orange        Backing up EEprom memory, initializing EEprom
  Purple        Sending creds
  Red           Error backing up or initializing EEprom
  Yellow        Error backing up or initializing EEprom
  Blue          Not logged in including failed login attempt(s)
  
  Budgeting Memory
  ================
  - Current Setup 
  ---------------
  Sketch uses 27468 bytes (95%) of program storage space. Maximum is 28672 
  bytes. Global variables use 1538 bytes (60%) of dynamic memory, leaving 1022 
  bytes for local variables. Maximum is 2560 bytes.
  Remember also that without the bootloader the Arduino IDE estimate might be 
  high.

  - Add LED only                +17% program storage, +22% dynamic memory (OFF)
  - Add External EEPROM only     +1% program storage,  +2% dynamic memory (ON)
  - Add Encryption (just libs)   +2% program storage,  +6% dynamic memory (ON)
  - No LED Library LED Only      -5% program storage,  -4% dynamic memory (ON)
  - DEBUG                        +6% program storage                      (OFF)

  Recent Changes
  ==============
  2018/12/04 Removed //#include <SPI.h>                                         

  The Program 
  ===========
  - Includes                                                                    */
#define F_CPU                     16000000UL

#include "Button2.h";                                                           // for the button on the rotary encoder
#include <Rotary.h>                                                             // for the rotary encoder
#include <avr/eeprom.h>                                                         // for reading and writing EEprom
#include <Keyboard.h>                                                           // for simulating a USB keyboard and sending output to it

//- Switches
//#define DEBUG                                                                 // determines if debugging is sent out via serial monitor.  Device won't automatically run program 
                                                                                // when it's plugged in to USB unless DEBUG is NOT defined.
                                                                                // define one and only one of the following: LED_DISPLAY, LCD_DISPLAY, INLINE_LED_CODE
//#define LED_DISPLAY                                                           // defined when you want to use the LED display instaed of the LCD display
#define INLINE_LED_CODE                                                         // uses sort of crappy font but it works okay.
//#define LCD_DISPLAY                                                           // define when you want to use the 16x2 LCD display instead of the LED display
#define ENCRYPTION                                                              // if we're encrypting the master password, account name, username and passwords then define this
#define EXTERNAL_EEPROM                                                         // defined when you're using the 24LC512 external EEprom chip
//#define I2C_SCAN                                                              // define only when you want to scan the i2c bus for addresses in use

//- Macros
//#define ClearBit(x,y) x &= ~y
//#define SetBit(x,y) x |= y
//#define ClearBitNo(x,y) x &= ~_BV(y)
//#define SetState(x) SetBit(machineState, x)
  
#ifdef I2C_SCAN
  #include <Wire.h>                                                             // for all i2c communication
#endif
#ifdef ENCRYPTION
  #include <Crypto.h>
  #include <SHA256.h>
  #include <AES.h>
  #include <string.h>
#endif

//#ifdef EXTERNAL_EEPROM
//  #include <avr/io.h>
//#else
//  #include <EEPROM.h>                                                           // for reading and writing AtMega32u4 internal EEprom
//#endif

#include <avr/io.h>
#include <EEPROM.h>                                                           // for reading and writing AtMega32u4 internal EEprom

#ifdef LED_DISPLAY
  #include <Adafruit_SSD1306.h>
#endif

#ifdef INLINE_LED_CODE
  #include <avr/io.h> 
  #include <avr/sfr_defs.h> 
  #include <util/delay.h> 
  #include <avr/interrupt.h>
  #include <util/twi.h>
  #include <stdio.h>
  #include <stdint.h>
  #include <avr/pgmspace.h>
  #include "font8x16.h"
#endif
#ifdef LCD_DISPLAY
  #include <hd44780.h>                                                          // main hd44780 header
  #include <hd44780ioClass/hd44780_I2Cexp.h>                                    // i2c expander i/o class header
#endif

//- Defines

#ifdef INLINE_LED_CODE
  #define SCL_PORT                PORTD                                         // PORTD
  #define SCL_DIR                 DDRD                                          // DDRD
  #define SCL_PIN                 PIND                                          // PIND
  #define SCL_BIT                 0                                             // 0 

  #define SDA_PORT                PORTD                                         // PORTD
  #define SDA_DIR                 DDRD                                          // DDRD
  #define SDA_PIN                 PIND                                          // PIND
  #define SDA_BIT                 1                                             // 1

  #define SCL_low()               SCL_PORT  &= ~_BV(SCL_BIT)   
  #define SCL_high()              SCL_PORT  |=  _BV(SCL_BIT)   
  #define SDA_low()               SDA_PORT  &= ~_BV(SDA_BIT)   
  #define SDA_high()              SDA_PORT  |=  _BV(SDA_BIT)   
  
  #define ssd1306_clear()         ssd1306_fill(0);
  #define ssd1306_char(c)         ssd1306_char_font6x8(c)
  #define ssd1306_string(s)       ssd1306_string_font6x8(s)
#endif

#define BAUD_RATE                 38400                                         // Baud rate for the Serial monitor, best for 16MHz

#define ERR_NO_EXT_EEPROM         1
#define ERR_READ_ACCOUNT          2
#define ERR_READ_MASTER           3
#define ERR_EEPROM_WRITE          4
#define ERR_READ_ACCOUNT_2        5
#define ERR_READ_USER             6
#define ERR_READ_STYLE            7
#define ERR_READ_PASS             8

#define ROTARY_PIN1               9                                             // for AtMega32u4 / Arduino Pro Mini
#define ROTARY_PIN2               8                                             //   "                               
#define BUTTON_PIN                7                                             //   "                              
//#define LED_PIN                 16                                           
#define RED_PIN                   19                                            // Pin locations for the RGB LED, must be PWM capable
#define GREEN_PIN                 20                                           
#define BLUE_PIN                  21                                           
                                                                               
#define ADC_READ_PIN              18                                            // we read the voltage from this floating pin to seed the random number generator, don't ground it!
                                                                               
#define LCD_SCREEN_WIDTH          16                                            // characters across
#define LCD_SCREEN_HEIGHT         2                                             // characters down
#define LED_LINE_HEIGHT_PIX       10                                            // the height of a character on the LED display in pixels.
#define NO_LED_LIB_LIN_HEIGHT_PIX 4
#define LED_CHAR_WIDTH_PIX        6                                             // the width of a character on the LED display in pixels.
#define NO_LED_LIB_CHAR_WIDTH_PIX 8

#define SHOW_SPLASHSCREEN         2000                                          // in microseconds
#define SHOW_ADAFRUIT             500                                           // "

#define INITIAL_MEMORY_STATE_CHAR -1                                            // 11111111 binary twos complement, -1 decimal, 0xFF hex.  When factory fresh all bytes in EEprom memory = 0xFF.
#define INITIAL_MEMORY_STATE_BYTE 0xFF                                          // 11111111 binary twos complement, -1 decimal, 0xFF hex.  When factory fresh all bytes in EEprom memory = 0xFF.
#define NULL_TERM                 0x00                                          // The null terminator, NUL, ASCII 0, or '\0'                 

#define EVENT_NONE                0                                             // used to switch of the previous event to avoid infinate looping
#define EVENT_SINGLE_CLICK        1                                             // a single click on the rotary encoder
//#define EVENT_DOUBLE_CLICK      2                                             // double click on the rotary encoder
//#define EVENT_TRIPLE_CLICK      3                                             // triple click on the rotary encoder
#define EVENT_LONG_CLICK          4                                             // holding the button on the rotary encoder down for more than 1 second(?) (changed from default directly in the library)
#define EVENT_PRESSED             5                                             // when the rotary encoder button clicks down
#define EVENT_RELEASED            6                                             // when the rotary encoder button is released
#define EVENT_ROTATE_CW           7                                             // turning the rotary encoder clockwise
#define EVENT_ROTATE_CC           8                                             // turning the rotary encoder counter clockwise.
#define EVENT_SHOW_MAIN_MENU      9                                             // to show the main menu
//#define EVENT_SHOW_ACCT_MENU    10                                            // 
#define EVENT_SHOW_EDIT_MENU      11                                            // to show the menu used for editing account name, username and password (creds)
#define EVENT_RESET               12                                            // Factory Reset event
#define EVENT_LOGOUT              13                                            // logging out of the device
#define EVENT_BACKUP              14                                            // copying the content of the primary external EEprom to the backup EEprom

#ifdef EXTERNAL_EEPROM                                                          // SPI and I2C serial mode defines

  #define SPI_SS_PRIMARY          PB6                                           // chip select primary (copy source)
  #define SPI_SS_PRIMARY_PORT     PORTB
  #define SPI_SS_PRIMARY_PIN      PINB
  #define SPI_SS_PRIMARY_DDR      DDRB
  #define SPI_SS_SECONDARY        PD7                                           // chip select seconday (copy target)
  #define SPI_SS_SECONDARY_PORT   PORTD                                         // 
  #define SPI_SS_SECONDARY_PIN    PIND                                          // 
  #define SPI_SS_SECONDARY_DDR    DDRD                                          // 
  #define SPI_MOSI                PB2                                           // mosi
  #define SPI_MOSI_PORT           PORTB
  #define SPI_MOSI_PIN            PINB
  #define SPI_MOSI_DDR            DDRB
  #define SPI_MISO                PB3                                           // miso
  #define SPI_MISO_PORT           PORTB
  #define SPI_MISO_PIN            PINB
  #define SPI_MISO_DDR            DDRB
  #define SPI_SCK                 PB1                                           // clock
  #define SPI_SCK_PORT            PORTB
  #define SPI_SCK_PIN             PINB
  #define SPI_SCK_DDR             DDRB

  #define SLAVE_PRIMARY_SELECT    SPI_SS_PRIMARY_PORT &= ~(1<<SPI_SS_PRIMARY);
  #define SLAVE_PRIMARY_DESELECT  SPI_SS_PRIMARY_PORT |= (1<<SPI_SS_PRIMARY)
  #define SLAVE_SECONDARY_SELECT  SPI_SS_SECONDARY_PORT &= ~(1<<SPI_SS_SECONDARY);
  #define SLAVE_SECONDARY_DESELECT SPI_SS_SECONDARY_PORT |= (1<<SPI_SS_SECONDARY)

// Instruction Set -- from data sheet
  #define EEPROM_READ             0b00000011                                    // read memory
  #define EEPROM_WRITE            0b00000010                                    // write to memory

  #define EEPROM_WRDI             0b00000100                                    // write disable
  #define EEPROM_WREN             0b00000110                                    // write enable

  #define EEPROM_RDSR             0b00000101                                    // read status register
  #define EEPROM_WRSR             0b00000001                                    // write status register

                                                                                // EEPROM Status Register Bits -- from data sheet
                                                                                // Use these to parse status register
  #define EEPROM_WRITE_IN_PROGRESS  0
  #define EEPROM_WRITE_ENABLE_LATCH 1
  #define EEPROM_BLOCK_PROTECT_0  2
  #define EEPROM_BLOCK_PROTECT_1  3

  #define MIN_AVAIL_ADDR          0x00                                          // assuming we start at the very beginning of EEprom
  #define DATAOUT                 16                                            // MOSI
  #define DATAIN                  14                                            // MISO 
  #define SPICLOCK                15                                            // SCK
  #define SLAVESELECT             10                                            // SS
  #define WREN                    6                                             // opcodes
  #define WRDI                    4
  #define RDSR                    5
  #define WRSR                    1
  #define READ                    3
  #define WRITE                   2
#endif

//- Memory Layout
#define MEMORY_INITIALIZED_FLAG   0x01                                          // signals if memory has been initialized correctly
#ifdef EXTERNAL_EEPROM
  #define EEPROM_BYTES_PER_PAGE   0x20                                          // 32. can't exceed 255 (real page size is 64 for 25LC256)
  #define MAX_AVAIL_ADDR          0x7FFF                                        // 32,767. 25LC256 = 256kbits capacity.
  #define MIN_AVAIL_ADDR          0x00                                          // assuming we start at the very beginning of EEprom
  #define ACCOUNT_SIZE            EEPROM_BYTES_PER_PAGE                         // bytes, put on the 1/2 page boundry
  #define USERNAME_SIZE           EEPROM_BYTES_PER_PAGE                         // bytes, put on the 1/2 page boundry
  #define STYLE_SIZE              0x02                                          // bytes, we are storing the null terminator
  #define PREV_POS_SIZE           0x01                                          // bytes, datatype byte, no null terminator
  #define NEXT_POS_SIZE           0x01                                          // bytes, datatype byte, no null terminator
//#define PASSWORD_SIZE           (EEPROM_BYTES_PER_PAGE - STYLE_SIZE - PREV_POS_SIZE - NEXT_POS_SIZE)  // 28    
  #define PASSWORD_SIZE           EEPROM_BYTES_PER_PAGE    
//#define CREDS_TOT_SIZE          (ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + PREV_POS_SIZE + NEXT_POS_SIZE) // 98 or 3.0625 pages (round up on following line)
  #define CREDS_TOT_SIZE          0x80                                          // 128.  leaving an extra 30 bytes on the end so we're on the page boundry
//#define MASTER_PASSWORD_SIZE    32                                            // aes256 keysize = 32 bytes.  aes128 keysize = 16 bytes, aes256 blocksize = 16!
  #define MASTER_PASSWORD_SIZE    16                                            // aes256 keysize = 32 bytes.  aes128 keysize = 16 bytes, aes256 blocksize = 16!
  #define LOGIN_FAILURES_SIZE     1
  #define SHOW_PASSWORD_FLAG_SIZE 1
  #define KEYBOARD_FLAG_SIZE      1
  #define FIRST_RUN_FLAG_SIZE     1
  #define GET_ADDR_RESET_FLAG     MAX_AVAIL_ADDR                                // address of the reset flag; when not set to 0x01 indicates that memory hasn't been initialized; 32,768
  #define CREDS_ACCOMIDATED       255                                           // 255 is max for the 25LC256 with the configuration related values stored at the end. Can't exceed 256. TODO: calculate (MAX_AVAIL_ADDR + 1) / CREDS_TOT_SIZE
  #define GET_ADDR_ACCT(pos)      (MIN_AVAIL_ADDR + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_USER(pos)      (MIN_AVAIL_ADDR + ACCOUNT_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_PASS(pos)      (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_STYLE(pos)     (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_PREV_POS(pos)  (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_NEXT_POS(pos)  (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + PREV_POS_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_SETTINGS       (MAX_AVAIL_ADDR - ((EEPROM_BYTES_PER_PAGE * 2) - 1))// use the last page for storing the settings.  TODO: move this to internal EEprom so it is protected by lock bits. 32,704 (cannot be < 32,640)
  #define GET_ADDR_LOGIN_FAILURES (GET_ADDR_SETTINGS)                           //
  #define GET_ADDR_SHOW_PW        (GET_ADDR_SETTINGS + LOGIN_FAILURES_SIZE)     //
  #define GET_ADDR_KEYBOARD_FLAG  (GET_ADDR_SETTINGS + LOGIN_FAILURES_SIZE + SHOW_PASSWORD_FLAG_SIZE) //
#else
  #define MAX_AVAIL_ADDR          0x03FF                                        // 1,023 is the max address of the EEprom on AtMega32u4
  #define MIN_AVAIL_ADDR          0x00                                          // assuming we start at the very beginning of EEprom
  #define ACCOUNT_SIZE            16                                            // bytes
  #define USERNAME_SIZE           32                                            // bytes
  #define PASSWORD_SIZE           16                                            // bytes
  #define STYLE_SIZE              2                                             // bytes, we are storing the null terminator
  #define PREV_POS_SIZE           1                                             // bytes
  #define NEXT_POS_SIZE           1                                             // bytes
  #define CREDS_TOT_SIZE          (ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + PREV_POS_SIZE + NEXT_POS_SIZE)
  #define MASTER_PASSWORD_SIZE    16
  #define LOGIN_FAILURES_SIZE     1
  #define SHOW_PASSWORD_FLAG_SIZE 1
  #define FIRST_RUN_FLAG_SIZE     1
  #define GET_ADDR_RESET_FLAG     MAX_AVAIL_ADDR
  #define CREDS_ACCOMIDATED       13                                            // 13 is max for the AtMega32u4 with the configuration related values stored at the end. Can't exceed 256. TODO: calculate
  #define GET_ADDR_ACCT(pos)      (MIN_AVAIL_ADDR + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_USER(pos)      (MIN_AVAIL_ADDR + ACCOUNT_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_PASS(pos)      (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_STYLE(pos)     (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_PREV_POS(pos)  (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_NEXT_POS(pos)  (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + PREV_POS_SIZE + (pos * CREDS_TOT_SIZE))
  #define GET_ADDR_MASTER_PASS    (MAX_AVAIL_ADDR - SHOW_PASSWORD_FLAG_SIZE - LOGIN_FAILURES_SIZE - MASTER_PASSWORD_SIZE)
  #define GET_ADDR_LOGIN_FAILURES (MAX_AVAIL_ADDR - SHOW_PASSWORD_FLAG_SIZE - LOGIN_FAILURES_SIZE)
  #define GET_ADDR_SHOW_PW        (MAX_AVAIL_ADDR - SHOW_PASSWORD_FLAG_SIZE)
#endif

#define MAX_AVAIL_INT_ADDR         0x03FF                                       // 1,023 is the max address of the EEprom on AtMega32u4
#define MIN_AVAIL_INT_ADDR         0x00                                         // assuming we start at the very beginning of EEprom
#define GET_ADDR_MASTER_PASS       (MAX_AVAIL_ADDR - MASTER_PASSWORD_SIZE)      // store master password near the end of EEprom



//- States
#define STATE_ENTER_MASTER        0x000001                                      // 1       00000000.00000001  entering the master password
//#define STATE_AUTHENTICATED     0x000002                                      // 2       00000000.00000010  unused
#define STATE_RESET               0x000004                                      // 4       00000000.00000100  doing a factory reset
//#define STATE_SHOW_ALPHA        0x000008                                      // 8       00000000.00001000  unused
#define STATE_SHOW_MAIN_MENU      0x000010                                      // 16      00000000.00010000  showing the main menu
#define STATE_FIND_ACCOUNT        0x000020                                      // 32      00000000.00100000  searching for an account
#define STATE_EDIT_STYLE          0x000040                                      // 64      00000000.01000000  editing the style for sending username and password
#define STATE_EDIT_USERNAME       0x000080                                      // 128     00000000.10000000  entering the username
#define STATE_EDIT_PASSWORD       0x000100                                      // 256     00000001.00000000  entering the password
#define STATE_LOGOUT              0x000200                                      // 512     00000010.00000000  logged out
#define STATE_EDIT_CREDS_MENU     0x000400                                      // 1024    00000100.00000000  showing the edit menu
#define STATE_EDIT_ACCOUNT        0x000800                                      // 2048    00001000.00000000  entering the account name
#define STATE_SEND_CREDS_MENU     0x001000                                      // 4096    00010000.00000000  showing the menu that sends creds via keyboard

#ifdef INLINE_LED_CODE
  #define SSD1306_I2C_ADDR        0x78                                          // Slave address
#endif
#ifdef LED_DISPLAY
  #define LED_I2C_SLAVE_ADDR      0x3C                                          // i2c address of the LED display 
#endif
                                                                                
#define MAX_IDLE_TIME             3600000                                       // one hour; the idle time allowed before automatic logout
#define LONG_CLICK_LENGTH         500                                           // milliseconds to hold down the rotary encoder button to trigger EVENT_LONG_CLICK
#define UN_PW_DELAY               2000                                          // time in milliseconds to wait after entering username before entering password

//- Function Prototypes (some)
#ifdef EXTERNAL_EEPROM
  void initSPI(void);                                                           // Init SPI to run EEPROM with phase, polarity = 0,0
  void SPI_tradeByte(uint8_t byte);                                             // Generic.  Just loads up HW SPI register and waits
  void EEPROM_send16BitAddress(uint16_t address);                               // splits 16-bit address into 2 bytes, sends both
  uint8_t EEPROM_readStatus(void);                                              // reads the EEPROM status register
  void EEPROM_writeEnable(void);                                                // helper: sets EEPROM write enable
  uint8_t read_eeprom_byte(uint16_t address);                                   // gets a byte from a given memory location
  void write_eeprom_byte(uint16_t address, uint8_t byte);                       // writes a byte to a given memory location
  void EEPROM_clearAll(void);                                                   // sets every byte in memory to zero
  //uint16_t EEPROM_readWord(uint16_t address);                                 // gets two bytes from a given memory location
  //void EEPROM_writeWord(uint16_t address, uint16_t word);                     // gets two bytes to a given memory location
#endif
#ifdef INLINE_LED_CODE
  void i2c_init();
  void i2c_start(void);
  void i2c_stop(void);
  void i2c_writebyte(uint8_t byte);

  void ssd1306_init(void);
  void ssd1306_send_command(uint8_t command);
  void ssd1306_setpos(uint8_t x, uint8_t y);
  void ssd1306_fill4(uint8_t, uint8_t, uint8_t, uint8_t);
  void ssd1306_fill2(uint8_t p1, uint8_t p2);
  void ssd1306_fill(uint8_t p);
  void ssd1306_fillscreen(uint8_t fill);
  void ssd1306_char_font6x8(char ch);
  void ssd1306_string_font6x8(char *s);
  void ssd1306_numdec_font6x8(uint16_t num);
  void ssd1306_numdecp_font6x8(uint16_t num);
  void ssd1306_draw_bmp(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1, 
                        const uint8_t bitmap[]);
#endif

//- Menus (globals)
#define MENU_SIZE                 8                                             // selections in the menu

#define MAIN_MENU_NUMBER          0
#define MAIN_MENU_ELEMENTS        8                                             // number of selections in the main menu
                                                                            
char * mainMenu[] =      {                       "Master Password",             // menu picks appear only on the top line
                                                 "Find Account",                // after an account is found send user sendMenu menu
                                                 "Edit Account",                // sends user to enterMenu menu
                                                 "Logout",                      // locks the user out until master password is re-entered
                                                 "Show Passwrd O  ",            // determines if passwords are displayed or not
                                                 "Keyboard O  ",                // flag that determines if the input by keyboard feature is on or off
                                                 "Backup EEprom",               // duplicate the external EEprom
                                                 "Reset"            };          // factory reset; erases all creds from memory
#define ENTER_MASTER_PASSWORD     0
#define FIND_ACCOUNT              1
#define EDIT_ACCOUNT              2
#define LOGOUT                    3
#define SET_SHOW_PASSWORD         4
#define SET_KEYBOARD              5
#define BACKUP_EEPROM             6
#define FACTORY_RESET             7

#define ENTER_MASTER_POS          0
#define FIND_ACCT_POS             1
#define SET_SHOW_PW_POS           4                                             // array position of the selection that indicates if show password is on or off
#define SET_KEYBOARD_POS          5                                             // array position of the selection that indicates if keyboard input is enabled

uint8_t menuNumber = MAIN_MENU_NUMBER;                                          // holds the menu number of the currently displayed menu
uint8_t elements = MAIN_MENU_ELEMENTS;                                          // holds the number of selections in the currently displayed menu
char *currentMenu[MENU_SIZE];                                                   // holds the size of the currently displayed menu

#define SEND_MENU_NUMBER          1
#define SEND_MENU_ELEMENTS        6                                             // number of selections in the send creds menu
const char * const sendMenu[] =       {          "Send User & Pass",            // menu picks appear only on the top line
                                                 "Send Password",  
                                                 "Send Username",
                                                 "Send Account",
                                                 "Delete Account",              // TODO: test delete account
                                                 "Backup All",
                                                 ""                 };
#define SEND_USER_AND_PASSWORD    0
#define SEND_PASSWORD             1
#define SEND_USERNAME             2
#define SEND_ACCOUNT              3
#define DELETE_ACCOUNT            4
#define BACKUP_ALL                5

#define EDIT_MENU_NUMBER          2
#define EDIT_MENU_ELEMENTS        6                                             // the number of selections in the menu for editing credentials
const char * const enterMenu[] =       {         "Add Account",
                                                 "Account Name",                // menu picks appear only on the top line
                                                 "Edit Username",  
                                                 "Edit Password",
                                                 "Indicate Style",              // 0, <CR>, 1, <TAB> between username and password when both sent
                                                 "GeneratePasswrd",
                                                 ""                 };
#define ADD_ACCOUNT               0
#define EDIT_ACCT_NAME            1
#define EDIT_USERNAME             2
#define EDIT_PASSWORD             3
#define EDIT_STYLE                4
#define GENERATE_PASSWORD         5

#define ERR_ACCOUNT_POS           -1

//- Global Variables                                                            // char is signed by default. byte is unsigned.
uint8_t accountName[ACCOUNT_SIZE];
uint8_t username[USERNAME_SIZE];                                                // holds the username of the current account
uint8_t password[PASSWORD_SIZE];                                                // holds the password of the current account
uint8_t style[STYLE_SIZE];                                                      // holds the style of the current account (<TAB> or <CR> between send username and password)

#define LEN_ALL_CHARS             93
#define DEFAULT_ALPHA_EDIT_POS    12                                            // allChars is sort of unnecessary TODO: eliminate allChars
const char allChars[LEN_ALL_CHARS] = 
" 0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz.@-=~!#$%^&*()_+[]{}|;':,<>?/'"; 
                                                                                // used to edit text via rotary encoder

char line1DispBuff[21];                                                         // used to buffer output of line 1 for the led display
char line2DispBuff[21];                                                         // used to buffer output of line 2 for the led display

#ifdef LED_DISPLAY
  const char spaceFilled20[] = "                    ";
#endif
#ifdef INLINE_LED_CODE
  const char spaceFilled16[] = "                ";
#endif
#ifdef LCD_DISPLAY
  const char spaceFilled16[] = "                ";
#endif

#define TAB_KEY                   KEY_TAB                                       // TAB key is ascii 0x2b (not 0x09) or 0x2b, 0xB3; KEY_TAB from Keyboard.h, 0xB3

uint8_t masterPassword[MASTER_PASSWORD_SIZE];                                   // this is where we store the master password for the device

uint8_t loginFailures;                                                          // count of the number of consecutive login failures since the last successful password entry.

#define MAX_LOGIN_FAILURES        10                                            // "Factory Reset" after MAX_LOGIN_FAILURES attempts to login. Gurads against brute force attack.

uint8_t showPasswordsFlag;                                                      // flag indicating if we show passwords via the UI, or hide them.
uint8_t keyboardFlag;                                                           // flag indicating if we're using the keyboard to edit creds

//- Global Volatile variables.

volatile uint8_t event = EVENT_NONE;                                            // this is the only variable manipulated in the interrupt.

//- Globals associated with state

int machineState;                                                               // TODO: we might change this so that states are mutually exclusive, it is presently unnecessarily long.
int position = 0;                                                               // the position of the rotary encoder, used to navigate menus and enter text.
uint8_t enterPosition = 0;                                                      // when alpha editing w/ rotary encoder, position in the edited word
uint8_t acctPosition = 0;                                                       // the positon of the selected account.
uint8_t acctCount = 0;                                                          // the number of accounts in EEprom.
boolean authenticated = false;                                                  // indicates if the correct master password has been provided
boolean addAccountFlag = false;                                                 // flipped on when we're adding an account.  Deliberate decision not to put into machineState.
unsigned long lastActivityTime;                                                 // used to automatically logout after a period of inactivity
uint8_t iterationCount = 0;                                                     // # of times ProcessEvent() called since last evaluation of lastActivityTime
uint8_t headPosition;                                                           // the head of the doubly linked list that sorts the account names
uint8_t tailPosition;                                                           // the tail of the doubly linked list that sorts the account names

//- Object setup

#ifdef LED_DISPLAY
  #if (SSD1306_LCDHEIGHT != 32)
    #error("Height incorrect, please fix Adafruit_SSD1306.h!");
  #endif
  #define OLED_RESET 4
  Adafruit_SSD1306 led(OLED_RESET);                                             // the LED display object.
#endif
#ifdef LCD_DISPLAY
  hd44780_I2Cexp lcd;                                                           // declare lcd object: auto locate & auto config expander chip
#endif 
#ifdef ENCRYPTION
  SHA256 sha256;
  AESSmall128 aes;                                                              // 16 byte key, 32 byte block
  //AESSmall256 aes;                                                            // 32 byte key, 32 byte block; this uses 4% more program memory
#endif

Rotary rotaryEncoder = Rotary(ROTARY_PIN1, ROTARY_PIN2);                        // the rotary encoder object.
Button2 encoderButton = Button2(BUTTON_PIN);                                    // the button on the rotary encoder.

#ifdef EXTERNAL_EEPROM
  uint8_t eeprom_output_data;
  uint8_t eeprom_input_data=0;
  uint8_t clr;
  int address=0;
  char buffer[64];                                                              //data buffer
#endif

//- Main Program Control

void setup() {                                                                  // runs first when the device is powered on
  #ifdef DEBUG
    Serial.begin(BAUD_RATE);
    while(!Serial);
    delay(50);
    debug(F("Password Pump"));
  #endif
  #ifdef I2C_SCAN
    i2cScan();
  #endif
  
  pinMode(RED_PIN,   OUTPUT);                                                   // RGB LED pins
  pinMode(GREEN_PIN, OUTPUT);                                                   // "
  pinMode(BLUE_PIN,  OUTPUT);                                                   // "
  pinMode(BUTTON_PIN, INPUT_PULLUP);                                            // setup button pin for input enable internal 20k pull-up resistor, goes LOW when pressed, HIGH when released
  pinMode(ADC_READ_PIN, INPUT);                                                 // this pin will float in a high impedance/Hi-Z state and it's voltage
                                                                                // will be read with every spin to seed the random number generator.
  randomSeed(analogRead(ADC_READ_PIN));                                         // do not ground this pin; use this or randomSeed(millis()); used for password generation

  //encoderButton.setClickHandler(buttonClickHandler);                          // fires after ReleasedHandler on short click
  //encoderButton.setLongClickHandler(buttonLongClickHandler);                  // fires when button is pressed and held
  //encoderButton.setDoubleClickHandler(buttonDoubleClickHandler);              // fires when button is double clicked, do this twice in a row to logout.
  //encoderButton.setTripleClickHandler(buttonTripleClickHandler);              // fires when button is triple clicked, do this twice in a row to reset.
  encoderButton.setReleasedHandler(buttonReleasedHandler);                      // fires when button is released
  //encoderButton.setPressedHandler(buttonPressedHandler);                      // fires when button is pressed
  //encoderButton.setTapHandler(buttonTapHandler);                              // not sure when this fires, but we turn it into EVENT_SINGLE_CLICK.

  #ifdef LED_DISPLAY                                                            // if we're using the LED display
                                                                                // by default, we'll generate the high voltage from the 3.3v line internally.
    led.begin(SSD1306_SWITCHCAPVCC, LED_I2C_SLAVE_ADDR);                        // initialize with the I2C addr LED_I2C_SLAVE_ADDR (0x3C) for the 128x32.
    led.display();                                                              // Show image buffer on the display hardware. TODO: decide if we want the AdaFruit splash screen
    delay(SHOW_ADAFRUIT);                                                       // Since the buffer is intialized with an Adafruit splashscreen
                                                                                // internally, this will display the splashscreen.
    led.setTextSize(1);                                                         // 1 is pretty small, but 2 is too big.
    led.setTextColor(WHITE);
    strcpy(line1DispBuff,"PasswordPump");
    DisplayBuffer();                                                            // presents line1DispBuff and line2DispBuff to user via LED display.
  #endif
  #ifdef INLINE_LED_CODE
    i2c_init();
    ssd1306_init();
    ssd1306_clear(); 
    ssd1306_string_font8x16xy(16, 0, "PasswordPump");
  #endif
  #ifdef LCD_DISPLAY                                                            // otherwise we're using the LCD display
    int status = lcd.begin(LCD_SCREEN_WIDTH, LCD_SCREEN_HEIGHT);
    if(status)                                                                  // non zero status means it was unsuccessful
    {
      debugMetric("Fatal LCD error: ", status);
      status = -status;                                                         // convert negative status value to positive number
                                                                                // begin() failed so blink error code using the onboard LED if possible
      hd44780::fatalError(status);                                              // does not return
    }

    lcd.backlight();                                                            // Turn on the blacklight
    lcd.setCursor(0, 0);
    lcd.print(F("  PasswordPump  "));
  #endif 

  delay(SHOW_SPLASHSCREEN);

  #ifdef EXTERNAL_EEPROM
    initSPI();
  #endif

  if (getResetFlag() != MEMORY_INITIALIZED_FLAG) {                              // if memory has never been initialized, initialize it.
    loginFailures = MAX_LOGIN_FAILURES + 1;                                     // so that a condition inside of EVENT_RESET evaluates to true and the reset logic is executed. 
    event = EVENT_RESET;                                                        // this is the first time we're turning on the device, initialize memory (25LC256 comes with 0x00 in every address space)
    ProcessEvent();                                                             // the reset event will write 0xFF to the location for the reset flag
  };
  
  loginFailures = getLoginFailures();                                           // getLoginFailures returns a byte.
  if (loginFailures == INITIAL_MEMORY_STATE_BYTE ) {                            // if loginFailures has never been written too
    loginFailures = 0;                                                          // set it to zero
    writeLoginFailures();                                                       // and write it to EEprom.
  }

  keyboardFlag = getKeyboardFlag();                                             // setup the keyboard flag
  if (keyboardFlag == INITIAL_MEMORY_STATE_BYTE ) {                             // this should never be true because the reset event sets the keyboard flag to a value
    keyboardFlag = false;                                                       // but, for safety, set the keyboard flag to OFF
    writeKeyboardFlag();                                                        // and write it to EEprom.
  }
  flipOnOff(keyboardFlag,SET_KEYBOARD_POS,10);

  showPasswordsFlag = getShowPasswordsFlag();                                   // setup the show passwords flag and menu item. (getShowPasswordsFlag returns byte)
  if (showPasswordsFlag == INITIAL_MEMORY_STATE_BYTE ) {                        // this should never be true because the reset event sets the show passwords flag to a value
    showPasswordsFlag = true;                                                   // but, for safety, set the show password flag to ON
    writeShowPasswordsFlag();                                                   // and write it to EEprom.
  }
  flipOnOff(showPasswordsFlag,SET_SHOW_PW_POS,14);                              // set the menu item to Show Passwrd ON or Show Passwrd OFF.

  countAccounts();                                                              // count the number of populated accounts in EEprom
  
  PCICR |= (1 << PCIE0);                                                        // Setup interrupts for rotary encoder
  PCMSK0 |= (1 << PCINT4) | (1 << PCINT5);                                      //

  lastActivityTime = millis();                                                  // establish the start time for when the device is powered up
  authenticated = false;                                                        // we're not authenticated yet!
  headPosition = findHeadPosition();                                            // find the head of the doubly linked list that sorts by account name
  tailPosition = findTailPosition();                                            // find the tail of the doubly linked list that sorts by account name
  setBlue();                                                                    // not yet authenticated, LED is orange
  event = EVENT_SHOW_MAIN_MENU;                                                 // first job is to show the first element of the main menu

  sei();                                                                        // Turn on global interrupts
}

void loop() {
  encoderButton.loop();                                                         // polling for button press TODO: replace w/ interrupt
  ProcessEvent();  
}

void ProcessEvent()                                                             // processes events
{ 
  if (event != EVENT_NONE) {
    lastActivityTime = millis();                                                // bump up the lastActivityTime, we don't reset iterationCount here, not necessary and slows responsiveness just a bit
  } else {                                                                      // event == EVENT_NONE
    if (++iterationCount == 255) {                                              // we don't want to call millis() every single time through the loop
      iterationCount = 0;                                                       // necessary?  won't we just wrap around?
      if (millis() < (lastActivityTime + MAX_IDLE_TIME)) {                      // check to see if the device has been idle for MAX_IDLE_TIME milliseconds
        return;                                                                 // not time to logout yet and event == EVENT_NONE, so just return.
      } else {
        event = EVENT_LOGOUT;                                                   // otherwise we've been idle for more than MAX_IDLE_TIME, logout.
      }
    } else {                                                                    // iterationCount is < 255
      return;                                                                   // not time to check millis() yet, just return
    }
  }
  if (event == EVENT_ROTATE_CW) {                                               // scroll forward through something depending on state...
    if (((STATE_SHOW_MAIN_MENU) == (machineState & (STATE_SHOW_MAIN_MENU ))) &&
         authenticated                                                      ) { // this prevents navigation away from 'Enter Master Password' when not authenticated.
      if (position < MAIN_MENU_ELEMENTS - 1) {
        position++;
        MenuDown(currentMenu);
      }
    } else if ((STATE_ENTER_MASTER   == (machineState & STATE_ENTER_MASTER )) ||
               (STATE_EDIT_ACCOUNT   == (machineState & STATE_EDIT_ACCOUNT )) ||
               (STATE_EDIT_STYLE     == (machineState & STATE_EDIT_STYLE   )) ||
               (STATE_EDIT_USERNAME  == (machineState & STATE_EDIT_USERNAME)) ||
               (STATE_EDIT_PASSWORD  == (machineState & STATE_EDIT_PASSWORD))) {
      if (position < LEN_ALL_CHARS) {
        position++;
      }
      char charToPrint = allChars[position];
      ShowChar(charToPrint, enterPosition);
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)){
      if (position < SEND_MENU_ELEMENTS - 1) {
        position++;
        MenuDown(currentMenu);
      }
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)){
      if ((position < EDIT_MENU_ELEMENTS - 1) && (acctCount > 0)) {
        position++;
        MenuDown(currentMenu);
        SwitchRotatePosition(position);
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {     
      if (position < (acctCount - 1)) {
        position++;
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
      } else {
        position = acctCount - 1;                                               // maximum for position in this state is acctCount
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
      }
    }
    event = EVENT_NONE;                                                         // to prevent infinite looping
    
  } else if (event == EVENT_ROTATE_CC) {                                        // scroll backward through something depending on state...
    if ((STATE_SHOW_MAIN_MENU) == (machineState & (STATE_SHOW_MAIN_MENU ))) {
      if (position > 0) {
        position--;
        MenuUp(currentMenu);
      }
    } else if ((STATE_ENTER_MASTER  == (machineState & STATE_ENTER_MASTER )) ||
               (STATE_EDIT_ACCOUNT  == (machineState & STATE_EDIT_ACCOUNT )) ||
               (STATE_EDIT_STYLE    == (machineState & STATE_EDIT_STYLE   )) ||
               (STATE_EDIT_USERNAME == (machineState & STATE_EDIT_USERNAME)) ||
               (STATE_EDIT_PASSWORD == (machineState & STATE_EDIT_PASSWORD))) {
      if (position > 0) {
        position--;
      }
      char charToPrint = allChars[position];
      ShowChar(charToPrint, enterPosition);
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)){
      if (position > 0) {
        position--;
        MenuUp(currentMenu);
      }
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)){
      if (position > 0) {
        position--;
        MenuUp(currentMenu);
        SwitchRotatePosition(position);
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {
      if (position > 0) {
        position--;
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
      } else {
        position = 0;
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
      }
    }
    event = EVENT_NONE;

  } else if (event == EVENT_SHOW_MAIN_MENU) {                                   // show the main menu
    menuNumber = MAIN_MENU_NUMBER;
    int arraySize = 0;
    for (uint8_t i = 0; i < MENU_SIZE; i++) {
      arraySize += sizeof(mainMenu[i]);  
    }
    memcpy(currentMenu, mainMenu, arraySize);
    elements = MAIN_MENU_ELEMENTS;
    machineState = STATE_SHOW_MAIN_MENU;
    if (authenticated) {
      position = FIND_ACCT_POS; 
    } else {
      position = ENTER_MASTER_POS;
    }
    ShowMenu(position, currentMenu);
    readAcctFromEEProm(acctPosition, accountName);
    if (authenticated) DisplayLine2(accountName);
    event = EVENT_NONE;

  } else if (event == EVENT_SHOW_EDIT_MENU) {                                   // show the main menu
    menuNumber = EDIT_MENU_NUMBER;
    int arraySize = 0;
    for (uint8_t i = 0; i < MENU_SIZE; i++) {
      arraySize += sizeof(enterMenu[i]);  
    }
    memcpy(currentMenu, enterMenu, arraySize);
    elements = EDIT_MENU_ELEMENTS;
    machineState = STATE_EDIT_CREDS_MENU;
    if (position < 0 || position > (EDIT_MENU_ELEMENTS - 1)) position = 0;      // for safety
    ShowMenu(position, currentMenu);
    readAcctFromEEProm(acctPosition, accountName);
    DisplayLine2(accountName);
    event = EVENT_NONE;

  } else if (event == EVENT_LONG_CLICK) {                                       // jump up / back to previous menu 
    if (STATE_ENTER_MASTER == (machineState & STATE_ENTER_MASTER)){
      ReadFromSerial(masterPassword);
      authenticated = authenticateMaster(masterPassword);                       // authenticateMaster writes to masterPassword ifdef ENCRYPTION
      if (authenticated) {
        position = FIND_ACCT_POS;
        machineState = STATE_SHOW_MAIN_MENU;
        ShowMenu(position, currentMenu);
        DisplayLine2("Authenticated");
        event = EVENT_NONE;
      } else {
        if (loginFailures > MAX_LOGIN_FAILURES) {
          event = EVENT_RESET;                                                  // factory reset after 10 failed attempts to enter master password!
        } else {  
          position = 0;
          machineState = STATE_SHOW_MAIN_MENU;
          ShowMenu(position, currentMenu);
          char buffer[4];
          itoa(loginFailures, buffer, 10);                                      // convert login failures to a string and put it in buffer.
          #ifdef LED_DISPLAY
            strcpy(line2DispBuff,buffer);
            strcat(line2DispBuff, " failure(s)");
            DisplayBuffer();
          #endif
          #ifdef INLINE_LED_CODE
            strcpy(line2DispBuff, buffer);
            strcat(line2DispBuff, " failure(s)");
            DisplayBuffer();
          #endif
          #ifdef LCD_DISPLAY
            lcd.setCursor(0,1);
            PrintBlankLine();
            lcd.setCursor(0,1);
            lcd.print(buffer);
            lcd.setCursor(5,1);
            lcd.print(F(" failure(s)"));
          #endif
          event = EVENT_NONE;
        }
      }
    } else if (STATE_EDIT_ACCOUNT == (machineState & STATE_EDIT_ACCOUNT)) {
      ReadFromSerial(accountName);
      #ifdef ENCRYPTION
        uint8_t pos = 0;
        while (accountName[pos++] != NULL_TERM);                                // make sure the password is 16 chars long, pad with NULL_TERM
        while (pos < ACCOUNT_SIZE) accountName[pos++] = NULL_TERM;              // "           "              "
        uint8_t buffer[ACCOUNT_SIZE];
        encrypt32Bytes(buffer, accountName);
        writeToEEPromByteArr(ACCOUNT_SIZE, 
                             buffer, 
                             GET_ADDR_ACCT(acctPosition));                      // write the password to EEProm
      #else
        writeToEEPromByteArr(ACCOUNT_SIZE, 
                             accountName, 
                             GET_ADDR_ACCT(acctPosition));                      // write the account to EEProm
      #endif
      addAccountFlag = false;
      position = EDIT_USERNAME;
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_USERNAME == (machineState & STATE_EDIT_USERNAME)) {
      ReadFromSerial(username);
      #ifdef ENCRYPTION
        uint8_t pos = 0;
        while (username[pos++] != NULL_TERM);                                   // make sure the password is 16 chars long, pad with NULL_TERM
        while (pos < USERNAME_SIZE) username[pos++] = NULL_TERM;                // "           "              "
        uint8_t buffer[USERNAME_SIZE];
        encrypt32Bytes(buffer, username);
        writeToEEPromByteArr(USERNAME_SIZE, 
                             buffer, 
                             GET_ADDR_USER(acctPosition));                      // write the password to EEProm
      #else
        writeToEEPromByteArr(USERNAME_SIZE, 
                             username, 
                             GET_ADDR_USER(acctPosition));                      // write the username to EEProm
      #endif
      position = EDIT_PASSWORD;
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_PASSWORD == (machineState & STATE_EDIT_PASSWORD)) {
      ReadFromSerial(password);
      #ifdef ENCRYPTION
        uint8_t pos = 0;
        while (password[pos++] != NULL_TERM);                                   // make sure the password is 16 chars long, pad with NULL_TERM
        while (pos < PASSWORD_SIZE) password[pos++] = NULL_TERM;                // "           "              "
        uint8_t buffer[PASSWORD_SIZE];
        encrypt32Bytes(buffer, password);
        writeToEEPromByteArr(PASSWORD_SIZE, 
                             buffer, 
                             GET_ADDR_PASS(acctPosition));                      // write the password to EEProm
      #else
        writeToEEPromByteArr(PASSWORD_SIZE, 
                             password, 
                             GET_ADDR_PASS(acctPosition));                      // write the password to EEProm
      #endif
      BlankLine2();                                                             // clear the password off of the display  TODO: check showPasswordFlag?
      position = EDIT_STYLE;
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_STYLE == (machineState & STATE_EDIT_STYLE)) {
      ReadFromSerial(style);
      writeToEEPromByteArr(STYLE_SIZE, style, GET_ADDR_STYLE(acctPosition));    // write the style to EEProm
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)){
      event = EVENT_SHOW_MAIN_MENU;
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)){
      BlankLine2();
      event = EVENT_SHOW_MAIN_MENU;
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)){      // long click after selecting an account
      machineState = STATE_SHOW_MAIN_MENU;
      ShowMenu(EDIT_MENU_NUMBER, currentMenu);
      DisplayLine2(accountName);                                                // this might be redundant
    } else {
      event = EVENT_SHOW_MAIN_MENU;                                             // if any other state show main menu (e.g just after EVENT_RESET)
    }

  } else if (event == EVENT_SINGLE_CLICK) {
    if (STATE_SHOW_MAIN_MENU == (machineState & STATE_SHOW_MAIN_MENU)) {
      switch(position) {
        case ENTER_MASTER_PASSWORD:                                             // Enter master password
          machineState = STATE_ENTER_MASTER;
          position = DEFAULT_ALPHA_EDIT_POS;                                    // puts the position of the rotary encoder over 'A' for quicker password  entry
          enterPosition = 0;
          char charToPrint[2];
          charToPrint[0] = allChars[enterPosition];
          charToPrint[1] = NULL_TERM;
          DisplayLine2(charToPrint);
          if (keyboardFlag) {
            Serial.begin(BAUD_RATE);
            while(!Serial);
          }
          event = EVENT_NONE;
          break;
        case FIND_ACCOUNT:                                                      // Find account
          machineState = STATE_FIND_ACCOUNT;
          position = acctPosition;
          readAcctFromEEProm(acctPosition, accountName);
          DisplayLine2(accountName);
          event = EVENT_NONE;
          break;
        case LOGOUT:                                                            // Logout  DEFECT: why is this being skipped over
          event = EVENT_LOGOUT;                 
          break;
        case SET_KEYBOARD:
          keyboardFlag = !keyboardFlag;
          flipOnOff(keyboardFlag,SET_KEYBOARD_POS,10);
          DisplayLine1(mainMenu[SET_KEYBOARD]);
          event = EVENT_NONE;
          break;
        case SET_SHOW_PASSWORD:
          showPasswordsFlag = !showPasswordsFlag;
          flipOnOff(showPasswordsFlag,SET_SHOW_PW_POS,14);                      // set the menu item to Show Passwrd ON or Show Passwrd OFF.
          DisplayLine1(mainMenu[SET_SHOW_PW_POS]);
          event = EVENT_NONE;
          break;
        case BACKUP_EEPROM:                                                     // Backup EEprom
          event = EVENT_BACKUP;
          break;
        case FACTORY_RESET:                                                     // Reset
          event = EVENT_RESET;
          break;
        case EDIT_ACCOUNT:                                                      // Show the enter account menu
          menuNumber = EDIT_MENU_NUMBER;
          elements = EDIT_MENU_ELEMENTS;
          int arraySize = 0;
          for (uint8_t i = 0; i < MENU_SIZE; i++) {
            arraySize += sizeof(enterMenu[i]);  
          }
          memcpy(currentMenu, enterMenu, arraySize);
          elements = EDIT_MENU_ELEMENTS;
          position = 0;
          machineState = STATE_EDIT_CREDS_MENU;
          ShowMenu(position, currentMenu);
          readAcctFromEEProm(acctPosition, accountName);
          DisplayLine2(accountName);
          event = EVENT_NONE;
          break;
        default:
          break;
      }
      if (event == EVENT_SINGLE_CLICK) {                                        // stop the infinite loop of single clicks
        event = EVENT_NONE;
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {     // Go to the send menu
        acctPosition = position;
        menuNumber = SEND_MENU_NUMBER;
        elements = SEND_MENU_ELEMENTS;
        int arraySize = 0;
        for (uint8_t i = 0; i < MENU_SIZE; i++) {
          arraySize += sizeof(sendMenu[i]);  
        }
        memcpy(currentMenu, sendMenu, arraySize);
        elements = SEND_MENU_ELEMENTS;
        position = 0;
        machineState = STATE_SEND_CREDS_MENU;
        ShowMenu(position, currentMenu);
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
        event = EVENT_NONE;
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      enterPosition = 0;
      char charToPrint[2];
      charToPrint[0] = allChars[enterPosition];
      charToPrint[1] = NULL_TERM;                                               // TODO: this shouldn't be necessary
      switch(position) {
         case EDIT_ACCT_NAME:                                                   // Enter account name
            machineState = STATE_EDIT_ACCOUNT; 
            position = DEFAULT_ALPHA_EDIT_POS;                                  // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            if (keyboardFlag) {
              Serial.begin(BAUD_RATE);
              while(!Serial);
            }
            break; 
         case EDIT_USERNAME:                                                    // Enter username     
            machineState = STATE_EDIT_USERNAME;
            position = DEFAULT_ALPHA_EDIT_POS;                                  // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            if (keyboardFlag) {
              Serial.begin(BAUD_RATE);
              while(!Serial);
            }
            break;
         case EDIT_PASSWORD:                                                    // Enter Password   
            machineState = STATE_EDIT_PASSWORD;
            position = DEFAULT_ALPHA_EDIT_POS;                                  // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            if (keyboardFlag) {
              Serial.begin(BAUD_RATE);
              while(!Serial);
            }
            break;
         case EDIT_STYLE:
            machineState = STATE_EDIT_STYLE;
            position = 0;                                                       // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            if (keyboardFlag) {
              Serial.begin(BAUD_RATE);
              while(!Serial);
            }
            break;
         case GENERATE_PASSWORD:                                                // Automatic UUID enter password 
            machineState = STATE_EDIT_PASSWORD;                                 // pretend we're entering the password
            setUUID(password);                                                  // put a UUID in the password char array
            BlankLine2();
            event = EVENT_LONG_CLICK;                                           // and trigger long click to write the password to eeprom.
            break;
         case ADD_ACCOUNT:                                                      // Add account
            addAccountFlag = true;
            acctPosition = getNextFreeAcctPos();                                // get the position of the next EEprom location for account name marked empty.
            if (acctPosition != ERR_ACCOUNT_POS) {
              username[0] = NULL_TERM;
              password[0] = NULL_TERM;
              strcpy(accountName,"Add Account");
              DisplayLine2(accountName);
              acctCount++;
            } else {
              DisplayLine2("No space");
            }
            position = EDIT_ACCT_NAME;
            ShowMenu(position, currentMenu);
            event = EVENT_NONE;
            break;
      }
    } else if (STATE_EDIT_ACCOUNT == (machineState & STATE_EDIT_ACCOUNT)) {
      accountName[enterPosition] = allChars[position];                          // set the last char in accountName to be the selected character from allChars
      accountName[enterPosition + 1] = NULL_TERM;                               // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    } else if (STATE_EDIT_USERNAME == (machineState & STATE_EDIT_USERNAME)) {
      username[enterPosition] = allChars[position];
      username[enterPosition + 1] = NULL_TERM;                                  // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    } else if (STATE_EDIT_PASSWORD == (machineState & STATE_EDIT_PASSWORD)) {
      password[enterPosition] = allChars[position];
      password[enterPosition + 1] = NULL_TERM;                                  // push the null terminator out ahead of the last char in the string
      if (!showPasswordsFlag) {                                                 // mask the password being entered if showPasswordsFlag is OFF
        #ifdef LED_DISPLAY
          line2DispBuff[enterPosition] = '*';
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef INLINE_LED_CODE
          line2DispBuff[enterPosition] = '*';
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef LCD_DISPLAY
          enterPosition++;
          lcd.setCursor(enterPosition - 1, 1);  
          lcd.print('*');
        #endif 
      } else {
        #ifdef LED_DISPLAY
          line2DispBuff[enterPosition] = allChars[position];
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef INLINE_LED_CODE
          line2DispBuff[enterPosition] = allChars[position];
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef LCD_DISPLAY
          enterPosition++;
          lcd.setCursor(enterPosition,1);
          lcd.print(allChars[position]);
        #endif  
      }
      event = EVENT_NONE;
    } else if (STATE_EDIT_STYLE == (machineState & STATE_EDIT_STYLE)) {
      style[enterPosition] = allChars[position];
      style[enterPosition + 1] = NULL_TERM;                                     // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)){
      setPurple();
      switch(position) {
         case SEND_USER_AND_PASSWORD:                                                                
            sendUsernameAndPassword();                                          // SEND THE USERNAME AND PASSWORD
            break; 
         case SEND_PASSWORD:                                                    // Find account
            sendPassword();                                                     // SEND THE USERNAME
            break;
         case SEND_USERNAME:                                                    // Enter account
            sendUsername();                                                     // SEND THE PASSWORD
            break;
         case SEND_ACCOUNT:                                                     // SEND THE ACCOUNT NAME
            sendAccount();
            break;
         case DELETE_ACCOUNT:                                                   // Delete account
            deleteAccount(acctPosition);
            if (acctPosition <= (acctCount - 1)) {                              // if we deleted the last account
              acctCount = acctPosition;                                         // adjust acctCount
            }
            acctPosition = 0;
            DisplayLine2("Account deleted");
            break;
         case BACKUP_ALL:                                                       // Send all
            sendAll();
            break;
      }
      setGreen();
      event = EVENT_NONE;
    } else if (STATE_ENTER_MASTER == (machineState & STATE_ENTER_MASTER)) {
      masterPassword[enterPosition] = allChars[position];
      masterPassword[enterPosition + 1] = NULL_TERM;                            // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        if (showPasswordsFlag) {
          line2DispBuff[enterPosition] = allChars[position];
        } else {
          line2DispBuff[enterPosition] = '*';
        }
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        if (showPasswordsFlag) {
          line2DispBuff[enterPosition] = allChars[position];
        } else {
          line2DispBuff[enterPosition] = '*';
        }
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif
      event = EVENT_NONE;
    }

  } else if (event == EVENT_BACKUP) {
    #ifdef EXTERNAL_EEPROM
      CopyEEPromToBackup();
    #endif
    event = EVENT_NONE;

  } else if (event == EVENT_RESET) {
    if (authenticated || (loginFailures > MAX_LOGIN_FAILURES)) {                // TODO: re-enter master password here to authorize creds reset
      acctCount = 0;
      acctPosition = 0;
      masterPassword[0] = NULL_TERM;                                            // set master password to null terminator in memory
      authenticated = false;                                                    // we're no longer authenticated, we need to re-enter the master password
      DisplayLine1("Initializing...");
      InitializeEEProm();                                                       // sets all of memory = INITIAL_MEMORY_STATE_BYTE, 0xFF/255/0b11111111
      InitializeIntEEProm();                                                    // initialize internal EEprom
      writeResetFlag(MEMORY_INITIALIZED_FLAG);                                  // signals that all memory has been initialized to INITIAL_MEMORY_STATE_BYTE
      setBlue();                                                                // we are no longer logged in
      loginFailures = 0;                                                        // set login failures back to zero, this also serves as a flag to indicate if it's the first power on
      writeLoginFailures();                                                     // write login failure count back to EEprom
      showPasswordsFlag = true;                                                 // to match the out of box setting (true / 255)
      writeShowPasswordsFlag();                                                 // write show passwords flag back to EEprom
      flipOnOff(showPasswordsFlag,SET_SHOW_PW_POS,14);                          // set the menu item to Show Passwrd ON or Show Passwrd OFF.
      keyboardFlag = false;
      writeKeyboardFlag();
      flipOnOff(keyboardFlag,SET_KEYBOARD_POS,10);                              // set the menu item accordingly
      accountName[0] = NULL_TERM;
      password[0] = NULL_TERM;
      username[0] = NULL_TERM;
      DisplayLine2("All creds erased");
    } else { 
      DisplayLine2("Not logged in");
    }
    event = EVENT_SHOW_MAIN_MENU;                                               // if any other state show main menu (e.g just after EVENT_RESET)

  } else if (event == EVENT_LOGOUT) {                                           // TODO: you need to be logged in to logout, check for authentication here
    if(authenticated) {    
      DisplayLine2("Logged out");
      position = 0;
      masterPassword[0] = NULL_TERM;
      authenticated = false;                                                    // we're no longer authenticated, we need to re-enter the master password
      loginFailures = 0;
      writeLoginFailures();
      setBlue();
      event = EVENT_SHOW_MAIN_MENU;
    } else {
      DisplayLine2("Not logged in");
      event = EVENT_SHOW_MAIN_MENU;
    }
  }
}

void ReadFromSerial(char *buffer) {
  if(keyboardFlag) {
    uint8_t serialCharCount = Serial.available();
    if (serialCharCount > 0) {
      for (uint8_t i = 0; i < (serialCharCount - 1); i++) {
        buffer[i] = Serial.read();                                              // read values input via serial monitor
      }
      buffer[serialCharCount - 1] = NULL_TERM;
//    while (Serial.avalable()) Serial.read();
      Serial.read();                                                            // read the last byte in the buffer, throw it out.
    }
    Serial.end();
  }
}

void SwitchRotatePosition(uint8_t pos) {
  switch(pos) {                                                                 // decide what to print on line 2 of the display
    case ADD_ACCOUNT:
      BlankLine2();
      break;
    case EDIT_ACCT_NAME:
      readAcctFromEEProm(acctPosition, accountName);
      DisplayLine2(accountName);
      break;
    case EDIT_USERNAME:
      readUserFromEEProm(acctPosition, username);
      DisplayLine2(username);
      break;
    case EDIT_PASSWORD:
      readPassFromEEProm(acctPosition, password);
      if (showPasswordsFlag) {
        DisplayLine2(password);
      } else {
        BlankLine2();
      }
      break;
    case EDIT_STYLE:
      readStyleFromEEProm(acctPosition, style);
      DisplayLine2(style);
      break;
    case GENERATE_PASSWORD:
      BlankLine2();
      break;
  }
}

//- Interrupt Service Routines
ISR(PCINT0_vect) {                                                              // Interrupt service routine for rotary encoder
  unsigned char result = rotaryEncoder.process();   
  //if (result == DIR_NONE) {                                                   // this actually fires many times per 'click' 
  //   debug(F("ISR fired: none"));
  //   event = EVENT_NONE;
  //} else 
  if (result == DIR_CW) {                                                       // rotated encoder clockwise
    event = EVENT_ROTATE_CW;
  }
  else if (result == DIR_CCW) {                                                 // rotated encoder counter clockwise
    event = EVENT_ROTATE_CC;
  }
}

//- Button

void buttonReleasedHandler(Button2& btn) {
  if(btn.wasPressedFor() > LONG_CLICK_LENGTH) {
    event = EVENT_LONG_CLICK;
  } else {
    event = EVENT_SINGLE_CLICK;
  }
}

/*                                                                              // Leaving these here for potential future use.
void buttonClickHandler(Button2& btn) {
  debugMetric("buttonClick",position);
  event = EVENT_SINGLE_CLICK;
}

void buttonLongClickHandler(Button2& btn) {
  debug("buttonLongClick reset pos");
  event = EVENT_LONG_CLICK;
  position = 0;
}

void buttonPressedHandler(Button2& btn) {
  debug("buttonPressed");
  event = EVENT_PRESSED;
}

void changed(Button2& btn) {
    Serial.println("changed");
}

void buttonDoubleClickHandler(Button2& btn) {
  //debug("buttonDoubleClick");
  event = EVENT_DOUBLE_CLICK;
}

void buttonTripleClickHandler(Button2& btn) {
  //debug("buttonTripleClick");
  event = EVENT_TRIPLE_CLICK;
}

void buttonTapHandler(Button2& btn) {
  debugMetric("buttonTap",position);
  event = EVENT_SINGLE_CLICK;
}
*/

//- Delete Account

void deleteAccount(uint8_t position) {
  DisplayLine2("Erasing creds");
  uint8_t emptyPassword[PASSWORD_SIZE];
  for (uint8_t i = 0; i < PASSWORD_SIZE; i++) {
    emptyPassword[i] = NULL_TERM;                                               // to completely overwrite the password in EEProm
  }
  byte allBitsOnArray[2];
  allBitsOnArray[0] = INITIAL_MEMORY_STATE_BYTE;                                // this makes the account name free/empty/available
  allBitsOnArray[1] = NULL_TERM;
  byte firstNullTermArray[1];
  firstNullTermArray[0] = NULL_TERM;                                            // equivalent to ""
  writeAllToEEProm( allBitsOnArray,                                             // account:  "-1\0", the -1 signals that the position is free/empty/available.
                    firstNullTermArray,                                         // username: "\0", so when it is read it will come back empty
                    emptyPassword,                                              // password: "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", to overwrite the entire pw
                    position                );                                  // position to be deleted
  accountName[0] = NULL_TERM;
  strcpy(password, emptyPassword);
  username[0] = NULL_TERM;
  DisplayLine2("Creds erased");
}

//- UUID Generation

void setUUID(uint8_t *password) {
  for (uint8_t i = 0; i < PASSWORD_SIZE; i++) {
    password[i] = random(33,126);                                               // maybe we should use allChars here instead? We're generating PWs w/ chars that we can't input...
  }                                                                             // 32 = space, 127 = <DEL>, so we want to choose from everything in between.
}

//- Keyboard Functions

void sendAccount() {
  readAcctFromEEProm(acctPosition, accountName);                                // read the account name from EEProm
  char accountNameChar[ACCOUNT_SIZE];
  memcpy(accountNameChar,accountName,ACCOUNT_SIZE);
  DisplayLine2(accountNameChar);                                                // show on bottom line of display
  Keyboard.begin();                                                             // TODO: can we do a <CTL><A> <BS> here first?  That will clear out pre-populated usernames.
  Keyboard.println(accountNameChar);                                            // type the account name through the keyboard
  Keyboard.end();
}

void sendUsername() {
  readUserFromEEProm(acctPosition, username);                                   // read the username from EEProm
  char usernameChar[USERNAME_SIZE];
  memcpy(usernameChar,username,USERNAME_SIZE);
  DisplayLine2(usernameChar);                                                   // show on bottom line of display
  Keyboard.begin();                                                             // TODO: can we do a <CTL><A> <BS> here first?  That will clear out pre-populated usernames.
  Keyboard.println(usernameChar);                                               // type the username through the keyboard
  Keyboard.end();
}

void sendPassword() {                                                           // TODO: can we do a <CTL><A> <BS> here first? That will clear out pre-populated passwords.
  readPassFromEEProm(acctPosition, password);                                   // read the password from EEProm
  char passwordChar[PASSWORD_SIZE];
  memcpy(passwordChar,password,PASSWORD_SIZE);
  Keyboard.begin();
  Keyboard.println(passwordChar);                                               // type the password through the keyboard, then <enter>
  Keyboard.end();
}

void sendUsernameAndPassword() {
  readAcctFromEEProm(acctPosition, accountName);                                // TODO: is the read from EEprom necessary at this point?
  char accountNameChar[ACCOUNT_SIZE];
  memcpy(accountNameChar,accountName,ACCOUNT_SIZE);
  DisplayLine2(accountNameChar);
  readUserFromEEProm(acctPosition, username);                                   // read the username from EEProm
  char usernameChar[USERNAME_SIZE];
  memcpy(usernameChar,username,USERNAME_SIZE);
  readPassFromEEProm(acctPosition, password);                                   // read the password from EEProm
  char passwordChar[PASSWORD_SIZE];
  memcpy(passwordChar,password,PASSWORD_SIZE);
  readStyleFromEEProm(acctPosition, style);                                     // read the style from EEprom
  Keyboard.begin();
  uint8_t i = 0;
  while (usernameChar[i++] != NULL_TERM) {
    Keyboard.write(usernameChar[i - 1]);                                        // seems to be a problem only with single character usernames.
  }
  //Keyboard.write(usernameChar, i);                                            // these 2 lines can be substituted for the line above (Keyboard.write())
  //Keyboard.print(usernameChar);                                               // type the username through the keyboard
  if ((strcmp(style, "0") == 0              ) ||
      (style[0] == INITIAL_MEMORY_STATE_CHAR)   ) {                             // this should make <CR> the default
    Keyboard.println("");                                                       // send <CR> through the keyboard
  } else {
    Keyboard.press(TAB_KEY);                                                    // if style isn't default or "0" then send <TAB> TODO: test this
    Keyboard.release(TAB_KEY);
  }
  delay(UN_PW_DELAY);
  Keyboard.println(passwordChar);                                               // type the username through the keyboard
  Keyboard.end();
}

void sendAll() {                                                                // this is the function we use to backup all of the accountnames, usernames and passwords
  for(uint8_t acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    #ifdef EXTERNAL_EEPROM
      if (read_eeprom_byte(GET_ADDR_ACCT(acctPos)) != 
          INITIAL_MEMORY_STATE_BYTE                   ) {
    #else
      cli();                                                                    // disable interrupts
      char ch = (char) EEPROM.read(GET_ADDR_ACCT(acctPos));                     // EEPROM.read returns a byte.  byte = unsigned char.  char = signed char.
      sei();                                                                    // re-enable interrupts
      if (ch != INITIAL_MEMORY_STATE_CHAR) {
    #endif
        readAcctFromEEProm(acctPos,accountName);
        char accountNameChar[ACCOUNT_SIZE];
        memcpy(accountNameChar,accountName,ACCOUNT_SIZE);
        readUserFromEEProm(acctPos,username);
        char usernameChar[USERNAME_SIZE];
        memcpy(usernameChar,username,USERNAME_SIZE);
        readPassFromEEProm(acctPos,password); 
        char passwordChar[PASSWORD_SIZE];
        memcpy(passwordChar,password,PASSWORD_SIZE);
        Keyboard.begin();
        Keyboard.println(accountNameChar);
        Keyboard.println("");                                                   
        Keyboard.println(usernameChar);
        Keyboard.println("");                                                   
        Keyboard.println(passwordChar);
        Keyboard.println("");                                                   
        Keyboard.println("");                                                   // place a carriage return between each account
        Keyboard.end();
      }
  }
}

//- Display Control

void DisplayLine1(char* lineToPrint) {
  #ifdef LED_DISPLAY
    strcpy(line1DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line1DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    BlankLine1();
    lcd.setCursor(0,0);
    lcd.print(lineToPrint);
  #endif 
}

void DisplayLine2(char* lineToPrint) {
  #ifdef LED_DISPLAY
    strcpy(line2DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line2DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    BlankLine2();
    lcd.setCursor(0,1);
    lcd.print(lineToPrint);
  #endif 
}

void BlankLine1() {
  #ifdef LED_DISPLAY
    strcpy(line1DispBuff,spaceFilled20);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line1DispBuff,spaceFilled16);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    lcd.setCursor(0,0);
    PrintBlankLine();
  #endif 
}

void BlankLine2() {
  #ifdef LED_DISPLAY
    strcpy(line2DispBuff,spaceFilled20);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line2DispBuff,spaceFilled16);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    lcd.setCursor(0,1);
    PrintBlankLine();                                                           // this is where LCD output is trashed, but not hung yet (on 2nd time called)
  #endif 
}

void PrintBlankLine() {
  #ifdef LCD_DISPLAY
    lcd.print(spaceFilled16);                                                   // this is where the LCD is trashed
  #endif 
}

void DisplayBuffer() {
#ifdef LED_DISPLAY
  #ifdef DEBUG
    debugNoLF("line1DispBuff: ");debug(line1DispBuff);
    debugNoLF("line2DispBuff: ");debug(line2DispBuff);
  #endif
  led.clearDisplay();
  led.setCursor(0,0);
  led.print(line1DispBuff);
  led.setCursor(0,LED_LINE_HEIGHT_PIX);
  led.print(line2DispBuff);
  led.display();
#endif
#ifdef INLINE_LED_CODE
  #ifdef DEBUG
    debugNoLF("line1DispBuff: ");debug(line1DispBuff);
    debugNoLF("line2DispBuff: ");debug(line2DispBuff);
  #endif
//  Serial.print("1 DisplayBuffer: ");
//  Serial.println(line1DispBuff);
  ssd1306_clear();
  ssd1306_string_font8x16xy(0, 0, line1DispBuff);
  ssd1306_string_font8x16xy(0, NO_LED_LIB_LIN_HEIGHT_PIX, line2DispBuff);
//  Serial.print("2 DisplayBuffer: ");
//  Serial.println(line2DispBuff);
#endif
}

void ShowMenu(uint8_t position, char **menu) {
  char line1[LCD_SCREEN_WIDTH+1] = "";
  strcat(line1, menu[position]);
  DisplayLine1(line1);
}

void MenuUp(char **menu) { 
  if (position > -1) {
    ShowMenu(position, menu);
  }
}

void MenuDown(char **menu){ 
  if (position < elements) {
    ShowMenu(position, menu);
  }
}

void ShowChar(char charToShow, uint8_t  pos) {
  #ifdef LED_DISPLAY
    DisplayBuffer();
    led.setCursor(LED_CHAR_WIDTH_PIX * pos,LED_LINE_HEIGHT_PIX);
    led.print(charToShow);
    led.display();
  #endif
  #ifdef INLINE_LED_CODE
    DisplayLine2(line2DispBuff);
    char charToPrint[2];
    charToPrint[0] = charToShow;
    charToPrint[1] = NULL_TERM;
    ssd1306_string_font8x16xy(NO_LED_LIB_CHAR_WIDTH_PIX * pos,
                              NO_LED_LIB_LIN_HEIGHT_PIX,
                              charToPrint                       );
  #endif
  #ifdef LCD_DISPLAY
    lcd.setCursor(pos,1);
    lcd.print(charToShow);
  #endif
}

void flipOnOff(uint8_t flag, uint8_t pos, uint8_t startInx) {
  if (!flag) {
    mainMenu[pos][startInx]   = 'F';
    mainMenu[pos][startInx+1] = 'F';
    mainMenu[pos][startInx+2] = NULL_TERM;
  } else {
    mainMenu[pos][startInx]   = 'N';
    mainMenu[pos][startInx+1] = ' ';
    mainMenu[pos][startInx+2] = NULL_TERM;
  }
}

//- RGB LED

void setPurple() {
  setColor(170, 0, 255);                                                        // Purple Color
}

void setRed(){
  setColor(255, 0, 0);                                                          // Red Color
}

void setGreen(){
  setColor(0, 255, 0);                                                          // Green Color
}


void setYellow(){
  setColor(255, 255, 0);                                                        // Yellow Color
}


void setBlue(){
  setColor(0, 0, 255);                                                          // Blue Color
}

/*
void setWhite(){
  setColor(255, 255, 255);                                                      // White Color
}

void setOff(){
  setColor(0,0,0);                                                              // Off
}

void setOrange(){
  setColor(255, 128, 0);                                                        // Orange Color
}
*/


void setColor(uint8_t  redValue, uint8_t  greenValue, uint8_t  blueValue) {
  analogWrite(RED_PIN, redValue);
  analogWrite(GREEN_PIN, greenValue);
  analogWrite(BLUE_PIN, blueValue);
}

//- Encryption

boolean authenticateMaster(uint8_t *password) {                                 // verify if the master password is correct here
  uint8_t buff[MASTER_PASSWORD_SIZE];                                           // changed from char to byte for external eeprom
//#ifdef EXTERNAL_EEPROM
//  uint8_t buf;
//  buf = read_eeprom_byte(GET_ADDR_MASTER_PASS);
//  if (buf == INITIAL_MEMORY_STATE_BYTE) {                                     // this is coming back as 0 when it should be 0xFF.
//#else
    cli();                                                                      // disable interrupts
    byte aByte = EEPROM.read(GET_ADDR_MASTER_PASS);
    sei();                                                                      // re-enable interrupts
    if (aByte == INITIAL_MEMORY_STATE_BYTE){                                    // first time, we need to write instead of read
//#endif
      #ifdef ENCRYPTION
        uint8_t pos = 0;
        while (password[pos++] != NULL_TERM);                                   // make sure the unencrypted password is 16 chars long
        while (pos < MASTER_PASSWORD_SIZE) password[pos++] = NULL_TERM;         // "           "              " , right padded w/ null terminator
        sha256Hash(password);                                                   // hash the master password
      #endif
      eeprom_write_int_bytes(GET_ADDR_MASTER_PASS, password, MASTER_PASSWORD_SIZE); // write the (hased) master password to EEprom
      setGreen();                                                               // turn the RGB green to signal the correct password was provided
    } else {                                                                    // (buf != INITIAL_MEMORY_STATE_BYTE) | (ch != INITIAL_MEMORY_STATE_CHAR)
      if (!eeprom_read_int_string(GET_ADDR_MASTER_PASS, buff, MASTER_PASSWORD_SIZE)) 
        ErrRpt(ERR_READ_MASTER);
      #ifdef ENCRYPTION
        uint8_t pos = 0;
        while (password[pos++] != NULL_TERM);                                   // make sure the password is 16 chars long
        while (pos < MASTER_PASSWORD_SIZE) password[pos++] = NULL_TERM;         // "           "              "
        sha256Hash(password);                                                   // hash the master password
        if (0 == memcmp(password, buff, MASTER_PASSWORD_SIZE)) {                // entered password hash matches master password hash, authenticated
      #else
        if (0 == strcmp(password, buff)) {                                      // entered password matches master password, authenticated
      #endif
          setGreen();                                                           // turn the RGB green to signal the correct password was provided
          loginFailures = 0;                                                    // reset loginFailues to zero
          writeLoginFailures();                                                 // record loginFailures in EEprom
                                                                                // encrypt a word using the master password as the key
        } else {                                                                // failed authentication
// Begin: decoy password comment                                                // Following section commented out because decoy logic needs to change to accomodate a hashed master password
//        if (0 == strcmp(password,strcat(buff,"FR"))) {                        // check for decoy password; masterPassword + "FR".
//          loginFailures = MAX_LOGIN_FAILURES + 2;                             // to turn this functionality back on we'd need to store a hashed version of masterPassword + "FR"
//          event = EVENT_RESET;                                                // in EEprom for comparison to the input password.
//          ProcessEvent();
//        } else {
// End: decoy password comment
          setRed();                                                            // turn the RGB red to signal the incorrect password was provided
          loginFailures++;
          writeLoginFailures();
          return false;
        }
    }
    #ifdef ENCRYPTION
      uint8_t pos = 0;
      while (password[pos++] != NULL_TERM);                                     // make sure the password is 16 chars long
      while (pos < MASTER_PASSWORD_SIZE) password[pos++] = NULL_TERM;           // "           "              "
      aes.setKey(password, MASTER_PASSWORD_SIZE);                               // set the key for aes to equal the master password
    #endif
  return true;
}                                                                               // and check it against the same word that's stored hashed
                                                                                // in eeprom.  This word is written (hashed) to eeprom the 
                                                                                // first time ever a master password is entered.

#ifdef ENCRYPTION
  void sha256Hash(char *password)
  {
    size_t size = strlen(password);
    size_t posn, len;
    uint8_t value[MASTER_PASSWORD_SIZE];

    sha256.reset();
    for (posn = 0; posn < size; posn += MASTER_PASSWORD_SIZE) {
        len = size - posn;
        if (len > MASTER_PASSWORD_SIZE)
            len = MASTER_PASSWORD_SIZE;
        sha256.update(password + posn, len);
    }
    sha256.finalize(value, sizeof(value));
    memcpy(password, value, MASTER_PASSWORD_SIZE);
  }
#endif

//- EEPROM functions

void writeAllToEEProm(uint8_t *accountName, 
                      uint8_t *username, 
                      uint8_t *password, 
                      uint8_t pos)        {                                     // used by delete account and factory reset.
  writeToEEPromByteArr(ACCOUNT_SIZE, accountName, GET_ADDR_ACCT(pos));
  writeToEEPromByteArr(USERNAME_SIZE, username, GET_ADDR_USER(pos));
  writeToEEPromByteArr(PASSWORD_SIZE, password, GET_ADDR_PASS(pos));
}

void writeToEEPromByteArr(uint8_t bufsize, uint8_t *buf, unsigned int ee_addr){
  #ifdef ENCRYPTION                                                             // TODO: this precompiler directive might not be necessary
    if (!eeprom_write_bytes(ee_addr, buf, bufsize))                             // if we write thusly in both situations.
      ErrRpt(ERR_EEPROM_WRITE);
  #else 
    if (!eeprom_write_bytes(ee_addr, buf)) 
      ErrRpt(ERR_EEPROM_WRITE);                                                 
  #endif
}

void countAccounts() {                                                          // count all of the account names from EEprom.
  for(uint8_t acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    #ifdef EXTERNAL_EEPROM
      if (read_eeprom_byte(GET_ADDR_ACCT(acctPos)) != 
          INITIAL_MEMORY_STATE_BYTE                         ) {
    #else
      cli();                                                                    // disable interrupts
      uint8_t ch = EEPROM.read(GET_ADDR_ACCT(acctPos));
      sei();                                                                    // re-enable interrupts
      if (ch != INITIAL_MEMORY_STATE_BYTE) {
    #endif
        acctCount++;
      }
  }
}

uint8_t getNextFreeAcctPos() {                                                  // return the position of the next EEprom location for account name marked empty.
  for(uint8_t acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    #ifdef EXTERNAL_EEPROM
      if (read_eeprom_byte(GET_ADDR_ACCT(acctPos)) == 
          INITIAL_MEMORY_STATE_BYTE                     ) {
    #else
      cli();                                                                    // disable interrupts
      uint8_t ch = EEPROM.read(GET_ADDR_ACCT(acctPos));                         // TODO: this is skipping some free addresses! Overwritten by password write?
      sei();                                                                    // re-enable interrupts
      if (ch == INITIAL_MEMORY_STATE_BYTE) {
    #endif
        return acctPos;
      }
  }
  return ERR_ACCOUNT_POS;
}

void readAcctFromEEProm(uint8_t pos, uint8_t *buf) {
  if (pos > -1) {
//  if (!eeprom_read_string(GET_ADDR_ACCT(pos), buf, ACCOUNT_SIZE)) 
    if (!eeprom_read_byteArr(GET_ADDR_ACCT(pos), buf, ACCOUNT_SIZE)) 
      ErrRpt(ERR_READ_ACCOUNT_2);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_BYTE) {
    buf[0] = NULL_TERM;                                                         // 8 bit twos complement of 255 or 0xFF
  } else {
    #ifdef ENCRYPTION
      decrypt32(buf, buf);
    #endif
  }
}

void readUserFromEEProm(uint8_t pos, uint8_t *buf) {
  if (pos > -1) {
//  if (!eeprom_read_string(GET_ADDR_USER(pos), buf, USERNAME_SIZE)) 
    if (!eeprom_read_byteArr(GET_ADDR_USER(pos), buf, USERNAME_SIZE)) 
      ErrRpt(ERR_READ_USER);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_BYTE) {
    buf[0] = NULL_TERM;
  } else {
    #ifdef ENCRYPTION
      decrypt32(buf, buf);
    #endif
  }
}

void readStyleFromEEProm(uint8_t pos, char *buf) {
  if (pos > -1) {
//  if (!eeprom_read_string(GET_ADDR_STYLE(pos), buf, STYLE_SIZE)) 
    if (!eeprom_read_byteArr(GET_ADDR_STYLE(pos), buf, STYLE_SIZE)) 
      ErrRpt(ERR_READ_STYLE);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_CHAR) buf[0] = NULL_TERM;
}

void readPassFromEEProm(uint8_t pos, uint8_t *buf) {                            // TODO: reduce readPassFromEEProm, readUserFromEEProm and readAcctFromEEProm to a single function.
  if (pos > -1) {
    if (!eeprom_read_byteArr(GET_ADDR_PASS(pos), buf, PASSWORD_SIZE)) 
      ErrRpt(ERR_READ_PASS);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_BYTE) {
    buf[0] = NULL_TERM;
  } else {
    #ifdef ENCRYPTION
      decrypt32(buf, buf);
    #endif
  }
}

void decrypt32(uint8_t *outBuffer, uint8_t *inBuffer) {                         // Necessary because blocksize of AES128/256 = 16 bytes.
  uint8_t leftInBuf[16];
  uint8_t rightInBuf[16];

  memcpy(leftInBuf, inBuffer, 16);
  memcpy(rightInBuf, inBuffer + 16, 16);
  
  aes.decryptBlock(leftInBuf, leftInBuf);                                       // decrypt the buffer 
  aes.decryptBlock(rightInBuf, rightInBuf);                                     // decrypt the buffer 

  memcpy(outBuffer, leftInBuf, 16);
  memcpy(outBuffer + 16, rightInBuf, 16);
}

uint8_t getLoginFailures() {
  #ifdef EXTERNAL_EEPROM
    return read_eeprom_byte(GET_ADDR_LOGIN_FAILURES);
  #else
    cli();                                                                      // disable interrupts
    return EEPROM.read(GET_ADDR_LOGIN_FAILURES);
    sei();                                                                      // re-enable interrupts
  #endif
}

uint8_t getResetFlag () {
  #ifdef EXTERNAL_EEPROM
    return read_eeprom_byte(GET_ADDR_RESET_FLAG);
  #else
    cli();                                                                      // disable interrupts
    return EEPROM.read(GET_ADDR_RESET_FLAG);
    sei();                                                                      // re-enable interrupts
  #endif
}

uint8_t getShowPasswordsFlag() {
  #ifdef EXTERNAL_EEPROM
    return read_eeprom_byte(GET_ADDR_SHOW_PW);
  #else
    cli();                                                                      // disable interrupts
    return EEPROM.read(GET_ADDR_SHOW_PW);
    sei();                                                                      // re-enable interrupts
  #endif
}

uint8_t getKeyboardFlag() {
  #ifdef EXTERNAL_EEPROM
    return read_eeprom_byte(GET_ADDR_KEYBOARD_FLAG);
  #else
    cli();                                                                      // disable interrupts
    return EEPROM.read(GET_ADDR_KEYBOARD_FLAG);
    sei();                                                                      // re-enable interrupts
  #endif
}

uint8_t getNextPtr(uint8_t pos) {
  return read_eeprom_byte(GET_ADDR_NEXT_POS(pos));
}

uint8_t getPrevPtr(uint8_t pos) {
  return read_eeprom_byte(GET_ADDR_PREV_POS(pos));
}

void writeNextPtr(uint8_t pos, uint8_t nextPtr) {
  write_eeprom_byte(GET_ADDR_NEXT_POS(pos), nextPtr);
}

void writePrevPtr(uint8_t pos, uint8_t prevPtr) {
  write_eeprom_byte(GET_ADDR_PREV_POS(pos), prevPtr);
}

void writeLoginFailures() {
  #ifdef EXTERNAL_EEPROM
    write_eeprom_byte(GET_ADDR_LOGIN_FAILURES, loginFailures);
  #else
    cli();                                                                      // disable interrupts
    EEPROM.write(GET_ADDR_LOGIN_FAILURES, loginFailures);
    sei();                                                                      // re-enable interrupts
  #endif
}

void writeResetFlag(uint8_t buf) {
  #ifdef EXTERNAL_EEPROM
    write_eeprom_byte(GET_ADDR_RESET_FLAG, buf);
  #else
    cli();                                                                      // disable interrupts
    EEPROM.write(GET_ADDR_RESET_FLAG, buf);
    sei();                                                                      // re-enable interrupts
  #endif
}

void writeShowPasswordsFlag() {
  #ifdef EXTERNAL_EEPROM
    write_eeprom_byte(GET_ADDR_SHOW_PW, showPasswordsFlag);
  #else
    cli();                                                                      // disable interrupts
    EEPROM.write(GET_ADDR_SHOW_PW, showPasswordsFlag);
    sei();                                                                      // re-enable interrupts
  #endif
  sei();                                                                        // re-enable interrupts
}
                                                                                // Following code is from smching: https://gist.github.com/smching/05261f11da11e0a5dc834f944afd5961 
void writeKeyboardFlag() {
  #ifdef EXTERNAL_EEPROM
    write_eeprom_byte(GET_ADDR_KEYBOARD_FLAG, keyboardFlag);
  #else
    cli();                                                                      // disable interrupts
    EEPROM.write(GET_ADDR_KEYBOARD_FLAG, keyboardFlag);
    sei();                                                                      // re-enable interrupts
  #endif
  sei();                                                                        // re-enable interrupts
}
                                                                                // This function is used by the other, higher-level functions
                                                                                // to prevent bugs and runtime errors due to invalid addresses.
boolean eeprom_is_addr_ok(unsigned int addr) {                                  // Returns true if the address is between the
  return ((addr >= MIN_AVAIL_ADDR) && (addr <= MAX_AVAIL_ADDR));                // minimum and maximum allowed values, false otherwise.
}
                                                                                // Writes a sequence of bytes to eeprom starting at the specified address.
                                                                                // Returns true if the whole array is successfully written.
                                                                                // Returns false if the start or end addresses aren't between
                                                                                // the minimum and maximum allowed values.
                                                                                // When returning false, nothing gets written to eeprom.
boolean eeprom_write_bytes( unsigned int startAddr, 
                            const uint8_t* buf, 
                            uint8_t numBytes) {
                                                                                // counter
  uint8_t i;
                                                                                // both first byte and last byte addresses must fall within
                                                                                // the allowed range 
  if (!eeprom_is_addr_ok(startAddr) || 
      !eeprom_is_addr_ok(startAddr + numBytes)) {
    return false;
  }
  #ifdef EXTERNAL_EEPROM
    if (numBytes > EEPROM_BYTES_PER_PAGE) numBytes = EEPROM_BYTES_PER_PAGE;
    write_eeprom_array(startAddr, buf, numBytes);
  #else
    cli();                                                                      // disable interrupts
    for (i = 0; i < numBytes; i++) {
      EEPROM.write(startAddr + i, buf[i]);
    }
    sei();                                                                      // re-enable interrupts
  #endif
  return true;
}
boolean eeprom_write_int_bytes( unsigned int startAddr, 
                                const uint8_t* buf, 
                                uint8_t numBytes) {
                                                                                // counter
  uint8_t i;
                                                                                // both first byte and last byte addresses must fall within
                                                                                // the allowed range 
  if (!eeprom_is_addr_ok(startAddr) || 
      !eeprom_is_addr_ok(startAddr + numBytes)) {
    return false;
  }
  cli();                                                                        // disable interrupts
  for (i = 0; i < numBytes; i++) {
    EEPROM.write(startAddr + i, buf[i]);
  }
  sei();                                                                        // re-enable interrupts
  return true;
}

                                                                                // Writes a string starting at the specified address.
                                                                                // Returns true if the whole string is successfully written.
                                                                                // Returns false if the address of one or more bytes fall outside the allowed range.
                                                                                // If false is returned, nothing gets written to the eeprom.
boolean eeprom_write_string(unsigned int addr, const char* string) {
  uint8_t numChar;                                                              // actual number of bytes to be written
                                                                                // write the string contents plus the string terminator byte (0x00)
  numChar = strlen(string) + 1;
  return eeprom_write_bytes(addr, (const uint8_t*) string, numChar);
}

                                                                                // Reads a string starting from the specified address.
                                                                                // Returns true if at least one byte (even only the string terminator one) is read.
                                                                                // Returns false if the start address falls outside the allowed range or declare buffer size is zero.
                                                                                // 
                                                                                // The reading might stop for several reasons:
                                                                                // - no more space in the provided buffer
                                                                                // - last eeprom address reached
                                                                                // - string terminator byte (0x00) encountered.
boolean eeprom_read_int_string( unsigned int addr, 
                                unsigned char* buffer, 
                                uint8_t bufSize) {
  uint8_t ch;                                                                   // byte read from eeprom
  uint8_t bytesRead;                                                            // number of bytes read so far
  if (!eeprom_is_addr_ok(addr)) {                                               // check start address
    return false;
  }

  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
                                                                                // is there is room for the string terminator only, no reason to go further TODO: is this right?
  if (bufSize == 1) {                                                           // can we delete this logic block alltogether?  Is the null terminator consuming the last array element of buffer?
    buffer[0] = NULL_TERM;
    return true;
  }
  cli();                                                                        // disable global interrupts
  bytesRead = 0;                                                                // initialize byte counter
  ch = EEPROM.read(addr + bytesRead);                                           // read next byte from eeprom
  buffer[bytesRead] = ch;                                                       // store it into the user buffer
  bytesRead++;                                                                  // increment byte counter
                                                                                // stop conditions:
                                                                                // - the character just read is the string terminator one (0x00)
                                                                                // - we have filled the user buffer
                                                                                // - we have reached the last eeprom address
  while ( (bytesRead < bufSize) && ((addr + bytesRead) <= MAX_AVAIL_ADDR) ) {   // eliminate check for NULL_TERM because of hashing
                                                                                // if no stop condition is met, read the next byte from eeprom
    ch = EEPROM.read(addr + bytesRead);
    buffer[bytesRead] = ch;                                                     // store it into the user buffer
    bytesRead++;                                                                // increment byte counter
  }
  sei();                                                                        // enable global interrupts
  return true;
}                                                                               // end smching

boolean eeprom_read_byteArr(unsigned int addr, 
                            uint8_t* buffer, 
                            uint8_t bufSize   ) {
#ifdef EXTERNAL_EEPROM
  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
  read_eeprom_array(addr, buffer, bufSize);
  return true;
#else
  uint8_t bytesRead;                                                            // number of bytes read so far
  if (!eeprom_is_addr_ok(addr)) {                                               // check start address
    return false;
  }

  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
  
  cli();                                                                        // disable interrupts
                                                                                // is there is room for the string terminator only, no reason to go further
  bytesRead = 0;                                                                // initialize byte counter
  buffer[bytesRead] = EEPROM.read(addr + bytesRead);                            // read next byte from eeprom, store it into the user buffer
  bytesRead++;                                                                  // increment byte counter
                                                                                // stop conditions:
                                                                                // - we have filled the user buffer
  while ( (bytesRead < bufSize) && ((addr + bytesRead) <= MAX_AVAIL_ADDR) ) {   // - we have reached the last eeprom address
                                                                                // if no stop condition is met, read the next byte from eeprom
    buffer[bytesRead] = EEPROM.read(addr + bytesRead);                          // store it into the user buffer
    bytesRead++;                                                                // increment byte counter
  }
  sei();                                                                        // re-enable interrupts

  return true;
#endif
}                                                                               // end smching

#ifdef EXTERNAL_EEPROM
  void InitializeEEProm(void) {
    cli();                                                                      // disable global interrupts
    uint16_t pageAddress = 0;
    boolean colorRed = true;                                                    // show purple during healthy EEprom initialize
    while (pageAddress < MAX_AVAIL_ADDR) {
      if (colorRed) {
        setRed();
        colorRed = false;
      } else {
        setYellow();
        colorRed = true;
      }
      EEPROM_writeEnable();
      SLAVE_PRIMARY_SELECT;
      SPI_tradeByte(EEPROM_WRITE);
      EEPROM_send16BitAddress(pageAddress);
      for (uint8_t i = 0; i < EEPROM_BYTES_PER_PAGE; i++) {
        SPI_tradeByte(INITIAL_MEMORY_STATE_BYTE);
      }
      SLAVE_PRIMARY_DESELECT;
      pageAddress += EEPROM_BYTES_PER_PAGE;
      while (EEPROM_readStatus() & _BV(EEPROM_WRITE_IN_PROGRESS)) {;
      }
    }
    setBlue();
    sei();                                                                      // enable global interrupts
  }
#endif

void InitializeIntEEProm() {
  cli();                                                                        // disable global interrupts
  boolean colorRed = true;
  for (unsigned int addr = MIN_AVAIL_INT_ADDR; addr <= MAX_AVAIL_INT_ADDR; addr++) {
    if(addr%64==0) {                                                            // RGB is purple when initializing EEprom
      if (colorRed) {
        setRed();
        colorRed = false;
      } else {
        setYellow();
        colorRed = true;
      }
    }
    EEPROM.write(addr, INITIAL_MEMORY_STATE_BYTE);                              // second parameter is a byte.  TODO: do a bulk write to improve speed
  }
  setBlue();
  sei();                                                                        // enable global interrupts
}

void ErrRpt(uint8_t err) {                                                      // Expects an error number to be passed in. Using numbers to save on global memory
  char buf[5];
  itoa(err, buf, 10);                                                           // convert the int to a string, base 10.
  char message[21] = "Error: ";
  strcat(message, buf);
  DisplayLine2(message);                                                        // Print the error number on line 2 of the display (e.g. "Err: 2" for error reading the account)
}

//- SSD1306 Routines
#ifdef INLINE_LED_CODE
  const uint8_t ssd1306_init_sequence [] PROGMEM = {                            // Initialization Sequence
    0xAE,                                                                       // Display OFF (sleep mode)
    0x20, 0b00,                                                                 // Set Memory Addressing Mode
                                                                                // 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
                                                                                // 10=Page Addressing Mode (RESET); 11=Invalid
    0xB0,                                                                       // Set Page Start Address for Page Addressing Mode, 0-7
    0xC8,                                                                       // Set COM Output Scan Direction
    0x00,                                                                       // ---set low column address
    0x10,                                                                       // ---set high column address
    0x40,                                                                       // --set start line address
    0x81, 0xFF,                                                                 // Set contrast control register 0x3F
    0xA1,                                                                       // Set Segment Re-map. A0=address mapped; A1=address 127 mapped. 
    0xA6,                                                                       // Set display mode. A6=Normal; A7=Inverse
    0xA8, 0x3F,                                                                 // Set multiplex ratio(1 to 64)
    0xA4,                                                                       // Output RAM to Display
                                                                                // 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
    0xD3, 0x00,                                                                 // Set display offset. 00 = no offset
    0xD5,                                                                       // --set display clock divide ratio/oscillator frequency
    0xF0,                                                                       // --set divide ratio
    0xD9, 0x22,                                                                 // Set pre-charge period
    0xDA, 0x12,                                                                 // Set com pins hardware configuration    
    0xDB,                                                                       // --set vcomh
    0x20,                                                                       // 0x20,0.77xVcc
    0x8D, 0x14,                                                                 // Set DC-DC enable
    0xAF                                                                        // Display ON in normal mode
  
  };

  void ssd1306_init(void)
  {
    for (uint8_t i = 0; i < sizeof (ssd1306_init_sequence); i++) {
      ssd1306_send_command(pgm_read_byte(&ssd1306_init_sequence[i]));
    }
  }

  void ssd1306_send_command(uint8_t command)
  {
    i2c_start();
    i2c_writebyte(SSD1306_I2C_ADDR);                                            // Slave address, SA0=0
    i2c_writebyte(0x00);                                                        // write command
    i2c_writebyte(command);
    i2c_stop();
  }

  void ssd1306_setpos(uint8_t x, uint8_t y)
  {
    i2c_start();
    i2c_writebyte(SSD1306_I2C_ADDR);                                            // Slave address, SA0=0
    i2c_writebyte(0x00);                                                        // write command
    i2c_writebyte(0xb0 + y);
    i2c_writebyte(((x & 0xf0) >> 4) | 0x10);                                    // | 0x10
    i2c_writebyte((x & 0x0f));                                                  // | 0x01
    i2c_stop();
  }

  void ssd1306_fill4(uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4) 
  {
    ssd1306_setpos(0, 0);
    i2c_start();
    i2c_writebyte(SSD1306_I2C_ADDR);
    i2c_writebyte(0x40);                                                        // write data
    for (uint16_t i = 0; i < 128 * 8 / 4; i++) {
      i2c_writebyte(p1);
      i2c_writebyte(p2);
      i2c_writebyte(p3);
      i2c_writebyte(p4);
    }
    i2c_stop();
  }

  void ssd1306_fill(uint8_t p) {
    ssd1306_fill4(p, p, p, p);
  }

  void ssd1306_string_font8x16xy(uint8_t x, uint8_t y, const char s[]) {
    uint8_t ch, j = 0;
    while (s[j] != '\0') {
      ch = s[j] - 32;
      if (x > 120) {
        x = 0;
        y++;
      }
      ssd1306_setpos(x, y);
      i2c_start();
      i2c_writebyte(SSD1306_I2C_ADDR);
      i2c_writebyte(0x40);                                                      // write data
      for (uint8_t i = 0; i < 8; i++) {
        i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[ch * 16 + i]));
      }
      i2c_stop();
      ssd1306_setpos(x, y + 1);
      i2c_start();
      i2c_writebyte(SSD1306_I2C_ADDR);
      i2c_writebyte(0x40);                                                      // write data
      for (uint8_t i = 0; i < 8; i++) {
        i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[ch * 16 + i + 8]));
      }
      i2c_stop();
      x += 8;
      j++;
    }
  }

  void ssd1306_char_font8x16(char ch) 
  {
    uint8_t c = ch - 32;
    i2c_start();
    i2c_writebyte(SSD1306_I2C_ADDR);
    i2c_writebyte(0x40);                                                        // write data
    for (uint8_t i = 0; i < 8; i++) {
      i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[c * 16 + i]));
    }
    i2c_stop();
  }

//- Software I2C Routines
  void i2c_init() {
    SDA_DIR |= (1 << SDA_BIT);                                                  // Set port as output
    SCL_DIR |= (1 << SCL_BIT);                                                  // Set port as output
  }

  void i2c_start() {
    SCL_high();
    SDA_high();
    SDA_low();
    SCL_low();
  }

  void i2c_stop() {
    SCL_low();
    SDA_low();
    SCL_high();
    SDA_high();
  }

  void i2c_writebyte(uint8_t byte) {
    uint8_t i;
    for (i = 0; i < 8; i++) {
      if ((byte << i) & 0x80) {
        SDA_high();
      } else {
        SDA_low();
      }
      SCL_high();
      SCL_low();
    }
    SDA_high();
    SCL_high();
    SCL_low();
  }
#endif

//- Debugging Routines                                                          // These routines are helpful for debugging.
                                                                                // For sending output to the serial monitor. Set the baud rate in setup.
#ifdef DEBUG
  void debug(String text) {
    Serial.println(text);
  }

  void debugNoLF(String text) {
    Serial.print(text);
  }

  void debugInt(signed int anInt) {
    char myInt[10];
    itoa(anInt,myInt,10);
    debug(myInt);
  }

  void debugMetric(const char myString[], signed int anInt) {
    debugNoLF(myString);debugNoLF(F(": "));
    debugInt(anInt);
    Serial.print(F("\r\n"));
  }
#else
  void debug(String text) {
  }

  void debugNoLF(String text) {
  }

  void debugInt(signed int anInt) {
  }

  void debugMetric(const char myString[], signed int anInt) {
  }
#endif

#ifdef I2C_SCAN
  void i2cScan() {
    uint8_t error, address;
    int nDevices;
  
    Serial.println("Scanning...");
   
    nDevices = 0;
    for(address = 1; address < 127; address++ ) {
                                                                                // The i2c_scanner uses the return value of
                                                                                // the Write.endTransmisstion to see if
                                                                                // a device did acknowledge to the address.
      Wire.beginTransmission(address);
      error = Wire.endTransmission();
  
      if (error == 0)
      {
        Serial.print("I2C device found at address 0x");
        if (address<16)
        Serial.print("0");
        Serial.print(address,HEX);
        Serial.println("  !");
  
        nDevices++;
      }
      else if (error==4)
      {
        Serial.print("Unknown error at address 0x");
        if (address<16)
        Serial.print("0");
        Serial.println(address,HEX);
      }    
    }
    if (nDevices == 0)
      Serial.println("No I2C devices found\n");
    else
      Serial.println("done\n");
  }
#endif

#ifdef EXTERNAL_EEPROM
                                                                                // SPI logic from "Make: AVR Programming", Chapter 16. SPI, by Elloit Williams, Published by Maker Media, Inc, 2014
                                                                                // https://www.safaribooksonline.com/library/view/make-avr-programming/9781449356484/ch16.html
  void initSPI(void) {
    SPI_SS_PRIMARY_DDR |= (1 << SPI_SS_PRIMARY);                                // set SS output for primary EEprom chip
    SPI_SS_PRIMARY_DDR |= (1 << SPI_SS_PRIMARY);                                // set SS output for primary EEprom chip
    SPI_SS_PRIMARY_PORT |= (1 << SPI_SS_PRIMARY);                               // start off not selected (high)

    SPI_SS_SECONDARY_DDR |= (1 << SPI_SS_SECONDARY);                            // set SS output for backup EEprom chip
    SPI_SS_SECONDARY_DDR |= (1 << SPI_SS_SECONDARY);                            // set SS output for backup EEprom chip
    SPI_SS_SECONDARY_PORT |= (1 << SPI_SS_SECONDARY);                           // start off not selected (high)

    SPI_MOSI_DDR |= (1 << SPI_MOSI);                                            // output on MOSI
    SPI_MISO_PORT |= (1 << SPI_MISO);                                           // pullup on MISO
    SPI_SCK_DDR |= (1 << SPI_SCK);                                              // output on SCK

                                                                                // Don't have to set phase, polarity b/c default works with 25LCxxx chips
//  SPCR |= (1 << SPR1);                                                        // original coment said this was "div 16, safer for breadboards", but it looks like div 64
    SPCR |= (1 << SPR0);                                                        // div 16 (if alone)
    SPCR |= (1 << SPR1);                                                        // div 128 (with line above)
    //SPCR |= (1 << SPI2X);                                                     // add this to double the rate to div 8, pg. 183 of https://www.pjrc.com/teensy/atmega32u4.pdf
    SPCR |= (1 << MSTR);                                                        // clockmaster
    SPCR |= (1 << SPE);                                                         // enable
  }

  void SPI_tradeByte(uint8_t byte) {
    SPDR = byte;                                                                // SPI starts sending immediately
    loop_until_bit_is_set(SPSR, SPIF);                                          // wait until done
                                                                                // SPDR now contains the received byte
  }

  void EEPROM_send16BitAddress(uint16_t address) {
    SPI_tradeByte((uint8_t) (address >> 8));                                    // most significant byte
    SPI_tradeByte((uint8_t) address);                                           // least significant byte
  }

  uint8_t EEPROM_readStatus(void) {
    SLAVE_PRIMARY_SELECT;
    SPI_tradeByte(EEPROM_RDSR);
    SPI_tradeByte(0);                                                           // clock out eight bits
    SLAVE_PRIMARY_DESELECT;
    return (SPDR);                                                              // return the result
  }

  uint8_t EEPROM_readStatusSecondary(void) {
    SLAVE_SECONDARY_SELECT;
    SPI_tradeByte(EEPROM_RDSR);
    SPI_tradeByte(0);                                                           // clock out eight bits
    SLAVE_SECONDARY_DESELECT;
    return (SPDR);                                                              // return the result
  }

  void EEPROM_writeEnable(void) {
    SLAVE_PRIMARY_SELECT;
    SPI_tradeByte(EEPROM_WREN);
    SLAVE_PRIMARY_DESELECT;
  }

  uint8_t read_eeprom_byte(uint16_t address) {
    cli();                                                                      // disable global interrupts
    SLAVE_PRIMARY_SELECT;
    SPI_tradeByte(EEPROM_READ);
    EEPROM_send16BitAddress(address);
    SPI_tradeByte(0);
    SLAVE_PRIMARY_DESELECT;
    sei();                                                                      // enable global interrupts
    return (SPDR);
  }

  void read_eeprom_array( uint16_t address, 
                          uint8_t *buffer, 
                          uint8_t sizeOfBuffer  ) {                             // READ EEPROM bytes
    cli();                                                                      // disable global interrupts
    SLAVE_PRIMARY_SELECT;
    SPI_tradeByte(EEPROM_READ);
    EEPROM_send16BitAddress(address);
    for (uint8_t i = 0; i < sizeOfBuffer; i++) {
      SPI_tradeByte(0);
      *buffer++ = SPDR;                                                         //get data byte
    }
    SLAVE_PRIMARY_DESELECT;
    sei();                                                                      // enable global interrupts
  }

  void write_eeprom_byte(uint16_t address, uint8_t byte) {
    cli();                                                                      // disable global interrupts
    EEPROM_writeEnable();
    SLAVE_PRIMARY_SELECT;
    SPI_tradeByte(EEPROM_WRITE);
    EEPROM_send16BitAddress(address);
    SPI_tradeByte(byte);
    SLAVE_PRIMARY_DESELECT;
    while (EEPROM_readStatus() & _BV(EEPROM_WRITE_IN_PROGRESS)) {;
    }
    sei();                                                                      // enable global interrupts
  }

  void write_eeprom_array(uint16_t address, 
                          uint8_t *buffer, 
                          uint8_t sizeOfBuffer) {
    cli();                                                                      // disable global interrupts
    EEPROM_writeEnable();
    SLAVE_PRIMARY_SELECT;
    SPI_tradeByte(EEPROM_WRITE);
    EEPROM_send16BitAddress(address);

    for (uint8_t i=0;i<sizeOfBuffer;i++)
    {
      SPI_tradeByte(buffer[i]);
    }
    SLAVE_PRIMARY_DESELECT;
    while (EEPROM_readStatus() & _BV(EEPROM_WRITE_IN_PROGRESS)) {;
    }
    sei();                                                                      // enable global interrupts
  }

  void CopyEEPromToBackup() {                                                   // Make a byte for byte duplicate of the primary external EEprom device
    cli();                                                                      // disable global interrupts
    uint8_t buffer[EEPROM_BYTES_PER_PAGE];                                      // make a buffer the same size as the page size.
    boolean colorRed = true;
    for ( uint16_t address = MIN_AVAIL_ADDR; 
          address < MAX_AVAIL_ADDR; 
          address += EEPROM_BYTES_PER_PAGE) {
      if (colorRed) {                                                           // if everything is working correctly user will see purple
        setRed();
        colorRed = false;
      } else {
        setYellow();
        colorRed = true;
      }
      read_eeprom_array(address, buffer, EEPROM_BYTES_PER_PAGE);                // read in a page at a time from the primary/source device
      SLAVE_SECONDARY_SELECT;
      SPI_tradeByte(EEPROM_WREN);                                               // write enable secondary
      SLAVE_SECONDARY_DESELECT;
      SLAVE_SECONDARY_SELECT;                                                   // select the secondary EEprom device for SPI
      SPI_tradeByte(EEPROM_WRITE);
      EEPROM_send16BitAddress(address);
      for (uint8_t i=0;i<EEPROM_BYTES_PER_PAGE;i++) {                           // write the page out byte for byte
        SPI_tradeByte(buffer[i]);
      }
      SLAVE_SECONDARY_DESELECT;                                                 // deselect the secondary EEprom device for SPI
      while (EEPROM_readStatusSecondary() & _BV(EEPROM_WRITE_IN_PROGRESS)) {
      }
    }
    setGreen();
    sei();                                                                      // done with the copy, re-enable global interrupts
  }

#endif

#ifdef ENCRYPTION
  void encrypt32Bytes(uint8_t *outBuffer, uint8_t *inBuffer) {
    uint8_t leftInBuffer[16];
    uint8_t rightInBuffer[16];
  
    memcpy(leftInBuffer, inBuffer, 16);
    memcpy(rightInBuffer, inBuffer + 16, 16);
    
    aes.encryptBlock(leftInBuffer, leftInBuffer);
    aes.encryptBlock(rightInBuffer, rightInBuffer);
    
    memcpy(outBuffer, leftInBuffer, 16);
    memcpy(outBuffer + 16, rightInBuffer, 16);
  }
#endif

uint8_t findHeadPosition() {
  uint8_t prevPtr;
  for(uint8_t pos = 0; pos <= CREDS_ACCOMIDATED; pos++) {
    prevPtr = getPrevPtr(pos);
    if(prevPtr == INITIAL_MEMORY_STATE_BYTE) {
      return(pos);
    }
  }
  return(0);
}

uint8_t findTailPosition() {
  uint8_t nextPtr;
  for(uint8_t pos = 0; pos <= CREDS_ACCOMIDATED; pos++) {
    nextPtr = getNextPtr(pos);
    if(nextPtr == INITIAL_MEMORY_STATE_BYTE) {
      return(pos);
    }
  }
  return(0);
}

void insertCreds(uint8_t accountPosition, uint8_t *accountName) {               // traverse through the linked list finding the right spot to insert this record in the list
  
  uint8_t acctBuf[ACCOUNT_SIZE];
  uint8_t currentPosition = headPosition;
  uint8_t prevPosition = INITIAL_MEMORY_STATE_BYTE;
  readAcctFromEEProm(currentPosition, acctBuf);  
  while (currentPosition != tailPosition && 
         (strncmp(accountName, acctBuf, ACCOUNT_SIZE) < 0)) {                   // if Return value < 0 then it indicates str1 is less than str2.
    prevPosition = currentPosition;
    currentPosition = getNextPtr(currentPosition);
    readAcctFromEEProm(currentPosition, acctBuf);  
  }
  if(currentPosition == headPosition) {
    headPosition = accountPosition;
  }
  if (currentPosition == tailPosition) {
    tailPosition = accountPosition;
  }
  writePrevPtr(accountPosition, prevPosition);                                  // insert between prevPosition and currentPosition
  writeNextPtr(accountPosition, currentPosition);
  if (prevPosition != INITIAL_MEMORY_STATE_BYTE) {
    writeNextPtr(prevPosition, accountPosition);
  }
  if (currentPosition != INITIAL_MEMORY_STATE_BYTE) {
    writePrevPtr(currentPosition, accountPosition);
  }
}

void deleteCreds(uint8_t accountPosition) {
}