#include "prj.h"
//#include "bmp_einstein.h"
//#include "bmp_weather_station.h"
#include "soft_i2c.h"
#include "font6x8.h"
#include "font8x16.h"
#include "oled_ssd1306.h"
 
#define SCL_low()              SCL_PORT  &= ~_BV(SCL_BIT)   
#define SCL_high()             SCL_PORT  |=  _BV(SCL_BIT)   

#define SDA_low()              SDA_PORT  &= ~_BV(SDA_BIT)   
#define SDA_high()             SDA_PORT  |=  _BV(SDA_BIT)   

// ----------------------------------------------------------------------------

void i2c_oled_demo()
{
  ssd1306_clear(); _delay_ms(200);
  // show text on screen
  //=====================
  ssd1306_clear(); _delay_ms(200);
	ssd1306_string_font8x16xy(16, 0, "I2C OLED Demo");
	ssd1306_string_font8x16xy(16, 4, "Version 1.00");
//sd1306_setpos(30, 7);	
//sd1306_string_font6x8("2017-03-25");
	_delay_ms(5000);
}


void setup()
{
    // Small delay is necessary if ssd1306_init is the 
    // first operation in the application.
	_delay_ms(40);

  i2c_init();
	ssd1306_init();
}

void loop() {
  i2c_oled_demo();
}

void i2c_init()
{
  SDA_DIR |= (1 << SDA_BIT);  // Set port as output
  SCL_DIR |= (1 << SCL_BIT);  // Set port as output
}

void i2c_start(void)
{

    SCL_high();
    SDA_high();
    SDA_low();
    SCL_low();
}

void i2c_stop(void)
{

    SCL_low();
    SDA_low();
    SCL_high();
    SDA_high();
}

void i2c_writebyte(uint8_t byte)
{
  uint8_t i;
  for (i = 0; i < 8; i++)
  {
    if ((byte << i) & 0x80)
        {
    
            SDA_high();
            }
    else
        {
    
            SDA_low();
            }
    
  
        SCL_high();
        SCL_low();
  }

    SDA_high();
    SCL_high();
    SCL_low();
}
// ----------------------------------------------------------------------------
 
const uint8_t ssd1306_init_sequence [] PROGMEM = {  // Initialization Sequence
  0xAE,     // Display OFF (sleep mode)
  0x20, 0b00,   // Set Memory Addressing Mode
          // 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
          // 10=Page Addressing Mode (RESET); 11=Invalid
  0xB0,     // Set Page Start Address for Page Addressing Mode, 0-7
  0xC8,     // Set COM Output Scan Direction
  0x00,     // ---set low column address
  0x10,     // ---set high column address
  0x40,     // --set start line address
  0x81, 0x3F,   // Set contrast control register
  0xA1,     // Set Segment Re-map. A0=address mapped; A1=address 127 mapped. 
  0xA6,     // Set display mode. A6=Normal; A7=Inverse
  0xA8, 0x3F,   // Set multiplex ratio(1 to 64)
  0xA4,     // Output RAM to Display
          // 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
  0xD3, 0x00,   // Set display offset. 00 = no offset
  0xD5,     // --set display clock divide ratio/oscillator frequency
  0xF0,     // --set divide ratio
  0xD9, 0x22,   // Set pre-charge period
  0xDA, 0x12,   // Set com pins hardware configuration    
  0xDB,     // --set vcomh
  0x20,     // 0x20,0.77xVcc
  0x8D, 0x14,   // Set DC-DC enable
  0xAF      // Display ON in normal mode
  
};

 
// ----------------------------------------------------------------------------

void ssd1306_init(void)
{
    for (uint8_t i = 0; i < sizeof (ssd1306_init_sequence); i++)
    {
    ssd1306_send_command(pgm_read_byte(&ssd1306_init_sequence[i]));
  }
}

void ssd1306_send_command(uint8_t command)
{
  i2c_start();
  i2c_writebyte(SSD1306_SA);  // Slave address, SA0=0
  i2c_writebyte(0x00);  // write command
  i2c_writebyte(command);
  i2c_stop();
}

void ssd1306_setpos(uint8_t x, uint8_t y)
{
  i2c_start();
  i2c_writebyte(SSD1306_SA);  // Slave address, SA0=0
  i2c_writebyte(0x00);  // write command
  i2c_writebyte(0xb0 + y);
  i2c_writebyte(((x & 0xf0) >> 4) | 0x10); // | 0x10

    i2c_writebyte((x & 0x0f)); // | 0x01
  i2c_stop();
}

void ssd1306_fill4(uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4) 
{
  ssd1306_setpos(0, 0);
    i2c_start();
  i2c_writebyte(SSD1306_SA);
  i2c_writebyte(0x40);  //write data
  for (uint16_t i = 0; i < 128 * 8 / 4; i++) 
    {
    i2c_writebyte(p1);
    i2c_writebyte(p2);
    i2c_writebyte(p3);
    i2c_writebyte(p4);
  }
  i2c_stop();
}

void ssd1306_fill(uint8_t p) 
{
  ssd1306_fill4(p, p, p, p);
}

// ----------------------------------------------------------------------------

void ssd1306_char_font6x8(char ch) 
{
  uint8_t c = ch - 32;
  i2c_start();
  i2c_writebyte(SSD1306_SA);
  i2c_writebyte(0x40);  //write data
  for (uint8_t i = 0; i < 6; i++)
  {
    i2c_writebyte(pgm_read_byte(&ssd1306xled_font6x8[c * 6 + i]));
  }
  i2c_stop();
}

void ssd1306_string_font6x8(char *s) 
{
  while (*s) 
    {
    ssd1306_char_font6x8(*s++);
  }
}

 
// ----------------------------------------------------------------------------

void ssd1306_string_font8x16xy(uint8_t x, uint8_t y, const char s[]) 
{
  uint8_t ch, j = 0;
  while (s[j] != '\0') 
    {
    ch = s[j] - 32;
    if (x > 120) 
        {
      x = 0;
      y++;
    }
    ssd1306_setpos(x, y);
    i2c_start();
      i2c_writebyte(SSD1306_SA);
      i2c_writebyte(0x40);  //write data
    for (uint8_t i = 0; i < 8; i++) 
        {
      i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[ch * 16 + i]));
    }
    i2c_stop();
    ssd1306_setpos(x, y + 1);
    i2c_start();
      i2c_writebyte(SSD1306_SA);
      i2c_writebyte(0x40);  //write data
    for (uint8_t i = 0; i < 8; i++) 
        {
      i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[ch * 16 + i + 8]));
    }
    i2c_stop();
    x += 8;
    j++;
  }
}
