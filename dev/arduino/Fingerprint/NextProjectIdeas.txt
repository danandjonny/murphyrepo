- On a breadboard add the following components:
    fingerprint reader
    piezo buzzer
    piezo siren
    ATMega328p
    Liquid Crystal Display
    red LED
    Future:
        CO sensor
        Smoke sensor
        Flame sensor
Features
    Ability to enable / disable the motion alarm (and all other alarms)
    When motion is detected
        - beep via piezo buzzer for 15 seconds
        - sound the alarm if a valid fingerprint isn't provided
        - disable the motion alarm, piezo, and siren if a valid fingerprint is 
          provided.
    Provide a mechanism for enabling the motion alarm
        - when the motion alarm is enabled, provide a 15 second grace period
    Provide a means by which to enroll fingerprints
        - the first fingerprint enrolled is the 'master' fingerprint
        - authentication via the master fingerprint is necessary to enroll 
          additional fingerprints
    To enroll the master fingerprint:
        - the first time the unit is powered up, solicite enrollment of the master
          fingerprint via the LCD and the fingerprint sensor.
    To enroll subsequent fingerprints:
        - turn on fingerprint enrollment mode
        - solicite the master fingerprint
        - if the master fingerprint is provided, allow the enrollment of
          additional fingerprints, up to a limit of n.
    Include three buttons, up, down, and select, to control a menu
    Display a menu on the LCD.  Certain parts of the menu only appear when the
        master fingerprint was used for authentication.
 
    Speification; navigation and behavior:
    Main Menu selections (navigate menu with up, down and select buttons when 
        !MENU_LOCKED)
        Show "Enable alarm" (only visible when fingerprint(s) are enrolled)
            MENU_LOCKED = true
            GRACE_PERIOD_TO_EXIT = true
            makes piezo beep once per second (for 30 seconds)
            show "arming siren, exit room" for 30 seconds
            after 30 seconds show "locked and armed"
            GRACE_PERIOD_TO_EXIT = false
            after 30 seconds and when motion is detected
                GRACE_PERIOD_TO_SOUND = true
                Show "Scan fingerprint to unlock."
                piezo beeps twice per second
                When an enrolled fingerprint is scanned
                    stop piezo beep
                    display "alarm disarmed" for 5 seconds
                    set MENU_LOCKED = true if KEY_SWITCH_LOCKED else MENU_LOCKED = false
                    Display "device locked" or main menu based on MENU_LOCKED
                    GRACE_PERIOD_TO_SOUND = false
                When an enrolled fingerprint isn't scanned within 30 seconds
                    GRACE_PERIOD_TO_SOUND = false
                    ALERTING is true
                    LED On
                    if SIREN_ENABLED, sound the siren, SIREN_SOUNDING = true
                    continue to solicite an enrolled fingerprint
                    After 5 minutes
                        stop sounding the siren if SIREN_SOUNDING
                        Turn off LED after 5 minutes (make it flash)
                        Set INTRUSION_DETECTED to true
                        Show "Intrusion detected"
                        set MENU_LOCKED = true if KEY_SWITCH_LOCKED else MENU_LOCKED = false
                        when a button is pressed return to main menu if !MENU_LOCKED
        Show "Enroll fingerprint" (only visible if a master is enrolled)
            1. show "scan fingerprint"
            if fingerprint is scanned correctly show "fingerprint scanned" 
                wait 5 seconds
                return to main menu
            else show "error scanning, try again" for 5 seconds, goto 1
            after 3 failed attempts
                show "fingerprint scanning failed" for 5 seconds
                return to main menu
        Show "Enroll master fingerprint" (if zero masters enrolled or when master
            authenticated)
            1. Show "Scan master fingerprint"
            if fingerprint is scanned correctly show "fingerprint scanned" 
                wait 5 seconds
                return to main menu
            else show "error scanning, try again" for 5 seconds goto 1
            after 3 failed attempts
                show "fingerprint scanning failed" for 5 seconds
                return to main menu
        Show "Disable Siren" if SIREN_ENABLED, show "Enable Siren" if !SIREN_ENABLED
            Set SIREN_ENABLED accordingly
            disables the alarm if it is or is not sounding
            Show "Siren Disabled" for 5 seconds
            goto the main menu    
    Actuate Key Switch
        set KEY_SWITCH_LOCKED to true or false 
        MENU_LOCKED = true if KEY_SWITCH_LOCKED
        MENU_LOCKED = false if !KEY_SWITCH_LOCKED
        Show "Device Locked" when key switch is locked, main menu when it is not.   

    To set the siren the user should select "Enable Alarm" AND actuate the key
    switch to the locked position.  The key switch must be unlocked to navigate
    the menu.  The siren can be disabled without the key switch, but the menu
    cannot be navigated without the key switch.

States:
    MASTER_ENROLLED         at least one master fingerprint has been enrolled
    KEY_SWITCH_LOCKED       locks menu navigation when ALERTING
    SIREN_SOUNDING          the siren is sounding.  Only happens when ALERTING.
    ALERTING                the motion detector was triggered and a fingerprint hasn't been supplied.  LED is on when ALERTING
    GRACE_PERIOD_TO_EXIT    
    GRACE_PERIOD_TO_SOUND
    SIREN_ENABLED           the siren will sound when ALERTING
    INTRUSION_DETECTED      ALERTING occurred, !FINGERPRINT_VALIDATED
    FINGERPRINT_VALIDATED   a valid fingerprint was supplied while ALERTING
    MENU_ENABLE_ALARM
    MENU_ENROLL_FINGER
    MENU_ENROLL_MASTER
    MENU_DISABLE_SIREN

Jonny Christmas List
Wave keyboard
cowboy hat
subscription to popular science and/or popular mechanics and/or Maker: magazine
