/*************************************************** 
Author: Dan Murphy
Date:   November 29, 2015

Description
-----------
This program is designed for use with a solderless
breadboard with the following components:
  ATMega 328p
  Fingerprint sensor
  LCD, 2 x 16
  motion detector
  A 12v siren
  Active piezo buzzer
  5V relay
  3 LEDs, green, yellow, red
  9v battery clip & battery
  2 6 AA battery holders & batteries
  16mhz crystal
  4 buttons
  2 capacitors
  12 resistors of various values
  1 diode
  2 terminal block adapters
  1 potentiometer, 10,000ohms
  2 capacitors and a voltage regulator
  
  The program allows the user to enroll fingerprints,
  delete all enrolled fingerprints, arm the siren, and
  disarm the siren when an enrolled fingerprint is 
  scanned.

  TODO: 04/21/2016 Add the ability to send a text when 
        the alarm sounds via ESP8266.
 ****************************************************/


#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>
#include <LiquidCrystal.h>

#define CALIBRATION_TIME 30
#define CALIBRATE_WAIT 250ul
#define LCD_SCREEN_WIDTH 16
#define NUM_OF_FINGERS_IN_READER 256
#define ERROR_CONDITION -1
#define GRACE_PERIOD_MILLIS 25000ul
#define DEBOUNCE_TIME 50ul
#define SLOW 50ul

LiquidCrystal lcd(12, 11, 10, 9, 8, 7);

// pin #4 is IN from sensor (GREEN wire)
// pin #5 is OUT from arduino  (WHITE wire)
SoftwareSerial fingerSensorSerial(4, 5);
Adafruit_Fingerprint finger = Adafruit_Fingerprint(&fingerSensorSerial);

int pirPin = 13; //the digital pin connected to the PIR sensor's output 
int ledPin = 6;  //the LED that flashes when the PIR senses motion. Also controls the beeper.
int sirenPin = 14;
int buttonUpPin = 2;
int buttonDownPin = 3;
int selectPin = 15;

#define POS_DISARM 0
#define POS_ARM    1
#define POS_ENROLL 2
#define POS_REMOVE 3

char menu[5][LCD_SCREEN_WIDTH+1] = {      "Disarm        ",
                                          "Arm           ",
                                          "Enroll        ",
                                          "Reset         ",
                                          "              "    };
volatile int selectPos = 0;
volatile boolean armed;
volatile unsigned long last_micros;
long debouncing_time = DEBOUNCE_TIME; // Debouncing Time in Milliseconds
int ledState = LOW;                   // ledState used to set the LED
unsigned long previousMillis = 0;     // will store last time LED was updated
const long interval = 1000;           // interval at which to blink (milliseconds)

void setup()  
{
  //Serial.begin(9600);
  pinMode(pirPin, INPUT);     // set the motion sensor pin for input
  pinMode(ledPin, OUTPUT);    // set the motion sensor LED & beeper for output
  pinMode(sirenPin, OUTPUT);  // main siren attached to analog 0 (we only need it for digital)
  digitalWrite(pirPin, LOW); 

  FlashAndBeep();             // beep when the unit is turned on.

  lcd.begin(LCD_SCREEN_WIDTH, 2);

  LCDPrintLine1And2("  Fingerprint   ","     Reader     ");
  delay(1000);

  selectPos = 0; // set the position of the menu's cursor

  // set the data rate for the sensor serial port
  finger.begin(57600);
  
  if (finger.verifyPassword()) {
    LCDPrintLine1Clear2("  Found reader  ");
    delay(1000);
  } else {
    LCDPrintLine1Clear2("   No reader    ");
    while (1);
  }
  // Setup the interrupts for the up and down buttons
  pinMode(buttonUpPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(buttonUpPin), LCDMenuUp, FALLING);
  pinMode(buttonDownPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(buttonDownPin), LCDMenuDown, FALLING);
  pinMode(selectPin, INPUT);
}

void loop()                     // run over and over again
{
// This block of code is for troubleshooting.
/*unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);
  }
*/
  armed = false;
  LCDShowMenu(selectPos);
  delay(25);
  
  if (digitalRead(selectPin) == LOW) {
  FlashAndBeep();
  switch(selectPos) {
      case  POS_DISARM:
        DisarmDevice();
        break;
      case  POS_ARM:
        ArmDevice();
        break;
      case  POS_ENROLL:
          if (EnrollFingerprint(GetNextFingerprintId()) != ERROR_CONDITION) {
            LCDPrintLine1Clear2("finger enrolled ");
            delay(1000);
          } else {
            LCDPrintLine2("error enrolling ");
            delay(1000);
          }
        break;
      case  POS_REMOVE:
        DeleteAllFingerprints();
        break;
    }
  }
}

void DisarmDevice() {
  digitalWrite(ledPin, LOW); 
  digitalWrite(sirenPin, LOW);
  LCDPrintLine1Clear2(" Alarm silenced ");
  delay(1000);
}

void FlashAndBeep() {
  digitalWrite(ledPin, HIGH); 
  delay(250);
  digitalWrite(ledPin, LOW); 
  
}

void ArmDevice() {
  unsigned long motionStartTime = 0;
  PIRCalibrate();
  armed = true;
  LCDPrintLine1Clear2("     Armed      ");
  while(armed) {
    if(digitalRead(pirPin) == HIGH) {
      motionStartTime = millis(); 
      digitalWrite(ledPin, HIGH);                    // this sounds warning tone
      PollForAuthorizedFinger(motionStartTime);
      DisarmDevice();
      armed = false;
    } 
    delay(SLOW);                                      // slow things down a bit
  }
}

void PollForAuthorizedFinger(unsigned long motionStartTime) {
  boolean alarmSounding = false;
  unsigned long presentTime = 0;
  while (getFingerprintID() != FINGERPRINT_OK) {
    presentTime = millis();
    if ((motionStartTime + GRACE_PERIOD_MILLIS) < presentTime) { 
                                                // this is 25 seconds at 16mhz
      if (!alarmSounding) {
        SoundSiren();
        alarmSounding = true;
        LCDPrintLine1And2("   Intrusion    ","    Detected    ");
        // This is where we could signal the ESP8266 to send a text message
        delay(500); // just to show the message above
      }
    }
    delay(SLOW);                                  //don't need to run this at full speed.
  }
}

void SoundSiren() {
  digitalWrite(sirenPin, HIGH);
}

int EnrollFingerprint(int id) {
  int p = -1;
  LCDPrintLine1And2("waiting for     ","valid finger    ");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      LCDPrintLine1Clear2("image taken     ");
      break;
    case FINGERPRINT_NOFINGER:
      LCDPrintLine1And2("waiting for     ","valid finger    ");
      delay(SLOW); // slow things down
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      LCDPrintLine1Clear2("comm error      ");
      break;
    case FINGERPRINT_IMAGEFAIL:
      LCDPrintLine1Clear2("imaging error   ");
      break;
    default:
      LCDPrintLine1Clear2("unknown error   ");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:                 // image converted
      break;
    case FINGERPRINT_IMAGEMESS:         // image too messy
      LCDPrintLine1Clear2("too messy       ");
      return ERROR_CONDITION;
    case FINGERPRINT_PACKETRECIEVEERR:  // communication error
      LCDPrintLine1Clear2("comm error      ");
      return ERROR_CONDITION;
    case FINGERPRINT_FEATUREFAIL:       // could not find finerprint features
      LCDPrintLine1Clear2("no features     ");
      return ERROR_CONDITION;
    case FINGERPRINT_INVALIDIMAGE:      // could not find fingerprint features
      LCDPrintLine1Clear2("no features     ");
      return ERROR_CONDITION;
    default:
      LCDPrintLine1Clear2("unknown error   ");
      return ERROR_CONDITION;           // unknown error
  }
  
  LCDPrintLine1Clear2("remove finger   ");  
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  p = -1;
  LCDPrintLine1Clear2("place finger    ");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      LCDPrintLine1Clear2("image taken     ");
      break;
    case FINGERPRINT_NOFINGER:
      LCDPrintLine1Clear2("place finger    ");
      delay(SLOW); // slow down a bit
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      LCDPrintLine1Clear2("comm error      ");
      break;
    case FINGERPRINT_IMAGEFAIL:
      LCDPrintLine1Clear2("imaging error   ");
      break;
    default:
      LCDPrintLine1Clear2("unknown error   ");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      LCDPrintLine1Clear2("image converted ");
      delay(1000);
      break;
    case FINGERPRINT_IMAGEMESS:
      LCDPrintLine1Clear2("too messy       ");
      return ERROR_CONDITION;
    case FINGERPRINT_PACKETRECIEVEERR:
      LCDPrintLine1Clear2("comm error      ");
      return ERROR_CONDITION;
    case FINGERPRINT_FEATUREFAIL:
      LCDPrintLine1Clear2("no features     ");
      return ERROR_CONDITION;
    case FINGERPRINT_INVALIDIMAGE:
      LCDPrintLine1Clear2("no features     ");
      return ERROR_CONDITION;
    default:
      LCDPrintLine1Clear2("unknown error   ");
      return ERROR_CONDITION;
  }
  
  // OK converted!
  LCDPrintLine1Clear2("creating model  ");
  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    LCDPrintLine1Clear2("prints matched  ");
    delay(1000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    LCDPrintLine1Clear2("comm error      ");
    return ERROR_CONDITION;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    LCDPrintLine1Clear2("no match        ");
    return ERROR_CONDITION;
  } else {
    LCDPrintLine1Clear2("unknown error   ");
    return ERROR_CONDITION;
  }   
  
  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    LCDPrintLine1Clear2("stored!         ");
    delay(1000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    LCDPrintLine1Clear2("comm error      ");
    return ERROR_CONDITION;
  } else if (p == FINGERPRINT_BADLOCATION) {
    LCDPrintLine1Clear2("bad location    ");
    return ERROR_CONDITION;
  } else if (p == FINGERPRINT_FLASHERR) {
    LCDPrintLine1Clear2("err write flash ");
    return ERROR_CONDITION;
  } else {
    LCDPrintLine1Clear2("unknown error   ");
    return ERROR_CONDITION;
  }   
}

void DeleteAllFingerprints() {
  for(int i = 0; i <= NUM_OF_FINGERS_IN_READER; i++) {
    if (FingerprintExists(i)) {
      // delete the fingerpint here
      deleteFingerprint(i);
    }
  }
  LCDPrintLine1Clear2("prints deleted  ");
  delay(1000);
}

void ShowFingerMatch() {
  LCDBlank();
  char idText[]="ID #: ";
  char str1[LCD_SCREEN_WIDTH];
  sprintf(str1,"%s%d",idText,finger.fingerID);
  char confidenceText[]="Confidence: ";
  char str2[LCD_SCREEN_WIDTH];
  sprintf(str2,"%s%d",confidenceText,finger.confidence);
  LCDPrintLine1And2( str1, str2);
  delay(1000);
}

uint8_t getFingerprintID() {
  uint8_t p = finger.getImage();
  switch (p) {
    case FINGERPRINT_OK:
      LCDPrintLine1Clear2("Image taken     ");
      break;
    case FINGERPRINT_NOFINGER:
      LCDPrintLine1And2("   Scan finger  ","to silence alarm");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      LCDPrintLine1Clear2("Comm error      ");
      return p;
    case FINGERPRINT_IMAGEFAIL:
      LCDPrintLine1Clear2("Imaging error   ");
      return p;
    default:
      LCDPrintLine1Clear2("Unknown error   ");
      return p;
  }

  // OK success!

  p = finger.image2Tz();
  switch (p) {
    case FINGERPRINT_OK:
      LCDPrintLine1Clear2("Image converted ");
      break;
    case FINGERPRINT_IMAGEMESS:
      LCDPrintLine1Clear2("Image too messy ");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      LCDPrintLine1Clear2("Comm error      ");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      LCDPrintLine1Clear2("No features     ");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      LCDPrintLine1Clear2("Invalid image   ");
      return p;
    default:
      LCDPrintLine1Clear2("Unknown error   ");
      return p;
  }
  
  // OK converted!
  p = finger.fingerFastSearch();
  if (p == FINGERPRINT_OK) {
    LCDPrintLine1Clear2("Found a match   ");        // found a match!
    ShowFingerMatch();
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    LCDPrintLine1Clear2("Comm error      ");
  } else if (p == FINGERPRINT_NOTFOUND) {
    LCDPrintLine1Clear2("No match found  ");
  } else {
    LCDPrintLine1Clear2("Unknown error   ");
  }   
  return p;
}

boolean FingerprintExists(uint16_t id)
{
 uint8_t p = finger.loadModel(id);
  switch (p) {
    case FINGERPRINT_OK:
      return true;
      break;
    default:
      return false;
  }
}

void PIRCalibrate() {
  LCDPrintLine1And2(" 30 seconds to  ","   exit room    ");
  for(int i = 0; i < CALIBRATION_TIME; i++){ 
    digitalWrite(ledPin, HIGH); 
    delay(CALIBRATE_WAIT); 
    digitalWrite(ledPin, LOW); 
    delay(CALIBRATE_WAIT); 
    digitalWrite(ledPin, HIGH); 
    delay(CALIBRATE_WAIT); 
    digitalWrite(ledPin, LOW); 
    delay(CALIBRATE_WAIT); 
  } 
}

int GetNextFingerprintId() {
  for(int i = 1; i <= NUM_OF_FINGERS_IN_READER; i++) { // not sure if you can save fingerprint # 0.
    if (!FingerprintExists(i)) {
      return(i);
    }
  }
  return ERROR_CONDITION;
}

uint8_t deleteFingerprint(uint8_t id) {
  uint8_t p = -1;
  
  p = finger.deleteModel(id);

  if (p == FINGERPRINT_OK) {
    LCDBlank();
    char idText[]="Deleted: ";
    char str1[LCD_SCREEN_WIDTH];
    sprintf(str1,"%s%d",idText,id);
    LCDPrintLine1Clear2(str1);
    Serial.println("Deleted!");
  } else {
    LCDBlank();
    char idText[]="Err deleting ";
    char str1[LCD_SCREEN_WIDTH];
    sprintf(str1,"%s%d",idText,id);
    LCDPrintLine1Clear2(str1);
  }    
}

void LCDPrintLine1And2(char* line1ToPrint, char* line2ToPrint) {
  LCDBlank();
  lcd.setCursor(0, 0);
  lcd.print(line1ToPrint);
  lcd.setCursor(0, 1);
  lcd.print(line2ToPrint);
}

void LCDPrintLine1Clear2(char* lineToPrint) {
      lcd.setCursor(0, 0);
      lcd.print(lineToPrint);
      LCDBlankLine2();
}

void LCDPrintLine2(char* lineToPrint) {
  LCDBlankLine2();
  lcd.setCursor(0,1);
  lcd.print(lineToPrint);
}

void LCDBlank() {
  LCDBlankLine1();
  LCDBlankLine2();
}

void LCDBlankLine1() {
    lcd.setCursor(0,0);
    lcd.print("                ");
}

void LCDBlankLine2() {
    lcd.setCursor(0,1);
    lcd.print("                ");
}

void LCDShowMenu(int position) {
  char line1[LCD_SCREEN_WIDTH+1] = "";
  strcat(line1,"->");
  strcat(line1,menu[position]);
  LCDPrintLine1And2(line1,menu[position+1]);
}

void LCDMenuUp() { 
  if (!armed) {
    if((long)(micros() - last_micros) >= debouncing_time * 1000) {
      if (selectPos > 0) {
        selectPos -= 1;
        LCDShowMenu(selectPos);
      }
      last_micros = micros();
    }
  }
}

void LCDMenuDown(){ 
  if (!armed) {
    if((long)(micros() - last_micros) >= debouncing_time * 1000) {
      if (selectPos < 3) {
        selectPos +=1;
        LCDShowMenu(selectPos);
      }
      last_micros = micros();
    }
  }
}

