/*
	Microchip 24LC256 I2C 256K EEPROM Arduino Library - Header File
	Made by Gonçalo Passos (more info @ http://diogopassos.pt) 2018
	BSD 2-Clause License
*/

#ifndef _24LC256_H
#define _24LC256_H

#include "Arduino.h"
#include "Wire.h"
//#include "WSWire.h"

#define WRITE_SUCESS 1
#define WRITE_ERROR -1
//#define E24LC256_SIZE 0X7D00
#define E24LC256_SIZE 0XFA00                                                    // the 24LC512 is 64,000 bytes


class E24LC256 
{
public:
	bool detect();
	E24LC256(byte address);
	int8_t writeByte(unsigned int address, byte data);
	int8_t writePage(unsigned int address,int size, byte *buffer);
	byte readByte(unsigned int address);
	int readPage(unsigned int address,int size, byte *buffer);
private:
	byte address;
	void ack_pooling();


};

#endif
