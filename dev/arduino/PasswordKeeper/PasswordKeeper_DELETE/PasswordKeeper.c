/*PasswordKeeper.ino

  Project Name: PasswordPump
  Version:      1.0
  Date:         2018/09/22 - 
  Device:       Arduino Pro Micro / ATmega32U4
  Language:     C
  Clock Speed:  16MHz
  Voltage:      5v
  Author:       Daniel J. Murphy

  Purpose
  =======
  - To manage usernames and passwords and to type them in via keyboard/USB.

  Features
  ========
  - Authenticate with master password
  - Search for accounts
  - Send account username and password as if typed in keyboard
  - Add account name, username, password (generated or not)
  - Delete account
  - Edit account name, username, password
  - Backup all accounts to a text file
  - Logout / de-authenticate, via menu or two double clicks
  - Factory reset, via menu or two triple clicks (when authenticated)
  - Factory reset after 10 failed login attempts
  - Configurable password display
  
  Components
  ==========
  - I2C LCD display 16x2, user interface
  - RGB LED
  - Rotary Encoder
  
  Known Defects
  =============  
  x passwords are not generating correctly
  - the LCD's backlight is not turning on, needed to operate in the dark
  - in the switch statement for EVENT_SINGLE_CLICK the case statements 
    are not in order. When they are in order it doesn't evaluate 
    correctly.
  x after add account the account isn't showing in find account
  
  TODO / Enhancements
  ===================
    x = implemented but not tested  
    - = unimplemented
  x Delete account (account name, username, password)
  - add encryption for the usernames and passwords (and accounts?)
  x add a feature that dumps all account names, usernames and passwords out
    through the keyboard (like to be inserted into an editor as a backup).
  - add a decoy password that executes a factory reset
  x have the unit factory reset after 10 failed attempts.  Store the failed
    attempt count in EEprom.
  - remove the space character and the escape (/ or \) from enterable characters?
  - have the unit automatically logout after a period of inactivity, this will
    require use of the timer.
  - decide if the encrypted pw will be saved to EEprom
  - incorporate the use of an i2c EEprom chip to expand available memory
  - learn how to set the lock bits
  - ground unused pins
  - reconsider the organization of the menus
  x add a feature whereby the unit factory resets after two triple clicks, even
    if not yet authenticated.
  x add a feature whereby the unit logsout after two double clicks.
  x decide if you should display the passwords.  Possibly make it configurable.
  - add the ability to pump a single tab or a single carrige return from the menu.
  - work on the workflow; which menu items are defaulted after which events.
  - mask passwords on input

  Warnings
  ========
  - Program memory is nearly full so be careful and watch out for flaky 
    behavior.  Once over 80% of global space you're in trouble. Currently
    at 74% of program storage space and 69% of dynamic memory (glabal variable
    space).

  Suggestions
  ===========
  - Best viewed in an editor w/ 160 columns, most comments are at column 80
  - Please submit defects you find so I can improve the quality of the program
    and learn more about embedded programming.

  Copyright
  =========
  - Copyright ©2018, Daniel Murphy <dan-murphy@comcast.net>
  
  Contributors
  ============
  smching: https://gist.github.com/smching/05261f11da11e0a5dc834f944afd5961 
  for EEPROMUtil.cpp.
  Source code has been pulled from all over the internet,
  it would be impossible for me to cite all contributors.
  Special thanks to Elliott Williams for his essential book
  "Make: AVR Programming", which is highly recommended. 

  License
  =======
  Daniel J. Murphy hereby disclaims all copyright interest in this
  program written by Daniel J. Murphy.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Libraries 
  =========
  - https://github.com/LennartHennigs/Button2
  - https://github.com/brianlow/Rotary
  - https://github.com/arduino-libraries/Keyboard
  - https://www.arduinolibraries.info/libraries/hd44780
  - https://www.arduino.cc/en/Reference/EEPROM
  
  The Program 
  ===========
  - Includes                                                                    */
#include <Wire.h>
#include <hd44780.h>                                                            // main hd44780 header
#include <hd44780ioClass/hd44780_I2Cexp.h>                                      // i2c expander i/o class header
#include "Button2.h";
#include <Rotary.h>
#include <avr/eeprom.h>
#include <EEPROM.h>
#include <Keyboard.h>

//- Macros
#define ClearBit(x,y) x &= ~y
#define SetBit(x,y) x |= y
#define ClearBitNo(x,y) x &= ~_BV(y)                                            
#define SetState(x) SetBit(machineState, x)

//- Defines
#define DEBUG                    0                                              // turns on (1) and off (0) output from debug* functions
#define BAUD_RATE                38400                                          // Baud rate for the Serial monitor, best for 16MHz

#define ROTARY_PIN1              9                                              // for AtMega32u4 / Arduino Pro Mini , was 8
#define ROTARY_PIN2              8                                              //   "                               , was 9
#define BUTTON_PIN               7                                              //   "                               , was 13
#define LED_PIN                  16
#define RED_PIN                  19                                             // Pin locations for the RGB LED, must be PWM capable
#define GREEN_PIN                20
#define BLUE_PIN                 21

#define ADC_READ_PIN             10                                             // we read the voltage from this floating pin to seed the random number generator, don't ground it!

#define LCD_SCREEN_WIDTH        16
#define LCD_SCREEN_HEIGHT       2

#define RED                     1                                               // TODO: should we use an enum here?  Must be a better way...
#define GREEN                   2
#define YELLOW                  3
#define BLUE                    4
#define PURPLE                  5
#define WHITE                   6
#define OFF                     7

#define EVENT_NONE              0
#define EVENT_SINGLE_CLICK      1
#define EVENT_DOUBLE_CLICK      2
#define EVENT_TRIPLE_CLICK      3
#define EVENT_LONG_CLICK        4
//#define EVENT_PRESSED           5
#define EVENT_RELEASED          6 
#define EVENT_ROTATE_CW         7
#define EVENT_ROTATE_CC         8
#define EVENT_SHOW_MAIN_MENU    9
#define EVENT_SHOW_ACCT_MENU    10
#define EVENT_SHOW_EDIT_MENU    11
#define EVENT_RESET             12
#define EVENT_LOGOUT            13

#define EEPROM_MEM_SIZE         0x03FF

#define STATE_ENTER_MASTER        0x000001                                      // 1       00000000.00000000.00000000.00000001
#define STATE_CHANGE_MASTER       0x000002                                      // 2       00000000.00000000.00000000.00000010
#define STATE_RESET               0x000004                                      // 4       00000000.00000000.00000000.00000100
#define STATE_SHOW_ALPHA          0x000008                                      // 8       00000000.00000000.00000000.00001000
#define STATE_SHOW_MAIN_MENU      0x000010                                      // 16      00000000.00000000.00000000.00010000
#define STATE_FIND_ACCOUNT        0x000020                                      // 32      00000000.00000000.00000000.00100000
#define STATE_IDLE                0x000040                                      // 64      00000000.00000000.00000000.01000000
#define STATE_ENTER_USERNAME      0x000080                                      // 128     00000000.00000000.00000000.10000000
#define STATE_ENTER_PASSWORD      0x000100                                      // 256     00000000.00000000.00000001.00000000
#define STATE_LOGOUT              0x000200                                      // 512     00000000.00000000.00000010.00000000
#define STATE_EDIT_CREDS_MENU     0x000400                                      // 1024    00000000.00000000.00000100.00000000
#define STATE_ENTER_ACCOUNT       0x000800                                      // 2048    00000000.00000000.00001000.00000000
#define STATE_SEND_CREDS_MENU     0x001000                                      // 4096    00000000.00000000.00010000.00000000
#define STATE_SEND_USER_AND_PASS  0x002000                                      // 8192    00000000.00000000.00100000.00000000
#define STATE_SEND_PASSWORD       0x004000                                      // 16384   00000000.00000000.01000000.00000000
#define STATE_SEND_USERNAME       0x008000                                      // 32768   00000000.00000000.10000000.00000000
#define STATE_DELETE_ACCT         0x010000                                      // 65536   00000000.00000001.00000000.00000000
#define STATE_ADD_ACCOUNT         0x020000                                      // 131072  00000000.00000010.00000000.00000000
//#define STATE_AUTHENTICATED     0x040000                                      // 262144  00000000.00000100.00000000.00000000
//#define                         0x080000                                      // 524288  00000000.00001000.00000000.00000000
//#define                         0x100000                                      // 1048576 00000000.00010000.00000000.00000000
//#define                         0x200000                                      // 1048576 00000000.00100000.00000000.00000000
//#define                         0x400000                                      // 2097152 00000000.01000000.00000000.00000000

#define DIRECTION_RIGHT         1
#define DIRECTION_LEFT          255

#define MAX_IDLE_TIME           900000                                          // fifteen minutes

#define LCD_I2C_SLAVE_ADDR      0x27
//#define LCD_I2C_SLAVE_ADDR    0x3F

#define PASSWORD_SIZE           16
#define USERNAME_SIZE           32
#define ACCOUNT_SIZE            8

#define CREDS_ACCOMIDATED       5                                               //18

char accountsArray[CREDS_ACCOMIDATED][ACCOUNT_SIZE];
char username[USERNAME_SIZE];
char password[PASSWORD_SIZE];


#define MENU_SIZE               6

#define MAIN_MENU_NUMBER        0
#define MAIN_MENU_ELEMENTS      6
                                                                            
char *mainMenu[] =       {                       "Enter Master PW",             // menu picks appear only on the top line
                                                 "Find Account",                // after an account is found send user sendMenu menu
                                                 "Edit Account",                // sends user to enterMenu menu
                                                 "Logout",
                                                 "Set Show PW O   ",
                                                 "Reset"            };
#define FIND_ACCT_POS           1
#define SET_SHOW_PW_POS         4

int menuNumber = MAIN_MENU_NUMBER;
int elements = MAIN_MENU_ELEMENTS;
char *currentMenu[MENU_SIZE];

#define SEND_MENU_NUMBER        1
#define SEND_MENU_ELEMENTS      5
char *sendMenu[] =       {                       "Send User & Pass",            // menu picks appear only on the top line
                                                 "Send Password",  
                                                 "Send Username",
                                                 "Delete Account",
                                                 "Backup All",
                                                 ""                 };

#define ENTER_MENU_NUMBER       2
#define ENTER_MENU_ELEMENTS     5
char *enterMenu[] =       {                      "Account Name",                // menu picks appear only on the top line
                                                 "Edit Username",  
                                                 "Edit Password",
                                                 "GeneratePassword",
                                                 "Add Account",
                                                 ""                 };

#define LEN_ALL_CHARS           93
char allChars[LEN_ALL_CHARS] = "0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz -=~!@#$%^&*()_+[]{}|;':,.<>?/'";

                                                                                // TODO: disable interrupts when reading/writing EEPROM

#define TAB_KEY                 0x09                                            // TAB key is ascii 0x09

#define BASE_ADDR_ACCT          0x0000
#define BASE_ADDR_USER          0x0090                                          // TODO: calculate this
#define BASE_ADDR_PASS          0x02D0                                          // TODO: calculate this

int getAddrAcct(int pos) {
  return BASE_ADDR_ACCT + (pos * ACCOUNT_SIZE);
}

int getAddrUser(int pos) {
  return BASE_ADDR_USER + (pos * USERNAME_SIZE);
}

int getAddrPass(int pos) {
  return BASE_ADDR_PASS + (pos * PASSWORD_SIZE);
}

//#define MASTER_PASSWORD_SIZE    EEPROM_MEM_SIZE - (getAddrPass(CREDS_ACCOMIDATED - 1) + PASSWORD_SIZE)  // 15
#define MASTER_PASSWORD_SIZE      16
char masterPassword[MASTER_PASSWORD_SIZE];
int masterPasswordAddr = getAddrPass(CREDS_ACCOMIDATED - 1) + PASSWORD_SIZE;  

unsigned long lastActivityTime;
byte loginFailures;
int loginFailuresAddr = getAddrPass(CREDS_ACCOMIDATED - 1) + PASSWORD_SIZE + MASTER_PASSWORD_SIZE;
byte showPasswordsFlag;
int showPasswordsFlagAddr = loginFailuresAddr + sizeof(loginFailures);
#define MAX_LOGIN_FAILURES        10                                            // "Factory Reset" after MAX_LOGIN_FAILURES attempts to login

//- Volatile variables.  These need to be double checked.

volatile byte event = EVENT_NONE;
volatile byte lastEvent = EVENT_NONE;
volatile long int machineState;
volatile int position = 0;
volatile int enterPosition = 0;
volatile int acctPosition = 0;
volatile int accountCount = 0;
volatile byte direction;
volatile byte color = RED;
volatile boolean authenticated = false;

//- Object setup

Rotary rotaryEncoder = Rotary(ROTARY_PIN1, ROTARY_PIN2);
Button2 encoderButton = Button2(BUTTON_PIN);
hd44780_I2Cexp lcd;                                                             // declare lcd object: auto locate & auto config expander chip

//- Main Program Control

void setup() {
  int status;
  Serial.begin(BAUD_RATE);
  while(!Serial);
  delay(50);
  debug(F("Password Pump"));

  pinMode(RED_PIN, OUTPUT);                                                     // RGB LED
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);

  pinMode(LED_PIN, OUTPUT);

  pinMode(ADC_READ_PIN, INPUT);                                                 // this pin will float in a high impedance/Hi-Z state and it's voltage
                                                                                // will be read with every spin to seed the random number generator.
  randomSeed(analogRead(ADC_READ_PIN));                                         // do not ground this pin; use this or randomSeed(millis());

  encoderButton.setClickHandler(buttonClickHandler);                            // fires after ReleasedHandler on short click
  //encoderButton.setReleasedHandler(buttonReleasedHandler);                    // fires when button is released
  //encoderButton.setPressedHandler(buttonPressedHandler);                      // fires when button is pressed
  //encoderButton.setTapHandler(buttonTapHandler);                                // not sure when this fires, but we turn it into EVENT_SINGLE_CLICK.
  encoderButton.setLongClickHandler(buttonLongClickHandler);                    // fires when button is pressed and held
  encoderButton.setDoubleClickHandler(buttonDoubleClickHandler);                // fires when button is double clicked, do this twice in a row to logout.
  encoderButton.setTripleClickHandler(buttonTripleClickHandler);                // fires when button is triple clicked, do this twice in a row to reset.

  status = lcd.begin(LCD_SCREEN_WIDTH, LCD_SCREEN_HEIGHT);
  if(status) // non zero status means it was unsuccessful
  {
    debugMetric("Fatal LCD error: ", status);
    status = -status;                                                           // convert negative status value to positive number
                                                                                // begin() failed so blink error code using the onboard LED if possible
    hd44780::fatalError(status);                                                // does not return
  }

  lcd.backlight();                                                              // Turn on the blacklight, is not working
  lcd.setCursor(0, 0);
  lcd.print(F("  PasswordPump  "));
  delay(1000);
  PCICR |= (1 << PCIE0);                                                        // Setup interrupts for rotary encoder
  PCMSK0 |= (1 << PCINT4) | (1 << PCINT5);                                      //
  sei();                                                                        // Turn on global interrupts

  
  int arraySize = 0;
  for (int i = 0; i < MENU_SIZE; i++) {
    arraySize += sizeof(mainMenu[i]);  
  }
  memcpy(currentMenu, mainMenu, arraySize);
  elements = MAIN_MENU_ELEMENTS;
  position = 0;
  event = EVENT_SHOW_MAIN_MENU;
  authenticated = false;
  setBlue();                                                                    // not yet authenticated

  loadAccounts();                                                               // Load all the accounts into the accountArray.
  loginFailures = getLoginFailures();
  if (loginFailures = 0xFF) {
    loginFailures = 0;
    writeLoginFailures();                                                       // write login failures back to EEprom on first use
  }

  showPasswordsFlag = getShowPasswordsFlag();                                   // setup the show passwords flag and menu item.
  if (showPasswordsFlag = 0xFF) {                                               // if this it the first time we're using the unit
    showPasswordsFlag = 0x01;                                                   // set the show password flag to ON
    writeShowPasswordsFlag();                                                   // and write it to EEprom.
  }
  setShowPasswordsMenuItem();
  
  lastActivityTime = millis();
}

void loop() {
  encoderButton.loop();                                                         // polling for button press TODO: replace w/ interrupt
  ProcessEvent();  
}

void ProcessEvent()                                                             // processes events
{ //debug(F("ProcessEvent()"));
/*  if (event == EVENT_NONE) {                                                  // this block of code was causing unpredictable behavior
    return;
  } else {
    if (authenticated) {
      if (lastEvent == EVENT_TRIPLE_CLICK &&
          event     == EVENT_TRIPLE_CLICK ) {
          event = EVENT_RESET;                                                  // if triple click has been entered twice, factory reset, only if authenticated
      } else if (lastEvent == EVENT_DOUBLE_CLICK &&
                 event     == EVENT_DOUBLE_CLICK ) {
          event = EVENT_LOGOUT;                                                 // if double click has been entered twice, logout
      }
    }
    lastEvent = event;
  }
*/  
  if (event != EVENT_NONE) {
    lastActivityTime = millis();
  } else {                                                                      // event == EVENT_NONE
    if (millis() < (lastActivityTime + MAX_IDLE_TIME)) {                        // check to see if the device has been idle for MAX_IDLE_TIME milliseconds
      return;
    } else {
      event = EVENT_LOGOUT;                                                     // ... and if so, logout.
    }
  }
  
  if (event == EVENT_ROTATE_CW) {                                               // scroll forward through something depending on state...
    //debug("event == EVENT_ROTATE_CW");
    //debugMetric("state", machineState);
    //debugMetric("position",position);
    if (STATE_SHOW_MAIN_MENU == (machineState & STATE_SHOW_MAIN_MENU)) {
      if (position < MAIN_MENU_ELEMENTS - 1) {
        position++;
        LCDMenuDown(currentMenu);
        //debugMetric("Position", position);
      }
    } else if ((STATE_ENTER_MASTER   == (machineState & STATE_ENTER_MASTER  )) ||
               (STATE_ENTER_ACCOUNT  == (machineState & STATE_ENTER_ACCOUNT )) ||
               (STATE_ENTER_USERNAME == (machineState & STATE_ENTER_USERNAME)) ||
               (STATE_ENTER_PASSWORD == (machineState & STATE_ENTER_PASSWORD))) {
      if (position < LEN_ALL_CHARS) {
        position++;
      }
      char charToPrint = allChars[position];
      LCDShowChar(charToPrint, enterPosition);
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)) {
      if (position < SEND_MENU_ELEMENTS - 1) {
        position++;
        LCDMenuDown(currentMenu);
        //debugMetric("Position", position);
      }
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      //debug("EVENT_ROTATE_CW, STATE_EDIT_CREDS_MENU");
      if (position < ENTER_MENU_ELEMENTS - 1) {
        position++;
        LCDMenuDown(currentMenu);
        //debugMetric("acctPosition",acctPosition);
        switch(position) {
          case 0:
            LCDPrintLine2(accountsArray[acctPosition]);
            debug(accountsArray[acctPosition]);
            break;
          case 1:
            readUserFromEEProm(acctPosition,username);
            LCDPrintLine2(username);
            debug(username);
            break;
          case 2:
            readPassFromEEProm(acctPosition,password);
            if (showPasswordsFlag) {
              LCDPrintLine2(password);
            } else {
              LCDBlankLine2();
            }
            debug(password);
            break;
        }
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {
      if (position < (accountCount - 1)) {
        position++;
        acctPosition = position;
        LCDPrintLine2(accountsArray[acctPosition]);                             // This is where we will scroll through account names on LCD line 2.
        //debugMetric("acctPosition",acctPosition);
      } else {
        position = accountCount - 1;                                            // maximum for position in this state is accountCount
        acctPosition = position;
        LCDPrintLine2(accountsArray[acctPosition]);                             // This is where we will scroll through account names on LCD line 2.
        //debugMetric("acctPosition",acctPosition);
      }
    }
    event = EVENT_NONE;

  } else if (event == EVENT_ROTATE_CC) {                                        // scroll backward through something depending on state...
    //debug("event == EVENT_ROTATE_CW");
    //debugMetric("state", machineState);
    //debugMetric("position",position);
    if (STATE_SHOW_MAIN_MENU == (machineState & STATE_SHOW_MAIN_MENU)) {
      if (position > 0) {
        position--;
        LCDMenuUp(currentMenu);
        //debugMetric("Position", position);
      }
    } else if ((STATE_ENTER_MASTER   == (machineState & STATE_ENTER_MASTER  )) ||
               (STATE_ENTER_ACCOUNT  == (machineState & STATE_ENTER_ACCOUNT )) ||
               (STATE_ENTER_USERNAME == (machineState & STATE_ENTER_USERNAME)) ||
               (STATE_ENTER_PASSWORD == (machineState & STATE_ENTER_PASSWORD))) {
      if (position > 0) {
        position--;
      }
      char charToPrint = allChars[position];
      LCDShowChar(charToPrint, enterPosition);
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)) {
      if (position > 0) {
        position--;
        LCDMenuUp(currentMenu);
        //debugMetric("Position", position);
      }
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      if (position > 0) {
        position--;
        LCDMenuUp(currentMenu);
        switch(position) {
          case 0:
            LCDPrintLine2(accountsArray[acctPosition]);
            break;
          case 1:
            readUserFromEEProm(acctPosition,username);
            LCDPrintLine2(username);
            break;
          case 2:
            readPassFromEEProm(acctPosition,password);
            if (showPasswordsFlag) {
              LCDPrintLine2(password);
            } else {
              LCDBlankLine2();
            }
            break;
        }
        //debugMetric("Position", position);
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {
      if (position > 0) {
        position--;
        acctPosition = position;
        LCDPrintLine2(accountsArray[acctPosition]);                             // This is where we will scroll through account names on LCD line 2.
        //debugMetric("acctPosition",acctPosition);
      } else {
        position = 0;
        acctPosition = position;
        LCDPrintLine2(accountsArray[acctPosition]);                             // This is where we will scroll through account names on LCD line 2.
        //debugMetric("acctPosition",acctPosition);
      }
    }
    event = EVENT_NONE;

  } else if (event == EVENT_SHOW_MAIN_MENU) {                                   // show the main menu
    menuNumber = MAIN_MENU_NUMBER;
    int arraySize = 0;
    for (int i = 0; i < MENU_SIZE; i++) {
      arraySize += sizeof(mainMenu[i]);  
    }
    memcpy(currentMenu, mainMenu, arraySize);
    elements = MAIN_MENU_ELEMENTS;
    machineState = STATE_SHOW_MAIN_MENU;
    position = 0;
    LCDShowMenu(position, currentMenu);
    LCDPrintLine2(accountsArray[acctPosition]);
    event = EVENT_NONE;

  } else if (event == EVENT_SHOW_EDIT_MENU) {                                   // show the main menu
    menuNumber = ENTER_MENU_NUMBER;
    int arraySize = 0;
    for (int i = 0; i < MENU_SIZE; i++) {
      arraySize += sizeof(enterMenu[i]);  
    }
    memcpy(currentMenu, enterMenu, arraySize);
    elements = ENTER_MENU_ELEMENTS;
    machineState = STATE_EDIT_CREDS_MENU;
    position = 0;
    LCDShowMenu(position, currentMenu);
    LCDPrintLine2(accountsArray[acctPosition]);
    event = EVENT_NONE;
    //debugMetric("machineState",machineState);

  } else if (event == EVENT_LONG_CLICK) {                                       // jump up / back to previous menu 
    //debug("EVENT_LONG_CLICK");
    if (STATE_ENTER_MASTER == (machineState & STATE_ENTER_MASTER)){
      authenticated = authenticateMaster(masterPassword);
      if (authenticated) {
        //debug(F("authenticated"));
        position = FIND_ACCT_POS;
        machineState = STATE_SHOW_MAIN_MENU;
        LCDShowMenu(position, currentMenu);
        LCDPrintLine2("Authenticated");
        event = EVENT_NONE;
      } else {
        if (loginFailures > MAX_LOGIN_FAILURES) {
          event = EVENT_RESET;                                                  // factory reset after 10 failed attempts to enter master password!
        } else {  
          position = 0;
          machineState = STATE_SHOW_MAIN_MENU;
          LCDShowMenu(position, currentMenu);
          char buffer[4];
          itoa(loginFailures, buffer, 10);                                      // convert login failures to a string and put it in buffer.
          lcd.setCursor(0,1);
          lcd.print(buffer);
          lcd.setCursor(5,1);
          lcd.print(F(" failure(s)"));
          //debug("authentication failed");
          event = EVENT_NONE;
        }
      }
    } else if (STATE_ENTER_ACCOUNT == (machineState & STATE_ENTER_ACCOUNT)) {
      writeAcctToEEProm(accountsArray[acctPosition], acctPosition);             // write the account to EEProm
      if (STATE_ADD_ACCOUNT == machineState & STATE_ADD_ACCOUNT) {
        accountCount++;
      }
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_ENTER_USERNAME == (machineState & STATE_ENTER_USERNAME)) {
      writeUserToEEProm(username, acctPosition);                                // write the username to EEProm
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_ENTER_PASSWORD == (machineState & STATE_ENTER_PASSWORD)) {
      writePassToEEProm(password, acctPosition);                                // write the password to EEProm
      LCDBlankLine2();                                                          // clear the password off of the LCD  TODO: check showPasswordFlag?
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      event = EVENT_SHOW_MAIN_MENU;
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)){
      LCDBlankLine2();
      event = EVENT_SHOW_MAIN_MENU;
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)){       // long click after selecting an account
      //acctPosition = position;                                                 // acctPosition == the selected account, use to edit
      position = 2;                                                              // edit menu
      machineState = STATE_SHOW_MAIN_MENU;
      LCDShowMenu(position, currentMenu);
      LCDPrintLine2(accountsArray[acctPosition]);                                // this might be redundant
    }

//  } else if (event == EVENT_PRESSED) {
//    event = EVENT_NONE;

  } else if (event == EVENT_SINGLE_CLICK) {
    debugMetric("Position in EVENT_SINGLE_CLICK",position);
    debugMetric("state in EVENT_SINGLE_CLICK",machineState);
    if (STATE_SHOW_MAIN_MENU == (machineState & STATE_SHOW_MAIN_MENU)) {
      debug(F("inside if (STATE_SHOW_MAIN_MENU == (machineState & STATE_SHOW_MAIN_MENU))"));
      switch(position) {
        case 0:                                                                  // Enter master password
          debug(F("EVENT_SINGLE_CLICK, state SHOW_MAIN_MENU, case 0"));
          machineState = STATE_ENTER_MASTER;
          position = 0;
          enterPosition = 0;
          char charToPrint[2];
          charToPrint[0] = allChars[enterPosition];
          charToPrint[1] = '\0';
          LCDPrintLine2(charToPrint);
          event = EVENT_NONE;
          break;
        case 1:                                                                  // Find account
          debug(F("EVENT_SINGLE_CLICK, state SHOW_MAIN_MENU, case 1"));
          machineState = STATE_FIND_ACCOUNT;
          position = 0;
          LCDPrintLine2(accountsArray[position]);
          event = EVENT_NONE;
          break;
        case 3:                                                                  // Logout  DEFECT: why is this being skipped over
          debug(F("EVENT_SINGLE_CLICK, state SHOW_MAIN_MENU, case 3"));
          //machineState = STATE_LOGOUT;                                         // TODO: is this necessary
          position = 0;
          event = EVENT_LOGOUT;                 
          break;
        case 4:                                                                
          debug(F("EVENT_SINGLE_CLICK, state SHOW_MAIN_MENU, case 4"));
          showPasswordsFlag = !showPasswordsFlag;
          setShowPasswordsMenuItem();
          LCDPrintLine1(mainMenu[SET_SHOW_PW_POS]);
          event = EVENT_NONE;
          break;
        case 5:                                                                  // Reset
          debug(F("EVENT_SINGLE_CLICK, state SHOW_MAIN_MENU, case 5"));
          //machineState = STATE_RESET;                                          // TODO: is this necessary
          position = 0;
          event = EVENT_RESET;                                                          
          break;
        case 2:                                                                  // Show the enter account menu
          debug(F("EVENT_SINGLE_CLICK, state SHOW_MAIN_MENU, case 2"));
          menuNumber = ENTER_MENU_NUMBER;
          elements = ENTER_MENU_ELEMENTS;
          int arraySize = 0;
          for (int i = 0; i < MENU_SIZE; i++) {
            arraySize += sizeof(enterMenu[i]);  
          }
          memcpy(currentMenu, enterMenu, arraySize);
          elements = ENTER_MENU_ELEMENTS;
          position = 0;
          machineState = STATE_EDIT_CREDS_MENU;
          //debugMetric("STATE_EDIT_CREDS_MENU",STATE_EDIT_CREDS_MENU);
          //debugMetric("machineState1",machineState);
          LCDShowMenu(position, currentMenu);
          LCDPrintLine2(accountsArray[acctPosition]);
          //debug("after LCDShowMenu(position, currentMenu) for ENTER MENU");
          event = EVENT_NONE;
          //debugMetric("machineState2",machineState);
          break;
        default:
          debugMetric("Position in default",position);
          break;
      }
      debugMetric("Position after default",position);
      if (event == EVENT_SINGLE_CLICK) {                                        // stop the infinite loop of single clicks
        event = EVENT_NONE;
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {     // Go to the send menu
    // go back to main menu and add send to main menu instead?
        acctPosition = position;
        menuNumber = SEND_MENU_NUMBER;
        elements = SEND_MENU_ELEMENTS;
        int arraySize = 0;
        for (int i = 0; i < MENU_SIZE; i++) {
          arraySize += sizeof(sendMenu[i]);  
        }
        memcpy(currentMenu, sendMenu, arraySize);
        elements = SEND_MENU_ELEMENTS;
        position = 0;
        machineState = STATE_SEND_CREDS_MENU;
        LCDShowMenu(position, currentMenu);
        LCDPrintLine2(accountsArray[acctPosition]);
        event = EVENT_NONE;
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      enterPosition = 0;
      char charToPrint[2];
      charToPrint[0] = allChars[enterPosition];
      charToPrint[1] = '\0';                                                    // TODO: this shouldn't be necessary
      debugMetric("STATE_EDIT_CREDS_MENU, position",position);
      switch(position) {
         case 0:                                                                // Enter account name
            machineState = STATE_ENTER_ACCOUNT; 
            position = 0;
            LCDPrintLine2(charToPrint);
            event = EVENT_NONE;
            break; 
         case 1:                                                                // Enter username     
            machineState = STATE_ENTER_USERNAME;
            position = 0;
            LCDPrintLine2(charToPrint);
            event = EVENT_NONE;
            break;
         case 2:                                                                // Enter Password   
            machineState = STATE_ENTER_PASSWORD;
            position = 0;
            LCDPrintLine2(charToPrint);
            event = EVENT_NONE;
            break;
         case 3:                                                                // Automatic UUID enter password 
            debug(F("case 3 in EVENT_SINGLE_CLICK state STATE_EDIT_CREDS_MENU"));
            machineState = STATE_ENTER_PASSWORD;                                // pretend we're entering the password
            setUUID(password);                                                  // put a UUID in the password char array
            debug(password);
            event = EVENT_LONG_CLICK;                                           // and trigger long click to write the password to eeprom.
            break;
         case 4:                                                                // Add account
            machineState |= STATE_ADD_ACCOUNT;
            acctPosition = getNextFreeAcctPos();
            strcpy(accountsArray[acctPosition],"Add Account");
            LCDPrintLine2(accountsArray[acctPosition]);
            event = EVENT_NONE;
      }
    } else if (STATE_ENTER_ACCOUNT == (machineState & STATE_ENTER_ACCOUNT)) {
      accountsArray[acctPosition][enterPosition] = allChars[position];
      accountsArray[acctPosition][enterPosition + 1] = '\0';                    // push the null terminator out ahead of the last char in the string
      enterPosition++;
      lcd.setCursor(enterPosition,1);
      lcd.print(allChars[position]);
      //debugNoLF("account so far: ");debug(accountsArray[acctPosition]);
      event = EVENT_NONE;
    } else if (STATE_ENTER_USERNAME == (machineState & STATE_ENTER_USERNAME)) {
      username[enterPosition] = allChars[position];
      username[enterPosition + 1] = '\0';                                       // push the null terminator out ahead of the last char in the string
      enterPosition++;
      lcd.setCursor(enterPosition,1);
      lcd.print(allChars[position]);
      //debugNoLF("username so far: ");debug(username);
      event = EVENT_NONE;
    } else if (STATE_ENTER_PASSWORD == (machineState & STATE_ENTER_PASSWORD)) {
      password[enterPosition] = allChars[position];
      password[enterPosition + 1] = '\0';                                       // push the null terminator out ahead of the last char in the string
      enterPosition++;
      if (!showPasswordsFlag) {                                                 // mask the password being entered if showPasswordsFlag is OFF
        lcd.setCursor(enterPosition - 1, 1);  
        lcd.print('*');
      }
      lcd.setCursor(enterPosition,1);
      lcd.print(allChars[position]);
      //debugNoLF("password so far: ");debug(password);
      event = EVENT_NONE;
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)) {
      switch(position) {
         case 0:                                                                
            //machineState = STATE_SEND_USER_AND_PASS;                          // Are these states necessary???
            sendUsernameAndPassword();                                          // SEND THE USERNAME AND PASSWORD
            break; 
         case 1:                                                                // Find account
            //machineState = STATE_SEND_PASSWORD;
            sendPassword();                                                     // SEND THE USERNAME
            break;
         case 2:                                                                // Enter account
            //machineState = STATE_SEND_USERNAME;
            sendUsername();                                                     // SEND THE PASSWORD
            break;
         case 3:                                                                // Delete account
            deleteAccount(acctPosition);
            if (acctPosition <= (accountCount - 1)) {                           // if we deleted the last account
              accountCount = acctPosition;                                      // adjust accountCount
            }
            acctPosition = 0;
            LCDPrintLine2("Account deleted");
            //machineState = STATE_DELETE_ACCT;
            break;
         case 4:                                                                // Send all
            sendAll();
            break;
      }
      event = EVENT_NONE;
    } else if (STATE_ENTER_MASTER == (machineState & STATE_ENTER_MASTER)) {
      masterPassword[enterPosition] = allChars[position];
      masterPassword[enterPosition + 1] = '\0';                                 // push the null terminator out ahead of the last char in the string
      enterPosition++;
      lcd.setCursor(enterPosition,1);
      lcd.print(allChars[position]);
      //debugNoLF("masterPassword so far: ");debug(masterPassword);
      event = EVENT_NONE;
    }

  } else if (event == EVENT_RESET) {
    if (authenticated || (loginFailures > MAX_LOGIN_FAILURES)) {                // TODO: enter master password here to authorize creds reset
      LCDPrintLine2("Erasing creds");
      char emptyPassword[PASSWORD_SIZE];
      for (int i = 0; i < PASSWORD_SIZE; i++) {
        emptyPassword[i] = '\0';                                                // to completely overwrite the password in EEProm
      }
      char allBitsOnArray[1];
      allBitsOnArray[0] = 0xFF;                                                 // this makes the account name empty/available
      allBitsOnArray[1] = '\0';
      for (int pos = 0; pos < CREDS_ACCOMIDATED; pos++) {
        writeAllToEEProm(allBitsOnArray, "\0",emptyPassword, pos);              // first char of username replaced with null terminator
      }
      accountCount = 0;
      acctPosition = 0;
      writeMasterPassToEEProm(allBitsOnArray);                                  // first char of master password set to 255 (all 1s) to mark it as empty
      strcpy(masterPassword,"\0");                                              // set master password to null terminator in memory
      authenticated = false;                                                    // we're no longer authenticated, we need to re-enter the master password
      loginFailures = 0;
      writeLoginFailures();
      showPasswordsFlag = 0;
      writeShowPasswordsFlag();
      setShowPasswordsMenuItem();
      size_t n = sizeof(accountsArray)/sizeof(accountsArray[0]);                // get the number of elements in the accounts array
      for (int i = 0; i < n; i++){                                              // empty out the accountsArray
        for (int j = 0; j < ACCOUNT_SIZE; j++) {
          accountsArray[i][j] = '\0';      
        }
      }
      //loadAccounts();
      setBlue();  
      LCDPrintLine2("All creds erased");
      event = EVENT_NONE;
    } else { 
      LCDPrintLine2(" Not logged in");
      event = EVENT_NONE;
    }

  } else if (event == EVENT_LOGOUT) {                                           // TODO: you need to be logged in to logout, check for authentication here
    if(authenticated) {    
      //debug("EVENT_LOGOUT");
      LCDPrintLine2("Logged out");
      position = 0;
      strcpy(masterPassword,"\0");                                              // set master password to null terminator in memory
      authenticated = false;                                                    // we're no longer authenticated, we need to re-enter the master password
      loginFailures = 0;
      writeLoginFailures();
      setBlue();        
      event = EVENT_SHOW_MAIN_MENU;
    } else {
      LCDPrintLine2("Not logged in");
      event = EVENT_SHOW_MAIN_MENU;
    }
  }
}

//- Interrupt Service Routines
ISR(PCINT0_vect) {                                                              // Interrupt service routine for rotary encoder
  unsigned char result = rotaryEncoder.process();   
  if (result == DIR_NONE) {                                                     // this actually fires many times per 'click' 
    // debug("ISR fired: none");
    // event = EVENT_NONE;
    // do nothing
  }
  else if (result == DIR_CW) {                                                  // rotated encoder clockwise
    event = EVENT_ROTATE_CW;
    //debug("ISR fired: rotate clockwise");
    //ProcessEvent();
  }
  else if (result == DIR_CCW) {                                                 // rotated encoder counter clockwise
    event = EVENT_ROTATE_CC;
    //debug("ISR fired: rotate counter clockwise");
    //ProcessEvent();
  }
}

//- Button

void buttonClickHandler(Button2& btn) {
  //debugMetric("buttonClick",position);
  event = EVENT_SINGLE_CLICK;
}

/*
 
 void buttonPressedHandler(Button2& btn) {
  //debug("buttonPressed");
  event = EVENT_PRESSED;
}
*/

void buttonReleasedHandler(Button2& btn) {
  //debugMetric("buttonReleased",btn.wasPressedFor());
  event = EVENT_RELEASED;
}

/*
void changed(Button2& btn) {
    Serial.println("changed");
}
*/

void buttonLongClickHandler(Button2& btn) {
  //debug("buttonLongClick reset pos");
  event = EVENT_LONG_CLICK;
  position = 0;
}

void buttonDoubleClickHandler(Button2& btn) {
  //debug("buttonDoubleClick");
  event = EVENT_DOUBLE_CLICK;
}

void buttonTripleClickHandler(Button2& btn) {
  //debug("buttonTripleClick");
  event = EVENT_TRIPLE_CLICK;
}


/*
  void buttonTapHandler(Button2& btn) {
  debugMetric("buttonTap",position);
  event = EVENT_SINGLE_CLICK;
}
*/
//- Delete Account
void deleteAccount(int position) {
  LCDPrintLine2("Erasing creds");
  char emptyPassword[PASSWORD_SIZE];
  for (int i = 0; i < PASSWORD_SIZE; i++) {
    emptyPassword[i] = '\0';                                                    // to completely overwrite the password in EEProm
  }
  char allBitsOnArray[1];
  allBitsOnArray[0] = 0xFF;                                                     // this makes the account name empty/available
  allBitsOnArray[1] = '\0';
  writeAllToEEProm(allBitsOnArray, "\0",emptyPassword, position);               // first char of username replaced with null terminator
  for (int j = 0; j < ACCOUNT_SIZE; j++) {
    accountsArray[position][j] = '\0';      
  }
  LCDPrintLine2("Creds erased");
}

//- UUID Generation

void setUUID(char *password) {
  for (int i = 0; i < PASSWORD_SIZE; i++) {
    password[i] = random(33,126);                                               // maybe we should use allChars here instead? We're generating PWs w/ chars that we can't input...
  }
}

//- keyboard functions

void sendUsername() {
  readUserFromEEProm(acctPosition, username);                                   // read the username from EEProm
  LCDPrintLine2(username);                                                      // show on bottom line of LCD
  if(DEBUG) {                                                                   // if debugging just print the
    debugNoLF(F("Send Username: "));debug(username);                               // username to the serial monitor
  }  else  {
    Keyboard.begin();
    Keyboard.print(username);                                                   // type the username through the keyboard
    Keyboard.print(TAB_KEY);                                                    // tab over to the password field.  Won't work everywhere.
    Keyboard.end();
  }
}

void sendPassword() {
  readPassFromEEProm(acctPosition, password);                                   // read the password from EEProm
  if(DEBUG) {                                                                   // if debugging just print the
    debugNoLF(F("Send Password: "));debug(password);                               // password to the serial monitor
  }  else  {
    Keyboard.begin();
    Keyboard.println(password);                                                 // type the password through the keyboard, then <enter>
    Keyboard.end();
  }
}

void sendUsernameAndPassword() {
  LCDPrintLine2(accountsArray[acctPosition]);
  readUserFromEEProm(acctPosition, username);                                   // read the username from EEProm
  readPassFromEEProm(acctPosition, password);                                   // read the password from EEProm
  if(DEBUG) {                                                                   // if debugging just print the
    debug(username);                                                            // username and password to the serial monitor
    debug(password);                                                            // and password to the serial monitor
  }  else  {
    Keyboard.begin();
    Keyboard.print(username);                                                   // type the username in through the keyboard, then
    Keyboard.print(TAB_KEY);                                                    // the <tab> key,
    Keyboard.println(password);                                                 // then the password and the <enter> key.
    Keyboard.end();
  }
}

void sendAll() {
  int initialAcctPosition = acctPosition;  
  for(acctPosition = 0; acctPosition < CREDS_ACCOMIDATED; acctPosition++) {
    byte ch = EEPROM.read(getAddrAcct(acctPosition));
    if (ch != 0xFF) {
      eeprom_read_string(getAddrAcct(acctPosition), accountsArray[acctPosition], ACCOUNT_SIZE);
      if(DEBUG) {
        debug(accountsArray[acctPosition]);
        sendUsernameAndPassword();
        debug(F(""));
      } else { 
        Keyboard.begin();
        Keyboard.println(accountsArray[acctPosition]);
        Keyboard.end();
        sendUsernameAndPassword();
        Keyboard.begin();
        Keyboard.println("");                                                    // place a carriage return between each account
        Keyboard.end();
      }
    }
  }
  acctPosition = initialAcctPosition;
}

//- LCD Control

void LCDPrintLine1(char* lineToPrint) {
  LCDBlankLine1();
  lcd.setCursor(0,0);
  lcd.print(lineToPrint);
}

void LCDPrintLine2(char* lineToPrint) {
  LCDBlankLine2();
  lcd.setCursor(0,1);
  lcd.print(lineToPrint);
}

void LCDPrintLine1Clear2(char* lineToPrint) {
  lcd.setCursor(0, 0);
  lcd.print(lineToPrint);
  LCDBlankLine2();
}

void LCDBlank() {
  LCDBlankLine1();
  LCDBlankLine2();
}

void LCDBlankLine1() {
  lcd.setCursor(0,0);
  PrintBlankLine();
}

void LCDBlankLine2() {
  lcd.setCursor(0,1);
  PrintBlankLine();
}

void PrintBlankLine() {
  lcd.print("                ");
}

void LCDShowMenu(int position, char **menu) {
  char line1[LCD_SCREEN_WIDTH+1] = "";
  strcat(line1,menu[position]);
  LCDPrintLine1(line1);
}

void LCDMenuUp(char **menu) { 
  if (position > -1) {
    LCDShowMenu(position, menu);
  }
}

void LCDMenuDown(char **menu){ 
  if (position < (elements)) {
    LCDShowMenu(position, menu);
  }
}

void LCDShowChar(char charToShow, int pos) {
  lcd.setCursor(pos,1);
  lcd.print(charToShow);
}


void setShowPasswordsMenuItem() {
  if (showPasswordsFlag) {
    mainMenu[SET_SHOW_PW_POS][13] = 'F';
    mainMenu[SET_SHOW_PW_POS][14] = 'F';
  } else {
    mainMenu[SET_SHOW_PW_POS][13] = 'N';
    mainMenu[SET_SHOW_PW_POS][14] = ' ';
  }
}

//- RGB LED

void setPurple() {
  //debug("setPurple()");
  setColor(170, 0, 255);                                                        // Purple Color
  color = PURPLE;
}

void setRed(){
  //debug("setRed()");
  setColor(255, 0, 0);                                                          // Red Color
  color = RED;
}

void setGreen(){
  //debug("setGreen()");
  setColor(0, 255, 0);                                                          // Green Color
  color = GREEN;
}

void setYellow(){
  //debug("setYellow()");
  setColor(255, 255, 0);                                                        // Green Color
  color = GREEN;
}

void setBlue(){
  //debug("setBlue()");
  setColor(0, 0, 255);                                                          // Blue Color
  color = BLUE;
}

void setWhite(){
  //debug("setWhite()");
  setColor(255, 255, 255);                                                      // White Color
  color = WHITE;
}

void setOff(){
  //debug("setOff()");
  setColor(0,0,0);                                                              // Off
  color = OFF;
}

void setOrange(){
  //debug("setOrange()");
  setColor(255, 128, 0);                                                        // Orange Color
  color = GREEN;
}

void setColor(int redValue, int greenValue, int blueValue) {
  //debug("setColor()");
  analogWrite(RED_PIN, redValue);
  analogWrite(GREEN_PIN, greenValue);
  analogWrite(BLUE_PIN, blueValue);
}

void showColor(int color) {                                                     // There's got to be a better way to do this...
  switch(color) {
     case RED  :
        setRed();
        break; 
     case YELLOW  :
        setYellow();
        break;
     case GREEN  :
        setGreen();
        break; 
     case BLUE  :
        setBlue();
        break;
     case PURPLE  :
        setPurple();
        break;
     case WHITE  :
        setWhite();
        break;
     case OFF  :
        setOff();
        break;
     default :
        setOff();
  }  
}

//- Encryption

boolean authenticateMaster(char *password) {                                    // verify if the master password is correct here
  //debug("authenticateMaster");
  char buff[MASTER_PASSWORD_SIZE];
  int result;
  encrypt(password);
  byte ch = EEPROM.read(masterPasswordAddr);
  if (ch == 0xFF) {                                                             // first time, we need to write instead of read
    //debugNoLF("First time write pw to EEPROM: ");debug(password);
    writeMasterPassToEEProm(password);    
    setGreen();
    return true;
  } else {
    eeprom_read_string(masterPasswordAddr, buff, MASTER_PASSWORD_SIZE);
    //debugNoLF("Password read from EEPROM: ");debug(buff);
    result = strcmp(password, buff);                                            // will need to decrypt pw here if you encrypt it in place
    if (result == 0) {                                                          // entered password matches master password, authenticated
        setGreen();
        loginFailures = 0;
        writeLoginFailures();
        return true;                                                            // encrypt a word using the master password as the key
    } else {                                                                    // failed authentication
        setRed();
        loginFailures++;
        writeLoginFailures();
        return false;
    }
  }
}                                                                               // and check it against the same word that's stored encrypted
                                                                                // in eeprom.  This word is written (encrypted) to eeprom the 
                                                                                // first time ever a master password is entered.
void encrypt(char *stringToEncrypt) {
                                                                                // do nothing until encryption is implemented
}                                                                                

//- Debugging Routines
                                                                                // These routines are helpful for debugging, I will leave them in for your use.
                                                                                // For sending output to the serial monitor. Set the baud rate in setup.
void debug(String text) {
  if (DEBUG) {
    Serial.println(text);
  }
}

void debugNoLF(String text) {
  if (DEBUG) {
    Serial.print(text);
  }
}

void debugInt(signed int anInt) {
  if (DEBUG) {
    char myInt[10];
    itoa(anInt,myInt,10);
    debug(myInt);
  }
}

void debugLong(signed long aLong) {
  if (DEBUG) {
    char myLong[10];
    ltoa(aLong,myLong,10);
    debug(myLong);
  }
}

void debugDouble(double aDouble) {
  if (DEBUG) {
    char *myDouble = ftoa(aDouble);
    debug(myDouble);
  }
}

void debugMetric(const char myString[], signed int anInt) {
  if (DEBUG) {
    debugNoLF(myString);debugNoLF(F(": "));
    debugInt(anInt);
    Serial.print(F("\r\n"));
  }
}

void debugMetricLong(const char myString[], signed long aLong) {
  if (DEBUG) {
    debugNoLF(myString);debugNoLF(F(": "));
    debugLong(aLong);
    Serial.print(F("\r\n"));
  }
}

void debugMetricDouble(const char myString[], double aDouble) {
  if (DEBUG) {
    debugNoLF(myString);debugNoLF(F(": "));
    debugDouble(aDouble);
    Serial.print(F("\r\n"));
  }
}
                                                                                // quick and dirty ftoa for legacy code
char *ftoa(double f)                                                            // from https://www.microchip.com/forums/m1020134.aspx
{
    static char        buf[17];
    char *            cp = buf;
    unsigned long    l, rem;

    if(f < 0) {
        *cp++ = '-';
        f = -f;
    }
    l = (unsigned long)f;
    f -= (double)l;
    rem = (unsigned long)(f * 1e6);
    sprintf(cp, "%lu.%10.10lu", l, rem);
    return buf;
}
 
//- EEPROM functions

void writeAllToEEProm(String accountname, String username, String password, int pos) {
  writeAcctToEEProm(accountname, pos);
  writeUserToEEProm(username, pos);
  writePassToEEProm(password, pos);
}

void writeAcctToEEProm(String accountname, int pos) {
  writeToEEProm(ACCOUNT_SIZE, accountname, getAddrAcct(pos));
  //debugNoLF("accountname: ");debug(accountname);
}

void writeUserToEEProm(String username, int pos) {
  writeToEEProm(USERNAME_SIZE, username, getAddrUser(pos));
  //debugNoLF("username: ");debug(username);
}

void writePassToEEProm(String password, int pos) {
  writeToEEProm(PASSWORD_SIZE, password, getAddrPass(pos));
  //debugNoLF("password: ");debug(password);
}

void writeMasterPassToEEProm(String password){
  writeToEEProm(MASTER_PASSWORD_SIZE, password, masterPasswordAddr);
  //debugNoLF("Master password: ");debug(password);
}

void writeToEEProm(int bufsize, String myString, int ee_addr) {
  char buf[bufsize];
  char myStringChar[bufsize];
  myString.toCharArray(myStringChar, bufsize);                                  //convert string to char array (TODO: is this necessary?)
  strcpy(buf, myStringChar);
  cli();                                                                        // disable interrupts
  eeprom_write_string(ee_addr, buf);
  sei();                                                                        // re-enable interrupts
}

void loadAccounts() {
  for(int acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    byte ch = EEPROM.read(getAddrAcct(acctPos));
    if (ch != 0xFF) {
      eeprom_read_string(getAddrAcct(acctPos), accountsArray[acctPos], PASSWORD_SIZE);
      accountCount++;
    }
  }
  //debugMetric("accountCount",accountCount);  
}

int getNextFreeAcctPos() {
  for(int acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    byte ch = EEPROM.read(getAddrAcct(acctPos));
    if (ch == 0xFF) {
      return acctPos;
    }
  }
  return -1;
}
/*
void loadCredsArray() {
  for(int acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    readAllFromEEProm(  *allCreds[acctPos].account, 
                        *allCreds[acctPos].username, 
                        *allCreds[acctPos].password, acctPos);
  }
}
*/

void readAllFromEEProm(char *accountname, char *username, char *password, int pos) {
  //readAcctFromEEProm(pos, accountname);
  readUserFromEEProm(pos, username);
  readPassFromEEProm(pos, password);
}

/*
  void readAcctFromEEProm(int pos, char *buf) {
  eeprom_read_string(getAddrAcct(pos), buf, ACCOUNT_SIZE);
  debugNoLF("read account: ");debug(buf);
  strcpy(accountname, buf);
}
*/

void readUserFromEEProm(int pos, char *buf) {
  if (pos > -1) {
    eeprom_read_string(getAddrUser(pos), buf, USERNAME_SIZE);
  } else {
    buf[0] = '\0';
  }
  //strcpy(username, buf);
  //debugNoLF("read user: ");debug(buf);
}

void readPassFromEEProm(int pos, char *buf) {
  if (pos > -1) {
    eeprom_read_string(getAddrPass(pos), buf, PASSWORD_SIZE);
  } else {
    buf[0] = '\0';
  }
  //strcpy(password, buf);
  //debugNoLF("read password: ");debug(buf);
}

byte getLoginFailures() {
  return EEPROM.read(loginFailuresAddr);
}

byte getShowPasswordsFlag() {
  return EEPROM.read(showPasswordsFlagAddr);
}

void writeLoginFailures() {
  EEPROM.write(loginFailuresAddr, loginFailures);
}

void writeShowPasswordsFlag() {
  EEPROM.write(showPasswordsFlagAddr, showPasswordsFlag);
}

//////////////////////////////////////////////////////////////////////////
// Following code is from smching: https://gist.github.com/smching/05261f11da11e0a5dc834f944afd5961 


                                                                                // Absolute min and max eeprom addresses. Actual values are hardware-dependent.
                                                                                // These values can be changed e.g. to protect eeprom cells outside this range.
const int EEPROM_MIN_ADDR = 0;
const int EEPROM_MAX_ADDR = EEPROM_MEM_SIZE;                                    // 1023

                                                                                // Returns true if the address is between the
                                                                                // minimum and maximum allowed values, false otherwise.
                                                                                //
                                                                                // This function is used by the other, higher-level functions
                                                                                // to prevent bugs and runtime errors due to invalid addresses.
boolean eeprom_is_addr_ok(int addr) {
  return ((addr >= EEPROM_MIN_ADDR) && (addr <= EEPROM_MAX_ADDR));
}

                                                                                // Writes a sequence of bytes to eeprom starting at the specified address.
                                                                                // Returns true if the whole array is successfully written.
                                                                                // Returns false if the start or end addresses aren't between
                                                                                // the minimum and maximum allowed values.
                                                                                // When returning false, nothing gets written to eeprom.
boolean eeprom_write_bytes(int startAddr, const byte* array, int numBytes) {
                                                                                // counter
  int i;
                                                                                // both first byte and last byte addresses must fall within
                                                                                // the allowed range 
  if (!eeprom_is_addr_ok(startAddr) || !eeprom_is_addr_ok(startAddr + numBytes)) {
    return false;
  }
  for (i = 0; i < numBytes; i++) {
    EEPROM.write(startAddr + i, array[i]);
  }
  return true;
}

// Writes a string starting at the specified address.
// Returns true if the whole string is successfully written.
// Returns false if the address of one or more bytes fall outside the allowed range.
// If false is returned, nothing gets written to the eeprom.
boolean eeprom_write_string(int addr, const char* string) {
  int numBytes; // actual number of bytes to be written
  //write the string contents plus the string terminator byte (0x00)
  numBytes = strlen(string) + 1;
  return eeprom_write_bytes(addr, (const byte*)string, numBytes);
}

                                                                                // Reads a string starting from the specified address.
                                                                                // Returns true if at least one byte (even only the string terminator one) is read.
                                                                                // Returns false if the start address falls outside the allowed range or declare buffer size is zero.
                                                                                // 
                                                                                // The reading might stop for several reasons:
                                                                                // - no more space in the provided buffer
                                                                                // - last eeprom address reached
                                                                                // - string terminator byte (0x00) encountered.
boolean eeprom_read_string(int addr, char* buffer, int bufSize) {
  byte ch;                                                                      // byte read from eeprom
  int bytesRead;                                                                // number of bytes read so far
  if (!eeprom_is_addr_ok(addr)) {                                               // check start address
    return false;
  }

  if (bufSize == 0) { // how can we store bytes in an empty buffer ?
    return false;
  }
                                                                                // is there is room for the string terminator only, no reason to go further
  if (bufSize == 1) {
    buffer[0] = 0;
    return true;
  }
  bytesRead = 0;                                                                // initialize byte counter
  ch = EEPROM.read(addr + bytesRead);                                           // read next byte from eeprom
  buffer[bytesRead] = ch;                                                       // store it into the user buffer
  bytesRead++;                                                                  // increment byte counter
                                                                                // stop conditions:
                                                                                // - the character just read is the string terminator one (0x00)
                                                                                // - we have filled the user buffer
                                                                                // - we have reached the last eeprom address
  while ( (ch != 0x00) && (bytesRead < bufSize) && ((addr + bytesRead) <= EEPROM_MAX_ADDR) ) {
    // if no stop condition is met, read the next byte from eeprom
    ch = EEPROM.read(addr + bytesRead);
    buffer[bytesRead] = ch;                                                     // store it into the user buffer
    bytesRead++;                                                                // increment byte counter
  }
                                                                                // make sure the user buffer has a string terminator, (0x00) as its last byte
  if ((ch != 0x00) && (bytesRead >= 1)) {
    buffer[bytesRead - 1] = 0;
  }
  return true;
}
                                                                                // end smching                                                                                
