 
 Hi, this is Dan Murphy, and this is the PasswordPump, a USB device that manages
 credentials.  With the device you can edit and save credentials, and you can
 send them to your Windows PC as if they were typed into the keyboard.  The 
 device has a master password, which will serve as the encryption key to the rest of 
 the passwords on the device.  The master password is hashed with SHA-256, and 
 then that hash is used as the encryption key for all of the passwords, which 
 are encrypted with AES-128.  The micro controller is an AtMel Mega32u4, which 
 is operating at a clock speed of 16mHz, has 32k of program memory (which is now
 entirely consumed by the code for this project), and has only 1k of EEprom.  EEprom is where
 we are storing the credentials, and 1k is only enough room for about a dozen
 sets of credentials; therefore more EEprom is added in the form of two 25LC256 
 chips from Microchip Technology, which each have a capacity of 256k bits, or 32k bytes, 
 enough room for 254 sets of credentials and a complete backup.  
 The internal EEprom can be protected from prying eyes by setting lock bits on the 
 microcontrollert that prevent an attacker from being able to see the program and 
 the content of EEprom.  This is not a substitute for encryption but it does 
 mean that an attacker would have to be very highly motivated.  If we implement
 external EEprom, however, there are no lock bits, and encryption becomes much
 more important.  Ideally I'd like to store the hashed master password on
 internal EEprom, which would be protected by the lock bits, and the encrypted 
 credentials alone on the external EEprom.  
 
 A demo showing how the device works.  This is the external EEprom (24LC512),
 and this is where I've left room for it on the PCB.
 
 First I'll login to the device.
 
 Next, I'll show you the main menu.
 
 Next I'll show you all of the operations possible when you edit credentials.
 
 Then I'll show you a list off all the credentials that are already loaded.
 
 Finally I'll authenticate into a few existing accounts.
 
 
 
 
    Features
    ========
  - USB device that manages credentials for applications and web sites
  - Authenticate with a master password, which will also serve as the encryption key
  - Search for, locate and select accounts
  - Send account name, username and password to an application or web page as if typed in keyboard
  - Edit existing account name, username, password
  - Add new and delete old credentials
  - Add accounts (or edit existing accounts) with a generated password, like a UUID
  - Backup all credentials to a text file
  - Decoy password feature
  - Configurable password display on or off
  - Logout / de-authenticate via menu
  - Factory reset via menu (when authenticated)
  - Factory reset after 10 failed login attempts
  - Works w/ EEprom internal to AtMega32u4 (1kB) or 24LC512 (32kB). (precompiler
    directive).
  - Works w/ 16x2 LCD or 132x64 LED displays (precompiler directive)
  - [show versions 1, 2 and 3 of the prototype]
  - once I've fabricated the customer PCB and printed out a case for the unit it
    should be small enough to carry on a keyring.

    Demo
    ====
  - Login via master password
  - Find an account
  - Send creds
  - Find another account
  - Send creds
  - Show the features in send creds where you can send just username or just
    password or just account name; delete the account, backup all
  - 
  
    Remaning Tasks and Enhancements
    ===============================
  x design the custom PCB
  x Have the device automatically logout after a period of inactivity.
  x Add AES-128 encryption for the passwords (excluding the master password)
  - Hash the master password and only store the hash
  - Set the lock bits on the AtMega32u4 to prevent an attacker from being able
    to read the credentials or the source code.
  - Add the encryption, sha-256 hash the master password, aes-256 or perhaps
    authenticated encrypted with associated data like  Acorn128 or Ascon128.
  - Add the ability to interface with a keyboard for entering credentials that 
    you want to store.
  - Currently the device only stores about a dozen sets of credentials.  So I'd 
    like to finishing adding external EEprom, the 24LC512 from MicroChip 
    Technology, which will store at least 256 sets of credentials.
  - design and print a case
  - Finish implementing a doubly linked list so that the accounts appear in 
    sorted order.
  - Fix remaining defects and throughly test
  
  
	Description					Product Cost		Ship	Lot Size	From
	===========
  - Arduino Pro Micro				$2.87			free	  1			China		https://www.aliexpress.com/item/New-Pro-Micro-for-arduino-ATmega32U4-5V-16MHz-Module-with-2-row-pin-header-For-Leonardo/32768308647.html?spm=a2g0s.9042311.0.0.12914c4d0Xj2PY
  - RGB LED							 0.02			free  	100			China		https://www.aliexpress.com/item/Free-shipping-100pcs-5mm-RGB-LED-Common-Cathode-Tri-Color-Emitting-Diodes-f5-RGB-Diffused/32330160607.html?spm=2114.search0104.3.17.6ae840aaohPCBN&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10130_318_10890_10547_319_10546_10548_317_10545_10696_450_10084_10083_10618_452_535_534_533_10307_532_204_10059_10884_323_325_10887_100031_320_321_322_10103_448_449_5728415,searchweb201603_55,ppcSwitch_0&algo_expid=106caae9-74c0-4f4a-b224-e96dd4f8efb1-2&algo_pvid=106caae9-74c0-4f4a-b224-e96dd4f8efb1&transAbTest=ae803_5
  - Resistors						~0.07			free	100			China		https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20181024040631&SearchText=resistors	
  - Custom PCB						 1.10			free	 20			China		https://www.aliexpress.com/item/2PCS-PCB-Board-Stripboard-Veroboard-Prototype-Card-Prototyping-Circuit-Double-Sided-Universal-DOT-Perfboard-Breadboard-Bricolage/32880212821.html?spm=2114.search0104.3.10.5d17703cU0we8a&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10130_318_10890_10547_319_10546_10548_317_10545_10696_450_10084_10083_10618_452_535_534_533_10307_532_204_10059_10884_323_325_10887_100031_320_321_322_10103_448_449_5728415-10890,searchweb201603_55,ppcSwitch_0&algo_expid=938e72f3-c597-4efc-b3be-66aeeef7dc47-1&algo_pvid=938e72f3-c597-4efc-b3be-66aeeef7dc47&transAbTest=ae803_5
  - Rotary Encoder					 0.42			free	  5			China		https://www.aliexpress.com/item/5pcs-Rotary-encoder-code-switch-EC11-audio-digital-potentiometer-with-switch-5Pin-handle-length-15mm/32798669185.html?spm=2114.search0104.3.1.4bc224f8PBVffZ&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10130_318_10890_10547_319_10546_10548_317_10545_10696_450_10084_10083_10618_452_535_534_533_10307_532_204_10059_10884_323_325_10887_100031_320_321_322_10103_448_449_5728415,searchweb201603_55,ppcSwitch_0&algo_expid=601795c0-e796-4076-a084-03df4f6bab0f-0&algo_pvid=601795c0-e796-4076-a084-03df4f6bab0f&transAbTest=ae803_5
  - Knob							 0.15			free	 10			China		https://www.aliexpress.com/item/10pcs-Plastic-Volume-Control-Knob-Potentiometer-Knob-Cap-for-Encoder-Potentiometer-6mm-Round-Shaft-WH148/32811813896.html?spm=a2g0s.9042311.0.0.15554c4d2B1pdZ
  - OLED 128x32						 1.64			free	  2			China		https://www.aliexpress.com/item/ShengYang-1pcs-0-91-inch-OLED-module-0-91-white-blue-OLED-128X32-OLED-LCD-LED/32927682460.html?spm=2114.search0104.3.8.60765daeMuRsBD&ws_ab_test=searchweb0_0,searchweb201602_3_5731312_10065_10068_10130_10890_10547_319_10546_317_10548_5730312_10545_10696_5728812_10084_10083_5729212_10618_5731412_10307_5731212_5731112_328_10059_10884_5732012_5731512_10887_100031_5731612_321_322_10103_5732512_5731712,searchweb201603_55,ppcSwitch_0&algo_expid=f653859a-2805-475d-85a3-b236a7dae281-1&algo_pvid=f653859a-2805-475d-85a3-b236a7dae281
  - 2 25LC256 External EEprom        0.41           free     20         China       https://trade.aliexpress.com/orderList.htm?spm=2114.11010108.1000002.15.650c649beSGpy2&tracelog=ws_topbar
    Case                             ?.??
    Assembly                         ?.??
    -----
	Total							$6.68														
									=====


	Description					Product Cost		Ship
	===========
  - Arduino Pro Micro				$2.87			free
  - RGB LED							 0.02			free
  - Resistors						~0.07			free
  - Custom PCB						 1.10			free
  - Rotary Encoder					 0.42			free
  - Knob							 0.15			free
  - OLED 128x32						 1.64			free
  - 2 25LC256 External EEprom        0.41           free
    -----
	Total							$6.68
									=====

									  Menu Navigation
  ===============
	Master Password
	Find Account
	 [scroll through accounts list]
		Send User & Pass
		Send Password
		Send Username
		Send Account
		Edit Credentials
			Account Name
			Edit Username
			Edit Password
			Indicate Style
			GeneratePasswrd
		Delete Account [confirm]
	Add Account
		Account Name
		Edit Username
		Edit Password
		Indicate Style
		GeneratePasswrd
	Logout
	Keyboard ON/OFF
	Show Passwrd ON/OFF
	Backup EEprom [confirm]
	Backup to File
	Restore Backup [confirm]
	Fix Corruption [confirm]
	Reset [confirm]


									
	Trivia
	======
  - Cost of parts $7.14
  - Lines of code ~1,900.
  ~ Programming language: C
  - IDE: ATMel Studio 7.0

  Link to Video
  =============
  https://www.youtube.com/watch?v=Td839HR0Skk&t=10s
  http://bit.ly/PasswordPump
  https://app.bitly.com/BibegKpZnby/bitlinks/2zcTTAK for tracking data on the link
  above.
  
Hackathon Notes
  Presentation Order
  Team Name
    Introvert(s)
  Problem or Idea
    Adding encryption to an existing USB device that stores credentials (account 
    name, username and password) in EEProm and automatically types the 
    credentials into a Windows PC when selected.  The master password should be 
    hashed w/ SHA-256 and all other passwords should be encrypted with AES-128.  
    The device is an ATMega32u4 and is already resource constrained by existing 
    software.  The code is written in C/C++.  [Link to video describing the 
    device is here: http://bit.ly/PasswordPump] .
  Site
    Lexington
  Team Members
    Dan Murphy
  Status
    
  Recording Start Time
  