/*
  PasswordKeeper.ino

  Project Name: PasswordPump | CredsPump | PasswordKeeper
  Version:      1.0
  Date:         2018/09/22 - 2018/11/26
  Device:       Arduino Pro Micro w/ ATmega32u4
  Language:     C
  Memory:       32kB flash, 2.5kB SRAM, 1kB EEPROM
  EEprom:       internal=1kB, 24LC512 external=32kB 
  Clock Speed:  16MHz
  Voltage:      5v
  Author:       Daniel J. Murphy
  Components:   RGB LED, 16x2 Liquid Crystal Display or 132x64 LED display,
                one momentaty push button, one rotary encoder, 3 4.7kohm 
                resistors for I2C, 3 220ohm resistors for the RGB
  Purpose
  =======
  - To manage usernames and passwords and to type them in via keyboard/USB.

  Features
  ========
  - Authenticate with master password
  - Search for accounts
  - Send account name, username and password as if typed in keyboard
  - Add account name, username, password (generated or not)
  - Edit existing account name, username, password
  - Delete account
  - Backup all accounts to a text file
  - Logout / de-authenticate via menu
  - Factory reset via menu (when authenticated)
  - Factory reset after 10 failed login attempts
  - Decoy password feature
  - Configurable password display on or off
  - Works w/ 16x2 LCD or 132x64 LED displays (precompiler directive)
  - Works w/ EEprom internal to AtMega32u4 (1kB) or 24LC512 (32kB). (precompiler
    directive).
  - all passwords (except master password) are encrypted.
  
  Known Defects
  =============  
    - = outstanding
    ? = fixed but needs testing
    x = fixed
  - in the switch statement for EVENT_SINGLE_CLICK the case statements 
    are not in order. When they are in order it doesn't evaluate 
    correctly.
  - When using extended memory crash at line 1367 where the LCD output is 
    trashed. Suspect some conflict w/ the 24LC512 EEprom chip.  Hangs subsequent
    to line 1367. Search for "trashed".
  - Backup all isn't consistently printing carriage returns.
  - can't seem to send a <tab> character via the Keyboard.  Tried KEY_TAB, 
    TAB_KEY, 0x2b, 0xB3, '	'.
  - we are authenticated after blowing all the creds away after 10 failures
  - single character usernames and passwords are not working well
  ? single click after Reset brings you to alpha edit mode
  x passwords are not generating correctly
  x the LCD's backlight is not turning on, needed to operate in the dark
  x after add account the account isn't showing in find account
  x after hitting reset show password = 0
  x turning the rotary encoder fast doesn't scroll thru the menu fast [sending 
    output to the serial monitor corrects this] 
  x can't add a new account; can't scroll edit menu on first account add
  x long click seems too long
  x first account added doesn't immediatly show in find account after it's 
    added.
  x after reset, stuck in reset.
  x in LCD mode too many chars in "Show Passwrd OFF", freezes device.
  x Crash when scrolling up the menu after selecting 'add account'.
  x failures are not showing correctly because you need to blank out the line 
    first
  x after adding an account we can't find it
  
  TODO / Enhancements
  ===================
    x = implemented but not tested  
    - = unimplemented
    ? - tried to implement, ran into problems
    % - concerned there isn't enough memory left to implement
    * - implemented and tested
  x Encrypt all passwords (except the master password)
  - A master key generated from the user password is hashed using SHA-256, which 
    is subsequently used to encrypt the password database with AES-128. For 
    SHA-256 check out:
        https://github.com/Cathedrow/Cryptosuite.
        https://github.com/simonratner/Arduino-SHA-256  
        https://rweather.github.io/arduinolibs/crypto.html (AEAD)
    Cryptography experts no longer recommend CBC for use in newer designs. It 
    was an important mode in the past but newer designs should be using 
    authenticated encryption with associated data (AEAD) instead.   
  - finish the video
    https://screencast-o-matic.com/
  - test the decoy password feature.
  - reconsider the organization of the menus
  - work on the workflow; which menu items are defaulted after which events.
  - decide if the encrypted pw will be saved to EEprom
  - learn how to set the lock bits
  - ground unused pins
  - remove the space character and the escape (/ or \) from enterable 
    characters?
  - make it possible to edit creds via keyboard
  - make it possible to import creds via XML file
  - implement a doubly linked list to keep the accounts in order
  ? incorporate the use of an i2c EEprom chip to expand available memory
  ? add a feature whereby the unit factory resets after two triple clicks, even
    if not yet authenticated. (commented out, caused problems)
  ? add a feature whereby the unit logsout after two double clicks. (commented
    out, caused problems)
  x get ScreenCastOMatic to record the video for the project 
  x add function(s) to send error output to the display device (e.g. no external 
    EEprom.
  x Make it possible to send the account name, where a URL can be entered.
  x eliminate the need for the acctsArray to conserve memory.
  x make room for a next pointer and a previous pointer to maintain a doubly 
    linked list so that we can sort the accountnames.
  % add a decoy password that executes a factory reset when the password plus
    the characters "FR" are supplied as the master password.
  * have the unit automatically logout after a period of inactivity, this will
    require use of the timer. (probably not enough memory left to add this)
  % add the ability to pump a single tab or a single carrige return from the 
    menu.
  x decide if you should display the passwords.  Possibly make it configurable.
  x mask passwords on input
  x Delete account (account name, username, password)
  x add a feature that dumps all account names, usernames and passwords out
    through the keyboard (like to be inserted into an editor as a backup).
  x have the unit factory reset after 10 failed attempts.  Store the failed

  Components
  ==========
  - I2C LCD display 16x2 characters, user interface or
  - I2C LED display 32x128 pixels.
  - RGB LED
  - Rotary Encoder
  - internal AtMega32u4 EEprom or
  - 24LC512 EEprom
  - 3 220ohm resistors
  - 4 4.7kohm resistors (to hold i2c SDA and SCL lines high) / optional
  
  Warnings
  ========
  - Program memory is nearly full so be careful and watch out for flaky 
    behavior.  Once over 80% of global space you're in trouble. Currently
    at 88% of program storage space and 70% of dynamic memory (glabal variable
    space).  We're only allowing for the management of 5 sets of creds. 307 
    bytes left.  The LED library is large.

  Suggestions
  ===========
  - Best viewed in an editor w/ 160 columns, most comments are at column 80
  - Please submit defects you find so I can improve the quality of the program
    and learn more about embedded programming.

  Copyright
  =========
  - Copyright ©2018, Daniel Murphy <dan-murphy@comcast.net>
  
  Contributors
  ============
  smching: https://gist.github.com/smching/05261f11da11e0a5dc834f944afd5961 
  for EEPROMUtil.cpp.
  Source code has been pulled from all over the internet,
  it would be impossible for me to cite all contributors.
  Special thanks to Elliott Williams for his essential book
  "Make: AVR Programming", which is highly recommended. 

  License
  =======
  Daniel J. Murphy hereby disclaims all copyright interest in this
  program written by Daniel J. Murphy.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Libraries 
  =========
  - https://github.com/LennartHennigs/Button2
  - https://github.com/brianlow/Rotary
  - https://github.com/arduino-libraries/Keyboard
  - https://www.arduinolibraries.info/libraries/hd44780
  - https://www.arduino.cc/en/Reference/EEPROM
  - https://github.com/gngz/24LC256-Arduino-Library                             // we are using a small library built for the 24LC256 with a 24LC512.  The 
                                                                                // library was modified to change the size of EEprom for the chip from 32,000
                                                                                // bytes to 64,000 bytes.  i2c is periodically hanging with this library.
  Hardware
  ========
  - 24LC512 (External EEprom)
    Tested Part: MICROCHIP - 24LC512-I/P - EEPROM SERIELL 512K,24LC512, DIP8

    Number Name ConnectTo     Note
    1       A0     GND
    2       A1     GND
    3       A2     GND
    4       GND    GND
    5       SDA    SDA        4.7kOhm PullUp (close to ATMega32u4)
    6       SCL    SCL        4.7kOhm PullUp (close to ATMega32u4)
    7       WP     GND
    8       VCC    (+5V0)  

  Budgeting Memory
  ================
  - Current Setup 
  ---------------
  Sketch uses 26820 bytes (93%) of program storage space. Maximum is 28672 bytes.
  Global variables use 1633 bytes (63%) of dynamic memory, leaving 927 bytes for 
  local variables. Maximum is 2560 bytes.
  Remember also that without the bootloader the Arduino IDE estimate might be 
  high.

  - Add LED only                +17% program storage, +22% dynamic memory (OFF)
  - Add External EEPROM only     +1% program storage,  +2% dynamic memory (OFF)
  - Add Encryption (just libs)   +2% program storage,  +6% dynamic memory (ON)
  - No LED Library LED Only      -5% program storage,  -4% dynamic memory (ON)
  - DEBUG                        +6% program storage                      (OFF)

  The Program 
  ===========
  - Includes                                                                    */
#include <Wire.h>                                                               // for all i2c communication
#include "Button2.h";                                                           // for the button on the rotary encoder
#include <Rotary.h>                                                             // for the rotary encoder
#include <avr/eeprom.h>                                                         // for reading and writing EEprom
#include <Keyboard.h>                                                           // for simulating a USB keyboard and sending output to it
#include <SPI.h>                                                                // TODO: double check if this is required

//- Switches
//#define DEBUG                                                                 // determines if debugging is sent out via serial monitor.  Device won't automatically run program 
                                                                                // when it's plugged in to USB unless DEBUG is NOT defined.
                                                                                // define one and only one of the following: LED_DISPLAY, LCD_DISPLAY, INLINE_LED_CODE
//#define LED_DISPLAY                                                           // defined when you want to use the LED display instaed of the LCD display
#define INLINE_LED_CODE                                                         // uses sort of crappy font but it works okay.
//#define LCD_DISPLAY                                                           // define when you want to use the 16x2 LCD display instead of the LED display
#define ENCRYPTION                                                              // if we're encrypting the master password, account name, username and passwords then define this
//#define EXTERNAL_EEPROM                                                       // defined when you're using the 24LC512 external EEprom chip
//#define I2C_SCAN                                                              // define only when you want to scan the i2c bus for addresses in use

//- Macros
//#define ClearBit(x,y) x &= ~y
//#define SetBit(x,y) x |= y
//#define ClearBitNo(x,y) x &= ~_BV(y)
//#define SetState(x) SetBit(machineState, x)
#ifdef INLINE_LED_CODE
  #define SCL_low()              SCL_PORT  &= ~_BV(SCL_BIT)   
  #define SCL_high()             SCL_PORT  |=  _BV(SCL_BIT)   
  #define SDA_low()              SDA_PORT  &= ~_BV(SDA_BIT)   
  #define SDA_high()             SDA_PORT  |=  _BV(SDA_BIT)   
#endif
  
//- Defines

#ifdef ENCRYPTION
  #include <Crypto.h>
  #include <SHA256.h>
  #include <AES.h>
  #include <string.h>
#endif

#ifdef EXTERNAL_EEPROM
  #include <24LC256.h>                                                          // for reading and writing 24LC512 external EEprom
#else
  #include <EEPROM.h>                                                           // for reading and writing AtMega32u4 internal EEprom
#endif

#ifdef LED_DISPLAY
  #include <Adafruit_SSD1306.h>
#endif
#ifdef INLINE_LED_CODE
  #include "prj.h"
  #include "soft_i2c.h"
  #include "font8x16.h"
  #include "oled_ssd1306.h"
#endif
#ifdef LCD_DISPLAY
  #include <hd44780.h>                                                          // main hd44780 header
  #include <hd44780ioClass/hd44780_I2Cexp.h>                                    // i2c expander i/o class header
#endif

#define BAUD_RATE                 38400                                         // Baud rate for the Serial monitor, best for 16MHz

#define ERR_NO_EXT_EEPROM         1
#define ERR_READ_ACCOUNT          2
#define ERR_READ_MASTER           3
#define ERR_EEPROM_WRITE          4
#define ERR_READ_ACCOUNT_2        5
#define ERR_READ_USER             6
#define ERR_READ_STYLE            7
#define ERR_READ_PASS             8

#define ROTARY_PIN1               9                                             // for AtMega32u4 / Arduino Pro Mini
#define ROTARY_PIN2               8                                             //   "                               
#define BUTTON_PIN                7                                             //   "                              
//#define LED_PIN                 16                                           
#define RED_PIN                   19                                            // Pin locations for the RGB LED, must be PWM capable
#define GREEN_PIN                 20                                           
#define BLUE_PIN                  21                                           
                                                                               
#define ADC_READ_PIN              10                                            // we read the voltage from this floating pin to seed the random number generator, don't ground it!
                                                                               
#define LCD_SCREEN_WIDTH          16                                            // characters across
#define LCD_SCREEN_HEIGHT         2                                             // characters down
#define LED_LINE_HEIGHT_PIX       10                                            // the height of a character on the LED display in pixels.
#define NO_LED_LIB_LIN_HEIGHT_PIX 4
#define LED_CHAR_WIDTH_PIX        6                                             // the width of a character on the LED display in pixels.
#define NO_LED_LIB_CHAR_WIDTH_PIX 8

#define SHOW_SPLASHSCREEN         2000                                          // in microseconds
#define SHOW_ADAFRUIT             500                                           // "

#define INITIAL_MEMORY_STATE_CHAR -1                                            // 11111111 binary twos complement, -1 decimal, 0xFF hex.  When factory fresh all bytes in EEprom memory = 0xFF.
#define INITIAL_MEMORY_STATE_BYTE 0xFF                                          // 11111111 binary twos complement, -1 decimal, 0xFF hex.  When factory fresh all bytes in EEprom memory = 0xFF.
#define NULL_TERM                 0x00                                          // The null terminator, NUL, ASCII 0, or '\0'                 

#define EVENT_NONE                0                                             // used to switch of the previous event to avoid infinate looping
#define EVENT_SINGLE_CLICK        1                                             // a single click on the rotary encoder
//#define EVENT_DOUBLE_CLICK      2                                             // double click on the rotary encoder
//#define EVENT_TRIPLE_CLICK      3                                             // triple click on the rotary encoder
#define EVENT_LONG_CLICK          4                                             // holding the button on the rotary encoder down for more than 1 second(?) (changed from default directly in the library)
#define EVENT_PRESSED             5                                             // when the rotary encoder button clicks down
#define EVENT_RELEASED            6                                             // when the rotary encoder button is released
#define EVENT_ROTATE_CW           7                                             // turning the rotary encoder clockwise
#define EVENT_ROTATE_CC           8                                             // turning the rotary encoder counter clockwise.
#define EVENT_SHOW_MAIN_MENU      9                                             // to show the main menu
//#define EVENT_SHOW_ACCT_MENU    10                                            // 
#define EVENT_SHOW_EDIT_MENU      11                                            // to show the menu used for editing account name, username and password (creds)
#define EVENT_RESET               12                                            // Factory Reset event
#define EVENT_LOGOUT              13                                            // logging out of the device

//- Memory Layout
#define MIN_AVAIL_ADDR            0x00                                          // assuming we start at the very beginning of EEprom
#define INTERNAL_EEPROM_MEM_SIZE  0x03FF                                        // 1,023 is the max address of the EEprom on AtMega32u4
//#define EXTERNAL_EEPROM_MEM_SIZE  0x7CFF                                      // 32kB / 0x7FFF is the size of the EEprom on the 24LC512  is it really 0x7CFF / 31999?
#define EXTERNAL_EEPROM_MEM_SIZE  0xF9FF                                        // 63,999  / 0xF9FF is the max address for EEprom on the 24LC512  
#ifdef EXTERNAL_EEPROM
  #define MAX_AVAIL_ADDR          EXTERNAL_EEPROM_MEM_SIZE                      // 64kB, for EXTERNAL_EEPROM
#else
  #define MAX_AVAIL_ADDR          INTERNAL_EEPROM_MEM_SIZE                      // 1023, for INTERNAL_EEPROM
#endif
#define ACCOUNT_SIZE              16                                            // bytes
#define USERNAME_SIZE             32                                            // bytes
#define PASSWORD_SIZE             16                                            // bytes
#define STYLE_SIZE                2                                             // bytes, we are storing the null terminator
#define PREV_POS_SIZE             1                                             // bytes
#define NEXT_POS_SIZE             1                                             // bytes
#define CREDS_TOT_SIZE            (ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + PREV_POS_SIZE + NEXT_POS_SIZE)
#define MASTER_PASSWORD_SIZE      16
#define LOGIN_FAILURES_SIZE       1
#define SHOW_PASSWORD_FLAG_SIZE   1
#define CREDS_ACCOMIDATED         13                                            // 13 is max for the AtMega32u4 with the configuration related values stored at the end. Can't exceed 256. TODO: calculate
#define GET_ADDR_ACCT(pos)        (MIN_AVAIL_ADDR + (pos * CREDS_TOT_SIZE))
#define GET_ADDR_USER(pos)        (MIN_AVAIL_ADDR + ACCOUNT_SIZE + (pos * CREDS_TOT_SIZE))
#define GET_ADDR_PASS(pos)        (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + (pos * CREDS_TOT_SIZE))
#define GET_ADDR_STYLE(pos)       (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + (pos * CREDS_TOT_SIZE))
#define GET_ADDR_PREV_POS(pos)    (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + (pos * CREDS_TOT_SIZE))
#define GET_ADDR_NEXT_POS(pos)    (MIN_AVAIL_ADDR + ACCOUNT_SIZE + USERNAME_SIZE + PASSWORD_SIZE + STYLE_SIZE + PREV_POS_SIZE + (pos * CREDS_TOT_SIZE))
#define GET_ADDR_MASTER_PASS      (MAX_AVAIL_ADDR - SHOW_PASSWORD_FLAG_SIZE - LOGIN_FAILURES_SIZE - MASTER_PASSWORD_SIZE)
#define GET_ADDR_LOGIN_FAILURES   (MAX_AVAIL_ADDR - SHOW_PASSWORD_FLAG_SIZE - LOGIN_FAILURES_SIZE)
#define GET_ADDR_SHOW_PW          (MAX_AVAIL_ADDR - SHOW_PASSWORD_FLAG_SIZE)

//- States
#define STATE_ENTER_MASTER        0x000001                                      // 1       00000000.00000000.00000000.00000001  entering the master password
//#define STATE_AUTHENTICATED     0x000002                                      // 2       00000000.00000000.00000000.00000010  unused
#define STATE_RESET               0x000004                                      // 4       00000000.00000000.00000000.00000100  doing a factory reset
//#define STATE_SHOW_ALPHA        0x000008                                      // 8       00000000.00000000.00000000.00001000  unused
#define STATE_SHOW_MAIN_MENU      0x000010                                      // 16      00000000.00000000.00000000.00010000  showing the main menu
#define STATE_FIND_ACCOUNT        0x000020                                      // 32      00000000.00000000.00000000.00100000  searching for an account
#define STATE_EDIT_STYLE          0x000040                                      // 64      00000000.00000000.00000000.01000000  editing the style for sending username and password
#define STATE_EDIT_USERNAME       0x000080                                      // 128     00000000.00000000.00000000.10000000  entering the username
#define STATE_EDIT_PASSWORD       0x000100                                      // 256     00000000.00000000.00000001.00000000  entering the password
#define STATE_LOGOUT              0x000200                                      // 512     00000000.00000000.00000010.00000000  logged out
#define STATE_EDIT_CREDS_MENU     0x000400                                      // 1024    00000000.00000000.00000100.00000000  showing the edit menu
#define STATE_EDIT_ACCOUNT        0x000800                                      // 2048    00000000.00000000.00001000.00000000  entering the account name
#define STATE_SEND_CREDS_MENU     0x001000                                      // 4096    00000000.00000000.00010000.00000000  showing the menu that sends creds via keyboard
//#define STATE_SEND_USER_AND_PASS0x002000                                      // 8192    00000000.00000000.00100000.00000000  unused
//#define STATE_SEND_PASSWORD     0x004000                                      // 16384   00000000.00000000.01000000.00000000  unused
//#define STATE_SEND_USERNAME     0x008000                                      // 32768   00000000.00000000.10000000.00000000  unused
//#define STATE_DELETE_ACCT       0x010000                                      // 65536   00000000.00000001.00000000.00000000  unused
//#define STATE_ADD_ACCOUNT       0x020000                                      // 131072  00000000.00000010.00000000.00000000  unused
//#define                         0x040000                                      // 262144  00000000.00000100.00000000.00000000  unused
//#define                         0x080000                                      // 524288  00000000.00001000.00000000.00000000  unused
//#define                         0x100000                                      // 1048576 00000000.00010000.00000000.00000000  unused
//#define                         0x200000                                      // 1048576 00000000.00100000.00000000.00000000  unused
//#define                         0x400000                                      // 2097152 00000000.01000000.00000000.00000000  unused

#define LCD_I2C_SLAVE_ADDR        0x27                                          // i2c address of the LCD display
#define LED_I2C_SLAVE_ADDR        0x3C                                          // i2c address of the LED display 
#define LC512_SLAVE_ADDR          0x50                                          // i2c address of the 24LC512 external EEprom chip w/ A0, A1, and A2 grounded.
                                                                                // The address should be 0x50 but the find i2c utility is reporting 0x3F and
                                                                                // that seems to be working. 0x3F w/ LCD, 0x3C w/ LED
                                                                                
#define MAX_IDLE_TIME             3600000                                       // one hour; the idle time allowed before automatic logout
#define LONG_CLICK_LENGTH         500                                           // milliseconds to hold down the rotary encoder button to trigger EVENT_LONG_CLICK
#define UN_PW_DELAY               2000                                          // time in milliseconds to wait after entering username before entering password

                                                                                // char is signed by default. byte is unsigned.
char accountName[ACCOUNT_SIZE];
char username[USERNAME_SIZE];                                                   // holds the username of the current account
byte password[PASSWORD_SIZE];                                                   // holds the password of the current account
char style[STYLE_SIZE];                                                         // holds the style of the current account (<TAB> or <CR> between send username and password)

//- Menus
#define MENU_SIZE                 6                                             // selections in the menu

#define MAIN_MENU_NUMBER          0
#define MAIN_MENU_ELEMENTS        6                                             // number of selections in the main menu
                                                                            
char *mainMenu[] =       {                       "Master Password",             // menu picks appear only on the top line
                                                 "Find Account",                // after an account is found send user sendMenu menu
                                                 "Edit Account",                // sends user to enterMenu menu
                                                 "Logout",                      // locks the user out until master password is re-entered
                                                 "Show Passwrd O  ",            // determines if passwords are displayed or not
                                                 "Reset"            };          // factory reset; erases all creds from memory
#define ENTER_MASTER_PASSWORD     0
#define FIND_ACCOUNT              1
#define EDIT_ACCOUNT              2
#define LOGOUT                    3
#define SET_SHOW_PASSWORD         4
#define FACTORY_RESET             5

#define ENTER_MASTER_POS          0
#define FIND_ACCT_POS             1
#define SET_SHOW_PW_POS           4                                             // array position of the selection that indicates if show password is on or off

int menuNumber = MAIN_MENU_NUMBER;                                              // holds the menu number of the currently displayed menu
int elements = MAIN_MENU_ELEMENTS;                                              // holds the number of selections in the currently displayed menu
char *currentMenu[MENU_SIZE];                                                   // holds the size of the currently displayed menu

#define SEND_MENU_NUMBER          1
#define SEND_MENU_ELEMENTS        6                                             // number of selections in the send creds menu
const char * const sendMenu[] =       {          "Send User & Pass",            // menu picks appear only on the top line
                                                 "Send Password",  
                                                 "Send Username",
                                                 "Send Account",
                                                 "Delete Account",              // TODO: test delete account
                                                 "Backup All",
                                                 ""                 };
#define SEND_USER_AND_PASSWORD    0
#define SEND_PASSWORD             1
#define SEND_USERNAME             2
#define SEND_ACCOUNT              3
#define DELETE_ACCOUNT            4
#define BACKUP_ALL                5

#define EDIT_MENU_NUMBER          2
#define EDIT_MENU_ELEMENTS        6                                             // the number of selections in the menu for editing credentials
const char * const enterMenu[] =       {         "Add Account",
                                                 "Account Name",                // menu picks appear only on the top line
                                                 "Edit Username",  
                                                 "Edit Password",
                                                 "Indicate Style",              // 0, <CR>, 1, <TAB> between username and password when both sent
                                                 "GeneratePasswrd",
                                                 ""                 };
#define ADD_ACCOUNT               0
#define EDIT_ACCT_NAME            1
#define EDIT_USERNAME             2
#define EDIT_PASSWORD             3
#define EDIT_STYLE                4
#define GENERATE_PASSWORD         5

#define ERR_ACCOUNT_POS           -1

#define LEN_ALL_CHARS             93
#define DEFAULT_ALPHA_EDIT_POS    12                                            // allChars is sort of unnecessary TODO: eliminate allChars
const char allChars[LEN_ALL_CHARS] = " 0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz.@-=~!#$%^&*()_+[]{}|;':,<>?/'"; 
                                                                                // used to edit text via rotary encoder

char line1DispBuff[21];                                                         // used to buffer output of line 1 for the led display
char line2DispBuff[21];                                                         // used to buffer output of line 2 for the led display

#ifdef LED_DISPLAY
  const char spaceFilled20[] = "                    ";
#endif
#ifdef INLINE_LED_CODE
  const char spaceFilled16[] = "                ";         
#endif
#ifdef LCD_DISPLAY
  const char spaceFilled16[] = "                ";
#endif

#define TAB_KEY                   KEY_TAB                                       // TAB key is ascii 0x2b (not 0x09) or 0x2b, 0xB3; KEY_TAB from Keyboard.h, 0xB3

byte masterPassword[MASTER_PASSWORD_SIZE];                                      // this is where we store the master password for the device

byte loginFailures;                                                             // count of the number of consecutive login failures since the last successful password entry.

#define MAX_LOGIN_FAILURES        10                                            // "Factory Reset" after MAX_LOGIN_FAILURES attempts to login. Gurads against brute force attack.

byte showPasswordsFlag;                                                         // flag indicating if we show passwords via the UI, or hide them.

//- Global Volatile variables.

volatile byte event = EVENT_NONE;                                               // this is the only variable manipulated in the interrupt.

//- Globals associated with state

long int machineState;                                                          // we might change this so that states are mutually exclusive, it is presently unnecessarily long.
int position = 0;                                                               // the position of the rotary encoder, used to navigate menus and enter text.
int enterPosition = 0;                                                          // when alpha editing w/ rotary encoder, position in the edited word
int acctPosition = 0;                                                           // the positon of the selected account.
int acctCount = 0;                                                              // the number of accounts in EEprom.
boolean authenticated = false;                                                  // indicates if the correct master password has been provided
boolean addAccountFlag = false;                                                 // flipped on when we're adding an account.  Deliberate decision not to put into machineState.
unsigned long lastActivityTime;                                                 // used to automatically logout after a period of inactivity
byte iterationCount = 0;                                                        // # of times ProcessEvent() called since last evaluation of lastActivityTime

//- Object setup

#ifdef LED_DISPLAY
  #if (SSD1306_LCDHEIGHT != 32)
    #error("Height incorrect, please fix Adafruit_SSD1306.h!");
  #endif
  #define OLED_RESET 4
  Adafruit_SSD1306 led(OLED_RESET);                                             // the LED display object.
#endif
#ifdef LCD_DISPLAY
  hd44780_I2Cexp lcd;                                                           // declare lcd object: auto locate & auto config expander chip
#endif 
#ifdef ENCRYPTION
  SHA256 sha256;
  AESSmall128 aes128;
#endif

#ifdef EXTERNAL_EEPROM
  #define WRITE_SUCESS 1
  #define WRITE_ERROR -1
  E24LC256 externalMemory(LC512_SLAVE_ADDR);                                    // the 24LC256 external EEprom memory object
#endif

Rotary rotaryEncoder = Rotary(ROTARY_PIN1, ROTARY_PIN2);                        // the rotary encoder object.
Button2 encoderButton = Button2(BUTTON_PIN);                                    // the button on the rotary encoder.

//- Main Program Control

void setup() {
  #ifdef DEBUG
    Serial.begin(BAUD_RATE);
    while(!Serial);
    delay(50);
    debug(F("Password Pump"));
  #endif
  #ifdef I2C_SCAN
    i2cScan();
  #endif
  
  pinMode(RED_PIN, OUTPUT);                                                     // RGB LED pins
  pinMode(GREEN_PIN, OUTPUT);                                                   // "
  pinMode(BLUE_PIN, OUTPUT);                                                    // "

//pinMode(LED_PIN, OUTPUT);                                                     // TODO: is this used???
  
  pinMode(BUTTON_PIN, INPUT_PULLUP);                                            // setup button pin for input enable internal 20k pull-up resistor, goes LOW when pressed, HIGH when released

  pinMode(ADC_READ_PIN, INPUT);                                                 // this pin will float in a high impedance/Hi-Z state and it's voltage
                                                                                // will be read with every spin to seed the random number generator.
  randomSeed(analogRead(ADC_READ_PIN));                                         // do not ground this pin; use this or randomSeed(millis());

  //encoderButton.setClickHandler(buttonClickHandler);                          // fires after ReleasedHandler on short click
  //encoderButton.setLongClickHandler(buttonLongClickHandler);                  // fires when button is pressed and held
  //encoderButton.setDoubleClickHandler(buttonDoubleClickHandler);              // fires when button is double clicked, do this twice in a row to logout.
  //encoderButton.setTripleClickHandler(buttonTripleClickHandler);              // fires when button is triple clicked, do this twice in a row to reset.
  encoderButton.setReleasedHandler(buttonReleasedHandler);                      // fires when button is released
  //encoderButton.setPressedHandler(buttonPressedHandler);                      // fires when button is pressed
  //encoderButton.setTapHandler(buttonTapHandler);                              // not sure when this fires, but we turn it into EVENT_SINGLE_CLICK.

  #ifdef LED_DISPLAY                                                            // if we're using the LED display
                                                                                // by default, we'll generate the high voltage from the 3.3v line internally.
    led.begin(SSD1306_SWITCHCAPVCC, LED_I2C_SLAVE_ADDR);                        // initialize with the I2C addr LED_I2C_SLAVE_ADDR (0x3C) for the 128x32.
    led.display();                                                              // Show image buffer on the display hardware. TODO: decide if we want the AdaFruit splash screen
    delay(SHOW_ADAFRUIT);                                                       // Since the buffer is intialized with an Adafruit splashscreen
                                                                                // internally, this will display the splashscreen.
    led.setTextSize(1);                                                         // 1 is pretty small, but 2 is too big.
    led.setTextColor(WHITE);
    strcpy(line1DispBuff,"PasswordPump");
    DisplayBuffer();                                                            // presents line1DispBuff and line2DispBuff to user via LED display.
  #endif
  #ifdef INLINE_LED_CODE
    i2c_init();
    ssd1306_init();
    ssd1306_clear(); 
    ssd1306_string_font8x16xy(16, 0, "PasswordPump");
  #endif
  #ifdef LCD_DISPLAY                                                            // otherwise we're using the LCD display
    int status = lcd.begin(LCD_SCREEN_WIDTH, LCD_SCREEN_HEIGHT);
    if(status)                                                                  // non zero status means it was unsuccessful
    {
      debugMetric("Fatal LCD error: ", status);
      status = -status;                                                         // convert negative status value to positive number
                                                                                // begin() failed so blink error code using the onboard LED if possible
      hd44780::fatalError(status);                                              // does not return
    }

    lcd.backlight();                                                            // Turn on the blacklight
    lcd.setCursor(0, 0);
    lcd.print(F("  PasswordPump  "));
  #endif 

  delay(SHOW_SPLASHSCREEN);
  
  #ifdef EXTERNAL_EEPROM
    if(externalMemory.detect()) {                                               // Check if eeprom is connected and detectable
      DisplayLine2("ext eeprom exist");                                      // TODO: send this output to the display device
    } else {
      DisplayLine1("No ext eeprom");
      ErrRpt(ERR_NO_EXT_EEPROM);
    }
  #endif

  loginFailures = getLoginFailures();                                           // getLoginFailures returns a byte.
  if (loginFailures == INITIAL_MEMORY_STATE_BYTE) {
    event = EVENT_RESET;                                                        // this is the first time we're turning on the device, initialize memory
    ProcessEvent();                                                             // the reset event will set login failures and write it to EEprom.
  }

  showPasswordsFlag = getShowPasswordsFlag();                                   // setup the show passwords flag and menu item. (getShowPasswordsFlag returns byte)
  if (showPasswordsFlag == INITIAL_MEMORY_STATE_BYTE ) {                        // this should never be true because the reset event sets the show passwords flag to a value
    showPasswordsFlag = true;                                                   // but, for safety, set the show password flag to ON
    writeShowPasswordsFlag();                                                   // and write it to EEprom.
  }
  setShowPasswordsMenuItem();                                                   // set the menu item to Show Passwrd ON or Show Passwrd OFF.

  countAccounts();                                                              // count the number of populated accounts in EEprom

  PCICR |= (1 << PCIE0);                                                        // Setup interrupts for rotary encoder
  PCMSK0 |= (1 << PCINT4) | (1 << PCINT5);                                      //

  lastActivityTime = millis();                                                  // establish the start time for when the device is powered up
  authenticated = false;
  setBlue();                                                                    // not yet authenticated, LED is blue
  event = EVENT_SHOW_MAIN_MENU;                                                 // first job is to show the first element of the main menu

  sei();                                                                        // Turn on global interrupts
}

void loop() {
  encoderButton.loop();                                                         // polling for button press TODO: replace w/ interrupt
  ProcessEvent();  
}

void ProcessEvent()                                                             // processes events
{ 
  if (event != EVENT_NONE) {
    lastActivityTime = millis();                                                // bump up the lastActivityTime, we don't reset iterationCount here, not necessary and slows responsiveness just a bit
  } else {                                                                      // event == EVENT_NONE
    if (++iterationCount == 255) {                                              // we don't want to call millis() every single time through the loop
      iterationCount = 0;                                                       // necessary?  won't we just wrap around?
      if (millis() < (lastActivityTime + MAX_IDLE_TIME)) {                      // check to see if the device has been idle for MAX_IDLE_TIME milliseconds
        return;                                                                 // not time to logout yet and event == EVENT_NONE, so just return.
      } else {
        event = EVENT_LOGOUT;                                                   // otherwise we've been idle for more than MAX_IDLE_TIME, logout.
      }
    } else {                                                                    // iterationCount is < 255
      return;                                                                   // not time to check millis() yet, just return
    }
  }
 
  if (event == EVENT_ROTATE_CW) {                                               // scroll forward through something depending on state...
    if (((STATE_SHOW_MAIN_MENU) == (machineState & (STATE_SHOW_MAIN_MENU ))) &&
         authenticated                                                      ) { // this prevents navigation away from 'Enter Master Password' when not authenticated.
      if (position < MAIN_MENU_ELEMENTS - 1) {
        position++;
        MenuDown(currentMenu);
        //debugMetric("Position", position);
      }
    } else if ((STATE_ENTER_MASTER   == (machineState & STATE_ENTER_MASTER )) ||
               (STATE_EDIT_ACCOUNT   == (machineState & STATE_EDIT_ACCOUNT )) ||
               (STATE_EDIT_STYLE     == (machineState & STATE_EDIT_STYLE   )) ||
               (STATE_EDIT_USERNAME  == (machineState & STATE_EDIT_USERNAME)) ||
               (STATE_EDIT_PASSWORD  == (machineState & STATE_EDIT_PASSWORD))) {
      if (position < LEN_ALL_CHARS) {
        position++;
      }
      char charToPrint = allChars[position];
      ShowChar(charToPrint, enterPosition);
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)) {
      if (position < SEND_MENU_ELEMENTS - 1) {
        position++;
        MenuDown(currentMenu);
        //debugMetric("Position", position);
      }
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      //debug("EVENT_ROTATE_CW, STATE_EDIT_CREDS_MENU");
      if ((position < EDIT_MENU_ELEMENTS - 1) && (acctCount > 0)) {
        position++;
        MenuDown(currentMenu);
        //debugMetric("acctPosition",acctPosition);
        //debugMetric("position",position);
        SwitchRotatePosition(position);
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {     
      debugMetric("machineState",machineState);
      debugMetric("acctCount",acctCount);
      debugMetric("position",position);
      if (position < (acctCount - 1)) {
        position++;
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
        debugMetric("acctPosition",acctPosition);
      } else {
        position = acctCount - 1;                                               // maximum for position in this state is acctCount
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
        debugMetric("acctPosition",acctPosition);
      }
      debugMetric("Rotate CW; acctPosition",acctPosition);
    }
    event = EVENT_NONE;                                                         // to prevent infinite looping
    
  } else if (event == EVENT_ROTATE_CC) {                                        // scroll backward through something depending on state...
    if ((STATE_SHOW_MAIN_MENU) == (machineState & (STATE_SHOW_MAIN_MENU ))) {
      if (position > 0) {
        position--;
        MenuUp(currentMenu);
        //debugMetric("Position", position);
      }
    } else if ((STATE_ENTER_MASTER  == (machineState & STATE_ENTER_MASTER )) ||
               (STATE_EDIT_ACCOUNT  == (machineState & STATE_EDIT_ACCOUNT )) ||
               (STATE_EDIT_STYLE    == (machineState & STATE_EDIT_STYLE   )) ||
               (STATE_EDIT_USERNAME == (machineState & STATE_EDIT_USERNAME)) ||
               (STATE_EDIT_PASSWORD == (machineState & STATE_EDIT_PASSWORD))) {
      if (position > 0) {
        position--;
      }
      char charToPrint = allChars[position];
      ShowChar(charToPrint, enterPosition);
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)) {
      if (position > 0) {
        position--;
        MenuUp(currentMenu);
        //debugMetric("Position", position);
      }
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      if (position > 0) {
        position--;
        //debug("before MenuUp in EVENT_ROTATE_CC, STATE_EDIT_CREDS_MENU");
        MenuUp(currentMenu);
        //debug("after MenuUp in EVENT_ROTATE_CC, STATE_EDIT_CREDS_MENU");
        SwitchRotatePosition(position);
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {
      if (position > 0) {
        position--;
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
        debugMetric("acctPosition",acctPosition);
      } else {
        position = 0;
        acctPosition = position;
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
        debugMetric("acctPosition",acctPosition);
      }
      debugMetric("Rotate CC; acctPosition",acctPosition);
    }
    event = EVENT_NONE;

  } else if (event == EVENT_SHOW_MAIN_MENU) {                                   // show the main menu
    menuNumber = MAIN_MENU_NUMBER;
    int arraySize = 0;
    for (byte i = 0; i < MENU_SIZE; i++) {
      arraySize += sizeof(mainMenu[i]);  
    }
    memcpy(currentMenu, mainMenu, arraySize);
    elements = MAIN_MENU_ELEMENTS;
    machineState = STATE_SHOW_MAIN_MENU;
    if (authenticated) {
      position = FIND_ACCT_POS; 
    } else {
      position = ENTER_MASTER_POS;
    }
    ShowMenu(position, currentMenu);
    readAcctFromEEProm(acctPosition, accountName);
    if (authenticated) DisplayLine2(accountName);
    event = EVENT_NONE;

  } else if (event == EVENT_SHOW_EDIT_MENU) {                                   // show the main menu
    menuNumber = EDIT_MENU_NUMBER;
    int arraySize = 0;
    for (byte i = 0; i < MENU_SIZE; i++) {
      arraySize += sizeof(enterMenu[i]);  
    }
    memcpy(currentMenu, enterMenu, arraySize);
    elements = EDIT_MENU_ELEMENTS;
    machineState = STATE_EDIT_CREDS_MENU;
    if (position < 0 || position > (EDIT_MENU_ELEMENTS - 1)) position = 0;      // for safety
    ShowMenu(position, currentMenu);
    readAcctFromEEProm(acctPosition, accountName);
    DisplayLine2(accountName);
    event = EVENT_NONE;

  } else if (event == EVENT_LONG_CLICK) {                                       // jump up / back to previous menu 
    if (STATE_ENTER_MASTER == (machineState & STATE_ENTER_MASTER)){
      authenticated = authenticateMaster(masterPassword);                       // authenticateMaster writes to masterPassword ifdef ENCRYPTION
      if (authenticated) {
        position = FIND_ACCT_POS;
        machineState = STATE_SHOW_MAIN_MENU;
        ShowMenu(position, currentMenu);
        DisplayLine2("Authenticated");
        event = EVENT_NONE;
      } else {
        if (loginFailures > MAX_LOGIN_FAILURES) {
          event = EVENT_RESET;                                                  // factory reset after 10 failed attempts to enter master password!
        } else {  
          position = 0;
          machineState = STATE_SHOW_MAIN_MENU;
          ShowMenu(position, currentMenu);
          char buffer[4];
          itoa(loginFailures, buffer, 10);                                      // convert login failures to a string and put it in buffer.
          #ifdef LED_DISPLAY
            strcpy(line2DispBuff,buffer);
            strcat(line2DispBuff, " failure(s)");
            DisplayBuffer();
          #endif
          #ifdef INLINE_LED_CODE
            strcpy(line2DispBuff,buffer);
            strcat(line2DispBuff, " failure(s)");
            DisplayBuffer();
          #endif
          #ifdef LCD_DISPLAY
            lcd.setCursor(0,1);
            PrintBlankLine();
            lcd.setCursor(0,1);
            lcd.print(buffer);
            lcd.setCursor(5,1);
            lcd.print(F(" failure(s)"));
          #endif
          event = EVENT_NONE;
        }
      }
    } else if (STATE_EDIT_ACCOUNT == (machineState & STATE_EDIT_ACCOUNT)) {
      writeToEEProm(ACCOUNT_SIZE, accountName, GET_ADDR_ACCT(acctPosition));    // write the account to EEProm
      addAccountFlag = false;
      position = EDIT_USERNAME;
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_USERNAME == (machineState & STATE_EDIT_USERNAME)) {
      writeToEEProm(USERNAME_SIZE, username, GET_ADDR_USER(acctPosition));      // write the username to EEProm
      position = EDIT_PASSWORD;
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_PASSWORD == (machineState & STATE_EDIT_PASSWORD)) {
      #ifdef ENCRYPTION
        byte pos = 0;
        while (password[pos++] != NULL_TERM);                                   // make sure the password is 16 chars long, pad with NULL_TERM
        while (pos < PASSWORD_SIZE) password[pos++] = NULL_TERM;                // "           "              "
        byte buffer[PASSWORD_SIZE];
        aes128.encryptBlock(buffer, password);                                  // encrypt the password
        for (byte i = 0; i < PASSWORD_SIZE; i++) {                              // copy the encrypted content of buffer back into password
          password[i] = buffer[i];                                              // TODO: use memcpy instead?
        }
      #endif
      writeToEEPromByteArr(PASSWORD_SIZE, password, GET_ADDR_PASS(acctPosition)); // write the password to EEProm
      BlankLine2();                                                             // clear the password off of the display  TODO: check showPasswordFlag?
      position = EDIT_STYLE;
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_STYLE == (machineState & STATE_EDIT_STYLE)) {
      writeToEEProm(STYLE_SIZE, style, GET_ADDR_STYLE(acctPosition));           // write the style to EEProm
      event = EVENT_SHOW_EDIT_MENU;   
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      event = EVENT_SHOW_MAIN_MENU;
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)){
      BlankLine2();
      event = EVENT_SHOW_MAIN_MENU;
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)){      // long click after selecting an account
      machineState = STATE_SHOW_MAIN_MENU;
      ShowMenu(EDIT_MENU_NUMBER, currentMenu);
      DisplayLine2(accountName);                                                // this might be redundant
    } else {
      event = EVENT_SHOW_MAIN_MENU;                                             // if any other state show main menu (e.g just after EVENT_RESET)
    }

  } else if (event == EVENT_SINGLE_CLICK) {
    if (STATE_SHOW_MAIN_MENU == (machineState & STATE_SHOW_MAIN_MENU)) {
      switch(position) {
        case ENTER_MASTER_PASSWORD:                                             // Enter master password
          machineState = STATE_ENTER_MASTER;
          position = DEFAULT_ALPHA_EDIT_POS;                                    // puts the position of the rotary encoder over 'A' for quicker password  entry
          enterPosition = 0;
          char charToPrint[2];
          charToPrint[0] = allChars[enterPosition];
          charToPrint[1] = NULL_TERM;
          DisplayLine2(charToPrint);
          event = EVENT_NONE;
          break;
        case FIND_ACCOUNT:                                                      // Find account
          machineState = STATE_FIND_ACCOUNT;
          position = acctPosition;
          readAcctFromEEProm(acctPosition, accountName);
          DisplayLine2(accountName);
          event = EVENT_NONE;
          break;
        case LOGOUT:                                                            // Logout  DEFECT: why is this being skipped over
          event = EVENT_LOGOUT;                 
          break;
        case SET_SHOW_PASSWORD:                                                                
          showPasswordsFlag = !showPasswordsFlag;
          setShowPasswordsMenuItem();
          DisplayLine1(mainMenu[SET_SHOW_PW_POS]);
          event = EVENT_NONE;
          break;
        case FACTORY_RESET:                                                     // Reset
          event = EVENT_RESET;
          break;
        case EDIT_ACCOUNT:                                                      // Show the enter account menu
          menuNumber = EDIT_MENU_NUMBER;
          elements = EDIT_MENU_ELEMENTS;
          int arraySize = 0;
          for (byte i = 0; i < MENU_SIZE; i++) {
            arraySize += sizeof(enterMenu[i]);  
          }
          memcpy(currentMenu, enterMenu, arraySize);
          elements = EDIT_MENU_ELEMENTS;
          position = 0;
          machineState = STATE_EDIT_CREDS_MENU;
          ShowMenu(position, currentMenu);
          readAcctFromEEProm(acctPosition, accountName);
          DisplayLine2(accountName);
          event = EVENT_NONE;
          break;
        default:
          break;
      }
      if (event == EVENT_SINGLE_CLICK) {                                        // stop the infinite loop of single clicks
        event = EVENT_NONE;
      }
    } else if (STATE_FIND_ACCOUNT == (machineState & STATE_FIND_ACCOUNT)) {     // Go to the send menu
        acctPosition = position;
        menuNumber = SEND_MENU_NUMBER;
        elements = SEND_MENU_ELEMENTS;
        int arraySize = 0;
        for (byte i = 0; i < MENU_SIZE; i++) {
          arraySize += sizeof(sendMenu[i]);  
        }
        memcpy(currentMenu, sendMenu, arraySize);
        elements = SEND_MENU_ELEMENTS;
        position = 0;
        machineState = STATE_SEND_CREDS_MENU;
        ShowMenu(position, currentMenu);
        readAcctFromEEProm(acctPosition, accountName);
        DisplayLine2(accountName);
        event = EVENT_NONE;
    } else if (STATE_EDIT_CREDS_MENU == (machineState & STATE_EDIT_CREDS_MENU)) {
      enterPosition = 0;
      char charToPrint[2];
      charToPrint[0] = allChars[enterPosition];
      charToPrint[1] = NULL_TERM;                                               // TODO: this shouldn't be necessary
      switch(position) {
         case EDIT_ACCT_NAME:                                                   // Enter account name
            machineState = STATE_EDIT_ACCOUNT; 
            position = DEFAULT_ALPHA_EDIT_POS;                                  // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            break; 
         case EDIT_USERNAME:                                                    // Enter username     
            machineState = STATE_EDIT_USERNAME;
            position = DEFAULT_ALPHA_EDIT_POS;                                  // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            break;
         case EDIT_PASSWORD:                                                    // Enter Password   
            machineState = STATE_EDIT_PASSWORD;
            position = DEFAULT_ALPHA_EDIT_POS;                                  // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            break;
         case EDIT_STYLE:
            machineState = STATE_EDIT_STYLE;
            position = 0;                                                       // setting postion for starting alpha input
            DisplayLine2(charToPrint);
            event = EVENT_NONE;
            break;
         case GENERATE_PASSWORD:                                                // Automatic UUID enter password 
            machineState = STATE_EDIT_PASSWORD;                                 // pretend we're entering the password
            setUUID(password);                                                  // put a UUID in the password char array
            BlankLine2();
            event = EVENT_LONG_CLICK;                                           // and trigger long click to write the password to eeprom.
            break;
         case ADD_ACCOUNT:                                                      // Add account
            addAccountFlag = true;
            acctPosition = getNextFreeAcctPos();                                // get the position of the next EEprom location for account name marked empty.
            if (acctPosition != ERR_ACCOUNT_POS) {
              username[0] = NULL_TERM;
              password[0] = NULL_TERM;
              strcpy(accountName,"Add Account");
              DisplayLine2(accountName);
              acctCount++;
            } else {
              DisplayLine2("No room");
            }
            position = EDIT_ACCT_NAME;
            ShowMenu(position, currentMenu);
            event = EVENT_NONE;
            break;
      }
    } else if (STATE_EDIT_ACCOUNT == (machineState & STATE_EDIT_ACCOUNT)) {
      accountName[enterPosition] = allChars[position];                          // set the last char in accountName to be the selected character from allChars
      accountName[enterPosition + 1] = NULL_TERM;                               // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    } else if (STATE_EDIT_USERNAME == (machineState & STATE_EDIT_USERNAME)) {
      username[enterPosition] = allChars[position];
      username[enterPosition + 1] = NULL_TERM;                                  // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    } else if (STATE_EDIT_PASSWORD == (machineState & STATE_EDIT_PASSWORD)) {
      password[enterPosition] = allChars[position];
      password[enterPosition + 1] = NULL_TERM;                                  // push the null terminator out ahead of the last char in the string
      if (!showPasswordsFlag) {                                                 // mask the password being entered if showPasswordsFlag is OFF
        #ifdef LED_DISPLAY
          line2DispBuff[enterPosition] = '*';
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef INLINE_LED_CODE
          line2DispBuff[enterPosition] = '*';
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef LCD_DISPLAY
          enterPosition++;
          lcd.setCursor(enterPosition - 1, 1);  
          lcd.print('*');
        #endif 
      } else {
        #ifdef LED_DISPLAY
          line2DispBuff[enterPosition] = allChars[position];
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef INLINE_LED_CODE
          line2DispBuff[enterPosition] = allChars[position];
          line2DispBuff[enterPosition + 1] = NULL_TERM;                         // push the null terminator out ahead of the last char in the string
          DisplayBuffer();
          enterPosition++;
        #endif
        #ifdef LCD_DISPLAY
          enterPosition++;
          lcd.setCursor(enterPosition,1);
          lcd.print(allChars[position]);
        #endif  
      }
      event = EVENT_NONE;
    } else if (STATE_EDIT_STYLE == (machineState & STATE_EDIT_STYLE)) {
      style[enterPosition] = allChars[position];
      style[enterPosition + 1] = NULL_TERM;                                     // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        line2DispBuff[enterPosition] = allChars[position];
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    } else if (STATE_SEND_CREDS_MENU == (machineState & STATE_SEND_CREDS_MENU)) {
      setPurple();
      switch(position) {
         case SEND_USER_AND_PASSWORD:                                                                
            sendUsernameAndPassword();                                          // SEND THE USERNAME AND PASSWORD
            break; 
         case SEND_PASSWORD:                                                    // Find account
            sendPassword();                                                     // SEND THE USERNAME
            break;
         case SEND_USERNAME:                                                    // Enter account
            sendUsername();                                                     // SEND THE PASSWORD
            break;
         case SEND_ACCOUNT:                                                     // SEND THE ACCOUNT NAME
            sendAccount();
            break;
         case DELETE_ACCOUNT:                                                   // Delete account
            deleteAccount(acctPosition);
            if (acctPosition <= (acctCount - 1)) {                              // if we deleted the last account
              acctCount = acctPosition;                                         // adjust acctCount
            }
            acctPosition = 0;
            DisplayLine2("Account deleted");
            break;
         case BACKUP_ALL:                                                       // Send all
            sendAll();
            break;
      }
      setGreen();
      event = EVENT_NONE;
    } else if (STATE_ENTER_MASTER == (machineState & STATE_ENTER_MASTER)) {
      masterPassword[enterPosition] = allChars[position];
      masterPassword[enterPosition + 1] = NULL_TERM;                            // push the null terminator out ahead of the last char in the string
      #ifdef LED_DISPLAY
        if (showPasswordsFlag) {
          line2DispBuff[enterPosition] = allChars[position];
        } else {
          line2DispBuff[enterPosition] = '*';
        }
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef INLINE_LED_CODE
        if (showPasswordsFlag) {
          line2DispBuff[enterPosition] = allChars[position];
        } else {
          line2DispBuff[enterPosition] = '*';
        }
        line2DispBuff[enterPosition + 1] = NULL_TERM;                           // push the null terminator out ahead of the last char in the string
        DisplayBuffer();
        enterPosition++;
      #endif
      #ifdef LCD_DISPLAY
        enterPosition++;
        lcd.setCursor(enterPosition,1);
        lcd.print(allChars[position]);
      #endif 
      event = EVENT_NONE;
    }

  } else if (event == EVENT_RESET) {
    if (authenticated || (loginFailures > MAX_LOGIN_FAILURES)) {                // TODO: re-enter master password here to authorize creds reset
      setYellow();
      acctCount = 0;
      acctPosition = 0;
      masterPassword[0] = NULL_TERM;                                            // set master password to null terminator in memory
      authenticated = false;                                                    // we're no longer authenticated, we need to re-enter the master password
      DisplayLine2("Initializing...");
      InitializeEEProm();                                                       // write 0xFF to every memory location in EEprom.
      DisplayLine2("Initialized.");                                             // signal memory reset to user
      loginFailures = 0;                                                        // set login failures back to zero, this also serves as a flag to indicate if it's the first power on
      writeLoginFailures();                                                     // write login failure count back to EEprom
      showPasswordsFlag = 1;                                                    // to match the out of box setting (true / 255)
      writeShowPasswordsFlag();                                                 // write show passwords flag back to EEprom
      setShowPasswordsMenuItem();                                               // set the menu item accordingly
      accountName[0] = NULL_TERM;
      password[0] = NULL_TERM;
      username[0] = NULL_TERM;
      DisplayLine2("All creds erased");
    } else { 
      DisplayLine2("Not logged in");
    }
    event = EVENT_SHOW_MAIN_MENU;                                               // if any other state show main menu (e.g just after EVENT_RESET)

  } else if (event == EVENT_LOGOUT) {                                           // TODO: you need to be logged in to logout, check for authentication here
    if(authenticated) {    
      DisplayLine2("Logged out");
      position = 0;
      masterPassword[0] = NULL_TERM;
      authenticated = false;                                                    // we're no longer authenticated, we need to re-enter the master password
      loginFailures = 0;
      writeLoginFailures();
      setBlue();        
      event = EVENT_SHOW_MAIN_MENU;
    } else {
      DisplayLine2("Not logged in");
      event = EVENT_SHOW_MAIN_MENU;
    }
  }
}

void SwitchRotatePosition(int pos) {
  switch(pos) {                                                                 // decide what to print on line 2 of the display
    case ADD_ACCOUNT:
      BlankLine2();
      break;
    case EDIT_ACCT_NAME:
      readAcctFromEEProm(acctPosition, accountName);
      DisplayLine2(accountName);
      //debugNoLF("EVENT_ROTATE_C? STATE_EDIT_CREDS_MENU: ");debug(accountName);
      break;
    case EDIT_USERNAME:
      readUserFromEEProm(acctPosition, username);
      DisplayLine2(username);
      //debug(username);
      break;
    case EDIT_PASSWORD:
      readPassFromEEProm(acctPosition, password);
      if (showPasswordsFlag) {
        DisplayLine2(password);
      } else {
        BlankLine2();
      }
      //debug(password);
      break;
    case EDIT_STYLE:
      readStyleFromEEProm(acctPosition, style);
      DisplayLine2(style);
      break;
    case GENERATE_PASSWORD:
      BlankLine2();
      break;
  }
}

//- Interrupt Service Routines
ISR(PCINT0_vect) {                                                              // Interrupt service routine for rotary encoder
  unsigned char result = rotaryEncoder.process();   
//if (result == DIR_NONE) {                                                     // this actually fires many times per 'click' 
//   debug(F("ISR fired: none"));
//   event = EVENT_NONE;
//} else 
  if (result == DIR_CW) {                                                       // rotated encoder clockwise
    event = EVENT_ROTATE_CW;
    #ifdef DEBUG
      Serial.print("-");
    #endif
  }
  else if (result == DIR_CCW) {                                                 // rotated encoder counter clockwise
    event = EVENT_ROTATE_CC;
    #ifdef DEBUG
      Serial.print(".");
    #endif
  }
}

//- Button

void buttonReleasedHandler(Button2& btn) {
  if(btn.wasPressedFor() > LONG_CLICK_LENGTH) {
    event = EVENT_LONG_CLICK;
  } else {
    event = EVENT_SINGLE_CLICK;
  }
}

/*                                                                              // Leaving these here for potential future use.
void buttonClickHandler(Button2& btn) {
  debugMetric("buttonClick",position);
  event = EVENT_SINGLE_CLICK;
}

void buttonLongClickHandler(Button2& btn) {
  debug("buttonLongClick reset pos");
  event = EVENT_LONG_CLICK;
  position = 0;
}

void buttonPressedHandler(Button2& btn) {
  debug("buttonPressed");
  event = EVENT_PRESSED;
}

void changed(Button2& btn) {
    Serial.println("changed");
}

void buttonDoubleClickHandler(Button2& btn) {
  //debug("buttonDoubleClick");
  event = EVENT_DOUBLE_CLICK;
}

void buttonTripleClickHandler(Button2& btn) {
  //debug("buttonTripleClick");
  event = EVENT_TRIPLE_CLICK;
}

void buttonTapHandler(Button2& btn) {
  debugMetric("buttonTap",position);
  event = EVENT_SINGLE_CLICK;
}
*/

//- Delete Account

void deleteAccount(int position) {
  DisplayLine2("Erasing creds");
  byte emptyPassword[PASSWORD_SIZE];
  for (byte i = 0; i < PASSWORD_SIZE; i++) {
    emptyPassword[i] = NULL_TERM;                                               // to completely overwrite the password in EEProm
  }
  char allBitsOnArray[1];
  allBitsOnArray[0] = INITIAL_MEMORY_STATE_CHAR;                                // this makes the account name free/empty/available
  allBitsOnArray[1] = NULL_TERM;
  char firstCharNullTermArray[1];
  firstCharNullTermArray[0] = NULL_TERM;                                        // equivalent to ""
  writeAllToEEProm( allBitsOnArray,                                             // account:  "-1\0", the -1 signals that the position is free/empty/available.
                    firstCharNullTermArray,                                     // username: "\0", so when it is read it will come back empty
                    emptyPassword,                                              // password: "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", to overwrite the entire pw
                    position                );                                  // position to be deleted
  accountName[0] = NULL_TERM;
  strcpy(password, emptyPassword);
  username[0] = NULL_TERM;
  DisplayLine2("Creds erased");
}

//- UUID Generation

void setUUID(byte *password) {
  for (byte i = 0; i < PASSWORD_SIZE; i++) {
    password[i] = random(33,126);                                               // maybe we should use allChars here instead? We're generating PWs w/ chars that we can't input...
  }                                                                             // 32 = space, 127 = <DEL>, so we want to choose from everything in between.
}

//- Keyboard Functions

void sendAccount() {
  readAcctFromEEProm(acctPosition, accountName);                                // read the account name from EEProm
  DisplayLine2(accountName);                                                    // show on bottom line of display
  #ifdef DEBUG                                                                  // if debugging just print the
    debugNoLF(F("Send Account: "));debug(accountName);                          // account to the serial monitor
  #endif
  Keyboard.begin();                                                             // TODO: can we do a <CTL><A> <BS> here first?  That will clear out pre-populated usernames.
  Keyboard.println(accountName);                                                // type the username through the keyboard
  Keyboard.end();
}

void sendUsername() {
  readUserFromEEProm(acctPosition, username);                                   // read the username from EEProm
  DisplayLine2(username);                                                       // show on bottom line of display
  #ifdef DEBUG                                                                  // if debugging just print the
    debugNoLF(F("Send Username: "));debug(username);                            // username to the serial monitor
  #endif
  Keyboard.begin();                                                             // TODO: can we do a <CTL><A> <BS> here first?  That will clear out pre-populated usernames.
  Keyboard.println(username);                                                   // type the username through the keyboard
  Keyboard.end();
}

void sendPassword() {                                                           // TODO: can we do a <CTL><A> <BS> here first? That will clear out pre-populated passwords.
  readPassFromEEProm(acctPosition, password);                                   // read the password from EEProm
  char passwordChar[PASSWORD_SIZE];
  for (int i = 0; i < PASSWORD_SIZE; i++) {
    passwordChar[i] = (char) password[i];
  }
  #ifdef DEBUG                                                                  // if debugging just print the
    debugNoLF(F("Send Password: "));debug(passwordChar);                        // password to the serial monitor
  #endif
  Keyboard.begin();
  Keyboard.println(passwordChar);                                               // type the password through the keyboard, then <enter>
  Keyboard.end();
}

void sendUsernameAndPassword() {
  readAcctFromEEProm(acctPosition, accountName);                                // TODO: is the read from EEprom necessary at this point?
  DisplayLine2(accountName);
  readUserFromEEProm(acctPosition, username);                                   // read the username from EEProm
  readPassFromEEProm(acctPosition, password);                                   // read the password from EEProm
  char passwordChar[PASSWORD_SIZE];
  for (int i = 0; i < PASSWORD_SIZE; i++) {
    passwordChar[i] = (char) password[i];
  }
  readStyleFromEEProm(acctPosition, style);                                     // read the style from EEprom
  #ifdef DEBUG                                                                  // if debugging just print the
    debug(username);                                                            // username and password to the serial monitor
    debug(passwordChar);                                                        // and password to the serial monitor
    debug(style);
  #endif
  Keyboard.begin();
  int i = 0;
  while (username[i++] != NULL_TERM) {
    Keyboard.write(username[i - 1]);                                            // seems to be a problem only with single character usernames.
  }
  //Keyboard.write(username, i);                                                // these 2 lines can be substituted for the line above (Keyboard.write())
  //Keyboard.print(username);                                                   // type the username through the keyboard
  if ((strcmp(style, "0") == 0              ) ||
      (style[0] == INITIAL_MEMORY_STATE_CHAR)   ) {                             // this should make <CR> the default
    Keyboard.println("");                                                       // send <CR> through the keyboard
  } else {
    Keyboard.press(TAB_KEY);                                                    // if style isn't default or "0" then send <TAB> TODO: test this
    Keyboard.release(TAB_KEY);
  }
  delay(UN_PW_DELAY);
  Keyboard.println(passwordChar);                                               // type the username through the keyboard
  Keyboard.end();
}

void sendAll() {                                                                // this is the function we use to backup all of the accountnames, usernames and passwords
  for(byte acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    #ifdef EXTERNAL_EEPROM
      //char chAry[1];
      //externalMemory.readPage(GET_ADDR_ACCT(acctPos), 1, chAry);
      //char ch = chAry[0];
      byte ch = externalMemory.readByte(GET_ADDR_ACCT(acctPos));
      if (ch != INITIAL_MEMORY_STATE_BYTE) {
    #else
      char ch = (char) EEPROM.read(GET_ADDR_ACCT(acctPos));                     // EEPROM.read returns a byte.  byte = unsigned char.  char = signed char.
      if (ch != INITIAL_MEMORY_STATE_CHAR) {
    #endif
        readAcctFromEEProm(acctPos,accountName);
        readUserFromEEProm(acctPos,username);
        readPassFromEEProm(acctPos,password); 
        char passwordChar[PASSWORD_SIZE];
        for (int i = 0; i < PASSWORD_SIZE; i++) {
          passwordChar[i] = (char) password[i];
        }
        #ifdef DEBUG
          debugMetric("acctPos",acctPos);
          debug(accountName);
          debug(username);
          debug(passwordChar);
          debug(F(""));
        #endif
        Keyboard.begin();
        Keyboard.println(accountName);
        Keyboard.println("");                                                   
        Keyboard.println(username);
        Keyboard.println("");                                                   
        Keyboard.println(passwordChar);
        Keyboard.println("");                                                   
        Keyboard.println("");                                                   // place a carriage return between each account
        Keyboard.end();
      }
  }
}

//- Display Control

void DisplayLine1(char* lineToPrint) {
  #ifdef LED_DISPLAY
    strcpy(line1DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line1DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    BlankLine1();
    lcd.setCursor(0,0);
    lcd.print(lineToPrint);
  #endif 
}

void DisplayLine2(char* lineToPrint) {
  #ifdef LED_DISPLAY
    strcpy(line2DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line2DispBuff, lineToPrint);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    BlankLine2();
    lcd.setCursor(0,1);
    lcd.print(lineToPrint);
  #endif 
}

void BlankLine1() {
  #ifdef LED_DISPLAY
    strcpy(line1DispBuff,spaceFilled20);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line1DispBuff,spaceFilled16);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    lcd.setCursor(0,0);
    PrintBlankLine();
  #endif 
}

void BlankLine2() {
  #ifdef LED_DISPLAY
    strcpy(line2DispBuff,spaceFilled20);
    DisplayBuffer();
  #endif
  #ifdef INLINE_LED_CODE
    strcpy(line2DispBuff,spaceFilled16);
    DisplayBuffer();
  #endif
  #ifdef LCD_DISPLAY
    lcd.setCursor(0,1);
    PrintBlankLine();                                                           // this is where LCD output is trashed, but not hung yet (on 2nd time called)
  #endif 
}

void PrintBlankLine() {
  #ifdef LCD_DISPLAY
    lcd.print(spaceFilled16);                                                   // this is where the LCD is trashed
  #endif 
}

void DisplayBuffer() {
#ifdef LED_DISPLAY
  #ifdef DEBUG
    debugNoLF("line1DispBuff: ");debug(line1DispBuff);
    debugNoLF("line2DispBuff: ");debug(line2DispBuff);
  #endif
  led.clearDisplay();
  led.setCursor(0,0);
  led.print(line1DispBuff);
  led.setCursor(0,LED_LINE_HEIGHT_PIX);
  led.print(line2DispBuff);
  led.display();
#endif
#ifdef INLINE_LED_CODE
  #ifdef DEBUG
    debugNoLF("line1DispBuff: ");debug(line1DispBuff);
    debugNoLF("line2DispBuff: ");debug(line2DispBuff);
  #endif
  ssd1306_clear();
  ssd1306_string_font8x16xy(0, 0, spaceFilled16);
  ssd1306_string_font8x16xy(0, 0, line1DispBuff);
  ssd1306_string_font8x16xy(0, NO_LED_LIB_LIN_HEIGHT_PIX, spaceFilled16);
  ssd1306_string_font8x16xy(0, NO_LED_LIB_LIN_HEIGHT_PIX, line2DispBuff);
#endif
}

void ShowMenu(int position, char **menu) {
  char line1[LCD_SCREEN_WIDTH+1] = "";
  strcat(line1, menu[position]);
  DisplayLine1(line1);
}

void MenuUp(char **menu) { 
  if (position > -1) {
    ShowMenu(position, menu);
  }
}

void MenuDown(char **menu){ 
  if (position < elements) {
    ShowMenu(position, menu);
  }
}

void ShowChar(char charToShow, int pos) {
  #ifdef LED_DISPLAY
    DisplayBuffer();
    led.setCursor(LED_CHAR_WIDTH_PIX * pos,LED_LINE_HEIGHT_PIX);
    led.print(charToShow);
    led.display();
  #endif
  #ifdef INLINE_LED_CODE
    DisplayLine2(line2DispBuff);
    char charToPrint[2];
    charToPrint[0] = charToShow;
    charToPrint[1] = NULL_TERM;
    ssd1306_string_font8x16xy(NO_LED_LIB_CHAR_WIDTH_PIX * pos,NO_LED_LIB_LIN_HEIGHT_PIX,charToPrint);
  #endif
  #ifdef LCD_DISPLAY
    lcd.setCursor(pos,1);
    lcd.print(charToShow);
  #endif
}


void setShowPasswordsMenuItem() {                                               // sets the menu item to be consistent with the showPasswordsFlag.
  if (showPasswordsFlag) {
    mainMenu[SET_SHOW_PW_POS][14] = 'F';
    mainMenu[SET_SHOW_PW_POS][15] = 'F';
    mainMenu[SET_SHOW_PW_POS][16] = NULL_TERM;
  } else {
    mainMenu[SET_SHOW_PW_POS][14] = 'N';
    mainMenu[SET_SHOW_PW_POS][15] = ' ';
    mainMenu[SET_SHOW_PW_POS][16] = NULL_TERM;
  }
}

//- RGB LED

void setPurple() {
  setColor(170, 0, 255);                                                        // Purple Color
}

void setRed(){
  setColor(255, 0, 0);                                                          // Red Color
}

void setGreen(){
  setColor(0, 255, 0);                                                          // Green Color
}


void setYellow(){
  setColor(255, 255, 0);                                                        // Yellow Color
}


void setBlue(){
  setColor(0, 0, 255);                                                          // Blue Color
}

/*
void setWhite(){
  setColor(255, 255, 255);                                                      // White Color
}

void setOff(){
  setColor(0,0,0);                                                              // Off
}

void setOrange(){
  setColor(255, 128, 0);                                                        // Orange Color
}
*/

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(RED_PIN, redValue);
  analogWrite(GREEN_PIN, greenValue);
  analogWrite(BLUE_PIN, blueValue);
}

//- Encryption

boolean authenticateMaster(byte *password) {                                    // verify if the master password is correct here
  char buff[MASTER_PASSWORD_SIZE];                                              // 
  #ifdef EXTERNAL_EEPROM
    //char chAry[1];
    //externalMemory.readPage(GET_ADDR_MASTER_PASS, 1, chAry);
    //char ch = chAry[0];
    //debug("externalMemory.readByte(GET_ADDR_MASTER_PASS);");
    byte ch = externalMemory.readByte(GET_ADDR_MASTER_PASS);
    if (ch == INITIAL_MEMORY_STATE_BYTE) {
  #else
    char ch = (char)EEPROM.read(GET_ADDR_MASTER_PASS);
    if (ch == INITIAL_MEMORY_STATE_CHAR){                                       // first time, we need to write instead of read
  #endif
  #ifdef ENCRYPTION
      byte pos = 0;
      while (password[pos++] != NULL_TERM);                                     // make sure the unencrypted password is 16 chars long
      while (pos < MASTER_PASSWORD_SIZE) password[pos++] = NULL_TERM;           // "           "              "
      sha256Hash(password);                                                     // hash the master password
  #endif
      debugNoLF("eeprom_write_bytes: ");debug(password);
      eeprom_write_bytes(GET_ADDR_MASTER_PASS, password, MASTER_PASSWORD_SIZE); // write the (hased) master password to EEprom
      setGreen();                                                               // turn the RGB green to signal the correct password was provided
    } else {
      if (!eeprom_read_string(GET_ADDR_MASTER_PASS, buff, MASTER_PASSWORD_SIZE)) ErrRpt(ERR_READ_MASTER);
  #ifdef ENCRYPTION
      byte pos = 0;
      while (password[pos++] != NULL_TERM);                                     // make sure the password is 16 chars long
      while (pos < MASTER_PASSWORD_SIZE) password[pos++] = NULL_TERM;           // "           "              "
      sha256Hash(password);                                                     // hash the master password
      if (0 == memcmp(password, buff, MASTER_PASSWORD_SIZE)) {                  // entered password hash matches master password hash, authenticated
  #else
      if (0 == strcmp(password, buff)) {                                        // entered password matches master password, authenticated
  #endif
        setGreen();                                                             // turn the RGB green to signal the correct password was provided
        loginFailures = 0;                                                      // reset loginFailues to zero
        writeLoginFailures();                                                   // record loginFailures in EEprom
                                                                                // encrypt a word using the master password as the key
      } else {                                                                  // failed authentication
// Begin: decoy password comment                                                // Following section commented out because decoy logic needs to change to accomodate a hashed master password
//        if (0 == strcmp(password,strcat(buff,"FR"))) {                        // check for decoy password; masterPassword + "FR".
//          loginFailures = MAX_LOGIN_FAILURES + 2;
//          event = EVENT_RESET;
//          ProcessEvent();
//        } else {
// End: decoy password comment
        setRed();                                                               // turn the RGB red to signal the incorrect password was provided
        loginFailures++;
        writeLoginFailures();
        return false;
      }
    }
    #ifdef ENCRYPTION
      byte pos = 0;
      while (password[pos++] != NULL_TERM);                                     // make sure the password is 16 chars long
      while (pos < MASTER_PASSWORD_SIZE) password[pos++] = NULL_TERM;           // "           "              "
      aes128.setKey(password, MASTER_PASSWORD_SIZE);                            // set the key for acorn128 to equal the master password
    #endif
  return true;
}                                                                               // and check it against the same word that's stored encrypted
                                                                                // in eeprom.  This word is written (encrypted) to eeprom the 
                                                                                // first time ever a master password is entered.

#ifdef ENCRYPTION
void sha256Hash(char *password)
{
    size_t size = strlen(password);
    size_t posn, len;
    uint8_t value[MASTER_PASSWORD_SIZE];

    sha256.reset();
    for (posn = 0; posn < size; posn += MASTER_PASSWORD_SIZE) {
        len = size - posn;
        if (len > MASTER_PASSWORD_SIZE)
            len = MASTER_PASSWORD_SIZE;
        sha256.update(password + posn, len);
    }
    sha256.finalize(value, sizeof(value));
    for (int i = 0; i < MASTER_PASSWORD_SIZE; i++) {
      password[i] = value[i];
    }
}
#endif

//- EEPROM functions

void writeAllToEEProm(String accountName, String username, byte *password, int pos) { // used by delete account and factory reset.
  writeToEEProm(ACCOUNT_SIZE, accountName, GET_ADDR_ACCT(pos));
  writeToEEProm(USERNAME_SIZE, username, GET_ADDR_USER(pos));
  writeToEEPromByteArr(PASSWORD_SIZE, password, GET_ADDR_PASS(pos));
}

void writeToEEPromByteArr(int bufsize, byte *buf, unsigned int ee_addr) {
  #ifdef ENCRYPTION                                                             // TODO: this precompiler directive might not be necessary
    if (!eeprom_write_bytes(ee_addr, buf, bufsize))                             // if we write thusly in both situations.
      ErrRpt(ERR_EEPROM_WRITE);
  #else 
    if (!eeprom_write_string(ee_addr, buf)) 
      ErrRpt(ERR_EEPROM_WRITE);                                                 
  #endif
}

void writeToEEProm(int bufsize, String myString, unsigned int ee_addr) {        // TODO: change the String parameter to char *
  char buf[bufsize];
  myString.toCharArray(buf, bufsize);                                           // convert string to char array (TODO: is this necessary?)
  if (!eeprom_write_string(ee_addr, buf)) ErrRpt(ERR_EEPROM_WRITE);             // the return status from eeprom_write_string should be sent back to caller
}

void countAccounts() {                                                          // count all of the account names from EEprom.
  for(byte acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    #ifdef EXTERNAL_EEPROM
      //char chAry[1];
      //externalMemory.readPage(GET_ADDR_ACCT(acctPos), 1, chAry);
      //byte ch = chAry[0];
      byte ch = externalMemory.readByte(GET_ADDR_ACCT(acctPos));
      if (ch != INITIAL_MEMORY_STATE_BYTE) {
    #else
      byte ch = EEPROM.read(GET_ADDR_ACCT(acctPos));
      if (ch != INITIAL_MEMORY_STATE_BYTE) {
    #endif
        acctCount++;
      }
  }
  debugMetric("acctCount",acctCount);
}

int getNextFreeAcctPos() {                                                      // return the position of the next EEprom location for account name marked empty.
  for(byte acctPos = 0; acctPos < CREDS_ACCOMIDATED; acctPos++) {
    #ifdef EXTERNAL_EEPROM
      //char chAry[1];
      //externalMemory.readPage(GET_ADDR_ACCT(acctPos), 1, chAry);
      //char ch = chAry[0];
      byte ch = externalMemory.readByte(GET_ADDR_ACCT(acctPos));
      if (ch == INITIAL_MEMORY_STATE_BYTE) {
    #else
      byte ch = EEPROM.read(GET_ADDR_ACCT(acctPos));                            // TODO: this is skipping some free addresses! Overwritten by password write?
      if (ch == INITIAL_MEMORY_STATE_BYTE) {
    #endif
        debugMetric("getNextFreeAcctPos acctPos",acctPos);
        return acctPos;
      }
  }
  return ERR_ACCOUNT_POS;
}

void readAcctFromEEProm(int pos, char *buf) {
  if (pos > -1) {
    if (!eeprom_read_string(GET_ADDR_ACCT(pos), buf, ACCOUNT_SIZE)) ErrRpt(ERR_READ_ACCOUNT_2);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_CHAR) buf[0] = NULL_TERM;                  // 8 bit twos complement of 255 or 0xFF
}

void readUserFromEEProm(int pos, char *buf) {
  if (pos > -1) {
    if (!eeprom_read_string(GET_ADDR_USER(pos), buf, USERNAME_SIZE)) ErrRpt(ERR_READ_USER);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_CHAR) buf[0] = NULL_TERM;
}

void readStyleFromEEProm(int pos, char *buf) {
  if (pos > -1) {
    if (!eeprom_read_string(GET_ADDR_STYLE(pos), buf, STYLE_SIZE)) ErrRpt(ERR_READ_STYLE);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_CHAR) buf[0] = NULL_TERM;
}

void readPassFromEEProm(int pos, byte *buf) {
  if (pos > -1) {
    if (!eeprom_read_byteArr(GET_ADDR_PASS(pos), buf, PASSWORD_SIZE)) ErrRpt(ERR_READ_PASS);
  } else {
    buf[0] = NULL_TERM;
  }
  if (buf[0] == INITIAL_MEMORY_STATE_CHAR) {
    buf[0] = NULL_TERM;
  } else {
    #ifdef ENCRYPTION
      debugMetric("pos",pos);
      debugNoLF("1) buf[]: ");debug(buf);
      byte byteArrayFrom[PASSWORD_SIZE];
      byte byteArrayTo[PASSWORD_SIZE];
      for (byte i = 0; i < PASSWORD_SIZE; i++) {
        byteArrayFrom[i] = buf[i];
      }
      aes128.decryptBlock(byteArrayTo, byteArrayFrom);                          // decrypt the password TODO: can you just use buf here instead of byteArrayFrom?
      for (byte i = 0; i < PASSWORD_SIZE; i++) {
        buf[i] = byteArrayTo[i];
      }
      debugNoLF("2) buf[]: ");debug(buf);
    #endif
  }
}

byte getLoginFailures() {
  #ifdef EXTERNAL_EEPROM
    //char chAry[1];
    //externalMemory.readPage(GET_ADDR_LOGIN_FAILURES, 1, chAry);
    //byte ch = chAry[0];
    //debug("b4 externalMemory.readByte");
    byte rtn = externalMemory.readByte(GET_ADDR_LOGIN_FAILURES);
    return rtn;
  #else
    return EEPROM.read(GET_ADDR_LOGIN_FAILURES);
  #endif
}

byte getShowPasswordsFlag() {
  #ifdef EXTERNAL_EEPROM
    //char chAry[1];
    //externalMemory.readPage(GET_ADDR_SHOW_PW, 1, chAry);
    //byte ch = chAry[0];
    //return ch;
    //debug("b4 externalMemory.readByte");
    byte rtn = externalMemory.readByte(GET_ADDR_SHOW_PW);
    //debug("after externalMemory.readByte 2");
    return rtn;
  #else
    return EEPROM.read(GET_ADDR_SHOW_PW);
  #endif
}

void writeLoginFailures() {
  cli();                                                                        // disable interrupts
  #ifdef EXTERNAL_EEPROM
    //char buffer[1];
    //buffer[0] = loginFailures;
    //externalMemory.writePage(GET_ADDR_LOGIN_FAILURES, sizeof(byte), buffer);
    //debug("b4 externalMemory.writeByte");
    externalMemory.writeByte(GET_ADDR_LOGIN_FAILURES, loginFailures);
    //debug("after externalMemory.writeByte");
  #else
    EEPROM.write(GET_ADDR_LOGIN_FAILURES, loginFailures);
  #endif
  sei();                                                                        // re-enable interrupts
}

void writeShowPasswordsFlag() {
  cli();                                                                        // disable interrupts
  #ifdef EXTERNAL_EEPROM
    //char buffer[1];
    //buffer[0] = showPasswordsFlag;
    //externalMemory.writePage(GET_ADDR_SHOW_PW, sizeof(byte), buffer);
    externalMemory.writeByte(GET_ADDR_SHOW_PW, showPasswordsFlag);
  #else
    EEPROM.write(GET_ADDR_SHOW_PW, showPasswordsFlag);
  #endif
  sei();                                                                        // re-enable interrupts
}
                                                                                // Following code is from smching: https://gist.github.com/smching/05261f11da11e0a5dc834f944afd5961 
                                                                                // This function is used by the other, higher-level functions
                                                                                // to prevent bugs and runtime errors due to invalid addresses.
boolean eeprom_is_addr_ok(unsigned int addr) {                                  // Returns true if the address is between the
  return ((addr >= MIN_AVAIL_ADDR) && (addr <= MAX_AVAIL_ADDR));                // minimum and maximum allowed values, false otherwise.
}
                                                                                // Writes a sequence of bytes to eeprom starting at the specified address.
                                                                                // Returns true if the whole array is successfully written.
                                                                                // Returns false if the start or end addresses aren't between
                                                                                // the minimum and maximum allowed values.
                                                                                // When returning false, nothing gets written to eeprom.
boolean eeprom_write_bytes(unsigned int startAddr, const byte* array, int numBytes) {
                                                                                // counter
  int i;
                                                                                // both first byte and last byte addresses must fall within
                                                                                // the allowed range 
  if (!eeprom_is_addr_ok(startAddr) || !eeprom_is_addr_ok(startAddr + numBytes)) {
    return false;
  }
  cli();                                                                        // disable interrupts
  for (i = 0; i < numBytes; i++) {
    #ifdef EXTERNAL_EEPROM
      //char buffer[1];
      //buffer[0] = array[i];
      //externalMemory.writePage(startAddr + i, sizeof(byte), buffer);
      externalMemory.writeByte(startAddr + i, array[i]);
    #else
      EEPROM.write(startAddr + i, array[i]);
    #endif
  }
  sei();                                                                        // re-enable interrupts
  return true;
}

                                                                                // Writes a string starting at the specified address.
                                                                                // Returns true if the whole string is successfully written.
                                                                                // Returns false if the address of one or more bytes fall outside the allowed range.
                                                                                // If false is returned, nothing gets written to the eeprom.
boolean eeprom_write_string(unsigned int addr, const char* string) {
  int numBytes;                                                                 // actual number of bytes to be written
                                                                                // write the string contents plus the string terminator byte (0x00)
  numBytes = strlen(string) + 1;
  return eeprom_write_bytes(addr, (const byte*)string, numBytes);
}

                                                                                // Reads a string starting from the specified address.
                                                                                // Returns true if at least one byte (even only the string terminator one) is read.
                                                                                // Returns false if the start address falls outside the allowed range or declare buffer size is zero.
                                                                                // 
                                                                                // The reading might stop for several reasons:
                                                                                // - no more space in the provided buffer
                                                                                // - last eeprom address reached
                                                                                // - string terminator byte (0x00) encountered.
boolean eeprom_read_string(unsigned int addr, unsigned char* buffer, int bufSize) {
#ifdef EXTERNAL_EEPROM
  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
  externalMemory.readPage(addr, bufSize, buffer);
  return true;
#else
  byte ch;                                                                      // byte read from eeprom
  int bytesRead;                                                                // number of bytes read so far
  if (!eeprom_is_addr_ok(addr)) {                                               // check start address
    return false;
  }

  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
                                                                                // is there is room for the string terminator only, no reason to go further TODO: is this right?
  if (bufSize == 1) {                                                           // can we delete this logic block alltogether?  Is the null terminator consuming the last array element of buffer?
    buffer[0] = NULL_TERM;
    return true;
  }
  bytesRead = 0;                                                                // initialize byte counter
  ch = EEPROM.read(addr + bytesRead);                                           // read next byte from eeprom
  buffer[bytesRead] = ch;                                                       // store it into the user buffer
  bytesRead++;                                                                  // increment byte counter
                                                                                // stop conditions:
                                                                                // - the character just read is the string terminator one (0x00)
                                                                                // - we have filled the user buffer
                                                                                // - we have reached the last eeprom address
  while ( (bytesRead < bufSize) && ((addr + bytesRead) <= MAX_AVAIL_ADDR) ) {   // eliminate check for NULL_TERM because of hashing
    // if no stop condition is met, read the next byte from eeprom
    ch = EEPROM.read(addr + bytesRead);
    buffer[bytesRead] = ch;                                                     // store it into the user buffer
    bytesRead++;                                                                // increment byte counter
  }
  return true;
#endif
}                                                                               // end smching

boolean eeprom_read_byteArr(unsigned int addr, byte* buffer, int bufSize) {
#ifdef EXTERNAL_EEPROM
  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
  externalMemory.readPage(addr, bufSize, buffer);
  return true;
#else
  int bytesRead;                                                                // number of bytes read so far
  if (!eeprom_is_addr_ok(addr)) {                                               // check start address
    return false;
  }

  if (bufSize == 0) {                                                           // how can we store bytes in an empty buffer ?
    return false;
  }
                                                                                // is there is room for the string terminator only, no reason to go further
  bytesRead = 0;                                                                // initialize byte counter
  buffer[bytesRead] = EEPROM.read(addr + bytesRead);                            // read next byte from eeprom, store it into the user buffer
  bytesRead++;                                                                  // increment byte counter
                                                                                // stop conditions:
                                                                                // - we have filled the user buffer
  while ( (bytesRead < bufSize) && ((addr + bytesRead) <= MAX_AVAIL_ADDR) ) {   // - we have reached the last eeprom address
                                                                                // if no stop condition is met, read the next byte from eeprom
    buffer[bytesRead] = EEPROM.read(addr + bytesRead);                          // store it into the user buffer
    bytesRead++;                                                                // increment byte counter
  }

  return true;
#endif
}                                                                               // end smching

void InitializeEEProm() {
  boolean colorPurple = false;
  for (unsigned int addr = MIN_AVAIL_ADDR; addr <= MAX_AVAIL_ADDR; addr++) {
    if(addr%256==0) {                                                           // flash the RGB so that the user knows something is happening
      if (colorPurple) {
        setYellow();
        colorPurple = false;
      } else {
        setPurple();
        colorPurple = true;
      }
    }
    #ifdef EXTERNAL_EEPROM
        //byte buffer[1];
        //buffer[0] = INITIAL_MEMORY_STATE_BYTE;
        //externalMemory.writePage(addr, sizeof(char), buffer);
        externalMemory.writeByte(addr, INITIAL_MEMORY_STATE_BYTE);              // TODO: evaluate return code, WRITE_SUCCESS or WRITE_ERROR, send result to display device
    #else
        EEPROM.write(addr, INITIAL_MEMORY_STATE_BYTE);                          // second parameter is a byte.  TODO: do a bulk write to improve speed
    #endif
  }
  setBlue();
}

void ErrRpt(int err) {                                                          // Expects an error number to be passed in. Using numbers to save on global memory
  char buf[5];
  itoa(err, buf, 10);                                                           // convert the int to a string, base 10.
  char message[21] = "Error: ";
  strcat(message, buf);
  DisplayLine2(message);                                                        // Print the error number on line 2 of the display (e.g. "Err: 2" for error reading the account)
}

//- SSD1306 Routines
#ifdef INLINE_LED_CODE
  const uint8_t ssd1306_init_sequence [] PROGMEM = {                            // Initialization Sequence
    0xAE,                                                                       // Display OFF (sleep mode)
    0x20, 0b00,                                                                 // Set Memory Addressing Mode
                                                                                // 00=Horizontal Addressing Mode; 01=Vertical Addressing Mode;
                                                                                // 10=Page Addressing Mode (RESET); 11=Invalid
    0xB0,                                                                       // Set Page Start Address for Page Addressing Mode, 0-7
    0xC8,                                                                       // Set COM Output Scan Direction
    0x00,                                                                       // ---set low column address
    0x10,                                                                       // ---set high column address
    0x40,                                                                       // --set start line address
    0x81, 0xFF,                                                                 // Set contrast control register 0x3F
    0xA1,                                                                       // Set Segment Re-map. A0=address mapped; A1=address 127 mapped. 
    0xA6,                                                                       // Set display mode. A6=Normal; A7=Inverse
    0xA8, 0x3F,                                                                 // Set multiplex ratio(1 to 64)
    0xA4,                                                                       // Output RAM to Display
                                                                                // 0xA4=Output follows RAM content; 0xA5,Output ignores RAM content
    0xD3, 0x00,                                                                 // Set display offset. 00 = no offset
    0xD5,                                                                       // --set display clock divide ratio/oscillator frequency
    0xF0,                                                                       // --set divide ratio
    0xD9, 0x22,                                                                 // Set pre-charge period
    0xDA, 0x12,                                                                 // Set com pins hardware configuration    
    0xDB,                                                                       // --set vcomh
    0x20,                                                                       // 0x20,0.77xVcc
    0x8D, 0x14,                                                                 // Set DC-DC enable
    0xAF                                                                        // Display ON in normal mode
  
  };

  void ssd1306_init(void)
  {
    for (uint8_t i = 0; i < sizeof (ssd1306_init_sequence); i++) {
      ssd1306_send_command(pgm_read_byte(&ssd1306_init_sequence[i]));
    }
  }

  void ssd1306_send_command(uint8_t command)
  {
    i2c_start();
    i2c_writebyte(SSD1306_SA);                                                  // Slave address, SA0=0
    i2c_writebyte(0x00);                                                        // write command
    i2c_writebyte(command);
    i2c_stop();
  }

  void ssd1306_setpos(uint8_t x, uint8_t y)
  {
    i2c_start();
    i2c_writebyte(SSD1306_SA);                                                  // Slave address, SA0=0
    i2c_writebyte(0x00);                                                        // write command
    i2c_writebyte(0xb0 + y);
    i2c_writebyte(((x & 0xf0) >> 4) | 0x10);                                    // | 0x10
    i2c_writebyte((x & 0x0f));                                                  // | 0x01
    i2c_stop();
  }

  void ssd1306_fill4(uint8_t p1, uint8_t p2, uint8_t p3, uint8_t p4) 
  {
    ssd1306_setpos(0, 0);
    i2c_start();
    i2c_writebyte(SSD1306_SA);
    i2c_writebyte(0x40);                                                        // write data
    for (uint16_t i = 0; i < 128 * 8 / 4; i++) {
      i2c_writebyte(p1);
      i2c_writebyte(p2);
      i2c_writebyte(p3);
      i2c_writebyte(p4);
    }
    i2c_stop();
  }

  void ssd1306_fill(uint8_t p) {
    ssd1306_fill4(p, p, p, p);
  }

  void ssd1306_string_font8x16xy(uint8_t x, uint8_t y, const char s[]) {
    uint8_t ch, j = 0;
    while (s[j] != '\0') {
      ch = s[j] - 32;
      if (x > 120) {
        x = 0;
        y++;
      }
      ssd1306_setpos(x, y);
      i2c_start();
      i2c_writebyte(SSD1306_SA);
      i2c_writebyte(0x40);                                                      // write data
      for (uint8_t i = 0; i < 8; i++) {
        i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[ch * 16 + i]));
      }
      i2c_stop();
      ssd1306_setpos(x, y + 1);
      i2c_start();
      i2c_writebyte(SSD1306_SA);
      i2c_writebyte(0x40);                                                      // write data
      for (uint8_t i = 0; i < 8; i++) {
        i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[ch * 16 + i + 8]));
      }
      i2c_stop();
      x += 8;
      j++;
    }
  }

  void ssd1306_char_font8x16(char ch) 
  {
    uint8_t c = ch - 32;
    i2c_start();
    i2c_writebyte(SSD1306_SA);
    i2c_writebyte(0x40);                                                        // write data
    for (uint8_t i = 0; i < 8; i++) {
      i2c_writebyte(pgm_read_byte(&ssd1306xled_font8x16[c * 16 + i]));
    }
    i2c_stop();
  }

//- Software I2C Routines
  void i2c_init() {
    SDA_DIR |= (1 << SDA_BIT);                                                  // Set port as output
    SCL_DIR |= (1 << SCL_BIT);                                                  // Set port as output
  }

  void i2c_start(void) {
    SCL_high();
    SDA_high();
    SDA_low();
    SCL_low();
  }

  void i2c_stop(void) {
    SCL_low();
    SDA_low();
    SCL_high();
    SDA_high();
  }

  void i2c_writebyte(uint8_t byte) {
    uint8_t i;
    for (i = 0; i < 8; i++) {
      if ((byte << i) & 0x80) {
        SDA_high();
      } else {
        SDA_low();
      }
      SCL_high();
      SCL_low();
    }
    SDA_high();
    SCL_high();
    SCL_low();
  }
#endif

//- Debugging Routines                                                          // These routines are helpful for debugging.
                                                                                // For sending output to the serial monitor. Set the baud rate in setup.
#ifdef DEBUG
  void debug(String text) {
    Serial.println(text);
  }

  void debugNoLF(String text) {
    Serial.print(text);
  }

  void debugInt(signed int anInt) {
    char myInt[10];
    itoa(anInt,myInt,10);
    debug(myInt);
  }

  void debugMetric(const char myString[], signed int anInt) {
    debugNoLF(myString);debugNoLF(F(": "));
    debugInt(anInt);
    Serial.print(F("\r\n"));
  }
#else
  void debug(String text) {
  }

  void debugNoLF(String text) {
  }

  void debugInt(signed int anInt) {
  }

  void debugMetric(const char myString[], signed int anInt) {
  }
#endif

#ifdef I2C_SCAN
  void i2cScan() {
    byte error, address;
    int nDevices;
  
    Serial.println("Scanning...");
   
    nDevices = 0;
    for(address = 1; address < 127; address++ ) {
                                                                                // The i2c_scanner uses the return value of
                                                                                // the Write.endTransmisstion to see if
                                                                                // a device did acknowledge to the address.
      Wire.beginTransmission(address);
      error = Wire.endTransmission();
  
      if (error == 0)
      {
        Serial.print("I2C device found at address 0x");
        if (address<16)
        Serial.print("0");
        Serial.print(address,HEX);
        Serial.println("  !");
  
        nDevices++;
      }
      else if (error==4)
      {
        Serial.print("Unknown error at address 0x");
        if (address<16)
        Serial.print("0");
        Serial.println(address,HEX);
      }    
    }
    if (nDevices == 0)
      Serial.println("No I2C devices found\n");
    else
      Serial.println("done\n");
  }
#endif