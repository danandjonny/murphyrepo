Daniel Murphy
46 Williams Dr.
Lunenburg, MA 01462





Dylan Martin
510 State St.
Adrian, Michigan  49221



Hi Mr. Martin,

Thanks for ordering the PasswordPump Kit.  Apologies for the delay, it took a long
time for the 25LC256 EEprom chips to finally arrive from China.  They're also 
expensive at almost $2 each.  

When you're soldering the RGB LED into place please be careful not to bridge the 
gaps between the leads with solder.  I always use the continuity function on my
multimeter to make sure that none of the leads are connected together.  Outside
of that the soldering should be straight forward.  The program is already flashed
onto the Arduino Micro.  There are operating instructions on 5volts.org.

Please be aware that the micro USB connection is fragile.  Suggestions for 
dealing with this also appear up on 5volts.org.

If you have any problems or are unhappy for any reason send me an email or call 
me at 978-809-0770 and I'm happy to help you out.  Please be aware that we're in 
different time zones.

Best of Luck,

Dan