#include <ESP8266WiFi.h>                                                // include libraries
#include <ESP8266WebServer.h>
#include <Servo.h> 
#include "WEMOS_Motor.h"
#include <stdlib.h>

#define dirBackwards    LOW
#define dirForwards     HIGH 
#define SERVOPIN        16                                              // = D0 pin to which the servo is attached
#define STRAIGHT        90                                              // 0 = left, 90 = straight. 180 = right    
#define OFFSET          0
#define HARDLEFT        45      
#define HARDRIGHT       135                                             // 115
#define DIFFERENTIAL    50
#define STOP            0
#define TRUE            1
#define FALSE           0
#define TRIGGER         14                                              // D5
#define ECHO            12                                              // D6

#define GOLEFT            1
#define GORIGHT           2
#define GOSTRAIGHT        3
#define EVADE             4
#define MIN_EVADE_DIS     50                                            // centimeters
#define TIME_TO_EVADE     2000                                          // milliseconds
#define SLEEP_FORWARD     1000                                          // milliseconds
#define SPEED_STEP        10                                            // percent of duty cycle increments/decrements
#define MAX_SPEED         100
#define MEDIUM_SPEED      75                                            // % duty cycle
#define MIN_SPEED         0                                             // % duty cycle
#define CHANGE_DIR_TIME   250
#define SWEEP_INCREMENT   5                                             // degrees
#define SERVO_SHIFT_TIME  150                                           // milliseconds
byte lastDirection;
boolean isLeft = FALSE;
boolean isRight = FALSE;
boolean isStraight = TRUE;
boolean autonomous = FALSE;
int vehicleSpeed = MEDIUM_SPEED;
long duration, distance;

Motor M1(0x30,_MOTOR_A, 1000);                                          //Motor A Motor shiled I2C Address: 0x30, PWM frequency: 1000Hz(1kHz)
Motor M2(0x30,_MOTOR_B, 1000);                                          //Motor B

Servo servo;                                                            // define variable for the Servo object

ESP8266WebServer server(80);                                            // configure server
//const char *form = "<center><form action='/'>"
String form;
String formBase = "<center><form action='/'>"
"<button name='dir' type='submit' value='4'>Go</button><br>"
"<button name='dir' type='submit' value='1'>Left</button>"
"<button name='dir' type='submit' value='6'>Straight</button>"
"<button name='dir' type='submit' value='2'>Right</button><br>"
"<button name='dir' type='submit' value='3'>Reverse</button><br>"
"<button name='dir' type='submit' value='5'>Stop</button><br>"
"<button name='dir' type='submit' value='7'>Show Distance</button><br>"
"<button name='dir' type='submit' value='8'>Autonomous</button><p>"
"<button name='dir' type='submit' value='9'>Faster</button>"
"<button name='dir' type='submit' value='10'>Slower</button>"
"</form></center>";

int calcAngle(int myAngle) {
    return myAngle + OFFSET;
}

void stopMotor(void) {
    Serial.println("Stop");
    M1.setmotor(_STOP);
    M2.setmotor( _STOP);
//  M1.setmotor(_SHORT_BRAKE);
//  M2.setmotor( _SHORT_BRAKE);
//  M1.setmotor(_STANDBY);
//  M2.setmotor( _STANDBY);
}

void forwardMotor(void) {
    Serial.println("Go");
    M1.setmotor( _CW, vehicleSpeed);
    M2.setmotor(_CW, vehicleSpeed);
}

void reverseMotor(void) {
    Serial.println("Backward");
    M1.setmotor( _CCW, vehicleSpeed);
    M2.setmotor(_CCW, vehicleSpeed);
}

void left(void) {
    Serial.println("Left");
//  M1.setmotor( _CW, vehicleSpeed - DIFFERENTIAL);
//  M2.setmotor(_CW, vehicleSpeed);
    servo.write(calcAngle(HARDLEFT));
    isLeft      = TRUE;
    isRight     = FALSE;
    isStraight  = FALSE;
    delay(CHANGE_DIR_TIME);                                               // wait for the servo to arrive at it's destination
}

void right(void) {
    Serial.println("Right");
//  M1.setmotor( _CW, vehicleSpeed);
//  M2.setmotor(_CW, vehicleSpeed - DIFFERENTIAL);
    servo.write(calcAngle(HARDRIGHT));
    isLeft      = FALSE;
    isRight     = TRUE;
    isStraight  = FALSE;
    delay(CHANGE_DIR_TIME);                                               // wait for the servo to arrive at it's destination
}

void straight(void) {
    Serial.println("Straight");
    servo.write(calcAngle(STRAIGHT));
    if (isLeft) {
      servo.write(calcAngle(STRAIGHT));
    } else if (isRight) {
//    servo.write(calcAngle(STRAIGHT));
      servo.write(calcAngle(HARDLEFT));                               // when pointing right it was not returning to STRAIGHT, so this workaround was
      delay(200);                                                     // implemented.  Regardless of power supply
      servo.write(calcAngle(STRAIGHT));
    }
    isLeft     = FALSE;
    isRight    = FALSE;
    isStraight = TRUE;
    delay(CHANGE_DIR_TIME);                                               // wait for the servo to arrive at it's destination
}

long readDistance() {
    long long durationTotal = 0;
    long maxDuration = 0;
    long minDuration = 2147483647;
    for (int i = 0; i < 10; i++) {    
      digitalWrite(TRIGGER, LOW);  
      delayMicroseconds(2); 
      
      digitalWrite(TRIGGER, HIGH);
      delayMicroseconds(10); 
      
      digitalWrite(TRIGGER, LOW);
      duration = pulseIn(ECHO, HIGH);
      durationTotal += duration;
      if(duration > maxDuration) {
        maxDuration = duration;
      } 
      if (duration < minDuration) {
        minDuration = duration;
      }
      delay(10);                                                          // 10 milliseconds
    }

    durationTotal -= maxDuration;
    durationTotal -= minDuration;
    distance = ((durationTotal/8)/2) / 29.1;

    if(!autonomous) {
      char distanceStr[256];
      sprintf(distanceStr, "%ld", distance);
      form = formBase + "Centimeters: " + distanceStr;
    }
    
    return distance;
}

int pointToBestOpening() {
  long distance = 0;
  long maxDistance = 0;
  int maxDistanceDegrees = HARDLEFT;
  for (int degrees = HARDLEFT; degrees <= HARDRIGHT; degrees+=SWEEP_INCREMENT){
    servo.write(calcAngle(degrees));
    delay(SERVO_SHIFT_TIME);
    distance = readDistance();
    char outStr[10];
    sprintf(outStr, "distance: %d degrees: %d", distance, degrees);
    Serial.println(outStr);
    if (distance > maxDistance) {
      maxDistance = distance;
      maxDistanceDegrees = degrees;
    }
  }
  servo.write(calcAngle(maxDistanceDegrees));
  delay(SERVO_SHIFT_TIME);
  if(!autonomous) {
    char distanceStr[256];
    sprintf(distanceStr, "%ld", distance);
    form = formBase + "Centimeters: " + distanceStr;
  }
  return(maxDistance);
}

byte getBestDirection() {                                                 // method not presently called
  long leftDistance, rightDistance, straightDistance, maxDistance;
  byte rtnDistance;
  left();                                                                 // move the wheels to the left
  leftDistance = readDistance();                                          // read the distance to the next obstical
  maxDistance = leftDistance;
  right();                                                                // move the wheels to the right
  rightDistance = readDistance();                                         // read the distance to the next obstical
  if (rightDistance > maxDistance) maxDistance = rightDistance;
  straight();                                                             // point the wheels straight
  straightDistance = readDistance();                                      // read the distance to the next obstical
  if (straightDistance > maxDistance) maxDistance = straightDistance;

  if (maxDistance == leftDistance)
  {
      rtnDistance = GOLEFT;
  } else if (maxDistance == rightDistance) {
      rtnDistance = GORIGHT;
  } else if (maxDistance == straightDistance) {
    rtnDistance = GOSTRAIGHT;
  }

  if (rtnDistance <= MIN_EVADE_DIS) {
    rtnDistance = EVADE;
  }
  return rtnDistance;                                                     // return the direction with the longest distance to the next obstical
}

void evade() {                                                            // method not presently called
    reverseMotor();
    delay(TIME_TO_EVADE);
    stopMotor();
    switch (lastDirection) {                                              // turn is some direction that was not the last direction
      case GOLEFT:
        lastDirection = GORIGHT;
        right();
        break;
      case GORIGHT:
        lastDirection = GOLEFT;
        left();
        break;
      case GOSTRAIGHT:
        lastDirection = GOSTRAIGHT;
        right();                                                          // this should really be determined from the second best direction
        break;
    }
    forwardMotor();
}

void handleAutonomous() {
  stopMotor();
  long distance = pointToBestOpening();
  if (distance <= MIN_EVADE_DIS) {
    reverseMotor();
    delay(TIME_TO_EVADE);
  } else {
    forwardMotor();
    delay(SLEEP_FORWARD);
  }
  
/*  for (int i = 0; i < 10; i++) {
      if (readDistance() <= MIN_EVADE_DIS) {
        stopMotor();      
      }
    }
    stopMotor();
    switch (getBestDirection()) {
      case GOLEFT:
        lastDirection = GOLEFT;
        left();
        forwardMotor();
        break;
      case GORIGHT:
        lastDirection = GORIGHT;
        right();
        forwardMotor();
        break;
      case GOSTRAIGHT:
        lastDirection = GOSTRAIGHT;
        straight();
        forwardMotor();
        break;
      case EVADE:
        evade();
        break;
  }
*/
}

void handle_form() {
    // only move if we submitted the form
    if (server.arg("dir"))
    {
        // get the value of request argument "dir"
        int direction = server.arg("dir").toInt();
        // chose direction
        switch (direction)
        {
            case 1:
                left();
                break;
            case 2:
                right();
                break;
            case 3:
                reverseMotor();
                break;
            case 4:
                forwardMotor();
                break;
            case 5:
                stopMotor();
                break;
            case 6:
                straight();
                break;
            case 7:
                pointToBestOpening();
//              readDistance();
                break;
            case 8:
                autonomous = !autonomous;
                break;
            case 9:
                if (vehicleSpeed < MAX_SPEED) {
                  vehicleSpeed += SPEED_STEP;
                }
                break;
            case 10:
                if (vehicleSpeed > MIN_SPEED) {
                  vehicleSpeed -= SPEED_STEP;
                }
                break;
        }
        delay(300);                                                     // move for 300ms, gives chip time to update wifi also
    }
    server.send(200, "text/html", form);                                // in all cases send the response
}

void setup() {
    Serial.begin ( 9600 );
    WiFi.begin("HOME-4D62", "basket2979fancy");                         // connect to wifi network
    WiFi.config(IPAddress(10,0,0,50), IPAddress(10,0,0,1), IPAddress(255,255,255,0)); // static ip, gateway, netmask
    while (WiFi.status() != WL_CONNECTED) { delay(200); Serial.print ( "." );}               // connect
    Serial.print ( "IP address: " );
    Serial.println ( WiFi.localIP() );
    server.on("/", handle_form);                                        // set up the callback for http server
    server.begin();                                                     // start the webserver
    M1.setmotor(_STANDBY);
    M2.setmotor( _STANDBY);
    servo.attach(SERVOPIN);  
    servo.write(calcAngle(STRAIGHT));                                   // point the vehicle straight ahead   
    lastDirection = GOSTRAIGHT;
    straight();    
    pinMode(TRIGGER,      OUTPUT);
    pinMode(ECHO,         INPUT);
    pinMode(BUILTIN_LED,  OUTPUT);
    form = formBase;
}

void loop() {
    if (autonomous) {
//      for (int i = 0; i < 10; i++) {                                    // necessary to provide a way with which to break out of autonomous mode
        handleAutonomous();
//      }
    }
    server.handleClient();                                                // check for client connections
}
