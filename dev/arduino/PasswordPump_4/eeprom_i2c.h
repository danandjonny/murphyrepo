#ifndef _EEPROM_I2C_H
#define _EEPROM_I2C_H

#include "prj.h"

// support I2C EEPROM 
// e.g. eepromType = EEPROM_24C04
//===========================
#define   EEPROM_24C01    1     // 128 bytes
#define   EEPROM_24C02    2     // 256 bytes
#define   EEPROM_24C04    3     // 512 bytes
#define   EEPROM_24C08    4     // 1kB
#define   EEPROM_24C16    5     // 2kB 

#define   EEPROM_24C32    6     // 4KB
#define   EEPROM_24C64    7     // 8kB
#define   EEPROM_24C128   8     // 16kB
#define   EEPROM_24C256   9     // 32kB
#define   EEPROM_24C512   10    // 64kB

#define MAX_ITER    200 
#define PAGE_SIZE   8 
 
void eeprom1_init(void); 
int  eeprom1_i2c_write_eeprom(uint16_t eeaddr, int len, uint8_t *buf);
int  eeprom1_i2c_read_eeprom (uint16_t eeaddr, int len, uint8_t *buf);
 
//#define eeprom1_EEPROM_DEV_ADR    0xA0    
#define eeprom1_EEPROM_DEV_ADR    0x50    
#define eeprom1_EEPROM_TYPE       EEPROM_24C512

#endif 
