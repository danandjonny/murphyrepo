#ifndef _I2C_H
#define _I2C_H

#include "prj.h"

#define MAX_ITER    200 
extern unsigned char twst; 
void I2C_Init(void); 

#define i2c_START()  { TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); }

/* wait for transmission */
#define i2c_WAIT()            { while ((TWCR & _BV(TWINT)) == 0) ; } 
 
#define i2c_WRITEBYTE(x)      { TWDR = (x);TWCR = _BV(TWINT) | _BV(TWEN);  i2c_WAIT(); }
#define i2c_STOP()            { TWCR = _BV(TWINT) | _BV(TWSTO) | _BV(TWEN);}

unsigned char I2C_Read(unsigned char ack); 
int I2C_Write_n_bytes(unsigned char sla, uint8_t eeaddr,  uint8_t *buf,int len);
int I2C_Read_n_bytes(unsigned char sla, uint8_t eeaddr, uint8_t *buf, int len);

#endif
