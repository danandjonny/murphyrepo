/*
 * Blink.c
 *
 * Created: 11/25/2016 9:49:21 AM
 * Author : djmurphy
 */ 

#ifndef F_CPU
#define F_CPU 1000000UL		// 1 MHz clock speed
#endif

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRD = 0xFF;			//Makes PORTC as Output
	while(1)				//infinite loop
	{
		PORTD = 0b00000001;	//Sets bit 0 on portd to output
		_delay_ms(100);		//1 second delay
		PORTD= 0x00;		//Turns OFF All LEDs
		_delay_ms(100);		//1 second delay
	}
}
