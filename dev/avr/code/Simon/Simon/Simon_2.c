/*
 * Simon.c
 *
 * Created: 9/11/2016 12:56:24 PM
 * Author : djmurphy
 *
 * Baud rate is 1200 at 1MHz for unknown reasons, 9600 @ 8MHz
 *
 */ 
#define F_CPU 1000000UL														// run CPU at 1 MHz

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include "USART.h"
#include "music.h"
#include <stdlib.h>

#define ClearBitNo(x,y) x &= ~_BV(y)										// equivalent to cbi(x,y)
#define SetBitNo(x,y) x |= _BV(y)											// equivalent to sbi(x,y)
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))								// get number of elements in a two dimensional array

#define PORT_LEDS				PORTB
#define DDR_LED					DDRB
#define LED1					DDB2
#define LED2					DDB3
#define	LED3					DDB4
#define LED4					DDB5

#define POWER_PORT				PORTC
#define DDR_POWER				DDRC
#define POWER_LED				DDC5

#define	PORT_BUTTONS			PORTC
#define PIN_BUTTONS				PINC
#define DDR_BUTTON				DDRC
#define BUTTON1					DDC1		
#define BUTTON2					DDC2
#define BUTTON3					DDC3
#define BUTTON4					DDC4
#define UNUSED_BUTTON5			DDC5
#define UNUSED_BUTTON0			DDC0
#define PORTBUTTON1				PORTC1
#define PORTBUTTON2				PORTC2
#define PORTBUTTON3				PORTC3
#define PORTBUTTON4				PORTC4

#define PCINT_BUTTON1			PCINT9	
#define PCINT_BUTTON2			PCINT10
#define PCINT_BUTTON3			PCINT11
#define PCINT_BUTTON4			PCINT12
#define PCIE_BUTTON				PCIE1
#define PCMSK_BUTTON			PCMSK1
#define UNUSED_BUTTON			255

#define BUTTON_LED_OFFSET		1
#define BUTTON_NOTE_OFFSET		1

#define DDR_PIEZO				DDRB
#define PIEZO_OUT				DDB1

#define NUM_ROUNDS				5											// number of rounds to win before winning the game
#define TIME_TO_LIGHT			500											// milliseconds the LED is on
#define TIME_TO_WAIT			500											// time to wait between LED lights
#define NUM_BUTTONS				4
#define MIN_PRESSES_TO_WIN		3											// number of correct matches needed to win the first round
#define MAX_PRESSES_TO_WIN		MIN_PRESSES_TO_WIN + NUM_ROUNDS				// number of correct matches needed to win the last round
#define TIME_TO_PRESS			5000										// 5 seconds 
#define TIME_ROUND_DECREMENT	50											// how long to pause between led lights during playback
#define DELAY_BEFORE_START		500											// pause after playUp() before game starts
#define SCORE_DELAY				1000										// milliseconds to display the score/round number
#define DEBOUNCE_TIME			2000										// in microseconds

#define STATE_PLAY_NOTE			1											// tell Timer1 we are playing a note
#define STATE_REST				2											// tell Timer1 we are playing a rest
#define STATE_DONE				4											// tell Timer1 we are done playing
#define TIME_TO_SLEEP			60000										// One minute in milliseconds
#define RASPBERRY_SIZE			1500
#define CYBORG_EYES_SPEED		100

#define HIGH_SCORE_ADDRESS		0x00										// address in eeprom where the high score is stored

#define DEBUG_ON				0											// set to 1 to see output on serial terminal.  Adversely effects performance.
#if DEBUG_ON
	#define DEBUG_STRING(x)		debug(x)
	#define	DEBUG_INT(x)		debugInt(x)
	#define DEBUG_METRIC(x,y)	debugMetric(x,y)
#else
	#define DEBUG_STRING(x)		do {} while (0)
	#define	DEBUG_INT(x)		do {} while (0)
	#define DEBUG_METRIC(x,y)	do {} while (0)
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

typedef uint8_t byte;

void FlashAll();
int rand_lim(int limit);
void LightLED(int led_num, int timeToLight);
void InitPiezo();
void LoserBuzz();
void WinnerBuzz();
void PlayNote(float icr);
void NoteOff();
void initrand();
void delayms(int milliseconds);
void StartMillisecondsTimer(void);
void StopMillisecondsTimer(void);
void WaitForButtonPress(void);
void bomb(void);
void playUp(void);
void StartTheme();
void GoToSleep(void);
void TurnOnPowerLight(void);
void TurnOffPowerLight(void);
void InitADC0(void);
void LightBinary(byte roundNum);
void WriteHighScore(byte highScore);
void ReadHighScore(volatile byte *score);



volatile byte buttonhistory = 0b0011110;        							// default is high because of the pull-up, correct setting
volatile byte globalButtonPressed = 0;
volatile uint32_t milliseconds = 0;

volatile byte ledArray[NUM_BUTTONS] = {LED1,LED2,LED3,LED4};
volatile byte buttonArray[NUM_BUTTONS] = {BUTTON1,BUTTON2,BUTTON3,BUTTON4};
volatile int notePlace = 0;
volatile int state = STATE_PLAY_NOTE;
volatile int numElements;
volatile uint8_t ADCvalue = 128;											// Global variable, set to volatile if used with ISR
volatile byte highScore;

void debug(const char myString[]) {
	byte i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
	printString("\r\n");
}

void debugInt(signed int anInt) {
	char myInt[4];
	itoa(anInt,myInt,10);
	debug(myInt);
}

void debugMetric(const char myString[], signed int anInt) {
	byte i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
	char myInt[4];
	itoa(anInt,myInt,10);
	debug(myInt);
}



void delayms(int milliseconds) {
	for (int i = 0; i < milliseconds; i++) {
		_delay_ms(1);
	}
}

ISR(ADC_vect)
{
	ADCvalue = ADCH;          // only need to read the high value for 8 bit
	OCR1A = ADCvalue;
	// REMEMBER: once ADCH is read the ADC will update
	
	// if you need the value of ADCH in multiple spots, read it into a register
	// and use the register and not the ADCH

}

void initInturrupts() {														// set up the pin change interrupts for the buttons 
	PCICR |= (1 << PCIE_BUTTON);											// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
	PCMSK_BUTTON |= (1 << PCINT_BUTTON1) | (1 << PCINT_BUTTON2) | (1 << PCINT_BUTTON3) | (1 << PCINT_BUTTON4);// Pin Change Mask Register 0
}

ISR (PCINT1_vect)	{														// Interrupts for buttons 
	DEBUG_STRING("ISR(PCINT1_VECT)");
	byte changedbits;

	if (globalButtonPressed == UNUSED_BUTTON) {
		globalButtonPressed = 0;
		return;
	}

	changedbits = PIN_BUTTONS ^ buttonhistory;
	buttonhistory = PIN_BUTTONS;
	
	if(changedbits & (1 << BUTTON1))
	{
		if( (buttonhistory & (1 << BUTTON1)) == 2 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON1)) {							// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, LED1);
				NoteOff();
				globalButtonPressed = BUTTON1;
				DEBUG_STRING("button 1 release");
				StopMillisecondsTimer();
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON1)) {						// still pressed
				SetBitNo(PORT_LEDS, LED1);
				PlayNote(lightNotes[BUTTON1 - BUTTON_NOTE_OFFSET]);
				DEBUG_STRING("button 1 press");
			}
		}
	}

	if(changedbits & (1 << BUTTON2))
	{
		if( (buttonhistory & (1 << BUTTON2)) == 4 )
		{
			globalButtonPressed = BUTTON2;
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON2)) {							// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, LED2);
				NoteOff();
				globalButtonPressed = BUTTON2;
				DEBUG_STRING("button 2 release");
				StopMillisecondsTimer();
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON2)) {						// still pressed
				SetBitNo(PORT_LEDS, LED2);
				PlayNote(lightNotes[BUTTON2 - BUTTON_NOTE_OFFSET]);
				DEBUG_STRING("button 2 press");
			}
		}
	}
	
	if(changedbits & (1 << BUTTON3))
	{
		if( (buttonhistory & (1 << BUTTON3)) == 8 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON3)) {							// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, LED3);
				NoteOff();
				globalButtonPressed = BUTTON3;
				DEBUG_STRING("button 3 release");
				StopMillisecondsTimer();
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON3)) {						// still pressed
				SetBitNo(PORT_LEDS, LED3);
				PlayNote(lightNotes[BUTTON3 - BUTTON_NOTE_OFFSET]);
				DEBUG_STRING("button 3 press");
			}
		}
	}
	
	if(changedbits & (1 << BUTTON4))
	{
		if( (buttonhistory & (1 << BUTTON4)) == 16 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON4)) {							// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, LED4);
				NoteOff();
				globalButtonPressed = BUTTON4;
				DEBUG_STRING("button 4 release");
				StopMillisecondsTimer();
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON4)) {						// still pressed
				SetBitNo(PORT_LEDS, LED4);
				PlayNote(lightNotes[BUTTON4 - BUTTON_NOTE_OFFSET]);
				DEBUG_STRING("button 4 press");
			}
		}
	}
}

void initTimer0(void) {														// This timer is used to play music
	OCR0A = 255;														 
	TCCR0B |= (1 << WGM12);													// Mode 4, CTC on OCR0A
	TIMSK0 |= (1 << OCIE0A);												// Set interrupt on compare match
	TCCR0B |= (1 << CS02) | (1 << CS00);									// set prescaler to 1024 and start the timer

	numElements = NELEMS(song_ptr[0]);
}

ISR (TIMER0_COMPA_vect)	{													// Timer0 interrupt, plays back music
	//DEBUG_STRING("ISR (TIMER0_COMPA_vect)");
	switch(state) {
		case STATE_PLAY_NOTE  :
			PlayNote(song_ptr[0][notePlace]);
			OCR0A = song_ptr[1][notePlace] * 2;
			state = STATE_REST;
			break;
		case STATE_REST  :
			NoteOff();
			OCR0A = song_ptr[2][notePlace];
			state = STATE_PLAY_NOTE;
			break;
		case STATE_DONE :
			OCR0A = 0;
			TCCR0B &= ~((1 << CS00)|(1 << CS01)|(1 << CS02));				// Turn off the timer that plays music
			NoteOff();
	}
	notePlace++;
	
	if (notePlace == numElements) {
		state = STATE_DONE;
	}
}

void InitPiezo() {															// Using Timer1, plays individual notes through the piezo buzzer
	OCR1A = ADCvalue;														// Volume
	ICR1 = 0;																// Pitch (for example only)
	TCCR1B |= ((1<<CS10)|(1<<WGM13));										// No prescale, phase correct PWM
	TCCR1A |= ((1<<WGM11)|(1<<COM1A1));										// Phase correct PWM, clear OCR1A at top
}

void initTimer2(void) {														// Measures milliseconds
	TCCR2A |= (1 << WGM21);													// Configure timer 2 for CTC mode
	TIMSK2 |= (1 << OCIE2A);												// Set interrupt on compare match
	OCR2A = 0;																// Fire interrupt every millisecond
}

ISR (TIMER2_COMPA_vect)	{													// Timer2 interrupt, increment milliseconds
	//DEBUG_STRING("ISR (TIMER2_COMPA_vect)");
	milliseconds+=1;
}

void StartMillisecondsTimer(void) {
	DEBUG_STRING("StartMillisecondsTimer");
	milliseconds = 0;
	TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20);						// set prescaler to 1024 and start the timer
}

void StopMillisecondsTimer(void) {
	DEBUG_STRING("StopMillisecondsTimer");
	TCCR0B &= ~((1 << CS20)|(1 << CS21)|(1 << CS22));
}

/************************************************************************/
/* Initialize the AVR                                                   */
/************************************************************************/
void InitAVR() {
	DEBUG_STRING("InitAVR()");

	DDR_LED |= (1 << LED1);													// LED pins are outputs */
	DDR_LED |= (1 << LED2);
	DDR_LED |= (1 << LED3);
	DDR_LED |= (1 << LED4);
	
	DDR_POWER |= (1 << POWER_LED);											// Set Power On LED as output
	
	DDR_BUTTON &= ~(1 << BUTTON1);											// Button pins are inputs */
	DDR_BUTTON &= ~(1 << BUTTON2);
	DDR_BUTTON &= ~(1 << BUTTON3);
	DDR_BUTTON &= ~(1 << BUTTON4);
	PORT_BUTTONS |= ((1 << PORTBUTTON1) | (1 << PORTBUTTON2) | (1 << PORTBUTTON3) | (1 << PORTBUTTON4));  // turn on pullups
	
	DDR_BUTTON |= (1 << UNUSED_BUTTON0);									// Unused pins set as outputs so buttonhistory will make sense*/
	DDR_BUTTON |= (1 << UNUSED_BUTTON5);

	DDR_PIEZO |= (1 << PIEZO_OUT);											// Piezo out
}

int main(void)
{
	int timeToLight = TIME_TO_LIGHT;
	int timeToWait = TIME_TO_WAIT;
	int pressesToWin = MIN_PRESSES_TO_WIN;
	byte score = 0;

	initUSART();
	DEBUG_STRING("main()");
	InitPiezo();
	initTimer2();
	initInturrupts(); 
	InitAVR();
	TurnOnPowerLight();														// Turn on the power light
	InitADC0();
	sei();																	// enable interrupts
		
    while (1)
    {
		StartTheme();
		pressesToWin = MIN_PRESSES_TO_WIN;
		int LEDToLightArray[NUM_ROUNDS][MAX_PRESSES_TO_WIN];
		WaitForButtonPress();
		state = STATE_DONE;													// turn off the music if it's still playing
		DEBUG_METRIC("milliseconds: ", milliseconds);
		srand(milliseconds);												// seed the random number generator.
		
		for (int round_num = 0; round_num < NUM_ROUNDS; round_num++)		// Initialize the array 
		{
			for (int press_num = 0; press_num < MAX_PRESSES_TO_WIN; press_num++)
			{
				LEDToLightArray[round_num][press_num] = ledArray[rand_lim(NUM_BUTTONS - 1)];
				//DEBUG_METRIC("LEDToLightArray[round_num][press_num]: ",LEDToLightArray[round_num][press_num]);
			}	 
		}
		DEBUG_STRING("About to ReadHighScore()");
		ReadHighScore(&highScore);
		DEBUG_METRIC("Read High Score:", highScore);
		if (highScore == 255) {												// after the chip is burned the hih
			WriteHighScore(0);
			DEBUG_STRING("Overwrite highScore of 255");
		}
		LightBinary(highScore);
		playUp();
		byte gameOver = 0;
		for (byte round_num = 0;round_num < NUM_ROUNDS && !gameOver; round_num++)
		{
			_delay_ms(DELAY_BEFORE_START);
			for (int try_num = 0; try_num < pressesToWin && !gameOver; try_num++)
			{
				for (int press_num = 0; press_num <= try_num; press_num++)
				{
					/* Light up LEDToLightArray[round_num][try_num] */
					delayms(timeToWait);
					PlayNote(lightNotes[LEDToLightArray[round_num][press_num] - BUTTON_LED_OFFSET - 1]);
					LightLED(LEDToLightArray[round_num][press_num], timeToLight);
					NoteOff();
					DEBUG_METRIC("Lit LED: ",LEDToLightArray[round_num][press_num] - BUTTON_LED_OFFSET);
				}
				for (int press_num = 0; press_num <= try_num && !gameOver; press_num++) {
					globalButtonPressed = 0;
					StartMillisecondsTimer();
					while (globalButtonPressed == 0 && milliseconds < TIME_TO_PRESS);
					/* If button input does not match light or if time expires */
					if ((globalButtonPressed != LEDToLightArray[round_num][press_num]  - BUTTON_LED_OFFSET) || milliseconds >= TIME_TO_PRESS) {
						gameOver = 1;
						DEBUG_STRING("Loser");
						LoserBuzz();
						if (milliseconds >= TIME_TO_PRESS) {
							StopMillisecondsTimer();
						}
					}
				}
			}
			/* if not gameOver, Winner Buzzer */
			if (!gameOver) {
				DEBUG_STRING("Winner");
				WinnerBuzz();
				timeToLight -= TIME_ROUND_DECREMENT;
				timeToWait -= TIME_ROUND_DECREMENT;
				pressesToWin++;
				LightBinary(round_num + 1);									// Round over, display the round number in binary
			}
			FlashAll();
			score = round_num;
			if (score > highScore) {
				DEBUG_METRIC("Write High Score: ", score);
				WriteHighScore(score);										// we have a new high score!
				highScore = score;
			}

		}
		/* if not gameOver, Winner Buzzer */
		if (!gameOver) {
			DEBUG_STRING("Winner");
			for (byte i = 0; i < 3; i++) {
				WinnerBuzz();
			}
		}
    }
}

int rand_lim(int limit) {													// return a random number between 0 and limit inclusive.
    int divisor = RAND_MAX/(limit+1);
    int retval;
    do { 
        retval = rand() / divisor;
    } while (retval > limit);
    return retval;
}

void LightLED(int led_num, int timeToLight) {
	SetBitNo(PORT_LEDS, led_num);
	delayms(timeToLight);
	ClearBitNo(PORT_LEDS, led_num);
}

void FlashAll() {
	for (int i = 0; i < 4; i++) {
		SetBitNo(PORT_LEDS, ledArray[i]);
	}
	_delay_ms(200);
	for (int i = 0; i < 4; i++) {
		ClearBitNo(PORT_LEDS, ledArray[i]);
	}
}

void WaitForButtonPress() {													// cyborg eyes until button press
	StartMillisecondsTimer();
	globalButtonPressed = UNUSED_BUTTON;
	while (globalButtonPressed == UNUSED_BUTTON) {
		if (milliseconds > TIME_TO_SLEEP) {
			GoToSleep();
		}
		for (int i = 0; i < NUM_BUTTONS && globalButtonPressed == UNUSED_BUTTON; i++) {
			SetBitNo(PORT_LEDS, ledArray[i]);
			_delay_ms(CYBORG_EYES_SPEED);
			ClearBitNo(PORT_LEDS, ledArray[i]);
		}
		for (int i = NUM_BUTTONS - 1; i >= 0 && globalButtonPressed == UNUSED_BUTTON; i--) {
			SetBitNo(PORT_LEDS, ledArray[i]);
			_delay_ms(CYBORG_EYES_SPEED);
			ClearBitNo(PORT_LEDS, ledArray[i]);
		}
	}
	StopMillisecondsTimer();
}

void NoteOff() {
	ICR1 = 0;
}

void PlayNote(float icr) {
	ICR1 = icr; 
}

void WinnerBuzz() {
	for (int i = 0; i < 4; i++) {
		PlayNote(NOTE_B5);
		SetBitNo(PORT_LEDS, ledArray[i]);
		_delay_ms(100);
		NoteOff();
		_delay_ms(100);
		PlayNote(NOTE_D3);
		_delay_ms(100);
		NoteOff();
		ClearBitNo(PORT_LEDS, ledArray[i]);
		_delay_ms(100);
	}
}

void LoserBuzz() {
	PlayNote(NOTE_G2);
	_delay_ms(RASPBERRY_SIZE);
	NoteOff();
	bomb();
}

void bomb() {
	for (int i=NELEMS(allNotes);i>=0;i--) {
		PlayNote(allNotes[i]);
		_delay_ms(20);
		NoteOff();
	}
}

void playUp() {
	for (int i=0;i<NELEMS(allNotes);i++) {
		PlayNote(allNotes[i]);
		_delay_ms(20);
		NoteOff();
	}
}

void StartTheme() {
	state = STATE_PLAY_NOTE;
	notePlace = 0;
	initTimer0();
}

void GoToSleep(void) {
	TurnOffPowerLight();
	byte adcsra = ADCSRA;													// save the ADC Control and Status Register A
	ADCSRA = 0;																// disable the ADC
	EICRA = _BV(ISC01);														// configure INT0 to trigger on falling edge
	EIMSK = _BV(INT0);														// enable INT0
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	cli();																	// stop interrupts to ensure the BOD timed sequence executes as required
	sleep_enable();
	uint8_t mcucr1 = MCUCR | _BV(BODS) | _BV(BODSE);						// disable brown-out detection while sleeping (20-25�A)
	uint8_t mcucr2 = mcucr1 & ~_BV(BODSE);
	MCUCR = mcucr1;
	MCUCR = mcucr2;
	//sleep_bod_disable();													// for AVR-GCC 4.3.3 and later, this is equivalent to the previous 4 lines of code
	sei();																	// ensure interrupts enabled so we can wake up again
	sleep_cpu();															// go to sleep
	sleep_disable();														// wake up here
	ADCSRA = adcsra;														// restore ADCSRA
}

void TurnOnPowerLight() {
	SetBitNo(POWER_PORT, POWER_LED);
}

void TurnOffPowerLight() {
	ClearBitNo(POWER_PORT, POWER_LED);
}

/************************************************************************/
/* Analog to digital conversion                                         */
/************************************************************************/
void InitADC0() {
    ADMUX = 0;																// use ADC0
    ADMUX |= (1 << REFS0);													// use AVcc as the reference
    ADMUX |= (1 << ADLAR);													// Right adjust for 8 bit resolution

    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);					// 128 prescale for 16Mhz
    ADCSRA |= (1 << ADATE);													// Set ADC Auto Trigger Enable
    
    ADCSRB = 0;																// 0 for free running mode

    ADCSRA |= (1 << ADEN);													// Enable the ADC
    ADCSRA |= (1 << ADIE);													// Enable Interrupts

    ADCSRA |= (1 << ADSC);													// Start the ADC conversion
}

void LightBinary(byte numToLight) {											// Shows the score/rounds in binary using the LEDs
	byte bits[NUM_BUTTONS];
	for (byte i = 0; i < NUM_BUTTONS; i++) {
		if (numToLight & (1 << i)) {
			bits[i] = 1;
		} else {
			bits[i] = 0;
		}
	}
	byte arrayElement = 0;
	for (byte i = LED4; i >= LED1; i--) {									// reverse the order of the bits so the least significant digit is on the right
		if (bits[arrayElement]) {
			SetBitNo(PORT_LEDS, i);
		}
		arrayElement++;
	}
	_delay_ms(SCORE_DELAY);
	PORT_LEDS = 0b00000000;													// Turn off all of the LEDs
}


void ReadHighScore(volatile byte *highScore) {								// Reads the high score from eeprom
	DEBUG_STRING("ReadHighScore()");
	uint8_t *address_high_score = (uint8_t *) HIGH_SCORE_ADDRESS;
	*highScore = eeprom_read_byte(address_high_score);
}

void WriteHighScore(byte highScore) {										// Saves the high score to eeprom
	DEBUG_STRING("WriteHighScore()");
	uint8_t *address_high_score = (uint8_t *) HIGH_SCORE_ADDRESS;
	eeprom_update_byte(address_high_score, highScore);
}
