/*
 * Simon.c
 *
 * Created: 9/11/2016 12:56:24 PM
 * Author : djmurphy
 */ 
#define F_CPU 1000000UL													// run CPU at 1 MHz

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "USART.h"
#include "music.h"
#include <stdlib.h>

#define ClearBit(x,y) x &= ~y
#define SetBit(x,y) x |= y
#define ToggleBit(x,y) x ^= y
#define ClearBitNo(x,y) x &= ~_BV(y)      // equivalent to cbi(x,y)
#define SetBitNo(x,y) x |= _BV(y)         // equivalent to sbi(x,y)

#define DEBOUNCE_TIME			500												// microseconds
#define PRESS_WAIT_TIME			3000

#define HEARTBEAT_LED_PIN       PB0

#define PORT_LEDS				PORTB
#define DDR_LED					DDRB
#define LED1					DDB1
#define LED2					DDB2
#define	LED3					DDB3
#define LED4					DDB4

#define	PORT_BUTTONS			PORTC
#define PIN_BUTTONS				PINC
#define DDR_BUTTON				DDRC
#define BUTTON1					DDC1		
#define BUTTON2					DDC2
#define BUTTON3					DDC3
#define BUTTON4					DDC4
#define UNUSED_BUTTON5			DDC5
#define UNUSED_BUTTON0			DDC0
#define PORTBUTTON1				PORTC1
#define PORTBUTTON2				PORTC2
#define PORTBUTTON3				PORTC3
#define PORTBUTTON4				PORTC4

#define PCINT_BUTTON1			PCINT9	
#define PCINT_BUTTON2			PCINT10
#define PCINT_BUTTON3			PCINT11
#define PCINT_BUTTON4			PCINT12
#define PCIE_BUTTON				PCIE1
#define PCMSK_BUTTON			PCMSK1

#define DDR_PIEZO				DDRB
#define PIEZO_OUT				DDB1

#define NUM_ROUNDS				1
#define TIME_TO_LIGHT			500		/* milliseconds */
#define NUM_BUTTONS				4
#define PRESSES_TO_WIN			6

#define DEBUG 1

void FlashHeartbeat();
void FlashAll();
int rand_lim(int limit);
void LightLED(int led_num);
void InitPiezo();
void LoserBuzz();
void WinnerBuzz();
void PlayNote(float icr);
void NoteOff();

typedef uint8_t byte;

volatile byte buttonhistory = 0b0011110;        							// default is high because of the pull-up, correct setting
volatile byte globalButtonPressed = 0;

volatile byte ledArray[NUM_BUTTONS] = {LED1,LED2,LED3,LED4};
volatile byte buttonArray[NUM_BUTTONS] = {BUTTON1,BUTTON2,BUTTON3,BUTTON4};

void debug(const char myString[]) {
	if (DEBUG) {
		byte i = 0;
		while (myString[i]) {
			transmitByte(myString[i]);
			i++;
		}
		printString("\r\n");
	}
}

void debugInt(signed int anInt) {
	if (DEBUG) {
		char myInt[4];
		itoa(anInt,myInt,10);
		debug(myInt);
	}
}

void initTimer0(void)
{
}

/* set up the pin change interrupts for the buttons */
void initInturrupts() {
	PCICR |= (1 << PCIE_BUTTON);													// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
	PCMSK_BUTTON |= (1 << PCINT_BUTTON1) | (1 << PCINT_BUTTON2) | (1 << PCINT_BUTTON3) | (1 << PCINT_BUTTON4);// Pin Change Mask Register 0
}

ISR (PCINT1_vect)	{											// Interrupts for buttons 
	debug("ISR(PCINT1_VECT)");
	byte changedbits;

	changedbits = PIN_BUTTONS ^ buttonhistory;
	buttonhistory = PIN_BUTTONS;
	
	if(changedbits & (1 << BUTTON1))
	{
		if( (buttonhistory & (1 << BUTTON1)) == 2 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON1)) {					// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, BUTTON1);
				NoteOff();
				debug("button 1 release");
				globalButtonPressed = BUTTON1;
			}
		}
		else
		{														// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON1)) {					// still pressed
				SetBitNo(PORT_LEDS, BUTTON1);
				PlayNote(NOTE_A);
				debug("button 1 press");
			}
		}
	}

	if(changedbits & (1 << BUTTON2))
	{
		if( (buttonhistory & (1 << BUTTON2)) == 4 )
		{
			globalButtonPressed = BUTTON2;
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON2)) {					// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, BUTTON2);
				NoteOff();
				debug("button 2 release");
				globalButtonPressed = BUTTON2;
			}
		}
		else
		{														// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON2)) {					// still pressed
				SetBitNo(PORT_LEDS, BUTTON2);
				PlayNote(NOTE_D);
				debug("button 2 press");
			}
		}
	}
	
	if(changedbits & (1 << BUTTON3))
	{
		if( (buttonhistory & (1 << BUTTON3)) == 8 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON3)) {					// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, BUTTON3);
				NoteOff();
				debug("button 3 release");
				globalButtonPressed = BUTTON3;
			}
		}
		else
		{														// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON3)) {					// still pressed
				SetBitNo(PORT_LEDS, BUTTON3);
				PlayNote(NOTE_G);
				debug("button 3 press");
			}
		}
	}
	
	if(changedbits & (1 << BUTTON4))
	{
		if( (buttonhistory & (1 << BUTTON4)) == 16 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON4)) {					// LOW to HIGH pin change (button released)
				ClearBitNo(PORT_LEDS, BUTTON4);
				NoteOff();
				debug("button 4 release");
				globalButtonPressed = BUTTON4;
			}
		}
		else
		{														// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON4)) {					// still pressed
				SetBitNo(PORT_LEDS, BUTTON4);
				PlayNote(NOTE_B);
				debug("button 4 press");
			}
		}
	}
}

/************************************************************************/
/* Initialize the AVR                                                   */
/************************************************************************/
void InitAVR() {
	debug("INITAVR()");
	DDR_LED |= (1 << HEARTBEAT_LED_PIN);                     /* HEARTBEAT_LED_PIN is now an output           */

	DDR_LED |= (1 << LED1);		/* LED pins are outputs */
	DDR_LED |= (1 << LED2);
	DDR_LED |= (1 << LED3);
	DDR_LED |= (1 << LED4);
	
	DDR_BUTTON &= ~(1 << BUTTON1);	/* Button pins are inputs */
	DDR_BUTTON &= ~(1 << BUTTON2);
	DDR_BUTTON &= ~(1 << BUTTON3);
	DDR_BUTTON &= ~(1 << BUTTON4);
	PORT_BUTTONS |= ((1 << PORTBUTTON1) | (1 << PORTBUTTON2) | (1 << PORTBUTTON3) | (1 << PORTBUTTON4));  // turn on pullups
		
	DDR_BUTTON |= (1 << UNUSED_BUTTON0);		/* Unused pins set as outputs so buttonhistory will make sense*/
	DDR_BUTTON |= (1 << UNUSED_BUTTON5);

	DDR_PIEZO |= (1 << PIEZO_OUT);	// Piezo out

}

void InitPiezo() {
	OCR1A = 0b00001001;									// Volume
	ICR1 = 1516.852228;									// Pitch (for example only)
	TCCR1B |= ((1<<CS10)|(1<<WGM13));					// No prescale, phase correct PWM
	TCCR1A |= ((1<<WGM11)|(1<<COM1A1));					// Phase correct PWM, clear OCR1A at top
}

int main(void)
{
	FlashHeartbeat();
	initUSART();
	debug("reaching...");
	initTimer0();
	initInturrupts(); 
	InitAVR();
	InitPiezo();
	sei();																// enable interrupts
	
	WinnerBuzz();

	int LEDToLightArray[NUM_ROUNDS][PRESSES_TO_WIN];
	/* Initialize the array */
	for (int round_num = 0; round_num < NUM_ROUNDS; round_num++) 
	{
		for (int press_num = 0; press_num < PRESSES_TO_WIN; press_num++)
		{
			LEDToLightArray[round_num][press_num] = ledArray[rand_lim(NUM_BUTTONS - 1) + 1];
			debugInt(LEDToLightArray[round_num][press_num]);
		}	 
	}

    while (1) 
    {
		FlashHeartbeat();
		_delay_ms(1000);
		for (int round_num = 0;round_num < NUM_ROUNDS; round_num++)
		{
			FlashAll();
			byte gameOver = 0;
			for (int try_num = 0; try_num < PRESSES_TO_WIN; try_num++)
			{
				for (int press_num = 0; press_num <= try_num; press_num++)
				{
					/* Light up LEDToLightArray[round_num][try_num] */
					LightLED(LEDToLightArray[round_num][press_num]);
					debug("Lit LED:");
					debugInt(LEDToLightArray[round_num][press_num]);
				}
				for (int press_num = 0; press_num <= try_num; press_num++) {
					/* Wait for input, set a timer here */
					globalButtonPressed = 0;
					while (globalButtonPressed == 0);
					/* If button input does not match light or if time expires */
					debug("globalButtonPressed:");
					debugInt(globalButtonPressed);
					if (globalButtonPressed != LEDToLightArray[round_num][press_num]) {
						gameOver = 1;
						press_num = try_num;
						try_num = PRESSES_TO_WIN;
						debug("Loser");
						LoserBuzz();
					}
				}
			}
			/* if not gameOver, Winner Buzzer */
			if (!gameOver) {
				debug("Winner");
				WinnerBuzz();
			}
		}
    }
}

int rand_lim(int limit) {
/* return a random number between 0 and limit inclusive.
 */
    int divisor = RAND_MAX/(limit+1);
    int retval;
    do { 
        retval = rand() / divisor;
    } while (retval > limit);
    return retval;
}

void LightLED(int led_num) {
	/* might want to play a certain tone here, too */
	SetBitNo(PORT_LEDS, led_num);
	_delay_ms(TIME_TO_LIGHT);
	ClearBitNo(PORT_LEDS, led_num);
}

void FlashHeartbeat() {
	SetBitNo(PORT_LEDS, HEARTBEAT_LED_PIN);
	_delay_ms(100);
	ClearBitNo(PORT_LEDS, HEARTBEAT_LED_PIN);
}

void FlashAll() {
	for (int i = 0; i < 4; i++) {
		SetBitNo(PORT_LEDS, ledArray[i]);
	}
	_delay_ms(200);
	for (int i = 0; i < 4; i++) {
		ClearBitNo(PORT_LEDS, ledArray[i]);
	}
}

void NoteOff() {
	ICR1 = 0;
}

void PlayNote(float icr) {
	ICR1 = icr; 
}

void WinnerBuzz() {
	// Should also flash lights here
	for (int i = 0; i < 4; i++) {
		PlayNote(NOTE_HIGH_E);
		SetBitNo(PORT_LEDS, ledArray[i]);
		_delay_ms(100);
		NoteOff();
		_delay_ms(100);
		PlayNote(NOTE_D);
		_delay_ms(100);
		NoteOff();
		ClearBitNo(PORT_LEDS, ledArray[i]);
		_delay_ms(100);
	}
}

void LoserBuzz() {
	PlayNote(NOTE_HIGH_E);
	_delay_ms(1000);
	NoteOff();
}
