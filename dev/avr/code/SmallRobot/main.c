/*
 * SmallRobot.c
 *
 * Created: 7/6/2017 9:19:38 AM
 * Author : djmurphy
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define ledoff PORTB &= ~(1<<PB4)
#define ledon PORTB |= (1<<PB4)

void move(uint8_t lspeed, uint8_t rspeed);
void flashLED(uint8_t flashes);
void setup();
void loop();
void initADC();
void analogWrite(uint8_t pin, int value);
uint8_t analogRead(uint8_t channel);
void initTimer0();
void delayMS(int ticks);

int main() {
	setup();
	int rightAnalogRead = 0;
	int leftAnalogRead = 0;
	while (1 == 1) {
		leftAnalogRead = analogRead(PB2);
		rightAnalogRead = analogRead(PB3);
		move(leftAnalogRead,rightAnalogRead);
		flashLED(leftAnalogRead);
	}
	return 0;
}

void setup(){
	initADC();
	DDRB |= 1<<DDB0;					// set the data direction register for the right motor to OUTPUT.
	DDRB |= 1<<DDB1;					// set the data direction register for the left motor to OUTPUT.
	DDRB |= 1<<DDB4;					// set the data direction register for the LED to OUTPUT.
	DDRB &= ~(1<<DDB2);					// set the data direction register for the right sensor to INPUT
	DDRB &= ~(1<<DDB3);					// set the data direction register for the left sensor to INPUT
	initTimer0();
    analogWrite(PB0, 0);
    analogWrite(PB1, 0);
}

void initADC()
{
  /* this function initialises the ADC 

        ADC Prescaler Notes:
	--------------------

	   ADC Prescaler needs to be set so that the ADC input frequency is between 50 - 200kHz.
  
           For more information, see table 17.5 "ADC Prescaler Selections" in 
           chapter 17.13.2 "ADCSRA � ADC Control and Status Register A"
          (pages 140 and 141 on the complete ATtiny25/45/85 datasheet, Rev. 2586M�AVR�07/10)

           Valid prescaler values for various clock speeds
	
	     Clock   Available prescaler values
           ---------------------------------------
             1 MHz   8 (125kHz), 16 (62.5kHz)
             4 MHz   32 (125kHz), 64 (62.5kHz)
             8 MHz   64 (125kHz), 128 (62.5kHz)
            16 MHz   128 (125kHz)

           Below example set prescaler to 128 for mcu running at 8MHz
           (check the datasheet for the proper bit values to set the prescaler)
  */

  	ADCSRA =
			(1 << ADEN)  |     // Enable ADC (62.5kHz)
			(1 << ADPS2) |     // set prescaler to 16, bit 2
			(0 << ADPS1) |     // set prescaler to 16, bit 1
			(0 << ADPS0);      // set prescaler to 16, bit 0
}

void initTimer0(void) {

	TCCR0A = 1 << COM0A0 | 1 << COM0A1 | 1 << COM0B0 | 1 << COM0B1 | 1 << WGM00 | 1 << WGM01;	// Fast PWM, Set OC0A/OC0B on Compare Match, clear OC0A/OC0B at BOTTOM (inverting mode)
	TCCR0B |= 1<<CS00;																			// no prescaling
}

void move(uint8_t lspeed, uint8_t rspeed){
	analogWrite(PB0, rspeed);
	analogWrite(PB1, lspeed);
}

void flashLED(uint8_t ticks){
	ledon;
	delayMS(ticks);
	ledoff;
	delayMS(ticks);
}

void delayMS(int ticks) {
	for (int i = 1; i <= ticks; i++) {
		_delay_ms(1);
	}
}

void analogWrite(uint8_t pin, int val)
{
	val = 255 - val;		// light speeds up the motors
	if (pin == PB0) {
		OCR0A = val;		// set the right motor speed
		} else if (pin == PB1) {
		OCR0B = val;		// set the left motor speed
	}
}

uint8_t  analogRead(uint8_t channel)
{

	switch (channel) {
		case PB2:
		ADMUX =
		// 8-bit resolution
		// set ADLAR to 1 to enable the Left-shift result (only bits ADC9..ADC2 are available)
		// then, only reading ADCH is sufficient for 8-bit results (256 values)
		(1 << ADLAR) |     // left shift result
		(0 << REFS1) |     // Sets ref. voltage to VCC, bit 1
		(0 << REFS0) |     // Sets ref. voltage to VCC, bit 0
		(0 << MUX3)  |     // use ADC2 for input (PB2), MUX bit 3
		(0 << MUX2)  |     // use ADC2 for input (PB2), MUX bit 2
		(0 << MUX1)  |     // use ADC2 for input (PB2), MUX bit 1
		(1 << MUX0);       // use ADC2 for input (PB2), MUX bit 0
		break;
		case PB3:
		ADMUX =
		(1 << ADLAR) |     // left shift result
		(0 << REFS1) |     // Sets ref. voltage to VCC, bit 1
		(0 << REFS0) |     // Sets ref. voltage to VCC, bit 0
		(0 << MUX3)  |     // use ADC2 for input (PB2), MUX bit 3
		(0 << MUX2)  |     // use ADC2 for input (PB2), MUX bit 2
		(1 << MUX1)  |     // use ADC2 for input (PB2), MUX bit 1
		(1 << MUX0);       // use ADC2 for input (PB2), MUX bit 0
		break;
	}

	ADCSRA |= (1 << ADSC);         // start ADC measurement
	while (ADCSRA & (1 << ADSC) ); // wait till conversion complete

	return ADCH;
}

