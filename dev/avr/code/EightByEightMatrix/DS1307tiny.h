/**
 * DS1307tiny - DS1307 Real-time Clock library
 *
 * @created: 2015-02-25
 * @author: Neven Boyanov
 *
 * This is part of the Tinusaur/DS1307tiny project.
 *
 * Copyright (c) 2016 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Please, as a favor, retain the link http://tinusaur.org to The Tinusaur Project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/ds1307tiny
 *
 */

#ifndef DS1307TINY_H
#define DS1307TINY_H

// ============================================================================

#include <stdint.h>

// ----------------------------------------------------------------------------

#define DS1307_RESULT_SUCCESS 0x00
#define DS1307_RESULT_FAILURE 0xFF

#define DS1307_RESULT_CLOCKHALT 0x01

#define DS1307_I2CADDR 0x68
//#define DS1307_I2CADDR 208	//this is the RTC address on the Alarm Clock project

#define DS1307_REGISTERS	0x00
#define DS1307_REG_SECONDS	0x00
#define DS1307_REG_MINUTES	0x01
#define DS1307_REG_HOURS	0x02
#define DS1307_REG_WEEKDAY	0x03
#define DS1307_REG_DATE		0x04
#define DS1307_REG_MONTH	0x05
#define DS1307_REG_YEAR		0x06
#define DS1307_REG_CONTROL	0x07
#define DS1307_RAM_BASE		0x08

// ----------------------------------------------------------------------------

#define DS1307_SET_YEAR(val) { ds1307_write_reg8(DS1307_REG_YEAR, val) };
#define DS1307_SET_MONTH(val) { ds1307_write_reg8(DS1307_REG_MONTH, val) };
#define DS1307_SET_DATE(val) { ds1307_write_reg8(DS1307_REG_DATE, val) };
#define DS1307_SET_WEEKDAY(val) { ds1307_write_reg8(DS1307_REG_WEEKDAY, val) };
#define DS1307_SET_HOURS(val) { ds1307_write_reg8(DS1307_REG_HOURS, val) };
#define DS1307_SET_MINUTES(val) { ds1307_write_reg8(DS1307_REG_MINUTES, val) };
#define DS1307_SET_SECONDS(val) { ds1307_write_reg8(DS1307_SET_SECONDS, val) };

// ----------------------------------------------------------------------------

static uint8_t bcd2bin (uint8_t val) { return val - 6 * (val >> 4); }
static uint8_t bin2bcd (uint8_t val) { return val + 6 * (val / 10); }

// ----------------------------------------------------------------------------

uint8_t ds1307_read_reg8(uint8_t reg_addr);
uint8_t ds1307_write_reg8(uint8_t reg_addr, uint8_t reg_data);
uint8_t ds1307_init(void);
uint8_t ds1307_setdatetime(
	uint16_t year, uint8_t month, uint8_t date, 
	uint8_t weekday, 
	uint8_t hour, uint8_t min, uint8_t sec);

// ============================================================================

#endif