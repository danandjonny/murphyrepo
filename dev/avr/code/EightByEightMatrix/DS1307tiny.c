/**
 * DS1307tiny - DS1307 Real-time Clock library
 *
 * @created: 2015-03-03
 * @author: Neven Boyanov
 *
 * This is part of the Tinusaur/DS1307tiny project.
 *
 * Copyright (c) 2016 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Please, as a favor, retain the link http://tinusaur.org to The Tinusaur Project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/ds1307tiny
 *
 */

// ============================================================================

//	Ref: https://www.maximintegrated.com/en/products/digital/real-time-clocks/DS1307.html

// ----------------------------------------------------------------------------

#include <stdint.h>
#include <util/delay.h>

#include "owowod.h"
#include "debugging.h"

#include "usitwim.h"

#include "ds1307tiny.h"

// ----------------------------------------------------------------------------

uint8_t ds1307_read_reg8(uint8_t reg_addr)
{
	uint8_t buffer[2];
	uint8_t result;

	buffer[1] =	reg_addr;
	usitwim_data_write(DS1307_I2CADDR, buffer, 2);
	
	buffer[1] =	reg_addr;
	usitwim_data_read(DS1307_I2CADDR, buffer, 2);
	result = buffer[1];
	
	return result;
}

uint8_t ds1307_write_reg8(uint8_t reg_addr, uint8_t reg_data)
{
	uint8_t buffer[3];
	buffer[1] =	reg_addr;
	buffer[2] =	reg_data;
	usitwim_data_write(DS1307_I2CADDR, buffer, 3);
	// TODO: Check for success
	return DS1307_RESULT_SUCCESS;
}

uint8_t ds1307_init(void)
{
	uint8_t ds1307_result = (ds1307_read_reg8(DS1307_REG_SECONDS) >> 7);
	// /*DEBUGGING*/ DEBUGGING_VARU("init/result", ds1307_result);
	if (ds1307_result)
	{
		// /*DEBUGGING*/ DEBUGGING_VARU("CH", ds1307_result);
		// /*DEBUGGING*/ DEBUGGING_STRINGLN("Clock Halt condition");
		return DS1307_RESULT_CLOCKHALT;
	}
	else
	{
		// /*DEBUGGING*/ DEBUGGING_STRINGLN("OK");
	}
	return DS1307_RESULT_SUCCESS;
}

uint8_t ds1307_setdatetime(
	uint16_t year, uint8_t month, uint8_t date, 
	uint8_t weekday, 
	uint8_t hour, uint8_t min, uint8_t sec)
{
	uint8_t buffer[9];
	buffer[1] =	DS1307_REGISTERS;
	buffer[2] =	bin2bcd(sec);
	buffer[3] =	bin2bcd(min);
	buffer[4] =	bin2bcd(hour);
	buffer[5] =	bin2bcd(weekday);
	buffer[6] =	bin2bcd(date);
	buffer[7] =	bin2bcd(month);
	buffer[8] =	bin2bcd(year - 2000);
	return usitwim_data_write(DS1307_I2CADDR, buffer, 9);
}

// ============================================================================