/*
  One MAX7219 connected to an 8x8 LED matrix.
 */

//#define F_CPU 1000000UL //Project > EightByEightMatrix Properties > Toolchain > AVR/GNU C Compiler > Symbols > F_CPU=1000000
// NOTE: The F_CPU (CPU frequency) should not be defined in the source code.
//       It should be defined in either (1) Makefile; or (2) in the IDE.

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "cpufreq.h"
#include "owowod.h"
#include "debugging.h"
#include "usitwim.h"
#include "ds1307tiny.h"

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                ATtiny
//               25/45/85
//              +----------+   (-)-------
//      (RST)---+ PB5  Vcc +---(+)-------
// --[OWOWOD]---+ PB3  PB2 +---[TWI/SCL]-
//           ---+ PB4  PB1 +---
// -------(-)---+ GND  PB0 +---[TWI/SDA]-
//              +----------+
//
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ----------------------------------------------------------------------------

#define DEBUGGING_DATETIME(y, m, d, wd, hh, mm, ss) { \
	DEBUGGING_NUMDEC(y); \
	DEBUGGING_STRING("-"); \
	DEBUGGING_NUMDEC(m); \
	DEBUGGING_STRING("-"); \
	DEBUGGING_NUMDEC(d); \
	DEBUGGING_STRING("/"); \
	DEBUGGING_NUMDEC(hh); \
	DEBUGGING_STRING(":"); \
	DEBUGGING_NUMDEC(mm); \
	DEBUGGING_STRING(":"); \
	DEBUGGING_NUMDEC(ss); \
}
// Note: the wd variable (weekday) is not currently used.

// ----------------------------------------------------------------------------


/* defines for the dot matrix */
#define CLK_HIGH()  PORTB |= (1<<PB2)
#define CLK_LOW()   PORTB &= ~(1<<PB2)
#define CS_HIGH()   PORTB |= (1<<PB1)
#define CS_LOW()    PORTB &= ~(1<<PB1)
#define DATA_HIGH() PORTB |= (1<<PB0)
#define DATA_LOW()  PORTB &= ~(1<<PB0)
#define INIT_PORT() DDRB |= (1<<PB0) | (1<<PB1) | (1<<PB2)
/* end of defines for the dot matrix */

/* characters to display */
uint8_t smile[8] = {
        0b00000000,
        0b01100110,
        0b01100110,
        0b00011000,
        0b00011000,
        0b10000001,
        0b01000010,
        0b00111100};

uint8_t sad[8] = {
        0b00000000,
        0b01100110,
        0b01100110,
        0b00011000,
        0b00011000,
        0b00000000,
        0b00111100,
        0b01000010,
};

uint8_t ok[8] = {
	0b00000000,
	0b01100110,
	0b01100110,
	0b00011000,
	0b00011000,
	0b00000000,
	0b01111110,
	0b00000000,
};

uint8_t clear[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
};

/* end of characters to display */

// ----------------------------------------------------------------------------

uint8_t ds1307_adjust(void)
{
	// return ds1307_setdatetime(2013, 10, 25, 5, 10, 00, 00);	// The Tinusaur Birthday
	return ds1307_setdatetime(2016, 04, 29, 5, 17, 34, 00); // Set date/time
}

// ----------------------------------------------------------------------------

void spi_send(uint8_t data)
{
    uint8_t i;

    for (i = 0; i < 8; i++, data <<= 1)
    {
  CLK_LOW();
  if (data & 0x80)
      DATA_HIGH();
  else
      DATA_LOW();
  CLK_HIGH();
    }
    
}

void max7219_writec(uint8_t high_byte, uint8_t low_byte)
{
    CS_LOW();
    spi_send(high_byte);
    spi_send(low_byte);
    CS_HIGH();
}

void max7219_clear(void)
{
    uint8_t i;
    for (i = 0; i < 8; i++)
    {
  max7219_writec(i+1, 0);
    }
}

void max7219_init(void)
{
    INIT_PORT();
    // Decode mode: none
    max7219_writec(0x09, 0);
    // Intensity: 3 (0-15)
    max7219_writec(0x0A, 1);
    // Scan limit: All "digits" (rows) on
    max7219_writec(0x0B, 7);
    // Shutdown register: Display on
    max7219_writec(0x0C, 1);
    // Display test: off
    max7219_writec(0x0F, 0);
    max7219_clear();
}


uint8_t display[8];

void update_display(void)
{
    uint8_t i;

    for (i = 0; i < 8; i++)
    {
  max7219_writec(i+1, display[i]);
    }
}

void image(uint8_t im[8])
{
    uint8_t i;

    for (i = 0; i < 8; i++)
  display[i] = im[i];
}

void set_pixel(uint8_t r, uint8_t c, uint8_t value)
{
    switch (value)
    {
    case 0: // Clear bit
  display[r] &= (uint8_t) ~(0x80 >> c);
  break;
    case 1: // Set bit
  display[r] |= (0x80 >> c);
  break;
    default: // XOR bit
  display[r] ^= (0x80 >> c);
  break;
    }
}


int main(void)
{
//    uint8_t i;

	/* initialize the DS1307 */
	// ---- Initialization ----
	DEBUGGING_INIT();
	
	// ---- Setup CPU Frequency ----
	#if F_CPU == 1000000UL
	#pragma message "F_CPU=1MHZ"
	CLKPR_SET(CLKPR_1MHZ);
	DEBUGGING_REINIT(OWOWOD_BITLEN_FCPU1MHZ_009600BPS);
	#elif F_CPU == 8000000UL
	#pragma message "F_CPU=8MHZ"
	CLKPR_SET(CLKPR_8MHZ);
	DEBUGGING_REINIT(OWOWOD_BITLEN_FCPU8MHZ_009600BPS);
	#else
	#pragma message "F_CPU=????"
	#error "CPU frequency should be either 1 MHz or 8 MHz"
	#endif

	DEBUGGING_CRLF(); DEBUGGING_STRINGLN("HELLO[DS1307tiny]");	// Not really needed

    //max7219_init();
    //image(sad);
    //update_display();
    //_delay_ms(1000);
    //image(clear);
    //update_display();


	usitwim_init();
	
    //image(smile);
    //update_display();
    //_delay_ms(1000);
    //image(clear);
    //update_display();

	uint8_t ds1307_result;
	// /*DEBUGGING*/ ds1307_setdatetime(2016, 04, 29, 5, 17, 34, 00); // Set date/time
	if ((ds1307_result = ds1307_init()) != DS1307_RESULT_SUCCESS)
	{
		if (ds1307_result == DS1307_RESULT_CLOCKHALT)
		{
			/*DEBUGGING*/ DEBUGGING_STRINGLN("DS1307_RESULT_CLOCKHALT");
			/*DEBUGGING*/ DEBUGGING_STRINGLN("Init: date/time");
			ds1307_adjust();
		}
		else
		{
			/*DEBUGGING*/ DEBUGGING_ERROR(ds1307_result, "init");
			return -1;
		}
	}

/* end of initialize the DS1307 */
        
    while(1)
    {
		//image(sad);
		//update_display();
		//_delay_ms(100);
		//image(ok);
		//update_display();
		//_delay_ms(100);
		//image(smile);
		//update_display();
		//_delay_ms(100);
		//image(ok);
		//update_display();
		//_delay_ms(100);

  // Invert display
//  for (i = 0 ; i < 8*8; i++)
//  {
//      set_pixel(i / 8, i % 8, 2);
//      update_display();
//      _delay_ms(10);
//  }
//  _delay_ms(500);

		/* Work with the DS1307 */
		uint8_t year = ds1307_read_reg8(DS1307_REG_YEAR);
		// /*DEBUGGING*/ DEBUGGING_VARU("year", bcd2bin(year));
		
		uint8_t month = ds1307_read_reg8(DS1307_REG_MONTH);
		// /*DEBUGGING*/ DEBUGGING_VARU("month", month);
		// /*DEBUGGING*/ DEBUGGING_VARU("month", bcd2bin(month));
		
		uint8_t date = ds1307_read_reg8(DS1307_REG_DATE);
		// /*DEBUGGING*/ DEBUGGING_VARU("date", bcd2bin(date));
		
		uint8_t weekday = ds1307_read_reg8(DS1307_REG_WEEKDAY);
		// /*DEBUGGING*/ DEBUGGING_VARU("weekday", weekday);
		
		uint8_t hours = ds1307_read_reg8(DS1307_REG_HOURS);
		// /*DEBUGGING*/ DEBUGGING_VARU("hour", bcd2bin(hours));
		
		uint8_t minutes = ds1307_read_reg8(DS1307_REG_MINUTES);
		// /*DEBUGGING*/ DEBUGGING_VARU("min", bcd2bin(minutes));
		
		uint8_t seconds = ds1307_read_reg8(DS1307_REG_SECONDS);
		// /*DEBUGGING*/ DEBUGGING_VARU("sec", bcd2bin(seconds));

		DEBUGGING_DATETIME(
		bcd2bin(year) + 2000, bcd2bin(month), bcd2bin(date),
		weekday,
		bcd2bin(hours), bcd2bin(minutes), bcd2bin(seconds));
		
		_delay_ms(1000);
		/*DEBUGGING*/ DEBUGGING_CRLF();
		/* end of work with the DS1307 */

    }
}