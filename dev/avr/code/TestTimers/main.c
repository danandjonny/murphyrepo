// Datasheet Location: http://www.atmel.com/Images/Atmel-2505-Setup-and-Use-of-AVR-Timers_ApplicationNote_AVR130.pdf
// ATMEGA328P
//
#define F_CPU 8000000UL														// run CPU at 8 MHz

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include "USART.h"

void debug(const char myString[]);
void debugMetric(const char myString[], signed int anInt);

volatile uint32_t milliseconds = 0;
volatile uint8_t seconds = 0;

/************************************************************************/
/* main program                                                         */
/************************************************************************/

int main(void)
{
	initUSART();
	debug("reached main()");
	TCCR2A |= (1 << WGM21);													// Clear Timer on Compare or CTC mode (WGM2[2:0] = 2) pg. 195, 205
	TIMSK2 |= (1 << OCIE2A);												// output compare interrupt enable
	OCR2A = 249;															// Fire interrupt every millisecond
	TCCR2B |= (1 << CS20) | (1 << CS21); 									// 64
	TCNT2 = 0;
	sei();																	// enable interrupts
	while (1)	{
	}
}

/************************************************************************/
/* Debug Routines                                                       */
/************************************************************************/
void debug(const char myString[]) {
	uint8_t i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
	printString("\r\n");
}

void debugMetric(const char myString[], signed int anInt) {
	uint8_t i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
	char myInt[4];
	itoa(anInt,myInt,10);
	debug(myInt);
}

ISR (TIMER2_COMPA_vect)	{
	milliseconds += 1;
	if (milliseconds % 1000 == 0) {
		seconds += 1;
		debugMetric("Seconds: ", seconds);
	}
}

