/*
 * Lab1.c
 *
 * Created: 1/14/2017 1:12:24 PM
 * Author : djmurphy
 */ 
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "USART.h"

#define begin	{
#define end		}
#define t1	250		// milliseconds
#define t2	125		// milliseconds
#define t3	60		// milliseconds
// need a process running slowly to update the lcd

volatile unsigned char time1, time2, time3;
unsigned char tsk2c, tsk3m, led;
unsigned int time;  // milliseconds since last reset

FILE uart_str = FDEV_SETUP_STREAM(uart_putchar,uart_getchar,_FDEV_SETUP_RW);

ISR(TIMER0_COMPA_vect) {	// timer0 compare A 0
	if (time1 > 0) --time1;
	if (time2 > 0) --time2;
	if (time3 > 0) --time3;
}

void initialize() {
	DDRB = 0x0f;			// make 4 bits of portb output
	PORTB = 0;				// turns on 4 of the LEDs
	DDRD = (1 << PORTD1);	// turn on bit one as an output (bit 1 of port D is the uart transmit line)
	TIMSK0 = (1 << OCIE0A);
	OCR0A = 249;			// 250 counts for one millisecond
	TCCR0A = (1 << WGM01);	// enable clear on compare match
	TCCR0B = 0b00000011;	// prescaler divide by 64
	led = 0xFF;				// all LEDs off
	time1 = t1;
	time2 = t2;
	time3 = t3;
	tsk2c = 4;
	tsk3m = 0;				// button not pushed at reset time
	uart_init();			// set baud rate
	stdout = stdin = stderr = &uart_str;
	fprintf(stdout,"starting...\n\r");
	sei();					// sets the master interrupt enable bit; starts timers
} // initialize

void task1() {
	
}

void task2() {
	
}

void task3() {
	
}

int main(void)
{
	initialize();
    while (1) 
    {
		if (0 == time1) {time1 = t1; task1();}
		if (0 == time2) {time2 = t2; task2();}
		if (0 == time3) {time3 = t3; task3();}		
    }
}

