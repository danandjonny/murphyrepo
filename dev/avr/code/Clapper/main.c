# define F_CPU 1000000UL
#include <avr/io.h>					
#include <util/delay.h>		
#include <avr/interrupt.h>	
#include <avr/sleep.h>	

//********* Definitions *********
#define LED 				PB0		
#define RELAY_ON			PB1		// wired to the base of the transistor that opens the relay

#define PUSHBUTTON			PB4		// tactile input switch
#define CLAP_INPUT			PB2 	// filtered audio signal 
#define SPEAKER				PB3 	// digital audio signal (on/off)
#define CLAP_INT			PCINT2	// inturrupt for sound
#define PUSHBUTTON_INT		PCINT4	// inturrupt for push button

#define DEBOUNCE_TIME		30
#define DEBOUNCE_TIME_SHORT 10
#define INPUT_BLOCK			100

#define PIN_SWITCH				PINB

//**** Function Prototypes *****
void setup(void);
void flash_led( int times );
uint8_t get_adc(void);
uint8_t is_input_low(char port, char channel);
uint8_t is_input_high(char port, char channel);
void sleep(void);

//**** Global Variables & Constants *****
volatile uint16_t milliseconds = 0;

//**** MAIN Program ****
int main(void)
{
	uint8_t buttonPressed = 0;
	setup();

	// set the initial state of the LED and Lamp
	PORTB &= ~( 1 << LED );	// turn off the led

	while(1) {	
		sleep();
		buttonPressed = is_input_low(PINB,PUSHBUTTON);
		// if a clap is heard ( or the switch is pressed ) listen for another and take action
		if( is_input_high( PINB, CLAP_INPUT) || buttonPressed ) {
			milliseconds = 0;
			while( milliseconds < 3000 ) {			// if another clap is heard ( or pushbutton press ) within 3 seconds
				if( is_input_high( PINB, CLAP_INPUT) || buttonPressed ) {
					PORTB ^= (1 << RELAY_ON);		// toggle the relay
					PORTB ^= ( 1 << LED );			// toggle the led
					buttonPressed = 0;
					_delay_ms( 2000 );				// delay to prevent high duty cycle switching
				}
			}
		}

	}
}

void setup(void)
{
	//********* Port Config *********
	//  outputs
	DDRB |=  (( 1 << LED) | (1 << RELAY_ON));
	PORTB &= ~(1 << LED);			// set the outputs low
	PORTB &= ~(1 << RELAY_ON);
		
	// inputs
	DDRB &= ~( ( 1 << PUSHBUTTON ) | ( 1 << CLAP_INPUT ) | ( 1 << SPEAKER ) );
	PORTB |= ( 1 << PUSHBUTTON );	// enable pullup (hardwired as a pullup anyhow)

	//********* Timer Config *********
	TCCR0A |= ( 1 << WGM01 );		// CTC mode and start timer
	TCCR0B |= ( 1 << CS01 );		// with ck/8 div for 125000Hz
	OCR0A = 124;					// top for 1ms interrupts on timer0. Since 125000 / 1000 = 125, then - 1 = 124 since top and bottom are counted
	TIMSK |= ( 1 << OCIE0A );		// OCRA compare match interrupt enable
	
	//********** ADC Config **********
	ADMUX |= ( ( 1 << ADLAR ) | ( 1 << MUX1 ) | ( 1 << MUX0 ) );		// left adjust and select ADC3
	ADCSRA |= ( ( 1 << ADEN ) | ( 1 << ADPS1 ) | ( 1 << ADPS0 ) );		// ADC enable and clock divide 8MHz by 8 for 125khz sample rate
	DIDR0 |= ( 1 << ADC3D );											// disable digital input on analog input channel to conserve power
	
	sei();	// global interrupt enable
}

// this function flashes the indicator led n times, and preserves the state of the LED when 
// when the function is called.
void flash_led( int times )
{
	// read the current state of the LED
	uint8_t led_state = ( bit_is_set( PINB, LED ) ); 
	
	PORTB &= ~( 1 << LED );			// turn off led
//	_delay_ms(50);
	
	for( int i = 0; i < times; i++ ) {
		PORTB |= ( 1 << LED );		// turn on LED
		_delay_ms(25);				// on time
		
		PORTB &= ~( 1 << LED );		// turn off led
		_delay_ms(25);				// off time
	}
	
	// restore LED state
	if( led_state )	{
		PORTB |= ( 1 << LED );
	}
	else {
		PORTB &= ~( 1 << LED );
	}
}

// debounce function
// returns true if the debounced input is high
uint8_t is_input_high( char port, char channel)
{
        if ( bit_is_set( port, channel ) ) {
			_delay_ms( DEBOUNCE_TIME_SHORT );
            if ( bit_is_set( port, channel ) ) {
				_delay_ms( INPUT_BLOCK );
			}
			flash_led(1);
			return 1;
        }
        return 0;
}

// debounce function
// returns true if the debounced input is low
uint8_t is_input_low( char port, char channel ) 
{
        if (bit_is_clear( port, channel ) ) {
			_delay_ms( DEBOUNCE_TIME );
            if (bit_is_clear(port, channel)) {
				_delay_ms( INPUT_BLOCK );
			}
			flash_led(1);
			return 1;
        }
        return 0;
}

uint8_t get_adc()
{
	ADCSRA |= ( 1 << ADSC );			// start the ADC Conversion
	while( ADCSRA & ( 1 << ADSC )); 	// wait for the conversion to be complete
	return ADCH;	// return the 8-bit left adjusted adc value
}

// This ISR iterates a ( 16-bit ) global variable once every millisecond
ISR(TIMER0_COMPA_vect)
{
	++milliseconds;
}

ISR(PCINT0_vect) {
	// This is called when the interrupt occurs, but I don't need to do anything in it
}

void sleep() {

	GIMSK |= _BV(PCIE);								// Enable Pin Change Interrupts
	PCMSK |= _BV(CLAP_INT) | _BV(PUSHBUTTON_INT);   // Use PB2 & PB4 as interrupt pin
	ADCSRA &= ~_BV(ADEN);							// ADC off
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);			// replaces above statement

	sleep_enable();									// Sets the Sleep Enable bit in the MCUCR Register (SE BIT)
	sei();											// Enable interrupts
	sleep_cpu();									// sleep

	cli();											// Disable interrupts
	PCMSK &= ~_BV(CLAP_INT);						// Turn off PB2 as interrupt pin
	PCMSK &= ~_BV(PUSHBUTTON_INT);					// Turn off PB4 as interrupt pin
	sleep_disable();								// Clear SE bit
	ADCSRA |= _BV(ADEN);							// ADC on

	sei();											// Enable interrupts
} // sleep
