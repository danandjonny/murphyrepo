/*
 * Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Wire.h>

#include "ThingSpeak.h"
#include <SPI.h>
//#include <WiFi.h>

const char* ssid = "HOME-4D62";
const char* password = "basket2979fancy";
int readings[200];
int readingNum = 0;
uint8_t loopCount = 0;
uint8_t sensorTurn = 1;
uint8_t sentText = 0;
int status = WL_IDLE_STATUS;

WiFiClient  client;
WiFiClient  clientGet;
IPAddress   serverGet(52,6,12,3);                                   // ThingSpeak 

ESP8266WebServer server ( 80 );

const int led = 16;

void drawGraph() {
  String out = "";
  char temp[100];
  out += "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"1400\" height=\"300\">\n";
  out += "<rect width=\"1400\" height=\"300\" fill=\"rgb(250, 230, 210)\" stroke-width=\"1\" stroke=\"rgb(0, 0, 0)\" />\n";
  out += "<g stroke=\"black\">\n";
  for (int x = 1; x < 120; x += 1) {
    sprintf(temp, "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke-width=\"1\" />\n", x * 10, 140 - readings[x-1], (x * 10) + 10, 140 - readings[x]);
    out += temp;
  }
  out += "</g>\n</svg>\n";

  server.send ( 200, "image/svg+xml", out);
}

void handleRoot() {
	digitalWrite ( led, 1 );
	char temp[400];
	int sec = millis() / 1000;
	int min = sec / 60;
	int hr = min / 60;

	snprintf ( temp, 400,

"<html>\
  <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>ESP8266 Demo</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>CO Sensor</h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
    <img src=\"/test.svg\" />\
  </body>\
</html>",

		hr, min % 60, sec % 60
	);
	server.send ( 200, "text/html", temp );
	digitalWrite ( led, 0 );
}

void handleNotFound() {
	digitalWrite ( led, 1 );
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";

	for ( uint8_t i = 0; i < server.args(); i++ ) {
		message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
	}

	server.send ( 404, "text/plain", message );
	digitalWrite ( led, 0 );
}

void setup ( void ) {
	pinMode ( led, OUTPUT );
	digitalWrite ( led, 0 );
	Serial.begin ( 115200 );
	status = WiFi.begin ( ssid, password );
  if ( status != WL_CONNECTED) { 
    Serial.println("Couldn't get a wifi connection");
  } 
	Serial.println ( "" );

	// Wait for connection
	while ( WiFi.status() != WL_CONNECTED ) {
		delay ( 500 );
		Serial.print ( "." );
	}

	Serial.println ( "" );
	Serial.print ( "WiFi connected to " );
	Serial.println ( ssid );
	Serial.print ( "IP address: " );
	Serial.println ( WiFi.localIP() );

	if ( MDNS.begin ( "esp8266" ) ) {
		Serial.println ( "MDNS responder started" );
	}

	server.on ( "/", handleRoot );
	server.on ( "/test.svg", drawGraph );
	server.on ( "/inline", []() {
		server.send ( 200, "text/plain", "this works as well" );
	} );
	server.onNotFound ( handleNotFound );
	server.begin();
	Serial.println ( "HTTP server started" );

  Wire.begin();   // join i2c bus

  // For ThingSpeak:
  //WiFi.begin(ssid, pass);
  ThingSpeak.begin(client);
}

void loop ( void ) {
  int sensorValue;
  int thresholdValue;
  
  loopCount++;
	
	server.handleClient();

  Wire.requestFrom(0x10, 4);                            // request 4 bytes from slave device #8
  while (Wire.available()) {                            // slave may send less than requested
    int highByteSensor = Wire.read();
    int lowByteSensor = Wire.read();
    sensorValue = (highByteSensor<<8)|lowByteSensor;
    Serial.println(sensorValue);
    int highByteThreshold = Wire.read();
    int lowByteThreshold = Wire.read();
    thresholdValue = (highByteThreshold<<8)|lowByteThreshold;
    Serial.println(thresholdValue);

    for (int i = 200; i > 0; i--) {
      readings[i] = readings[i - 1];
    }
    readings[0] = sensorValue;
  }
  delay(500);                                           // approx. 1 sensor read per second, 1 theshold read per second
                                                        // TODO: make the threshold reads less frequent and sensor reads more frequent
  // See this URL for an example: https://github.com/sandeepmistry/esp8266-Arduino/blob/master/esp8266com/esp8266/libraries/ESP8266WiFi/examples/WiFiClient/WiFiClient.ino
  if ((sensorValue >= thresholdValue) and !sentText) {  // Trigger a text message via ThingSpeak and Twilio if sensor value exceeds threshold value
    sentText = 1;                                       // after 1 SMS is sent, ESP8266 must be reset before another SMS is sent
    if (clientGet.connect(serverGet, 80)) {             // or should it be 443 for HTTPS?
      Serial.println("connected"); 
                                                        // To trigger the text message via browser:  
                                                        // https://api.thingspeak.com/apps/thinghttp/send_request?api_key=YD4YRQ3ZHOX4E2X6
      String command = "GET /apps/thinghttp/send_request?api_key=YD4YRQ3ZHOX4E2X6  HTTP/1.1\r\nHost: api.thingspeak.com\r\nConnection: close\r\n\r\n";
      Serial.println(command);
      clientGet.print(command);
      clientGet.println();
      delay(10);
      while(clientGet.available()){                     // Read all the lines of the reply from server and print them to Serial
        String line = clientGet.readStringUntil('\r');
        Serial.print(line);
      } 
      Serial.println();
      Serial.println("closing connection");
    } else {
      Serial.println("Connection to ThingSpeak for SMS failed.");
    }
  }
  
  if (loopCount > 40) {                                 // @500ms / loop, this is true every 20 seconds
    loopCount = 0;
    if (sensorTurn) {
      sensorTurn = 0;
      ThingSpeak.writeField(107922, 1, sensorValue, "M9EMFPS6V1G1YN2D");    // Write sensor value to ThingSpeak
    } else {
      sensorTurn = 1;
      ThingSpeak.writeField(107922, 2, thresholdValue, "M9EMFPS6V1G1YN2D"); // Write threshold value to ThingSpeak
    }
  }

}
