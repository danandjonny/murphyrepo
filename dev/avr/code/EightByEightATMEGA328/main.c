/*
  One MAX7219 connected to an 8x8 LED matrix.  Works when F_CPU = 8000000
 */

//#define F_CPU 8000000UL //Project > EightByEightMatrix Properties > Toolchain > AVR/GNU C Compiler > Symbols > F_CPU=1000000
// NOTE: The F_CPU (CPU frequency) should not be defined in the source code.
//       It should be defined in either (1) Makefile; or (2) in the IDE.

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <math.h>

#include "USART.h"

#define DEBUG					1
#define SCK						PB5
#define MISO					PB4
#define MOSI					PB3

#define CLK_HIGH()				PORTB |= (1<<SCK)
#define CLK_LOW()				PORTB &= ~(1<<SCK)
#define CS_HIGH()				PORTB |= (1<<MISO)
#define CS_LOW()				PORTB &= ~(1<<MISO)
#define DATA_HIGH()				PORTB |= (1<<MOSI)
#define DATA_LOW()				PORTB &= ~(1<<MOSI)
#define INIT_PORT()				DDRB |= (1<<MOSI) | (1<<MISO) | (1<<SCK)

#define	DS1307_ADDRESS			0xD0									// 208 dec,I2C bus address of DS1307 RTC
#define SECONDS_REGISTER		0x00
#define MINUTES_REGISTER		0x01
#define HOURS_REGISTER			0x02
#define DAYOFWK_REGISTER		0x03
#define DAYS_REGISTER			0x04
#define MONTHS_REGISTER			0x05
#define YEARS_REGISTER			0x06
#define CONTROL_REGISTER		0x07
#define RAM_BEGIN				0x08
#define RAM_END					0x3F
#define F_SCL					100000L									// I2C clock speed 100 KHz
#define READ					1
#define TW_START_OTHER_I2C		0xA4
#define TW_STOP					0x94									// send stop condition (TWINT,TWSTO,TWEN)
#define TW_ACK					0xC4									// return ACK to slave
#define TW_NACK					0x84									// don't return ACK to slave
#define TW_SEND					0x84									// send data (TWINT,TWEN)
#define TW_READY				(TWCR & 0x80)							// ready when TWINT returns to logic 1.
#define TW_STATUS_OTHER_I2C		(TWSR & 0xF8)
#define NUM_I2C_DEVICES			1										// when searching for I2C devices, will loop until this number are found
#define I2C_DEVICE_ONLINE_WAIT	1000									// wait 1 second for I2C devices to come online

#define I2C_Stop() TWCR = TW_STOP										// inline macro for stop condition

/************************************************************************/
/* Typedefs					                                            */
/************************************************************************/
typedef uint8_t byte;

struct stateType {
	uint16_t years;
	byte months;
	byte dayOfWeek;
	byte day;
	byte hours;
	byte minutes;
	byte seconds;
	int rtcRead;
	int timer1ElapsedSecs;
	byte militaryTime;
	int timer1LastSecSampled;
} stateType;

uint8_t display[8];

/************************************************************************/
/* Function Prototypes		                                            */
/************************************************************************/
void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)	;// Writes a byte of data to the device register
byte I2C_ReadRegister(byte busAddr, byte deviceRegister)				;// Reads a byte of data from the device register
byte DecToBcd(byte val)													;// Convert normal decimal numbers to binary coded decimal
byte BcdToDec(byte val)													;// Convert binary coded decimal to normal decimal numbers
void DS1307_ClearClockHaltBit(byte seconds)								;// When clock halt bit is cleared (0) the oscillator is enabled, time advances on the chip
void msDelay(int delay)  												;// Wait for 'delay' milliseconds
void InitRTC()															;// Initialize RTC, read date and time from DS1307.
void ReadTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds) ;// Read time
void ReadDate(volatile uint16_t *inYears, volatile byte *inMonths, volatile byte *inDay, volatile byte *inDayOfWeek);
void debugTime() 														;
void debugDate()														;

/************************************************************************/
/* Global Variables			                                            */
/************************************************************************/
// define the state variable and initialize it
struct stateType volatile g_state = {	.years				 = 0,
										.months				 = 0,
										.dayOfWeek			 = 0,
										.day				 = 0,
										.hours               = 0,
										.minutes             = 0,
										.seconds             = 0,
										.rtcRead             = 0,
										.timer1ElapsedSecs   = 0,
										.militaryTime        = 0,
										.timer1LastSecSampled = -1	};

/************************************************************************/
/* Debug Routines                                                       */
/************************************************************************/
void debug(const char myString[]) {
	if (DEBUG) {
		byte i = 0;
		while (myString[i]) {
			transmitByte(myString[i]);
			i++;
		}
		printString("\r\n");
	}
}

void debugNoLF(const char myString[]) {
	if (DEBUG) {
		byte i = 0;
		while (myString[i]) {
			transmitByte(myString[i]);
			i++;
		}
	}
}

void debugInt(signed int anInt) {
	if (DEBUG) {
		char myInt[4];
		itoa(anInt,myInt,10);
		debug(myInt);
	}
}

void debugMetric(const char myString[], signed int anInt) {
	debugNoLF(myString);debugNoLF(": ");
	debugInt(anInt);
	printString("\r\n");
}

void debugTime() {
	debugMetric("Hours",	g_state.hours	);
	debugMetric("Minutes",	g_state.minutes	);
	debugMetric("Seconds",	g_state.seconds	);
}

void debugDate() {
	debugMetric("Years",	g_state.years		);
	debugMetric("Months",	g_state.months		);
	debugMetric("Day",		g_state.day			);
	debugMetric("DayOfWeek",g_state.dayOfWeek	);
}

/************************************************************************/
/* I2C Routines                                                         */
/************************************************************************/
void I2C_Init()															// Calculate TWBR setting for I2C bit rate
{																		// at 16 , the SCL frequency  will be 16/(16+2(TWBR)), assuming
	debug("I2C_INIT()");
	TWSR = 0;															// set prescalar to zero
	TWBR = ((F_CPU/F_SCL)-16)/2;										// set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)												// look for a device at the specified address
{																		// return 1=found, 0=not found
	TWCR = TW_START_OTHER_I2C;											// send start condition
	int i = 0;
	while ((!TW_READY) && (i++ < 101)) {
	}																	// wait
	TWDR = addr;														// load device's bus address
	TWCR = TW_SEND;														// and send it
	i = 0;
	while ((!TW_READY) && (i++ < 101)) {
	}																	// wait
	if (TW_STATUS_OTHER_I2C!=0x18) {
		//debug("Device not detected.");
		//debugNoLF("Address: ");
		//debugInt(addr);
	}
	return (TW_STATUS_OTHER_I2C==0x18);									// return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)											// returns the count of I2C devices found
{   byte foundCount;
	foundCount = 0;
	for (byte addr=start;addr<0xFF;addr++)								// search all 256 addresses
	{
		if (I2C_Detect(addr)) {											// I2C detected?
			foundCount++;
			debugNoLF("I2C ADDRESS FOUND: ");
			debugInt(addr);
		}
	}
	return foundCount;													// none detected, so return 0.
}

void I2C_Start (byte slaveAddr)											// Look for a device at the specified address.  Same as I2C_Detect.
{   I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)												// sends a data byte to slave
{   TWDR = data;														// load data to be sent
	TWCR = TW_SEND;														// and send it
	int i = 0;
	while ((!TW_READY) && (i++ < 101)) {
		//printString("/");												// wait
	}
	return (TW_STATUS_OTHER_I2C!=0x28);
}

byte I2C_ReadACK ()														// reads a data byte from slave
{   TWCR = TW_ACK;														// ack = will read more data
	int i = 0;
	while ((!TW_READY) && (i++ < 101)) {
		//printString("+");
	}																	// wait
	return TWDR;
}

byte I2C_ReadNACK () 													// reads a data byte from slave
{   TWCR = TW_NACK;														// nack = not reading more data
	int i = 0;
	while ((!TW_READY) && (i++ < 101)) {
		//printString(".");
	}																	// wait
	//printString("\r\n");
	return TWDR;
}

void I2C_WriteByte(byte busAddr, byte data)								// Writes a byte of data to the I2C slave
{   I2C_Start(busAddr);													// send bus address
	I2C_Write(data);													// then send the data byte
	I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)	// Writes a byte of data to the device register
{   I2C_Start(busAddr);													// send bus address
	I2C_Write(deviceRegister);											// first byte = device register address
	I2C_Write(data);													// second byte = data for device register
	I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister)				// Reads a byte of data from the device register
{   byte data = 0;
	I2C_Start(busAddr);													// send device address
	I2C_Write(deviceRegister);											// set register pointer
	I2C_Start(busAddr+READ);											// restart as a read operation
	data = I2C_ReadNACK();												// read the register data
	I2C_Stop();															// stop
	return data;
}

void I2C_FindDevices()													// Makes 100 attempts to find I2C devices
{	int foundCount;
	int findI2CTries;

	foundCount = 0;
	findI2CTries = 0;
	foundCount = I2C_FindDevice (0x00);
	while ((foundCount < (NUM_I2C_DEVICES)) && (findI2CTries < 3))
	{
		findI2CTries += 1;
		debugMetric("FIND DEVICE ATTEMPT #", findI2CTries);
		foundCount = I2C_FindDevice (0x00);
		msDelay(250);
	}
}

/************************************************************************/
/* DS1307 Real Time Clock Interface                                     */
/************************************************************************/
void DS1307_ReadTime(byte *hours, byte *minutes, byte *seconds)			// returns hours, minutes, and seconds in BCD format
{   //debug("DS1307_ReadTime()");
	*hours = I2C_ReadRegister(DS1307_ADDRESS,HOURS_REGISTER);
	*minutes = I2C_ReadRegister(DS1307_ADDRESS,MINUTES_REGISTER);
	*seconds = I2C_ReadRegister(DS1307_ADDRESS,SECONDS_REGISTER);
	if (*hours & 0x40)	{												// 12hr mode:
		*hours &= 0x1F;													// use bottom 5 bits (pm bit = temp & 0x20)
		} else {
		*hours &= 0x3F;													// 24hr mode: use bottom 6 bits
	}
}

void DS1307_ReadDate(byte *years, byte *months, byte *days, byte *dayOfWeek)	// returns months, days, and years in BCD format
{	//debug("DS1307_ReadDate");
	*years	=		I2C_ReadRegister(DS1307_ADDRESS,YEARS_REGISTER	);
	*months =		I2C_ReadRegister(DS1307_ADDRESS,MONTHS_REGISTER	);
	*days	=		I2C_ReadRegister(DS1307_ADDRESS,DAYS_REGISTER	);
	*dayOfWeek =	I2C_ReadRegister(DS1307_ADDRESS,DAYOFWK_REGISTER);
}

void DS1307_WriteTime(byte hours, byte minutes, byte seconds)			// Write time to the DS1307
{	//debug("DS1307_WriteTime()");										// There's a problem with this function; you can't save a time that's in military format and is > 12:59.
	I2C_WriteRegister(DS1307_ADDRESS,HOURS_REGISTER, DecToBcd(hours));
	I2C_WriteRegister(DS1307_ADDRESS,MINUTES_REGISTER, DecToBcd(minutes));
	I2C_WriteRegister(DS1307_ADDRESS,SECONDS_REGISTER, DecToBcd(seconds));
	DS1307_ClearClockHaltBit(0x00);										// To clear the clock halt bit
}

void DS1307_WriteDate(uint16_t years, byte months, byte days, byte dayOfWeek) // Write date to the DS1307
{	//debug("DS1307_WriteDate()");
	//years -= 2000;
	//debugMetric("years: ", years);
	//debugMetric("months: ",  months);
	//debugMetric("days: ", days);
	//debugMetric("dayOfWeek: ", dayOfWeek);
	
	I2C_WriteRegister(DS1307_ADDRESS, YEARS_REGISTER	,DecToBcd(years		));
	I2C_WriteRegister(DS1307_ADDRESS, MONTHS_REGISTER	,DecToBcd(months	));
	I2C_WriteRegister(DS1307_ADDRESS, DAYS_REGISTER		,DecToBcd(days		));
	I2C_WriteRegister(DS1307_ADDRESS, DAYOFWK_REGISTER	,dayOfWeek			 );

}

void DS1307_ClearClockHaltBit(byte seconds)								// When clock halt bit is cleared (0) the oscillator is enabled, time advances on the chip
{	byte clockHaltBit = 0b10000000;
	I2C_WriteRegister(DS1307_ADDRESS,SECONDS_REGISTER, seconds & ~(1 << clockHaltBit));
}

/************************************************************************/
/* Time                                                                 */
/************************************************************************/
void InitRTC()															// Initialize RTC, read date and time from DS1307.
{																		// Bit 7 of Register 0 is the clock halt (CH) bit. When this bit is set to 1, the
	// oscillator is disabled. When cleared to 0, the oscillator is enabled. On
	// first application of power to the device the time and date registers are
	// typically reset to 01/01/00 01 00:00:00 (MM/DD/YY DOW HH:MM:SS). The CH bit
	// in the seconds register will be set to a 1.
	DS1307_ClearClockHaltBit(g_state.seconds);							// clear the clock halt bit
	g_state.rtcRead = 0;

	int badRead = 1;
	int i = 0;
	while ((badRead) && (i++ <= 3)) {
		ReadTime(   &g_state.hours,
					&g_state.minutes,
					&g_state.seconds	);
		ReadDate(	&g_state.years,
					&g_state.months,
					&g_state.day,
					&g_state.dayOfWeek	);
		badRead = 0;
		if (g_state.hours > 23 || g_state.hours < 0) {
			//debugMetric("Bad hours read: ",g_state.hours);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		if (g_state.minutes > 59 || g_state.minutes < 0) {
			//debugMetric("Bad minutes read: ", g_state.minutes);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		if (g_state.seconds > 59 || g_state.seconds < 0) {
			//debugMetric("Bad seconds read: ", g_state.seconds);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		
		if ((g_state.seconds == 0) &&
			(g_state.minutes == 0) &&
			(g_state.seconds == 0)) {
			badRead = 1;
			msDelay(1000);
		}
		//debugMetric("RTC READ ATTEMPT", i);
	}
	DS1307_ClearClockHaltBit(g_state.seconds);							// clear the clock halt bit
}

void ReadTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds) // Read time
{	//debug("ReadTime()");
	if(!g_state.rtcRead) {												// if we have just powered on get the time from the RTC
		byte minutesRTC;
		byte hoursRTC;
		byte secondsRTC;
		DS1307_ReadTime(&hoursRTC,&minutesRTC,&secondsRTC);
		*inHours	= BcdToDec(hoursRTC);
		*inMinutes	= BcdToDec(minutesRTC);
		*inSeconds	= BcdToDec(secondsRTC);
		//g_state.rtcRead = 1;											// comment this out for now because we're not using Timer1 to keep time
		//debugTime();													// this works because g_state.* has been populated by the three lines above; sloppy side affect
	} else {
		*inHours	= g_state.hours;									
		*inMinutes	= g_state.minutes;
		*inSeconds	= g_state.seconds;
	}
}

void ReadDate(volatile uint16_t *inYears, volatile byte *inMonths, volatile byte *inDay, volatile byte *inDayOfWeek) // Read date from DS1307
{	//debug("ReadDate()");
	byte yearsRTC;
	byte monthsRTC;
	byte dayRTC;
	byte dayOfWeekRTC = 0;
	DS1307_ReadDate(&yearsRTC,&monthsRTC,&dayRTC,&dayOfWeekRTC);
		
	//*inYears		= BcdToDec(yearsRTC		) + 2000;
	*inYears		= BcdToDec(yearsRTC		);
	*inMonths		= BcdToDec(monthsRTC	);
	*inDay			= BcdToDec(dayRTC		);
	*inDayOfWeek	= BcdToDec(dayOfWeekRTC	);
	
	//debugDate();														// this works because g_state.* has been populated by the four lines above; sloppy side affect
}

/************************************************************************/
/* Misc Routines                                                        */
/************************************************************************/
void msDelay(int delay)  												// Wait for 'delay' milliseconds
{	for (int i=0;i<delay;i++)											// to remove code inlining
		_delay_ms(1);													// at cost of timing accuracy
}

byte DecToBcd(byte val)													// Convert normal decimal numbers to binary coded decimal
{
	return ( (val/10*16) + (val%10) );
}

byte BcdToDec(byte val)													// Convert binary coded decimal to normal decimal numbers
{
	return ( (val/16*10) + (val%16) );
}

void spi_send(uint8_t data)
{
    uint8_t i;

    for (i = 0; i < 8; i++, data <<= 1)
    {
		CLK_LOW();
		if (data & 0x80)
			DATA_HIGH();
		else
			DATA_LOW();
		CLK_HIGH();
    }
    
}

/************************************************************************/
/* MAX7219 / XYAI5 Functions                                            */
/************************************************************************/

void max7219_writec(uint8_t high_byte, uint8_t low_byte)
{
    CS_LOW();
    spi_send(high_byte);
    spi_send(low_byte);
    CS_HIGH();
}

void max7219_clear(void)
{
    uint8_t i;
    for (i = 0; i < 8; i++)
    {
		max7219_writec(i+1, 0);
    }
}

void max7219_init(void)
{   
	INIT_PORT();
    max7219_writec(0x09, 0);											// Decode mode: none
    max7219_writec(0x0A, 1);											// Intensity: 3 (0-15)
    max7219_writec(0x0B, 7);											// Scan limit: All "digits" (rows) on
    max7219_writec(0x0C, 1);											// Shutdown register: Display on
    max7219_writec(0x0F, 0);											// Display test: off
    max7219_clear();
}


void update_display(void)
{
    uint8_t i;

    for (i = 0; i < 8; i++)
    {
		max7219_writec(i+1, display[i]);
    }
}

void image(uint8_t im[8])
{
    uint8_t i;

    for (i = 0; i < 8; i++)
		display[i] = im[i];
}

void set_pixel(uint8_t r, uint8_t c, uint8_t value)
{
    switch (value)
    {
		case 0: // Clear bit
			display[r] &= (uint8_t) ~(0x80 >> c);
			break;
		case 1: // Set bit
			display[r] |= (0x80 >> c);
			break;
		default: // XOR bit
			display[r] ^= (0x80 >> c);
			break;
    }
}

void ShowOnDisplay(uint8_t im[8]) 
{
	image(im);
	update_display();
}

void InvertDisplay() {
	for (int i = 0 ; i < 8*8; i++)
	{
		set_pixel(i / 8, i % 8, 2);
		update_display();
	}
}

/************************************************************************/
/* Characters                                                          */
/************************************************************************/

uint8_t smile[8] = {
	0b00000000,
	0b01100110,
	0b01100110,
	0b00011000,
	0b00011000,
	0b10000001,
	0b01000010,
	0b00111100};

uint8_t sad[8] = {
	0b00000000,
	0b01100110,
	0b01100110,
	0b00011000,
	0b00011000,
	0b00000000,
	0b00111100,
	0b01000010,
};

uint8_t ok[8] = {
	0b00000000,
	0b01100110,
	0b01100110,
	0b00011000,
	0b00011000,
	0b00000000,
	0b01111110,
	0b00000000,
};

uint8_t clear[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
};

uint8_t time[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
};


/************************************************************************/
/* Initialize                                                           */
/************************************************************************/
void Initialize() {
	initUSART();

	debug("Initializing Max7219");
	max7219_init();
	ShowOnDisplay(sad);													// show a frown on the dot matrix display
	_delay_ms(1000);

	debug("Initialize I2C");
	I2C_Init();															// set I2C clock frequency
	_delay_ms(I2C_DEVICE_ONLINE_WAIT);									// wait for I2C devices to come on line

	debug("Find devices in I2C bus");
	I2C_FindDevices();
	debug("Initialize DS1307 RTC");
	InitRTC();

	ShowOnDisplay(smile);												// show a smile on the dot matrix display
	_delay_ms(1000);
	InvertDisplay();
	_delay_ms(1000);
	ShowOnDisplay(clear);
	
    //DS1307_WriteTime(7, 35, 0);										// save the firmware upload time to the DS1307 the first time the chip is burned but not the second
	//DS1307_WriteDate(17,5,25,5);										// save the firmware upload date to the DS1307 the first time the chip is burned but not the second

}

int main(void)
{
	Initialize();
        
    while(1)
    {
		ReadTime(   &g_state.hours,
					&g_state.minutes,
					&g_state.seconds	);
		ReadDate(	&g_state.years,
					&g_state.months,
					&g_state.day,
					&g_state.dayOfWeek	);
		debugDate();
		debugTime();

		// Straight Binary
		time[0] = g_state.years;
		time[1] = g_state.months;
		time[2] = g_state.day;
		time[4] = g_state.hours;
		time[5] = g_state.minutes;
		time[6] = g_state.seconds;
		
		//Binary Coded Decimal
		//time[0] = DecToBcd(g_state.years);
		//time[1] = DecToBcd(g_state.months);
		//time[2] = DecToBcd(g_state.day);
		//time[4] = DecToBcd(g_state.hours);
		//time[5] = DecToBcd(g_state.minutes);
		//time[6] = DecToBcd(g_state.seconds);
		
		for (uint8_t i = 0; i < 8; i++) {
			uint8_t res = pow(2,i);
			if (res > 2) res++;
			time[7] = res;
			ShowOnDisplay(time);
			_delay_ms(62);												// 62 = trunc(1000ms / 8 leds / 2 up and back)
		}
		for (uint8_t i = 7; i > 0; i--) {
			uint8_t res = pow(2,i);
			if (res > 2) res++;											// TODO: not sure why this is necessary?
			time[7] = res;
			ShowOnDisplay(time);
			time[7] = 1;												// necessary hack to get the last 1 to work
			_delay_ms(63);												// 63 = trunc(1000 / 8 / 2) + 1
		}
		ShowOnDisplay(time);
    }
}