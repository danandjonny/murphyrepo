﻿//-----------------------------------------------------------------------------
// ss01: Experiments with interfacing ATmega328 to an Seven-Segment display
//
// Author : Daniel Murphy <dmurphy@comcast.net>
// Contributors:    Bruce E. Hall <bhall66@gmail.com>
//                  Elliott Williams (Make: AVR Programming)    
// Version : 0.3
// Date : 2015-09-04
// Target : ATmega328P microcontroller
// Language : C
//
// Fuse settings: TODO: document fuse settings

//  ---------------------------------------------------------------------------
//  INCLUDES
#include "Clock.h"

//  GLOBAL VARIABLES
volatile byte portbhistory = 0xFF;     // default is high because of the pull-up
volatile byte count;

//  ---------------------------------------------------------------------------
//  MISC ROUTINES
void debug(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
        printString("\r\n");
    }
}

void debugInt(int anInt) {
    if (DEBUG) {
        char myInt[4];
        itoa(anInt,myInt,10);
        debug(myInt);
    }
}

void InitAVR() {
    debug("InitAVR()");
    DDRB = 0x3F;                        // 0011.1111; set B0-B5 as outputs
    DDRC = 0x00;                        // 0000.0000; set PORTC as inputs
}

void msDelay(int delay)  {              // put into a routine
    for (int i=0;i<delay;i++)           // to remove code inlining
    _delay_ms(1);                       // at cost of timing accuracy
}

void OneShortBeep() {
    debug("OneShortBeep()");
    SetBit(PORTB,BUZZER_PIN);
    msDelay(250);
    ClearBit(PORTB,BUZZER_PIN);
}

void FlashLED() {
//  debug("FlashLED()");
    SetBit(PORTB,LED_PIN);
    msDelay(100);
    ClearBit(PORTB,LED_PIN);
}

// ---------------------------------------------------------------------------
// I2C (TWI) ROUTINES
//
// On the AVRmega series, PA4 is the data line (SDA) and PA5 is the clock (SCL
// The standard clock rate is 100 KHz, and set by I2C_Init. It depends on the AVR osc. freq.
#define F_SCL 100000L                   // I2C clock speed 100 KHz
#define READ 1                          
#define TW_START 0xA4                   // send start condition (TWINT,TWSTA,TWEN)
#define TW_STOP 0x94                    // send stop condition (TWINT,TWSTO,TWEN)
#define TW_ACK 0xC4                     // return ACK to slave
#define TW_NACK 0x84                    // don't return ACK to slave
#define TW_SEND 0x84                    // send data (TWINT,TWEN)
#define TW_READY (TWCR & 0x80)          // ready when TWINT returns to logic 1.
#define TW_STATUS (TWSR & 0xF8)         // returns value of status register
#define I2C_Stop() TWCR = TW_STOP       // inline macro for stop condition

void I2C_Init()
// at 16 MHz, the SCL frequency will be 16/(16+2(TWBR)), assuming prescalar of 0.
// so for 100KHz SCL, TWBR = ((F_CPU/F_SCL)-16)/2 = ((16/0.1)-16)/2 = 144/2 = 72.
{
    debug("InitI2C()");
    TWSR = 0;                           // set prescalar to zero
    TWBR = ((F_CPU/F_SCL)-16)/2;        // set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)
// look for device at specified address; return 1=found, 0=not found
{
    TWCR = TW_START;                    // send start condition
    while (!TW_READY);                  // wait
    TWDR = addr;                        // load device's bus address
    TWCR = TW_SEND;                     // and send it
    while (!TW_READY);                  // wait
    return (TW_STATUS==0x18);           // return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)
// returns with address of first device found; 0=not found
{
    for (byte addr=start;addr<0xFF;addr++)  // search all 256 addresses
    {
        if (I2C_Detect(addr))               // I2C detected?
        return addr;                        // leave as soon as one is found
    }
    return 0;                               // none detected, so return 0.
}

void I2C_Start (byte slaveAddr)
{
    I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)                  // sends a data byte to slave
{
    TWDR = data;                            // load data to be sent
    TWCR = TW_SEND;                         // and send it
    while (!TW_READY);                      // wait
    return (TW_STATUS!=0x28);
}

byte I2C_ReadACK ()                         // reads a data byte from slave
{
    TWCR = TW_ACK;                          // ack = will read more data
    while (!TW_READY);                      // wait
    return TWDR;
    //return (TW_STATUS!=0x28);
}

byte I2C_ReadNACK ()                        // reads a data byte from slave
{
    TWCR = TW_NACK;                         // nack = not reading more data
    int i = 0;
    while ((!TW_READY) &&(i++ < 101)) {
        printString(".");
    }                      // wait
    printString("\r\n");
    return TWDR;
    //return (TW_STATUS!=0x28);
}

void I2C_WriteByte(byte busAddr, byte data)
{
    I2C_Start(busAddr);                     // send bus address
    I2C_Write(data);                        // then send the data byte
    I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)
{
    I2C_Start(busAddr);                     // send bus address
    I2C_Write(deviceRegister);              // first byte = device register address
    I2C_Write(data);                        // second byte = data for device register
    I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister)
{
    byte data = 0;
    I2C_Start(busAddr);                     // send device address
    I2C_Write(deviceRegister);              // set register pointer
    I2C_Start(busAddr+READ);                // restart as a read operation
    data = I2C_ReadNACK();                  // read the register data
    I2C_Stop();                             // stop
    return data;
}

// ---------------------------------------------------------------------------
// DS1307 RTC ROUTINES
#define DS1307              0xD0                         // I2C bus address of DS1307 RTC
//#define DS1307              0x68                         // I2C bus address of DS1307 RTC
#define SECONDS_REGISTER    0x00
#define MINUTES_REGISTER    0x01
#define HOURS_REGISTER      0x02
#define DAYOFWK_REGISTER    0x03
#define DAYS_REGISTER       0x04
#define MONTHS_REGISTER     0x05
#define YEARS_REGISTER      0x06
#define CONTROL_REGISTER    0x07
#define RAM_BEGIN           0x08
#define RAM_END             0x3F

void DS1307_GetTime(byte *hours, byte *minutes, byte *seconds)
// returns hours, minutes, and seconds in BCD format
{
//  debug("DS1307_GetTime()");
    *hours = I2C_ReadRegister(DS1307,HOURS_REGISTER);
    *minutes = I2C_ReadRegister(DS1307,MINUTES_REGISTER);
    *seconds = I2C_ReadRegister(DS1307,SECONDS_REGISTER);
    if (*hours & 0x40)                      // 12hr mode:
        *hours &= 0x1F;                         // use bottom 5 bits (pm bit = temp & 0x20)
    else *hours &= 0x3F;                    // 24hr mode: use bottom 6 bits
}

void DS1307_GetDate(byte *months, byte *days, byte *years)
// returns months, days, and years in BCD format
{
    *months = I2C_ReadRegister(DS1307,MONTHS_REGISTER);
    *days = I2C_ReadRegister(DS1307,DAYS_REGISTER);
    *years = I2C_ReadRegister(DS1307,YEARS_REGISTER);
}

void SetTimeDate()
// simple, hard-coded way to set the date.
{
    I2C_WriteRegister(DS1307,MONTHS_REGISTER, 0x08);
    I2C_WriteRegister(DS1307,DAYS_REGISTER, 0x31);
    I2C_WriteRegister(DS1307,YEARS_REGISTER, 0x13);
    I2C_WriteRegister(DS1307,HOURS_REGISTER, 0x08+0x40); // add 0x40 for PM
    I2C_WriteRegister(DS1307,MINUTES_REGISTER, 0x51);
    I2C_WriteRegister(DS1307,SECONDS_REGISTER, 0x00);
}

void InitRTCCHBit() {
/*  Bit 7 of Register 0 is the clock halt (CH) bit. When this bit is set to 1, the
    oscillator is disabled. When cleared to 0, the oscillator is enabled. On 
    first application of power to the device the time and date registers are 
    typically reset to 01/01/00 01 00:00:00 (MM/DD/YY DOW HH:MM:SS). The CH bit 
    in the seconds register will be set to a 1. 
*/
    I2C_WriteRegister(DS1307,SECONDS_REGISTER,0x00);
}

void WriteTime(byte hours, byte minutes) 
{
    debug("WriteTime()");
    // get the hour portion of the hours:
    byte hour = hours - (hours / 10 * 10);
    // get tens hour portion
    byte tenHours = hours / 10;
    // put tenHours in bits 5 & 4, put hour in bit 3 - 0, put 24 hr mode flag in 
    // bit 6.
    byte writeHours = tenHours << 4 | hour | 0x40; 
    I2C_WriteRegister(DS1307,HOURS_REGISTER, writeHours); 
    // get the minute portion of minutes:
    byte minute = minutes - (minutes / 10 * 10);
    // get tens minutes portion
    byte tenMinutes = minutes / 10;
    // put tenMinutes in bit 6, 5 & 4, put minute in bit 3 - 0
    byte writeMinutes = tenMinutes << 4 | minute;
    I2C_WriteRegister(DS1307,MINUTES_REGISTER, writeMinutes);
}

// ---------------------------------------------------------------------------
// 7-SEGMENT BACKPACK (HT16K33) ROUTINES
//
// The HT16K33 driver contains 16 bytes of display memory, mapped to 16 row x 8 column output
// Each column can drive an individual 7-segment display; only 0-4 are used for this device.
// Each row drives a segment of the display; only rows 0-6 are used.
//
//         0        For example, to display the number 3, we need to light up segments
//      -------     0, 1, 2, 3, and 6. This would be binary 0100.1111 or 0x4F.
//     5|     |1
//      |  6  |     Mapping to the display address memory:
//      -------       0x00 Digit 0 (left most digit)
//     4|     |2      0x02 Digit 1
//      |  3  |       0x04 colon ":" on bit1
//      -------       0x06 Digit 2
//                    0x08 Digit 4 (right-most digit)
//
#define HT16K33             0xE0            // I2C bus address for Ht16K33 backpack
#define HT16K33_ON          0x21            // turn device oscillator on
#define HT16K33_STANDBY     0x20            // turn device oscillator off
#define HT16K33_DISPLAYON   0x81            // turn on output pins
#define HT16K33_DISPLAYOFF  0x80            // turn off output pins
#define HT16K33_BLINKON     0x85            // blink rate 1 Hz (-2 for 2 Hz)
#define HT16K33_BLINKOFF    0x81            // same as display on
#define HT16K33_DIM         0xE0            // add level (15=max) to byte
static const byte numberTable[] =           // convert number to lit-segments
{
    0x3F, // 0
    0x06, // 1
    0x5B, // 2
    0x4F, // 3
    0x66, // 4
    0x6D, // 5
    0x7D, // 6
    0x07, // 7
    0x7F, // 8
    0x6F, // 9
    0x77, // A
    0x7C, // b
    0x39, // C
    0x5E, // d
    0x79, // E
    0x71, // F
    0x00, //<blank>
};

void SS_Init()
{
    debug("SS_Init()");
    I2C_WriteByte(HT16K33,HT16K33_ON);          // turn on device oscillator
    I2C_WriteByte(HT16K33,HT16K33_DISPLAYON);   // turn on display, no blink
    I2C_WriteByte(HT16K33,HT16K33_DIM + 15);    // set max brightness
}

void SS_SetDigitRaw(byte digit, byte data)      // digits (L-to-R) are 0,1,2,3
// Send segment-data to specified digit (0-3) on LED display
{
    if (digit>4) return;                        // only digits 0-4
    if (digit>1) digit++;                       // skip over colon @ position 2
    digit <<= 1;                                // multiply by 2
    I2C_WriteRegister(HT16K33,digit,data);      // send segment-data to display
}

void SS_BlankDigit(byte digit)
// Blanks out specified digit (0-3) on LED display
{
    SS_SetDigitRaw(digit,0x00);                 // turn off all segments on specified digit
}

void SS_SetDigit(byte digit, byte data)
// display data value (0-F) on specified digit (0-3) of LED display
{
    if (data>0x10) return;                      // only values <=16
    SS_SetDigitRaw(digit,numberTable[data]);    // show value on display
}

void SS_SetColon(byte data)                     // 0=off, 1=on
// the colon is represented by bit1 at address 0x04. There are three other single LED
// "decimal points" on the display, which are at the following bit positions
// bit2=top left, bit3=bottom left, bit4=top right
{
    I2C_WriteRegister(HT16K33,0x04,data<<1);
}

void SS_SetDigits(byte d0, byte d1, byte d2, byte d3, byte colon)
{
    SS_SetDigit(0,d0);
    SS_SetDigit(1,d1);
    SS_SetDigit(2,d2);
    SS_SetDigit(3,d3);
    SS_SetColon(colon);
}

void SS_Integer(int data, byte base)
{
    char st[5]="";
    itoa(data,st,base);                         // convert to string
    byte len = strlen(st);
    if (len>4) return;
    for (byte digit=0; digit<4; digit++)        // for all 4 digits
    {
        byte blanks = 4-len;                    // number of blanks
        if (digit<blanks)                       // right-justify display
        SS_SetDigit(digit,0x10);                // padding with blanks
        else
        {
            char ch = st[digit-blanks];         // get char for this digit
            if (ch>='a') ch-=87;                // correct for hex digits
            else ch-='0';                       // ascii -> numeric value
            SS_SetDigit(digit,ch);              // display digit
        }
    }
}

// ---------------------------------------------------------------------------
// APPLICATION ROUTINES
void SS_IntegerTest()                           // count 0 to 255 on display
{
    for (int i=0;i<256;i++)
    {
        SS_Integer(i,16);                       // choose your base here: 16=hex
        msDelay(150);
    }
}

void SS_BeefTest()                              // displays pulsating 'beeF' message
{
    debug("SS_BeefTest()");
    SS_SetDigits(0x0b,0x0e,0x0e,0x0f,0);        // write 'beeF'
    for (byte count=0; count<3; count++)
    {
        for (byte j=15; j>0; j--)               // gradually dim the display
        {
            I2C_WriteByte(HT16K33,HT16K33_DIM + j);
            msDelay(100);
        }
        for (byte j=0; j<16; j++)               // gradually brighten the display
        {
            I2C_WriteByte(HT16K33,HT16K33_DIM + j);
            msDelay(100);
        }
    }
}

void SS_CircleTest()                            // show rotating circle on each digit
{
    debug("SS_CircleTest()");
    for (byte count=0; count<15; count++)
    {
        for (byte i=0; i<6; i++)                // display each segment in turn
        {
            SS_SetDigitRaw(0,1<<i);
            SS_SetDigitRaw(1,1<<i);
            SS_SetDigitRaw(2,1<<i);
            SS_SetDigitRaw(3,1<<i);
            msDelay(100);
        }
    }
}

/*
void ShowTime(byte hours, byte minutes)
// display current time on 7-segment LED display from BCD input
{
    SS_SetDigits(   hours >> 4,         // 10 hour digit
                    hours & 0x0F,       // 1 hour digit
                    minutes >> 4,       // 10 minute digit
                    minutes & 0x0F,     // 1 minute digit
                    1               );  // turn on colon
}
*/

void ColonOn() {
    SS_SetColon(1);
}

void ColonOff() {
    SS_SetColon(0);
}

void ShowTime(byte hours, byte minutes) {
    ShowHour(hours);
    ShowMinute(minutes);
    ColonOn();  // 1 turns on colon, 0 turns it off
}

void ShowTemp(byte tempF) {
    byte ones = tempF - (tempF / 10) * 10;
    byte tens = tempF / 10;
    SS_SetDigit(0,tens);
    SS_SetDigit(1,ones);
    SS_SetDigit(2,0x0f);
    SS_SetDigit(3,0x10);
    ColonOff();
}

void ShowHour(byte hours) {
    SS_SetDigit(0, hours / 10);
    SS_SetDigit(1, hours - (hours / 10 * 10));
}

void ShowMinute(byte minutes) {
    SS_SetDigit(2, minutes / 10);
    SS_SetDigit(3, minutes - (minutes / 10 * 10));
}

/* Show "AC" (Alarm Clock) in the minutes position */
void BlankMinuteForAlarm() {
    SS_SetDigit(2, 0x0a);
    SS_SetDigit(3, 0x0c);    
}

/* Show "AC" (Alarm Clock) in the hours position */
void BlankHourForAlarm() {
    SS_SetDigit(0, 0x0a);
    SS_SetDigit(1, 0x0c);    
}

void BlankMinuteForTime() {
    SS_SetDigit(2, 0x0c);
    SS_SetDigit(3, 0x0c);    
}

void BlankHourForTime() {
    SS_SetDigit(0, 0x0c);
    SS_SetDigit(1, 0x0c);    
}

void initInturrupts() {
    // Clear the BUTTON_STATE_PIN, BUTTON_ACTION_PIN, SWITCH_PIN pin
    DDRB &= ~((1 << DDB0) | (1 << DDB1) | (1 << DDB2)); 
    /*  BUTTON_STATE_PIN,BUTTON_ACTION_PIN,SWITCH_PIN (PCINT0, PCINT1, PCINT2 
        pin) are now inputs */
                                                        
    /* turn On the Pull-up */
    PORTB |= ((1 << PORTB0) | (1 << PORTB1) | (1 << PORTB2)); 
    /*  BUTTON_STATE_PIN, BUTTON_ACTION_PIN and SWITCH_PIN are now inputs with 
        pull-up enabled */

    /* Pin Change Interrupt Control Register */
    PCICR |= (1 << PCIE0);     // set PCIE0 to enable PCMSK0 scan
    /* Pin Change Mask Register 0 */
    /* set PCINT0 to trigger an interrupt on state change */
    PCMSK0 |= (1 << PCINT0) | (1 << PCINT1) | (1 << PCINT2);   
    /* Turn on interrupts */
    sei();                  
}

// ---------------------------------------------------------------------------
// INTURRUPTS
ISR (PCINT0_vect)
{
    debug("ISR()");
    byte changedbits;

    changedbits = PINB ^ portbhistory;
    portbhistory = PINB;
    
    char myString[4];
    itoa(changedbits,myString,16);
    debug(myString);

    if(changedbits & (1 << BUTTON_ACTION_PIN))
    {
        if( (portbhistory & (1 << BUTTON_ACTION_PIN)) == 2 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PINB, BUTTON_ACTION_PIN)) {
                /* LOW to HIGH pin change (action button released)*/
                FlashLED();
                // debug("PCINT0_vect, PINB1 set, low to high");
                ProcessEvent(EVENT_PRESS_ACTION);
            }
        }
        else
        {
            /* HIGH to LOW pin change (action button pressed) */
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PINB, BUTTON_ACTION_PIN)) {            /* still pressed */
                FlashLED();
                // debug("PCINT0_vect, PINB1 set, high to low");
            }
        }
    }

    if(changedbits & (1 << BUTTON_STATE_PIN))
    {
    /* PCINT0 changed */
        if( (portbhistory & (1 << BUTTON_STATE_PIN)) == 1 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PINB, BUTTON_STATE_PIN)) {
                /* LOW to HIGH pin change (state button released) */
                FlashLED();
                // debug("PCINT0_vect, PINB0 set, low to high");
                ProcessEvent(EVENT_PRESS_STATE);
            }
        }
        else
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PINB, BUTTON_STATE_PIN)) {            /* still pressed */
                /* HIGH to LOW pin change (state button pressed) */
                FlashLED();
                // debug("PCINT0_vect, PINB0 set, high to low");
            }
        }
    }

    if(changedbits & (1 << SWITCH_PIN))
    {
    /* PCINT0 changed */
        if( (portbhistory & (1 << SWITCH_PIN)) == 4 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PINB, SWITCH_PIN)) {
                /* LOW to HIGH pin change (alarm switch right) */
                FlashLED();
                // debug("PCINT0_vect, PINB2 set, low to high");
                ProcessEvent(EVENT_SWITCH_OFF);
            }
        }
        else
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PINB, SWITCH_PIN)) {            /* still pressed */
                /* HIGH to LOW pin change (alarm switch left) */
                FlashLED();
                // debug("PCINT0_vect, PINB2 set, high to low");
                ProcessEvent(EVENT_SWITCH_ON);
            }
        }
    }
}

/* Timer overflow interrupt */
ISR(TIMER0_OVF_vect) {
    count++;
    if (count == 250) {
        float photoVolts =  (256 - (((float) ReadADC(PHOTO_PIN) / 1024) * 256)) - 150;
//      debug("photoVolts");
//      debugInt(photoVolts);
        OCR0A = photoVolts;
        count=0;
    }
}


/* Initialize the timer */
void initTimer0(void)
{
    DDRD |= (1 << DDD6);                    /* PD6 is now an output           */
    OCR0A = 0;                              /* set this for PWM duty cycle    */
    TCCR0A |= (1 << COM0A1);                /* set none-inverting mode        */
    TCCR0A |= (1 << WGM01) | (1 << WGM00);  /* set fast PWM Mode              */
    TCCR0B |= (1 << CS01);                  /* set prescaler to 8, starts PWM */
    TIMSK0 |= (1 << TOIE0);                 /* overflow interrupt enable      */
    sei();
}

// Initialize analog to digital conversion
void InitADC0() {
  ADMUX |= (1 << REFS0);                                /* reference voltage on AVCC */
  ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); /* ADC clock prescaler/128   */
  ADCSRA |= (1 << ADEN);                                /* enable ADC                */
}



// ---------------------------------------------------------------------------
// MAIN PROGRAM

void Initialize() {
    initUSART();
    debug("Initialize()");
    debug("Initializing AVR");
    InitAVR();                                  // set port direction
    debug("Done initializing AVR.  Send one short beep.");
    FlashLED();
    OneShortBeep();
    debug("Done with short beep.  Initializing I2C.");
    FlashLED();
    I2C_Init();                                 // set I2C clock frequency
    debug("Done initializing I2C.  Initialize HT16K33 LED controller.");
    FlashLED();
    SS_Init();                                  // initialize HT16K33 LED controller
    debug("Done initializing LED controller. Initialize ADC.");
    FlashLED();
    InitADC0();
    debug("Done initializing ADC. Initialize timer.");
    FlashLED();
    initTimer0();
    debug("Done initializing Timer. Initialize inturrupts.");
    FlashLED();
    initInturrupts();
    debug("Done initializing inturrupts.  Initialize RTC.");
    FlashLED();
    debug("Initialize the RTC.");
    FlashLED();
    InitRTCCHBit();
    debug("Done initializing RTC.");
    FlashLED();
//    SS_BeefTest();                              // pulsating 'beef' message
//    debug("Done with beef test.  Executing integer test.\r\n");
//    FlashLED();
//    SS_IntegerTest();                           // count in hexadecimal
//    debug("Done with beef test.  Executing circle test.");
//    FlashLED();
//    SS_CircleTest();                            // circle animation
//    debug("Done with circle test.  Starting main loop.");
//    FlashLED();
 
    /* Initialize the clock state */
    g_state.mode = SHOWING_TIME;
    g_state.alarmHours = 0;
    g_state.alarmMinutes = 0;
    
    /* Read the wake time from chip memory */
    ReadAlarmTime(&g_state.alarmHours, &g_state.alarmMinutes);
    /*  Wake hours and minutes are corrupt the first time they are read
        from memory, clean that up. */
    if ((g_state.alarmHours < 0) || (g_state.alarmHours > 12)) {
        g_state.alarmHours = 0;
    }
    if ((g_state.alarmMinutes < 0) || (g_state.alarmMinutes > 59)) {
        g_state.alarmMinutes = 0;
    }
    InitAlarmSwitch();

    debug("Finished initialization.\r\n");
}

void WriteAlarmTime(byte hours, byte minutes) {
    debug("WriteAlarmTime()");
    doubleByte alarmTimeWrite;
    alarmTimeWrite = (hours * 100) + minutes;
    debug("alarmTimeWrite:");
    debugInt((int) alarmTimeWrite);
    eeprom_update_word((doubleByte *) ALARM_ADDRESS, alarmTimeWrite);

    debugAlarmTime();
}

/* Determine if the alarm switch is on or off and process the appropriate event
*/
void InitAlarmSwitch() { 
    // if switch is OFF
    if (bit_is_set(PINB, SWITCH_PIN)) {
        ProcessEvent(EVENT_SWITCH_OFF);
    }
    // if switch is ON
    if (bit_is_clear(PINB, SWITCH_PIN)) {            
        ProcessEvent(EVENT_SWITCH_ON);
    }
}

void ReadAlarmTime(byte *hours, byte *minutes) {
    debug("ReadAlarmTime()");
    doubleByte alarmTimeInt;
    alarmTimeInt = eeprom_read_word((doubleByte *) ALARM_ADDRESS);
    *hours = alarmTimeInt / 100;
    *minutes = alarmTimeInt - ((alarmTimeInt / 100) * 100);

    debug("Read alarm time: ");
    debugAlarmTime();
}   

void debugAlarmTime() {
    char myHours[4];
    itoa(g_state.alarmHours,myHours,10);
    debug("Alarm Hours: ");
    debug(myHours);

    char myMinutes[4];
    itoa(g_state.alarmMinutes,myMinutes,10);
    debug("Alarm Minutes: ");
    debug(myMinutes);
}

void debugTime() {
    char myHours[4];
    itoa(g_state.hours,myHours,10);
    debug("Hours: ");
    debug(myHours);

    char myMinutes[4];
    itoa(g_state.minutes,myMinutes,10);
    debug("Minutes: ");
    debug(myMinutes);
}

void SoundAlarm()
{
    debug("SoundAlarm()");
    SetBit(PORTB,BUZZER_PIN);
}

void SilenceAlarm()
{
    debug("SilenceAlarm()");
    ClearBit(PORTB,BUZZER_PIN);
}


void SnoozeAlarm()
{
    debug("SnoozeAlarm()");
    SilenceAlarm();
    g_state.alarmMinutes += SNOOZE_TIME;
    if (g_state.alarmMinutes > 59) {
        g_state.alarmMinutes = g_state.alarmMinutes - 60;
        g_state.alarmHours += 1;
    }
}


int WasWakeAlarmTriggered() { // TODO: pass in the structure by value
//  debug("WasWakeAlarmTriggered()");
    if (!((g_state.mode & BUZZING) == BUZZING)) {
        if ((g_state.hours == g_state.alarmHours) &&
            (g_state.minutes == g_state.alarmMinutes) &&
            (g_state.seconds >= 0 && g_state.seconds <= 2)) {
                debug("Wake alarm triggered");
                return 1;
        }
    }
    return 0;
}

void GetTime(byte *inHours, byte *inMinutes, byte *inSeconds) {
//  debug("GetTime()");
    byte minutesRTC;
    byte hoursRTC;
    byte secondsRTC;
    DS1307_GetTime(&hoursRTC,&minutesRTC,&secondsRTC);
    *inHours =   (hoursRTC >> 4) * 10 + (hoursRTC & 0x0F);
    *inMinutes = (minutesRTC >> 4) * 10 + (minutesRTC & 0x0F);
    *inSeconds = (secondsRTC >> 4) * 10 + (secondsRTC & 0x0F);
}

int ConvertReadingToF(int reading) {
    float voltage = reading * (5000/1024);
    float temperatureC = (voltage - 500) / 10;
    // now convert to Fahrenheit
    float temperatureF = (temperatureC * 9.0 / 5.0) + 32.0;
    return(temperatureF);
}

int ReadADC(int channel) {
//    debug("ReadADC()");
    ADMUX = (0xf0 & ADMUX) | channel;  /* set channel */
                                       /* first sample after you’ve changed the 
                                          multiplexer will still be from the old 
                                          analog source—you need to wait at 
                                          least one complete ADC cycle before 
                                          getting a value from the new channel */
    ADCSRA |= (1 << ADSC);                     /* start ADC conversion */
    loop_until_bit_is_clear(ADCSRA, ADSC);     /* wait until done */
    ADCSRA |= (1 << ADSC);                     /* start ADC conversion */
    loop_until_bit_is_clear(ADCSRA, ADSC);     /* wait until done */
    doubleByte adcValue = ADC;                   /* read ADC in */
    return(adcValue);
}

int main(void)
{    
    Initialize(&g_state);
    debug("main()");
    while(1) {
        ProcessEvent(EVENT_TIME_CHANGED);
        if (WasWakeAlarmTriggered(g_state)) {
            ProcessEvent(EVENT_WAKE_TRIGGERED);
        }
        FlashLED();
        msDelay(1000);                          // one half second between updates
    }
}

void ProcessEvent(byte event)
{
//  debug("ProcessEvent()");
//  debug("event:");
//  debugInt(event);
//  debug("g_state.mode.mode:");
//  debugInt(g_state.mode);
    if (event == EVENT_TIME_CHANGED) {
        byte lastMinute = g_state.minutes;  // so we can check if the minute changed
        /* If the time isn't being set, get the time from the RTC */
        if (!(g_state.mode & SETTING_TIME_HOUR) && 
            !(g_state.mode & SETTING_TIME_MINUTE)) {  
            GetTime( &g_state.hours, 
                     &g_state.minutes, 
                     &g_state.seconds);
        }
        if (g_state.mode & SHOWING_TIME) {
            /* If the minute changed, display new time on the LEDs */
            if (lastMinute != g_state.minutes) {
                ShowTime(g_state.hours, g_state.minutes);
            }
        }
    /*  if the STATE (left) button was pressed */
    } else if (event == EVENT_PRESS_STATE) {
        /*  If the clock is showing time and is NOT sounding the alarm, put it 
            into set wake hour mode */
        if ( (g_state.mode & SHOWING_TIME) &&
            !(g_state.mode & BUZZING) ) {
            g_state.mode = (g_state.mode ^ SHOWING_TIME) | SETTING_WAKE_HOUR;
            ShowHour(g_state.alarmHours);
            BlankMinuteForAlarm(); 
        /*  If the clock is setting the wake hour, put it into setting wake 
            minute mode */
        } else if (g_state.mode & SETTING_WAKE_HOUR) {
            g_state.mode = (g_state.mode ^ SETTING_WAKE_HOUR) | SETTING_WAKE_MINUTE;
            ShowMinute(g_state.alarmMinutes);
            BlankHourForAlarm();
        /*  If clock is setting wake minute set the mode to setting time hour */
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {
            g_state.mode ^= SETTING_WAKE_MINUTE; // Turn off setting wake minute
            g_state.mode |= SETTING_TIME_HOUR; // Turn on setting time hour
            WriteAlarmTime(g_state.alarmHours, g_state.alarmMinutes);
            ShowHour(g_state.hours);
            BlankMinuteForTime();
        } else if (g_state.mode & SETTING_TIME_HOUR) {
            g_state.mode ^= SETTING_TIME_HOUR;
            g_state.mode |= SETTING_TIME_MINUTE;
            ShowMinute(g_state.minutes);
            BlankHourForTime();
        } else if (g_state.mode & SETTING_TIME_MINUTE) {
            g_state.mode ^= SETTING_TIME_MINUTE; // stop showing minute
            g_state.mode |= SHOWING_TIME; // go back to showing time
            // save the new time to the RTC
            WriteTime(g_state.hours, g_state.minutes); 
            ShowTime(g_state.hours, g_state.minutes);
        } else if (g_state.mode & SHOWING_TEMP) {
            g_state.mode ^= SHOWING_TEMP; // turn off showing temp
            g_state.mode |= SHOWING_TIME; // go back to showing time
            ShowTime(g_state.hours, g_state.minutes);
        }
        /*  No matter which mode the clock is in, if the state button is pressed 
            silence the alarm if it's sounding */
        if (g_state.mode & BUZZING) {
            g_state.mode ^= BUZZING; // remove BUZZING from the mode.
            SilenceAlarm();
        }
    /* if the ACTION (right) button was pressed */
    } else if (event == EVENT_PRESS_ACTION) {
        if (g_state.mode & SHOWING_TIME) {
            g_state.mode ^= SHOWING_TIME;
            g_state.mode |= SHOWING_TEMP;
//          byte tempF = ReadTemp();
            byte tempF = ConvertReadingToF(ReadADC(TEMP_PIN));
            ShowTemp(tempF);
        }
        /* if setting wake hour, increment the hour */
        if (g_state.mode & SETTING_WAKE_HOUR) {
            g_state.alarmHours += 1;
            debugInt(g_state.alarmHours);
            if (g_state.alarmHours > 23) {
                g_state.alarmHours = 0;
            }
            ShowHour(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {
        /* if setting wake minute, increment the minute */
            g_state.alarmMinutes += 1;
            if (g_state.alarmMinutes > 59) {
                g_state.alarmMinutes = 0;
            }
            ShowMinute(g_state.alarmMinutes);
        } else if (g_state.mode & SETTING_TIME_HOUR) {
            g_state.hours += 1;            
            if (g_state.hours > 23) {
                g_state.hours = 0;
            }
            ShowHour(g_state.hours);
        } else if (g_state.mode & SETTING_TIME_MINUTE) {
            g_state.minutes += 1;
            if (g_state.minutes > 59) {
                g_state.minutes = 0;
            }
            ShowMinute(g_state.minutes);
        } else if (g_state.mode & BUZZING) {
        /* If the alarm is sounding, change the clock mode to snoozing */
            g_state.mode ^= BUZZING;   // remove BUZZING from the mode
            g_state.mode |= SNOOZING;  // add snoozing
            SnoozeAlarm(&g_state);
        }
    /*  if it's time to sound the alarm to wake up; sound the alarm if the switch
        is on  */
    } else if (event == EVENT_WAKE_TRIGGERED) {
        if (!(g_state.mode & WAKE)) {
            g_state.mode |= BUZZING;
            SoundAlarm();
        }
    } else if (event == EVENT_SWITCH_OFF) {
        g_state.mode |= WAKE; // wake alarm is off, set the bit
        if (g_state.mode & BUZZING) {
            g_state.mode ^= BUZZING; // remove BUZZING from the mode.
            SilenceAlarm();
        }
   } else if (event == EVENT_SWITCH_ON) {
        g_state.mode &= ~WAKE; // wake alarm is on, clear the bit
   }
}

