// Standard AVR includes
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>              // Needed to use interrupts

// Standard includes
#include <string.h>                     // string manipulation routines
#include <stdlib.h>
#include <stdint.h>                     // has to be added to use uint8_t

#include "USART.h"
#include "macros.h"

#define DEBOUNCE_TIME  500              // microseconds

// Clock Modes
#define BUZZING                 0x01    // 1  1     00000000.00000001
#define SNOOZING                0x02    // 2  2     00000000.00000010
#define SHOWING_TIME            0x04    // 3  4     00000000.00000100
#define SETTING_TIME_HOUR       0x08    // 4  8     00000000.00001000
#define SETTING_TIME_MINUTE     0x10    // 5  16    00000000.00010000
#define SETTING_WAKE_HOUR       0x20    // 6  32    00000000.00100000
#define SETTING_WAKE_MINUTE     0x40    // 7  64    00000000.01000000
#define SHOWING_TEMP            0x80    // 8  128   00000000.10000000
#define SETTING_TUNE            0x100   // 9  256   00000001.00000000
#define SHOWING_CO              0x200   // 10 512   00000010.00000000
#define SHOWING_FIRE            0x400   // 11 1024  00000100.00000000
#define SHOWING_MOTION          0x800   // 12 2048  00001000.00000000
#define WAKE                    0x1000  // 13 4096  00010000.00000000


// Events
#define EVENT_NONE              0
#define EVENT_PRESS_STATE       1
#define EVENT_PRESS_ACTION      2
#define EVENT_SWITCH_ON         3
#define EVENT_SWITCH_OFF        4
#define EVENT_VOLUME_ADJUST     5
#define EVENT_TIME_CHANGED      6
#define EVENT_WAKE_TRIGGERED    7
#define EVENT_CO_DETECTED       8
#define EVENT_SMOKE_DETECTED    9
#define EVENT_MOTION_DETECTED   10

// Pin definitions
#define LED_PIN                 PD5     // heartbeat LED
#define BUZZER_PIN              PB3     // piezo buzzer
#define CO_POWER_PIN            PD7     // CO sensor for power
#define CO_READ_PIN             PC2     // where we read CO level
#define BUTTON_ACTION_PIN       PB1 
#define BUTTON_STATE_PIN        PB0 
#define SWITCH_PIN              PB2     // turns alarm and CO sensor on and off
#define TEMP_PIN                PC0     // for measuring temperature
#define PHOTO_PIN               PC1     // the photo-sensitive resistor
#define ALARM_ADDRESS           0x00

// Application specific defines
#define SNOOZE_TIME             10      // TODO: allow the user to set this
#define CO_ALARM_THRESHOLD      600     // TODO: Make this configurable

// Controls output to UART
#define DEBUG                   1

// Clock speed; external crystal required
#define F_CPU 16000000UL                // run CPU at 16 MHz

// Macros
#define ShiftBit(bit) (1 << (bit))
//#define ClearBit(x,y) x &= ~ShiftBit(y)
//#define SetBit(x,y) x |= ShiftBit(y)   
//#define ToggleBit(x,y) x ^= ShiftBit(y)

#define ClearBit(x,y) x &= ~y
#define SetBit(x,y) x |= y   
#define ToggleBit(x,y) x ^= y


#define ClearBitNo(x,y) x &= ~_BV(y)    // equivalent to cbi(x,y)
#define SetBitNo(x,y) x |= _BV(y)       // equivalent to sbi(x,y)


//  Typedefs
typedef uint8_t byte;                   // I just like byte & sbyte better
typedef uint16_t doubleByte;

// Function Prototypes
void ProcessEvent(byte event);
void Initialize();
void ShowTime(byte hours, byte minutes);
void ShowHour(byte hours);
void ShowMinute(byte minutes);
void BlankMinuteForAlarm();
void BlankHourForAlarm();
void WriteAlarmTime(byte hours, byte minutes);
void ReadAlarmTime();
void debugAlarmTime();
void debugInt(int anInt);
void WriteTime(byte hours, byte minutes);
void debugTime();
void GetTime(byte *inHours, byte *inMinutes, byte *inSeconds);
void ReadAlarmTime(byte *hours, byte *minutes);
void ColonOn();
void ColonOff();
void InitAlarmSwitch();
int ConvertReadingToF(int reading);
int ReadTemp();
void InitADC0();
void InitRTCCHBit();
void initTimer0(void);
int ReadADC(int channel);
int WasCOGasAlarmTriggered(void);
byte CheckForEvents();
void debug(const char myString[]);
void msDelay(int delay);
void FlashLED();
void InitAVR();
void OneShortBeep();
void initTimer0(void);
void initTimer1(void);
void initTimer2(void);
void initInturrupts();
void BlankMinuteForTime();
void BlankHourForTime();
void ShowTemp(byte tempF);
void DisplayWarnForCO();
int main(void);

// Structures
struct clockStateType {
    doubleByte mode;
    byte hours;
    byte minutes;
    byte seconds;
    byte alarmHours;
    byte alarmMinutes;
    byte setTimeHours;
    byte setTimeMinutes;
    int rtcRead;
    byte event;
    int timer2ElapsedSecs;
    doubleByte COreading;
} clockStateType;

// Global Variables
// define the state variable and initialize it
struct clockStateType g_state = {  .mode = SHOWING_TIME,
                                   .hours = 0,
                                   .minutes = 0,
                                   .seconds = 0,
                                   .alarmHours = 0,
                                   .alarmMinutes = 0,
                                   .setTimeHours = 0,
                                   .setTimeMinutes = 0,
                                   .rtcRead = 0,
                                   .event = 0,
                                   .timer2ElapsedSecs = 0,
                                   .COreading = 0    };
