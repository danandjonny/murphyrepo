// Standard AVR includes
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>    // Needed to use interrupts

// Standard includes
#include <string.h>                     // string manipulation routines
#include <stdlib.h>
#include <stdint.h>            // has to be added to use uint8_t

#include "USART.h"
#include "macros.h"

#define DEBOUNCE_TIME  500         /* microseconds */

// Clock Modes
#define IS_BUZZING                 0x01     // 1    00000000.00000001
#define IS_SNOOZING                0x02     // 2    00000000.00000010
#define IS_SHOWING_TIME            0x04     // 4    00000000.00000100
#define IS_SETTING_TIME_HOUR       0x08     // 8    00000000.00001000
#define IS_SETTING_TIME_MINUTE     0x10     // 16   00000000.00010000
#define IS_SETTING_WAKE_HOUR       0x20     // 32   00000000.00100000
#define IS_SETTING_WAKE_MINUTE     0x40     // 64   00000000.01000000
#define IS_SHOWING_TEMP            0x80     // 128  00000000.10000000
#define IS_SETTING_TUNE            0x100    // 256  00000001.00000000
#define IS_SHOWING_CO              0x200    // 512  00000010.00000000
#define IS_SHOWING_FIRE            0x400    // 1024 00000100.00000000
#define IS_SHOWING_MOTION          0x800    // 2048 00001000.00000000
#define IS_WAKE                    0x1000   // 4096 00010000.00000000

// Events
#define EVENT_NONE                  0
#define EVENT_PRESS_STATE           1
#define EVENT_PRESS_ACTION          2
#define EVENT_SWITCH_OFF            3
#define EVENT_VOLUME_ADJUST         4
#define EVENT_WAKE_TRIGGERED        5
#define EVENT_TIME_CHANGED          6
#define EVENT_CO_DETECTED           7
#define EVENT_SWITCH_ON             8

#define LED_PIN                 5   // Boarduino LED on PB5
#define BUZZER_PIN              3   // piezo buzzer
#define BUTTON_ACTION_PIN       PB0 // TODO: make the substitutions in the C file (are PB0 and PB1 reversed?)
#define BUTTON_STATE_PIN        PB1 // TODO: make the substitutions in the C file (are PB0 and PB1 reversed?)
#define SWITCH_PIN              PB2 // TODO: determine the pin for the switch
#define ALARM_ADDRESS           0x00

#define SNOOZE_TIME             10  // TODO: allow the user to set this

#define DEBUG                   1

#define F_CPU 16000000L                 // run CPU at 16 MHz
//#define F_CPU 1000000L 
#define ClearBit(x,y) x &= ~_BV(y)      // equivalent to cbi(x,y)
#define SetBit(x,y) x |= _BV(y)         // equivalent to sbi(x,y)

//  ---------------------------------------------------------------------------
//  TYPEDEFS
// TODO: make this type consistant, substitute byte for uint8_t
typedef uint8_t byte;                   // I just like byte & sbyte better
//  ---------------------------------------------------------------------------

// Function Prototypes

int main(void);
void ProcessEvent(uint8_t event);
void Initialize();
void ShowTime(uint8_t hours, uint8_t minutes);
void ShowHour(uint8_t hours);
void ShowMinute(uint8_t minutes);
void BlankMinuteForAlarm();
void BlankHourForAlarm();
void WriteAlarmTime(uint8_t hours, uint8_t minutes);
void ReadAlarmTime();
void debugAlarmTime();
void debugInt(int anInt);
void WriteTime(uint8_t hours, uint8_t minutes);
void debugTime() ;
void GetTime(byte *inHours, byte *inMinutes, byte *inSeconds);
void ReadAlarmTime(byte *hours, byte *minutes);
void ColonOn();
void ColonOff();
void InitAlarmSwitch();

struct clockStateType {
    uint16_t clockMode;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
    uint8_t alarmHours;
    uint8_t alarmMinutes;
};

struct clockStateType g_clockState;

