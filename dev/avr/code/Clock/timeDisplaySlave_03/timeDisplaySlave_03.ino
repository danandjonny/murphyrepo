/*
********************************************************************
  Name    : timeDisplaySlave_03
  Author  : Daniel Murphy
  Date    : October 25, 2015
  Version : 3
  Notes   : The purpose of this program is to function as an I2C slave
          : responsible for displaying numerals and characters on a 
          : series of 4 single character seven segment displays set up
          : in a circuit with S8550 PNP transistors for multiplexed
          : switching. A shift register is used to shuttle a value to 
          : all of the SS displays, and the transistor associated with
          : the character position for which the value is intended is
          : switched on.  When the next number comes through the shift
          : register, it's transistor is turned on, and so on.  Values
          : coming in to the slave from the master have already been 
          : mapped so as to light the correct segments on the display.
          : See Clock.h for the mapping.  The setup includes 32 330ohm
          : resistors, 4 single character seven segment displays, 4
          : S8550 transistors, a 74HC595 shift register, and 4 
          : resistors (brown, brown, black, red, red) for the transistor 
          : switching.
********************************************************************

7 Segment (Common Anode) Display Map: 

    D   E  5V   F   G
 ___|___|___|___|___|____
|                        |
|        F               |
|    E       G           |
|        D               |
|    A       C           |
|        B       H(Dot)  |
|________________________|
    |   |   |   |   |
    A   B  5V   C   H

74HC595 Map:
     _______
Q1  |1 *  16|  Vcc                  PINS 1-7, 15   Q0 - Q7   Output Pins
Q2  |2    15|  Q0                   PIN 8	   GND	     Ground, Vss
Q3  |3    14|  DS                   PIN 9	   Q7"	     Serial Out
Q4  |4    13|  OE                   PIN 10	   MR	     Master Reclear, active low
Q5  |5    12|  ST_CP                PIN 11	   SH_CP     Shift register clock pin
Q6  |6    11|  SH_CP                PIN 12	   ST_CP     Storage register clock pin (latch pin)
Q7  |7    10|  MR                   PIN 13	   OE	     Output enable, active low
GND |8_____9|  Q7"                  PIN 14	   DS	     Serial data input
                                    PIN 16	   Vcc	     Positive supply voltage
           _______
   LED Q1-|1 *  16|-5V
   LED Q2-|2    15|-LED Q0
   LED Q3-|3    14|-PIN 11
   LED Q4-|4    13|-GND
   LED Q5-|5    12|-PIN 8
   LED Q6-|6    11|-PIN 12 ; 1uF TO GND
   DOT Q7-|7    10|-5V
      GND-|8_____9|-NILL

The values passed in from the master map to characters as follows:

Array Value      Character
-----------      ---------
0                0
1                1
2                2
...
9                9
10               A
11               B
...
35               Z
(decimal point)  36
(space)          37

This mapping is controlled on the master in Clock.h.

*/
#include <Wire.h>

int latchPin       = 8;     //Pin connected to ST_CP of 74HC595
int clockPin       = 10;    //Pin connected to SH_CP of 74HC595
int dataPin        = 9;     //Pin connected to DS of 74HC595
int hourTensPin    = 5;    // switch to turn on tens position for hours
int hourOnesPin    = 4;    // switch to turn on ones position for hours
int minuteTensPin  = 3;    // switch to turn on tens position for minutes
int minuteOnesPin  = 2;    // switch to turn on ones position for minutes
int hourTensPos    = 1;
int hourOnesPos    = 2;
int minuteTensPos  = 3;
int minuteOnesPos  = 4;
byte hourTensVal;
byte hourOnesVal;
byte minuteTensVal;
byte minuteOnesVal;
int delayTime = 5;

int slaveAddress = 16;  // for some reason this slave is detected on address 32 when set to 16 here.

byte theTime[4] = {0,0,0,0};

void setup() {     

  pinMode(latchPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  
  pinMode(hourTensPin, OUTPUT);
  pinMode(hourOnesPin, OUTPUT);
  pinMode(minuteTensPin, OUTPUT);
  pinMode(minuteOnesPin, OUTPUT);
    
  Wire.begin(slaveAddress);         // join i2c bus with address #8
  Wire.onReceive(receiveEvent);     // register event
}

void loop() {
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, hourTensVal);  
  digitalWrite(latchPin, HIGH);   // take the latch pin high so the LEDs will light up:
  digitalWrite(hourTensPin, LOW); // turn on the switch
  delay(delayTime);               // wait
  digitalWrite(hourTensPin, HIGH);// turn off the switch
  
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, hourOnesVal);  
  digitalWrite(latchPin, HIGH); //take the latch pin high so the LEDs will light up:
  digitalWrite(hourOnesPin, LOW);
  delay(delayTime);
  digitalWrite(hourOnesPin, HIGH);
  
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, minuteTensVal);  
  digitalWrite(latchPin, HIGH);   // take the latch pin high so the LEDs will light up:
  digitalWrite(minuteTensPin, LOW); // turn on the switch
  delay(delayTime);               // wait
  digitalWrite(minuteTensPin, HIGH);// turn off the switch
  
  digitalWrite(latchPin, LOW);  
  shiftOut(dataPin, clockPin, MSBFIRST, minuteOnesVal);  
  digitalWrite(latchPin, HIGH);   // take the latch                                                                   pin high so the LEDs will light up:
  digitalWrite(minuteOnesPin, LOW); // turn on the switch
  delay(delayTime);               // wait
  digitalWrite(minuteOnesPin, HIGH);// turn off the switch
}

// This function executes whenever data is received from The 
// master.  The function is registered as an event (see setup()).
void receiveEvent(int howMany) {
  int i = 0;
  while (1 <= Wire.available()) { // loop through all
    theTime[i] = Wire.read();
    i++;
  }
  hourTensVal = theTime[hourTensPos];
  hourOnesVal = theTime[hourOnesPos];
  minuteTensVal = theTime[minuteTensPos];
  minuteOnesVal = theTime[minuteOnesPos] + theTime[0];
}

