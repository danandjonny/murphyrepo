﻿//-----------------------------------------------------------------------------
// ss01: Experiments with interfacing ATmega328 to an Seven-Segment display
//
// Author : Daniel Murphy <dmurphy@comcast.net>
// Contributors:    Bruce E. Hall <bhall66@gmail.com>
//                  Elliott Williams (Make: AVR Programming)    
// Version : 0.6
// Date : 2015-09-09
// Target : ATmega328P microcontroller
// Language : C
//
// Fuse settings: TODO: document fuse settings

//  ---------------------------------------------------------------------------
//  INCLUDES
#include "Clock.h"

//  GLOBAL VARIABLES
volatile byte portbhistory = 0xFF;     // default is high because of the pull-up
volatile byte count;

//  ---------------------------------------------------------------------------
//  MISC ROUTINES
void debug(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
        printString("\r\n");
    }
}

void debugNoLF(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
    }
}

void debugNoLN(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
    }
}

void debugInt(signed int anInt) {
    if (DEBUG) {
        char myInt[4];
        itoa(anInt,myInt,10);
        debug(myInt);
    }
}

void InitAVR() {
    debug("InitAVR()");
    DDRB = 0b00111000;

//    DDRB = 0x3F;                        // 0011.1111; set B0-B5 as outputs
//    DDRB |= (1 << DDB3);
//    DDRB &= ~((1 << DDB0) | (1 << DDB1) | (1 << DDB2)); 

//    DDRC = 0x00;                        // 0000.0000; set PORTC as inputs
    DDRC = 0b00001000;
}

void msDelay(int delay)  {              // put into a routine
    for (int i=0;i<delay;i++)           // to remove code inlining
    _delay_ms(1);                       // at cost of timing accuracy
}

void OneShortBeep() {
    debug("OneShortBeep()");
    SetBitNo(PORTB,BUZZER_PIN);
    msDelay(250);
    ClearBitNo(PORTB,BUZZER_PIN);
}

void FlashLED() {
//  debug("FlashLED()");
    SetBitNo(PORTC,LED_PIN);
    msDelay(100);
    ClearBitNo(PORTC,LED_PIN);
}

// ---------------------------------------------------------------------------
// I2C (TWI) ROUTINES
//
// On the AVRmega series, PA4 is the data line (SDA) and PA5 is the clock (SCL
// The standard clock rate is 100 KHz, and set by I2C_Init. It depends on the AVR osc. freq.
#define F_SCL 100000L                   // I2C clock speed 100 KHz
#define READ 1                          
#define TW_START 0xA4                   // send start condition (TWINT,TWSTA,TWEN)
#define TW_STOP 0x94                    // send stop condition (TWINT,TWSTO,TWEN)
#define TW_ACK 0xC4                     // return ACK to slave
#define TW_NACK 0x84                    // don't return ACK to slave
#define TW_SEND 0x84                    // send data (TWINT,TWEN)
#define TW_READY (TWCR & 0x80)          // ready when TWINT returns to logic 1.
#define TW_STATUS (TWSR & 0xF8)         // returns value of status register
#define I2C_Stop() TWCR = TW_STOP       // inline macro for stop condition

void I2C_Init()                                         // at 16 MHz, the SCL frequency 
                                                        // will be 16/(16+2(TWBR)), assuming 
                                                        // prescalar of 0. So for 100KHz SCL, 
                                                        // TWBR = ((F_CPU/F_SCL)-16)/2 = ((16/0.1)-16)/2 = 144/2 
                                                        // = 72.
{
    debug("I2C_Init()");
    TWSR = 0;                                           // set prescalar to zero
    TWBR = ((F_CPU/F_SCL)-16)/2;                        // set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)                              // look for device at specified address; 
                                                        // return 1=found, 0=not found
{
    TWCR = TW_START;                                    // send start condition
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        //printString("-");
    }                                                   // wait
    TWDR = addr;                                        // load device's bus address
    TWCR = TW_SEND;                                     // and send it
    i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        //printString("|");
    }                                                   // wait
    if (TW_STATUS!=0x18) {
        //debug("Device not detected.");
        //debugNoLF("Address: ");
        //debugInt(addr);
    }
    return (TW_STATUS==0x18);                           // return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)                         // returns with address of first device found; 0=not found
{
    for (byte addr=start;addr<0xFF;addr++)              // search all 256 addresses
    {
        if (I2C_Detect(addr)) {                         // I2C detected?
            debugNoLF("I2C Address Found: ");
            debugInt(addr);       
//          return addr;                                // leave as soon as one is found
        }
    }
    return 0;                                           // none detected, so return 0.
}

void I2C_Start (byte slaveAddr)
{
    I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)                              // sends a data byte to slave
{
    TWDR = data;                                        // load data to be sent
    TWCR = TW_SEND;                                     // and send it
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString("/");                               // wait
    }
    return (TW_STATUS!=0x28);
}

byte I2C_ReadACK ()                                     // reads a data byte from slave
{
    TWCR = TW_ACK;                                      // ack = will read more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString("+");
    }                      // wait
    return TWDR;
    //return (TW_STATUS!=0x28);
}

byte I2C_ReadNACK ()                                    // reads a data byte from slave
{
    TWCR = TW_NACK;                                     // nack = not reading more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString(".");
    }                      // wait
    printString("\r\n");
    return TWDR;
    //return (TW_STATUS!=0x28);
}

void I2C_WriteByte(byte busAddr, byte data)
{
    I2C_Start(busAddr);                                 // send bus address
    I2C_Write(data);                                    // then send the data byte
    I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)
{
    I2C_Start(busAddr);                                 // send bus address
    I2C_Write(deviceRegister);                          // first byte = device register address
    I2C_Write(data);                                    // second byte = data for device register
    I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister)
{
    byte data = 0;
    I2C_Start(busAddr);                                 // send device address
    I2C_Write(deviceRegister);                          // set register pointer
    I2C_Start(busAddr+READ);                            // restart as a read operation
    data = I2C_ReadNACK();                              // read the register data
    I2C_Stop();                                         // stop
    return data;
}

void Cust_SS_WriteChars(byte charOne, byte charTwo, byte charThree, byte charFour) {
    I2C_Start(CUST_SS_LED_ADDR);                        // send bus address
    byte instruction = SetSSDisplayInstruction();
    I2C_Write(instruction);                             // first byte = device register address
    I2C_Write(ssCharMap[charOne]);                      // hours tens pos
    I2C_Write(ssCharMap[charTwo]);                      // hours ones pos
    I2C_Write(ssCharMap[charThree]);                    // minute tens pos
    I2C_Write(ssCharMap[charFour]);                     // minute ones pos
    I2C_Stop();
}

byte SetSSDisplayInstruction() {
    byte instruction = 0;
    if (g_state.militaryTime) {
        instruction |= 1 << MILITARY_TIME;
    }
    
    if (g_state.colonOn) {
        instruction |= 1 << DECIMAL_ON;
    }
    
    if (g_state.decimalBlink) {
        instruction |= 1 << DECIMAL_BLINK;
    }
    return instruction;     
}

void Cust_SS_WriteTime(byte hours, byte minutes)
{
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;
    
    I2C_Start(CUST_SS_LED_ADDR);                        // send bus address
    
    byte instruction = SetSSDisplayInstruction();
        
    I2C_Write(instruction);                             // first byte = instruction settings
    if (tensHours) {
        I2C_Write(ssCharMap[tensHours]);                // hours tens pos
    } else {
        I2C_Write(ssCharMap[CHAR_SPACE]);               // hours tens pos
    }
    I2C_Write(ssCharMap[onesHours]);                    // hours ones pos
    I2C_Write(ssCharMap[tensMinutes]);                  // minute tens pos
    I2C_Write(ssCharMap[onesMinutes]);// minute ones pos
    I2C_Stop();
}

void Cust_SS_WriteTimeAlarmSetHours(byte hours) {
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);                        // send bus address
    I2C_Write(0);                                       // colon off during set time
    I2C_Write(ssCharMap[tensHours]);                    // hours tens pos
    I2C_Write(ssCharMap[onesHours]);                    // hours ones pos
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_H]);      
    I2C_Stop();
}

void Cust_SS_WriteTimeAlarmSetMinutes(byte minutes) {
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);                        // send bus address
    I2C_Write(0);                                       // colon off during set time
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_SPACE]);      
    I2C_Write(ssCharMap[tensMinutes]);                  // minutes tens pos
    I2C_Write(ssCharMap[onesMinutes]);                  // minutes ones pos
    I2C_Stop();
}

void Cust_SS_WriteTimeSetHours(byte hours) {
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);                        // send bus address
    byte instruction = SetSSDisplayInstruction();
    I2C_Write(0);                             
    I2C_Write(ssCharMap[tensHours]);                    // hours tens pos
    I2C_Write(ssCharMap[onesHours]);                    // hours ones pos
    I2C_Write(ssCharMap[CHAR_S]);      
    I2C_Write(ssCharMap[CHAR_H]);      
    I2C_Stop();
}

void Cust_SS_WriteTimeSetMinutes(byte minutes) {
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);                        // send bus address
    byte instruction = SetSSDisplayInstruction();
    I2C_Write(0);                                       // colon off during set time
    I2C_Write(ssCharMap[CHAR_S]);      
    I2C_Write(ssCharMap[CHAR_SPACE]);      
    I2C_Write(ssCharMap[tensMinutes]);                  // minutes tens pos
    I2C_Write(ssCharMap[onesMinutes]);// minutes ones pos
    I2C_Stop();
}

// ---------------------------------------------------------------------------
// DS1307 RTC ROUTINES
#define DS1307              0xD0                         // 208 dec,I2C bus address of DS1307 RTC
//#define DS1307            0x68                         // I2C bus address of DS1307 RTC
#define SECONDS_REGISTER    0x00
#define MINUTES_REGISTER    0x01
#define HOURS_REGISTER      0x02
#define DAYOFWK_REGISTER    0x03
#define DAYS_REGISTER       0x04
#define MONTHS_REGISTER     0x05
#define YEARS_REGISTER      0x06
#define CONTROL_REGISTER    0x07
#define RAM_BEGIN           0x08
#define RAM_END             0x3F

void DS1307_GetTime(byte *hours, byte *minutes, byte *seconds)
// returns hours, minutes, and seconds in BCD format
{
//  debug("DS1307_GetTime()");
    *hours = I2C_ReadRegister(DS1307,HOURS_REGISTER);
    *minutes = I2C_ReadRegister(DS1307,MINUTES_REGISTER);
    *seconds = I2C_ReadRegister(DS1307,SECONDS_REGISTER);
    if (*hours & 0x40)                                  // 12hr mode:
        *hours &= 0x1F;                                 // use bottom 5 bits (pm bit = temp & 0x20)
    else *hours &= 0x3F;                                // 24hr mode: use bottom 6 bits
}

void DS1307_GetDate(byte *months, byte *days, byte *years)
// returns months, days, and years in BCD format
{
    *months = I2C_ReadRegister(DS1307,MONTHS_REGISTER);
    *days = I2C_ReadRegister(DS1307,DAYS_REGISTER);
    *years = I2C_ReadRegister(DS1307,YEARS_REGISTER);
}

void InitRTCCHBit() {
/*  Bit 7 of Register 0 is the clock halt (CH) bit. When this bit is set to 1, the
    oscillator is disabled. When cleared to 0, the oscillator is enabled. On 
    first application of power to the device the time and date registers are 
    typically reset to 01/01/00 01 00:00:00 (MM/DD/YY DOW HH:MM:SS). The CH bit 
    in the seconds register will be set to a 1. 
*/
    I2C_WriteRegister(DS1307,SECONDS_REGISTER,0x00);
}

void WriteTime(byte hours, byte minutes) 
{
    debug("WriteTime()");
    // get the hour portion of the hours:
    byte hour = hours - (hours / 10 * 10);
    // get tens hour portion
    byte tenHours = hours / 10;
    // put tenHours in bits 5 & 4, put hour in bit 3 - 0, put 24 hr mode flag in 
    // bit 6.
    byte writeHours = tenHours << 4 | hour | 0x40; 
    I2C_WriteRegister(DS1307,HOURS_REGISTER, writeHours); 
    // get the minute portion of minutes:
    byte minute = minutes - (minutes / 10 * 10);
    // get tens minutes portion
    byte tenMinutes = minutes / 10;
    // put tenMinutes in bit 6, 5 & 4, put minute in bit 3 - 0
    byte writeMinutes = tenMinutes << 4 | minute;
    I2C_WriteRegister(DS1307,MINUTES_REGISTER, writeMinutes);
}

// ---------------------------------------------------------------------------
// 7-SEGMENT BACKPACK (HT16K33) ROUTINES
//
// The HT16K33 driver contains 16 bytes of display memory, mapped to 16 row x 8 column output
// Each column can drive an individual 7-segment display; only 0-4 are used for this device.
// Each row drives a segment of the display; only rows 0-6 are used.
//
//         0        For example, to display the number 3, we need to light up segments
//      -------     0, 1, 2, 3, and 6. This would be binary 0100.1111 or 0x4F.
//     5|     |1
//      |  6  |     Mapping to the display address memory:
//      -------       0x00 Digit 0 (left most digit)
//     4|     |2      0x02 Digit 1
//      |  3  |       0x04 colon ":" on bit1
//      -------       0x06 Digit 2
//                    0x08 Digit 4 (right-most digit)
//
#define HT16K33             0xE0            // 224 dec, I2C bus address for Ht16K33 backpack
#define HT16K33_ON          0x21            // turn device oscillator on
#define HT16K33_STANDBY     0x20            // turn device oscillator off
#define HT16K33_DISPLAYON   0x81            // turn on output pins
#define HT16K33_DISPLAYOFF  0x80            // turn off output pins
#define HT16K33_BLINKON     0x85            // blink rate 1 Hz (-2 for 2 Hz)
#define HT16K33_BLINKOFF    0x81            // same as display on
#define HT16K33_DIM         0xE0            // add level (15=max) to byte
static const byte numberTable[] =           // convert number to lit-segments
{
    0x3F, // 0
    0x06, // 1
    0x5B, // 2
    0x4F, // 3
    0x66, // 4
    0x6D, // 5
    0x7D, // 6
    0x07, // 7
    0x7F, // 8
    0x6F, // 9
    0x77, // A
    0x7C, // b
    0x39, // C
    0x5E, // d
    0x79, // E
    0x71, // F
    0x00, //<blank>
};

void SS_Init()
{
    debug("SS_Init()");
    I2C_WriteByte(HT16K33,HT16K33_ON);          // turn on device oscillator
    I2C_WriteByte(HT16K33,HT16K33_DISPLAYON);   // turn on display, no blink
    I2C_WriteByte(HT16K33,HT16K33_DIM + 15);    // set max brightness
}

void SS_SetDigitRaw(byte digit, byte data)      // digits (L-to-R) are 0,1,2,3
// Send segment-data to specified digit (0-3) on LED display
{
    if (digit>4) return;                        // only digits 0-4
    if (digit>1) digit++;                       // skip over colon @ position 2
    digit <<= 1;                                // multiply by 2
    I2C_WriteRegister(HT16K33,digit,data);      // send segment-data to display
}

void SS_BlankDigit(byte digit)
// Blanks out specified digit (0-3) on LED display
{
    SS_SetDigitRaw(digit,0x00);                 // turn off all segments on specified digit
}

void SS_SetDigit(byte digit, byte data)
// display data value (0-F) on specified digit (0-3) of LED display
{
    if (data>0x10) return;                      // only values <=16
    SS_SetDigitRaw(digit,numberTable[data]);    // show value on display
}

void SS_SetColon(byte data)                     // 0=off, 1=on
// the colon is represented by bit1 at address 0x04. There are three other single LED
// "decimal points" on the display, which are at the following bit positions
// bit2=top left, bit3=bottom left, bit4=top right
{
    I2C_WriteRegister(HT16K33,0x04,data<<1);
}

void SS_SetDigits(byte d0, byte d1, byte d2, byte d3, byte colon)
{
    SS_SetDigit(0,d0);
    SS_SetDigit(1,d1);
    SS_SetDigit(2,d2);
    SS_SetDigit(3,d3);
    SS_SetColon(colon);
}

void SS_Integer(int data, byte base)
{
    char st[5]="";
    itoa(data,st,base);                         // convert to string
    byte len = strlen(st);
    if (len>4) return;
    for (byte digit=0; digit<4; digit++)        // for all 4 digits
    {
        byte blanks = 4-len;                    // number of blanks
        if (digit<blanks)                       // right-justify display
        SS_SetDigit(digit,0x10);                // padding with blanks
        else
        {
            char ch = st[digit-blanks];         // get char for this digit
            if (ch>='a') ch-=87;                // correct for hex digits
            else ch-='0';                       // ascii -> numeric value
            SS_SetDigit(digit,ch);              // display digit
        }
    }
}


// ---------------------------------------------------------------------------
// APPLICATION ROUTINES

void ToggleColon() {
    if (g_state.mode & WAKE) {    
        if (g_state.colonOn) {
            ColonOff();
        } else {
            ColonOn();
        }
        //Cust_SS_WriteTime(g_state.hours, g_state.minutes);
    }
}

void ColonOn() {
    g_state.colonOn = 1;
    SS_SetColon(1);
}

void ColonOff() {
    g_state.colonOn = 0;
    SS_SetColon(0);
}

void ShowTime(byte hours, byte minutes) {

    Cust_SS_WriteTime(hours, minutes);

    ShowHour(hours);
    ShowMinute(minutes);
    ColonOn();  
}

void ShowHour(byte hours) {
    if (hours > 12 && !g_state.militaryTime) {
        hours = hours - 12;
    }
    int tensHours = hours / 10;
    if (tensHours > 0) {
        SS_SetDigit(0, tensHours);
    } else {
        SS_SetDigit(0,0x10);
    }
    SS_SetDigit(1, hours - (hours / 10 * 10));
}

void ShowMinute(byte minutes) {
    SS_SetDigit(2, minutes / 10);
    SS_SetDigit(3, minutes - (minutes / 10 * 10));
}

/* Show "AC" (Alarm Clock) in the minutes position */
void BlankMinuteForAlarm() {
    SS_SetDigit(2, 0x0a);
    SS_SetDigit(3, 0x0c);    
}

/* Show "AC" (Alarm Clock) in the hours position */
void BlankHourForAlarm() {
    SS_SetDigit(0, 0x0a);
    SS_SetDigit(1, 0x0c);    
}

void BlankMinuteForTime() {
    SS_SetDigit(2, 0x0c);
    SS_SetDigit(3, 0x0c);    
}

void BlankHourForTime() {
    SS_SetDigit(0, 0x0c);
    SS_SetDigit(1, 0x0c);    
}

void DisplayWarnForCO() {

    Cust_SS_WriteChars(CHAR_SPACE, CHAR_C, CHAR_O, CHAR_SPACE);

    SS_SetDigit(0,0x10);
    SS_SetDigit(1, 0x0c);
    SS_SetDigit(2, 0x00);    
    SS_SetDigit(3,0x10);
    ColonOff();
}

void BlankDisplay() {
    Cust_SS_WriteChars(CHAR_SPACE,CHAR_SPACE,CHAR_SPACE,CHAR_SPACE);

    SS_SetDigit(0,0x10);
    SS_SetDigit(1,0x10);
    SS_SetDigit(2,0x10);
    SS_SetDigit(3,0x10);
}


void initInturrupts() {
    // Clear the BUTTON_STATE_PIN, BUTTON_ACTION_PIN, SWITCH_PIN pin (sets them to input)
//    DDRB &= ~((1 << DDB0) | (1 << DDB1) | (1 << DDB2)); 
    /*  BUTTON_STATE_PIN,BUTTON_ACTION_PIN,SWITCH_PIN (PCINT0, PCINT1, PCINT2 
        pin) are now inputs */
                                                        
    /* turn On the Pull-up */
    PORTB |= ((1 << PORTB0) | (1 << PORTB1) | (1 << PORTB2)); 
    /*  BUTTON_STATE_PIN, BUTTON_ACTION_PIN and SWITCH_PIN are now inputs with 
        pull-up enabled */

    /* Pin Change Interrupt Control Register */
    PCICR |= (1 << PCIE0);     // set PCIE0 to enable PCMSK0 scan
    /* Pin Change Mask Register 0 */
    /* set PCINT0 to trigger an interrupt on state change */
    PCMSK0 |= (1 << PCINT0) | (1 << PCINT1) | (1 << PCINT2);   
}

// ---------------------------------------------------------------------------
/* Initialize the timer, this timer is for the photoresistive sensor */
void initTimer0(void) {
    DDRD |= (1 << DDD6);                    // PD6 is now an output           
    OCR0A = 0;                              // set this for PWM duty cycle    
    TCCR0A |= (1 << COM0A1);                // set none-inverting mode        
    TCCR0A |= (1 << WGM01) | (1 << WGM00);  // set fast PWM Mode              
    TCCR0B |= (1 << CS01);                  // set prescaler to 8, starts PWM 
    TIMSK0 |= (1 << TOIE0);                 // overflow interrupt enable      
}

/* Real time clock */
void initTimer1(void) {                     // this is the real time clock
//  OCR1A = 0x3D08;                         // 15,624 - when we hit this # 1 sec has elapsed @16mhz
    OCR1A = 0x1E84;                         // 7,812 - when we hit this # 1 sec has elapsed @ 8mhz
    TCCR1B |= (1 << WGM12);                 // Mode 4, CTC on OCR1A
    TIMSK1 |= (1 << OCIE1A);                // Set interrupt on compare match
    TCCR1B |= (1 << CS12) | (1 << CS10);    // set prescaler to 1024 and start the timer
}

/* CO sensor */
void initTimer2(void) {
//  output pin for timer2 is OC2A which is PB3
//  DDRB |= (1 << DDB3);
    OCR2A = 72;                             // set PWM for 1.4v
    TCCR2A |= (1 << COM2A1);                // set none-inverting mode
    TCCR2A |= (1 << WGM21) | (1 << WGM20);  // set fast PWM Mode
    TCCR2B |= (1 << CS21);                  // set prescaler to 8 and starts PWM
                                            // set bits 0, 1, 2 to 0 to disable counter
    TIMSK2 |= (1 << TOIE2);                 // overflow interrupt enable      
}

// INTURRUPTS

/* Timer0 overflow interrupt */
ISR(TIMER0_OVF_vect) {
    count++;
    if (count == 250) {
        float photoVolts =  256 - (float) ReadADC(PHOTO_PIN)/4;
        //debug("photoVolts");
        //debugInt(photoVolts);
        OCR0A = photoVolts;
        count=0;
    }
}

/* Timer1 interrupt */
ISR (TIMER1_COMPA_vect)                 // this is the real time clock
{
//  debug("ISR (TIMER1_COMPA_vect)");
    if (g_state.rtcRead) {
        g_state.seconds += 1;
        g_state.timer1ElapsedSecs += 1;
//      debug("g_state.seconds:");
//      debugInt(g_state.seconds);
        if (g_state.seconds == 60) {
            g_state.seconds = 0;
            g_state.minutes += 1;
            if (g_state.minutes == 60) {
                g_state.minutes = 0;
                g_state.hours += 1;
                if (g_state.hours == 24) {
                    g_state.hours = 0;
                }
            }
            g_state.event = EVENT_TIME_CHANGED; 
            ProcessEvent(g_state.event); // not sure why this is necessary but without it RTC doesn't work
        }
    }
}


/* Timer2 overflow interrupt */
ISR (TIMER2_OVF_vect) {                             // PMW for the CO sensor
//  debug("TIMER2_OVF_vect");
    if((g_state.timer1ElapsedSecs >= 90) && (OCR2A == 72)) {
        g_state.timer1ElapsedSecs = 0;
        debug("changed OCR2A to 255");
        OCR2A = 255;                                // voltage = 5.0
        /* todo: make the CO reading the average of the last 10 readings */
        g_state.COreading = ReadADC(CO_READ_PIN);   // read the CO sensor
        debug("CO reading:");
        debugInt(g_state.COreading);
    } else if ((g_state.timer1ElapsedSecs >= 60) && (OCR2A == 255)) {
        g_state.timer1ElapsedSecs = 0;
        debug("changed OCR2A to 72");
        OCR2A = 72;                                 // voltage = 1.4
    }        
}

/* Interrupts for buttons and switches */
ISR (PCINT0_vect)
{
    debug("ISR(PCINT0_vect)");
    byte changedbits;

    changedbits = PINB ^ portbhistory;
    portbhistory = PINB;
    
    char myString[4];
    itoa(changedbits,myString,16);
    debug(myString);

    if(changedbits & (1 << BUTTON_ACTION_PIN))
    {
        if( (portbhistory & (1 << BUTTON_ACTION_PIN)) == 2 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PINB, BUTTON_ACTION_PIN)) {
                /* LOW to HIGH pin change (action button released)*/
                FlashLED();
                // debug("PCINT0_vect, PINB1 set, low to high");
                ProcessEvent(EVENT_PRESS_ACTION);
            }
        }
        else
        {
            /* HIGH to LOW pin change (action button pressed) */
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PINB, BUTTON_ACTION_PIN)) {            /* still pressed */
                FlashLED();
                // debug("PCINT0_vect, PINB1 set, high to low");
            }
        }
    }

    if(changedbits & (1 << BUTTON_STATE_PIN))
    {
    /* PCINT0 changed */
        if( (portbhistory & (1 << BUTTON_STATE_PIN)) == 1 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PINB, BUTTON_STATE_PIN)) {
                /* LOW to HIGH pin change (state button released) */
                FlashLED();
                // debug("PCINT0_vect, PINB0 set, low to high");
                ProcessEvent(EVENT_PRESS_STATE);
            }
        }
        else
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PINB, BUTTON_STATE_PIN)) {            /* still pressed */
                /* HIGH to LOW pin change (state button pressed) */
                FlashLED();
                // debug("PCINT0_vect, PINB0 set, high to low");
            }
        }
    }

    if(changedbits & (1 << SWITCH_PIN))
    {
    /* PCINT0 changed */
        if( (portbhistory & (1 << SWITCH_PIN)) == 4 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PINB, SWITCH_PIN)) {
                /* LOW to HIGH pin change (alarm switch right) */
                FlashLED();
                // debug("PCINT0_vect, PINB2 set, low to high");
                ProcessEvent(EVENT_SWITCH_OFF);
            }
        }
        else
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PINB, SWITCH_PIN)) {            /* still pressed */
                /* HIGH to LOW pin change (alarm switch left) */
                FlashLED();
                // debug("PCINT0_vect, PINB2 set, high to low");
                ProcessEvent(EVENT_SWITCH_ON);
            }
        }
    }
}


// Initialize analog to digital conversion
void InitADC0() {
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // ADC clock prescaler/128   
    ADMUX |= (1 << REFS0);                                // reference voltage on AVCC 
    ADMUX &= ~(1 << REFS1);                               //Avcc(+5v) as voltage reference
    //ADMUX |= (1 << REFS0);                              // internal 
    //ADMUX |= (1 << REFS1);                              //internal
    ADCSRA |= (1 << ADEN);                                // enable ADC                
}

double GetInternalTemp(void) // NOT CALLED ANYWHERE
{
    ADMUX = (3 << REFS0) | (8 << MUX0); // 1.1V REF, channel#8 is temperature
    ADCSRA |= (1 << ADEN) | (6 << ADPS0);       // enable the ADC div64
    msDelay(20);                  // wait for voltages to become stable.
    ADCSRA |= (1 << ADSC);      // Start the ADC

    while (ADCSRA & (1 << ADSC));       // Detect end-of-conversion
    // The offset of 324.31 could be wrong. It is just an indication.
    // The returned temperature is in degrees Celcius.
    return (ADCW - 324.31) / 1.22;
}

// ---------------------------------------------------------------------------
// MAIN PROGRAM

void WriteAlarmTime(byte hours, byte minutes) {
    debug("WriteAlarmTime()");
    doubleByte alarmTimeWrite;
    alarmTimeWrite = (hours * 100) + minutes;
    debug("alarmTimeWrite:");
    debugInt((int) alarmTimeWrite);
    eeprom_update_word((doubleByte *) ALARM_ADDRESS, alarmTimeWrite);

    debugAlarmTime();
}

/* Determine if the alarm switch is on or off and process the appropriate event
*/
void InitAlarmSwitch() { 
    if (bit_is_set(PINB, SWITCH_PIN)) {             // if switch is OFF
        //debug("****************SWITCH IS OFF***********************");
        ProcessEvent(EVENT_SWITCH_OFF);
    }
    if (bit_is_clear(PINB, SWITCH_PIN)) {           // if switch is ON           
        ProcessEvent(EVENT_SWITCH_ON);
    }
}

void ReadAlarmTime(byte *hours, byte *minutes) {
    debug("ReadAlarmTime()");
    doubleByte alarmTimeInt;
    alarmTimeInt = eeprom_read_word((doubleByte *) ALARM_ADDRESS);
    *hours = alarmTimeInt / 100;
    *minutes = alarmTimeInt - ((alarmTimeInt / 100) * 100);

    /*  Wake hours and minutes are corrupt the first time they are read
        from memory, clean that up. */
    if ((*hours < 0) || (*hours > 12)) {
        *hours = 0;
    }
    if ((*minutes < 0) || (*minutes > 59)) {
        *minutes = 0;
    }

    debugNoLF("Read alarm time: ");
    debugAlarmTime();
}   

void debugAlarmTime() {
    char myHours[4];
    itoa(g_state.alarmHours,myHours,10);
    //debugNoLF("Alarm Hours: ");
    //debug(myHours);

    char myMinutes[4];
    itoa(g_state.alarmMinutes,myMinutes,10);
    //debugNoLF("Alarm Minutes: ");
    //debug(myMinutes);
}

void debugTime() {
    char myHours[4];
    itoa(g_state.hours,myHours,10);
    debugNoLF("Hours: ");
    debug(myHours);
    debug("");

    char myMinutes[4];
    itoa(g_state.minutes,myMinutes,10);
    debugNoLF("Minutes: ");
    debug(myMinutes);
    debug("");
}

void SoundAlarm() { //todo: make this sound in some pattern
                    //todo: make this stop after a minute (configurable time)
    debug("SoundAlarm()");
    SetBitNo(PORTB,BUZZER_PIN);
}

void SilenceAlarm() {
    debug("SilenceAlarm()");
    ClearBitNo(PORTB,BUZZER_PIN);
}

void SnoozeAlarm() {
    debug("SnoozeAlarm()");
    SilenceAlarm();
    g_state.alarmMinutes += SNOOZE_TIME;
    if (g_state.alarmMinutes > 59) {
        g_state.alarmMinutes = g_state.alarmMinutes - 60;
        g_state.alarmHours += 1;
    }
}

int WasWakeAlarmTriggered() { // TODO: pass in the structure by value
    //debug("WasWakeAlarmTriggered()");
    if (!((g_state.mode & BUZZING) == BUZZING)) {
        if ((g_state.hours == g_state.alarmHours) &&
            (g_state.minutes == g_state.alarmMinutes) &&
            (g_state.seconds >= 0 && g_state.seconds <= 2)) {
                debug("Wake alarm triggered");
                return 1;
        }
    }
    return 0;
}

void GetTime(byte *inHours, byte *inMinutes, byte *inSeconds) {
    //debug("GetTime()");
    // if we have just powered on get the time from the RTC
    if(!g_state.rtcRead) {
        byte minutesRTC;
        byte hoursRTC;
        byte secondsRTC;
        DS1307_GetTime(&hoursRTC,&minutesRTC,&secondsRTC);
        *inHours =   (hoursRTC >> 4) * 10 + (hoursRTC & 0x0F);
        *inMinutes = (minutesRTC >> 4) * 10 + (minutesRTC & 0x0F);
        *inSeconds = (secondsRTC >> 4) * 10 + (secondsRTC & 0x0F);
        g_state.rtcRead = 1;    // all subsequent setting of time is accomplished
                                // via the Timer1 RTC.
        debugTime();
    } else {
        *inHours = g_state.hours;       // hours, mins and secs updated by Timer1
        *inMinutes = g_state.minutes;
        *inSeconds = g_state.seconds;
    }
}

void ShowTemp(int tempF) {
    debug("ShowTemp()");
    
    if (tempF > 120 || tempF < 0) {
        Cust_SS_WriteChars(CHAR_E,CHAR_R,CHAR_R,CHAR_SPACE);
    } else {
        byte ones = tempF - (tempF / 10) * 10;
        byte tens = tempF / 10;
        byte hundreds = tempF / 100;
        BlankDisplay();
        if (hundreds > 0) {
            SS_SetDigit(0,hundreds);
            hundreds = ssCharMap[hundreds];
        } else {
            hundreds = CHAR_SPACE;
        }
        if (tens > 0) {
            SS_SetDigit(1,tens);
            tens = ssCharMap[tens];
        } else {
            tens = CHAR_SPACE;
        }
        SS_SetDigit(2,ones);
        Cust_SS_WriteChars(hundreds,tens,ssCharMap[ones],CHAR_F);
        SS_SetDigit(3,0x0f);
        ColonOff();
    }
    debug("Temperature:");
    debugInt(tempF);
}

signed int ConvertToFarenheit(unsigned short celcius) {
    // Check for negative temperature 
    if (celcius > 127) { 
        celcius = ~celcius + 1; 
    } 

    signed int farenheit = 32.0 + (celcius * 9.0/5.0);
     
    debugNoLF("Temperature in farenheit: ");
    debugInt(farenheit);
    
    return farenheit;
}

int ReadADC(int channel) {
    //debug("ReadADC()");
    ADMUX &= 0xF0;                              // Clear the older channel that was read
    ADMUX |= channel;                           // Defines the new ADC channel to be read
                                                /* first sample after you've changed the 
                                                   multiplexer will still be from the old 
                                                   analog source, you need to wait at 
                                                   least one complete ADC cycle before 
                                                   getting a value from the new channel */
    ADCSRA |= (1 << ADSC);                      // start ADC conversion
    loop_until_bit_is_clear(ADCSRA, ADSC);      // wait until done
    ADCSRA |= (1 << ADSC);                      // start ADC conversion
    loop_until_bit_is_clear(ADCSRA, ADSC);      // wait until done
    int adcValue = ADCW;                        // read ADC in
    return(adcValue);
}

int WasCOGasAlarmTriggered() {
    //debug("WasCOGasAlarmTriggered()");
    // if the CO sensor is powered up
    if (bit_is_set(PINB, SWITCH_PIN)) {         //  if (g_state.mode & WAKE) {
        int co_reading = g_state.COreading;     //  int co_reading = ReadADC(CO_READ_PIN);
        //debug("co_reading");
        //debugInt(co_reading);
        if (co_reading > CO_ALARM_THRESHOLD) {
            return(1);
        }
   }
   return(0);     
}     

byte CheckForEvents() {
    // Check if the CO gas event was triggered.
    if (WasCOGasAlarmTriggered()) {     // CO alarm has priority over wake alarm
        return(EVENT_CO_DETECTED);
    } else if (WasWakeAlarmTriggered()) {
    // This needs to be called about once a second so that we don't skip
    // over the second(s) that trigger the wake alarm.
        return(EVENT_WAKE_TRIGGERED);
            // Check if the smoke event was triggered.
    //if (WasSmokeAlarmTriggered()) {
    //    g_state.event = EVENT_SMOKE_DETECTED;
    //}
    //if (WasMotionAlarmTriggered()) {              // Check if the motion event was triggered.
    //    g_state.event = EVENT_MOTION_DETECTED;
    //
    }
    return(g_state.event);
}

void Initialize() {

    initUSART();

    debug("Initialize()");

    debug("Initializing AVR");
    InitAVR();                                  // set port direction

    debug("Done initializing AVR.  Send one short beep.");
    FlashLED();
    OneShortBeep();

    debug("Done with short beep.  Initializing I2C.");
    FlashLED();
    I2C_Init();                                 // set I2C clock frequency

    debug("Show all I2C devices.");
    I2C_FindDevice (0x00);

    debug("Done initializing I2C.  Initialize HT16K33 LED controller.");
    FlashLED();
    SS_Init();                                  // initialize HT16K33 LED controller

    debug("Done initializing LED controller. Initialize ADC.");
    FlashLED();
    InitADC0();

    FlashLED();
 
    debug("Done initializing ADC. Initialize timers.");
    FlashLED();
    initTimer0();
    initTimer1();
    initTimer2();

    debug("Done initializing Timer. Initialize inturrupts.");
    FlashLED();
    initInturrupts();

    debug("Done initializing inturrupts.  Initialize RTC.");
    FlashLED();
    InitRTCCHBit();
    g_state.rtcRead = 0;

    int badRead = 1;
    int i = 0;
    while ((badRead) && (i++ <= 3)) {
        GetTime(    &g_state.hours, 
                    &g_state.minutes, 
                    &g_state.seconds);
        badRead = 0;
        if (g_state.hours > 23 || g_state.hours < 0) {
            g_state.hours = 0;
            g_state.minutes = 0;
            g_state.seconds = 0;
            badRead = 1;
        }
        if (g_state.minutes > 59 || g_state.minutes < 0) {
            g_state.hours = 0;
            g_state.minutes = 0;
            g_state.seconds = 0;
            badRead = 1;
        }
        if (g_state.seconds > 59 || g_state.seconds < 0) {
            g_state.hours = 0;
            g_state.minutes = 0;
            g_state.seconds = 0;
            badRead = 1;
        }
        
        if ((g_state.seconds == 0) &&
            (g_state.minutes == 0) &&
            (g_state.seconds == 0)) {
            badRead = 1;
            msDelay(1000);
        }
        debug("Attempt #:");
        debugInt(i);
        debug("");
    }
        
    FlashLED();
 
    debug("Done initializing the time.  Initialize the wake alarm.");
    FlashLED();
    g_state.alarmHours = 0;
    g_state.alarmMinutes = 0;                       //(no seconds for alarm)
    
    ReadAlarmTime(  &g_state.alarmHours, 
                    &g_state.alarmMinutes);         // Read the wake time from chip memory 
    InitAlarmSwitch();
    FlashLED();
    
    g_state.mode |= SHOWING_TIME;                   // set up to show the time immediatly
    g_state.event = EVENT_TIME_CHANGED;
    
    debug("Finished initialization.\r\n");
    FlashLED();
    sei();                                          // enable interrupts
}

int main(void) {
    Initialize();
    debug("main()");
    byte eventToProcess = g_state.event;
    while(1) {
        ProcessEvent(eventToProcess);
        if (g_state.event == eventToProcess) {      // did a new event happen 
                                                    // as we processed this event?
            g_state.event = EVENT_NONE;             // clear out the event just processed
            FlashLED();                             // heartbeat
            msDelay(1000);                          // one second between updates  
        }
        g_state.event = CheckForEvents();
        eventToProcess = g_state.event;
    }
}

void ProcessEvent(byte event)
{
    //debug("ProcessEvent()");
    //debug("event:");
    //debugInt(event);
    //debug("g_state.mode:");
    //debugInt(g_state.mode);
    if (event == EVENT_NONE) {
        if (g_state.mode & SHOWING_TIME) {
            ToggleColon();
        }
        return;
    } else if (event == EVENT_TIME_CHANGED) {           // set by Timer1 RTC
        if (g_state.mode & SHOWING_TIME) {
            ShowTime(g_state.hours, g_state.minutes);
        }
    /*  if the STATE (left) button was pressed */
    } else if (event == EVENT_PRESS_STATE) {
        /*  If the clock is showing time and is NOT sounding the alarm, put it 
            into set wake hour mode */
        if ( (g_state.mode & SHOWING_TIME) &&           // if showing time and not buzzing
            !(g_state.mode & BUZZING) ) {
            ClearBit(g_state.mode, SHOWING_TIME);       // Turn off showing time
            SetBit(g_state.mode, SETTING_WAKE_HOUR);    // Turn on setting wake hour
            Cust_SS_WriteTimeAlarmSetHours(g_state.alarmHours);
            ShowHour(g_state.alarmHours);
            BlankMinuteForAlarm(); 
        /*  If the clock is setting the wake hour, put it into setting wake 
            minute mode */
        } else if (g_state.mode & SETTING_WAKE_HOUR) {
            ClearBit(g_state.mode, SETTING_WAKE_HOUR);
            SetBit(g_state.mode, SETTING_WAKE_MINUTE);
            Cust_SS_WriteTimeAlarmSetMinutes(g_state.alarmMinutes);
            ShowMinute(g_state.alarmMinutes);
            BlankHourForAlarm();
        /*  If clock is setting wake minute set the mode to setting time hour */
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {
            ClearBit(g_state.mode, SETTING_WAKE_MINUTE);// Turn off setting wake minute
            SetBit(g_state.mode, SETTING_TIME_HOUR);    // Turn on setting time hour
            WriteAlarmTime(g_state.alarmHours, g_state.alarmMinutes);  // save wake time
            g_state.setTimeHours = g_state.hours;
            Cust_SS_WriteTimeSetHours(g_state.setTimeHours);
            ShowHour(g_state.setTimeHours);
            BlankMinuteForTime();
        /* If clock is setting time hour set the mode to setting time minute */
        } else if (g_state.mode & SETTING_TIME_HOUR) {
            ClearBit(g_state.mode, SETTING_TIME_HOUR);  // Turn off setting time hour
            SetBit(g_state.mode, SETTING_TIME_MINUTE);  // Turn on setting time minute
            g_state.setTimeMinutes = g_state.minutes;
            Cust_SS_WriteTimeSetMinutes(g_state.setTimeMinutes);
            ShowMinute(g_state.setTimeMinutes);
            BlankHourForTime();
        /* If the clock is setting time minute set the mode to showing time */
        } else if (g_state.mode & SETTING_TIME_MINUTE) {
            ClearBit(g_state.mode, SETTING_TIME_MINUTE);// stop showing minute
            SetBit(g_state.mode, SHOWING_TIME);         // start showing the time
            WriteTime(g_state.setTimeHours, g_state.setTimeMinutes);  // save the new time to the RTC
            g_state.hours = g_state.setTimeHours;
            g_state.minutes = g_state.setTimeMinutes;
            g_state.setTimeHours = 0;
            g_state.setTimeMinutes = 0;
            ShowTime(g_state.hours, g_state.minutes);
        /* if showing the temperature set the mode to show the time */
        } else if (g_state.mode & SHOWING_TEMP) {
            ClearBit(g_state.mode, SHOWING_TEMP);       // turn off showing temp
            SetBit(g_state.mode, SHOWING_TIME);         // go back to showing time
            ShowTime(g_state.hours, g_state.minutes);
        /* if showing CO alert set mode to show time and silence sounding alarm */
        } else if (g_state.mode & SHOWING_CO){
            ClearBit(g_state.mode, SHOWING_CO);
            SetBit(g_state.mode, SHOWING_TIME);
            g_state.COreading = 0;                      // so that the alarm doesn't keep sounding 
                                                        // after the button is pressed
            //SilenceAlarm();							// silence
            ShowTime(g_state.hours, g_state.minutes);	// shows time as of CO alarm trigger
		}
        /*  No matter which mode the clock is in, if the state button is pressed 
            silence the alarm if it's sounding */
        if (g_state.mode & BUZZING) {
            ClearBit(g_state.mode, BUZZING);            // remove BUZZING from the mode.
            SilenceAlarm();
        }
    /* if the ACTION (right) button was pressed */
    } else if (event == EVENT_PRESS_ACTION) {
        /* if time is showing and it's not buzzing, show the temperature */
        if ((g_state.mode & SHOWING_TIME) && 
            !(g_state.mode & BUZZING)) {
            debug("Show the temp");
            ClearBit(g_state.mode, SHOWING_TIME);       // no longer showing time
            SetBit(g_state.mode, SHOWING_TEMP);         // show the temperature
            signed int farenheit = ConvertToFarenheit(I2C_ReadRegister(TC74_ADDRESS_R, TC74_TEMP_REGISTER));
            debugNoLF("farenheit in ProcessEvent: ");
            debugInt(farenheit);
            ShowTemp(farenheit);
        }
        if (g_state.mode & SETTING_WAKE_HOUR) {             // if setting wake hour, increment the hour
            g_state.alarmHours += 1;
            // debugInt(g_state.alarmHours);
            if (g_state.alarmHours > 23) {
                g_state.alarmHours = 0;
            }
            Cust_SS_WriteTimeAlarmSetHours(g_state.alarmHours);
            ShowHour(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {    // else if setting wake minute, increment minute 
            g_state.alarmMinutes += 1;
            if (g_state.alarmMinutes > 59) {
                g_state.alarmMinutes = 0;
            }
            Cust_SS_WriteTimeAlarmSetMinutes(g_state.alarmMinutes);
            ShowMinute(g_state.alarmMinutes);
        } else if (g_state.mode & SETTING_TIME_HOUR) {      // if setting time hour, increment hour
            g_state.setTimeHours += 1;            
            if (g_state.setTimeHours > 23) {
                g_state.setTimeHours = 0;
            }
            Cust_SS_WriteTimeSetHours(g_state.setTimeHours);
            ShowHour(g_state.setTimeHours);
        } else if (g_state.mode & SETTING_TIME_MINUTE) {    // if setting time minute, increment minute
            g_state.setTimeMinutes += 1;
            if (g_state.setTimeMinutes > 59) {
                g_state.setTimeMinutes = 0;
            }
            Cust_SS_WriteTimeSetMinutes(g_state.setTimeMinutes);
            ShowMinute(g_state.setTimeMinutes);
        } else if (g_state.mode & SHOWING_CO) {
            ShowTime(g_state.hours, g_state.minutes);   // shows time as of CO alarm trigger
            msDelay(2000);                              // wait for two seconds
            DisplayWarnForCO();                         // back to showing CO  
        } else if (g_state.mode & BUZZING) {
        /* If the alarm is sounding, change the clock mode to snoozing */
            ClearBit(g_state.mode, BUZZING);			// remove BUZZING from the mode
            SetBit(g_state.mode, SNOOZING); 			// add snoozing
            SnoozeAlarm(&g_state);
        }
    /*  if it's time to sound the alarm to wake up; sound the alarm if the switch
        is on  */
    } else if (event == EVENT_WAKE_TRIGGERED) {
        if (g_state.mode & WAKE) {          			// if the wake alarm is enabled
            SetBit(g_state.mode, BUZZING);  			// set the mode to buzzer on
            SoundAlarm();                   			// sound the alarm
        }
    } else if (event == EVENT_CO_DETECTED) {
        if (g_state.mode & WAKE) {          			// if the wake alarm is enabled
            SetBit(g_state.mode, BUZZING);
            ClearBit(g_state.mode, SHOWING_TIME);
            SetBit(g_state.mode, SHOWING_CO);
            SoundAlarm();
            DisplayWarnForCO(); // todo: make this flash
        }
    } else if (event == EVENT_SWITCH_OFF) {
        // TODO: add code here so that the CO sensor is only on when the switch is on,
        // need to use another pin to feed the base of the transistor to flip the switch
        ClearBit(g_state.mode, WAKE);       			 // set the alarm so it will not sound
		g_state.decimalBlink = 0;                        // turn off the blinking decimal
        if (g_state.mode & BUZZING) {       			 // if the alarm is currently sounding
            ClearBit(g_state.mode, BUZZING);			 // remove BUZZING from the mode
            SilenceAlarm();                 			 // turn of the sounding alarm
        }
        ColonOff();                                      // turn off the colon
        //ClearBitNo(PORTD, CO_POWER_PIN);               // turn off CO sensor, save power todo: write CO OFF to SS
        TCCR2B &= ~((1 << CS20)|(1 << CS21)|(1 << CS22));// Turn off PWM for the CO sensor
   } else if (event == EVENT_SWITCH_ON) {
        SetBit(g_state.mode, WAKE);         			 // turn on the alarm so it will 
														 // sound when necessay
		g_state.decimalBlink = 1;                        // turn on the blinking decimal
        ColonOn();                                       // turn on the colon
	    ////SetBitNo(PORTD, CO_POWER_PIN);
	    //TCCR2B |= (1 << CS21);                         // Turn on PWM for the CO sensor
	    initTimer2();
   }
}
