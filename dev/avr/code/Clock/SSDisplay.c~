//-----------------------------------------------------------------------------
// ss01: Experiments with interfacing ATmega328 to an Seven-Segment display
//
// Author : Bruce E. Hall <bhall66@gmail.com>
// Website : w8bh.net
// Version : 1.0
// Date : 10 Sep 2013
// Target : ATTmega328P microcontroller
// Language : C, using AVR studio 6
// Size : 1338 bytes, using -O1 optimization
//
// Fuse settings: 8 MHz osc with 65 ms Delay, SPI enable; *NO* clock/8

//  ---------------------------------------------------------------------------
//  GLOBAL DEFINES
#define F_CPU 16000000L                 // run CPU at 16 MHz
//#define F_CPU 1000000L 
#define LED 5                           // Boarduino LED on PB5
#define ClearBit(x,y) x &= ~_BV(y)      // equivalent to cbi(x,y)
#define SetBit(x,y) x |= _BV(y)         // equivalent to sbi(x,y)

//  ---------------------------------------------------------------------------
//  INCLUDES
#include <avr/io.h>                     // deal with port registers
#include <util/delay.h>                 // used for _delay_ms function
#include <string.h>                     // string manipulation routines
#include <stdlib.h>

//  ---------------------------------------------------------------------------
//  TYPEDEFS
typedef uint8_t byte;                   // I just like byte & sbyte better
typedef int8_t sbyte;

//  ---------------------------------------------------------------------------
//  MISC ROUTINES
void InitAVR()
{
    DDRB = 0x3F;                        // 0011.1111; set B0-B5 as outputs
    DDRC = 0x00;                        // 0000.0000; set PORTC as inputs
}

void msDelay(int delay)                 // put into a routine
{                                       // to remove code inlining
    for (int i=0;i<delay;i++)           // at cost of timing accuracy
    _delay_ms(1);
}

void FlashLED()
{
    SetBit(PORTB,LED);
    msDelay(250);
    ClearBit(PORTB,LED);
    msDelay(250);
}

// ---------------------------------------------------------------------------
// I2C (TWI) ROUTINES
//
// On the AVRmega series, PA4 is the data line (SDA) and PA5 is the clock (SCL
// The standard clock rate is 100 KHz, and set by I2C_Init. It depends on the AVR osc. freq.
#define F_SCL 100000L                   // I2C clock speed 100 KHz
#define READ 1                          
#define TW_START 0xA4                   // send start condition (TWINT,TWSTA,TWEN)
#define TW_STOP 0x94                    // send stop condition (TWINT,TWSTO,TWEN)
#define TW_ACK 0xC4                     // return ACK to slave
#define TW_NACK 0x84                    // don't return ACK to slave
#define TW_SEND 0x84                    // send data (TWINT,TWEN)
#define TW_READY (TWCR & 0x80)          // ready when TWINT returns to logic 1.
#define TW_STATUS (TWSR & 0xF8)         // returns value of status register
#define I2C_Stop() TWCR = TW_STOP       // inline macro for stop condition

void I2C_Init()
// at 16 MHz, the SCL frequency will be 16/(16+2(TWBR)), assuming prescalar of 0.
// so for 100KHz SCL, TWBR = ((F_CPU/F_SCL)-16)/2 = ((16/0.1)-16)/2 = 144/2 = 72.
{
    TWSR = 0;                           // set prescalar to zero
    TWBR = ((F_CPU/F_SCL)-16)/2;        // set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)
// look for device at specified address; return 1=found, 0=not found
{
    TWCR = TW_START;                    // send start condition
    while (!TW_READY);                  // wait
    TWDR = addr;                        // load device's bus address
    TWCR = TW_SEND;                     // and send it
    while (!TW_READY);                  // wait
    return (TW_STATUS==0x18);           // return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)
// returns with address of first device found; 0=not found
{
    for (byte addr=start;addr<0xFF;addr++)  // search all 256 addresses
    {
        if (I2C_Detect(addr))               // I2C detected?
        return addr;                        // leave as soon as one is found
    }
    return 0;                               // none detected, so return 0.
}

void I2C_Start (byte slaveAddr)
{
    I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)                  // sends a data byte to slave
{
    TWDR = data;                            // load data to be sent
    TWCR = TW_SEND;                         // and send it
    while (!TW_READY);                      // wait
    return (TW_STATUS!=0x28);
}

byte I2C_ReadACK ()                         // reads a data byte from slave
{
    TWCR = TW_ACK;                          // ack = will read more data
    while (!TW_READY);                      // wait
    return TWDR;
    //return (TW_STATUS!=0x28);
}

byte I2C_ReadNACK ()                        // reads a data byte from slave
{
    TWCR = TW_NACK;                         // nack = not reading more data
    while (!TW_READY);                      // wait
    return TWDR;
    //return (TW_STATUS!=0x28);
}

void I2C_WriteByte(byte busAddr, byte data)
{
    I2C_Start(busAddr);                     // send bus address
    I2C_Write(data);                        // then send the data byte
    I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)
{
    I2C_Start(busAddr);                     // send bus address
    I2C_Write(deviceRegister);              // first byte = device register address
    I2C_Write(data);                        // second byte = data for device register
    I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister)
{
    byte data = 0;
    I2C_Start(busAddr);                     // send device address
    I2C_Write(deviceRegister);              // set register pointer
    I2C_Start(busAddr+READ);                // restart as a read operation
    data = I2C_ReadNACK();                  // read the register data
    I2C_Stop();                             // stop
    return data;
}

// ---------------------------------------------------------------------------
// DS1307 RTC ROUTINES
#define DS1307              0xD0                         // I2C bus address of DS1307 RTC
#define SECONDS_REGISTER    0x00
#define MINUTES_REGISTER    0x01
#define HOURS_REGISTER      0x02
#define DAYOFWK_REGISTER    0x03
#define DAYS_REGISTER       0x04
#define MONTHS_REGISTER     0x05
#define YEARS_REGISTER      0x06
#define CONTROL_REGISTER    0x07
#define RAM_BEGIN           0x08
#define RAM_END             0x3F
void DS1307_GetTime(byte *hours, byte *minutes, byte *seconds)
// returns hours, minutes, and seconds in BCD format
{
    *hours = I2C_ReadRegister(DS1307,HOURS_REGISTER);
    *minutes = I2C_ReadRegister(DS1307,MINUTES_REGISTER);
    *seconds = I2C_ReadRegister(DS1307,SECONDS_REGISTER);
    if (*hours & 0x40)                      // 12hr mode:
    *hours &= 0x1F;                         // use bottom 5 bits (pm bit = temp & 0x20)
    else *hours &= 0x3F;                    // 24hr mode: use bottom 6 bits
}

void DS1307_GetDate(byte *months, byte *days, byte *years)
// returns months, days, and years in BCD format
{
    *months = I2C_ReadRegister(DS1307,MONTHS_REGISTER);
    *days = I2C_ReadRegister(DS1307,DAYS_REGISTER);
    *years = I2C_ReadRegister(DS1307,YEARS_REGISTER);
}

void SetTimeDate()
// simple, hard-coded way to set the date.
{
    I2C_WriteRegister(DS1307,MONTHS_REGISTER, 0x08);
    I2C_WriteRegister(DS1307,DAYS_REGISTER, 0x31);
    I2C_WriteRegister(DS1307,YEARS_REGISTER, 0x13);
    I2C_WriteRegister(DS1307,HOURS_REGISTER, 0x08+0x40); // add 0x40 for PM
    I2C_WriteRegister(DS1307,MINUTES_REGISTER, 0x51);
    I2C_WriteRegister(DS1307,SECONDS_REGISTER, 0x00);
}

// ---------------------------------------------------------------------------
// 7-SEGMENT BACKPACK (HT16K33) ROUTINES
//
// The HT16K33 driver contains 16 bytes of display memory, mapped to 16 row x 8 column output
// Each column can drive an individual 7-segment display; only 0-4 are used for this device.
// Each row drives a segment of the display; only rows 0-6 are used.
//
//         0        For example, to display the number 7, we need to light up segments
//      -------     0, 1, 2, 3, and 6. This would be binary 0100.1111 or 0x4F.
//     5|     |1
//      |  6  |     Mapping to the display address memory:
//      -------       0x00 Digit 0 (left most digit)
//     4|     |2      0x02 Digit 1
//      |  3  |       0x04 colon ":" on bit1
//      -------       0x06 Digit 2
//                    0x08 Digit 4 (right-most digit)
//
//#define HT16K33             0xE0            // I2C bus address for Ht16K33 backpack
#define HT16K33             0x70            // I2C bus address for Ht16K33 backpack
#define HT16K33_ON          0x21            // turn device oscillator on
#define HT16K33_STANDBY     0x20            // turn device oscillator off
#define HT16K33_DISPLAYON   0x81            // turn on output pins
#define HT16K33_DISPLAYOFF  0x80            // turn off output pins
#define HT16K33_BLINKON     0x85            // blink rate 1 Hz (-2 for 2 Hz)
#define HT16K33_BLINKOFF    0x81            // same as display on
#define HT16K33_DIM         0xE0            // add level (15=max) to byte
static const byte numberTable[] =           // convert number to lit-segments
{
    0x3F, // 0
    0x06, // 1
    0x5B, // 2
    0x4F, // 3
    0x66, // 4
    0x6D, // 5
    0x7D, // 6
    0x07, // 7
    0x7F, // 8
    0x6F, // 9
    0x77, // A
    0x7C, // b
    0x39, // C
    0x5E, // d
    0x79, // E
    0x71, // F
    0x00, //<blank>
};

void SS_Init()
{
    I2C_WriteByte(HT16K33,HT16K33_ON);          // turn on device oscillator
    I2C_WriteByte(HT16K33,HT16K33_DISPLAYON);   // turn on display, no blink
    I2C_WriteByte(HT16K33,HT16K33_DIM + 15);    // set max brightness
}

void SS_SetDigitRaw(byte digit, byte data)      // digits (L-to-R) are 0,1,2,3
// Send segment-data to specified digit (0-3) on LED display
{
    if (digit>4) return;                        // only digits 0-4
    if (digit>1) digit++;                       // skip over colon @ position 2
    digit <<= 1;                                // multiply by 2
    I2C_WriteRegister(HT16K33,digit,data);      // send segment-data to display
}

void SS_BlankDigit(byte digit)
// Blanks out specified digit (0-3) on LED display
{
    SS_SetDigitRaw(digit,0x00);                 // turn off all segments on specified digit
}

void SS_SetDigit(byte digit, byte data)
// display data value (0-F) on specified digit (0-3) of LED display
{
    if (data>0x10) return;                      // only values <=16
    SS_SetDigitRaw(digit,numberTable[data]);    // show value on display
}

void SS_SetColon(byte data)                     // 0=off, 1=on
// the colon is represented by bit1 at address 0x04. There are three other single LED
// "decimal points" on the display, which are at the following bit positions
// bit2=top left, bit3=bottom left, bit4=top right
{
    I2C_WriteRegister(HT16K33,0x04,data<<1);
}

void SS_SetDigits(byte d0, byte d1, byte d2, byte d3, byte colon)
{
    SS_SetDigit(0,d0);
    SS_SetDigit(1,d1);
    SS_SetDigit(2,d2);
    SS_SetDigit(3,d3);
    SS_SetColon(colon);
}

void SS_Integer(int data, byte base)
{
    char st[5]="";
    itoa(data,st,base);                         // convert to string
    byte len = strlen(st);
    if (len>4) return;
    for (byte digit=0; digit<4; digit++)        // for all 4 digits
    {
        byte blanks = 4-len;                    // number of blanks
        if (digit<blanks)                       // right-justify display
        SS_SetDigit(digit,0x10);                // padding with blanks
        else
        {
            char ch = st[digit-blanks];         // get char for this digit
            if (ch>='a') ch-=87;                // correct for hex digits
            else ch-='0';                       // ascii -> numeric value
            SS_SetDigit(digit,ch);              // display digit
        }
    }
}

// ---------------------------------------------------------------------------
// APPLICATION ROUTINES
void SS_IntegerTest()                           // count 0 to 255 on display
{
    for (int i=0;i<256;i++)
    {
        SS_Integer(i,16);                       // choose your base here: 16=hex
        msDelay(150);
    }
}

void SS_BeefTest()                              // displays pulsating 'beeF' message
{
    SS_SetDigits(0x0b,0x0e,0x0e,0x0f,0);        // write 'beeF'
    for (byte count=0; count<3; count++)
    {
        for (byte j=15; j>0; j--)               // gradually dim the display
        {
            I2C_WriteByte(HT16K33,HT16K33_DIM + j);
            msDelay(100);
        }
        for (byte j=0; j<16; j++)               // gradually brighten the display
        {
            I2C_WriteByte(HT16K33,HT16K33_DIM + j);
            msDelay(100);
        }
    }
}

void SS_CircleTest()                            // show rotating circle on each digit
{
    for (byte count=0; count<15; count++)
    {
        for (byte i=0; i<6; i++)                // display each segment in turn
        {
            SS_SetDigitRaw(0,1<<i);
            SS_SetDigitRaw(1,1<<i);
            SS_SetDigitRaw(2,1<<i);
            SS_SetDigitRaw(3,1<<i);
            msDelay(100);
        }
    }
}

void LED_Time()
// display current time on 7-segment LED display from BCD input
{
    byte hours, minutes, seconds;
    DS1307_GetTime(&hours,&minutes,&seconds);
    SS_SetDigits(
    hours >> 4,                                 // 10 hour digit
    hours & 0x0F,                               // 1 hour digit
    minutes >> 4,                               // 10 minute digit
    minutes & 0x0F,                             // 1 minute digit
    1);                                         // turn on colon
}

// ---------------------------------------------------------------------------
// MAIN PROGRAM
void MainLoop()
{
    while(1)
    {
        LED_Time();                             // put time on LED
        FlashLED();
        msDelay(1000);                          // one second between updates
    }
}

int main(void)
{
    InitAVR();                                  // set port direction
    FlashLED();
    I2C_Init();                                 // set I2C clock frequency
    FlashLED();
    SS_Init();                                  // initialize HT16K33 LED controller
    FlashLED();
    SS_BeefTest();                              // pulsating 'beef' message
    FlashLED();
    SS_IntegerTest();                           // count in hexadecimal
    FlashLED();
    SS_CircleTest();                            // circle animation
    FlashLED();
    MainLoop();                                 // display time on LCD & LED
}
