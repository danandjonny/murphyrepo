// this code sets up a timer0 for 4ms @ 16Mhz clock cycle
// an interrupt is triggered each time the interval occurs.

// this code sets up a timer0 for 4ms @ 11Mhz clock cycle
// an interrupt is triggered each time the interval occurs.

#include <avr/io.h>
#include <avr/interrupt.h>

volatile uint8_t count;

/* Initialize the timer */
void initTimer0(void)
{
    OCR0A = 0xFF;                        // TCNT0 is compared to OCR0, when there's a
                                         // match the TOV0 bit is set in TIFR register.
                                         // TOV0 can generate a Timer Overflow interrupt.
   
    TIMSK0 |= (1 << OCIE0A);             // In order to activate the timer0 interrupts
                                         // you need to SET(1) the TOIE0 bit within
                                         // the TIMSK register.
//    or, instead
//    TIMSK0 |= (1 << TOIE0);            // this is probably better since we're
                                         // counting overflow events.                                   
   
   
    TCCR0A |= (1 << WGM01);              // timer/counter control register A
                                         // set for CTC mode; TOP = OCR0A
   
    sei();                               // enable interrupts

    TCCR0B |= (1 << CS00) | (1 << CS01); // set prescaler to 64 (do this last)                   

}


ISR (TIMER0_COMPA_vect)                  // timer0 overflow interrupt
{                                        // blink every 61 times through to
                                         // blink once per second w/ a prescaler of 64
    count += 1;                          // counting overflows
    if (count == 61) {
        count = 0;
        PORTC=~PORTC;                    //Invert the Value of PORTC
    }
}

int main(void)
{
   DDRC |= (1 << PC0);                     /* PC0 is now an output           */
   initTimer0();
   //Infinite loop
   while(1);
   return 1;
}
