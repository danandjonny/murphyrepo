# TODO: Update the remarks, especially at the top of the program
# x      Clean out the ServoBlaster code
# x      Start PIGPIO daemon on startup
# x      Install the voltage meter and test
#       Assemble the turrit
#       Remove the bottom part of the case from the Raspberry Pi
#       Test the pi on battery power
#       Install the new compass and test it
#       Figure out what to do with the 5v regulator breadboards
#       Clean up the UI
#       Add options to the UI:
#       UI: "safe mode", where the robot avoids obsticals even
#           when controlled from the web UI
#           Show bearing and distance
#           Add a "Get Distance" button
#           Add a "Get Bearing" button
#       Figure out how to mount the camera and the sonar
#       Change out the battery box
#       Backup the code
#       Add functions for vertical and horizontal control of servos
#       Clean up the pi account
#       Remove ServoBlaster from robotweb (uninstall it)
#       Defects: Right turn doesn't show speed
#
from time import sleep
import os
import smbus
import math
from rrb3 import *
from Adafruit_LED_Backpack import SevenSegment
import picamera
#from RPIO import PWM
import pigpio

class Robot:
    'common base class for all robots'
    def __init__ (self, slowSpeed, mediumSpeed, fastSpeed, batteryVolts):
	self.log("__init__()")
        # verify that fastSpeed is between 0 and 1,
        # and that fastSpeed >= mediumSpeed >= slowSpeed,
        # and that slowSpeed >= 0.
        if (fastSpeed < 0):
            fastSpeed = 0
        if (fastSpeed > 1):
            fastSpeed = 1
        if (mediumSpeed > fastSpeed):
            mediumSpeed = fastSpeed
        if (slowSpeed > mediumSpeed):
            slowSpeed = mediumSpeed

        # Input voltage for RaspiRobot is 6-12v (9 recommended for driving motors)
        if (batteryVolts < 6):
            batteryVolts = 6
        if (batteryVolts > 12):
            batteryVolts = 12
 
	# Create the raspirobot robot object, motor speed is 6 volts
        self.rr=RRB3(batteryVolts,6)

	# Initialize some variables
        self.fastSpeed = fastSpeed
        self.mediumSpeed = mediumSpeed
        self.slowSpeed = slowSpeed
        self.direction = 0
        self.leftSpeed = 0
        self.leftDirection = 0
        self.rightSpeed = 0
        self.rightDirection = 0
        self.rampUpIncrement = 0.10

	# Turn off the open collecters associated with the servos
        self.rr.set_oc1(0)
        self.rr.set_oc2(0)

	# Turn off the LEDs
        self.rr.set_led1(0)
        self.rr.set_led2(0)

        # Create display instance on default I2C address (0x70) and bus number.
        self.display = SevenSegment.SevenSegment()
        # Initialize the display. Must be called once before using the display.
        self.display.begin()

        # for the compass
        self.bus = smbus.SMBus(1)
        self.address = 0x1e

        self.leftDistance = 0
        self.rightDistance = 0
        self.midDistance = 0

	# Motors off, direction forward
        self.setMotors(0,0,0,0)

        # Initialize the servo
        self.servo1Pin = 17
        self.servo2Pin = 4
        self.servoWait = 0.25
#        servo = PWM.Servo()
        self.pi1 = pigpio.pi()
        self.pi2 = pigpio.pi()

# Follwoing line does not work when Robot called from movement_server.py
#       self.camera = picamera.PiCamera()

    def __del__(self):
	self.log("__del__()")
        self.off()
        
    def log(self,message):
	print message

    def servo1Left(self):
	self.log("servo1Left()")
        self.rr.set_oc1(1)
        self.pi1.set_servo_pulsewidth(self.servo1Pin,1000)
        sleep(self.servoWait)
        self.pi1.set_servo_pulsewidth(self.servo1Pin,0)
        self.rr.set_oc1(0)
        

    def servo1Right(self):
	self.log("servo1Right()")
        self.rr.set_oc1(1)
        self.pi1.set_servo_pulsewidth(self.servo1Pin,2000)
        sleep(self.servoWait)
        self.pi1.set_servo_pulsewidth(self.servo1Pin,0)
        self.rr.set_oc1(0)     

    def servo1Mid(self):
	self.log("servo1Mid()")
        self.rr.set_oc1(1)
        self.pi1.set_servo_pulsewidth(self.servo1Pin,1500)
        sleep(self.servoWait)
        self.pi1.set_servo_pulsewidth(self.servo1Pin,0)
        self.rr.set_oc1(0)     

    def servo1Pan(self):
	self.log("servo1Pan()")
	self.bothLEDsOn()
        self.servo1Left()
        self.leftDistance = self.readDistance()
        self.servo1Right()
        self.rightDistance = self.readDistance()
        self.servo1Mid()
        self.midDistance = self.readDistance()
	self.bothLEDsOff()

    # moves servo 2 to the left-most position
    def servo2Left(self):
	self.log("servo2Left()")
        self.rr.set_oc2(1)
        self.pi2.set_servo_pulsewidth(self.servo2Pin,1000)
        sleep(self.servoWait)
        self.pi2.set_servo_pulsewidth(self.servo2Pin,0)
        self.rr.set_oc2(0)
    
    # moves servo 2 to the right-most position    
    def servo2Right(self):
	self.log("servo2Right()")
        self.rr.set_oc2(1)
        self.pi2.set_servo_pulsewidth(self.servo2Pin,2000)
        sleep(self.servoWait)
        self.pi2.set_servo_pulsewidth(self.servo2Pin,0)
        self.rr.set_oc2(0)     

    # moves servo 2 to the middle position
    def servo2Mid(self):
	self.log("servo2Mid()")
        self.rr.set_oc2(1)
        self.pi2.set_servo_pulsewidth(self.servo2Pin,1500)
        sleep(self.servoWait)
        self.pi2.set_servo_pulsewidth(self.servo2Pin,0)
        self.rr.set_oc2(0)     

    def servo2Pan(self):
	self.log("servo2Pan()")
	self.bothLEDsOn()
        self.servo2Left()
        self.leftDistance = self.readDistance()
        self.servo2Right()
        self.rightDistance = self.readDistance()
        self.servo2Mid()
        self.midDistance = self.readDistance()
	self.bothLEDsOff()

    def getLeftDistance(self):
	self.log("getLeftDistance()")
        return self.leftDistance
    
    def getRightDistance(self):
	self.log("getRigtDistance()")
        return self.rightDistance
    
    def getMidDistance(self):
	self.log("getMidDistance()")
        return self.midDistance

    # Distance to nearest obstical via SR-04 ultrasonic range finder
    def readDistance(self):
	self.log("readDistance()")
        return self.rr.get_distance()

    def leftLEDOn(self):
	self.log("leftLEDOn()")
        self.rr.set_led1(1)

    def leftLEDOff(self):
	self.log("leftLEDOff()")
        self.rr.set_led1(0)

    def rightLEDOn(self):
	self.log("rightLEDOn()")
        self.rr.set_led2(1)

    def rightLEDOff(self):
	self.log("rightLEDOff()")
        self.rr.set_led2(0)

    def bothLEDsOn(self):
	self.log("bothLEDsOn()")
	self.rr.set_led1(1)
	self.rr.set_led2(1)
    
    def bothLEDsOff(self):
	self.log("bothLEDsOff()")
	self.rr.set_led1(0)
	self.rr.set_led2(0)

    def stop(self):
	self.log("stop()")
        print "Slowing..."
        self.changeSpeed(0,self.leftDirection,0,self.rightDirection)
	print "Stopped"

    def getDirection(self):
	self.log("getDirection()")
        return self.direction

    def forward(self, seconds, speed):
	self.log("forward()")
	print "Forward Seconds: " + str(seconds)
        self.changeSpeed(speed, 0, speed, 0)
        sleep(seconds)
        self.stop()

    def reverse(self, seconds, speed):
	self.log("reverse()")
	print "Reverse Seconds: " + str(seconds)
        self.changeSpeed(speed,1,speed,1)
        sleep(seconds)
        self.stop()

    def left(self, seconds, speed):
	self.log("left()")
	self.leftLEDOn()
        self.changeSpeed(speed,0,0,0)
        sleep(seconds)
        self.stop()
	self.leftLEDOff()

    def right(self, seconds, speed):
	self.log("right()")
	self.rightLEDOn()
        self.changeSpeed(0,0,speed,0)
        sleep(seconds)
        self.stop()
	self.rightLEDOff()

    def turnSharpLeft(self):
	self.log("turnSharpLeft()")
	print "turn left"
        self.left(1, self.mediumSpeed)

    def turnSharpRight(self):
	self.log("turnSharpRight()")
	print "turn right"
        self.right(1, self.mediumSpeed)

    # turn everything off
    def off(self):
	self.log("off()")
        self.rr.set_oc1(0)
        self.rr.set_oc2(0)
        self.rr.set_led1(0)
        self.rr.set_led2(0)
        self.setMotors(0,0,0,0)
#        os.system("sudo killall servod")
        self.display.clear()
        self.display.write_display()
	self.bothLEDsOff()

    def takePicture(self, imageName):
	self.log("takePicture()")
	self.bothLEDsOn()
        self.camera.capture(imageName)
	self.bothLEDsOff()

    def startRecording(self, videoName):
	self.log("startRecording()")
	self.bothLEDsOn()
        self.camera.start_recording(videoName)

    def stopRecording(self):
	self.log("stopRecording()")
	self.bothLEDsOff()
        self.camera.stop_recording()
        
#---functions below here are not good for long term use because they thrash the gears---#

    def spinHardRight180(self):
	self.log("spinHardRight180()")
	# set left motor forward, right motor backwards
        self.setMotors(1,1,1,0)
        sleep(0.50)
        self.stop()

    def spinHardLeft180(self):
	self.log("spinHardLeft180()")
        self.setMotors(1,0,1,1)
        sleep(0.50)
        self.stop()

    def spinHardRight360(self):
	self.log("spinHardRight360()")
    # set left motor forward, right motor backwards
        self.setMotors(1,1,1,0)
        sleep(1)
        self.stop()

    def spinHardLeft360(self):
	self.log("spinHardLeft360()")
        self.setMotors(1,0,1,1)
        sleep(1)
        self.stop()

#--------functions above this line are public------------#

    def changeSpeed(self, newLeftSpeed, newLeftDirection, newRightSpeed, newRightDirection):
	self.log("changeSpeed()") 
       	currentLeftSpeed = self.leftSpeed
        currentRightSpeed = self.rightSpeed

        if ((newLeftDirection != self.leftDirection) or (newRightDirection != self.rightDirection)):
            self.changeSpeed(0,self.leftDirection,0,self.rightDirection)

        while ((currentLeftSpeed != newLeftSpeed) or (currentRightSpeed != newRightSpeed)):
            if (currentLeftSpeed < newLeftSpeed):
                currentLeftSpeed += self.rampUpIncrement
                if (currentLeftSpeed > 1):
                    print "Warning: currentLeftSpeed > 1 in changeSpeed"
                    currentLeftSpeed = 1
            if (currentLeftSpeed > newLeftSpeed):
                currentLeftSpeed -= self.rampUpIncrement
                if (currentLeftSpeed < 0):
                    print "Warning: currentLeftSpeed < 0 in changeSpeed"
                    currentLeftSpeed = 0
            if (currentRightSpeed < newRightSpeed):
                currentRightSpeed += self.rampUpIncrement
                if (currentRightSpeed > 1):
                    print "Warning: currentRightSpeed > 1 in changeSpeed"
                    currentRightSpeed = 1
            if (currentRightSpeed > newRightSpeed):
                currentRightSpeed -= self.rampUpIncrement
                if (currentRightSpeed < 0):
                    print "Warning: currentRightSpeed < 0 in changeSpeed"
                    currentRightSpeed = 0

            self.setMotors(currentLeftSpeed,newLeftDirection,currentRightSpeed,newRightDirection)
            sleep(0.10)

    def setMotors(self,leftSpeed, leftDirection, rightSpeed, rightDirection):
	self.log("setMotors()")
        self.bothLEDsOn()
        if (leftSpeed > 1):
            print "Warning: left speed > 1"
            leftSpeed = 1
        if (leftSpeed < 0):
            print "Warning: left speed < 0"
            leftSpeed = 0
        if (rightSpeed > 1):
            print "Warning: right speed > 1"
            rightSpeed = 1
        if (rightSpeed < 0):
            print "Warning: right speed < 0"
            rightSpeed = 0
        self.leftSpeed = leftSpeed
        self.leftDirection = leftDirection
        self.rightSpeed = rightSpeed
        self.rightDirection = rightDirection
        self.speed = self.leftSpeed
        self.direction = self.leftDirection
        if (self.leftSpeed > 0):
            self.displayOnSSLED(self.leftSpeed * 10)
        else:
            self.displayOnSSLED(self.rightSpeed * 10)
        self.rr.set_motors(self.leftSpeed, self.leftDirection, self.rightSpeed, self.rightDirection)
        print "Left Speed: " + str(self.leftSpeed)
        print "Left Direction: " + str(self.leftDirection)
        print "Right Speed: " + str(self.rightSpeed)
        print "Right Direction: " + str(self.rightDirection)
        self.bothLEDsOff()

    def displayOnSSLED(self, i):
	self.log("displayOnSSLED()")
        # Clear the display buffer.
        self.display.clear()
        # Print the number with no decimal digits and left justified.
        self.display.print_float(i, decimal_digits=0, justify_right=True)
        # Set the colon on or off (True/False).
        self.display.set_colon(False)
        # Write the display buffer to the hardware.  This must be called to
        # update the actual display LEDs.
        self.display.write_display()
        print "Speed: " + str(i)

    # Start of Compass Functions
    def read_byte(self, adr):
	self.log("read_byte()")
        return self.bus.read_byte_data(self.address, adr)
    def read_word(self, adr):
	self.log("read_word()")
        high = self.bus.read_byte_data(self.address, adr)
        low = self.bus.read_byte_data(self.address, adr+1)
        val = (high << 8) + low
        return val
    def read_word_2c(self, adr):
	self.log("read_word_2c()")
        val = self.read_word(adr)
        if (val >= 0x8000):
            return -((65535 - val) + 1)
        else:
            return val
    def write_byte(self, adr, value):
	self.log("write_byte()")
        self.bus.write_byte_data(self.address, adr, value)

    def readBearing(self):
	self.log("readBearing()")
        self.write_byte(0, 0b01110000) # Set to 8 samples @ 15Hz
        self.write_byte(1, 0b00100000) # 1.3 gain LSb / Gauss 1090 (default)
        self.write_byte(2, 0b00000000) # Continuous sampling
        scale = 0.92
        x_out = self.read_word_2c(3) * scale
        y_out = self.read_word_2c(7) * scale
        z_out = self.read_word_2c(5) * scale
        bearing  = math.atan2(y_out, x_out) 
        if (bearing < 0):
            bearing += 2 * math.pi
        print "Bearing: ", math.degrees(bearing)
        return math.degrees(bearing)
    # End of Compass Functions

def main():
#    self.log("main()")
    robot = Robot(0.20,0.50,1,9)
    robot.leftLEDOn();
    sleep(1)
    robot.leftLEDOff()
    robot.rightLEDOn()
    sleep(1)
    robot.rightLEDOff()
#    robot.forwardSlow()
#    sleep(1)
#    robot.forwardMedium()
#    sleep(1)
    robot.servo1Left()
    robot.servo1Mid()
    robot.servo1Right()
    robot.servo1Pan()
    robot.servo2Left()
    robot.servo2Mid()
    robot.servo2Right()
    robot.servo2Pan()
#    print "left distance: ", robot.getLeftDistance()
#    print "right distance: ", robot.getRightDistance()
#    print "mid distance: ", robot.getMidDistance()
#    robot.forwardFast()
#    sleep(1)
#    robot.backwardSlow()
#    sleep(1)
#    robot.backwardMedium()
#    sleep(1)
#    robot.backwardFast()
#    sleep(1)
#    robot.turnSharpRight()
#    sleep(1)
#    robot.turnSharpLeft()
#    sleep(1)
#    robot.spinHardRight180()
#    sleep(1)
#    robot.spinHardLeft180()
#    sleep(1)
#    robot.spinHardRight360()
#    sleep(1)
#    robot.spinHardLeft360()
#    sleep(1)
#    direction = robot.getDirection()
#    print "Direction: ", direction
#    bearing = robot.readBearing()
#    print "Bearing: ", bearing
    robot.stop()
#    sleep(1)
#    robot.takePicture("robotPicture.jpg")
    robot.off()
    print "Done"
    
if __name__ == "__main__": main()
