#!/usr/bin/env python

import RPi.GPIO as GPIO
from time import sleep
from time import time
import os
import Pyro.core
import Pyro.naming
from Robot import Robot

class Movement(Pyro.core.ObjBase):

    # Set up the GPIO
    def _setup(self):
	self.robot = Robot(2,5,10,9)

    def __init__(self):
        Pyro.core.ObjBase.__init__(self)
        self._setup()

    def stop(self):        
	self.robot.stop()

    def forward(self, move_time):
	self.robot.forward(move_time,0.5)
        self.robot.stop()

    def backward(self, move_time):
	self.robot.reverse(move_time, 0.5)
	sleep(move_time)
	self.robot.stop()

    def left(self, move_time):
	self.robot.turnSharpLeft()
    
    def right(self, move_time):
	self.robot.turnSharpRight()
   
    def pan(self):
	self.robot.servo2Pan()

    def panLeft(self):
	self.robot.servo2Left()

    def panMid(self):
	self.robot.servo2Mid()

    def panRight(self):
        self.robot.servo2Right()

    def panUp(self):
	self.robot.servo1Left()

    def panAhead(self):
	self.robot.servo1Mid()

    def panDown(self):
	self.robot.servo1Right()

    def startServoBlaster(self):
        self.robot.startServoBlaster()

if __name__ == "__main__":
    # Create a Pyro server and register our module with it
    Pyro.core.initServer()
    ns = Pyro.naming.NameServerLocator().getNS()
    daemon = Pyro.core.Daemon()
    daemon.useNameServer(ns)
    uri = daemon.connect(Movement(),"robotmovement")
    daemon.requestLoop()
