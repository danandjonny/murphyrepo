/*
 * RayGun
 *
 * Created: 9/11/2016 12:56:24 PM
 * Author : djmurphy
 *
 * Baud rate is 1200 at 1MHz for unknown reasons, 9600 @ 8MHz
 *
 */ 
#define F_CPU 8000000UL														// run CPU at 1 MHz

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <stdlib.h>
#include "USART.h"
#include "music.h"

#define ClearBitNo(x,y) x &= ~_BV(y)										// equivalent to cbi(x,y)
#define SetBitNo(x,y) x |= _BV(y)											// equivalent to sbi(x,y)
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))								// get number of elements in a two dimensional array

#define DEBUG_ON				1											// set to 1 to see output on serial terminal.  Adversely effects performance.
#if DEBUG_ON
	#define DEBUG_STRING(x)		debug(x)
	#define	DEBUG_INT(x)		debugInt(x)
	#define DEBUG_METRIC(x,y)	debugMetric(x,y)
#else
	#define DEBUG_STRING(x)		do {} while (0)
	#define	DEBUG_INT(x)		do {} while (0)
	#define DEBUG_METRIC(x,y)	do {} while (0)
#endif

typedef uint8_t byte;
#define	PORT_BUTTONS			PORTB
#define PIN_BUTTONS				PINB
#define DDR_BUTTON				DDRB
#define BUTTON1					DDB1		
#define BUTTON2					DDB2
#define BUTTON3					DDB3
#define BUTTON4					DDB4
#define UNUSED_BUTTON5			DDB5
#define UNUSED_BUTTON0			DDB0
#define PORTBUTTON1				PORTB1
#define PORTBUTTON2				PORTB2
#define PORTBUTTON3				PORTB3
#define PORTBUTTON4				PORTB4

#define SPEAKER                 PB0                            /* OC0A */
#define SPEAKER_PORT            PORTB
#define SPEAKER_PIN             PINB
#define SPEAKER_DDR             DDRB

#define PCINT_BUTTON1			PCINT1
#define PCINT_BUTTON2			PCINT2
#define PCINT_BUTTON3			PCINT3
#define PCINT_BUTTON4			PCINT4
#define PCIE_BUTTON				PBIE1
#define PCMSK_BUTTON			PBMSK1
#define UNUSED_BUTTON			255

#define PORT_HEARTBEAT			PORTB
#define LED_HEARTBEAT			PORTB5
#define DDR_HEARTBEAT			DDRB

#define DDR_PIEZO				DDRB
#define PIEZO_OUT				DDB1
#define PIEZO_PORT				PORTB

#define NUM_BUTTONS				4
#define DEBOUNCE_TIME			2000										// in microseconds

#define RASPBERRY_SIZE			1500
#define TIME_TO_SLEEP			800000										// ~1 minute at 8mHz

#define PLAY_FORWARD			1
#define PLAY_BACKWARD			2

void InitPiezo();
void LoserBuzz();
void WinnerBuzz();
//void PlayNote(float icr);
void PlayNote(int icr, int duration);
void delayms(int milliseconds);
void Bomb(void);
void playUp(void);
void InitADC0(void);
void OldPlayNote(uint16_t period, uint16_t duration);
void StartMillisecondsTimer(void);
void StopMillisecondsTimer(void);
void GoToSleep(void);
void ToggleHeartbeat(void);
void OnHeartbeat(void);
void OffHeartbeat(void);
void Beep(void);
void PlayArray(int noteArray[], int durationArray[], int restArray[], int sizeOfArray, int direction);
void RayGun(void);
void RayGunII(void);

volatile uint32_t milliseconds = 0;
volatile byte buttonhistory = 0b0011110;        							// default is high because of the pull-up, correct setting
volatile byte globalButtonPressed = UNUSED_BUTTON;

volatile uint8_t ADCvalue = 128;											// Global variable, set to volatile if used with ISR

/************************************************************************/
/* Debug Routines                                                       */
/************************************************************************/
void debug(const char myString[]) {
	byte i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
	printString("\r\n");
}

void debugInt(signed int anInt) {
	char myInt[4];
	itoa(anInt,myInt,10);
	debug(myInt);
}

void debugMetric(const char myString[], signed int anInt) {
	byte i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
	char myInt[4];
	itoa(anInt,myInt,10);
	debug(myInt);
}

void delayms(int milliseconds) {
	for (int i = 0; i < milliseconds; i++) {
		_delay_ms(1);
	}
}

void delayus( uint16_t z ) {

	while ( z ) {
		_delay_us( 100 );					// delay 100 microseconds of a second
		z--;
	}
}

/************************************************************************/
/* Analog to digital conversion                                         */
/************************************************************************/
void InitADC0() {
	ADMUX = 0;																// use ADC0
	ADMUX |= (1 << REFS0);													// use AVcc as the reference
	ADMUX |= (1 << ADLAR);													// Right adjust for 8 bit resolution

	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);					// 128 prescale for 16Mhz
	ADCSRA |= (1 << ADATE);													// Set ADC Auto Trigger Enable
	
	ADCSRB = 0;																// 0 for free running mode

	ADCSRA |= (1 << ADEN);													// Enable the ADC
	ADCSRA |= (1 << ADIE);													// Enable Interrupts

	ADCSRA |= (1 << ADSC);													// Start the ADC conversion
}

ISR(ADC_vect)
{
	ADCvalue = ADCH;          // only need to read the high value for 8 bit
	OCR1A = ADCvalue;
	// REMEMBER: once ADCH is read the ADC will update
	
	// if you need the value of ADCH in multiple spots, read it into a register
	// and use the register and not the ADCH
}

/************************************************************************/
/* Interrupts                                                           */
/************************************************************************/
void initInturrupts() {														// set up the pin change interrupts for the buttons 
	PCICR |= (1 << PCIE_BUTTON);											// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
	PCMSK_BUTTON |= (1 << PCINT_BUTTON1) | (1 << PCINT_BUTTON2) | (1 << PCINT_BUTTON3) | (1 << PCINT_BUTTON4);// Pin Change Mask Register 0
}

ISR (PCINT1_vect)	{														// Interrupts for buttons 
	byte changedbits;

	changedbits = PIN_BUTTONS ^ buttonhistory;
	buttonhistory = PIN_BUTTONS;
	if(changedbits & (1 << BUTTON1))
	{
		if( (buttonhistory & (1 << BUTTON1)) == 2 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON1)) {							// LOW to HIGH pin change (button released)
				OffHeartbeat();
				globalButtonPressed = BUTTON1;
				DEBUG_STRING("button 1 release");
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON1)) {						// still pressed
				OnHeartbeat();
				DEBUG_STRING("button 1 press");
			}
		}
	}

	if(changedbits & (1 << BUTTON2))
	{
		if( (buttonhistory & (1 << BUTTON2)) == 4 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON2)) {							// LOW to HIGH pin change (button released)
				OffHeartbeat();
				globalButtonPressed = BUTTON2;
				DEBUG_STRING("button 2 release");
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON2)) {						// still pressed
				OnHeartbeat();
				DEBUG_STRING("button 2 press");
			}
		}
	}
	
	if(changedbits & (1 << BUTTON3))
	{
		if( (buttonhistory & (1 << BUTTON3)) == 8 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON3)) {							// LOW to HIGH pin change (button released)
				OffHeartbeat();
				globalButtonPressed = BUTTON3;
				DEBUG_STRING("button 3 release");
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON3)) {						// still pressed
				OnHeartbeat();
				DEBUG_STRING("button 3 press");
			}
		}
	}
	
	if(changedbits & (1 << BUTTON4))
	{
		if( (buttonhistory & (1 << BUTTON4)) == 16 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_BUTTONS, BUTTON4)) {							// LOW to HIGH pin change (button released)
				OffHeartbeat();
				globalButtonPressed = BUTTON4;
				DEBUG_STRING("button 4 release");
			}
		}
		else
		{																	// HIGH to LOW pin change (button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_BUTTONS, BUTTON4)) {						// still pressed
				OnHeartbeat();
				DEBUG_STRING("button 4 press");
			}
		}
	}
}

/************************************************************************/
/* Timers                                                               */
/************************************************************************/

void InitPiezo(void) {
	TCCR0A |= (1 << WGM01);													// CTC mode 
	TCCR0A |= (1 << COM0A0);												// Toggles pin each cycle through 
	TCCR0B |= (1 << CS00) | (1 << CS01);									// CPU clock / 64 
}


void InitTimer2(void) {														// Measures milliseconds
	TCCR2A |= (1 << WGM21);													// Configure timer 2 for CTC mode
	TIMSK2 |= (1 << OCIE2A);												// Set interrupt on compare match
	OCR2A = 0;																// Fire interrupt every millisecond
}

ISR (TIMER2_COMPA_vect)	{													// Timer2 interrupt, increment milliseconds
	milliseconds+=1;
}

/************************************************************************/
/* Initialize the AVR                                                   */
/************************************************************************/
void InitAVR() {
	DEBUG_STRING("InitAVR()");

	DDR_BUTTON &= ~(1 << BUTTON1);											// Button pins are inputs */
	DDR_BUTTON &= ~(1 << BUTTON2);
	DDR_BUTTON &= ~(1 << BUTTON3);
	DDR_BUTTON &= ~(1 << BUTTON4);
	PORT_BUTTONS |= ((1 << PORTBUTTON1) | (1 << PORTBUTTON2) | (1 << PORTBUTTON3) | (1 << PORTBUTTON4));  // turn on pullups
	
	DDR_BUTTON |= (1 << UNUSED_BUTTON0);									// Unused pins set as outputs so buttonhistory will make sense*/
	DDR_BUTTON |= (1 << UNUSED_BUTTON5);

	DDR_PIEZO |= (1 << PIEZO_OUT);											// Piezo out
	
	DDR_HEARTBEAT |= (1 << LED_HEARTBEAT);									// Heartbeat LED
}

/************************************************************************/
/* main program                                                         */
/************************************************************************/
void Initialize() {
	initUSART();
	DEBUG_STRING("Initialize()");
	InitPiezo();
	InitTimer2();
	initInturrupts();
	InitAVR();
	InitADC0();
	StartMillisecondsTimer();
	sei();																	// enable interrupts
	DEBUG_STRING("Done Initialize()");
}

int main(void)
{
	Initialize();
	globalButtonPressed = UNUSED_BUTTON;
	Beep();
    while (1)	{
		switch(globalButtonPressed) {
			//DEBUG_METRIC("switch globalButtonPressed: ", globalButtonPressed);
			case BUTTON1  :
				StopMillisecondsTimer();
				RayGunII();
				StartMillisecondsTimer();
				globalButtonPressed = UNUSED_BUTTON;
				break; 
	
			case BUTTON2  :
				StopMillisecondsTimer();
				PlayArray(allNotes[0],allNotes[1],allNotes[2],NELEMS(allNotes[0]), PLAY_FORWARD);
				StartMillisecondsTimer();
				globalButtonPressed = UNUSED_BUTTON;
				break; 
	
			case BUTTON3  :
				StopMillisecondsTimer();
				RayGun();
				StartMillisecondsTimer();
				globalButtonPressed = UNUSED_BUTTON;
				break; 

			case BUTTON4  :
				StopMillisecondsTimer();
				PlayArray(allNotes[0],allNotes[1],allNotes[2],NELEMS(allNotes[0]), PLAY_BACKWARD);
				StartMillisecondsTimer();
				globalButtonPressed = UNUSED_BUTTON;
				break; 

			case UNUSED_BUTTON	:
				if (milliseconds > TIME_TO_SLEEP) {
					GoToSleep();
					milliseconds = 0;
				}
		}		
	}
}

void StartMillisecondsTimer(void) {
	DEBUG_STRING("StartMillisecondsTimer");
	milliseconds = 0;
	TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20);						// set prescaler to 1024 and start the timer
}

void StopMillisecondsTimer(void) {
	DEBUG_STRING("StopMillisecondsTimer");
	TCCR2B &= ~((1 << CS20)|(1 << CS21)|(1 << CS22));
}

void ToggleHeartbeat() {
	PORT_HEARTBEAT ^= (1 << LED_HEARTBEAT);
}

void OnHeartbeat() {
	PORT_HEARTBEAT |= (1 << LED_HEARTBEAT);
}

void OffHeartbeat() {
	PORT_HEARTBEAT &= ~(1 << LED_HEARTBEAT);
}

void GoToSleep(void) {														// Put the AVR to sleep
	DEBUG_STRING("GoToSleep()");
	Beep();
	byte adcsra = ADCSRA;													// save the ADC Control and Status Register A
	ADCSRA = 0;																// disable the ADC
	EICRA = _BV(ISC01);														// configure INT0 to trigger on falling edge
	EIMSK = _BV(INT0);														// enable INT0
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	cli();																	// stop interrupts to ensure the BOD timed sequence executes as required
	sleep_enable();
	uint8_t mcucr1 = MCUCR | _BV(BODS) | _BV(BODSE);						// disable brown-out detection while sleeping (20-25�A)
	uint8_t mcucr2 = mcucr1 & ~_BV(BODSE);
	MCUCR = mcucr1;
	MCUCR = mcucr2;
	//sleep_bod_disable();													// for AVR-GCC 4.3.3 and later, this is equivalent to the previous 4 lines of code
	sei();																	// ensure interrupts enabled so we can wake up again
	DEBUG_STRING("Sleeping...");
	sleep_cpu();															// go to sleep
	sleep_disable();														// wake up here
	DEBUG_STRING("Waking...");
	ADCSRA = adcsra;														// restore ADCSRA
	Initialize();
}

/************************************************************************/
/* Sounds                                                               */
/************************************************************************/

void Beep() {
	PlayNote(NOTE_FS4,250);
}

void PlayArray(int noteArray[], int durationArray[], int restArray[], int sizeOfArray, int direction) {
	if (direction == PLAY_FORWARD) {
		for (int i = 0; i < sizeOfArray; ++i) {
			PlayNote(noteArray[i], durationArray[i]);
			delayms(restArray[i]);
		}
	} else if (direction == PLAY_BACKWARD) {
		for (int i = (sizeOfArray - 1); i >= 0; i--) {
			PlayNote(noteArray[i], durationArray[i]);
			delayms(restArray[i]);
		}
	}
}

void PlayNote(int period, int duration) {
//	period = period >> 9;
	TCNT0 = 0;																/* reset the counter */
	OCR0A = period;															/* set pitch */
	SPEAKER_DDR |= (1 << SPEAKER);											/* enable output on speaker */

	while (duration) {														/* Variable delay */
		_delay_ms(1);
		duration--;
	}
	SPEAKER_DDR &= ~(1 << SPEAKER);											/* turn speaker off */
}
 
void RayGun() {
	for (int j = 0; j < 3; j++) {
		for (int i = NELEMS(allNotes[0]) - 1; i >= 0; i--) {
			PlayNote(allNotes[0][i], 3);
			delayms(0);
		}
	}
}

void RayGunII() {
	for (int j = 0; j < 3; j++) {
		for (int i = 0; i < NELEMS(allNotes[0]); ++i) {
			PlayNote(allNotes[0][i], 5);
			delayms(0);
		}
		for (int i = NELEMS(allNotes[0]) - 1; i >= 0; i--) {
			PlayNote(allNotes[0][i], 5);
			delayms(0);
		}
	}
}