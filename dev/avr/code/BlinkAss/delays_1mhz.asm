;delays_1mhz.asm
delay_1ms1:					; 10 us/loop
	nop						; 1 cycle
	nop						; 1 cycle
	nop						; 1 cycle
	nop						; 1 cycle
	nop						; 1 cycle
	nop						; 1 cycle
	nop						; 1 cycle
	dec			r16
	brne		delay_1ms1	; [2 cycles]
	pop			r16			; restore the value in r16
	ret
;-------------------------------------------------
delay_1000ms:
	rcall		delay_500ms
	rcall		delay_500ms
	ret
;-------------------------------------------------
delay_500ms:
	rcall		delay_101ms
	rcall		delay_101ms
	rcall		delay_101ms
	rcall		delay_101ms
	rcall		delay_101ms		
	ret		
;-------------------------------------------------
delay_101ms:
	rcall		delay_25ms
	rcall		delay_25ms
	rcall		delay_25ms
	rcall		delay_25ms
	ret
;-------------------------------------------------
delay_25ms:
	rcall		delay_5ms
	rcall		delay_5ms
	rcall		delay_5ms
	rcall		delay_5ms
	rcall		delay_5ms
	ret
;-------------------------------------------------
delay_5ms:
	rcall		delay_1ms1
	rcall		delay_1ms1
	rcall		delay_1ms1
	rcall		delay_1ms1
	rcall		delay_1ms1
	ret
;-------------------------------------------------
delay_100ms:
	push		r16			; save the value in r16
	ldi			r16,99		; accounts for overhead of 12 cycles.
