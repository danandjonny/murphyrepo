;
; BlinkAssembler.asm
;
; Created: 11/25/2016 6:02:47 PM
; Author : djmurphy
;

.org		0x0000
	ldi		r16,0b00001111		; Make the lower 4 bits output
	out		ddrd,r16			; for port d.
top:
	sbi		portd,0				; Set bit 0 immediate of port d
	rcall	delay_1000ms		; Calling a subroutine

	cbi		portd,0				; Clear bit 0 immediate of port d
	rcall	delay_100ms

	rjmp top					; Relative jump to label top

#include "delays_1mhz.asm"		; This file has the delays subroutine
