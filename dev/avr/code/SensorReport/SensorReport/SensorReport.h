#define F_CPU 1000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "USART.h"
#include "macros.h"

#include <avr/pgmspace.h>
#include <stdbool.h>
#include <ctype.h>

#include "TWI.h"

// Standard includes
#include <string.h>														// string manipulation routines
#include <stdlib.h>
#include <stdint.h>														// has to be added to use uint8_t

#define DEBUG                   1										// Controls output to UART
#define SS_WRITE_SPEED			125										// how high the timer should count before lighting the next seven segment display
#define DEBOUNCE_TIME			500										// microseconds

// this maps characters 0 - 10 & A - Z to that which is necessary to light the
// correct LEDs on the custom seven segment display.
const uint8_t ssCharMap[38] = { 238,130,220,214,178,118,126,194,254,246,
	250,254,108,238,124,120,246,186,130,134,
	186,44,26,234,238,248,238,250,118,104,
174,174,14,186,178,220,1,0  };

static const uint8_t CHAR_A         = 10;
static const uint8_t CHAR_B         = 11;
static const uint8_t CHAR_C         = 12;
static const uint8_t CHAR_D         = 13;
static const uint8_t CHAR_E         = 14;
static const uint8_t CHAR_F         = 15;
static const uint8_t CHAR_G         = 16;
static const uint8_t CHAR_H         = 17;
static const uint8_t CHAR_I         = 18;
static const uint8_t CHAR_J         = 19;
static const uint8_t CHAR_K         = 20;
static const uint8_t CHAR_L         = 21;
static const uint8_t CHAR_M         = 22;
static const uint8_t CHAR_N         = 23;
static const uint8_t CHAR_O         = 24;
static const uint8_t CHAR_P         = 25;
static const uint8_t CHAR_Q         = 26;
static const uint8_t CHAR_R         = 27;
static const uint8_t CHAR_S         = 28;
static const uint8_t CHAR_T         = 29;
static const uint8_t CHAR_U         = 30;
static const uint8_t CHAR_V         = 31;
static const uint8_t CHAR_W         = 32;
static const uint8_t CHAR_X         = 33;
static const uint8_t CHAR_Y         = 34;
static const uint8_t CHAR_Z         = 35;
static const uint8_t CHAR_DECIMAL   = 36;
static const uint8_t CHAR_SPACE     = 37;

volatile uint16_t sensorDetected;										// the amount sensed from 0 - 1023
volatile uint16_t sensorThreshold;										// determines the threshold at which we sound the alarm

volatile uint8_t charPosition[3];
volatile uint8_t charToLight;
volatile uint8_t switchPosition;

// settings for I2C Slave
char I2C_buffer[4];
#define I2C_SLAVE_ADDRESS 0x10
void handle_I2C_interrupt(volatile uint8_t TWI_match_addr, uint8_t status);

