﻿//	Clock.c
/*
 A clock with the following features:
	- AtMega328P microcontroller running at 8mHz
	- Custom I2C seven segment display using discrete components
	- I2C LCD display to show date, time, temperature
	- beeping CO alarm using passive piezo buzzer and two 555 timers
	- I2C DS1307 Real Time Clock with battery backup
	- Wake alarm sound using UM66 Melody Generator
	- MQ-7 CO detector operating at two voltages; 1.4v & 5.0v.
	- night light via pulse width modulation
	- temperature using TC74A0

	All three timers are used; one for an internal RTC, one for
  to control the voltage to the CO sensor, and one to set
  the brightness of the LED/night light.

 Author:		Copyright ©2017, Daniel Murphy <dan-murphy@comcast.net>
 Contributors:	Bruce E. Hall <bhall66@gmail.com>
				Elliott Williams (Make: AVR Programming)    
 Version:		0.6
 Date:		2015-09-09
 Device:		ATMega328P-PU @ 8mHz
 Language:	C
*/
//	License
/*  Daniel J. Murphy hereby disclaims all copyright interest in this
	program written by Daniel J. Murphy.

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//	Known Defects
/*	
	-	After setting the time via action and state buttons, the new time 
		isn't immediately reflected on the LCD.
	-	When setting time and moving from day of week to hour of day 
		behavior is undefined.
*/
// TODO
/*
	-	Enable the internal pull up on all unused pins
	-	Implement the automatic calculation of day of week using the function
		DayOfWeek, and eliminate reading and writing day of week on DS1307.
	-	Add the TEA5767, move the CO PWM pin off of PORTB
	-	Add the ability to wake to alarm or music
	-   Add the ability to control music volume
*/
/************************************************************************/
/* Includes                                                             */
/************************************************************************/
#include "Clock.h"

/************************************************************************/
/* Debug Routines                                                       */
/************************************************************************/
void debug(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
        printString("\r\n");
    }
}

void debugNoLF(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
    }
}

void debugInt(signed int anInt) {
    if (DEBUG) {
        char myInt[4];
        itoa(anInt,myInt,10);
        debug(myInt);
    }
}

void debugMetric(const char myString[], signed int anInt) {
	debugNoLF(myString);debugNoLF(": ");
	debugInt(anInt);
    printString("\r\n");
}

void debugAlarmTime() {
	char myHours[4];
	itoa(g_state.alarmHours,myHours,10);
	debugNoLF("ALARM HOURS: ");
	debug(myHours);

	char myMinutes[4];
	itoa(g_state.alarmMinutes,myMinutes,10);
	debugNoLF("ALARM MINUTES: ");
	debug(myMinutes);
}

void debugTime() {
	char myHours[4];
	itoa(g_state.hours,myHours,10);
	debugNoLF("HOURS: ");
	debug(myHours);
	debug("");

	char myMinutes[4];
	itoa(g_state.minutes,myMinutes,10);
	debugMetric("MINUTES", g_state.minutes);
}

/************************************************************************/
/* I2C Routines                                                         */
/************************************************************************/
void I2C_Init()															// Calculate TWBR setting for I2C bit rate
{																		// at 16 , the SCL frequency  will be 16/(16+2(TWBR)), assuming 
																		// prescalar of 0. So for 100KHz SCL, 
																		// TWBR = ((F_CPU/F_SCL)-16)/2 = ((16/0.1)-16)/2 = 144/2 
																		// = 72.
    debug("I2C_INIT()");
    TWSR = 0;															// set prescalar to zero
    TWBR = ((F_CPU/F_SCL)-16)/2;										// set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)												// look for a device at the specified address
{																		// return 1=found, 0=not found
    TWCR = TW_START;													// send start condition
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
    }																	// wait
    TWDR = addr;														// load device's bus address
    TWCR = TW_SEND;														// and send it
    i = 0;
    while ((!TW_READY) && (i++ < 101)) {
    }																	// wait
    if (TW_STATUS!=0x18) {
        //debug("Device not detected.");
        //debugNoLF("Address: ");
        //debugInt(addr);
    }
    return (TW_STATUS==0x18);											// return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)											// returns the count of I2C devices found
{   byte foundCount;
    foundCount = 0;
    for (byte addr=start;addr<0xFF;addr++)								// search all 256 addresses
    {
        if (I2C_Detect(addr)) {											// I2C detected?
            foundCount++;
            debugNoLF("I2C ADDRESS FOUND: ");
            debugInt(addr);       
        }
    }
    return foundCount;													// none detected, so return 0.
}

void I2C_Start (byte slaveAddr)											// Look for a device at the specified address.  Same as I2C_Detect.
{   I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)												// sends a data byte to slave
{   TWDR = data;														// load data to be sent
    TWCR = TW_SEND;														// and send it
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
	//printString("/");													// wait
    }
    return (TW_STATUS!=0x28);
}

byte I2C_ReadACK ()														// reads a data byte from slave
{   TWCR = TW_ACK;														// ack = will read more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString("+");
    }																	// wait
    return TWDR;
}

byte I2C_ReadNACK () 													// reads a data byte from slave
{   TWCR = TW_NACK;														// nack = not reading more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString(".");
    }																	// wait
    printString("\r\n");
    return TWDR;
}

void I2C_WriteByte(byte busAddr, byte data)								// Writes a byte of data to the I2C slave
{   I2C_Start(busAddr);													// send bus address
    I2C_Write(data);													// then send the data byte
    I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)	// Writes a byte of data to the device register
{   I2C_Start(busAddr);													// send bus address
    I2C_Write(deviceRegister);											// first byte = device register address
    I2C_Write(data);													// second byte = data for device register
    I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister)				// Reads a byte of data from the device register	
{   byte data = 0;
    I2C_Start(busAddr);													// send device address
    I2C_Write(deviceRegister);											// set register pointer
    I2C_Start(busAddr+READ);											// restart as a read operation
    data = I2C_ReadNACK();												// read the register data
    I2C_Stop();															// stop
    return data;
}

void I2C_FindDevices()													// Makes 100 attempts to find I2C devices
{	int foundCount;
	int findI2CTries;

	foundCount = 0;
	findI2CTries = 0;
	foundCount = I2C_FindDevice (0x00);
	while ((foundCount < (NUM_I2C_DEVICES - 1)) && (findI2CTries < 10))
	{
		findI2CTries += 1;
		debugMetric("FIND DEVICE ATTEMPT #", findI2CTries);
		foundCount = I2C_FindDevice (0x00);
		msDelay(250);
	}
}

/************************************************************************/
/* Custom Seven Segment Display Routines                                */
/************************************************************************/
void SS_DisplayChars(byte charOne, byte charTwo, byte charThree, byte charFour) // Write 4 chars to the SS display
{   I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    byte instruction = SS_SetDisplayInstruction();
    I2C_Write(instruction);												// first byte = device register address
    I2C_Write(ssCharMap[charOne]);										// hours tens pos
    I2C_Write(ssCharMap[charTwo]);										// hours ones pos
    I2C_Write(ssCharMap[charThree]);									// minute tens pos
    I2C_Write(ssCharMap[charFour]);										// minute ones pos
    I2C_Stop();
//	debugMetric("Instruction: ", instruction);
//	debugMetric("charOne: ", ssCharMap[charOne]);
//	debugMetric("charOne: ", ssCharMap[charTwo]);
//	debugMetric("charOne: ", ssCharMap[charThree]);
//	debugMetric("charOne: ", ssCharMap[charFour]);
	
}

byte SS_SetDisplayInstruction()											// set the SS display I2C instruction byte for decimal and military time
{   byte instruction = 0;
    if (g_state.colonOn) {
        SetBitNo(instruction, DECIMAL_ON);
//		debug("decimal is on");
//  } else {
//		debug("decimal is off");
	}
    
    if (g_state.decimalBlink) {
        SetBitNo(instruction, DECIMAL_BLINK);
//		debug("decimal blink is on");
//  } else {
//		debug("decimal blink is off");
	}
    return instruction;     
}

void SS_DisplayTime(byte hours, byte minutes)							// Write hours and minutes to the SS display
{	//debug("SS_DisplayTime()");
	
	if (!g_state.militaryTime) {										// if we're not showing military time, 
		if (hours > 12) hours -= 12;									// subtract 12 from hours
	}
	
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;
    
    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    
    byte instruction = SS_SetDisplayInstruction();
        
    I2C_Write(instruction);												// first byte = instruction settings
    if (tensHours) {
        I2C_Write(ssCharMap[tensHours]);								// hours tens pos
    } else {
        I2C_Write(ssCharMap[CHAR_SPACE]);								// hours tens pos
    }
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[tensMinutes]);									// minute tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minute ones pos
    I2C_Stop();
//	debugMetric("tensHours: ", tensHours);
//	debugMetric("onesHours", onesHours);
//	debugMetric("tensMinutes: ", tensMinutes);
//	debugMetric("onesMinutes: ", onesMinutes);
}

void SS_DisplayTimeAlarmSetHours(byte hours)							// Display 'set alarm hours' to SS display
{    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[tensHours]);									// hours tens pos
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_H]);      
    I2C_Stop();
}

void SS_DisplayTimeAlarmSetMinutes(byte minutes)						// Display 'set alarm minutes' to SS display 
{   int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_SPACE]);      
    I2C_Write(ssCharMap[tensMinutes]);									// minutes tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minutes ones pos
    I2C_Stop();
}

void SS_DisplayTimeSetDays(byte days)									// Display 'set day of month' to SS display
{	int tensDays = days / 10;
	int onesDays = days - (days / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_D]);
	I2C_Write(ssCharMap[CHAR_A]);
	I2C_Write(ssCharMap[tensDays]);										// days tens pos
	I2C_Write(ssCharMap[onesDays]);										// days ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetDayOfWeek(byte dayOfWeek)							// Display 'set day of week' to SS display
{	int tensDayOfWeek = dayOfWeek / 10;
	int onesDayOfWeek = dayOfWeek - (dayOfWeek / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_D]);
	I2C_Write(ssCharMap[CHAR_W]);
	I2C_Write(ssCharMap[tensDayOfWeek]);								// day of week tens pos
	I2C_Write(ssCharMap[onesDayOfWeek]);								// day of week ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetMonths(byte months)								// Display 'set month of year' to SS display
{	int tensMonths = months / 10;
	int onesMonths = months - (months / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_M]);
	I2C_Write(ssCharMap[CHAR_O]);
	I2C_Write(ssCharMap[tensMonths]);									// months tens pos
	I2C_Write(ssCharMap[onesMonths]);									// months ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetYears(byte years)									// Display 'set year' to SS display
{	years -= 2000;
	int tensYears = years / 10;
	int onesYears = years - (years / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_Y]);
	I2C_Write(ssCharMap[CHAR_R]);
	I2C_Write(ssCharMap[tensYears]);									// years tens pos
	I2C_Write(ssCharMap[onesYears]);									// years ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetHours(byte hours)									// Display 'set time hours' to SS display
{	int tensHours = hours / 10;
	int onesHours = hours - (hours / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[tensHours]);									// hours tens pos
	I2C_Write(ssCharMap[onesHours]);									// hours ones pos
	I2C_Write(ssCharMap[CHAR_S]);
	I2C_Write(ssCharMap[CHAR_H]);
	I2C_Stop();
}

void SS_DisplayTimeSetMinutes(byte minutes)								// Display 'set time minutes' to SS display
{	int tensMinutes = minutes / 10;
	int onesMinutes = minutes - (minutes / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);														// colon off during set time
	I2C_Write(ssCharMap[CHAR_S]);
	I2C_Write(ssCharMap[CHAR_SPACE]);
	I2C_Write(ssCharMap[tensMinutes]);									// minutes tens pos
	I2C_Write(ssCharMap[onesMinutes]);									// minutes ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetMilitaryTime(byte milTimeSwitch)					// Display 'set military time' to SS display
{	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);														// colon off during set time
	I2C_Write(ssCharMap[2]);
	I2C_Write(ssCharMap[4]);
	I2C_Write(ssCharMap[CHAR_SPACE]);									// minutes tens pos
	I2C_Write(ssCharMap[milTimeSwitch]);								// minutes ones pos
	I2C_Stop();
}

void SS_BlankDisplay()													// Blank out the SS display
{	SS_DisplayChars(CHAR_SPACE,CHAR_SPACE,CHAR_SPACE,CHAR_SPACE);
}

void ToggleColon()														// Toggle the SS display colon
{	
	if (g_state.mode & WAKE) {
		if (g_state.colonOn) {
			ColonOff();
		} else {
			ColonOn();
		}
	}
}

void ColonOn()															// Set the SS display colon on
{	g_state.colonOn = 1;
}

void ColonOff()															// Set the SS display colon off
{	g_state.colonOn = 0;
}

/************************************************************************/
/* Custom Liquid Crystal Display Routines                               */
/************************************************************************/
void LCD_Reset()														// Clear out the LCD display
{	lcd_led(0);
	lcd_home();
	lcd_clrscr();
	lcd_gotoxy(0,0);
}

void LCD_DisplayTemp(byte tempF)										// Display current temperature in Fahrenheit on LCD
{	char buf[10];
	byte ones = 0, tens = 0, hundreds = 0;
	lcd_gotoxy(TEMP_X,TEMP_Y);
    if (tempF > 120 || tempF < 0) {
		lcd_putc('E');
		lcd_gotoxy(TEMP_X + 1,TEMP_Y);
		lcd_putc('R');
		lcd_gotoxy(TEMP_X + 2,TEMP_Y);
		lcd_putc('R');
	} else {
		ones = tempF - (tempF / 10) * 10;
		tens = tempF / 10;
		hundreds = tempF / 100;
	}
	if (!(hundreds > 0)) {
		lcd_putc(' ');
	} else {
		itoa(hundreds, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(TEMP_X + 1,TEMP_Y);
	if (!(tens > 0)) {
		lcd_putc(' ');
	} else {
		itoa(tens, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(TEMP_X + 2,TEMP_Y);
	itoa(ones, buf, 10);
	lcd_putc(*buf);
	lcd_gotoxy(TEMP_X + 3,TEMP_Y);
	lcd_putc('F');
}

void LCD_DisplayHours(byte hours)										// Display hours portion of the time
{	char buf[10];

	int tensHours = hours / 10;
	int onesHours = hours - (hours / 10) * 10;

	itoa(tensHours, buf, 10);
	lcd_gotoxy(HOUR_TENS_X,HOUR_TENS_Y);
	lcd_putc(*buf);

	itoa(onesHours, buf, 10);
	lcd_gotoxy(HOUR_ONES_X,HOUR_ONES_Y);
	lcd_putc(*buf);
}

void LCD_DisplayMinutes(byte minutes)									// Display the minutes portion of the time
{	char buf[10];

	int tensMinutes = minutes / 10;
	int onesMinutes = minutes - (minutes / 10) * 10;
	
	itoa(tensMinutes, buf, 10);
	lcd_gotoxy(MINUTE_TENS_X,MINUTE_TENS_Y);
	lcd_putc(*buf);

	itoa(onesMinutes, buf, 10);
	lcd_gotoxy(MINUTE_ONES_X,MINUTE_ONES_Y);
	lcd_putc(*buf);	
}

void LCD_DisplaySeconds(byte seconds)									// Display the seconds portion of the time
{	char buf[10];

	int tensSeconds = seconds / 10;
	int onesSeconds = seconds - (seconds / 10) * 10;

	itoa(tensSeconds, buf, 10);
	lcd_gotoxy(SECOND_TENS_X,SECOND_TENS_Y);
	lcd_putc(*buf);

	itoa(onesSeconds, buf, 10);
	lcd_gotoxy(SECOND_ONES_X,SECOND_ONES_Y);
	lcd_putc(*buf);
}

void LCD_DisplayDate(uint16_t years, byte months, byte day, byte dayOfWeek) // Display date on the LCD
{	char buf[10];

	LCD_Reset();
	lcd_gotoxy(COLON_1_X,COLON_1_Y);
	lcd_putc(':');

	lcd_gotoxy(COLON_2_X,COLON_2_Y);
	lcd_putc(':');

	int thouYears  =    years / 1000;
	int	hundYears  =   (years - (thouYears * 1000)) / 100;
	int tensYears  =  ((years - (thouYears * 1000)) - (hundYears * 100)) / 10;
	int onesYears  = (((years - (thouYears * 1000)) - (hundYears * 100)) - tensYears * 10);
	int tensMonths = months / 10;
	int onesMonths = months - (months / 10) * 10;
	int tensDay   = day / 10;
	int onesDay   = day - (day / 10) * 10;

	itoa(thouYears, buf, 10);
	lcd_gotoxy(YEAR_THOU_X,YEAR_THOU_Y);
	lcd_putc(*buf);

	itoa(hundYears, buf, 10);
	lcd_gotoxy(YEAR_HUND_X,YEAR_HUND_Y);
	lcd_putc(*buf);

	itoa(tensYears, buf, 10);
	lcd_gotoxy(YEAR_TENS_X,YEAR_TENS_Y);
	lcd_putc(*buf);

	itoa(onesYears, buf, 10);
	lcd_gotoxy(YEAR_ONES_X,YEAR_ONES_Y);
	lcd_putc(*buf);

	lcd_gotoxy(FORWARD_SLASH_1_X, FORWARD_SLASH_1_Y);
	lcd_putc('/');

	itoa(tensMonths, buf, 10);
	lcd_gotoxy(MONTH_TENS_X,MONTH_TENS_Y);
	lcd_putc(*buf);

	itoa(onesMonths, buf, 10);
	lcd_gotoxy(MONTH_ONES_X,MONTH_ONES_Y);
	lcd_putc(*buf);

	lcd_gotoxy(FORWARD_SLASH_2_X, FORWARD_SLASH_2_Y);
	lcd_putc('/');

	itoa(tensDay, buf, 10);
	lcd_gotoxy(DAY_TENS_X,DAY_TENS_Y);
	lcd_putc(*buf);

	itoa(onesDay, buf, 10);
	lcd_gotoxy(DAY_ONES_X,DAY_ONES_Y);
	lcd_putc(*buf);
	
	byte x = DAY_OF_WEEK_X;
	lcd_gotoxy(x, DAY_OF_WEEK_Y);
	const char *dayChars = DayOfWeekNumToString(dayOfWeek);
	int i = 0;
	while (dayChars[i] != '\0') {
		lcd_putc(dayChars[i++]);
		lcd_gotoxy(++x,DAY_OF_WEEK_Y);		
	}
}

/************************************************************************/
/* DS1307 Real Time Clock Interface                                     */
/************************************************************************/
void DS1307_ReadTime(byte *hours, byte *minutes, byte *seconds)			// returns hours, minutes, and seconds in BCD format
{   //debug("DS1307_ReadTime()");
    *hours = I2C_ReadRegister(DS1307_ADDRESS,HOURS_REGISTER);
    *minutes = I2C_ReadRegister(DS1307_ADDRESS,MINUTES_REGISTER);
    *seconds = I2C_ReadRegister(DS1307_ADDRESS,SECONDS_REGISTER);
    if (*hours & 0x40)	{												// 12hr mode:
        *hours &= 0x1F;													// use bottom 5 bits (pm bit = temp & 0x20)
    } else {
		*hours &= 0x3F;													// 24hr mode: use bottom 6 bits
	}
}

void DS1307_ReadDate(byte *years, byte *months, byte *days, byte *dayOfWeek) // returns months, days, and years in BCD format
{	//debug("DS1307_ReadDate");
	*years	=		I2C_ReadRegister(DS1307_ADDRESS,YEARS_REGISTER	);
	*months =		I2C_ReadRegister(DS1307_ADDRESS,MONTHS_REGISTER	);
    *days	=		I2C_ReadRegister(DS1307_ADDRESS,DAYS_REGISTER	);
	*dayOfWeek =	I2C_ReadRegister(DS1307_ADDRESS,DAYOFWK_REGISTER);
	//debugMetric("dayOfWeek: ", *dayOfWeek);
}

void DS1307_WriteTime(byte hours, byte minutes, byte seconds)			// Write time to the DS1307
{	//debug("DS1307_WriteTime()");										// There's a problem with this function; you can't save a time that's in military format and is > 12:59.
	I2C_WriteRegister(DS1307_ADDRESS,HOURS_REGISTER, DecToBcd(hours));
	I2C_WriteRegister(DS1307_ADDRESS,MINUTES_REGISTER, DecToBcd(minutes));
	I2C_WriteRegister(DS1307_ADDRESS,SECONDS_REGISTER, DecToBcd(seconds));		
	DS1307_ClearClockHaltBit(0x00);										// To clear the clock halt bit
}							

void DS1307_WriteDate(uint16_t years, byte months, byte days, byte dayOfWeek) // Write date to the DS1307
{	//debug("DS1307_WriteDate()");
	years -= 2000;
	//debugMetric("years: ", years);
	//debugMetric("months: ",  months);
	//debugMetric("days: ", days);
	//debugMetric("dayOfWeek: ", dayOfWeek);
	
	I2C_WriteRegister(DS1307_ADDRESS, YEARS_REGISTER	,DecToBcd(years		));
	I2C_WriteRegister(DS1307_ADDRESS, MONTHS_REGISTER	,DecToBcd(months	));
	I2C_WriteRegister(DS1307_ADDRESS, DAYS_REGISTER		,DecToBcd(days		));
	I2C_WriteRegister(DS1307_ADDRESS, DAYOFWK_REGISTER	,dayOfWeek			 );

}

void DS1307_ClearClockHaltBit(byte seconds)								// When clock halt bit is cleared (0) the oscillator is enabled, time advances on the chip
{	byte clockHaltBit = 0b10000000;
	I2C_WriteRegister(DS1307_ADDRESS,SECONDS_REGISTER, seconds & ~(1 << clockHaltBit));
}

/************************************************************************/
/* TEA5767                                                              */
/************************************************************************/
int TEA5767_write(void)
{
	uint8_t ret = 0;
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);								// I2C start
	while (!(TWCR & (1<<TWINT)));           							// I2C wait
	
																		// check value of TWI Status Register. Mask prescaler bits.
	uint8_t twst = TW_STATUS_TEA5767 & 0xF8;
	if ( (twst != TW_START_TEA5767) && (twst != TW_REP_START))
	{
		debugMetric("Write error @START, twst: ", twst);
		ret = 1;
	}
	
	TWDR = SLA_W;                            							// send address
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
	
	twst = TW_STATUS_TEA5767 & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) )
	{
		debugMetric("Write error @ADDR, twst: ", twst);
		ret = 1;
	}
	
	for (uint8_t i = 0; i < 5; i++)										// send registers
	{
		TWDR = write_bytes[i];
		TWCR = (1<<TWINT) | (1<<TWEN);
		while (!(TWCR & (1<<TWINT)));
																		// check value of TWI Status Register. Mask prescaler bits
		twst = TW_STATUS_TEA5767 & 0xF8;
		if( twst != TW_MT_DATA_ACK)
		{
			debugMetric("Write error, twst: ", twst);
			ret = 1;
			break;
		}
	}
	
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);								// I2C stop
																		// wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));
	return ret;
}

int TEA5767_read(void)													// Odczyt danych z TEA5767
{
	uint8_t ret = 0;
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);	// I2C start
	while (!(TWCR & (1<<TWINT)));
	
	// check value of TWI Status Register. Mask prescaler bits.
	uint8_t twst = TW_STATUS_TEA5767 & 0xF8;
	if ( (twst != TW_START_TEA5767) && (twst != TW_REP_START))
	{
		debugMetric("Read error @START, twst: ", twst);
		ret = 1;
	}
	
	TWDR = SLA_R;                           	// send address
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
	
	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS_TEA5767 & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) )
	{
		debugMetric("Read error @ADDR, twst: ", twst);
		ret = 1;
	}
	
	for (uint8_t i = 0; i < 5; i++)
	{
		if (i != 4)
		TWACK;
		else
		TWNACK;
		while (!(TWCR & (1<<TWINT)));
		read_bytes[i] = TWDR;
	}
	
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);	// I2C stop
	// wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));
	return ret;
}

int TEA5767_init(void)
{
	write_bytes[0] = 0b00101111;  // default: 99.9 MHz
	write_bytes[1] = 0x87;
	write_bytes[2] = TEA5767_SUD | TEA5767_SRCH_MID_LVL;
	//write_bytes[2] |= TEA5767_MONO;
	write_bytes[3] = TEA5767_XTAL;
	write_bytes[4] = 0b00000000;
	/** \todo chip detection/identification */
	TEA5767_tune(tune);													// TODO: read this from eeprom
	TEA5767_write();
	return 0;
}

void TEA5767_tune(uint32_t value)
{
	// reference frequency = 32768 Hz
	// low side injection formula
	debugMetric("Tune: ", (unsigned int)(value/100));
	uint16_t n = (uint32_t)4*(value*1000 - 225000) >> 15;
	write_bytes[0] = (write_bytes[0] & 0xC0) | (n >> 8);
	write_bytes[1] = n & 0xFF;
	tune = value;
}

void TEA5767_search(uint8_t up)
{
	if (up)
	{
		debug("Search up");
		write_bytes[2] |= TEA5767_SUD;
		TEA5767_tune(tune+150);
	}
	else
	{
		debug("Search down");
		write_bytes[2] &= ~TEA5767_SUD;
		TEA5767_tune(tune-150);
	}
	write_bytes[0] |= TEA5767_SEARCH | TEA5767_MUTE;
	TEA5767_write();
}

void TEA5767_exit_search(void)
{
	write_bytes[0] = (read_bytes[0] & 0x3f);
	write_bytes[1] = read_bytes[1];
	write_bytes[0] &= ~(TEA5767_SEARCH | TEA5767_MUTE);
	TEA5767_write();
	tune = ((((read_bytes[0]&0x3F)<<8)+read_bytes[1])*32768/4 + 225000)/1000;
	debugMetric("Exit search, tuned: ", (unsigned int)(tune/100));
}

int TEA5767_get_status(struct TEA5767_status *status)
{
	TEA5767_read();
	debugMetric("read_bytes[0]: ",read_bytes[0]);
	debugMetric("read_bytes[1]: ",read_bytes[1]);
	debugMetric("read_bytes[2]: ",read_bytes[2]);
	debugMetric("read_bytes[3]: ",read_bytes[3]);
	debugMetric("read_bytes[4]: ",read_bytes[4]);
	memset(status, 0, sizeof(*status));
	
	uint32_t freq = ((((read_bytes[0]&0x3F)<<8)+read_bytes[1])*32768/4 + 225000)/1000;
	debugMetric("Freq: ", (unsigned int)(freq/100));
	
	if (read_bytes[0] & TEA5767_READY_FLAG) /* ready */
	{
		debug("Ready");
		status->ready = 1;
		uint8_t val = read_bytes[2] & 0x7F; /* IF counter */
		if (abs(val - 0x36) < 2) 			 /* close match */
		{
			debug("Tuned!");
			status->tuned = 1;
		}
	}
	if (read_bytes[0] & TEA5767_BAND_LIMIT_FLAG)
	{
		debug("Band limit");
		status->band_limit = 1;
	}
	if (read_bytes[2] & TEA5767_STEREO)
	{
		debug("Stereo reception");
		status->stereo = 1;
	}
	status->rx_power = read_bytes[3] >> 4;
	debugMetric("rx_power: ", status->rx_power);
	return 0;
}


/************************************************************************/
/* Timers                                                               */
/************************************************************************/

void initTimer0(void) 													// Night Light - Initialize the timer, this timer is for the photo resistive sensor
{   OCR0A = 0;															// set this for PWM duty cycle    
    TCCR0A |= (1 << COM0A1);											// set none-inverting mode        
    TCCR0A |= (1 << WGM01) | (1 << WGM00);								// set fast PWM Mode              
    TCCR0B |= (1 << CS01);												// set prescaler to 8, starts PWM 
    TIMSK0 |= (1 << TOIE0);												// overflow interrupt enable      
}

void initTimer1(void) 													// Real Time Clock
{   OCR1A = 31249;														// 31249 - when we hit this # 1 sec has elapsed @ 8mhz w/ prescaler @ 256
    TCCR1B |= (1 << WGM12);												// Mode 4, CTC on OCR1A
    TIMSK1 |= (1 << OCIE1A);											// Set interrupt on compare match
    TCCR1B |= (1 << CS12);												// set prescaler to 1024 and start the timer
}												

void initTimer2(void) 													// CO sensor - this is for the two different voltages needed for the CO sensor, 1.4v & 5v
{	CO_DDR |= (1 << CO_POWER_PIN);										// output pin for timer2 is OC2A which is PB3
	OCR2A = 0;															// set PWM for 5v
	TCCR2A |= (1 << COM2A1) | (1 << COM2A0);							// Set OC2A on Compare Match, clear OC2A at BOTTOM (inverting mode)
	TCCR2A |= (1 << WGM21) | (1 << WGM20);								// set fast PWM Mode
	TCCR2B |= (1 << CS21);												// set prescaler to 8 and starts PWM
	TIMSK2 |= (1 << TOIE2);												// overflow interrupt enable
}

void offTimer2(void)													// Turn off PMW for the CO sensor
{	TCCR2B &= ~((1 << CS20)|(1 << CS21)|(1 << CS22));					// No clock source (Timer/Counter stopped).
	TIMSK2 &= ~(1<<TOIE2);												// Disable the timer overflow interrupt
	DDRB &= ~(1 << CO_POWER_PIN);										// CO_POWER_PIN no longer for output
}

/************************************************************************/
/* Interrupts                                                           */
/************************************************************************/
void InitInturrupts()													// Initialize interrupts for buttons and switches
{	PCICR |= (1 << PCIE_SWITCH);										// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
																		// Pin Change Mask Register 0 for port B
	PCMSK_SWITCH |= (1 << BUTTON_STATE_INT) | (1 << BUTTON_ACTION_INT) | (1 << ALARM_SWITCH_INT) | (1 << CO_PIN_INT);
																		// Set PCINT0 to trigger an interrupt on state change
	PCICR |= (1 << PCIE_RADIO);											// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
																		// Pin Change Mask Register 0 for port B
	PCMSK_RADIO |= (1 << BUTTON_SEARCH_UP_INT) | (1 << BUTTON_SEARCH_DOWN_INT);
																		// Set PCINT0 to trigger an interrupt on state change
}

ISR(TIMER0_OVF_vect) 													// Timer0 overflow interrupt, this is for the night light
{	count++;
	if (count == 255) {
		float photoVolts =  255 - (float) ReadADC(PHOTO_PIN)/4;			// bright = 24, dark = 239, range of 215
		if (photoVolts > 239) {
			photoVolts = 239;
		}
		if (photoVolts < 24) {
			photoVolts = 24;
		}
		photoVolts = (photoVolts - 24) * 1.186;
		//debug("photoVolts"); debugInt(photoVolts);
		OCR0A = photoVolts;
		count=0;
	}
}

ISR (TIMER1_COMPA_vect)													// Timer1 interrupt, this is the real time clock, triggered every second
{	//debug("ISR (TIMER1_COMPA_vect)");
	if (g_state.rtcRead) {
		g_state.timer1ElapsedSecs += 1;									// This is used for varying the voltage to the CO detector
		ToggleLED();
		if (++g_state.seconds == 60) {
			g_state.seconds = 0;
			if (++g_state.minutes == 60) {
				g_state.minutes = 0;
				if (++g_state.hours == 24) {
					g_state.hours = 0;
					if (++g_state.dayOfWeek == 8) {
						g_state.dayOfWeek = 1;
					}
					if (++g_state.day == 32) {
						g_state.months++;
						g_state.day = 1;
						} else if (g_state.day == 31) {
						if ((g_state.months == 4) || (g_state.months == 6) || (g_state.months == 9) || (g_state.months == 11)) {
							g_state.months++;
							g_state.day = 1;
						}
						} else if (g_state.day == 30) {
						if (g_state.months == 2) {
							g_state.months++;
							g_state.day = 1;
						}
						} else if (g_state.day == 29) {
						if ((g_state.months == 2) && (NotLeap())) {
							g_state.months++;
							g_state.day = 1;
						}
					}
					if (g_state.months == 13) {
						g_state.months = 1;
						g_state.years++;
					}
					g_state.event = EVENT_DAY_CHANGED;
					ProcessEvent(g_state.event);
				}
				g_state.event = EVENT_HOUR_CHANGED;
				ProcessEvent(g_state.event);
			}
			g_state.event = EVENT_MINUTE_CHANGED;
			ProcessEvent(g_state.event);								// not sure why this is necessary but without it RTC doesn't work
		}
		g_state.event = EVENT_SECOND_CHANGED;
		ProcessEvent(g_state.event);
	}
}

ISR (TIMER2_OVF_vect)													// Timer2 overflow interrupt, PMW for the CO sensor
{	//debug("TIMER2_OVF_vect");
	if((g_state.timer1ElapsedSecs >= 90) && (OCR2A == 183)) {
		sensorStable += 1;
		g_state.timer1ElapsedSecs = 0;
		g_state.COreading = (g_state.COReadsSum - g_state.COReadsMax - g_state.COReadsMin) / (g_state.COReadsCount - 2);
		//debugNoLF("CO READING: ");
		debugInt(g_state.COreading);
		g_state.COReadsCount = 0;
		g_state.COReadsSum = 0;
		g_state.COReadsMin = 1024;
		g_state.COReadsMax = 0;
		debug("CHANGED OCR2A TO 0, ENTERING 60 SECOND 5.0V READING/HEATING PHASE.");
		OCR2A = 0;														// voltage = 5.0
		} else if ((g_state.timer1ElapsedSecs >= 60) && (OCR2A == 0)) {
		g_state.timer1ElapsedSecs = 0;
		debug("CHANGED OCR2A TO 183, ENTERING 90 SECOND 1.4V PHASE.");
		OCR2A = 183;													// voltage = 1.4
	}
	if(	(g_state.timer1ElapsedSecs > 60) &&								// read for the last 30 seconds of the reading phase, 30 times
	(g_state.timer1ElapsedSecs < 90) &&
	(OCR2A == 183) &&
	(g_state.timer1LastSecSampled != g_state.timer1ElapsedSecs)) {
		g_state.timer1LastSecSampled = g_state.timer1ElapsedSecs;
		g_state.COSampleReading = ReadADC(CO_READ_PIN);					// read the CO sensor
		int tries = 1;
		while (1023 == g_state.COSampleReading && tries <= 10) {
			tries++;
			g_state.COSampleReading = ReadADC(CO_READ_PIN);				// read the CO sensor
		}
		debugNoLF("CO Reading: "); debugInt(g_state.COSampleReading);
		if(g_state.COSampleReading != 1023) {
			g_state.COReadsSum += g_state.COSampleReading;
			g_state.COReadsCount += 1;
			if(g_state.COSampleReading > g_state.COReadsMax) {
				g_state.COReadsMax = g_state.COSampleReading;
			}
			if(g_state.COSampleReading < g_state.COReadsMin) {
				g_state.COReadsMin = g_state.COSampleReading;
			}
			g_state.timer1LastSecSampled = g_state.timer1ElapsedSecs;
		}
	}
}

ISR (PCINT0_vect)														// Interrupts for buttons and switches
{	debug("ISR(PCINT0_VECT)");
	byte changedbbits;
	byte changeddbits;

	//char myStringTwo[4];
	//itoa(PIN_SWITCH,myStringTwo,2);
	//debugNoLF("PIN_SWITCH: ");debug(myStringTwo);

	//char myStringThree[4];
	//itoa(portbhistory,myStringThree,2);
	//debugNoLF("portbhistory: ");debug(myStringThree);

	char myStringFour[4];
	itoa(PIN_RADIO,myStringFour,2);
	debugNoLF("PIN_RADIO: ");debug(myStringFour);

	char myStringFive[4];
	itoa(portdhistory,myStringFive,2);
	debugNoLF("portdhistory: ");debug(myStringFive);

	changedbbits = PIN_SWITCH ^ portbhistory;
	ClearBitNo(changedbbits,CO_POWER_PIN);								// CO_POWER_PIN is not a switch, ignore it
	ClearBitNo(changedbbits,PORTB4);									// not a switch, ignore it

	//char myString[4];
	//itoa(changedbbits,myString,2);
	//debugNoLF("changedbbits: ");debug(myString);

	portbhistory = PIN_SWITCH;
	ClearBitNo(portbhistory,CO_POWER_PIN);								// TODO: remove this! [CO_POWER_PIN is not a switch, ignore it]
	ClearBitNo(portbhistory,PORTB4);									// TODO: remove this! [not a switch, ignore it]

	//char myStringSix[4];
	//itoa(portbhistory,myStringSix,2);
	//debugNoLF("portbhistory: ");debug(myStringSix);

	changeddbits = PIN_RADIO ^ portdhistory;
	ClearBitNo(changeddbits,PORTD0);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD1);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD2);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD5);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD6);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD7);									// not a switch, ignore it

	char myStringSeven[4];
	itoa(changeddbits,myStringSeven,2);
	debugNoLF("changeddbits: ");debug(myStringSeven);

	portdhistory = PIN_RADIO;
	ClearBitNo(portdhistory,PORTD0);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD1);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD2);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD5);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD6);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD7);									// not a switch, ignore it

	char myStringEight[4];
	itoa(portdhistory,myStringEight,2);
	debugNoLF("portdhistory: ");debug(myStringEight);

	if(changedbbits & (1 << BUTTON_STATE_PIN))							// PCINT0 changed
	{
		if( (portbhistory & (1 << BUTTON_STATE_PIN)) == 1 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, BUTTON_STATE_PIN)) {				// LOW to HIGH pin change (state button released)
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH0 set, low to high");
				ProcessEvent(EVENT_PRESS_STATE);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, BUTTON_STATE_PIN)) {			// still pressed
				// HIGH to LOW pin change (state button pressed)
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH0 set, high to low");
			}
		}
	}

	if(changedbbits & (1 << BUTTON_ACTION_PIN))
	{
		if( (portbhistory & (1 << BUTTON_ACTION_PIN)) == 2 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, BUTTON_ACTION_PIN)) {			// LOW to HIGH pin change (action button released)
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH1 set, low to high");
				ProcessEvent(EVENT_PRESS_ACTION);
			}
		}
		else
		{																// HIGH to LOW pin change (action button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, BUTTON_ACTION_PIN)) {			// still pressed
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH1 set, high to low");
			}
		}
	}

	if(changedbbits & (1 << ALARM_SWITCH_PIN))							// PCINT0 changed
	{
		if( (portbhistory & (1 << ALARM_SWITCH_PIN)) == 4 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {				// LOW to HIGH pin change (alarm switch LEFT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH2 SET, LOW TO HIGH");
				ProcessEvent(EVENT_SWITCH_ON);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, ALARM_SWITCH_PIN)) {			// still pressed
				// HIGH to LOW pin change (alarm switch RIGHT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH2 SET, HIGH TO LOW");	// alarm switch right
				ProcessEvent(EVENT_SWITCH_OFF);
			}
		}
	}

	if(changedbbits & (1 << CO_SWITCH_PIN))								// PCINT0 changed
	{
		if( (portbhistory & (1 << CO_SWITCH_PIN)) == 32 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {				// LOW to HIGH pin change (alarm switch LEFT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH5 SET, LOW TO HIGH");
				ProcessEvent(EVENT_CO_ON);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, CO_SWITCH_PIN)) {				// still pressed
				// HIGH to LOW pin change (alarm switch RIGHT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH5 SET, HIGH TO LOW");
				ProcessEvent(EVENT_CO_OFF);
			}
		}
	}

	if(changeddbits & (1 << BUTTON_SEARCH_UP_PIN))						// PCINT0 changed
	{
		if( (portdhistory & (1 << BUTTON_SEARCH_UP_PIN)) == (1 << BUTTON_SEARCH_UP_PIN) )	// TODO: test using this instead of "8"
		{																					// -or- can we say if (portdhistory == BUTTON_SEARCH_UP_PIN) ??
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, BUTTON_SEARCH_UP_PIN)) {			// LOW to HIGH pin change (button released)
				FlashLED();
				debug("PCINT0_VECT, PIN_RADIO3 SET, LOW TO HIGH");
				//ProcessEvent(0);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, BUTTON_SEARCH_UP_PIN)) {		// still pressed
				// HIGH to LOW pin change (search up button pressed)
				FlashLED();
				debug("PCINT0_VECT, PIN_RADIO3 SET, HIGH TO LOW");
				ProcessEvent(EVENT_SEARCH_UP);
			}
		}
	}

	if(changeddbits & (1 << BUTTON_SEARCH_DOWN_PIN))						// PCINT0 changed
	{
		if( (portdhistory & (1 << BUTTON_SEARCH_DOWN_PIN)) == (1 << BUTTON_SEARCH_DOWN_PIN) )	// TODO: test using this instead of 16
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_RADIO, BUTTON_SEARCH_DOWN_PIN)) {		// LOW to HIGH pin change (button released)
				FlashLED();
				debug("PCINT0_VECT, PIN_RADIO4 SET, LOW TO HIGH");
				//ProcessEvent(0);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_RADIO, BUTTON_SEARCH_DOWN_PIN)) {		// still pressed
				// HIGH to LOW pin change (search up button pressed)
				FlashLED();
				debug("PCINT0_VECT, PIN_SWITCH4 SET, HIGH TO LOW");
				ProcessEvent(EVENT_SEARCH_DOWN);
			}
		}
	}
}

/************************************************************************/
/* Analog to digital conversion                                         */
/************************************************************************/
void InitADC0()															// Initialize ADC
{	ADMUX |= (1 << REFS0);												// reference voltage on AVCC
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0);								// ADC clock prescaler  / 8
	ADCSRA |= (1 << ADEN);												// enable ADC
}

uint16_t ReadADC(int channel)											// Read analog to digital converter
{	//debug("ReadADC()");
	ADMUX &= 0xF0;														// Clear the older channel that was read
	ADMUX |= channel;													// Defines the new ADC channel to be read
																		// first sample after you've changed the
																		// multiplexer will still be from the old
																		// analog source, you need to wait at
																		// least one complete ADC cycle before
																		// getting a value from the new channel
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	uint8_t theLowADC = ADCL;
	uint16_t theTenBitResults = ADCH<<8 | theLowADC;
	return(theTenBitResults);
}

/************************************************************************/
/* Time                                                                 */
/************************************************************************/
void InitRTC()															// Initialize RTC, read date and time from DS1307.														
{																		// Bit 7 of Register 0 is the clock halt (CH) bit. When this bit is set to 1, the
																		// oscillator is disabled. When cleared to 0, the oscillator is enabled. On
																		// first application of power to the device the time and date registers are
																		// typically reset to 01/01/00 01 00:00:00 (MM/DD/YY DOW HH:MM:SS). The CH bit
																		// in the seconds register will be set to a 1.
	DS1307_ClearClockHaltBit(g_state.seconds);							// clear the clock halt bit
	g_state.rtcRead = 0;

	int badRead = 1;
	int i = 0;
	while ((badRead) && (i++ <= 3)) {
		ReadTime(   &g_state.hours,
					&g_state.minutes,
					&g_state.seconds	);
		ReadDate(	&g_state.years,
					&g_state.months,
					&g_state.day,
					&g_state.dayOfWeek	);
		badRead = 0;
		if (g_state.hours > 23 || g_state.hours < 0) {
			//debugMetric("Bad hours read: ",g_state.hours);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		if (g_state.minutes > 59 || g_state.minutes < 0) {
			//debugMetric("Bad minutes read: ", g_state.minutes);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		if (g_state.seconds > 59 || g_state.seconds < 0) {
			//debugMetric("Bad seconds read: ", g_state.seconds);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		
		if ((g_state.seconds == 0) &&
		(g_state.minutes == 0) &&
		(g_state.seconds == 0)) {
			badRead = 1;
			msDelay(1000);
		}
		//debugMetric("RTC READ ATTEMPT", i);
	}
	DS1307_ClearClockHaltBit(g_state.seconds);							// clear the clock halt bit
}

void ReadTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds) // Read time
{	//debug("ReadTime()");
	if(!g_state.rtcRead) {												// if we have just powered on get the time from the RTC
		byte minutesRTC;
		byte hoursRTC;
		byte secondsRTC;
		DS1307_ReadTime(&hoursRTC,&minutesRTC,&secondsRTC);
		*inHours = BcdToDec(hoursRTC);
		*inMinutes = BcdToDec(minutesRTC);
		*inSeconds = BcdToDec(secondsRTC);
		g_state.rtcRead = 1;											// all subsequent setting of time is accomplished
																		// via the Timer1 RTC.
		debugTime();
	} else {
		*inHours	= g_state.hours;									// hours, mins and secs updated by Timer1
		*inMinutes	= g_state.minutes;
		*inSeconds	= g_state.seconds;
	}
}
																		// Read date from DS1307
void ReadDate(volatile uint16_t *inYears, volatile byte *inMonths, volatile byte *inDay, volatile byte *inDayOfWeek)	
{	//debug("ReadDate()");
	byte yearsRTC;
	byte monthsRTC;
	byte dayRTC;
	byte dayOfWeekRTC = 0;
	DS1307_ReadDate(&yearsRTC,&monthsRTC,&dayRTC,&dayOfWeekRTC);	
	
//	dayOfWeekRTC = DayOfWeek(BcdToDec(yearsRTC) + 2000, BcdToDec(monthsRTC), BcdToDec(dayRTC)); // TODO: why is this causing a reset?
		
	*inYears		= BcdToDec(yearsRTC		) + 2000;	
	*inMonths		= BcdToDec(monthsRTC	);
	*inDay			= BcdToDec(dayRTC		);		
	*inDayOfWeek	= BcdToDec(dayOfWeekRTC	);
	
//	debugMetric("inYears: ", *inYears);
//	debugMetric("inMonths: ", *inMonths);
//	debugMetric("inDay: ", *inDay);
//	debugMetric("inDayOfWeek: ", *inDayOfWeek);
}

/*
byte DayOfWeek(int y, byte m, byte d)									// Implementation due to Tomohiko Sakamoto, calculate day of week from date
{   // y > 1752, 1 <= m <= 12
	debugMetric("y: ", y);
	debugMetric("m: ", m);
	debugMetric("d: ", d);
	
	static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	
	y -= m < 3;
	return ((y + y/4 - y/100 + y/400 + t[m-1] + d) % 7) + 1;			// 01 - 07, 01 = Sunday
}
*/

void ShowTime(byte hours, byte minutes)									// Update SS display with the time; hours and minutes
{	//debug("ShowTime()");
	SS_DisplayTime(hours, minutes);
	ColonOn();
}

/************************************************************************/
/* Alarms                                                               */
/************************************************************************/
void InitAlarms() 														// Determine if the alarm switch is on or off and process the appropriate event
{	portbhistory = 0b00000000;
	SetBitNo(portbhistory,BUTTON_STATE_PIN);
	SetBitNo(portbhistory,BUTTON_ACTION_PIN);
	portdhistory = 0b00000000;
	SetBitNo(portdhistory,BUTTON_SEARCH_UP_PIN);
	SetBitNo(portdhistory,BUTTON_SEARCH_DOWN_PIN);
	
    if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {						// if switch is ON, LEFT
		//debug("ALARM SWITCH IS ON");
        ProcessEvent(EVENT_SWITCH_ON);
		SetBitNo(portbhistory, ALARM_SWITCH_PIN);
    }
    if (bit_is_clear(PIN_SWITCH, ALARM_SWITCH_PIN)) {					// if switch is OFF, RIGHT           
		//debug("ALARM SWITCH IS OFF");
        ProcessEvent(EVENT_SWITCH_OFF);
		ClearBitNo(portbhistory,ALARM_SWITCH_PIN);
    }
	if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {						// if switch is ON, RIGHT
		//debug("CO IS ON");
		ProcessEvent(EVENT_CO_ON);
		SetBitNo(portbhistory, CO_SWITCH_PIN);
	}
	if (bit_is_clear(PIN_SWITCH, CO_SWITCH_PIN)) {						// if switch is OFF, LEFT
		//debug("CO IS OFF");
		ProcessEvent(EVENT_CO_OFF);
		ClearBitNo(portbhistory, CO_SWITCH_PIN);
	}
    char myString[4];
    itoa(portdhistory,myString,2);
    debugNoLF("initial portdhistory: ");debug(myString);

}

void SoundAlarm() 														// Trun on the CO alarm. TODO: make this stop after a minute (configurable time)
{	//debug("SoundAlarm()");
	SetBitNo(PORTC,BUZZER_PIN);
}

void SoundWake()														// Turn on the wake alarm														
{   //debug("SoundWake()");
    ClearBitNo(PORTD,WAKE_PIN);
}

void SilenceAlarm()														// Turn of the wake alarm
{   //debug("SILENCEALARM()");
    ClearBitNo(PORTC,BUZZER_PIN);
    SetBitNo(PORTD,WAKE_PIN);
}

void SnoozeAlarm()														// Snooze the wake alarm for SNOOZE_TIME
{   //debug("SnoozeAlarm()");
    SilenceAlarm();
    g_state.alarmMinutes += SNOOZE_TIME;
    if (g_state.alarmMinutes > 59) {
        g_state.alarmMinutes = g_state.alarmMinutes - 60;
        g_state.alarmHours += 1;
    }
}

int WasWakeAlarmTriggered() 											// TODO: pass in the structure by value
{	//debug("WasWakeAlarmTriggered()");
	if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {						// alarm switch LEFT, ON, HIGH
		if (!((g_state.mode & BUZZING) == BUZZING)) {					// if the alarm isn't already going off
			if ((g_state.hours == g_state.alarmHours) &&
			(g_state.minutes == g_state.alarmMinutes) &&
			(g_state.seconds >= 0 && g_state.seconds <= 2)) {			// so that we only trigger the alarm once
				//debug("WAKE ALARM TRIGGERED");
				return 1;
			}
		}
	}
	return 0;
}

/************************************************************************/
/* eeprom                                                               */
/************************************************************************/
void EEPROM_WriteAlarmTime(byte hours, byte minutes)					// Write the wake alarm time to eeprom
{	//debug("EEPROM_WriteAlarmTime()");
	//uint16_t alarmTimeWrite;
	//alarmTimeWrite = (hours << 8) | minutes;
	//debugMetric("ALARMTIMEWRITE",(int) alarmTimeWrite);
	uint8_t *address_minutes = (uint8_t *) ALARM_ADDRESS;
	eeprom_update_byte(address_minutes, minutes);
	uint8_t *address_hours = (uint8_t *) ALARM_ADDRESS + 1;
	eeprom_update_byte(address_hours, hours);

	debugAlarmTime();
}

void EEPROM_ReadAlarmTime(volatile byte *hours, volatile byte *minutes)	// Read the wake alarm time from eeprom
{    //debug("EEPROM_ReadAlarmTime()");
	uint8_t *address_minutes = (uint8_t *) ALARM_ADDRESS;
	*minutes = eeprom_read_byte(address_minutes);
	uint8_t *address_hours = (uint8_t *) ALARM_ADDRESS + 1;
	*hours = eeprom_read_byte(address_hours);

	if ((*hours < 0) || (*hours > 23)) {								// Wake hours and minutes are corrupt the first time they are read from memory, clean that up
		*hours = 0;
	}
	if ((*minutes < 0) || (*minutes > 59)) {
		*minutes = 0;
	}

	//debug("READ ALARM TIME");
	debugAlarmTime();
}

void EEPROM_WriteMilitaryTimeSwitch(byte military_time_switch)			// Write the military time switch setting to eeprom
{	//debug("EEPROM_WriteMilitaryTimeSwitch()");
	eeprom_update_byte((uint8_t *) MILITARY_TIME_ADDRESS, military_time_switch);
}

void EEPROM_ReadMilitaryTimeSwitch(volatile byte *military_time_switch)	// Read the military time switch setting from eeprom
{    //debug("EEPROM_ReadMilitaryTimeSwitch()");
	*military_time_switch = eeprom_read_byte((uint8_t *) MILITARY_TIME_ADDRESS);

	if ((*military_time_switch < 0) || (*military_time_switch > 1)) {	
		*military_time_switch = 1;
		//debug("mil time corrupt");
	}
	//debugMetric("Military Time Read from EEPROM: ", *military_time_switch);
	
	if (*military_time_switch) {										// set show military time state
		g_state.militaryTime = 1;
	} else {
		g_state.militaryTime = 0;
	}

}

/************************************************************************/
/* Temperature                                                          */
/************************************************************************/
double GetInternalTemp(void)											// get the chip temperature and convert to farenheit
{
	ADMUX = (3 << REFS0) | (8 << MUX0);									// 1.1V REF, channel#8 is temperature
	ADCSRA |= (1 << ADEN) | (6 << ADPS0);								// enable the ADC div64
	msDelay(20);														// wait for voltages to become stable.
	ADCSRA |= (1 << ADSC);												// Start the ADC

	while (ADCSRA & (1 << ADSC));										// Detect end-of-conversion
	//return (ADCW - 324.31) / 1.22;
	return (ADCW * (1.1/1023.0) * (25.0/0.314) * 1.8 + 32.0);
	
}

void ShowTemp(int tempF)												// Write room temp to SS display
{   //debug("SHOWTEMP()");
    
    if (tempF > 120 || tempF < 0) {
        SS_DisplayChars(CHAR_E,CHAR_R,CHAR_R,CHAR_SPACE);
    } else {
        byte ones = tempF - (tempF / 10) * 10;
        byte tens = tempF / 10;
        byte hundreds = tempF / 100;
        SS_BlankDisplay();
        if (!(hundreds > 0)) {
            hundreds = CHAR_SPACE;
        }
        if (!(tens > 0)) {
            tens = CHAR_SPACE;
        }
        SS_DisplayChars(hundreds,tens,ones,CHAR_F);
        ColonOff();
    }
	//debugMetric("TEMPERATURE (F)",tempF);
}

signed int ConvertToFarenheit(unsigned short celsius)					// Convert from celsius to farenheit		
{    if (celsius > 127) celsius = ~celsius + 1;							// Check for negative temperature 
    signed int farenheit = 32.0 + (celsius * 9.0/5.0);
    //debugMetric("TEMPERATURE IN FARENHEIT", farenheit);
    return farenheit;
}

/************************************************************************/
/* Carbon Monoxide                                                      */
/************************************************************************/
int WasCOGasAlarmTriggered()											// returns true if the CO alarm is tripped
{  //debug("WasCOGasAlarmTriggered()");
																		// if the CO sensor is powered up
   if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {							// alarm switch LEFT, ON, HIGH      
        //debug("co_reading");
        //debugInt(co_reading);
        if ((g_state.COreading > CO_ALARM_THRESHOLD) && (sensorStable >= CONSECUTIVE_CO_READING_THRESHOLD)) {
            return(1);
        }
   }
   return(0);     
}     

void DisplayWarnForCO()													// write "CO" to the seven segment display
{	SS_DisplayChars(CHAR_SPACE, CHAR_C, CHAR_O, CHAR_SPACE);
	ColonOff();
}

/************************************************************************/
/* Misc Routines                                                        */
/************************************************************************/
void msDelay(int delay)  												// Wait for 'delay' milliseconds
{	for (int i=0;i<delay;i++)											// to remove code inlining
	_delay_ms(1);														// at cost of timing accuracy
}

void SoundOneShortBeep()												// 250ms beep
{	//debug("SoundOneShortBeep()");
	SetBitNo(PORTC,BUZZER_PIN);											// set BUZZER_PIN HIGH
	msDelay(250);
	ClearBitNo(PORTC,BUZZER_PIN);										// set BUZZER_PIN LOW
}

void FlashLED()															// turn on the heartbeat LED for 100ms
{	//debug("FlashLED()");
	SetBitNo(PORTC,HEARTBEAT_LED_PIN);
	msDelay(100);
	ClearBitNo(PORTC,HEARTBEAT_LED_PIN);
}

void ToggleLED()														// Toggle the heartbeat LED
{	PORTC ^= (1<<HEARTBEAT_LED_PIN);
}

byte DecToBcd(byte val)													// Convert normal decimal numbers to binary coded decimal
{
	return ( (val/10*16) + (val%10) );
}

byte BcdToDec(byte val)													// Convert binary coded decimal to normal decimal numbers
{
	return ( (val/16*10) + (val%16) );
}

const char *DayOfWeekNumToString(byte dayOfWeekNum)						// Convert a numbered day of the week to an abbreviated name of the day
{	if (dayOfWeekNum < 1 || dayOfWeekNum > 7) return "Unk\0";
	const char* days[] = {"Sun\0","Mon\0","Tue\0","Wed\0","Thur\0","Fri\0","Sat\0"};
	return days[dayOfWeekNum - 1];
}

char NotLeap(void)														// Check for Leap Year
{
	if (!(g_state.years % 100)) {
		return (char)(g_state.years % 400);
		} else {
		return (char)(g_state.years % 4);
	}
}

void SetState(uint32_t state)											// Given a state, sets it on g_state.mode
{
	SetBit(g_state.mode, state);
}

void ClearState(uint32_t state)											// Clears a state from g_state.mode
{
	ClearBit(g_state.mode, state);										// Clear the state from the mode
}

/************************************************************************/
/* Initialize the AVR                                                   */
/************************************************************************/

void InitAVR()															// prepare pins for input and output
{	debug("INITAVR()");
	
	// configure all pins for input with pull-ups enabled
	DDRB = 0x00;														// put all pins in input mode
	PORTB = 0Xff;														// enable pull-ups
	DDRC = 0x00;														// put all pins in input mode
	PORTC = 0Xff;														// enable pull-ups
	DDRD = 0x00;														// put all pins in input mode
	PORTD = 0Xff;														// enable pull-ups

	// Selectively configure pins (anything not touched is an input w/ a pull-up)
	DDRB |= (1 << CO_POWER_PIN);										// Configure CO power pin for output.
	DDRC |= (1 << BUZZER_PIN);											// Configure buzzer pin for output.
	ClearBitNo(PORTC,BUZZER_PIN);										// Turn off the buzzer.
	DDRD |= (1 << WAKE_PIN);											// Configure wake pin for output.
	SetBitNo(PORTD,WAKE_PIN);
	DDRD |= (1 << NIGHTLIGHT_PIN);										// Configure night light pin for output
	DDRB &= ~(1 << BUTTON_STATE_PIN);									// Clear the BUTTON_STATE_PIN,
	DDRB &= ~(1 << BUTTON_ACTION_PIN);									// BUTTON_ACTION_PIN,
//	DDRD &= ~(1 << BUTTON_SEARCH_UP_PIN);								// set search up button to input
//	DDRD &= ~(1 << BUTTON_SEARCH_DOWN_PIN);								// set search down button to input
	//DDRD &= ~(1 << DDD0);												// RX pin
	//DDRD &= ~(1 << DDD1);												// TX pin
//	DDRD &= ~(1 << DDD2);												// Unused
	//DDRD &= ~(1 << DDD5);												// Output for wake alarm
	//DDRD &= ~(1 << DDD6);												// Output for nightlight
//	DDRD &= ~(1 << DDD7);												// Unused
	DDRB &= ~(1 << ALARM_SWITCH_PIN);									// and ALARM_SWITCH_PIN pin (sets them to input).
	DDRB &= ~(1 << DDB4);												// unused set to input so as not to effect portbhistory / PIN_SWITCH
	DDRB &= ~(1 << CO_SWITCH_PIN);										// and CO_SWITCH_PIN pin (sets them to input).
	PORTB |= ((1 << PORTB0) | (1 << PORTB1) | (1 << PORTB2));			// Turn On the Pull-ups TODO: is this necessary, we're using pull up resistors?
//	PORTD |= ((1 << PORTD3) | (1 << PORTD4));							// Turn on the pull-ups for the radio tune up and down buttons
	DDRC |= (1 << HEARTBEAT_LED_PIN);									// Configure the heartbeat LED for output.
																		// Make all unused pins inputs with the internal pull up resistor enabled
}

//void InitAVR()															// prepare pins for input and output
//{	debug("INITAVR()");
	//
	//// configure all pins for input with pull-ups enabled
	//DDRB = 0x00;														// put all pins in input mode
	//PORTB = 0Xff;														// enable pull-ups
	//DDRC = 0x00;														// put all pins in input mode
	//PORTC = 0Xff;														// enable pull-ups
	//DDRD = 0x00;														// put all pins in input mode
	//PORTD = 0Xff;														// enable pull-ups
//
	//// Selectively configure pins for output (anything not touched is an input w/ a pull-up)
	//DDRB |= (1 << CO_POWER_PIN);										// Configure CO power pin for output.
	//DDRC |= (1 << BUZZER_PIN);											// Configure buzzer pin for output.
	//DDRD |= (1 << WAKE_PIN);											// Configure wake pin for output.
	//ClearBitNo(PORTC,BUZZER_PIN);
	//SetBitNo(PORTD,WAKE_PIN);
	//DDRD |= (1 << NIGHTLIGHT_PIN);										// Configure nightlight pin for output
	//DDRB &= ~(1 << BUTTON_STATE_PIN);									// Clear the BUTTON_STATE_PIN,
	//DDRB &= ~(1 << BUTTON_ACTION_PIN);									// BUTTON_ACTION_PIN,
	//DDRB &= ~(1 << ALARM_SWITCH_PIN);									// and ALARM_SWITCH_PIN pin (sets them to input).
	//DDRB &= ~(1 << DDB4);												// unused set to input so as not to effect portbhistory / PIN_SWITCH
	//DDRB &= ~(1 << CO_SWITCH_PIN);										// and CO_SWITCH_PIN pin (sets them to input).
	//PORTB |= ((1 << PORTB0) | (1 << PORTB1) | (1 << PORTB2));			// Turn On the Pull-ups TODO: is this necessary, we're using pull up resistors?
	//DDRC |= (1 << HEARTBEAT_LED_PIN);									// Configure the heartbeat LED for output.
	//// Make all unused pins inputs with the internal pull up resistor enabled
//}


/************************************************************************/
/* High Level Program Control                                           */
/************************************************************************/
int main(void)															// main processing loop
{   Initialize();
    debug("MAIN()");
    byte eventToProcess = g_state.event;
    while(1) {															// Event loop, forever
//		wdt_reset();													// reset the watchdog timer every ~1 second
        ProcessEvent(eventToProcess);
        if (g_state.event == eventToProcess) {							// if a new event wasn't triggered as we processed this event
            g_state.event = EVENT_NONE;									// clear out the event just processed
            msDelay(1000);												// one second between updates  
        }
        g_state.event = CheckForEvents();								// check CO sensor and wake alarm
        eventToProcess = g_state.event;
    }
}

void Initialize()														// called at startup to prepare all peripherals
{	initUSART();

	//debug("==============================================================");
	//debug("Copyright (c) 2017, Daniel Murphy");
	//debug("==============================================================");

	debug("INITIALIZE THE SYSTEM.");
	//debug("INITIALIZED USART.  INITIALIZE AVR.");

	debug("INITIALIZING AVR.");
	InitAVR();															// set port direction

	debug("SEND ONE SHORT BEEP.");
	FlashLED();
	SoundOneShortBeep();

	debug("INITIALIZING I2C.");
	FlashLED();
	I2C_Init();															// set I2C clock frequency

	debug("WAIT FOR I2C DEVICES TO COME ONLINE.");
	msDelay(5000);
	
	debug("SHOW ALL I2C DEVICES.");
	FlashLED();
	I2C_FindDevices();

	debug("INITIALIZE RTC.");
	FlashLED();
	InitRTC();

	debug("INITIALIZE ADC.");
	FlashLED();
	InitADC0();
	
	debug("INITIALIZE TIMERS.");
	FlashLED();
	initTimer0();														// initialize Timer0
	FlashLED();
	initTimer1();														// initialize Timer1
	FlashLED();
	initTimer2();														// initialize Timer2

	debug("INITIALIZE INTERRUPTS.");
	FlashLED();
	InitInturrupts();
	
	debug("INITIALIZE THE WAKE ALARM.");
	FlashLED();
	g_state.alarmHours = 0;
	g_state.alarmMinutes = 0;											//(no seconds for alarm)
	
	EEPROM_ReadAlarmTime(	&g_state.alarmHours,
							&g_state.alarmMinutes	);					// Read the wake time from chip memory
	InitAlarms();														// Initialize the alarm switch.

	EEPROM_ReadMilitaryTimeSwitch(&g_state.militaryTime);
	debugMetric("Mil time after function call:", g_state.militaryTime);

	debug("INITIALIZE THE LCD.");
	FlashLED();	
    lcd_init(LCD_DISP_ON);												// caused the SS to not work when we were using the default TWBR of 10000
	LCD_Reset();

	debug("Initialize TEA5767.");
	FlashLED();
	TEA5767_init();	
	
	debug("ENABLE GLOBAL INTERRUPTS.\r\n");
	FlashLED();
	sei();																// enable interrupts

	debug("FINISHED INITIALIZATION.\r\n");
	FlashLED();
	SoundOneShortBeep();

	g_state.mode |= SHOWING_TIME;										// set up to show the time immediately
	sensorStable = 0;													// the CO sensor isn't stable yet.
	
	ProcessEvent(EVENT_DAY_CHANGED);									// this sets up all of the displays
	ProcessEvent(EVENT_HOUR_CHANGED);
	ProcessEvent(EVENT_MINUTE_CHANGED);
	ProcessEvent(EVENT_SECOND_CHANGED);

//	wdt_enable(WDTO_2S);												// enable the watchdog timer for 2 seconds TODO: figure out why it keeps triggering 31 seconds after system start
}
																		// check for events that don't have interrupts, this is in
byte CheckForEvents() 													// the mainline of processing
{																		// Check if the CO gas event was triggered.
	if (WasCOGasAlarmTriggered()) {										// CO alarm has priority over wake alarm
		return(EVENT_CO_DETECTED);
	} else if (WasWakeAlarmTriggered()) {								// This needs to be called about once a second so that we don't skip
																		// over the second(s) that trigger the wake alarm.
		return(EVENT_WAKE_TRIGGERED);
	}
	//} else if (WasSmokeAlarmTriggered()) {							// Check if the smoke event was triggered.
	//    g_state.event = EVENT_SMOKE_DETECTED;
	//} else if (WasMotionAlarmTriggered()) {							// Check if the motion event was triggered.
	//    g_state.event = EVENT_MOTION_DETECTED;
	//}
	return(g_state.event);
}

void ProcessEvent(byte event)											// called continuously from main; processes events
{
    //debug("ProcessEvent()");
    //debugNoLF("event:"); debugInt(event);
    //debugNoLF("g_state.mode:"); debugInt(g_state.mode);
    if (event == EVENT_NONE) {
        if (g_state.mode & SHOWING_TIME) {
            ToggleColon();
        }
        return;
	} else if (event == EVENT_SECOND_CHANGED) {
		LCD_DisplaySeconds(g_state.seconds);
    } else if (event == EVENT_MINUTE_CHANGED) {							// set by Timer1 RTC
        if (g_state.mode & SHOWING_TIME) {
            ShowTime(g_state.hours, g_state.minutes);
        }
		LCD_DisplayMinutes(g_state.minutes);
		LCD_DisplayTemp(ConvertToFarenheit(I2C_ReadRegister(TC74_ADDRESS_R, TC74_TEMP_REGISTER)));
	} else if (event == EVENT_HOUR_CHANGED) {
		LCD_DisplayHours(g_state.hours);
	} else if (event == EVENT_DAY_CHANGED) {							// change the date displayed
		LCD_DisplayDate(g_state.years, g_state.months,  g_state.day,g_state.dayOfWeek);  
    } else if (event == EVENT_PRESS_STATE) {							// if the STATE (left) button was pressed 
        if ( (g_state.mode & SHOWING_TIME) &&							// If the clock is showing time and is NOT sounding the alarm, put it into set wake hour mode 
            !(g_state.mode & BUZZING) ) {
            ClearBit(g_state.mode, SHOWING_TIME);						// Turn off showing time
            SetBit(g_state.mode, SETTING_WAKE_HOUR);					// Turn on setting wake hour
            SS_DisplayTimeAlarmSetHours(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_HOUR) {					// If the clock is setting the wake hour, put it into setting wake minute mode 
            ClearBit(g_state.mode, SETTING_WAKE_HOUR);
            SetBit(g_state.mode, SETTING_WAKE_MINUTE);
            SS_DisplayTimeAlarmSetMinutes(g_state.alarmMinutes);
		} else if (g_state.mode & SETTING_WAKE_MINUTE) {
			ClearBit(g_state.mode, SETTING_WAKE_MINUTE);
			SetBit(g_state.mode, SETTING_TIME_YEAR);	
            EEPROM_WriteAlarmTime(g_state.alarmHours, g_state.alarmMinutes);	// save wake time
			SS_DisplayTimeSetYears(g_state.years);
		} else if (g_state.mode & SETTING_TIME_YEAR) {					// move from setting year to setting month
			ClearBit(g_state.mode, SETTING_TIME_YEAR);					// clear setting year
			SetBit(g_state.mode, SETTING_TIME_MONTH);					// turn on setting month of year
			SS_DisplayTimeSetMonths(g_state.months);					// Prepare the SS display to set time month of year
		} else if (g_state.mode & SETTING_TIME_MONTH) {					// move from setting month to setting day
			ClearBit(g_state.mode, SETTING_TIME_MONTH);					// Clear setting time month of year
			SetBit(g_state.mode, SETTING_TIME_DAY);						// Turn on setting time day of month
			SS_DisplayTimeSetDays(g_state.day);							// Prepare the SS display to set time day of month
		} else if (g_state.mode & SETTING_TIME_DAY) {					// move from setting day into setting week
			ClearState(SETTING_TIME_DAY);
			SetState(SETTING_TIME_DAY_OF_WEEK);
			SS_DisplayTimeSetDayOfWeek(g_state.dayOfWeek);				// Prepare SS display to set time day of week
        } else if (g_state.mode & SETTING_TIME_DAY_OF_WEEK) {			// If clock is setting time day set the mode to setting time hour 
            ClearBit(g_state.mode, SETTING_TIME_DAY_OF_WEEK);			// Turn off setting day of week
            SetBit(g_state.mode, SETTING_TIME_HOUR);					// Turn on setting time hour
			DS1307_WriteDate(g_state.years,g_state.months,g_state.day,g_state.dayOfWeek);	// Write date to DS1307 TODO: fix defect where behavior is undefined here
			LCD_DisplayDate(g_state.years, g_state.months, g_state.day, g_state.dayOfWeek);// Display date on LCD
            SS_DisplayTimeSetHours(g_state.hours);						// Prepare the SS display to set time hours
        } else if (g_state.mode & SETTING_TIME_HOUR) {					// If clock is setting time hour set the mode to setting time minute 
            LCD_DisplayHours(g_state.hours);							// Make sure the LCD shows the correct hours
            ClearBit(g_state.mode, SETTING_TIME_HOUR);					// Turn off setting time hour
            SetBit(g_state.mode, SETTING_TIME_MINUTE);					// Turn on setting time minute
            SS_DisplayTimeSetMinutes(g_state.minutes);					// Prepare to set minutes
			
			// TODO: add setting time seconds

        } else if (g_state.mode & SETTING_TIME_MINUTE) {				// If the clock is setting time minute set the mode to showing time 
            DS1307_WriteTime(g_state.hours, g_state.minutes, g_state.seconds);	// save the new time to the DS1307
            ClearBit(g_state.mode, SETTING_TIME_MINUTE);
            SetBit(g_state.mode, SETTING_MIL_TIME_SWITCH);				// stop showing minute
            LCD_DisplayMinutes(g_state.minutes);						// Make sure the LCD reflects the correct minutes
			SS_DisplayTimeSetMilitaryTime(g_state.militaryTime);		// Turn on setting military time display
		} else if (g_state.mode & SETTING_MIL_TIME_SWITCH) {
			EEPROM_WriteMilitaryTimeSwitch(g_state.militaryTime);		// write the military time switch setting to eeprom
            ClearBit(g_state.mode, SETTING_MIL_TIME_SWITCH);			// stop showing military time set interface
            SetBit(g_state.mode, SHOWING_TIME);							// back to showing the time
            ShowTime(g_state.hours, g_state.minutes);					// show hours and minutes on the SS display
        } else if (g_state.mode & SHOWING_TEMP) {						// if showing the temperature set the mode to show the time 
            ClearBit(g_state.mode, SHOWING_TEMP);						// turn off showing temp
            SetBit(g_state.mode, SHOWING_TIME);							// go back to showing time
            ShowTime(g_state.hours, g_state.minutes);					// Display hours and minutes on the SS display
        } else if (g_state.mode & SHOWING_CO){							// if showing CO alert set mode to show time and silence sounding alarm 
            ClearBit(g_state.mode, SHOWING_CO);							// if the CO alarm went off clear the SS display and show hours and minutes
            SetBit(g_state.mode, SHOWING_TIME);							// set the mode to showing time
            g_state.COreading = 0;										// so that the alarm doesn't keep sounding 
																		// after the button is pressed
            //SilenceAlarm();											// silence
            ShowTime(g_state.hours, g_state.minutes);					// shows time as of CO alarm trigger
		}
																		// No matter which mode the clock is in, if the state button is pressed         
        if (g_state.mode & BUZZING) {									// silence the alarm if it's sounding 
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode.
            SilenceAlarm();
        }
    } else if (event == EVENT_PRESS_ACTION) {							// if the ACTION (right) button was pressed 
        if ((g_state.mode & SHOWING_TIME) &&							// if time is showing and it's not buzzing, show the temperature 
            !(g_state.mode & BUZZING)) {
            //debug("SHOW THE TEMP");
            ClearBit(g_state.mode, SHOWING_TIME);						// no longer showing time
            SetBit(g_state.mode, SHOWING_TEMP);							// show the temperature
            signed int farenheit = ConvertToFarenheit(I2C_ReadRegister(TC74_ADDRESS_R, TC74_TEMP_REGISTER));
            //debugMetric("FARENHEIT IN PROCESSEVENT",farenheit);
            ShowTemp(farenheit);
			int internalTemp = (int) GetInternalTemp();
			debugMetric("INTERNAL TEMPERATURE", internalTemp);
        }
        if (g_state.mode & SETTING_WAKE_HOUR) {							// if setting wake hour, increment the hour
			if (++g_state.alarmHours > 23) g_state.alarmHours = 0;
            SS_DisplayTimeAlarmSetHours(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {				// else if setting wake minute, increment minute 
			if (++g_state.alarmMinutes > 59) g_state.alarmMinutes = 0;
            SS_DisplayTimeAlarmSetMinutes(g_state.alarmMinutes);
		} else if (g_state.mode & SETTING_TIME_YEAR) {
			if (++g_state.years > 99) g_state.years = 0;
			SS_DisplayTimeSetYears(g_state.years);
		} else if (g_state.mode & SETTING_TIME_MONTH) {
			if (++g_state.months > 12) g_state.months = 1;				// TODO: make all cases use this format to save lines of code
			SS_DisplayTimeSetMonths(g_state.months);
		} else if (g_state.mode & SETTING_TIME_DAY) {
			if (++g_state.day > 31) g_state.day = 1;					//TODO: make this sensitive to the month that's set
			SS_DisplayTimeSetDays(g_state.day);
		} else if (g_state.mode & SETTING_TIME_DAY_OF_WEEK) {
			if (++g_state.dayOfWeek > 7) g_state.dayOfWeek = 1;
			SS_DisplayTimeSetDayOfWeek(g_state.dayOfWeek);
        } else if (g_state.mode & SETTING_TIME_HOUR) {					// if setting time hour, increment hour
			if(++g_state.hours > 23) g_state.hours = 0;
			SS_DisplayTimeSetHours(g_state.hours);
        } else if (g_state.mode & SETTING_TIME_MINUTE) {				// if setting time minute, increment minute
			if (++g_state.minutes > 59) g_state.minutes = 0;
			SS_DisplayTimeSetMinutes(g_state.minutes);
		} else if (g_state.mode & SETTING_MIL_TIME_SWITCH) {
			if (++g_state.militaryTime > 1) g_state.militaryTime = 0;
			SS_DisplayTimeSetMilitaryTime(g_state.militaryTime);
        } else if (g_state.mode & SHOWING_CO) {
            ShowTime(g_state.hours, g_state.minutes);					// shows time as of CO alarm trigger
            msDelay(2000);												// wait for two seconds
            DisplayWarnForCO();											// back to showing CO  
        } else if (g_state.mode & BUZZING) {							// If the alarm is sounding, change the clock mode to snoozing 
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
            SetBit(g_state.mode, SNOOZING); 							// add snoozing
            SnoozeAlarm(&g_state);
        }
    } else if (event == EVENT_WAKE_TRIGGERED) {							// if it's time to sound the alarm to wake up; sound the alarm if the switch is on  
        if (g_state.mode & WAKE) {          							// if the wake alarm is enabled
            SetBit(g_state.mode, BUZZING);  							// set the mode to buzzer on
			//debug("WAKE DETECTED.");
            SoundWake();                   								// play a song using UM66 
        }
    } else if (event == EVENT_CO_DETECTED) {
        if (g_state.mode & WAKE) {          							// if the wake alarm is enabled
            SetBit(g_state.mode, BUZZING);
            ClearBit(g_state.mode, SHOWING_TIME);
            SetBit(g_state.mode, SHOWING_CO);
			//debug("CO DETECTED");
            SoundAlarm();
            DisplayWarnForCO();											// todo: make this flash
        }
    } else if (event == EVENT_SWITCH_OFF) {
        ClearBit(g_state.mode, WAKE);       							// set the alarm so it will not sound
		g_state.decimalBlink = 0;										// turn off the blinking decimal
        if (g_state.mode & BUZZING) {       							// if the alarm is currently sounding
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
            SilenceAlarm();                 							// turn of the sounding alarm (both song and siren)
        }
        ColonOff();														// turn off the colon
   } else if (event == EVENT_SWITCH_ON) {
        SetBit(g_state.mode, WAKE);         							// turn on the alarm so it will 
																		// sound when necessary
		g_state.decimalBlink = 1;										// turn on the blinking decimal in the SS display
        ColonOn();														// turn on the colon
   } else if (event == EVENT_CO_ON) {
		PORTB |= (1<<CO_POWER_PIN);										// set CO_POWER_PIN as output
		initTimer2();
   } else if (event == EVENT_CO_OFF) {
		offTimer2();
		PORTB &= ~(1<<CO_POWER_PIN);
        if (g_state.mode & BUZZING) {       							// if the alarm is currently sounding
	        ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
	        SilenceAlarm();                 							// turn of the sounding alarm (both song and siren)
        }
   } else if (event == EVENT_SEARCH_UP) {
		tune += 50;
		if (tune <= 108000UL)
		{
			TEA5767_tune(tune);											// TODO: figure out if this is synchronous
			TEA5767_write();
		}
		else
		{
			tune = 108000UL;
		}
			   
   } else if (event == EVENT_SEARCH_DOWN) {
		tune -= 50;
		if (tune >= 87500UL)
		{
			TEA5767_tune(tune);
			TEA5767_write();
		}
		else
		{
			tune = 87500UL;
		}
   }
}