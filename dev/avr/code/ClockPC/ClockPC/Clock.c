﻿//	Clock.c
/*
	A clock with the following features:
	- AtMega328P microcontroller running at 8mHz
	- Custom I2C seven segment display using discrete components
	- I2C LCD display to show date, time, temperature
	- beeping CO alarm using passive piezo buzzer and two 555 timers
	- I2C DS1307 Real Time Clock with battery backup
	- Wake alarm sound using UM66 Melody Generator
	- MQ-7 CO detector operating at two voltages; 1.4v & 5.0v.
	- night light via pulse width modulation
	- I2C temperature using TC74A0
	- I2C FM Radio via TEA5767

	All three timers are used; one for an internal RTC, one for
	to control the voltage to the CO sensor, and one to set
	the brightness of the LED/night light.
  
	Beware of turning on too much debugging, it's easy to use all 
	of the data memory.

 Author:		Copyright ©2018, Daniel Murphy <dan-murphy@comcast.net>
 Contributors:	Bruce E. Hall <bhall66@gmail.com>
				Elliott Williams (Make: AVR Programming)   
				Peter Fleury (LCD library [lcdpcf8574])
				TEA5767: https://www.mikrocontroller.net/attachment/276392/Atmega_16_tea5767_c.pdf 
				Source code has been pulled from all over the internet,
				it would be impossible for me to cite all contributors.
 Version:		0.7
 Date:			2015-09-09 - 2017-11-2
 Device:		ATMega328P-PU @ 8mHz
 Language:		C
*/
//	License
/*  Daniel J. Murphy hereby disclaims all copyright interest in this
	program written by Daniel J. Murphy.

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//	Known Defects
/*	
	-   Time on the DS1307 is not always advancing.  As of 2018-02-17 it seems to be working.
	-	Timer for the night light doesn't work
	-	The wake time is not being saved between resets
	-	The decimal blinks when the alarm is on (good), but it doesn't start 
		blinking until the next minute occurs.
	-	The radio station is not saved in EEPROM.
	-	CO reading is not displaying on the LCD when CO alarm is triggered.
*/
// TODO
/*
	-	Instead of stepping by .05 mHz per button press, search for the next station.
	-	Change out the temp sensor for something more accurate.
	-	Implement a watchdog timer.
	-	Sleep to save energy.
	-	On the LCD indicate that the alarm is set.
	-	Add a smoke alarm.
	-	Add a motion alarm.
	-	Let the user set minutest to snooze.
*/
/************************************************************************/
/* Includes                                                             */
/************************************************************************/
#include "Clock.h"

/************************************************************************/
/* Debug Routines                                                       */
/************************************************************************/
void debug(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
        printString("\r\n");
    }
}

void debugNoLF(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
    }
}

void debugInt(signed int anInt) {
    if (DEBUG) {
        char myInt[4];
        itoa(anInt,myInt,10);
        debug(myInt);
    }
}

void debugMetric(const char myString[], signed int anInt) {
	debugNoLF(myString);debugNoLF(": ");
	debugInt(anInt);
    printString("\r\n");
}

void debugAlarmTime() {
	char myHours[4];
	itoa(g_state.alarmHours,myHours,10);
	debugNoLF("ALARM HOURS: ");
	debug(myHours);

	char myMinutes[4];
	itoa(g_state.alarmMinutes,myMinutes,10);
	debugNoLF("ALARM MINUTES: ");
	debug(myMinutes);
}

void debugTime() {
	debugMetric("Hours", g_state.hours);
	debugMetric("Minutes", g_state.minutes);
	debugMetric("Seconds", g_state.seconds);
}

/************************************************************************/
/* I2C Routines                                                         */
/************************************************************************/
void I2C_Init()															// Calculate TWBR setting for I2C bit rate
{																		// at 16 , the SCL frequency  will be 16/(16+2(TWBR)), assuming 
																		// prescalar of 0. So for 100KHz SCL, 
																		// TWBR = ((F_CPU/F_SCL)-16)/2 = ((16/0.1)-16)/2 = 144/2 
																		// = 72.
    debug("I2C_INIT()");
    TWSR = 0;															// set prescalar to zero
    TWBR = ((F_CPU/F_SCL)-16)/2;										// set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)												// look for a device at the specified address
{																		// return 1=found, 0=not found
    TWCR = TW_START_OTHER_I2C;													// send start condition
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
    }																	// wait
    TWDR = addr;														// load device's bus address
    TWCR = TW_SEND;														// and send it
    i = 0;
    while ((!TW_READY) && (i++ < 101)) {}								// wait
    if (TW_STATUS_OTHER_I2C!=0x18) {									// ??
    }
    return (TW_STATUS_OTHER_I2C==0x18);									// return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)											// returns the count of I2C devices found
{   byte foundCount;
    foundCount = 0;
    for (byte addr=start;addr<0xFF;addr++)								// search all 256 addresses
    {
        if (I2C_Detect(addr)) {											// I2C detected?
            foundCount++;
            debugNoLF("I2C ADDR FOUND: ");
            debugInt(addr);       
        }
    }
    return foundCount;													// none detected, so return 0.
}

void I2C_Start (byte slaveAddr)											// Look for a device at the specified address.  Same as I2C_Detect.
{   I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)												// sends a data byte to slave
{   TWDR = data;														// load data to be sent
    TWCR = TW_SEND;														// and send it
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
    }
    return (TW_STATUS_OTHER_I2C!=0x28);
}

byte I2C_ReadACK ()														// reads a data byte from slave
{   TWCR = TW_ACK;														// ack = will read more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString("+");
    }																	// wait
    return TWDR;
}

byte I2C_ReadNACK () 													// reads a data byte from slave
{   TWCR = TW_NACK;														// nack = not reading more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString(".");
    }																	// wait
    printString("\r\n");
    return TWDR;
}

void I2C_WriteByte(byte busAddr, byte data)								// Writes a byte of data to the I2C slave
{   I2C_Start(busAddr);													// send bus address
    I2C_Write(data);													// then send the data byte
    I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)	// Writes a byte of data to the device register
{   I2C_Start(busAddr);													// send bus address
    I2C_Write(deviceRegister);											// first byte = device register address
    I2C_Write(data);													// second byte = data for device register
    I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister)				// Reads a byte of data from the device register	
{   byte data = 0;
    I2C_Start(busAddr);													// send device address
    I2C_Write(deviceRegister);											// set register pointer
    I2C_Start(busAddr+READ);											// restart as a read operation
    data = I2C_ReadNACK();												// read the register data
    I2C_Stop();															// stop
    return data;
}

void I2C_FindDevices()													// Makes attempts to find I2C devices
{	int foundCount;
	int findI2CTries;

	foundCount = 0;
	findI2CTries = 0;
	debugMetric("FIND DEVICE ATTEMPT #", findI2CTries);
	foundCount = I2C_FindDevice (0x00);
	while (foundCount < NUM_I2C_DEVICES)								// 32 - SS display, 78 - LCD, 144 - temp, 192 - radio, 208 - RTC
	{
		findI2CTries += 1;
		debugMetric("FIND DEVICE ATTEMPT #", findI2CTries);
		foundCount = I2C_FindDevice (0x00);
		msDelay(250);
	}
}

/************************************************************************/
/* Custom Seven Segment Display Routines                                */
/************************************************************************/
void SS_DisplayChars(byte charOne, byte charTwo, byte charThree, byte charFour) // Write 4 chars to the SS display
{   I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    byte instruction = SS_SetDisplayInstruction();
    I2C_Write(instruction);												// first byte = device register address
    I2C_Write(ssCharMap[charOne]);										// hours tens pos
    I2C_Write(ssCharMap[charTwo]);										// hours ones pos
    I2C_Write(ssCharMap[charThree]);									// minute tens pos
    I2C_Write(ssCharMap[charFour]);										// minute ones pos
    I2C_Stop();
}

byte SS_SetDisplayInstruction()											// set the SS display I2C instruction byte for decimal and military time
{   byte instruction = 0;
    if (g_state.colonOn) {
        SetBitNo(instruction, DECIMAL_ON);
	}
    
    if (g_state.decimalBlink) {
        SetBitNo(instruction, DECIMAL_BLINK);
	}
    return instruction;     
}

void SS_DisplayTime(byte hours, byte minutes)							// Write hours and minutes to the SS display
{	//debug("SS_DisplayTime()");
	
	if (!g_state.militaryTime) {										// if we're not showing military time, 
		if (hours > 12) hours -= 12;									// subtract 12 from hours
	}
	
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;
    
    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    
    byte instruction = SS_SetDisplayInstruction();
        
    I2C_Write(instruction);												// first byte = instruction settings
    if (tensHours) {
        I2C_Write(ssCharMap[tensHours]);								// hours tens pos
    } else {
        I2C_Write(ssCharMap[CHAR_SPACE]);								// hours tens pos
    }
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[tensMinutes]);									// minute tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minute ones pos
    I2C_Stop();
}

void SS_DisplayTimeAlarmSetHours(byte hours)							// Display 'set alarm hours' to SS display
{   int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[tensHours]);									// hours tens pos
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_H]);      
    I2C_Stop();
}

void SS_DisplayTimeAlarmSetMinutes(byte minutes)						// Display 'set alarm minutes' to SS display 
{   int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_SPACE]);      
    I2C_Write(ssCharMap[tensMinutes]);									// minutes tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minutes ones pos
    I2C_Stop();
}

void SS_DisplayTimeSetDays(byte days)									// Display 'set day of month' to SS display
{	int tensDays = days / 10;
	int onesDays = days - (days / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_D]);
	I2C_Write(ssCharMap[CHAR_A]);
	I2C_Write(ssCharMap[tensDays]);										// days tens pos
	I2C_Write(ssCharMap[onesDays]);										// days ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetDayOfWeek(byte dayOfWeek)							// Display 'set day of week' to SS display
{	int tensDayOfWeek = dayOfWeek / 10;
	int onesDayOfWeek = dayOfWeek - (dayOfWeek / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_D]);
	I2C_Write(ssCharMap[CHAR_W]);
	I2C_Write(ssCharMap[tensDayOfWeek]);								// day of week tens pos
	I2C_Write(ssCharMap[onesDayOfWeek]);								// day of week ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetMonths(byte months)								// Display 'set month of year' to SS display
{	int tensMonths = months / 10;
	int onesMonths = months - (months / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_M]);
	I2C_Write(ssCharMap[CHAR_O]);
	I2C_Write(ssCharMap[tensMonths]);									// months tens pos
	I2C_Write(ssCharMap[onesMonths]);									// months ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetYears(byte years)									// Display 'set year' to SS display
{	years -= 2000;
	int tensYears = years / 10;
	int onesYears = years - (years / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[CHAR_Y]);
	I2C_Write(ssCharMap[CHAR_R]);
	I2C_Write(ssCharMap[tensYears]);									// years tens pos
	I2C_Write(ssCharMap[onesYears]);									// years ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetHours(byte hours)									// Display 'set time hours' to SS display
{	int tensHours = hours / 10;
	int onesHours = hours - (hours / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);
	I2C_Write(ssCharMap[tensHours]);									// hours tens pos
	I2C_Write(ssCharMap[onesHours]);									// hours ones pos
	I2C_Write(ssCharMap[CHAR_S]);
	I2C_Write(ssCharMap[CHAR_H]);
	I2C_Stop();
}

void SS_DisplayTimeSetMinutes(byte minutes)								// Display 'set time minutes' to SS display
{	int tensMinutes = minutes / 10;
	int onesMinutes = minutes - (minutes / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);														// colon off during set time
	I2C_Write(ssCharMap[CHAR_S]);
	I2C_Write(ssCharMap[CHAR_SPACE]);
	I2C_Write(ssCharMap[tensMinutes]);									// minutes tens pos
	I2C_Write(ssCharMap[onesMinutes]);									// minutes ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetSeconds(byte seconds)								// Display 'set time seconds' to SS display
{	int tensSeconds = seconds / 10;
	int onesSeconds = seconds - (seconds / 10) * 10;

	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);														// colon off during set time
	I2C_Write(ssCharMap[CHAR_S]);
	I2C_Write(ssCharMap[CHAR_E]);
	I2C_Write(ssCharMap[tensSeconds]);									// seconds tens pos
	I2C_Write(ssCharMap[onesSeconds]);									// seconds ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetMilitaryTime(byte milTimeSwitch)					// Display 'set military time' to SS display
{	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);														// colon off during set time
	I2C_Write(ssCharMap[2]);
	I2C_Write(ssCharMap[4]);
	I2C_Write(ssCharMap[CHAR_SPACE]);									// minutes tens pos
	I2C_Write(ssCharMap[milTimeSwitch]);								// minutes ones pos
	I2C_Stop();
}

void SS_DisplayTimeSetWakeToRadio(byte wakeToRadio)						// Display 'set wake to radio' to SS display
{	I2C_Start(CUST_SS_LED_ADDR);										// send bus address
	I2C_Write(0);														// colon off during set time
	I2C_Write(CHAR_W);													// wake
	I2C_Write(CHAR_R);													// radio
	I2C_Write(ssCharMap[CHAR_SPACE]);									// minutes tens pos
	I2C_Write(ssCharMap[wakeToRadio]);									// minutes ones pos
	I2C_Stop();
}

void SS_BlankDisplay()													// Blank out the SS display
{	SS_DisplayChars(CHAR_SPACE,CHAR_SPACE,CHAR_SPACE,CHAR_SPACE);
}

void ToggleColon(uint32_t mode)											// Toggle the SS display colon
{	
	if (mode & WAKE) {
		if (g_state.colonOn) {
			ColonOff();
		} else {
			ColonOn();
		}
	}
}

void ColonOn()															// Set the SS display colon on
{	g_state.colonOn = TRUE;
}

void ColonOff()															// Set the SS display colon off
{	g_state.colonOn = FALSE;
}

/************************************************************************/
/* Custom Liquid Crystal Display Routines                               */
/************************************************************************/
void LCD_Reset()														// Clear out the LCD display
{	lcd_led(0);
	lcd_home();
	lcd_clrscr();
	lcd_gotoxy(0,0);
}

void LCD_DisplayDateAndTemp(uint16_t years, byte months, byte day, 
							byte dayOfWeek, byte hours, byte minutes, 
							byte seconds, byte tempF			) {
	LCD_Reset();
	LCD_DisplayDate(years, months, day, dayOfWeek);
	LCD_DisplayHours(hours);
	LCD_DisplayMinutes(minutes);
	LCD_DisplaySeconds(seconds);
	LCD_DisplayTemp(tempF);
}

void LCD_DisplayTimeAndTemp(byte hours, byte minutes, byte seconds, byte tempF)
{
	LCD_DisplayHours(hours);
	lcd_gotoxy(COLON_1_X,COLON_1_Y);
	lcd_putc(':');
	LCD_DisplayMinutes(minutes);
	lcd_gotoxy(COLON_2_X,COLON_2_Y);
	lcd_putc(':');
	LCD_DisplaySeconds(seconds);
	LCD_DisplayTemp(tempF);
}

void LCD_DisplayTune(uint32_t tune) {
	char buf[10];

	tune = tune / 100;
	byte hundredsTune = tune / 1000;
	byte tensTune = (tune - (tune / 1000 * 1000)) / 100;
	byte onesTune = (tune - (tune / 100 * 100)) /10;
	byte tenthsTunes = tune - ((tune / 10) * 10);

	LCD_Reset();
	lcd_gotoxy(TUNE_X,TUNE_Y);
	if (hundredsTune != 0 ) {
		itoa(hundredsTune, buf, 10);
		lcd_putc(*buf);
	} else {
		lcd_putc(' ');
	}
	itoa(tensTune, buf, 10);
	lcd_putc(*buf);
	itoa(onesTune, buf, 10);
	lcd_putc(*buf);
	lcd_putc('.');
	itoa(tenthsTunes, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTemp(byte tempF)										// Display current temperature in Fahrenheit on LCD
{	//debug("LCD_DisplayTemp()");
	char buf[10];
	byte ones = 0, tens = 0, hundreds = 0;
	lcd_gotoxy(TEMP_X,TEMP_Y);
    if (tempF > MAX_TEMP || MIN_TEMP < 0) {
		lcd_putc('E');
		lcd_gotoxy(TEMP_X + 1,TEMP_Y);
		lcd_putc('R');
		lcd_gotoxy(TEMP_X + 2,TEMP_Y);
		lcd_putc('R');
	} else {
		tens = tempF / 10;
		hundreds = tempF / 100;
		ones = (tempF - (hundreds * 100)) - (tens * 10);				// subtract out the hundreds and tens portion of the temp
	}
	if (!(hundreds > 0)) {
		lcd_putc(' ');
	} else {
		itoa(hundreds, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(TEMP_X + 1,TEMP_Y);
	if (!(tens > 0)) {
		lcd_putc(' ');
	} else {
		itoa(tens, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(TEMP_X + 2,TEMP_Y);
	itoa(ones, buf, 10);
	lcd_putc(*buf);
	lcd_gotoxy(TEMP_X + 3,TEMP_Y);
	lcd_putc('F');
}

void LCD_DisplayCO(doubleByte COreading) 								// Display the CO reading on the LCD
{	//debug("LCD_DisplayCO()");
	char buf[10];
	byte ones = 0, tens = 0, hundreds = 0, thousands = 0;
	lcd_gotoxy(CO_X,CO_Y);
	if (COreading > MAX_CO || MIN_CO < 0) {
		lcd_gotoxy(CO_X,CO_Y);
		lcd_putc('C');
		lcd_gotoxy(CO_X + 1,CO_Y);
		lcd_putc('O');
		lcd_gotoxy(CO_X + 2,CO_Y);
		lcd_putc(' ');
		lcd_gotoxy(CO_X + 3,CO_Y);
		lcd_putc('E');
		lcd_gotoxy(CO_X + 4,CO_Y);
		lcd_putc('R');
		lcd_gotoxy(CO_X + 5,CO_Y);
		lcd_putc('R');
		} else {
		tens = COreading / 10;
		hundreds = COreading / 100;
		thousands = COreading / 1000;
		ones = (COreading - (thousands * 1000) - (COreading - (hundreds * 100))) - (tens * 10);
	}
	lcd_gotoxy(CO_X,CO_Y);
	lcd_putc('C');
	lcd_gotoxy(CO_X + 1,CO_Y);
	lcd_putc('O');
	lcd_gotoxy(CO_X + 2,CO_Y);
	lcd_putc('=');
	lcd_gotoxy(CO_X + 3,CO_Y);
	if (!(thousands > 0)) {
		lcd_putc(' ');
		} else {
		itoa(thousands, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(CO_X + 4,CO_Y);
	if (!(hundreds > 0)) {
		lcd_putc(' ');
		} else {
		itoa(hundreds, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(CO_X + 6,CO_Y);
	if (!(tens > 0)) {
		lcd_putc(' ');
		} else {
		itoa(tens, buf, 10);
		lcd_putc(*buf);
	}
	lcd_gotoxy(CO_X + 6,CO_Y);
	if (!(ones > 0)) {
		lcd_putc(' ');
		} else {
		itoa(ones, buf, 10);
		lcd_putc(*buf);
	}
}

void LCD_DisplayHours(byte hours)										// Display hours portion of the time
{	char buf[10];

	int tensHours = hours / 10;
	int onesHours = hours - (hours / 10) * 10;

	itoa(tensHours, buf, 10);
	lcd_gotoxy(HOUR_TENS_X,HOUR_TENS_Y);
	lcd_putc(*buf);

	itoa(onesHours, buf, 10);
	lcd_gotoxy(HOUR_ONES_X,HOUR_ONES_Y);
	lcd_putc(*buf);
}

void LCD_DisplayMinutes(byte minutes)									// Display the minutes portion of the time
{	char buf[10];

	int tensMinutes = minutes / 10;
	int onesMinutes = minutes - (minutes / 10) * 10;
	
	itoa(tensMinutes, buf, 10);
	lcd_gotoxy(MINUTE_TENS_X,MINUTE_TENS_Y);
	lcd_putc(*buf);

	itoa(onesMinutes, buf, 10);
	lcd_gotoxy(MINUTE_ONES_X,MINUTE_ONES_Y);
	lcd_putc(*buf);	
}

void LCD_DisplaySeconds(byte seconds)									// Display the seconds portion of the time
{	char buf[10];

	int tensSeconds = seconds / 10;
	int onesSeconds = seconds - (seconds / 10) * 10;

	itoa(tensSeconds, buf, 10);
	lcd_gotoxy(SECOND_TENS_X,SECOND_TENS_Y);
	lcd_putc(*buf);

	itoa(onesSeconds, buf, 10);
	lcd_gotoxy(SECOND_ONES_X,SECOND_ONES_Y);
	lcd_putc(*buf);
}
																		// Display date on the LCD
void LCD_DisplayDate(uint16_t years, byte months, byte day, byte dayOfWeek) 
{	char buf[10];

	LCD_Reset();
	lcd_gotoxy(COLON_1_X,COLON_1_Y);
	lcd_putc(':');

	lcd_gotoxy(COLON_2_X,COLON_2_Y);
	lcd_putc(':');

	int thouYears  =    years / 1000;
	int	hundYears  =   (years - (thouYears * 1000)) / 100;
	int tensYears  =  ((years - (thouYears * 1000)) - (hundYears * 100)) / 10;
	int onesYears  = (((years - (thouYears * 1000)) - (hundYears * 100)) - tensYears * 10);
	int tensMonths = months / 10;
	int onesMonths = months - (months / 10) * 10;
	int tensDay   = day / 10;
	int onesDay   = day - (day / 10) * 10;

	itoa(thouYears, buf, 10);
	lcd_gotoxy(YEAR_THOU_X,YEAR_THOU_Y);
	lcd_putc(*buf);

	itoa(hundYears, buf, 10);
	lcd_gotoxy(YEAR_HUND_X,YEAR_HUND_Y);
	lcd_putc(*buf);

	itoa(tensYears, buf, 10);
	lcd_gotoxy(YEAR_TENS_X,YEAR_TENS_Y);
	lcd_putc(*buf);

	itoa(onesYears, buf, 10);
	lcd_gotoxy(YEAR_ONES_X,YEAR_ONES_Y);
	lcd_putc(*buf);

	lcd_gotoxy(FORWARD_SLASH_1_X, FORWARD_SLASH_1_Y);
	lcd_putc('/');

	itoa(tensMonths, buf, 10);
	lcd_gotoxy(MONTH_TENS_X,MONTH_TENS_Y);
	lcd_putc(*buf);

	itoa(onesMonths, buf, 10);
	lcd_gotoxy(MONTH_ONES_X,MONTH_ONES_Y);
	lcd_putc(*buf);

	lcd_gotoxy(FORWARD_SLASH_2_X, FORWARD_SLASH_2_Y);
	lcd_putc('/');

	itoa(tensDay, buf, 10);
	lcd_gotoxy(DAY_TENS_X,DAY_TENS_Y);
	lcd_putc(*buf);

	itoa(onesDay, buf, 10);
	lcd_gotoxy(DAY_ONES_X,DAY_ONES_Y);
	lcd_putc(*buf);
	
	byte x = DAY_OF_WEEK_X;
	lcd_gotoxy(x, DAY_OF_WEEK_Y);
	const char *dayChars = DayOfWeekNumToString(dayOfWeek);
	int i = 0;
	while (dayChars[i] != '\0') {
		lcd_putc(dayChars[i++]);
		lcd_gotoxy(++x,DAY_OF_WEEK_Y);		
	}
}

void LCD_DisplayTimeAlarmSetHours(byte hours, byte showPrompt)			// Display hours portion of the time for setting
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Wake Hour: ");
	}
	lcd_gotoxy(11,0);
	
	int tensHours = hours / 10;
	int onesHours = hours - (hours / 10) * 10;

	itoa(tensHours, buf, 10);
	lcd_putc(*buf);

	itoa(onesHours, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeAlarmSetMinutes(byte minutes, byte showPrompt)		// Display hours portion of the time for setting
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Wake Minute: ");
	}
	lcd_gotoxy(13,0);
	
	int tensMinutes = minutes / 10;
	int onesMinutes = minutes - (minutes / 10) * 10;

	itoa(tensMinutes, buf, 10);
	lcd_putc(*buf);

	itoa(onesMinutes, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetYears(byte year, byte showPrompt)				// Display years portion of the time for setting
{	char buf[10];
	year -= 2000;
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Year: ");
	}
	lcd_gotoxy(6,0);
	
	int tensYear = year / 10;
	int onesYear = year - (year / 10) * 10;

	itoa(tensYear, buf, 10);
	lcd_putc(*buf);

	itoa(onesYear, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetMonths(byte month, byte showPrompt)
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Month: ");
	}
	lcd_gotoxy(7,0);
	
	int tensMonth = month / 10;
	int onesMonth = month - (month / 10) * 10;

	itoa(tensMonth, buf, 10);
	lcd_putc(*buf);

	itoa(onesMonth, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetDays(byte day, byte showPrompt)
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Day: ");
	}
	lcd_gotoxy(5,0);
	
	int tensDay = day / 10;
	int onesDay = day - (day / 10) * 10;

	itoa(tensDay, buf, 10);
	lcd_putc(*buf);

	itoa(onesDay, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetDayOfWeek(byte dayOfWeek, byte showPrompt)
{
//	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Day of Week: ");
	}
	lcd_gotoxy(12,0);
	byte x = 0;
	const char *dayChars = DayOfWeekNumToString(dayOfWeek);
	int i = 0;
	while (dayChars[i] != '\0') {
		lcd_putc(dayChars[i++]);
		lcd_gotoxy(++x + 12,0);
	}
	
}

void LCD_DisplayTimeSetHours(byte hour, byte showPrompt)
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Hour: ");
	}
	lcd_gotoxy(6,0);
	
	int tensHour = hour / 10;
	int onesHour = hour - (hour / 10) * 10;

	itoa(tensHour, buf, 10);
	lcd_putc(*buf);

	itoa(onesHour, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetMinutes(byte minute, byte showPrompt)
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Minute: ");
	}
	lcd_gotoxy(8,0);
	
	int tensMinute = minute / 10;
	int onesMinute = minute - (minute / 10) * 10;

	itoa(tensMinute, buf, 10);
	lcd_putc(*buf);

	itoa(onesMinute, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetSeconds(byte second, byte showPrompt)
{	char buf[10];
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Second: ");
	}
	lcd_gotoxy(8,0);
	
	int tensSecond = second / 10;
	int onesSecond = second - (second / 10) * 10;

	itoa(tensSecond, buf, 10);
	lcd_putc(*buf);

	itoa(onesSecond, buf, 10);
	lcd_putc(*buf);
}

void LCD_DisplayTimeSetMilitary(byte milTimeOn, byte showPrompt)
{	
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Mil Time: ");
	}
	lcd_gotoxy(10,0);

	
	if (milTimeOn) lcd_puts("On");
	if (!milTimeOn) lcd_puts("Off");
}

void LCD_DisplayTimeWakeToRadio(byte wakeToRadio, byte showPrompt)
{
	if (showPrompt) {
		LCD_Reset();
		lcd_gotoxy(0,0);
		lcd_puts("Radio Wake: ");
	}
	lcd_gotoxy(12,0);

	
	if (wakeToRadio) lcd_puts("Yes");
	if (!wakeToRadio) lcd_puts("No ");
}

/************************************************************************/
/* DS1307 Real Time Clock Interface                                     */
/************************************************************************/
void DS1307_ReadTime(byte *hours, byte *minutes, byte *seconds)			// returns hours, minutes, and seconds in BCD format
{   //debug("DS1307_ReadTime()");
    *hours = I2C_ReadRegister(DS1307_ADDRESS,HOURS_REGISTER);
    *minutes = I2C_ReadRegister(DS1307_ADDRESS,MINUTES_REGISTER);
    *seconds = I2C_ReadRegister(DS1307_ADDRESS,SECONDS_REGISTER);
	debugMetric("Read Seconds 1",*seconds);
    if (*hours & 0x40)	{												// 12hr mode: 0b01000000
        *hours &= 0x1F;													// use bottom 5 bits (pm bit = temp & 0x20) 0b00011111
    } else {
		*hours &= 0x3F;													// 24hr mode: use bottom 6 bits 0b00111111
	}
	*seconds &= 0b01111111;												// bit 7 is the clock halt bit so just use the bottom 7 bits for seconds
	debugMetric("Read Seconds 2",*seconds);
}
																		// returns months, days, and years in BCD format
void DS1307_ReadDate(byte *years, byte *months, byte *days, byte *dayOfWeek) 
{	//debug("DS1307_ReadDate");
	*years	=		I2C_ReadRegister(DS1307_ADDRESS,YEARS_REGISTER	);
	*months =		I2C_ReadRegister(DS1307_ADDRESS,MONTHS_REGISTER	);
    *days	=		I2C_ReadRegister(DS1307_ADDRESS,DAYS_REGISTER	);
	*dayOfWeek =	I2C_ReadRegister(DS1307_ADDRESS,DAYOFWK_REGISTER);
	//debugMetric("dayOfWeek", *dayOfWeek);
}

void DS1307_WriteTime(byte hours, byte minutes, byte seconds)			// Write time to the DS1307
{	//debug("DS1307_WriteTime()");										// There's a problem with this function; you can't save a time that's in military format and is > 12:59.
	I2C_WriteRegister(DS1307_ADDRESS,HOURS_REGISTER, DecToBcd(hours));
	I2C_WriteRegister(DS1307_ADDRESS,MINUTES_REGISTER, DecToBcd(minutes));
	DS1307_ClearClockHaltBit(seconds);									// To clear the clock halt bit and write seconds
}							

void DS1307_WriteDate(uint16_t years, byte months, byte days, byte dayOfWeek) // Write date to the DS1307
{	//debug("DS1307_WriteDate()");
	years -= 2000;
	//debugMetric("years", years);
	//debugMetric("months",  months);
	//debugMetric("days", days);
	//debugMetric("dayOfWeek", dayOfWeek);
	
	I2C_WriteRegister(DS1307_ADDRESS, YEARS_REGISTER	,DecToBcd(years		));
	I2C_WriteRegister(DS1307_ADDRESS, MONTHS_REGISTER	,DecToBcd(months	));
	I2C_WriteRegister(DS1307_ADDRESS, DAYS_REGISTER		,DecToBcd(days		));
	I2C_WriteRegister(DS1307_ADDRESS, DAYOFWK_REGISTER	,dayOfWeek			 );

}

void DS1307_ClearClockHaltBit(byte seconds)								// When clock halt bit is cleared (0) the oscillator is enabled, time always advances on the chip
{	byte clockHaltBit = 0b10000000;
	I2C_WriteRegister(DS1307_ADDRESS,SECONDS_REGISTER, DecToBcd(seconds) & ~(1 << clockHaltBit));
}

/************************************************************************/
/* TEA5767                                                              */
/************************************************************************/
int TEA5767_write(void)
{
	uint8_t ret = FALSE;
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);								// I2C start
	while (!(TWCR & (1<<TWINT)));           							// I2C wait
	
																		// check value of TWI Status Register. Mask prescaler bits.
	uint8_t twst = TW_STATUS & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START))
	{
		debugMetric("Write error @START, twst", twst);
		ret = TRUE;
	}
	
	TWDR = SLA_W;                            							// send address
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
	
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) )
	{
		debugMetric("Write error @ADDR, twst", twst);
		ret = TRUE;
	}
	
	for (uint8_t i = 0; i < 5; i++)										// send registers
	{
		TWDR = write_bytes[i];
		TWCR = (1<<TWINT) | (1<<TWEN);
		while (!(TWCR & (1<<TWINT)));
																		// check value of TWI Status Register. Mask prescaler bits
		twst = TW_STATUS & 0xF8;
		if( twst != TW_MT_DATA_ACK)
		{
			debugMetric("Write error, twst", twst);
			ret = TRUE;
			break;
		}
	}
	
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);								// I2C stop
																		// wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));
	return ret;
}

int TEA5767_read(void)													// Read data from TEA5767, put results in global variable static unsigned char read_bytes[5]
{
	uint8_t ret = FALSE;
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);								// I2C start
	while (!(TWCR & (1<<TWINT)));
	
	uint8_t twst = TW_STATUS & 0xF8;									// check value of TWI Status Register. Mask prescaler bits.
	if ( (twst != TW_START) && (twst != TW_REP_START))
	{
		debugMetric("Read error @START, twst", twst);
		ret = TRUE;
	}
	
	TWDR = SLA_R;                           							// send address
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (!(TWCR & (1<<TWINT)));
		
	twst = TW_STATUS & 0xF8;											// check value of TWI Status Register. Mask prescaler bits.
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) )
	{
		debugMetric("Read error @ADDR, twst", twst);
		ret = TRUE;
	}
	
	for (uint8_t i = 0; i < 5; i++)
	{
		if (i != 4)
			TWACK;
		else
			TWNACK;
		while (!(TWCR & (1<<TWINT)));
		read_bytes[i] = TWDR;											// read into global variable read_bytes
	}
	
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);								// I2C stop
																		
	while(TWCR & (1<<TWSTO));											// wait until stop condition is executed and bus released
	return ret;
}

int TEA5767_init(void)
{
	write_bytes[0] = 0b00101111;										// default: 99.9 MHz
	write_bytes[1] = 0x87;
	write_bytes[2] = TEA5767_SUD | TEA5767_SRCH_MID_LVL;
	//write_bytes[2] |= TEA5767_MONO;
	write_bytes[3] = TEA5767_XTAL;
	write_bytes[4] = 0b00000000;
	/** \todo chip detection/identification */
	EEPROM_ReadRadioStation(&g_state.tune);								// read the radio station from eeprom
	TEA5767_tune(g_state.tune);
	TEA5767_write();
	return 0;
}

void TEA5767_mute()
{
	//debug("Mute");
	write_bytes[0] |= TEA5767_MUTE;
	TEA5767_write();
}

void TEA5767_unmute()
{
	//debug("un-mute");
	write_bytes[0] &= ~TEA5767_MUTE;
	TEA5767_write();
}

void TEA5767_tune(uint32_t value)
{
	debugMetric("Tune", (unsigned int)(value/100));						// reference frequency = 32768 Hz
	uint16_t n = (uint32_t)4*(value*1000 - 225000) >> 15;				// low side injection formula
	write_bytes[0] = (write_bytes[0] & 0xC0) | (n >> 8);
	write_bytes[1] = n & 0xFF;
	g_state.tune = value;
}

void TEA5767_search(uint8_t up)
{
	if (up)
	{
		debug("Search up");
		write_bytes[2] |= TEA5767_SUD;
		TEA5767_tune(g_state.tune+150);
	}
	else
	{
		debug("Search down");
		write_bytes[2] &= ~TEA5767_SUD;
		TEA5767_tune(g_state.tune-150);
	}
	write_bytes[0] |= TEA5767_SEARCH | TEA5767_MUTE;
	TEA5767_write();
}

void TEA5767_exit_search(void)
{
	write_bytes[0] = (read_bytes[0] & 0x3f);
	write_bytes[1] = read_bytes[1];
	write_bytes[0] &= ~(TEA5767_SEARCH | TEA5767_MUTE);
	TEA5767_write();
	g_state.tune = ((((read_bytes[0]&0x3F)<<8)+read_bytes[1])*32768/4 + 225000)/1000;
	debugMetric("Exit search, tuned", (unsigned int)(g_state.tune/100));
}

int TEA5767_get_status(struct TEA5767_status *status)
{
	TEA5767_read();
	debugMetric("read_bytes[0]",read_bytes[0]);
	debugMetric("read_bytes[1]",read_bytes[1]);
	debugMetric("read_bytes[2]",read_bytes[2]);
	debugMetric("read_bytes[3]",read_bytes[3]);
	debugMetric("read_bytes[4]",read_bytes[4]);
	memset(status, 0, sizeof(*status));
	
	uint32_t freq = ((((read_bytes[0] & 0b00111111) << 8) + read_bytes[1]) * 32768/4 + 225000)/1000; // 0x3F 1st data byte: MUTE SM PLL13 PLL12 PLL11 PLL10 PLL9 PLL8
	debugMetric("Freq", (unsigned int)(freq/100));
	
	if (read_bytes[0] & TEA5767_READY_FLAG)	{							// if ready flag is set; TEA5767_READY_FLAG = 0b10000000 Ready Flag: if RF = 1 then a station has been found or the band limit
																		// has been reached; if RF = 0 then no station has been found
		debug("station has been found or the band limit has been reached");
		status->ready = 1;
		uint8_t val = read_bytes[2] & 0b01111111;						// IF counter 0x7F
		debugMetric("IF counter", val);
		if (abs(val - 0b00110110 ) < 2) 								// close match 0x36  [if 127 - 54 > 2?]
		{
			debug("Tuned!");
			status->tuned = 1;
		}
	} else {
		debug("no station has been found");
	}
	if (read_bytes[0] & TEA5767_BAND_LIMIT_FLAG)
	{
		debug("Band limit reached");
		status->band_limit = 1;
	} else {
		debug("band limit not reached");
	}
	if (read_bytes[2] & TEA5767_STEREO)
	{
		debug("Stereo reception");
		status->stereo = 1;
	} else {
		debug("mono reception");
		status->stereo = 0;
	}
	status->rx_power = read_bytes[3] >> 4;
	debugMetric("rx_power", status->rx_power);
	return 0;
}

/************************************************************************/
/* Initialize Timers                                                    */
/************************************************************************/

void initTimer0(void) 													// Night Light - Initialize the timer, this timer is for the photo resistive sensor
{   OCR0A = 0;															// set this for PWM duty cycle    
    TCCR0A |= (1 << COM0A1);											// set none-inverting mode        
    TCCR0A |= (1 << WGM01) | (1 << WGM00);								// set fast PWM Mode              
    TCCR0B |= (1 << CS01);												// set prescaler to 8, starts PWM 
    TIMSK0 |= (1 << TOIE0);												// overflow interrupt enable      
}

void initTimer1(void) 													// Real Time Clock
{   OCR1A = 31249;														// 31249 - when we hit this # 1 sec has elapsed @ 8mhz w/ prescaler @ 256
    TCCR1B |= (1 << WGM12);												// Mode 4, CTC on OCR1A
    TIMSK1 |= (1 << OCIE1A);											// Set interrupt on compare match
    TCCR1B |= (1 << CS12);												// set prescaler to 1024 and start the timer
}												

void initTimer2(void) 													// CO sensor - this is for the two different voltages needed for the CO sensor, 1.4v & 5v
{	CO_DDR |= (1 << CO_POWER_PIN);										// output pin for timer2 is OC2A which is PB3
	OCR2A = 0;															// set PWM for 5v
	TCCR2A |= (1 << COM2A1) | (1 << COM2A0);							// Set OC2A on Compare Match, clear OC2A at BOTTOM (inverting mode)
	TCCR2A |= (1 << WGM21) | (1 << WGM20);								// set fast PWM Mode
	TCCR2B |= (1 << CS21);												// set prescaler to 8 and starts PWM
	TIMSK2 |= (1 << TOIE2);												// overflow interrupt enable
}

void offTimer2(void)													// Turn off PMW for the CO sensor
{	TCCR2B &= ~((1 << CS20)|(1 << CS21)|(1 << CS22));					// No clock source (Timer/Counter stopped).
	TIMSK2 &= ~(1<<TOIE2);												// Disable the timer overflow interrupt
	SWITCH_DDR &= ~(1 << CO_POWER_PIN);									// CO_POWER_PIN no longer for output
}

/************************************************************************/
/* Interrupts                                                           */
/************************************************************************/
void InitInturrupts()													// Initialize interrupts for buttons and switches
{	PCICR |= (1 << PCIE_SWITCH);										// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
																		// Pin Change Mask Register 0 for port B
	PCMSK_SWITCH |= (1 << BUTTON_STATE_INT) | (1 << BUTTON_ACTION_INT) | (1 << ALARM_SWITCH_INT) | (1 << CO_PIN_INT);
																		// Set PCINT0 to trigger an interrupt on state change
	PCICR |= (1 << PCIE_RADIO);											// Pin Change Interrupt Control Register, set PCIE2 to enable PCMSK2 scan
																		// Pin Change Mask Register 2 for port D
	PCMSK_RADIO |= (1 << BUTTON_SEARCH_UP_INT) | (1 << BUTTON_SEARCH_DOWN_INT) | (1 << BUTTON_RADIO_ONOFF_INT);
																		// Set PCINT2 to trigger an interrupt on state change
}

ISR (PCINT2_vect)
{	debug("ISR(PCINT2_VECT)");
	byte changeddbits;

	char myStringFour[4];
	itoa(RADIO_PIN,myStringFour,2);
	debugNoLF("RADIO_PIN: ");debug(myStringFour);

	char myStringFive[4];
	itoa(portdhistory,myStringFive,2);
	debugNoLF("portdhistory: ");debug(myStringFive);

	changeddbits = RADIO_PIN ^ portdhistory;

	ClearBitNo(changeddbits,PORTD0);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD1);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD5);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD6);									// not a switch, ignore it
	ClearBitNo(changeddbits,PORTD7);									// not a switch, ignore it

	char myStringSeven[4];
	itoa(changeddbits,myStringSeven,2);
	debugNoLF("changeddbits: ");debug(myStringSeven);

	portdhistory = RADIO_PIN;

	ClearBitNo(portdhistory,PORTD0);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD1);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD5);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD6);									// not a switch, ignore it
	ClearBitNo(portdhistory,PORTD7);									// not a switch, ignore it

	char myStringEight[4];
	itoa(portdhistory,myStringEight,2);
	debugNoLF("portdhistory: ");debug(myStringEight);

	if(changeddbits & (1 << BUTTON_SEARCH_UP_PIN))						// PCINT0 changed
	{
		if( (portdhistory & (1 << BUTTON_SEARCH_UP_PIN)) == (1 << BUTTON_SEARCH_UP_PIN) )	// TODO: test using this instead of "8"
		{																// -or- can we say if (portdhistory == BUTTON_SEARCH_UP_PIN) ??
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, BUTTON_SEARCH_UP_PIN)) {			// LOW to HIGH pin change (button released)
				FlashLED();
				debug("PCINT2_VECT, RADIO_PIN3 SET, LOW TO HIGH");
				//ProcessEvent(0);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, BUTTON_SEARCH_UP_PIN)) {		// still pressed
				// HIGH to LOW pin change (search up button pressed)
				FlashLED();
				debug("PCINT2_VECT, RADIO_PIN3 SET, HIGH TO LOW");
				ProcessEvent(EVENT_SEARCH_UP);
			}
		}
	}

	if(changeddbits & (1 << BUTTON_SEARCH_DOWN_PIN))					// PCINT0 changed
	{
		if( (portdhistory & (1 << BUTTON_SEARCH_DOWN_PIN)) == (1 << BUTTON_SEARCH_DOWN_PIN) )	// TODO: test using this instead of 16
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(RADIO_PIN, BUTTON_SEARCH_DOWN_PIN)) {		// LOW to HIGH pin change (button released)
				FlashLED();
				debug("PCINT2_VECT, RADIO_PIN4 SET, LOW TO HIGH");
				//ProcessEvent(0);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(RADIO_PIN, BUTTON_SEARCH_DOWN_PIN)) {		// still pressed
																		// HIGH to LOW pin change (search up button pressed)
				FlashLED();
				debug("PCINT2_VECT, PIN_SWITCH4 SET, HIGH TO LOW");
				ProcessEvent(EVENT_SEARCH_DOWN);
			}
		}
	}

	if(changeddbits & (1 << BUTTON_RADIO_ONOFF_PIN))					// 
	{
		if( (portdhistory & (1 << BUTTON_RADIO_ONOFF_PIN)) == (1 << BUTTON_RADIO_ONOFF_PIN) )	// TODO: test using this instead of 16
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(RADIO_PIN, BUTTON_RADIO_ONOFF_PIN)) {		// LOW to HIGH pin change (button released)
				FlashLED();
				debug("PCINT2_VECT, RADIO_PIN2 SET, LOW TO HIGH");
				//ProcessEvent(0);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(RADIO_PIN, BUTTON_RADIO_ONOFF_PIN)) {		// button pressed
				// HIGH to LOW pin change (radio switch button pressed)
				FlashLED();
				debug("PCINT2_VECT, RADIO_PIN2 SET, HIGH TO LOW");
				ProcessEvent(EVENT_ONOFF_RADIO);
			}
		}
	}
}

ISR (PCINT0_vect)														// Interrupts for buttons and switches
{	//debug("ISR(PCINT0_VECT)");
	byte changedbbits;

	char myStringTwo[4];
	itoa(PIN_SWITCH,myStringTwo,2);
	//debugNoLF("PIN_SWITCH: ");debug(myStringTwo);

	char myStringThree[4];
	itoa(portbhistory,myStringThree,2);
	//debugNoLF("portbhistory: ");debug(myStringThree);

	changedbbits = PIN_SWITCH ^ portbhistory;
	ClearBitNo(changedbbits,CO_POWER_PIN);								// CO_POWER_PIN is not a switch, ignore it
	ClearBitNo(changedbbits,PORTB4);									// not a switch, ignore it

	char myString[4];
	itoa(changedbbits,myString,2);
	//debugNoLF("changedbbits: ");debug(myString);

	portbhistory = PIN_SWITCH;
	ClearBitNo(portbhistory,CO_POWER_PIN);								// TODO: remove this! [CO_POWER_PIN is not a switch, ignore it]
	ClearBitNo(portbhistory,PORTB4);									// TODO: remove this! [not a switch, ignore it]

	char myStringSix[4];
	itoa(portbhistory,myStringSix,2);
	//debugNoLF("portbhistory: ");debug(myStringSix);

	if(changedbbits & (1 << BUTTON_STATE_PIN))							// PCINT0 changed
	{
		if( (portbhistory & (1 << BUTTON_STATE_PIN)) == 1 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, BUTTON_STATE_PIN)) {				// LOW to HIGH pin change (state button released)
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH0 set, low to high");
				ProcessEvent(EVENT_PRESS_STATE);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, BUTTON_STATE_PIN)) {			// still pressed
				// HIGH to LOW pin change (state button pressed)
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH0 set, high to low");
			}
		}
	}

	if(changedbbits & (1 << BUTTON_ACTION_PIN))
	{
		if( (portbhistory & (1 << BUTTON_ACTION_PIN)) == 2 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, BUTTON_ACTION_PIN)) {			// LOW to HIGH pin change (action button released)
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH1 set, low to high");
				ProcessEvent(EVENT_PRESS_ACTION);
			}
		}
		else
		{																// HIGH to LOW pin change (action button pressed)
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, BUTTON_ACTION_PIN)) {			// still pressed
				FlashLED();
				//debug("PCINT0_vect, PIN_SWITCH1 set, high to low");
			}
		}
	}

	if(changedbbits & (1 << ALARM_SWITCH_PIN))							// PCINT0 changed
	{
		if( (portbhistory & (1 << ALARM_SWITCH_PIN)) == 4 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {				// LOW to HIGH pin change (alarm switch LEFT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH2 SET, LOW TO HIGH");
				ProcessEvent(EVENT_ALARM_SWITCH_ON);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, ALARM_SWITCH_PIN)) {			// still pressed, HIGH to LOW pin change (alarm switch RIGHT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH2 SET, HIGH TO LOW");	
				ProcessEvent(EVENT_ALARM_SWITCH_OFF);
			}
		}
	}

	if(changedbbits & (1 << CO_SWITCH_PIN))								// PCINT0 changed
	{
		if( (portbhistory & (1 << CO_SWITCH_PIN)) == 32 )
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {				// LOW to HIGH pin change (CO switch LEFT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH5 SET, LOW TO HIGH");
				ProcessEvent(EVENT_CO_ON);
			}
		}
		else
		{
			_delay_us(DEBOUNCE_TIME);
			if (bit_is_clear(PIN_SWITCH, CO_SWITCH_PIN)) {				// still pressed, HIGH to LOW pin change (CO switch RIGHT)
				FlashLED();
				//debug("PCINT0_VECT, PIN_SWITCH5 SET, HIGH TO LOW");
				ProcessEvent(EVENT_CO_OFF);
			}
		}
	}
}

ISR(TIMER0_OVF_vect) 													// Timer0 overflow interrupt, this is for the night light
{	count++;
	if (count == 255) {
		float photoVolts =  255 - (float) ReadADC(PHOTO_PIN)/4;			// bright = 24, dark = 239, range of 215
		if (photoVolts > 239) {
			photoVolts = 239;
		}
		if (photoVolts < 24) {
			photoVolts = 24;
		}
		photoVolts = (photoVolts - 24) * 1.186;
		//debug("photoVolts"); debugInt(photoVolts);
		OCR0A = photoVolts;
		count=0;
	}
}

ISR (TIMER1_COMPA_vect)													// Timer1 interrupt, this is the real time clock, triggered every second
{	//debug("ISR (TIMER1_COMPA_vect)");
	if (g_state.rtcRead) {
		g_state.timer1ElapsedSecs += 1;									// This is used for varying the voltage to the CO detector
		ToggleLED();
		if (!(g_state.mode & (SETTING_TIME_HOUR | SETTING_TIME_MINUTE | SETTING_TIME_SECOND))) {	// if we're not setting the time, allow time to pass as reflected in the UI
			Tick();
		}
	}
}

void Tick() 
{
	if (++g_state.seconds == 60) {
		g_state.seconds = 0;
		if (++g_state.minutes == 60) {
			g_state.minutes = 0;
			if (++g_state.hours == 24) {
				g_state.hours = 0;
				if (++g_state.dayOfWeek == 8) {
					g_state.dayOfWeek = 1;
				}
				if (++g_state.day == 32) {
					g_state.months++;
					g_state.day = 1;
					} else if (g_state.day == MAX_DAYS_IN_MONTH) {
					if ((g_state.months == 4) || (g_state.months == 6) || (g_state.months == 9) || (g_state.months == 11)) {
						g_state.months++;
						g_state.day = 1;
					}
					} else if (g_state.day == 30) {
					if (g_state.months == 2) {
						g_state.months++;
						g_state.day = 1;
					}
					} else if (g_state.day == 29) {
					if ((g_state.months == 2) && (NotLeap())) {
						g_state.months++;
						g_state.day = 1;
					}
				}
				if (g_state.months == 13) {
					g_state.months = 1;
					g_state.years++;
				}
				g_state.event = EVENT_DAY_CHANGED;
				ProcessEvent(g_state.event);
			}
			g_state.event = EVENT_HOUR_CHANGED;
			ProcessEvent(g_state.event);
		}
		g_state.event = EVENT_MINUTE_CHANGED;
		ProcessEvent(g_state.event);								// not sure why this is necessary but without it RTC doesn't work
	}
	g_state.event = EVENT_SECOND_CHANGED;
	ProcessEvent(g_state.event);
	
}

ISR (TIMER2_OVF_vect)													// Timer2 overflow interrupt, PMW for the CO sensor
{	//debug("TIMER2_OVF_vect");
	if((g_state.timer1ElapsedSecs >= 90) && (OCR2A == 183)) {
		g_state.sensorStable += 1;
		g_state.timer1ElapsedSecs = 0;
		debugMetric("g_state.COReadsSum", g_state.COReadsSum);
		debugMetric("g_state.COReadsMax", g_state.COReadsMax);
		debugMetric("g_state.COReadsMin", g_state.COReadsMin);
		debugMetric("g_state.COReadsCount", g_state.COReadsCount);
		g_state.COreading = (g_state.COReadsSum - g_state.COReadsMax - g_state.COReadsMin) / (g_state.COReadsCount - 2);
		debugMetric("g_state.COreading",g_state.COreading);				// CO reading from 1.4V run
		g_state.COReadsCount = 0;
		g_state.COReadsSum = 0;
		g_state.COReadsMin = 1024;										// maximum analog reading + 1
		g_state.COReadsMax = 0;											// minimum analog reading
		debug("Changed OCR2A TO 0, entering 60 second 5.0V reading/heating phase.");
		OCR2A = 0;														// voltage = ~5.0
	} else if ((g_state.timer1ElapsedSecs >= 60) && (OCR2A == 0)) {
		g_state.timer1ElapsedSecs = 0;
		debug("Changed OCR2A TO 183, entering 90 second 1.4V phase.");
		OCR2A = 183;													// voltage = ~1.4
	}
	if(	(g_state.timer1ElapsedSecs > 60) &&								// read for the last 30 seconds of the reading phase, 30 times
		(g_state.timer1ElapsedSecs < 90) &&
		(OCR2A == 183) &&
		(g_state.timer1LastSecSampled != g_state.timer1ElapsedSecs)) {
		g_state.timer1LastSecSampled = g_state.timer1ElapsedSecs;
		g_state.COSampleReading = ReadADC(CO_READ_PIN);					// read the CO sensor
		int tries = 1;
		while (MAX_ANALOG_READ_VALUE == g_state.COSampleReading && tries <= BAD_CO_READ_ATTEMPTS) {		
			tries++;													// if the reading is bad, retry BAD_CO_READ_ATTEMPTS times
			g_state.COSampleReading = ReadADC(CO_READ_PIN);				// read the CO sensor
		}
		debugMetric("COReading", g_state.COSampleReading);
		if(g_state.COSampleReading != MAX_ANALOG_READ_VALUE) {
			g_state.COReadsSum += g_state.COSampleReading;
			g_state.COReadsCount += 1;
			if(g_state.COSampleReading > g_state.COReadsMax) {			// check to see if this is the max reading so we can throw it out later
				g_state.COReadsMax = g_state.COSampleReading;
			}
			if(g_state.COSampleReading < g_state.COReadsMin) {			// check to see if this is the min reading so we can throw it out later
				g_state.COReadsMin = g_state.COSampleReading;
			}
			g_state.timer1LastSecSampled = g_state.timer1ElapsedSecs;
		}
	}
}

/************************************************************************/
/* Analog to digital conversion                                         */
/************************************************************************/
void InitADC0()															// Initialize ADC
{	ADMUX |= (1 << REFS0);												// reference voltage on AVCC
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0);								// ADC clock prescaler  / 8
	ADCSRA |= (1 << ADEN);												// enable ADC
}

uint16_t ReadADC(int channel)											// Read analog to digital converter
{	//debug("ReadADC()");
	ADMUX &= 0xF0;														// Clear the older channel that was read
	ADMUX |= channel;													// Defines the new ADC channel to be read
																		// first sample after you've changed the
																		// multiplexer will still be from the old
																		// analog source, you need to wait at
																		// least one complete ADC cycle before
																		// getting a value from the new channel
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	uint8_t theLowADC = ADCL;
	uint16_t theTenBitResults = ADCH<<8 | theLowADC;
	return(theTenBitResults);
}

/************************************************************************/
/* Time                                                                 */
/************************************************************************/
void InitRTC()															// Initialize RTC, read date and time from DS1307.														
{																		// Bit 7 of Register 0 is the clock halt (CH) bit. When this bit is set to 1, the
																		// oscillator is disabled. When cleared to 0, the oscillator is enabled. On
																		// first application of power to the device the time and date registers are
																		// typically reset to 01/01/00 01 00:00:00 (MM/DD/YY DOW HH:MM:SS). The CH bit
																		// in the seconds register will be set to a 1.
	DS1307_ClearClockHaltBit(g_state.seconds);							// clear the clock halt bit  TODO: delete this line of code
	g_state.rtcRead = FALSE;

	int badRead = TRUE;
	int i = 0;
	while ((badRead) && (i++ <= 3)) {
		ReadTime(   &g_state.hours,
					&g_state.minutes,
					&g_state.seconds	);
		ReadDate(	&g_state.years,
					&g_state.months,
					&g_state.day,
					&g_state.dayOfWeek	);
		badRead = FALSE;
		if (g_state.hours > 23 || g_state.hours < 0) {
			//debugMetric("Bad hours read",g_state.hours);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = TRUE;
		}
		if (g_state.minutes > 59 || g_state.minutes < 0) {
			//debugMetric("Bad minutes read", g_state.minutes);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = TRUE;
		}
		if (g_state.seconds > 59 || g_state.seconds < 0) {
			//debugMetric("Bad seconds read", g_state.seconds);
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = TRUE;
		}
		
		if ((g_state.hours == 0) &&
			(g_state.minutes == 0) &&
			(g_state.seconds == 0)) {
			badRead = TRUE;
			msDelay(1000);
		}
		//debugMetric("RTC READ ATTEMPT", i);
	}
	DS1307_ClearClockHaltBit(g_state.seconds);							// clear the clock halt bit
}

void ReadTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds) // Read time
{	debug("ReadTime()");
	if(!g_state.rtcRead) {												// if we have just powered on get the time from the RTC
		byte minutesRTC;
		byte hoursRTC;
		byte secondsRTC;
		DS1307_ReadTime(&hoursRTC,&minutesRTC,&secondsRTC);
		*inHours = BcdToDec(hoursRTC);
		*inMinutes = BcdToDec(minutesRTC);
		*inSeconds = BcdToDec(secondsRTC);
		g_state.rtcRead = TRUE;											// all subsequent setting of time is accomplished
		//debugTime();													// via the Timer1 RTC.
	} else {
		*inHours	= g_state.hours;									// hours, mins and secs updated by Timer1
		*inMinutes	= g_state.minutes;
		*inSeconds	= g_state.seconds;
	}
}
																		// Read date from DS1307
void ReadDate(volatile uint16_t *inYears, volatile byte *inMonths, volatile byte *inDay, volatile byte *inDayOfWeek)	
{	//debug("ReadDate()");
	byte yearsRTC;
	byte monthsRTC;
	byte dayRTC;
	byte dayOfWeekRTC = 0;
	DS1307_ReadDate(&yearsRTC,&monthsRTC,&dayRTC,&dayOfWeekRTC);	
	
//	dayOfWeekRTC = DayOfWeek(BcdToDec(yearsRTC) + 2000, BcdToDec(monthsRTC), BcdToDec(dayRTC)); // TODO: why is this causing a reset?
		
	*inYears		= BcdToDec(yearsRTC		) + 2000;	
	*inMonths		= BcdToDec(monthsRTC	);
	*inDay			= BcdToDec(dayRTC		);		
	*inDayOfWeek	= BcdToDec(dayOfWeekRTC	);
	
//	debugMetric("inYears", *inYears);
//	debugMetric("inMonths", *inMonths);
//	debugMetric("inDay", *inDay);
//	debugMetric("inDayOfWeek", *inDayOfWeek);
}

/*
byte DayOfWeek(int y, byte m, byte d)									// Implementation due to Tomohiko Sakamoto, calculate day of week from date
{   // y > 1752, 1 <= m <= 12
	debugMetric("y", y);
	debugMetric("m", m);
	debugMetric("d", d);
	
	static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	
	y -= m < 3;
	return ((y + y/4 - y/100 + y/400 + t[m-1] + d) % 7) + 1;			// 01 - 07, 01 = Sunday
}
*/

void ShowTime(byte hours, byte minutes)									// Update SS display with the time; hours and minutes
{	//debug("ShowTime()");
	SS_DisplayTime(hours, minutes);
	ColonOn();
}

/************************************************************************/
/* Alarms                                                               */
/************************************************************************/
void InitAlarms() 														// Determine if the alarm switch is on or off and process the appropriate event
{	portbhistory = 0b00000000;
	SetBitNo(portbhistory,BUTTON_STATE_PIN);
	SetBitNo(portbhistory,BUTTON_ACTION_PIN);
	portdhistory = 0b00000000;
	SetBitNo(portdhistory,BUTTON_SEARCH_UP_PIN);
	SetBitNo(portdhistory,BUTTON_SEARCH_DOWN_PIN);
	SetBitNo(portdhistory,BUTTON_RADIO_ONOFF_PIN);
	
    if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {						// if switch is ON, LEFT
		//debug("ALARM SWITCH IS ON");
        ProcessEvent(EVENT_ALARM_SWITCH_ON);
		SetBitNo(portbhistory, ALARM_SWITCH_PIN);
    }
    if (bit_is_clear(PIN_SWITCH, ALARM_SWITCH_PIN)) {					// if switch is OFF, RIGHT           
		//debug("ALARM SWITCH IS OFF");
        ProcessEvent(EVENT_ALARM_SWITCH_OFF);
		ClearBitNo(portbhistory,ALARM_SWITCH_PIN);
    }
	if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {						// if switch is ON, RIGHT
		//debug("CO IS ON");
		ProcessEvent(EVENT_CO_ON);
		SetBitNo(portbhistory, CO_SWITCH_PIN);
	}
	if (bit_is_clear(PIN_SWITCH, CO_SWITCH_PIN)) {						// if switch is OFF, LEFT
		//debug("CO IS OFF");
		ProcessEvent(EVENT_CO_OFF);
		ClearBitNo(portbhistory, CO_SWITCH_PIN);
	}
    char myString[4];
    itoa(portdhistory,myString,2);
    //debugNoLF("initial portdhistory: ");debug(myString);

}

void SoundAlarm() 														// Turn on the CO alarm. TODO: make this stop after a minute (configurable time)
{	//debug("SoundAlarm()");
	SetBitNo(BUZZER_PIN_PORT,BUZZER_PIN);
}

void SoundWake()														// Turn on the wake alarm														
{   //debug("SoundWake()");
	if (g_state.wakeToRadio && !g_state.radioOn) {
		UnMuteRadio();
	} else {
		ClearBitNo(WAKE_PIN_PORT,WAKE_PIN);
	}
}

void SilenceAlarm()														// Turn off the wake alarm
{   //debug("SILENCEALARM()");
    ClearBitNo(BUZZER_PIN_PORT,BUZZER_PIN);
    SetBitNo(WAKE_PIN_PORT,WAKE_PIN);
	if (g_state.wakeToRadio && g_state.radioOn) {
		MuteRadio();
	}
}

void SnoozeAlarm()														// Snooze the wake alarm for SNOOZE_TIME
{   //debug("SnoozeAlarm()");
    SilenceAlarm();
    g_state.alarmMinutes += SNOOZE_TIME;
    if (g_state.alarmMinutes > 59) {
        g_state.alarmMinutes = g_state.alarmMinutes - 60;
        g_state.alarmHours += 1;
    }
}

int WasWakeAlarmTriggered() 											// TODO: pass in the structure by value
{	//debug("WasWakeAlarmTriggered()");
	if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {						// alarm switch LEFT, ON, HIGH
		if (!((g_state.mode & BUZZING) == BUZZING)) {					// if the alarm isn't already going off
			if ((g_state.hours == g_state.alarmHours) &&
			(g_state.minutes == g_state.alarmMinutes) &&
			(g_state.seconds == 0)) {									// so that we only trigger the alarm once
				//debug("WAKE ALARM TRIGGERED");
				return 1;
			}
		}
	}
	return 0;
}

/************************************************************************/
/* eeprom                                                               */
/************************************************************************/
void EEPROM_WriteRadioStation(uint32_t station)							// Write the wake alarm time to eeprom
{	//debug("EEPROM_WriteRadioStation");
	eeprom_update_dword((uint32_t *) STATION_ADDRESS, station);
}

void EEPROM_ReadRadioStation(volatile uint32_t *station)				// Read the radio station from eeprom
{   //debug("EEPROM_ReadRadioStation()");
	*station = eeprom_read_dword((uint32_t *) STATION_ADDRESS);
	if (*station > TOP_OF_DIAL || *station < BOTTOM_OF_DIAL) {
		*station = STRONG_LOCAL_STATION;
		EEPROM_WriteRadioStation(*station);
	}
	g_state.tune = *station;	
	//debugMetric("Radio Station", (int) *station / 100);
}

void EEPROM_WriteAlarmTime(byte hours, byte minutes)					// Write the wake alarm time to eeprom
{	//debug("EEPROM_WriteAlarmTime()");
	uint8_t *address_minutes = (uint8_t *) ALARM_ADDRESS;
	eeprom_update_byte(address_minutes, minutes);
	uint8_t *address_hours = (uint8_t *) ALARM_ADDRESS + 1;
	eeprom_update_byte(address_hours, hours);
	//debugAlarmTime();
}

void EEPROM_ReadAlarmTime(volatile byte *hours, volatile byte *minutes)	// Read the wake alarm time from eeprom
{   //debug("EEPROM_ReadAlarmTime()");
	uint8_t *address_minutes = (uint8_t *) ALARM_ADDRESS;
	*minutes = eeprom_read_byte(address_minutes);
	uint8_t *address_hours = (uint8_t *) ALARM_ADDRESS + 1;
	*hours = eeprom_read_byte(address_hours);

	if ((*hours < 0) || (*hours > 23)) {								// Wake hours and minutes are corrupt the first time they are read from memory, clean that up
		*hours = 0;
	}
	if ((*minutes < 0) || (*minutes > 59)) {
		*minutes = 0;
	}
	//debugAlarmTime();
}

void EEPROM_WriteMilitaryTimeSwitch(byte wakeToRadio)			// Write the military time switch setting to eeprom
{
		eeprom_update_byte((uint8_t *) WAKE_TO_RADIO_ADDRESS, wakeToRadio);
}

void EEPROM_ReadMilitaryTimeSwitch(volatile byte *military_time_switch)	// Read the military time switch setting from eeprom
{    //debug("EEPROM_ReadMilitaryTimeSwitch()");
	*military_time_switch = eeprom_read_byte((uint8_t *) MILITARY_TIME_ADDRESS);

	if ((*military_time_switch < 0) || (*military_time_switch > 1)) {	
		*military_time_switch = TRUE;
		//debug("mil time corrupt");
	}
	//debugMetric("Military Time Read from EEPROM", *military_time_switch);
	
	if (*military_time_switch) {										// set show military time state
		g_state.militaryTime = TRUE;
	} else {
		g_state.militaryTime = FALSE;
	}

}

void EEPROM_WriteWakeToRadioSwitch(byte military_time_switch)			// Write the military time switch setting to eeprom
{	//debug("EEPROM_WriteMilitaryTimeSwitch()");
	eeprom_update_byte((uint8_t *) MILITARY_TIME_ADDRESS, military_time_switch);
}

void EEPROM_ReadWakeToRadioSwitch(volatile byte *wakeToRadio)	// Read the wake to radio switch setting from eeprom
{    //debug("EEPROM_ReadWakeToRadioSwitch()");
	*wakeToRadio = eeprom_read_byte((uint8_t *) WAKE_TO_RADIO_ADDRESS);

	if ((*wakeToRadio < 0) || (*wakeToRadio > 1)) {
		*wakeToRadio = TRUE;
		debug("wake to radio corrupt");
	}
	//debugMetric("Wake to Radio Read from EEPROM", *military_time_switch);
	
	if (*wakeToRadio) {										// set wake to radio state
		g_state.wakeToRadio = TRUE;
	} else {
		g_state.wakeToRadio = FALSE;
	}

}


/************************************************************************/
/* Temperature                                                          */
/************************************************************************/
																		// TODO: Somehow thus turns off the timer for the night light!  Function not currently called.
double GetInternalTemp(void)											// get the chip temperature and convert to farenheit
{
	ADMUX = (3 << REFS0) | (8 << MUX0);									// 1.1V REF, channel#8 is temperature
	ADCSRA |= (1 << ADEN) | (6 << ADPS0);								// enable the ADC div64
	msDelay(20);														// wait for voltages to become stable.
	ADCSRA |= (1 << ADSC);												// Start the ADC

	while (ADCSRA & (1 << ADSC));										// Detect end-of-conversion
	//return (ADCW - 324.31) / 1.22;
	return (ADCW * (1.1/1023.0) * (25.0/0.314) * 1.8 + 32.0);			// convert Celsius to Fahrenheit [* 1.8 + 32.0], do some other stuff(?)
}																		// volts2degC = 25.0/0.314, ad2volts=1.1/1023.0 (volts per analog read step)
																		// 1023 = 1.1v

void ShowTemp(int tempF)												// Write room temp to SS display
{   debug("ShowTemp()");
	debugMetric("tempF",tempF);
    if (tempF > MAX_TEMP || tempF < MIN_TEMP) {
        SS_DisplayChars(CHAR_E,CHAR_R,CHAR_R,CHAR_SPACE);
    } else {
        byte ones = tempF - (tempF / 10) * 10;
        byte tens = tempF / 10;
        byte hundreds = tempF / 100;
        SS_BlankDisplay();
        if (!(hundreds > 0)) {
            hundreds = CHAR_SPACE;
        }
        if (!(tens > 0)) {
            tens = CHAR_SPACE;
        }
		debugMetric("Temp Hundreds", hundreds);
		debugMetric("Temp Tens", tens);
		debugMetric("Temp Ones", ones);
        SS_DisplayChars(hundreds,tens,ones,CHAR_F);
        ColonOff();
    }
}

byte ConvertToFarenheit(unsigned short celsius)							// Convert from celsius to farenheit		
{    if (celsius > 127) celsius = ~celsius + 1;							// Check for negative temperature 
    byte farenheit = 32.0 + (celsius * 9.0/5.0);
    return farenheit;
}

byte getTemp() {
	unsigned short tempC = I2C_ReadRegister(TC74_ADDRESS_R, TC74_TEMP_REGISTER);
	g_state.tempF = ConvertToFarenheit(tempC);
	return g_state.tempF;
}

/************************************************************************/
/* Carbon Monoxide                                                      */
/************************************************************************/
int WasCOGasAlarmTriggered()											// returns true if the CO alarm is tripped
{  //debug("WasCOGasAlarmTriggered()");
																		// if the CO sensor is powered up
   if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {							// alarm switch RIGHT, ON, HIGH      
        if ((g_state.COreading > CO_ALARM_THRESHOLD) && 
			(g_state.sensorStable >= CONSEC_CO_READING_THRES)) {		// reading must be > CO_ALARM_THRESHOLD for CONSECUTIVE_CO_READING_THRESHOLD 		
			//debug("CO gas alarm triggered");
            return(1);													// ...times to signal alarm
        }
   }
   return(0);     
}     

void DisplayWarnForCO()													// write "CO" to the seven segment display
{	SS_DisplayChars(CHAR_SPACE, CHAR_C, CHAR_O, CHAR_SPACE);
	ColonOff();
	LCD_DisplayCO(g_state.COreading);									// display CO reading on the LCD
}

/************************************************************************/
/* Misc Functions                                                       */
/************************************************************************/
void msDelay(int delay)  												// Wait for 'delay' milliseconds
{	for (int i=0;i<delay;i++)											// to remove code inlining
	_delay_ms(1);														// at cost of timing accuracy
}

void SoundOneShortBeep()												// 250ms beep
{	//debug("SoundOneShortBeep()");
	SetBitNo(BUZZER_PIN_PORT,BUZZER_PIN);								// set BUZZER_PIN HIGH
	msDelay(250);
	ClearBitNo(BUZZER_PIN_PORT,BUZZER_PIN);								// set BUZZER_PIN LOW
}

void FlashLED()															// turn on the heartbeat LED for 100ms
{	//debug("FlashLED()");
	SetBitNo(HEARTBEAT_LED_PORT,HEARTBEAT_LED_PIN);
	msDelay(100);
	ClearBitNo(HEARTBEAT_LED_PORT,HEARTBEAT_LED_PIN);
}

void ToggleLED()														// Toggle the heartbeat LED
{	HEARTBEAT_LED_PORT ^= (1<<HEARTBEAT_LED_PIN);
}

byte DecToBcd(byte val)													// Convert normal decimal numbers to binary coded decimal
{
	return ( (val/10*16) + (val%10) );
}

byte BcdToDec(byte val)													// Convert binary coded decimal to normal decimal numbers
{
	return ( (val/16*10) + (val%16) );
}

const char *DayOfWeekNumToString(byte dayOfWeekNum)						// Convert a numbered day of the week to an abbreviated name of the day
{	if (dayOfWeekNum < 1 || dayOfWeekNum > 7) return "Unk\0";
	const char* days[] = {"Sun \0","Mon \0","Tue \0","Wed \0","Thur\0","Fri \0","Sat \0"};
	return days[dayOfWeekNum - 1];
}

byte GetDayOfWeek(int d, int m, int y) {
	return((d += m < 3 ? y-- : y - 2, 23*m/9 + d + 4 + y/4- y/100 + y/400)%7);
}

char NotLeap(void)														// Check for Leap Year
{
	if (!(g_state.years % 100)) {
		return (char)(g_state.years % 400);
	} else {
		return (char)(g_state.years % 4);
	}
}

//void SetState(uint32_t state)											// Given a state, sets it on g_state.mode
//{
//	SetBit(g_state.mode, state);
//}

void ClearState(uint32_t state)											// Clears a state from g_state.mode
{
	ClearBit(g_state.mode, state);										// Clear the state from the mode
}

void TrueUpTime() {
		debug("Truing up time.");
		debugTime();
		g_state.rtcRead = FALSE;
		ReadDate(				&g_state.years,		&g_state.months,
								&g_state.day,		&g_state.dayOfWeek);
		ReadTime(				&g_state.hours,		&g_state.minutes,	// true up the time with the DS1307 real time clock
								&g_state.seconds					  );
		debugTime();
}

void MuteRadio() {
	TEA5767_mute();
	g_state.radioOn = FALSE;

}

void UnMuteRadio() {
	TEA5767_unmute();
	g_state.radioOn = TRUE;

}

/************************************************************************/
/* Initialize the AVR                                                   */
/************************************************************************/

void InitAVR()															// prepare pins for input and output
{	debug("INITAVR()");
																		// configure all pins for input with pull-ups enabled
	SWITCH_DDR = 0x00;													// put all pins in input mode
	PORTB = 0Xff;														// enable pull-ups
																		// Make all unused pins inputs with the internal pull up resistor enabled
	DDRC = 0x00;														// put all pins in input mode
	PORTC = 0Xff;														// enable pull-ups
	RADIO_DDR = 0x00;													// put all pins in input mode
	PORTD = 0Xff;														// enable pull-ups
																		// Selectively configure pins (anything not touched is an input w/ a pull-up)
	DDRB |= (1 << UNUSED_B4_PIN);										// Unused, configure for ouput
	DDRD |= (1 << UNUSED_D7_PIN);										// Unused, configure for ouput 
	ClearBitNo(PORTB, UNUSED_B4_PIN);									// drive low
	ClearBitNo(PORTD, UNUSED_D2_PIN);									// drive low
	ClearBitNo(PORTD, UNUSED_D7_PIN);									// drive low
	BUZZER_PIN_DDR |= (1 << BUZZER_PIN);								// Configure buzzer pin for output.
	ClearBitNo(BUZZER_PIN_PORT,BUZZER_PIN);								// Turn off the buzzer.
	WAKE_PIN_DDR |= (1 << WAKE_PIN);									// Configure wake pin for output.
	SetBitNo(WAKE_PIN_PORT,WAKE_PIN);									// Turn off the wake alarm (redundant)
	NIGHTLIGHT_DDR |= (1 << NIGHTLIGHT_PIN);							// Configure night light pin for output
	HEARTBEAT_LED_DDR |= (1 << HEARTBEAT_LED_PIN);						// Configure the heartbeat LED for output.
	RADIO_DDR &= ~(1 << BUTTON_SEARCH_UP_PIN);							// set search up button to input (redundant)
	RADIO_DDR &= ~(1 << BUTTON_SEARCH_DOWN_PIN);						// set search down button to input (redundant)
	RADIO_DDR &= (1 << BUTTON_RADIO_ONOFF_PIN);							// set radio on/off button to input  (redundant)		IS THIS CORRECT????????????
	SWITCH_DDR |= (1 << CO_POWER_PIN);									// Configure CO power pin for output.
	SWITCH_DDR &= ~(1 << BUTTON_STATE_PIN);								// Configure button state pin for input (redundant)
	SWITCH_DDR &= ~(1 << BUTTON_ACTION_PIN);							// Configure button action pin for input (redundant)
	SWITCH_DDR &= ~(1 << ALARM_SWITCH_PIN);								// configure alarm switch pin for input (redundant)
	SWITCH_DDR &= ~(1 << UNUSED_B4_PIN);								// unused set to input so as not to effect portbhistory / PIN_SWITCH (redundant)
	SWITCH_DDR &= ~(1 << CO_SWITCH_PIN);								// and CO_SWITCH_PIN pin (sets them to input). (redundant)
																		// Turn On the Pull-ups TODO: is this necessary, we're using pull up resistors? (redundant)
	SWITCH_PORT |= ((1 << BUTTON_STATE_PORT) | (1 << BUTTON_ACTION_PORT) | (1 << ALARM_SWITCH_PORT) | (1 << CO_SWITCH_PORT));			
																		// Turn on the pull-ups for the radio tune up and down buttons (redundant)
	RADIO_PIN_PORT |= ((1 << BUTTON_SEARCH_UP_PORT) | (1 << BUTTON_SEARCH_DOWN_PORT) | (1 << BUTTON_RADIO_ONOFF_PORT));					
}

/************************************************************************/
/* High Level Program Control                                           */
/************************************************************************/
int main(void)															// main processing loop
{   Initialize();
    debug("Main()");
    while(1) {}															// Event loop, wait for interrupts forever
}

void Initialize()														// called at startup to prepare all peripherals
{	initUSART();

	//debug("==============================================================");
	//debug("Copyright (c) 2018, Daniel Murphy");
	//debug("==============================================================");

	debug("Initialize the system.");

//	debug("Initializing AVR.");
	InitAVR();															// set port direction

//	debug("Send one short beep.");
	FlashLED();
	SoundOneShortBeep();

//	debug("Initializing I2C.");
	FlashLED();
	I2C_Init();															// set I2C clock frequency

//	debug("Wait for I2C devices to come online.");
	msDelay(I2C_DEVICE_ONLINE_WAIT);
	
	debug("Show all I2C devices.");
	FlashLED();
	I2C_FindDevices();

//	debug("Initialize RTC.");
	FlashLED();
	InitRTC();

//	debug("Initialize ADC.");
	FlashLED();
	InitADC0();
	
//	debug("Initialize timers.");
	FlashLED();
	initTimer0();														// initialize Timer0
	FlashLED();
	initTimer1();														// initialize Timer1
	FlashLED();
	initTimer2();														// initialize Timer2

//	debug("Initialize interrupts.");
	FlashLED();
	InitInturrupts();
	
	debug("Initialize the wake alarm.");
	FlashLED();
	g_state.alarmHours = 0;
	g_state.alarmMinutes = 0;											//(no seconds for alarm)
	
	EEPROM_ReadAlarmTime(	&g_state.alarmHours,
							&g_state.alarmMinutes	);					// Read the wake time from chip memory
	InitAlarms();														// Initialize the alarm switch.

	EEPROM_ReadMilitaryTimeSwitch(&g_state.militaryTime);
	debugMetric("Mil time after function call:", g_state.militaryTime);

	EEPROM_ReadWakeToRadioSwitch(&g_state.wakeToRadio);
	debugMetric("Wake to Radio after function call:", g_state.militaryTime);

//	debug("Initialize the LCD.");
	FlashLED();	
    lcd_init(LCD_DISP_ON);												// caused the SS to not work when we were using the default TWBR of 10000
	LCD_Reset();

//	debug("Initialize TEA5767.");
	FlashLED();
	TEA5767_init();	
	
//	debug("Enable global interrupts.\r\n");
	FlashLED();
	sei();																// enable interrupts

	debug("Finished initialization.\r\n");
	FlashLED();
	SoundOneShortBeep();

	g_state.mode |= SHOWING_TIME;										// set up to show the time immediately
	g_state.sensorStable = 0;											// the CO sensor isn't stable yet.
	
	ProcessEvent(EVENT_DAY_CHANGED);									// this sets up all of the displays
	ProcessEvent(EVENT_HOUR_CHANGED);
	ProcessEvent(EVENT_MINUTE_CHANGED);
	ProcessEvent(EVENT_SECOND_CHANGED);
	MuteRadio();	
	
//	wdt_enable(WDTO_2S);												// enable the watchdog timer for 2 seconds TODO: figure out why it keeps triggering 31 seconds after system start
}
																		// check for events that don't have interrupts, this is in
void CheckForEvents() 													// the mainline of processing
{						
	//debug("CheckForEvents()");										// Check if the CO gas event was triggered.
	if (WasCOGasAlarmTriggered()) {										// CO alarm has priority over wake alarm
		ProcessEvent(EVENT_CO_DETECTED);
	} else if (WasWakeAlarmTriggered()) {								// This needs to be called about once a second so that we don't skip
																		// over the second(s) that trigger the wake alarm.
		debug("Wake alarm triggered in CheckForEvents()");
		ProcessEvent(EVENT_WAKE_TRIGGERED);
	}
	return;
}

void ProcessEvent(byte event)											// processes events
{	//debug("ProcessEvent()");
    if (event == EVENT_NONE) {
        if (g_state.mode & SHOWING_TIME) {
            ToggleColon(g_state.mode);
        }
        return;
	} else if (event == EVENT_SECOND_CHANGED) {
		if (g_state.previousEvent == EVENT_SEARCH_UP || g_state.previousEvent == EVENT_SEARCH_DOWN) {
			g_state.previousEvent = EVENT_SEC_AFTER_UP_OR_DOWN;
		} else if (g_state.previousEvent == EVENT_SEC_AFTER_UP_OR_DOWN) {
			g_state.previousEvent = EVENT_SECOND_CHANGED;
			LCD_DisplayDateAndTemp(	g_state.years, g_state.months, g_state.day,
									g_state.dayOfWeek, g_state.hours, g_state.minutes,
									g_state.seconds, g_state.tempF);
		} else {
			LCD_DisplaySeconds(g_state.seconds);
			CheckForEvents();
		}
    } else if (event == EVENT_MINUTE_CHANGED) {							// set by Timer1 RTC
        if (g_state.mode & SHOWING_TIME) {
            ShowTime(g_state.hours, g_state.minutes);
        }
		LCD_DisplayMinutes(g_state.minutes);
		if (!(g_state.mode & SHOWING_CO)) { 
			LCD_DisplayTemp(getTemp());
		}
	} else if (event == EVENT_HOUR_CHANGED) {
		LCD_DisplayHours(g_state.hours);
	} else if (event == EVENT_DAY_CHANGED) {							// new day, change the date displayed
		TrueUpTime();
		SS_DisplayTime(			g_state.hours,		g_state.minutes	  );
		LCD_DisplayDateAndTemp(	g_state.years,		g_state.months,		// display date, time, day of week and temperature on LCD
								g_state.day,		g_state.dayOfWeek,
								g_state.hours,		g_state.minutes,
								g_state.seconds,	g_state.tempF	  );
    } else if (event == EVENT_PRESS_STATE) {							// if the STATE (left) button was pressed 
        if ( (g_state.mode & SHOWING_TIME) &&							// If the clock is showing time and is NOT sounding the alarm, put it into set wake hour mode 
            !(g_state.mode & BUZZING) ) {
            ClearBit(g_state.mode, SHOWING_TIME);						// Turn off showing time
            SetState(SETTING_WAKE_HOUR);								// Turn on setting wake 
			LCD_DisplayTimeAlarmSetHours(g_state.alarmHours, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
            SS_DisplayTimeAlarmSetHours(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_HOUR) {					// If the clock is setting the wake hour, put it into setting wake minute mode 
            ClearBit(g_state.mode, SETTING_WAKE_HOUR);
            SetState(SETTING_WAKE_MINUTE);
			LCD_DisplayTimeAlarmSetMinutes(g_state.alarmMinutes, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
            SS_DisplayTimeAlarmSetMinutes(g_state.alarmMinutes);
		} else if (g_state.mode & SETTING_WAKE_MINUTE) {
			ClearBit(g_state.mode, SETTING_WAKE_MINUTE);
			SetState(SETTING_TIME_YEAR);	
            EEPROM_WriteAlarmTime(g_state.alarmHours, g_state.alarmMinutes);	// save wake time
			LCD_DisplayTimeSetYears(g_state.years, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetYears(g_state.years);
		} else if (g_state.mode & SETTING_TIME_YEAR) {					// move from setting year to setting month
			ClearBit(g_state.mode, SETTING_TIME_YEAR);					// clear setting year
			SetState(SETTING_TIME_MONTH);								// turn on setting month of year
			LCD_DisplayTimeSetMonths(g_state.months, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetMonths(g_state.months);					// Prepare the SS display to set time month of year
		} else if (g_state.mode & SETTING_TIME_MONTH) {					// move from setting month to setting day
			ClearBit(g_state.mode, SETTING_TIME_MONTH);					// Clear setting time month of year
			SetState(SETTING_TIME_DAY);									// Turn on setting time day of month
			LCD_DisplayTimeSetDays(g_state.day, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetDays(g_state.day);							// Prepare the SS display to set time day of month
		} else if (g_state.mode & SETTING_TIME_DAY) {					// move from setting day into setting week
			ClearState(SETTING_TIME_DAY);
			SetState(SETTING_TIME_DAY_OF_WEEK);
			g_state.dayOfWeek = 1+GetDayOfWeek(g_state.day,g_state.months,g_state.years);
			LCD_DisplayTimeSetDayOfWeek(g_state.dayOfWeek, PRINT_PROMPT);  // automatically show the correct day of week for the supplied date
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetDayOfWeek(GetDayOfWeek(g_state.day,g_state.months,g_state.years)); // Prepare SS display to set time day of week
        } else if (g_state.mode & SETTING_TIME_DAY_OF_WEEK) {			// If clock is setting time day set the mode to setting time hour 
            ClearBit(g_state.mode, SETTING_TIME_DAY_OF_WEEK);			// Turn off setting day of week
            SetState(SETTING_TIME_HOUR);								// Turn on setting time hour
			DS1307_WriteDate(g_state.years,g_state.months,g_state.day,g_state.dayOfWeek);	// Write date to DS1307 TODO: fix defect where behavior is undefined here
			LCD_DisplayTimeSetHours(g_state.hours, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
            SS_DisplayTimeSetHours(g_state.hours);						// Prepare the SS display to set time hours
        } else if (g_state.mode & SETTING_TIME_HOUR) {					// If clock is setting time hour set the mode to setting time minute 
            LCD_DisplayHours(g_state.hours);							// Make sure the LCD shows the correct hours
            ClearBit(g_state.mode, SETTING_TIME_HOUR);					// Turn off setting time hour
            SetState(SETTING_TIME_MINUTE);								// Turn on setting time minute
			LCD_DisplayTimeSetMinutes(g_state.minutes, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
            SS_DisplayTimeSetMinutes(g_state.minutes);					// Prepare to set minutes
        } else if (g_state.mode & SETTING_TIME_MINUTE) {				// If the clock is setting time minute set the mode to setting time second
            ClearBit(g_state.mode, SETTING_TIME_MINUTE);
            SetState(SETTING_TIME_SECOND);								// stop showing minute 
			ShowTime(g_state.hours, g_state.minutes);					// show hours and minutes on the SS display
			LCD_DisplayTimeSetSeconds(g_state.seconds,PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetSeconds(g_state.seconds);
        } else if (g_state.mode & SETTING_TIME_SECOND) {				// If the clock is setting time minute set the mode to showing time
            DS1307_WriteTime(g_state.hours, g_state.minutes, g_state.seconds);	// save the new time to the DS1307
			ClearBit(g_state.mode, SETTING_TIME_SECOND);
			SetState(SETTING_WAKE_TO_RADIO);							// stop showing minute
			ShowTime(g_state.hours, g_state.minutes);					// show hours and minutes on the SS display
			LCD_DisplayTimeWakeToRadio(g_state.wakeToRadio,PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetWakeToRadio(g_state.wakeToRadio);
		} else if (g_state.mode & SETTING_WAKE_TO_RADIO) {
			EEPROM_WriteWakeToRadioSwitch(g_state.wakeToRadio);			// write the wake to radio switch setting to eeprom
			ClearBit(g_state.mode, SETTING_WAKE_TO_RADIO);				// stop showing wake to radio interface
			SetState(SETTING_MIL_TIME_SWITCH);							// show the set military time switch interface next
			ShowTime(g_state.hours, g_state.minutes);					// show hours and minutes on the SS display
			LCD_DisplayTimeSetMilitary(g_state.militaryTime, PRINT_PROMPT);
			LCD_DisplayTimeAndTemp(g_state.hours, g_state.minutes, g_state.seconds, g_state.tempF);
			SS_DisplayTimeSetMilitaryTime(g_state.militaryTime);		// Turn on setting military time display
		} else if (g_state.mode & SETTING_MIL_TIME_SWITCH) {
			EEPROM_WriteMilitaryTimeSwitch(g_state.militaryTime);		// write the military time switch setting to eeprom
            ClearBit(g_state.mode, SETTING_MIL_TIME_SWITCH);			// stop showing military time set interface
            SetState(SHOWING_TIME);										// back to showing the time
            ShowTime(g_state.hours, g_state.minutes);					// show hours and minutes on the SS display
			LCD_DisplayDateAndTemp(	g_state.years, g_state.months, g_state.day,
									g_state.dayOfWeek, g_state.hours, g_state.minutes,
									g_state.seconds, g_state.tempF			);
        } else if (g_state.mode & SHOWING_TEMP) {						// if showing the temperature set the mode to show the time 
            ClearBit(g_state.mode, SHOWING_TEMP);						// turn off showing temp
            SetState(SHOWING_TIME);										// go back to showing time
            ShowTime(g_state.hours, g_state.minutes);					// Display hours and minutes on the SS display
			LCD_DisplayDateAndTemp(	g_state.years, g_state.months, g_state.day,
									g_state.dayOfWeek, g_state.hours, g_state.minutes,
									g_state.seconds, g_state.tempF			);
																		// Update the LCD dislay
        } else if (g_state.mode & SHOWING_CO){							// if showing CO alert set mode to show time and silence sounding alarm 
            ClearBit(g_state.mode, SHOWING_CO);							// if the CO alarm went off clear the SS display and show hours and minutes
            SetState(SHOWING_TIME);										// set the mode to showing time
            g_state.COreading = 0;										// so that the alarm doesn't keep sounding 
																		// after the button is pressed
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode.
            SilenceAlarm();												// silence
            ShowTime(g_state.hours, g_state.minutes);					// shows time 
		}
        if (g_state.mode & BUZZING) {									// silence the alarm if it's sounding 
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode.
            SilenceAlarm();
        }
    } else if (event == EVENT_PRESS_ACTION) {							// if the ACTION (right) button was pressed 
        if ((g_state.mode & SHOWING_TIME) &&							// if time is showing and it's not buzzing, show the temperature 
            !(g_state.mode & BUZZING)) {
            ClearBit(g_state.mode, SHOWING_TIME);						// no longer showing time
            SetState(SHOWING_TEMP);										// show the temperature
            ShowTemp(getTemp());
			TrueUpTime();
//			int internalTemp = (int) GetInternalTemp();					// TODO: this crashes the night light for unknown reasons...
//			debugMetric("INTERNAL TEMPERATURE", internalTemp);
        }
        if (g_state.mode & SETTING_WAKE_HOUR) {							// if setting wake hour, increment the hour
			if (++g_state.alarmHours > 23) g_state.alarmHours = 0;
			LCD_DisplayTimeAlarmSetHours(g_state.alarmHours, DONT_PRINT_PROMPT);
            SS_DisplayTimeAlarmSetHours(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {				// else if setting wake minute, increment minute 
			if (++g_state.alarmMinutes > 59) g_state.alarmMinutes = 0;
			LCD_DisplayTimeAlarmSetMinutes(g_state.alarmMinutes, DONT_PRINT_PROMPT);
            SS_DisplayTimeAlarmSetMinutes(g_state.alarmMinutes);
		} else if (g_state.mode & SETTING_TIME_YEAR) {
			if (++g_state.years > MAX_YEARS) g_state.years = MIN_YEARS;
			LCD_DisplayTimeSetYears(g_state.years, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetYears(g_state.years);
		} else if (g_state.mode & SETTING_TIME_MONTH) {
			if (++g_state.months > 12) g_state.months = 1;				
			LCD_DisplayTimeSetMonths(g_state.months, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetMonths(g_state.months);
		} else if (g_state.mode & SETTING_TIME_DAY) {
			if (++g_state.day > daysInMonth[g_state.months]) g_state.day = 1;		
			LCD_DisplayTimeSetDays(g_state.day, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetDays(g_state.day);
		} else if (g_state.mode & SETTING_TIME_DAY_OF_WEEK) {
			if (++g_state.dayOfWeek > 7) g_state.dayOfWeek = 1;
			LCD_DisplayTimeSetDayOfWeek(g_state.dayOfWeek, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetDayOfWeek(g_state.dayOfWeek);
        } else if (g_state.mode & SETTING_TIME_HOUR) {					// if setting time hour, increment hour
			if(++g_state.hours > 23) g_state.hours = 0;
			LCD_DisplayTimeSetHours(g_state.hours, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetHours(g_state.hours);
        } else if (g_state.mode & SETTING_TIME_MINUTE) {				// if setting time minute, increment minute
			if (++g_state.minutes > 59) g_state.minutes = 0;
			LCD_DisplayTimeSetMinutes(g_state.minutes, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetMinutes(g_state.minutes);
		} else if (g_state.mode & SETTING_TIME_SECOND) {
			if (++g_state.seconds > 59) g_state.seconds = 0;
			LCD_DisplayTimeSetSeconds(g_state.seconds, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetSeconds(g_state.seconds);
		} else if (g_state.mode & SETTING_WAKE_TO_RADIO) {
			if (++g_state.wakeToRadio > TRUE) g_state.wakeToRadio = FALSE;			
			LCD_DisplayTimeWakeToRadio(g_state.wakeToRadio, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetWakeToRadio(g_state.wakeToRadio);
		} else if (g_state.mode & SETTING_MIL_TIME_SWITCH) {
			if (++g_state.militaryTime > TRUE) g_state.militaryTime = FALSE;
			LCD_DisplayTimeSetMilitary(g_state.militaryTime, DONT_PRINT_PROMPT);
			SS_DisplayTimeSetMilitaryTime(g_state.militaryTime);
        } else if (g_state.mode & SHOWING_CO) {
            ShowTime(g_state.COhours, g_state.COminutes);				// shows time CO alarm went off 
            msDelay(2000);												// wait for two seconds
            DisplayWarnForCO();											// back to showing CO  
        } else if (g_state.mode & BUZZING) {							// If the alarm is sounding, change the clock mode to snoozing 
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
            SetState(SNOOZING); 										// add snoozing
            SnoozeAlarm(&g_state);
        }
    } else if (event == EVENT_WAKE_TRIGGERED) {							// if it's time to sound the alarm to wake up; sound the alarm if the switch is on  
        if (g_state.mode & WAKE) {          							// if the wake alarm is enabled
            SetState(BUZZING);  										// set the mode to buzzer on
			//debug("WAKE DETECTED.");
            SoundWake();                   								// play a song using UM66 
        }
    } else if (event == EVENT_CO_DETECTED) {
        SetState(BUZZING);
        SoundAlarm();													// sound the CO alarm, CO is high
        ClearBit(g_state.mode, SHOWING_TIME);							// stop showing time
        SetState(SHOWING_CO);											// start showing CO reading
		g_state.COhours = g_state.hours;								// set hours and minutes so they can be displayed when
		g_state.COminutes = g_state.minutes;							// the action button is pressed when the CO alarm is going off
        DisplayWarnForCO(g_state.COreading);							// todo: make this flash; display warning on CC display
    } else if (event == EVENT_ALARM_SWITCH_OFF) {
        ClearBit(g_state.mode, WAKE);       							// set the alarm so it will not sound
		g_state.decimalBlink = FALSE;									// turn off the blinking decimal
        if (g_state.mode & BUZZING) {       							// if the alarm is currently sounding
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
            SilenceAlarm();                 							// turn of the sounding alarm (both song and siren)
        }
        ColonOff();														// turn off the colon
   } else if (event == EVENT_ALARM_SWITCH_ON) {
        SetState(WAKE);         										// turn on the alarm so it will sound when necessary
		g_state.decimalBlink = TRUE;									// turn on the blinking decimal in the SS display
        ColonOn();														// turn on the colon
   } else if (event == EVENT_CO_ON) {
		PORTB |= (1<<CO_POWER_PIN);										// set CO_POWER_PIN as output
		initTimer2();													// initialize the timer that controls CO sensor voltage
   } else if (event == EVENT_CO_OFF) {
		offTimer2();													// turn off the timer that controls CO sensor voltage
		PORTB &= ~(1<<CO_POWER_PIN);									// no more current to the base of the CO transistor
        if (g_state.mode & BUZZING) {       							// if the alarm is currently sounding
	        ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
	        SilenceAlarm();                 							// turn of the sounding alarm (both song and siren)
        }
   } else if (event == EVENT_SEARCH_UP) {
//		TEA5767_search(SEARCH_UP);
//		msDelay(TEA5767_SEARCH_DELAY);
//		TEA5767_exit_search();
//		TEA5767_write();												// not sure if this is necessary...

		g_state.tune += TUNE_STEP;
		if (g_state.tune <= TOP_OF_DIAL)
		{
			TEA5767_tune(g_state.tune);									// TODO: figure out if this is synchronous
			TEA5767_write();
		}
		else
		{
			g_state.tune = BOTTOM_OF_DIAL;								// wrap around
		}
		struct TEA5767_status tea5767_status;
		TEA5767_get_status(&tea5767_status);

		LCD_DisplayTune(g_state.tune);
		EEPROM_WriteRadioStation(g_state.tune);
		g_state.previousEvent = EVENT_SEARCH_UP;			   
   } else if (event == EVENT_SEARCH_DOWN) {
//		TEA5767_search(SEARCH_DOWN);
//		msDelay(TEA5767_SEARCH_DELAY);
//		TEA5767_exit_search();
//		TEA5767_write();												// not sure if this is necessary...

		g_state.tune -= TUNE_STEP;
		if (g_state.tune >= BOTTOM_OF_DIAL) {
		 	TEA5767_tune(g_state.tune);
			TEA5767_write();
		} else {	
			g_state.tune = TOP_OF_DIAL;									// wrap around
		}
		struct TEA5767_status tea5767_status;
		TEA5767_get_status(&tea5767_status);

		LCD_DisplayTune(g_state.tune);
		EEPROM_WriteRadioStation(g_state.tune);
		g_state.previousEvent = EVENT_SEARCH_DOWN;
   } else if (event == EVENT_ONOFF_RADIO) {
		if (g_state.radioOn) {
			MuteRadio();
		} else {
			UnMuteRadio();
		}
   }
   
}