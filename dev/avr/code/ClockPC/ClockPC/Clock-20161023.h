// Clock speed; external crystal required
//#define F_CPU 16000000UL												// run CPU at 16 MHz
#define F_CPU 8000000UL													// run CPU at 8 MHz

// Standard AVR includes
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>												// Needed to use interrupts

// Standard includes
#include <string.h>														// string manipulation routines
#include <stdlib.h>
#include <stdint.h>														// has to be added to use uint8_t

#include "USART.h"
#include "macros.h"

#define DEBOUNCE_TIME  500												// microseconds 

// Clock Modes
#define BUZZING                 0x01     // 1    00000000.00000001
#define SNOOZING                0x02     // 2    00000000.00000010
#define SHOWING_TIME            0x04     // 4    00000000.00000100
#define SETTING_TIME_HOUR       0x08     // 8    00000000.00001000
#define SETTING_TIME_MINUTE     0x10     // 16   00000000.00010000
#define SETTING_WAKE_HOUR       0x20     // 32   00000000.00100000
#define SETTING_WAKE_MINUTE     0x40     // 64   00000000.01000000
#define SHOWING_TEMP            0x80     // 128  00000000.10000000
#define SETTING_TUNE            0x100    // 256  00000001.00000000
#define SHOWING_CO              0x200    // 512  00000010.00000000
#define SHOWING_FIRE            0x400    // 1024 00000100.00000000
#define SHOWING_MOTION          0x800    // 2048 00001000.00000000
#define WAKE                    0x1000   // 4096 00010000.00000000
#define PLAYING_SONG			0x2000	 // 8192 00100000.00000000

// Events
#define EVENT_NONE                  0
#define EVENT_PRESS_STATE           1
#define EVENT_PRESS_ACTION          2
#define EVENT_SWITCH_ON             3
#define EVENT_SWITCH_OFF            4
#define EVENT_VOLUME_ADJUST         5
#define EVENT_TIME_CHANGED          6
#define EVENT_WAKE_TRIGGERED        7
#define EVENT_CO_DETECTED           8
#define EVENT_SMOKE_DETECTED        9
#define EVENT_MOTION_DETECTED       10

// Pins
#define HEARTBEAT_LED_PIN       PC3 
#define BUZZER_PIN              PB4 
#define WAKE_PIN				PORTD7
#define CO_POWER_PIN            PORTB3 
#define BUTTON_STATE_PIN        DDB0
#define BUTTON_ACTION_PIN       DDB1 
#define SWITCH_PIN              DDB2 
#define TEMP_PIN                PC0
#define PHOTO_PIN               PC1	
#define CO_READ_PIN             PC2 
#define NIGHTLIGHT_PIN			DDD6

#define NUM_I2C_DEVICES   4

// I2C addresses
#define CUST_SS_LED_ADDR        32										// Custom seven segment display address
#define TC74_ADDRESS_R          0x90									// I2C Temp Sensor
#define TC74_ADDRESS_W          0x91
#define TC74_TEMP_REGISTER      0x00
#define TC74_CONFIG_REGISTER    0x01

// eeprom address
#define ALARM_ADDRESS           0x00

// Application specific defines
#define SNOOZE_TIME             10  // TODO: allow the user to set this
#define CO_ALARM_THRESHOLD      300 // TODO: Make this configurable

// Controls output to UART
#define DEBUG                   1

// Macros
#define ShiftBit(bit) (1 << (bit))
//#define ClearBit(x,y) x &= ~ShiftBit(y)
//#define SetBit(x,y) x |= ShiftBit(y)   
//#define ToggleBit(x,y) x ^= ShiftBit(y)

#define ClearBit(x,y) x &= ~y
#define SetBit(x,y) x |= y   
#define ToggleBit(x,y) x ^= y
#define ClearBitNo(x,y) x &= ~_BV(y)      // equivalent to cbi(x,y)
#define SetBitNo(x,y) x |= _BV(y)         // equivalent to sbi(x,y)

// The following defines must match what's in timeDisplaySlave, the custom seven segement display
#define  BLINK_DISPLAY    0
#define  DECIMAL_ON       1
#define  DECIMAL_BLINK    2
#define  MILITARY_TIME    3

//  Typedefs
typedef uint8_t byte;                   // I just like byte & sbyte better
typedef uint16_t doubleByte;

// Function Prototypes
int main(void);
void ProcessEvent(byte event);
void Initialize();
void ShowTime(byte hours, byte minutes);
void ShowHour(byte hours);
void ShowMinute(byte minutes);
void BlankMinuteForAlarm();
void BlankHourForAlarm();
void WriteAlarmTime(byte hours, byte minutes);
void ReadAlarmTime();
void debugAlarmTime();
void debugInt(int anInt);
void WriteTime(byte hours, byte minutes, byte seconds);
void debugTime() ;
void GetTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds);
void ReadAlarmTime(volatile byte *hours, volatile byte *minutes);
void ColonOn();
void ColonOff();
void InitAlarmSwitch();
int ConvertReadingToF(int reading);
int ReadTemp();
void InitADC0();
void InitRTCCHBit();
void initTimer0(void);
void initTimer1(void);
void initTimer2(void);
uint16_t ReadADC(int channel);
int WasCOGasAlarmTriggered(void);
byte CheckForEvents();
void BlankDisplay();
void ToggleColon();
void I2C_WriteTime(byte busAddr, byte deviceRegister);
void Cust_SS_WriteTime(byte hours, byte minutes);
byte SetSSDisplayInstruction();
void msDelay(int delay);
void ClearClockHaltBit(byte seconds);

// Structures
struct clockStateType {
    doubleByte mode;
    byte hours;
    byte minutes;
    byte seconds;
    byte alarmHours;
    byte alarmMinutes;
    byte setTimeHours;
    byte setTimeMinutes;
    int rtcRead;
    byte event;
    int timer1ElapsedSecs;
    doubleByte COreading;
	doubleByte COSampleReading;
    byte colonOn;
    byte militaryTime;
    byte decimalBlink;
	int16_t	COReadsCount; // if this doesn't work, try long long
	long long COReadsSum;
	int timer1LastSecSampled;
} clockStateType;

//  -------------------------GLOBAL VARIABLES---------------------------

// define the state variable and initialize it
struct clockStateType volatile g_state = {  .mode                = SHOWING_TIME,
                                            .hours               = 0,
                                            .minutes             = 0,
                                            .seconds             = 0,
                                            .alarmHours          = 0,
                                            .alarmMinutes        = 0,
                                            .setTimeHours        = 0,
                                            .setTimeMinutes      = 0,
                                            .rtcRead             = 0,
                                            .event               = 0,
                                            .timer1ElapsedSecs   = 0,
                                            .COreading           = 0,
											.COSampleReading	 = 0,
                                            .colonOn             = 1,
                                            .militaryTime        = 0,
                                            .decimalBlink        = 0,
											.COReadsCount		 = 0,
											.COReadsSum			 = 0,
											.timer1LastSecSampled = -1    };
                                   
// this maps characters 0 - 10 & A - Z to that which is necessary to light the
// correct LEDs on the custom seven segment display.
const int ssCharMap[38] = { 238,130,220,214,178,118,126,194,254,246,
                            250,254,108,238,124,120,246,186,130,134,
                            186,44,26,234,238,248,238,250,118,104,
                            174,174,14,186,178,220,1,0  };

static const int CHAR_A         = 10;
static const int CHAR_B         = 11;
static const int CHAR_C         = 12;
static const int CHAR_D         = 13;
static const int CHAR_E         = 14;
static const int CHAR_F         = 15;
static const int CHAR_G         = 16;
static const int CHAR_H         = 17;
static const int CHAR_I         = 18;
static const int CHAR_J         = 19;
static const int CHAR_K         = 20;
static const int CHAR_L         = 21;
static const int CHAR_M         = 22;
static const int CHAR_N         = 23;
static const int CHAR_O         = 24;
static const int CHAR_P         = 25;
static const int CHAR_Q         = 26;
static const int CHAR_R         = 27;
static const int CHAR_S         = 28;
static const int CHAR_T         = 29;
static const int CHAR_U         = 30;
static const int CHAR_V         = 31;
static const int CHAR_W         = 32;
static const int CHAR_X         = 33;
static const int CHAR_Y         = 34;
static const int CHAR_Z         = 35;
static const int CHAR_DECIMAL   = 36;
static const int CHAR_SPACE     = 37; 

volatile byte portbhistory = 0b1111111;        							// default is high because of the pull-up, correct setting
																		// for SWITCH_PIN to be determined during initialization
volatile byte count;
volatile byte sensorStable;