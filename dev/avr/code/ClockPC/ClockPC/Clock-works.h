																		// Clock speed; external crystal required
#define F_CPU 8000000UL													// run CPU at 8 MHz

// Standard AVR includes
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>												// Needed to use interrupts
#include <avr/pgmspace.h>
#include <avr/wdt.h>													// Watchdog timer

// Standard includes
#include <string.h>														// string manipulation routines
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>														// has to be added to use uint8_t

#include "USART.h"
#include "macros.h"
#include "lcdpcf8574.h"

#define DEBOUNCE_TIME  500												// microseconds 

// Clock Modes
#define BUZZING						0x01								// 1		00000000.00000000.00000000.00000001
#define SNOOZING					0x02								// 2		00000000.00000000.00000000.00000010
#define SHOWING_TIME				0x04     							// 4		00000000.00000000.00000000.00000100
#define SETTING_TIME_HOUR			0x08     							// 8		00000000.00000000.00000000.00001000
#define SETTING_TIME_MINUTE			0x10     							// 16		00000000.00000000.00000000.00010000
#define SETTING_WAKE_HOUR			0x20     							// 32		00000000.00000000.00000000.00100000
#define SETTING_WAKE_MINUTE			0x40     							// 64		00000000.00000000.00000000.01000000
#define SHOWING_TEMP				0x80     							// 128		00000000.00000000.00000000.10000000
#define SETTING_TUNE				0x100    							// 256		00000000.00000000.00000001.00000000
#define SHOWING_CO					0x200    							// 512		00000000.00000000.00000010.00000000
#define SHOWING_FIRE				0x400    							// 1024		00000000.00000000.00000100.00000000
#define SHOWING_MOTION				0x800    							// 2048		00000000.00000000.00001000.00000000
#define WAKE						0x1000   							// 4096		00000000.00000000.00010000.00000000
#define PLAYING_SONG				0x2000	 							// 8192		00000000.00000000.00100000.00000000
#define SETTING_TIME_DAY			0x4000								// 16384	00000000.00000000.01000000.00000000
#define SETTING_TIME_MONTH			0X8000								// 32768	00000000.00000000.10000000.00000000
#define SETTING_TIME_YEAR			0x10000								// 65536	00000000.00000001.00000000.00000000
#define SETTING_TIME_DAY_OF_WEEK	0x20000								// 131072	00000000.00000010.00000000.00000000
#define SETTING_MIL_TIME_SWITCH		0x40000								// 262144	00000000.00000100.00000000.00000000

// Events
#define EVENT_NONE                  0
#define EVENT_PRESS_STATE           1
#define EVENT_PRESS_ACTION          2
#define EVENT_SWITCH_ON             3
#define EVENT_SWITCH_OFF            4
#define EVENT_VOLUME_ADJUST         5
#define EVENT_MINUTE_CHANGED        6
#define EVENT_WAKE_TRIGGERED        7
#define EVENT_CO_DETECTED           8
#define EVENT_CO_ON			        9
#define EVENT_CO_OFF		       10
#define EVENT_SECOND_CHANGED	   11
#define EVENT_DAY_CHANGED		   12
#define EVENT_HOUR_CHANGED		   13

// Pins
#define HEARTBEAT_LED_PIN       PORTC3 
#define BUZZER_PIN              PORTC0 
#define WAKE_PIN				PORTD5
#define TEMP_PIN                PORTC0
#define PHOTO_PIN               PORTC1	
#define CO_READ_PIN             PORTC2 
#define NIGHTLIGHT_PIN			DDD6
#define CO_POWER_PIN            PORTB3

// Buttons and switches
#define PIN_SWITCH				PINB
#define BUTTON_STATE_PIN        DDB0
#define BUTTON_STATE_INT		PCINT0
#define BUTTON_ACTION_PIN       DDB1
#define BUTTON_ACTION_INT		PCINT1
#define ALARM_SWITCH_PIN        DDB2
#define ALARM_SWITCH_INT		PCINT2
#define CO_SWITCH_PIN			DDB5
#define CO_PIN_INT				PCINT5

#define PCMSK_SWITCH			PCMSK0
#define PCIE_SWITCH				PCIE0

#define NUM_I2C_DEVICES			4

																		// I2C addresses
#define CUST_SS_LED_ADDR        0x20									// 32 Custom seven segment display address
#define LCD_ADDRESS_NOT_USED	0x27									// 39 Liquid Crystal Display
#define TC74_ADDRESS_R          0x90									// 144 I2C Temp Sensor read
#define	DS1307_ADDRESS			0xD0									// 208 Real Time Clock
#define TC74_ADDRESS_W          0x91									// 145 I2C Temp Sensor write
#define TC74_TEMP_REGISTER      0x00									// 0 Temp register (?)
#define TC74_CONFIG_REGISTER    0x01									// 1 Temp config register (?)
																		// 78? (39 x 2)

																		// eeprom addresses
#define ALARM_ADDRESS           0x00
#define MILITARY_TIME_ADDRESS   0x03

																		// Application specific defines
#define SNOOZE_TIME							10  						// TODO: allow the user to set this
#define CO_ALARM_THRESHOLD					300							// typically 300 TODO: Make this configurable
#define CONSECUTIVE_CO_READING_THRESHOLD	10							// typically 10

#define HOUR_TENS_X		0												// controls the position of the time on the LCD
#define HOUR_TENS_Y		1
#define HOUR_ONES_X		1
#define HOUR_ONES_Y		1

#define MINUTE_TENS_X	3
#define MINUTE_TENS_Y	1
#define MINUTE_ONES_X	4
#define MINUTE_ONES_Y	1

#define SECOND_TENS_X	6
#define SECOND_TENS_Y	1
#define SECOND_ONES_X	7
#define SECOND_ONES_Y	1

#define COLON_1_X		2
#define COLON_1_Y		1
#define COLON_2_X		5
#define COLON_2_Y		1

#define YEAR_THOU_X			6											// Controls the position of the date on the RTC					
#define YEAR_THOU_Y			0
#define YEAR_HUND_X			7
#define YEAR_HUND_Y			0
#define YEAR_TENS_X			8
#define YEAR_TENS_Y			0
#define YEAR_ONES_X			9
#define YEAR_ONES_Y			0

#define MONTH_TENS_X		0
#define MONTH_TENS_Y		0
#define MONTH_ONES_X		1
#define MONTH_ONES_Y		0

#define DAY_TENS_X			3
#define DAY_TENS_Y			0
#define DAY_ONES_X			4
#define DAY_ONES_Y			0

#define TEMP_X				12
#define TEMP_Y				1

#define FORWARD_SLASH_1_X	2
#define FORWARD_SLASH_1_Y	0
#define FORWARD_SLASH_2_X	5
#define FORWARD_SLASH_2_Y	0

#define DAY_OF_WEEK_X		11
#define DAY_OF_WEEK_Y		0

//#define DS1307            0xD0										// defined in Clock.h with all the other I2C addresses 208 dec,I2C bus address of DS1307 RTC
#define SECONDS_REGISTER    0x00
#define MINUTES_REGISTER    0x01
#define HOURS_REGISTER      0x02
#define DAYOFWK_REGISTER    0x03
#define DAYS_REGISTER       0x04
#define MONTHS_REGISTER     0x05
#define YEARS_REGISTER      0x06
#define CONTROL_REGISTER    0x07
#define RAM_BEGIN           0x08
#define RAM_END             0x3F
#define F_SCL 100000L													// I2C clock speed 10 KHz
#define READ 1
#define TW_START 0xA4													// send start condition (TWINT,TWSTA,TWEN)
#define TW_STOP 0x94													// send stop condition (TWINT,TWSTO,TWEN)
#define TW_ACK 0xC4														// return ACK to slave
#define TW_NACK 0x84													// don't return ACK to slave
#define TW_SEND 0x84													// send data (TWINT,TWEN)
#define TW_READY (TWCR & 0x80)											// ready when TWINT returns to logic 1.
#define TW_STATUS (TWSR & 0xF8)											// returns value of status register
#define I2C_Stop() TWCR = TW_STOP										// inline macro for stop condition

																		// The following defines must match what's in timeDisplaySlave, 
																		// the custom seven segment display
#define  BLINK_DISPLAY    0
#define  DECIMAL_ON       1
#define  DECIMAL_BLINK    2
																		// Controls output to UART
#define DEBUG                   1

/************************************************************************/
/* Macros					                                            */
/************************************************************************/
#define ShiftBit(bit) (1 << (bit))
#define ClearBit(x,y) x &= ~y
#define SetBit(x,y) x |= y   
#define ToggleBit(x,y) x ^= y
#define ClearBitNo(x,y) x &= ~_BV(y)									// equivalent to cbi(x,y)
#define SetBitNo(x,y) x |= _BV(y)										// equivalent to sbi(x,y)

/************************************************************************/
/* Typedefs					                                            */
/************************************************************************/
typedef uint8_t byte;													// I just like byte & sbyte better
typedef uint16_t doubleByte;

/************************************************************************/
/* Function Prototypes                                                  */
/************************************************************************/
void debug(const char myString[]) 										;
void debugNoLF(const char myString[]) 									;
void debugInt(signed int anInt)                                         ;
void debugMetric(const char myString[], signed int anInt) 				;
void debugAlarmTime() 													;
void debugTime() 														;
void I2C_Init()															;// Calculate TWBR setting for I2C bit rate
byte I2C_Detect(byte addr)												;// look for a device at the specified address
byte I2C_FindDevice(byte start)											;// returns the count of I2C devices found
void I2C_Start (byte slaveAddr)											;// Look for a device at the specified address.  Same as I2C_Detect.
byte I2C_Write (byte data)												;// sends a data byte to slave
byte I2C_ReadACK ()														;// reads a data byte from slave
byte I2C_ReadNACK () 													;// reads a data byte from slave
void I2C_WriteByte(byte busAddr, byte data)								;// Writes a byte of data to the I2C slave
void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data)	;// Writes a byte of data to the device register
byte I2C_ReadRegister(byte busAddr, byte deviceRegister)				;// Reads a byte of data from the device register
void I2C_FindDevices()													;// Makes 100 attempts to find I2C devices
void SS_DisplayChars(byte charOne, byte charTwo, byte charThree, byte charFour) ;// Write 4 chars to the SS display
byte SS_SetDisplayInstruction()											;// set the SS display I2C instruction byte for decimal and military time
void SS_DisplayTime(byte hours, byte minutes)							;// Write hours and minutes to the SS display
void SS_DisplayTimeAlarmSetHours(byte hours)							;// Display 'set alarm hours' to SS display
void SS_DisplayTimeAlarmSetMinutes(byte minutes)						;// Display 'set alarm minutes' to SS display
void SS_DisplayTimeSetDays(byte days)									;// Display 'set day of month' to SS display
void SS_DisplayTimeSetDayOfWeek(byte dayOfWeek)							;// Display 'set day of week' to SS display
void SS_DisplayTimeSetMonths(byte months)								;// Display 'set month of year' to SS display
void SS_DisplayTimeSetYears(byte years)									;// Display 'set year' to SS display
void SS_DisplayTimeSetHours(byte hours)									;// Display 'set time hours' to SS display
void SS_DisplayTimeSetMinutes(byte minutes)								;// Display 'set time minutes' to SS display
void SS_DisplayTimeSetMilitaryTime(byte milTimeSwitch)					;// Display 'set military time' to SS display
void SS_BlankDisplay()													;// Blank out the SS display
void ToggleColon()														;// Toggle the SS display colon
void ColonOn()															;// Set the SS display colon on
void ColonOff()															;// Set the SS display colon off
void LCD_Reset()														;// Clear out the LCD display
void LCD_DisplayTemp(byte tempF)										;// Display current temperature in Fahrenheit on LCD
void LCD_DisplayHours(byte hours)										;// Display hours portion of the time
void LCD_DisplayMinutes(byte minutes)									;// Display the minutes portion of the time
void LCD_DisplaySeconds(byte seconds)									;// Display the seconds portion of the time
void LCD_DisplayDate(uint16_t years, byte months, byte day, byte dayOfWeek) ;// Display date on the LCD
void DS1307_ReadTime(byte *hours, byte *minutes, byte *seconds)			;// returns hours, minutes, and seconds in BCD format
void DS1307_ReadDate(byte *years, byte *months, byte *days, byte *dayOfWeek) ;// returns months, days, and years in BCD format
void DS1307_WriteTime(byte hours, byte minutes, byte seconds)			;// Write time to the DS1307
void DS1307_WriteDate(uint16_t years, byte months, byte days, byte dayOfWeek) ;// Write date to the DS1307
void DS1307_ClearClockHaltBit(byte seconds)								;// When clock halt bit is cleared (0) the oscillator is enabled, time advances on the chip
void initTimer0(void) 													;// Night Light - Initialize the timer, this timer is for the photo resistive sensor
void initTimer1(void) 													;// Real Time Clock
void initTimer2(void) 													;// CO sensor - this is for the two different voltages needed for the CO sensor, 1.4v & 5v
void offTimer2(void)													;// Turn off PMW for the CO sensor
void InitInturrupts()													;// Initialize interrupts for buttons and switches
ISR(TIMER0_OVF_vect) 													;// Timer0 overflow interrupt, this is for the night light
ISR (TIMER1_COMPA_vect)													;// Timer1 interrupt, this is the real time clock, triggered every second
ISR (TIMER2_OVF_vect)													;// Timer2 overflow interrupt, PMW for the CO sensor
ISR (PCINT0_vect)														;// Interrupts for buttons and switches
void InitADC0()															;// Initialize ADC
uint16_t ReadADC(int channel)											;// Read analog to digital converter
void InitRTC()															;// Initialize RTC, read date and time from DS1307.
void ReadTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds) ;// Read time
void ReadDate(volatile uint16_t *inYears, volatile byte *inMonths, volatile byte *inDay, volatile byte *inDayOfWeek);
byte DayOfWeek(int y, byte m, byte d)									;// Implementation due to Tomohiko Sakamoto, calculate day of week from date
void ShowTime(byte hours, byte minutes)									;// Update SS display with the time; hours and minutes
void InitAlarms() 														;// Determine if the alarm switch is on or off and process the appropriate event
void SoundAlarm() 														;// Trun on the CO alarm. TODO: make this stop after a minute (configurable time)
void SoundWake()														;// Turn on the wake alarm
void SilenceAlarm()														;// Turn of the wake alarm
void SnoozeAlarm()														;// Snooze the wake alarm for SNOOZE_TIME
int WasWakeAlarmTriggered() 											;// TODO: pass in the structure by value
void EEPROM_WriteAlarmTime(byte hours, byte minutes)					;// Write the wake alarm time to eeprom
void EEPROM_ReadAlarmTime(volatile byte *hours, volatile byte *minutes)	;// Read the wake alarm time from eeprom
void EEPROM_WriteMilitaryTimeSwitch(byte military_time_switch)			;// Write the military time switch setting to eeprom
void EEPROM_ReadMilitaryTimeSwitch(volatile byte *military_time_switch)	;// Read the military time switch setting from eeprom
double GetInternalTemp(void)											;// get the chip temperature and convert to farenheit
void ShowTemp(int tempF)												;// Write room temp to SS display
signed int ConvertToFarenheit(unsigned short celsius)					;// Convert from celsius to farenheit
int WasCOGasAlarmTriggered()											;// returns true if the CO alarm is tripped
void DisplayWarnForCO()													;// write "CO" to the seven segment display
void msDelay(int delay)  												;// Wait for 'delay' milliseconds
void SoundOneShortBeep()												;// 250ms beep
void FlashLED()															;// turn on the heartbeat LED for 100ms
void ToggleLED()														;// Toggle the heartbeat LED
byte DecToBcd(byte val)													;// Convert normal decimal numbers to binary coded decimal
byte BcdToDec(byte val)													;// Convert binary coded decimal to normal decimal numbers
const char *DayOfWeekNumToString(byte dayOfWeekNum)						;// Convert a numbered day of the week to an abbreviated name of the day
char NotLeap(void)														;// Check for Leap Year
void SetState(uint32_t state)											;// Given a state, sets it on g_state.mode
void ClearState(uint32_t state)											;// Clears a state from g_state.mode
void InitAVR()															;// prepare pins for input and output
int main(void);
void Initialize()														;// called at startup to prepare all peripherals
byte CheckForEvents() 													;// the mainline of processing
void ProcessEvent(byte event)											;// called continuously from main; processes events

/************************************************************************/
/* Structures				                                            */
/************************************************************************/
struct clockStateType {
    uint32_t mode;
	uint16_t years;
	byte months;
	byte dayOfWeek;
	byte day;
    byte hours;
    byte minutes;
    byte seconds;
    byte alarmHours;
    byte alarmMinutes;
    int rtcRead;
    byte event;
    int timer1ElapsedSecs;
    doubleByte COreading;
	doubleByte COSampleReading;
    byte colonOn;
    byte militaryTime;
    byte decimalBlink;
	int16_t	COReadsCount; 
	long long COReadsSum;
	int16_t COReadsMin;
	int16_t COReadsMax;
	int timer1LastSecSampled;
} clockStateType;

/************************************************************************/
/* Global Variables			                                            */
/************************************************************************/
																		// define the state variable and initialize it
struct clockStateType volatile g_state = {  .mode                = SHOWING_TIME,
											.years				 = 0,
											.months				 = 0,
											.dayOfWeek			 = 0,
											.day				 = 0,
                                            .hours               = 0,
                                            .minutes             = 0,
                                            .seconds             = 0,
                                            .alarmHours          = 0,
                                            .alarmMinutes        = 0,
                                            .rtcRead             = 0,
                                            .event               = 0,
                                            .timer1ElapsedSecs   = 0,
                                            .COreading           = 0,
											.COSampleReading	 = 0,
                                            .colonOn             = 1,
                                            .militaryTime        = 0,
                                            .decimalBlink        = 0,
											.COReadsCount		 = 0,
											.COReadsSum			 = 0,
											.COReadsMin			 = 1024,
											.COReadsMax			 = 0,
											.timer1LastSecSampled = -1    };
                                   
																		// this maps characters 0 - 10 & A - Z to that which is necessary to light the
																		// correct LEDs on the custom seven segment display.
const int ssCharMap[38] = { 238,130,220,214,178,118,126,194,254,246,
                            250,254,108,238,124,120,246,186,130,134,
                            186,44,26,234,238,248,238,250,118,104,
                            174,174,14,186,178,220,1,0  };

static const int CHAR_A         = 10;
//static const int CHAR_B       = 11;
static const int CHAR_C         = 12;
static const int CHAR_D         = 13;
static const int CHAR_E         = 14;
static const int CHAR_F         = 15;
//static const int CHAR_G       = 16;
static const int CHAR_H         = 17;
//static const int CHAR_I       = 18;
//static const int CHAR_J       = 19;
//static const int CHAR_K       = 20;
//static const int CHAR_L       = 21;
static const int CHAR_M         = 22;
//static const int CHAR_N       = 23;
static const int CHAR_O         = 24;
//static const int CHAR_P       = 25;
//static const int CHAR_Q       = 26;
static const int CHAR_R         = 27;
static const int CHAR_S         = 28;
static const int CHAR_T         = 29;
//static const int CHAR_U       = 30;
//static const int CHAR_V       = 31;
static const int CHAR_W         = 32;
//static const int CHAR_X       = 33;
static const int CHAR_Y         = 34;
//static const int CHAR_Z       = 35;
//static const int CHAR_DECIMAL = 36;
static const int CHAR_SPACE     = 37; 

volatile byte portbhistory = 0b1111111;        							// default is high because of the pull-up, correct setting
																		// for SWITCH_PIN to be determined during initialization
volatile byte count;
volatile byte sensorStable;