﻿//-----------------------------------------------------------------------------
// Clock.c
// A clock with the following features:
//	- I2C seven segment display using discrete components
//	- beeping alarm using active piezo buzzer and a 555 timer
//	- I2C DS1307 Real Time Clock with battery backup
//	- MQ-7 CO detector operating at two voltages; 1.4v & 5.0v.
//	- night light via pulse width modulation
//	- temperature
//
//	All three timers are used; one for an internal RTC, one for
//  to control the voltage to the CO sensor, and one to set
//  the brightness of the LED/night light.
//
// Author:		Daniel Murphy <dmurphy@comcast.net>
// Contributors:Bruce E. Hall <bhall66@gmail.com>
//              Elliott Williams (Make: AVR Programming)    
// Version:		0.6
// Date:		2015-09-09
// Device:		ATMega328P-PU @ 8mHz
// Language:	C
//
/*  Daniel J. Murphy hereby disclaims all copyright interest in this
	program written by Daniel J. Murphy.

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/************************************************************************/
/* Includes                                                             */
/************************************************************************/
#include "Clock.h"
/************************************************************************/
/* Debug Routines                                                       */
/************************************************************************/
void debug(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
        printString("\r\n");
    }
}

void debugNoLF(const char myString[]) {
    if (DEBUG) {
        byte i = 0;
        while (myString[i]) {
            transmitByte(myString[i]);
            i++;
        }
    }
}

void debugInt(signed int anInt) {
    if (DEBUG) {
        char myInt[4];
        itoa(anInt,myInt,10);
        debug(myInt);
    }
}

void debugMetric(const char myString[], signed int anInt) {
	debugNoLF(myString);debugNoLF(": ");
	debugInt(anInt);
    printString("\r\n");
}

void debugAlarmTime() {
	char myHours[4];
	itoa(g_state.alarmHours,myHours,10);
	debugNoLF("ALARM HOURS: ");
	debug(myHours);

	char myMinutes[4];
	itoa(g_state.alarmMinutes,myMinutes,10);
	debugNoLF("ALARM MINUTES: ");
	debug(myMinutes);
}

void debugTime() {
	char myHours[4];
	itoa(g_state.hours,myHours,10);
	debugNoLF("HOURS: ");
	debug(myHours);
	debug("");

	char myMinutes[4];
	itoa(g_state.minutes,myMinutes,10);
	debugMetric("MINUTES", g_state.minutes);
}

/************************************************************************/
/* I2C Routines                                                         */
/************************************************************************/
																		// The standard clock rate is 100 KHz, and set by I2C_Init. 
																		// It depends on the AVR osc. freq.
#define F_SCL 100000L													// I2C clock speed 10 KHz
#define READ 1                          
#define TW_START 0xA4													// send start condition (TWINT,TWSTA,TWEN)
#define TW_STOP 0x94													// send stop condition (TWINT,TWSTO,TWEN)
#define TW_ACK 0xC4														// return ACK to slave
#define TW_NACK 0x84													// don't return ACK to slave
#define TW_SEND 0x84													// send data (TWINT,TWEN)
#define TW_READY (TWCR & 0x80)											// ready when TWINT returns to logic 1.
#define TW_STATUS (TWSR & 0xF8)											// returns value of status register
#define I2C_Stop() TWCR = TW_STOP										// inline macro for stop condition

void I2C_Init()	{														// at 16 , the SCL frequency 
																		// will be 16/(16+2(TWBR)), assuming 
																		// prescalar of 0. So for 100KHz SCL, 
																		// TWBR = ((F_CPU/F_SCL)-16)/2 = ((16/0.1)-16)/2 = 144/2 
																		// = 72.
    debug("I2C_INIT()");
    TWSR = 0;															// set prescalar to zero
    TWBR = ((F_CPU/F_SCL)-16)/2;										// set SCL frequency in TWI bit register
}

byte I2C_Detect(byte addr)	{											// look for device at specified address; 
																		// return 1=found, 0=not found
    TWCR = TW_START;													// send start condition
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
    }																	// wait
    TWDR = addr;														// load device's bus address
    TWCR = TW_SEND;														// and send it
    i = 0;
    while ((!TW_READY) && (i++ < 101)) {
    }																	// wait
    if (TW_STATUS!=0x18) {
        //debug("Device not detected.");
        //debugNoLF("Address: ");
        //debugInt(addr);
    }
    return (TW_STATUS==0x18);											// return 1 if found; 0 otherwise
}

byte I2C_FindDevice(byte start)	{										// returns with address of first device found; 0=not found
    byte foundCount;
    foundCount = 0;
    for (byte addr=start;addr<0xFF;addr++)								// search all 256 addresses
    {
        if (I2C_Detect(addr)) {											// I2C detected?
            foundCount++;
            debugNoLF("I2C ADDRESS FOUND: ");
            debugInt(addr);       
        }
    }
    return foundCount;													// none detected, so return 0.
}

void I2C_Start (byte slaveAddr) {
    I2C_Detect(slaveAddr);
}

byte I2C_Write (byte data)	{											// sends a data byte to slave
    TWDR = data;														// load data to be sent
    TWCR = TW_SEND;														// and send it
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
	//printString("/");													// wait
    }
    return (TW_STATUS!=0x28);
}

byte I2C_ReadACK ()	{													// reads a data byte from slave
    TWCR = TW_ACK;														// ack = will read more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString("+");
    }																	// wait
    return TWDR;
}

byte I2C_ReadNACK () {													// reads a data byte from slave
    TWCR = TW_NACK;														// nack = not reading more data
    int i = 0;
    while ((!TW_READY) && (i++ < 101)) {
        printString(".");
    }																	// wait
    printString("\r\n");
    return TWDR;
}

void I2C_WriteByte(byte busAddr, byte data) {
    I2C_Start(busAddr);													// send bus address
    I2C_Write(data);													// then send the data byte
    I2C_Stop();
}

void I2C_WriteRegister(byte busAddr, byte deviceRegister, byte data) {
    I2C_Start(busAddr);													// send bus address
    I2C_Write(deviceRegister);											// first byte = device register address
    I2C_Write(data);													// second byte = data for device register
    I2C_Stop();
}

byte I2C_ReadRegister(byte busAddr, byte deviceRegister) {
    byte data = 0;
    I2C_Start(busAddr);													// send device address
    I2C_Write(deviceRegister);											// set register pointer
    I2C_Start(busAddr+READ);											// restart as a read operation
    data = I2C_ReadNACK();												// read the register data
    I2C_Stop();															// stop
    return data;
}

void FindI2CDevices() {
	int foundCount;
	int findI2CTries;

	foundCount = 0;
	findI2CTries = 0;
	foundCount = I2C_FindDevice (0x00);
	while ((foundCount < (NUM_I2C_DEVICES - 1)) && (findI2CTries < 101))
	{
		findI2CTries += 1;
		debugMetric("FIND DEVICE ATTEMPT #", findI2CTries);
		foundCount = I2C_FindDevice (0x00);
		msDelay(250);
	}
}

/************************************************************************/
/* Customer Seven Segment Display Routines                              */
/************************************************************************/
void Cust_SS_WriteChars(byte charOne, byte charTwo, byte charThree, byte charFour) {
    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    byte instruction = SetSSDisplayInstruction();
    I2C_Write(instruction);												// first byte = device register address
    I2C_Write(ssCharMap[charOne]);										// hours tens pos
    I2C_Write(ssCharMap[charTwo]);										// hours ones pos
    I2C_Write(ssCharMap[charThree]);									// minute tens pos
    I2C_Write(ssCharMap[charFour]);										// minute ones pos
    I2C_Stop();
	debugMetric("Instruction: ", instruction);
	debugMetric("charOne: ", ssCharMap[charOne]);
	debugMetric("charOne: ", ssCharMap[charTwo]);
	debugMetric("charOne: ", ssCharMap[charThree]);
	debugMetric("charOne: ", ssCharMap[charFour]);
	
}

byte SetSSDisplayInstruction() {
    byte instruction = 0;
    if (g_state.militaryTime) {
        SetBitNo(instruction, MILITARY_TIME);
    }
    
    if (g_state.colonOn) {
        SetBitNo(instruction, DECIMAL_ON);
    }
    
    if (g_state.decimalBlink) {
        SetBitNo(instruction, DECIMAL_BLINK);
    }
    return instruction;     
}

#define HOUR_TENS_X		0
#define HOUR_TENS_Y		0
#define HOUR_ONES_X		1
#define HOUR_ONES_Y		0

#define MINUTE_TENS_X	3
#define MINUTE_TENS_Y	0
#define MINUTE_ONES_X	4
#define MINUTE_ONES_Y	0

#define SECOND_TENS_X	6
#define SECOND_TENS_Y	0
#define SECOND_ONES_X	7
#define SECOND_ONES_Y	0

#define COLON_1_X		2
#define COLON_1_Y		0
#define COLON_2_X		5
#define COLON_2_Y		0

void Cust_LCD_WriteTime(byte hours, byte minutes, byte seconds) {
	char buf[10];

    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;
    int tensSeconds = seconds / 10;
    int onesSeconds = seconds - (seconds / 10) * 10;
	

	itoa(tensHours, buf, 10);
	lcd_gotoxy(HOUR_TENS_X,HOUR_TENS_Y);
	lcd_putc(*buf);

	itoa(onesHours, buf, 10);
	lcd_gotoxy(HOUR_ONES_X,HOUR_ONES_Y);
	lcd_putc(*buf);

	lcd_gotoxy(COLON_1_X,COLON_1_Y);
	lcd_putc(':');

	itoa(tensMinutes, buf, 10);
	lcd_gotoxy(MINUTE_TENS_X,MINUTE_TENS_Y);
	lcd_putc(*buf);

	itoa(onesMinutes, buf, 10);
	lcd_gotoxy(MINUTE_ONES_X,MINUTE_ONES_Y);
	lcd_putc(*buf);

	lcd_gotoxy(COLON_2_X,COLON_2_Y);
	lcd_putc(':');

	itoa(tensSeconds, buf, 10);
	lcd_gotoxy(SECOND_TENS_X,SECOND_TENS_Y);
	lcd_putc(*buf);

	itoa(onesSeconds, buf, 10);
	lcd_gotoxy(SECOND_ONES_X,SECOND_ONES_Y);
	lcd_putc(*buf);
}

void LCDReset() {
	lcd_led(0);
	lcd_home();
	lcd_clrscr();
	lcd_gotoxy(0,0);
}

void Cust_SS_WriteTime(byte hours, byte minutes) {
	debug("Cust_SS_WriteTime()");
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;
    
    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    
    byte instruction = SetSSDisplayInstruction();
        
    I2C_Write(instruction);												// first byte = instruction settings
    if (tensHours) {
        I2C_Write(ssCharMap[tensHours]);								// hours tens pos
    } else {
        I2C_Write(ssCharMap[CHAR_SPACE]);								// hours tens pos
    }
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[tensMinutes]);									// minute tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minute ones pos
    I2C_Stop();
	debugMetric("tensHours: ", tensHours);
	debugMetric("onesHours", onesHours);
	debugMetric("tensMinutes: ", tensMinutes);
	debugMetric("onesMinutes: ", onesMinutes);
}

void Cust_SS_WriteTimeAlarmSetHours(byte hours) {
    int tensHours = hours / 10;
    int onesHours = hours - (hours / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[tensHours]);									// hours tens pos
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_H]);      
    I2C_Stop();
}

void Cust_SS_WriteTimeAlarmSetMinutes(byte minutes) {
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[CHAR_A]);      
    I2C_Write(ssCharMap[CHAR_SPACE]);      
    I2C_Write(ssCharMap[tensMinutes]);									// minutes tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minutes ones pos
    I2C_Stop();
}

void Cust_SS_WriteTimeSetHours(byte hours) {
    int tensHours = hours / 1;
    int onesHours = hours - (hours / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);                             
    I2C_Write(ssCharMap[tensHours]);									// hours tens pos
    I2C_Write(ssCharMap[onesHours]);									// hours ones pos
    I2C_Write(ssCharMap[CHAR_S]);      
    I2C_Write(ssCharMap[CHAR_H]);      
    I2C_Stop();
}

void Cust_SS_WriteTimeSetMinutes(byte minutes) {
    int tensMinutes = minutes / 10;
    int onesMinutes = minutes - (minutes / 10) * 10;

    I2C_Start(CUST_SS_LED_ADDR);										// send bus address
    I2C_Write(0);														// colon off during set time
    I2C_Write(ssCharMap[CHAR_S]);      
    I2C_Write(ssCharMap[CHAR_SPACE]);      
    I2C_Write(ssCharMap[tensMinutes]);									// minutes tens pos
    I2C_Write(ssCharMap[onesMinutes]);									// minutes ones pos
    I2C_Stop();
}

/************************************************************************/
/* DS1307 Routines                                                      */
/************************************************************************/
//#define DS1307            0xD0										// defined in Clock.h with all the other I2C addresses 208 dec,I2C bus address of DS1307 RTC
#define SECONDS_REGISTER    0x00
#define MINUTES_REGISTER    0x01
#define HOURS_REGISTER      0x02
#define DAYOFWK_REGISTER    0x03
#define DAYS_REGISTER       0x04
#define MONTHS_REGISTER     0x05
#define YEARS_REGISTER      0x06
#define CONTROL_REGISTER    0x07
#define RAM_BEGIN           0x08
#define RAM_END             0x3F

void DS1307_GetTime(byte *hours, byte *minutes, byte *seconds)	{		// returns hours, minutes, and seconds in BCD format
//  debug("DS1307_GetTime()");
    *hours = I2C_ReadRegister(DS1307,HOURS_REGISTER);
    *minutes = I2C_ReadRegister(DS1307,MINUTES_REGISTER);
    *seconds = I2C_ReadRegister(DS1307,SECONDS_REGISTER);
    if (*hours & 0x40)													// 12hr mode:
        *hours &= 0x1F;													// use bottom 5 bits (pm bit = temp & 0x20)
    else *hours &= 0x3F;												// 24hr mode: use bottom 6 bits
}

void DS1307_GetDate(byte *months, byte *days, byte *years)	{			// returns months, days, and years in BCD format
    *months = I2C_ReadRegister(DS1307,MONTHS_REGISTER);
    *days = I2C_ReadRegister(DS1307,DAYS_REGISTER);
    *years = I2C_ReadRegister(DS1307,YEARS_REGISTER);
}

/************************************************************************/
/* Misc Routines                                                        */
/************************************************************************/
void msDelay(int delay)  {												// put into a routine
	for (int i=0;i<delay;i++)											// to remove code inlining
	_delay_ms(1);														// at cost of timing accuracy
}

void OneShortBeep() {
	debug("ONESHORTBEEP()");
	SetBitNo(PORTC,BUZZER_PIN);											// set BUZZER_PIN HIGH
	msDelay(250);	
	ClearBitNo(PORTC,BUZZER_PIN);										// set BUZZER_PIN LOW
}

void FlashLED() {
	//  debug("FlashLED()");
	SetBitNo(PORTC,HEARTBEAT_LED_PIN);
	msDelay(100);
	ClearBitNo(PORTC,HEARTBEAT_LED_PIN);
}

void ToggleLED() {
	PORTC ^= (1<<HEARTBEAT_LED_PIN);
}

void ToggleColon() {
    if (g_state.mode & WAKE) {    
        if (g_state.colonOn) {
            ColonOff();
        } else {
            ColonOn();
        }
    }
}

void ColonOn() {
    g_state.colonOn = 1;
}

void ColonOff() {
    g_state.colonOn = 0;
}

void BlankDisplay() {
    Cust_SS_WriteChars(CHAR_SPACE,CHAR_SPACE,CHAR_SPACE,CHAR_SPACE);
}

/************************************************************************/
/* Initialize the AVR                                                   */
/************************************************************************/
void InitAVR() {
	debug("INITAVR()");
	DDRB |= (1 << CO_POWER_PIN);										// Configure CO power pin for output.
	DDRC |= (1 << BUZZER_PIN);											// Configure buzzer pin for output.
	DDRD |= (1 << WAKE_PIN);											// Configure wake pin for output.
    ClearBitNo(PORTC,BUZZER_PIN);
    SetBitNo(PORTD,WAKE_PIN);
    DDRD |= (1 << NIGHTLIGHT_PIN);										// Configure nightlight pin for output
    DDRB &= ~(1 << BUTTON_STATE_PIN);									// Clear the BUTTON_STATE_PIN, 
	DDRB &= ~(1 << BUTTON_ACTION_PIN);									// BUTTON_ACTION_PIN, 
	DDRB &= ~(1 << ALARM_SWITCH_PIN);									// and ALARM_SWITCH_PIN pin (sets them to input).
	DDRB &= ~(1 << DDB4);												// unused set to input so as not to effect portbhistory / PIN_SWITCH
	DDRB &= ~(1 << CO_SWITCH_PIN);										// and CO_SWITCH_PIN pin (sets them to input).
    PORTB |= ((1 << PORTB0) | (1 << PORTB1) | (1 << PORTB2));			// Turn On the Pull-ups TODO: is this necessary, we're using pull up resistors?
	DDRC |= (1 << HEARTBEAT_LED_PIN);									// Configure the heartbeat LED for output.
}

/************************************************************************/
/* Timers                                                               */
/************************************************************************/
/* Night light */
void initTimer0(void) {													// Initialize the timer, this timer is for the photo resistive sensor
    OCR0A = 0;															// set this for PWM duty cycle    
    TCCR0A |= (1 << COM0A1);											// set none-inverting mode        
    TCCR0A |= (1 << WGM01) | (1 << WGM00);								// set fast PWM Mode              
    TCCR0B |= (1 << CS01);												// set prescaler to 8, starts PWM 
    TIMSK0 |= (1 << TOIE0);												// overflow interrupt enable      
}

/* Real time clock */
void initTimer1(void) {													// this is the real time clock
    OCR1A = 31249;														// 31249 - when we hit this # 1 sec has elapsed @ 8mhz w/ prescaler @ 256
    TCCR1B |= (1 << WGM12);												// Mode 4, CTC on OCR1A
    TIMSK1 |= (1 << OCIE1A);											// Set interrupt on compare match
    TCCR1B |= (1 << CS12);												// set prescaler to 1024 and start the timer
}

/* CO sensor */
void initTimer2(void) {													// this is for the two different voltages needed for the CO sensor, 1.4v & 5v
	DDRB |= (1 << CO_POWER_PIN);										// output pin for timer2 is OC2A which is PB3
	OCR2A = 0;															// set PWM for 5v
	TCCR2A |= (1 << COM2A1) | (1 << COM2A0);							// Set OC2A on Compare Match, clear OC2A at BOTTOM (inverting mode)
	TCCR2A |= (1 << WGM21) | (1 << WGM20);								// set fast PWM Mode
	TCCR2B |= (1 << CS21);												// set prescaler to 8 and starts PWM
	TIMSK2 |= (1 << TOIE2);												// overflow interrupt enable
}

void offTimer2(void) {
	TCCR2B &= ~((1 << CS20)|(1 << CS21)|(1 << CS22));					// Turn off PWM for the CO sensor, No clock source (Timer/Counter stopped).
	TIMSK2 &= ~(1<<TOIE2);												// Disable the timer overflow interrupt
	DDRB &= ~(1 << CO_POWER_PIN);										// CO_POWER_PIN no longer for output
}

/************************************************************************/
/* Interrupts                                                           */
/************************************************************************/
void initInturrupts() {
    PCICR |= (1 << PCIE_SWITCH);										// Pin Change Interrupt Control Register, set PCIE0 to enable PCMSK0 scan
																		// Pin Change Mask Register 0 for port B
    PCMSK_SWITCH |= (1 << BUTTON_STATE_INT) | (1 << BUTTON_ACTION_INT) | (1 << ALARM_SWITCH_INT) | (1 << CO_PIN_INT);			
																		// Set PCINT0 to trigger an interrupt on state change 
}

ISR(TIMER0_OVF_vect) {													// Timer0 overflow interrupt, this is for the night light 
    count++;
    if (count == 255) {
        float photoVolts =  255 - (float) ReadADC(PHOTO_PIN)/4;			// bright = 24, dark = 239, range of 215
		if (photoVolts > 239) {
			photoVolts = 239;
		}
		if (photoVolts < 24) {
			photoVolts = 24;
		}
		photoVolts = (photoVolts - 24) * 1.186;
        //debug("photoVolts"); debugInt(photoVolts);
        OCR0A = photoVolts;
        count=0;
    }
}

ISR (TIMER1_COMPA_vect)	{												// Timer1 interrupt, this is the real time clock
	//debug("ISR (TIMER1_COMPA_vect)");
    if (g_state.rtcRead) {
        g_state.timer1ElapsedSecs += 1;									// This is used for varying the voltage to the CO detector
        g_state.seconds += 1;
		ToggleLED();
		//debug("g_state.seconds:"); debugInt(g_state.seconds);
        if (g_state.seconds == 60) {
            g_state.seconds = 0;
            g_state.minutes += 1;
            if (g_state.minutes == 60) {
                g_state.minutes = 0;
                g_state.hours += 1;
                if (g_state.hours == 24) {
                    g_state.hours = 0;
                }
            }
            g_state.event = EVENT_MINUTE_CHANGED; 
            ProcessEvent(g_state.event);								// not sure why this is necessary but without it RTC doesn't work
        }
        g_state.event = EVENT_SECOND_CHANGED;
        ProcessEvent(g_state.event);
    }
}

/* check for leap year */
char not_leap(void)
{
	if (!(g_state.years % 100)) {
		return (char)(g_state.years % 400);
		} else {
		return (char)(g_state.years % 4);
	}
}

ISR (TIMER2_OVF_vect) {													// Timer2 overflow interrupt, PMW for the CO sensor
	//debug("TIMER2_OVF_vect");
	if((g_state.timer1ElapsedSecs >= 90) && (OCR2A == 183)) {
		sensorStable += 1;
		g_state.timer1ElapsedSecs = 0;
		g_state.COreading = (g_state.COReadsSum - g_state.COReadsMax - g_state.COReadsMin) / (g_state.COReadsCount - 2);
		debugNoLF("CO READING: ");
		debugInt(g_state.COreading);
		g_state.COReadsCount = 0;
		g_state.COReadsSum = 0;
		g_state.COReadsMin = 1024;
		g_state.COReadsMax = 0;
		debug("CHANGED OCR2A TO 0, ENTERING 60 SECOND 5.0V READING/HEATING PHASE.");
		OCR2A = 0;														// voltage = 5.0
	} else if ((g_state.timer1ElapsedSecs >= 60) && (OCR2A == 0)) {
		g_state.timer1ElapsedSecs = 0;
		debug("CHANGED OCR2A TO 183, ENTERING 90 SECOND 1.4V PHASE.");
		OCR2A = 183;													// voltage = 1.4
	}
	if(	(g_state.timer1ElapsedSecs > 60) &&								// read for the last 30 seconds of the reading phase, 30 times
	(g_state.timer1ElapsedSecs < 90) &&
	(OCR2A == 183) &&
	(g_state.timer1LastSecSampled != g_state.timer1ElapsedSecs)) {
		g_state.timer1LastSecSampled = g_state.timer1ElapsedSecs;
		g_state.COSampleReading = ReadADC(CO_READ_PIN);						// read the CO sensor
		int tries = 1;
		while (1023 == g_state.COSampleReading && tries <= 10) {
			tries++;
			g_state.COSampleReading = ReadADC(CO_READ_PIN);					// read the CO sensor
		}
		debugNoLF("CO Reading: "); debugInt(g_state.COSampleReading);
		if(g_state.COSampleReading != 1023) {
			g_state.COReadsSum += g_state.COSampleReading;
			g_state.COReadsCount += 1;
			if(g_state.COSampleReading > g_state.COReadsMax) {
				g_state.COReadsMax = g_state.COSampleReading;
			}
			if(g_state.COSampleReading < g_state.COReadsMin) {
				g_state.COReadsMin = g_state.COSampleReading;
			}
			g_state.timer1LastSecSampled = g_state.timer1ElapsedSecs;
		}
	}
}


ISR (PCINT0_vect)	{													// Interrupts for buttons and switches 
    debug("ISR(PCINT0_VECT)");
    byte changedbits;

    char myStringTwo[4];
    itoa(PIN_SWITCH,myStringTwo,2);
    debugNoLF("PIN_SWITCH: ");debug(myStringTwo);

    char myStringThree[4];
    itoa(portbhistory,myStringThree,2);
    debugNoLF("portbhistory: ");debug(myStringThree);

    changedbits = PIN_SWITCH ^ portbhistory;
	ClearBitNo(changedbits,CO_POWER_PIN);								// CO_POWER_PIN is not a switch, ignore it
	ClearBitNo(changedbits,PORTB4);										// not a switch, ignore it

    char myString[4];
    itoa(changedbits,myString,2);
    debugNoLF("ChangedBits: ");debug(myString);

    portbhistory = PIN_SWITCH;
    ClearBitNo(portbhistory,CO_POWER_PIN);								// CO_POWER_PIN is not a switch, ignore it
    ClearBitNo(portbhistory,PORTB4);									// not a switch, ignore it

    char myStringFour[4];
    itoa(portbhistory,myStringFour,2);
    debugNoLF("portbhistory: ");debug(myStringFour);

    if(changedbits & (1 << BUTTON_STATE_PIN))							// PCINT0 changed
    {
	    if( (portbhistory & (1 << BUTTON_STATE_PIN)) == 1 )
	    {
		    _delay_us(DEBOUNCE_TIME);
		    if (bit_is_set(PIN_SWITCH, BUTTON_STATE_PIN)) {					// LOW to HIGH pin change (state button released)
			    FlashLED();
			    //debug("PCINT0_vect, PIN_SWITCH0 set, low to high");
			    ProcessEvent(EVENT_PRESS_STATE);
		    }
	    }
	    else
	    {
		    _delay_us(DEBOUNCE_TIME);
		    if (bit_is_clear(PIN_SWITCH, BUTTON_STATE_PIN)) {					// still pressed
			    // HIGH to LOW pin change (state button pressed)
			    FlashLED();
			    //debug("PCINT0_vect, PIN_SWITCH0 set, high to low");
		    }
	    }
    }

    if(changedbits & (1 << BUTTON_ACTION_PIN))
    {
        if( (portbhistory & (1 << BUTTON_ACTION_PIN)) == 2 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PIN_SWITCH, BUTTON_ACTION_PIN)) {					// LOW to HIGH pin change (action button released)
                FlashLED();
                //debug("PCINT0_vect, PIN_SWITCH1 set, low to high");
                ProcessEvent(EVENT_PRESS_ACTION);
            }
        }
        else
        {																// HIGH to LOW pin change (action button pressed) 
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PIN_SWITCH, BUTTON_ACTION_PIN)) {				// still pressed 
                FlashLED();
                //debug("PCINT0_vect, PIN_SWITCH1 set, high to low");
            }
        }
    }

    if(changedbits & (1 << ALARM_SWITCH_PIN))									// PCINT0 changed 
    {
        if( (portbhistory & (1 << ALARM_SWITCH_PIN)) == 4 )
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {							// LOW to HIGH pin change (alarm switch LEFT) 
                FlashLED();
                debug("PCINT0_VECT, PIN_SWITCH2 SET, LOW TO HIGH");
                ProcessEvent(EVENT_SWITCH_ON);
            }
        }
        else
        {
            _delay_us(DEBOUNCE_TIME);
            if (bit_is_clear(PIN_SWITCH, ALARM_SWITCH_PIN)) {						// still pressed 
																		// HIGH to LOW pin change (alarm switch RIGHT) 
                FlashLED();
                debug("PCINT0_VECT, PIN_SWITCH2 SET, HIGH TO LOW");
                ProcessEvent(EVENT_SWITCH_OFF);
            }
        }
    }

    if(changedbits & (1 << CO_SWITCH_PIN))									// PCINT0 changed
    {
	    if( (portbhistory & (1 << CO_SWITCH_PIN)) == 32 )
	    {
		    _delay_us(DEBOUNCE_TIME);
		    if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {							// LOW to HIGH pin change (alarm switch LEFT)
			    FlashLED();
			    debug("PCINT0_VECT, PIN_SWITCH5 SET, LOW TO HIGH");
			    ProcessEvent(EVENT_CO_ON);
		    }
	    }
	    else
	    {
		    _delay_us(DEBOUNCE_TIME);
		    if (bit_is_clear(PIN_SWITCH, CO_SWITCH_PIN)) {						// still pressed
			    // HIGH to LOW pin change (alarm switch RIGHT)
			    FlashLED();
			    debug("PCINT0_VECT, PIN_SWITCH5 SET, HIGH TO LOW");
			    ProcessEvent(EVENT_CO_OFF);
		    }
	    }
    }
}

/************************************************************************/
/* Analog to digital conversion                                         */
/************************************************************************/
void InitADC0() {
	ADMUX |= (1 << REFS0);												// reference voltage on AVCC
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0);								// ADC clock prescaler  / 8
	ADCSRA |= (1 << ADEN);												// enable ADC
}

uint16_t ReadADC(int channel) {
	//debug("ReadADC()");
	ADMUX &= 0xF0;														// Clear the older channel that was read
	ADMUX |= channel;													// Defines the new ADC channel to be read
																		// first sample after you've changed the
																		// multiplexer will still be from the old
																		// analog source, you need to wait at
																		// least one complete ADC cycle before
																		// getting a value from the new channel
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	uint8_t theLowADC = ADCL;
	uint16_t theTenBitResults = ADCH<<8 | theLowADC;
	return(theTenBitResults);
}

/************************************************************************/
/* Time                                                                 */
/************************************************************************/
void InitRTC() {														//  Bit 7 of Register 0 is the clock halt (CH) bit. When this bit is set to 1, the
																		//	oscillator is disabled. When cleared to 0, the oscillator is enabled. On
																		//	first application of power to the device the time and date registers are
																		//	typically reset to 01/01/00 01 00:00:00 (MM/DD/YY DOW HH:MM:SS). The CH bit
																		//	in the seconds register will be set to a 1.
//	I2C_WriteRegister(DS1307,SECONDS_REGISTER,0x00);					// clear the clock halt bit
	ClearClockHaltBit(g_state.seconds);									// clear the clock halt bit
	g_state.rtcRead = 0;

	int badRead = 1;
	int i = 0;
	while ((badRead) && (i++ <= 3)) {
		GetTime(    &g_state.hours,
					&g_state.minutes,
					&g_state.seconds	);
		badRead = 0;
		if (g_state.hours > 23 || g_state.hours < 0) {
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		if (g_state.minutes > 59 || g_state.minutes < 0) {
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		if (g_state.seconds > 59 || g_state.seconds < 0) {
			g_state.hours = 0;
			g_state.minutes = 0;
			g_state.seconds = 0;
			badRead = 1;
		}
		
		if ((g_state.seconds == 0) &&
		(g_state.minutes == 0) &&
		(g_state.seconds == 0)) {
			badRead = 1;
			msDelay(1000);
		}
		debugMetric("RTC READ ATTEMPT", i);
	}
	ClearClockHaltBit(g_state.seconds);									// clear the clock halt bit
}

void GetTime(volatile byte *inHours, volatile byte *inMinutes, volatile byte *inSeconds) {
	//debug("GetTime()");
	if(!g_state.rtcRead) {												// if we have just powered on get the time from the RTC
		byte minutesRTC;
		byte hoursRTC;
		byte secondsRTC;
		DS1307_GetTime(&hoursRTC,&minutesRTC,&secondsRTC);
		*inHours =   (hoursRTC >> 4) * 10 + (hoursRTC & 0x0F);
		*inMinutes = (minutesRTC >> 4) * 10 + (minutesRTC & 0x0F);
		*inSeconds = (secondsRTC >> 4) * 10 + (secondsRTC & 0x0F);
		g_state.rtcRead = 1;											// all subsequent setting of time is accomplished
																		// via the Timer1 RTC.
		debugTime();
		} else {
		*inHours = g_state.hours;										// hours, mins and secs updated by Timer1
		*inMinutes = g_state.minutes;
		*inSeconds = g_state.seconds;
	}
}

// There's a problem with this function; you can't save a time that's in military format and is > 12:59.
void WriteTime(byte hours, byte minutes, byte seconds) {
	debug("WRITETIME()");
	byte hour = hours - (hours / 10 * 10);								// get the hour portion of the hours:
	byte tenHours = hours / 10;											// get tens hour portion
	byte writeHours = tenHours << 4 | hour;								// put tenHours in bits 5 & 4, put hour in bit 3 - 0, put 24 hr mode flag (0 = ON) in bit 6
	I2C_WriteRegister(DS1307,HOURS_REGISTER, writeHours);
	byte minute = minutes - (minutes / 10 * 10);						// get the minute portion of minutes:
	byte tenMinutes = minutes / 10;										// get tens minutes portion
	byte writeMinutes = tenMinutes << 4 | minute;						// put tenMinutes in bit 6, 5 & 4, put minute in bit 3 - 0
	I2C_WriteRegister(DS1307,MINUTES_REGISTER, writeMinutes);
	I2C_WriteRegister(DS1307,SECONDS_REGISTER,seconds);					// To clear the clock halt bit
	ClearClockHaltBit(0x00);
}

void ClearClockHaltBit(byte seconds) {
	byte clockHaltBit = 0b10000000;
	I2C_WriteRegister(DS1307,SECONDS_REGISTER, seconds & ~(1 << clockHaltBit));
}

void ShowTime(byte hours, byte minutes) {
	debug("ShowTime()");
	Cust_SS_WriteTime(hours, minutes);
	ColonOn();
}

/************************************************************************/
/* Alarms                                                                */
/************************************************************************/
void InitAlarms() {																// Determine if the alarm switch is on or off and process the appropriate event
	portbhistory = 0b00000000;
	SetBitNo(portbhistory,BUTTON_STATE_PIN);
	SetBitNo(portbhistory,BUTTON_ACTION_PIN);
    if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {								// if switch is ON, LEFT
		debug("ALARM SWITCH IS ON");
        ProcessEvent(EVENT_SWITCH_ON);
		SetBitNo(portbhistory, ALARM_SWITCH_PIN);
    }
    if (bit_is_clear(PIN_SWITCH, ALARM_SWITCH_PIN)) {							// if switch is OFF, RIGHT           
		debug("ALARM SWITCH IS OFF");
        ProcessEvent(EVENT_SWITCH_OFF);
		ClearBitNo(portbhistory,ALARM_SWITCH_PIN);
    }
	if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {								// if switch is ON, RIGHT
		debug("CO IS ON");
		ProcessEvent(EVENT_CO_ON);
		SetBitNo(portbhistory, CO_SWITCH_PIN);
	}
	if (bit_is_clear(PIN_SWITCH, CO_SWITCH_PIN)) {								// if switch is OFF, LEFT
		debug("CO IS OFF");
		ProcessEvent(EVENT_CO_OFF);
		ClearBitNo(portbhistory, CO_SWITCH_PIN);
	}
    char myString[4];
    itoa(portbhistory,myString,2);
    debugNoLF("initial portbhistory: ");debug(myString);

}

void WriteAlarmTime(byte hours, byte minutes) {
	debug("WRITEALARMTIME()");
	uint16_t alarmTimeWrite;
	alarmTimeWrite = (hours << 8) | minutes;
	debugMetric("ALARMTIMEWRITE",(int) alarmTimeWrite);
	uint8_t *address_minutes = (uint8_t *) ALARM_ADDRESS;
	eeprom_update_byte(address_minutes, minutes);
	uint8_t *address_hours = (uint8_t *) ALARM_ADDRESS + 1;
	eeprom_update_byte(address_hours, hours);

	debugAlarmTime();
}

void ReadAlarmTime(volatile byte *hours, volatile byte *minutes) {
    debug("READALARMTIME()");
	uint8_t *address_minutes = (uint8_t *) ALARM_ADDRESS;
	*minutes = eeprom_read_byte(address_minutes);
	uint8_t *address_hours = (uint8_t *) ALARM_ADDRESS + 1;
	*hours = eeprom_read_byte(address_hours);  

    if ((*hours < 0) || (*hours > 23)) {								// Wake hours and minutes are corrupt the first time they are read from memory, clean that up
        *hours = 0;
    }
    if ((*minutes < 0) || (*minutes > 59)) {
        *minutes = 0;
    }

    debug("READ ALARM TIME");
    debugAlarmTime();
}   

void SoundAlarm() {														//todo: make this stop after a minute (configurable time)
	debug("SoundAlarm()");
	SetBitNo(PORTC,BUZZER_PIN);
}

void SoundWake() {														
    debug("SoundWake()");
    ClearBitNo(PORTD,WAKE_PIN);
}

void SilenceAlarm() {
    debug("SILENCEALARM()");
    ClearBitNo(PORTC,BUZZER_PIN);
    SetBitNo(PORTD,WAKE_PIN);
}

void SnoozeAlarm() {
    debug("SnoozeAlarm()");
    SilenceAlarm();
    g_state.alarmMinutes += SNOOZE_TIME;
    if (g_state.alarmMinutes > 59) {
        g_state.alarmMinutes = g_state.alarmMinutes - 60;
        g_state.alarmHours += 1;
    }
}

int WasWakeAlarmTriggered() {											// TODO: pass in the structure by value
	//debug("WasWakeAlarmTriggered()");
	if (bit_is_set(PIN_SWITCH, ALARM_SWITCH_PIN)) {							// alarm switch LEFT, ON, HIGH
		if (!((g_state.mode & BUZZING) == BUZZING)) {					// if the alarm isn't already going off
			if ((g_state.hours == g_state.alarmHours) &&
			(g_state.minutes == g_state.alarmMinutes) &&
			(g_state.seconds >= 0 && g_state.seconds <= 2)) {			// so that we only trigger the alarm once
				debug("WAKE ALARM TRIGGERED");
				return 1;
			}
		}
	}
	return 0;
}

/************************************************************************/
/* Temperature                                                          */
/************************************************************************/
double GetInternalTemp(void)
{
	ADMUX = (3 << REFS0) | (8 << MUX0);									// 1.1V REF, channel#8 is temperature
	ADCSRA |= (1 << ADEN) | (6 << ADPS0);								// enable the ADC div64
	msDelay(20);														// wait for voltages to become stable.
	ADCSRA |= (1 << ADSC);												// Start the ADC

	while (ADCSRA & (1 << ADSC));										// Detect end-of-conversion
	//return (ADCW - 324.31) / 1.22;
	return (ADCW * (1.1/1023.0) * (25.0/0.314) * 1.8 + 32.0);
	
}

void ShowTemp(int tempF) {
    debug("SHOWTEMP()");
    
    if (tempF > 120 || tempF < 0) {
        Cust_SS_WriteChars(CHAR_E,CHAR_R,CHAR_R,CHAR_SPACE);
    } else {
        byte ones = tempF - (tempF / 10) * 10;
        byte tens = tempF / 10;
        byte hundreds = tempF / 100;
        BlankDisplay();
        if (!(hundreds > 0)) {
            hundreds = CHAR_SPACE;
        }
        if (!(tens > 0)) {
            tens = CHAR_SPACE;
        }
        Cust_SS_WriteChars(hundreds,tens,ones,CHAR_F);
        ColonOff();
    }
	debugMetric("TEMPERATURE (F)",tempF);
}

signed int ConvertToFarenheit(unsigned short celcius) {
    if (celcius > 127) {												// Check for negative temperature 
        celcius = ~celcius + 1; 
    } 
    signed int farenheit = 32.0 + (celcius * 9.0/5.0);
    debugMetric("TEMPERATURE IN FARENHEIT", farenheit);
    return farenheit;
}

/************************************************************************/
/* Carbon Monoxide                                                      */
/************************************************************************/
int WasCOGasAlarmTriggered() {
   //debug("WasCOGasAlarmTriggered()");
																		// if the CO sensor is powered up
   if (bit_is_set(PIN_SWITCH, CO_SWITCH_PIN)) {									// alarm switch LEFT, ON, HIGH      
        //debug("co_reading");
        //debugInt(co_reading);
        if ((g_state.COreading > CO_ALARM_THRESHOLD) && (sensorStable >= CONSECUTIVE_CO_READING_THRESHOLD)) {
            return(1);
        }
   }
   return(0);     
}     

void DisplayWarnForCO() {

	Cust_SS_WriteChars(CHAR_SPACE, CHAR_C, CHAR_O, CHAR_SPACE);
	ColonOff();
}

/************************************************************************/
/* Main Program                                                         */
/************************************************************************/
int main(void) {
    Initialize();
    debug("MAIN()");
    byte eventToProcess = g_state.event;
    while(1) {															// Event loop, forever
        ProcessEvent(eventToProcess);
        if (g_state.event == eventToProcess) {							// did a new event happen 
																		// as we processed this event?
            g_state.event = EVENT_NONE;									// clear out the event just processed
            //FlashLED();													// heartbeat
            msDelay(1000);												// one second between updates  
        }
        g_state.event = CheckForEvents();								// check CO sensor and wake alarm
        eventToProcess = g_state.event;
    }
}

void Initialize() {
	initUSART();

	debug("==============================================================");
	debug("Copyright 2017, Daniel Murphy");
	debug("==============================================================");

	debug("INITIALIZE THE SYSTEM.");
	debug("INITIALIZED USART.  INITIALIZE AVR.");

	debug("INITIALIZING AVR.");
	InitAVR();															// set port direction

	debug("DONE INITIALIZING AVR.  SEND ONE SHORT BEEP.");
	FlashLED();
	OneShortBeep();

	debug("DONE WITH SHORT BEEP.  INITIALIZING I2C.");
	FlashLED();
	I2C_Init();															// set I2C clock frequency

	debug("WAIT FOR I2C DEVICES TO COME ONLINE.");
	msDelay(2000);
	
	debug("SHOW ALL I2C DEVICES.");
	FlashLED();
	FindI2CDevices();

	debug("DONE INITIALIZING I2C.  INITIALIZE RTC.");
	FlashLED();
	InitRTC();

	debug("DONE INITIALIZING RTC. INITIALIZE ADC.");
	FlashLED();
	InitADC0();
	
	debug("DONE INITIALIZING ADC. INITIALIZE TIMERS.");
	FlashLED();
	initTimer0();														// initialize Timer0
	FlashLED();
	initTimer1();														// initialize Timer1
	FlashLED();
	initTimer2();														// initialize Timer2

	debug("DONE INITIALIZING TIMERS. INITIALIZE INTERRUPTS.");
	FlashLED();
	initInturrupts();
	
	debug("DONE INITIALIZING THE INTERRUPTS.  INITIALIZE THE WAKE ALARM.");
	FlashLED();
	g_state.alarmHours = 0;
	g_state.alarmMinutes = 0;											//(no seconds for alarm)
	
	ReadAlarmTime(  &g_state.alarmHours,
					&g_state.alarmMinutes	);							// Read the wake time from chip memory
	InitAlarms();														// Initialize the alarm switch.

	debug("DONE INITIALIZING THE WAKE ALARM. INITIALIZE THE LCD.");
	FlashLED();	
    lcd_init(LCD_DISP_ON_BLINK);										// caused the SS to not work when we were using the default TWBR of 10000
	LCDReset();
	
	debug("ENABLE GLOBAL INTERRUPTS.\r\n");
	FlashLED();
	sei();																// enable interrupts

	debug("FINISHED INITIALIZATION. SEND ONE SHORT BEEP.\r\n");
	FlashLED();
	OneShortBeep();

	g_state.mode |= SHOWING_TIME;										// set up to show the time immediately
	g_state.event = EVENT_MINUTE_CHANGED;
	sensorStable = 0;													// the CO sensor isn't stable yet.
}
																		// check for events that don't have interrupts, this is in
byte CheckForEvents() {													// the mainline of processing
																		// Check if the CO gas event was triggered.
	if (WasCOGasAlarmTriggered()) {										// CO alarm has priority over wake alarm
		return(EVENT_CO_DETECTED);
	} else if (WasWakeAlarmTriggered()) {								// This needs to be called about once a second so that we don't skip
																		// over the second(s) that trigger the wake alarm.
		return(EVENT_WAKE_TRIGGERED);
	}
	//} else if (WasSmokeAlarmTriggered()) {							// Check if the smoke event was triggered.
	//    g_state.event = EVENT_SMOKE_DETECTED;
	//} else if (WasMotionAlarmTriggered()) {							// Check if the motion event was triggered.
	//    g_state.event = EVENT_MOTION_DETECTED;
	//}
	return(g_state.event);
}

void ProcessEvent(byte event)
{
    //debug("ProcessEvent()");
    //debugNoLF("event:"); debugInt(event);
    //debugNoLF("g_state.mode:"); debugInt(g_state.mode);
    if (event == EVENT_NONE) {
        if (g_state.mode & SHOWING_TIME) {
            ToggleColon();
        }
        return;
	} else if (event == EVENT_SECOND_CHANGED) {
			Cust_LCD_WriteTime(g_state.hours, g_state.minutes, g_state.seconds);
    } else if (event == EVENT_MINUTE_CHANGED) {							// set by Timer1 RTC
        if (g_state.mode & SHOWING_TIME) {
            ShowTime(g_state.hours, g_state.minutes);
        }
    } else if (event == EVENT_PRESS_STATE) {							// if the STATE (left) button was pressed 
        if ( (g_state.mode & SHOWING_TIME) &&							// If the clock is showing time and is NOT sounding the alarm, put it into set wake hour mode 
            !(g_state.mode & BUZZING) ) {
            ClearBit(g_state.mode, SHOWING_TIME);						// Turn off showing time
            SetBit(g_state.mode, SETTING_WAKE_HOUR);					// Turn on setting wake hour
            Cust_SS_WriteTimeAlarmSetHours(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_HOUR) {					// If the clock is setting the wake hour, put it into setting wake minute mode 
            ClearBit(g_state.mode, SETTING_WAKE_HOUR);
            SetBit(g_state.mode, SETTING_WAKE_MINUTE);
            Cust_SS_WriteTimeAlarmSetMinutes(g_state.alarmMinutes);
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {				// If clock is setting wake minute set the mode to setting time hour 
            ClearBit(g_state.mode, SETTING_WAKE_MINUTE);				// Turn off setting wake minute
            SetBit(g_state.mode, SETTING_TIME_HOUR);					// Turn on setting time hour
            WriteAlarmTime(g_state.alarmHours, g_state.alarmMinutes);	// save wake time
            g_state.setTimeHours = g_state.hours;
            Cust_SS_WriteTimeSetHours(g_state.setTimeHours);
        } else if (g_state.mode & SETTING_TIME_HOUR) {					// If clock is setting time hour set the mode to setting time minute 
            ClearBit(g_state.mode, SETTING_TIME_HOUR);					// Turn off setting time hour
            SetBit(g_state.mode, SETTING_TIME_MINUTE);					// Turn on setting time minute
            g_state.setTimeMinutes = g_state.minutes;
            Cust_SS_WriteTimeSetMinutes(g_state.setTimeMinutes);
        } else if (g_state.mode & SETTING_TIME_MINUTE) {				// If the clock is setting time minute set the mode to showing time 
            ClearBit(g_state.mode, SETTING_TIME_MINUTE);				// stop showing minute
            SetBit(g_state.mode, SHOWING_TIME);							// start showing the time
            WriteTime(g_state.setTimeHours, g_state.setTimeMinutes, g_state.seconds);	// save the new time to the RTC
            g_state.hours = g_state.setTimeHours;
            g_state.minutes = g_state.setTimeMinutes;
            g_state.setTimeHours = 0;
            g_state.setTimeMinutes = 0;
            ShowTime(g_state.hours, g_state.minutes);
        } else if (g_state.mode & SHOWING_TEMP) {						// if showing the temperature set the mode to show the time 
            ClearBit(g_state.mode, SHOWING_TEMP);						// turn off showing temp
            SetBit(g_state.mode, SHOWING_TIME);							// go back to showing time
            ShowTime(g_state.hours, g_state.minutes);
        } else if (g_state.mode & SHOWING_CO){							// if showing CO alert set mode to show time and silence sounding alarm 
            ClearBit(g_state.mode, SHOWING_CO);
            SetBit(g_state.mode, SHOWING_TIME);
            g_state.COreading = 0;										// so that the alarm doesn't keep sounding 
																		// after the button is pressed
            //SilenceAlarm();											// silence
            ShowTime(g_state.hours, g_state.minutes);					// shows time as of CO alarm trigger
		}
																		// No matter which mode the clock is in, if the state button is pressed         
        if (g_state.mode & BUZZING) {									// silence the alarm if it's sounding 
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode.
            SilenceAlarm();
        }
    } else if (event == EVENT_PRESS_ACTION) {							// if the ACTION (right) button was pressed 
        if ((g_state.mode & SHOWING_TIME) &&							// if time is showing and it's not buzzing, show the temperature 
            !(g_state.mode & BUZZING)) {
            debug("SHOW THE TEMP");
            ClearBit(g_state.mode, SHOWING_TIME);						// no longer showing time
            SetBit(g_state.mode, SHOWING_TEMP);							// show the temperature
            signed int farenheit = ConvertToFarenheit(I2C_ReadRegister(TC74_ADDRESS_R, TC74_TEMP_REGISTER));
            debugMetric("FARENHEIT IN PROCESSEVENT",farenheit);
            ShowTemp(farenheit);
			int internalTemp = (int) GetInternalTemp();
			debugMetric("INTERNAL TEMPERATURE", internalTemp);
        }
        if (g_state.mode & SETTING_WAKE_HOUR) {							// if setting wake hour, increment the hour
            g_state.alarmHours += 1;
            // debugInt(g_state.alarmHours);
            if (g_state.alarmHours > 23) {
                g_state.alarmHours = 0;
            }
            Cust_SS_WriteTimeAlarmSetHours(g_state.alarmHours);
        } else if (g_state.mode & SETTING_WAKE_MINUTE) {				// else if setting wake minute, increment minute 
            g_state.alarmMinutes += 1;
            if (g_state.alarmMinutes > 59) {
                g_state.alarmMinutes = 0;
            }
            Cust_SS_WriteTimeAlarmSetMinutes(g_state.alarmMinutes);
        } else if (g_state.mode & SETTING_TIME_HOUR) {					// if setting time hour, increment hour
            g_state.setTimeHours += 1;            
            if (g_state.setTimeHours > 23) {
                g_state.setTimeHours = 0;
            }
            Cust_SS_WriteTimeSetHours(g_state.setTimeHours);
        } else if (g_state.mode & SETTING_TIME_MINUTE) {				// if setting time minute, increment minute
            g_state.setTimeMinutes += 1;
            if (g_state.setTimeMinutes > 59) {
                g_state.setTimeMinutes = 0;
            }
            Cust_SS_WriteTimeSetMinutes(g_state.setTimeMinutes);
        } else if (g_state.mode & SHOWING_CO) {
            ShowTime(g_state.hours, g_state.minutes);					// shows time as of CO alarm trigger
            msDelay(2000);												// wait for two seconds
            DisplayWarnForCO();											// back to showing CO  
        } else if (g_state.mode & BUZZING) {							// If the alarm is sounding, change the clock mode to snoozing 
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
            SetBit(g_state.mode, SNOOZING); 							// add snoozing
            SnoozeAlarm(&g_state);
        }
    } else if (event == EVENT_WAKE_TRIGGERED) {							// if it's time to sound the alarm to wake up; sound the alarm if the switch is on  
        if (g_state.mode & WAKE) {          							// if the wake alarm is enabled
            SetBit(g_state.mode, BUZZING);  							// set the mode to buzzer on
			debug("WAKE DETECTED.");
            SoundWake();                   								// play a song using UM66 
        }
    } else if (event == EVENT_CO_DETECTED) {
        if (g_state.mode & WAKE) {          							// if the wake alarm is enabled
            SetBit(g_state.mode, BUZZING);
            ClearBit(g_state.mode, SHOWING_TIME);
            SetBit(g_state.mode, SHOWING_CO);
			debug("CO DETECTED");
            SoundAlarm();
            DisplayWarnForCO();											// todo: make this flash
        }
    } else if (event == EVENT_SWITCH_OFF) {
        ClearBit(g_state.mode, WAKE);       							// set the alarm so it will not sound
		g_state.decimalBlink = 0;										// turn off the blinking decimal
        if (g_state.mode & BUZZING) {       							// if the alarm is currently sounding
            ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
            SilenceAlarm();                 							// turn of the sounding alarm (both song and siren)
        }
        ColonOff();														// turn off the colon
   } else if (event == EVENT_SWITCH_ON) {
        SetBit(g_state.mode, WAKE);         							// turn on the alarm so it will 
																		// sound when necessay
		g_state.decimalBlink = 1;										// turn on the blinking decimal
        ColonOn();														// turn on the colon
   } else if (event == EVENT_CO_ON) {
		PORTB |= (1<<CO_POWER_PIN);										// set CO_POWER_PIN as output
		initTimer2();
   } else if (event == EVENT_CO_OFF) {
		offTimer2();
		PORTB &= ~(1<<CO_POWER_PIN);
        if (g_state.mode & BUZZING) {       							// if the alarm is currently sounding
	        ClearBit(g_state.mode, BUZZING);							// remove BUZZING from the mode
	        SilenceAlarm();                 							// turn of the sounding alarm (both song and siren)
        }
   }
}
