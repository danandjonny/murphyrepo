//
//		*******************************************************************************
//	!!!!NOT SURE IF THIS CODE WORKS, THE CODE FOR THE ORIGINAL ANNOY-A-TROY IS MISSING!!!!
//		*******************************************************************************
//
// F_CPU tells util/delay.h our clock frequency
#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/sleep.h>
#include <avr/wdt.h>						// Supplied Watch Dog Timer Macros 
#include <avr/interrupt.h>					// Needed to use interrupts


int watchdog_counter;

void goToSleep(void);						// function prototype

void delayus( uint16_t z ) {

	while ( z ) {
		_delay_us( 100 );					// delay 100 microseconds of a second
		z--;
	}
}

//This runs each time the watch dog wakes us up from sleep
ISR(WDT_vect) {
	watchdog_counter++;
}
 
int main( void ) {
	int lengthOfBeep;
	int pitchOfBeep;
	
	ADCSRA &= ~(1<<ADEN);					// Disable ADC, saves ~230uA

	DDRB |= 1 << DDB3;						// set pin PB3 to output

	// beep when powered up
	PORTB |= 1 << PORTB3; 					// turn on PB3 (buzzer)
	delayus(100);							// wait 10 microseconds
	PORTB &= ~( 1 << PORTB3 );				// turn off PB3 (buzzer)

	int w = rand() % 30;					// set w to a random number of seconds up to 3 minutes
	while (1) {
		goToSleep();						// go to sleep
		if(watchdog_counter > w)
		{
			watchdog_counter = 0;
			lengthOfBeep = rand() % 400;    // length of beep a random # microsec > 0 < 401
		
		
			pitchOfBeep = rand() % 10;
			for (int i = 0; i < lengthOfBeep; i++) {
				PORTB &= ~( 1 << PORTB3 );  // turn off PB3 (buzzer)
				delayus( pitchOfBeep );	    // wait for PITCH_OF_BEEP * 100 microseconds
				PORTB |= 1 << PORTB3; 		// turn on PB3 (buzzer)
				delayus( pitchOfBeep );	    // wait for PITCH_OF_BEEP * 100 microseconds
			}
			PORTB &= ~( 1 << PORTB3 );	    // clear the bit to turn off the beep
			w = rand() % 30;				// set w to a random number of seconds up to 3 minutes
		}
	}
}

void goToSleep(void)
{
	uint8_t adcsra, mcucr1, mcucr2;

	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
	MCUCR &= ~(_BV(ISC01) | _BV(ISC00));      // INT0 on low level
	GIMSK |= _BV(INT0);                       // enable INT0
	adcsra = ADCSRA;                          // save ADCSRA
	ADCSRA &= ~_BV(ADEN);                     // disable ADC
	cli();                                    // stop interrupts to ensure the BOD timed sequence executes as required
	mcucr1 = MCUCR | _BV(BODS) | _BV(BODSE);  // turn off the brown-out detector
	mcucr2 = mcucr1 & ~_BV(BODSE);            // if the MCU does not have BOD disable capability,
	MCUCR = mcucr1;                           // this code has no effect
	MCUCR = mcucr2;
	sei();                                    // ensure interrupts enabled so we can wake up again
	sleep_cpu();                              // go to sleep
	sleep_disable();                          // wake up here
	ADCSRA = adcsra;                          // restore ADCSRA
}
