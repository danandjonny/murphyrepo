#include "uart.h"
#include <avr/io.h>
#define F_CPU 2000000UL

int uart_putchar (char c, FILE *f)
{
	loop_until_bit_is_set(UCSRA, UDRE);
	UDR = c;
	return (0);
}

void UART_Init (void)
{
	UCSRB=(1 << TXEN);
	UBRRL=(F_CPU / (BAUD_RATE * 16L) - 1);
	
	fdevopen (uart_putchar, 0);
}

