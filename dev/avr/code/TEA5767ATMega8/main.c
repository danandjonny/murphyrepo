/*
 * TEA5767ATMega8.c
 *
 * Created: 3/5/2017 9:50:48 AM
 * Author : djmurphy
 */ 

/** FM radio
	Built with WinAVR 20090313, avr-libc 1.6.6
	
	User interface: pushbuttons at PD2 and PD3 (connected to the GND), used internal pull-ups.
	Using internal 2 MHz oscillator (see makefile).
	Use external pullups (i.e. 3k9) at SDA and SCL.
*/
#include "TEA5767.h"
#include "uart.h"
#include <avr/io.h>
#include <util/delay.h>

#define LOCAL_DEBUG
#ifdef LOCAL_DEBUG
#define LOG(args) (printf("MAIN "), printf args)
#endif


void init(void)    
{
	DDRB = 0XFF;
	PORTB = 0XFF;
	
	/** \note PD2, PD3: input pushbuttons */
	DDRD = 0B11100000;
	PORTD = 0XFF;
	
	DDRC = 0B00000000;
	PORTC = 0Xff;      

	_delay_ms(100);
	

	/** \note SCL = F_CPU/(16 + 2*TWBR*Prescaler) */
    TWSR = 0;                         /* no prescaler */	
	/** \note TWBR should be 10 or higher if the TWI operates in Master mode. If TWBR is lower than
	10, the Master may produce an incorrect output on SDA and SCL for the reminder of the
	byte. The problem occurs when operating the TWI in Master mode, sending Start + SLA
	+ R/W to a Slave (a Slave does not need to be connected to the bus for the condition to
	happen). */
	TWBR = 12;
	TWCR = (1<<TWEN); 
#ifdef LOCAL_DEBUG	
	UART_Init();
#endif
}


int main(void)
{
	uint8_t searching = 0;
	struct TEA5767_status status;
	uint8_t counter = 0;
	
	init();
	
	LOG(("FM radio starting...\r\n"));
	
	TEA5767_init();
	TEA5767_tune(tune);
	TEA5767_write();	
	while(1)
	{
		uint8_t pb_state = PIND;
		if (searching == 0)
		{

#if 1
			// auto search
			if ((pb_state & (1<<2)) == 0)
			{
				TEA5767_search(0);
				searching = 1;
				_delay_ms(500);
			}
			else if ((pb_state & (1<<3)) == 0)
			{
				TEA5767_search(1);		
				searching = 2;
				_delay_ms(500);
			}
#else
			// manual search
			if ((pb_state & (1<<2)) == 0)
			{
				tune -= 50;
				if (tune >= 87500UL)
				{
					TEA5767_tune(tune);
					TEA5767_write();	
				}
				else
				{
					tune = 87500UL;
				}
			}
			else if ((pb_state & (1<<3)) == 0)
			{
				tune += 50;
				if (tune <= 108000UL)
				{
					TEA5767_tune(tune);
					TEA5767_write();	
				}
				else
				{
					tune = 108000UL;
				}
			}			

#endif
			// wait for keys release
			while ((pb_state & (1<<2)) == 0 || (pb_state & (1<<3)) == 0)
			{
				pb_state = PIND;	
			}
		}
		else
		{
			if (counter % 20 == 0)
			{
				TEA5767_get_status(&status);
				if (status.ready)
				{
					if (status.band_limit)
					{
						if (searching == 1)
						{
							// searching down, wrap
							LOG(("Down wrap\r\n"));
							TEA5767_tune(108000UL);
							TEA5767_search(0);
						}
						else
						{
							// searching up, wrap
							LOG(("Up wrap\r\n"));
							TEA5767_tune(87500UL);
							TEA5767_search(1);
						}
						_delay_ms(500);	
					}
					else
					{
						searching = 0;
						TEA5767_exit_search();	
					}
				}
			}
		}
		_delay_ms(20);	


		if (++counter == 200)
		{
			PORTB ^= 0x02;	
			counter = 0;
			TEA5767_get_status(&status);			
		}
	}
}

