#ifndef _UART_H
#define _UART_H

#include <stdio.h>

extern void UART_Init (void);
extern int uart_putchar (char c, FILE *f);

#define BAUD_RATE 9600		//Baud Rate f�r die Serielle Schnittstelle


#endif //_UART_H
