

// https://www.youtube.com/watch?v=BC5LSnm9R9k
#include <avr/io.h>
#include <avr/interrupt.h>

volatile int analogResult = 0;

ISR(ADC_vect)
{
 //analog result = 0 - 1023 (0v to 5.0v)
 analogResult = (ADCH<<8)|ADCL;
}

int main(void)
{
 DDRB &= ~(1<<DDB4); // make pin an input
 DDRB |= (1<<DDB3);  // make LED pin an output
 
 // Table 17-3. Voltage Reference Selections for ADC
 // use Vcc as Vref (REFSx)
 // Right adjust (ADLAR)
 ADMUX &= ~( (1<<REFS1)|(1<<REFS0)|(1<<ADLAR) ); //Note: the bits are zero initially

 // Table 17-4. Input Channel Selections
 // Single ended input on ADC2 (MUXx)
 ADMUX |= (1<<MUX1); // ADC2 (PB4)
 
 // Table 17-6. ADC Auto Trigger Source Selections
 ADCSRB &=~( (1<<ADTS2)|(1<<ADTS1)|(1<<ADTS0) ); // ADC Auto Trigger Source = Free running mode
 
 // Digital input disable.  Reduces power consumptions
 // DIDR0 |= (1<<ADC2D);
 
 // ADEN - enable the ADC
 // ADIE - ADC interrupt enable
 // ADATE - When this bit is written to one, Auto Triggering of the ADC is enabled.
 ADCSRA |= (1<<ADEN)|(1<<ADIE)|(1<<ADATE);

 sei(); // enable interrupts

 // Start conversion
 ADCSRA |= (1<<ADSC);  // In Free Running mode, write this bit to one to start the first conversion     
 while(1)
 {           
 /* while ( ( ADCSRA & (1<<ADSC))); // conversion is going on so wait.
   analogResult = (ADCH<<8)|ADCL; */
   
  if(analogResult<500) // 500 ~= 2.5v
  {
   PORTB |= (1<<PB3);
  }
  else
  {
   PORTB &= ~(1<<PB3);
  }
 }
}
  
 

ATTiny85_ADC_Example.c

// https://www.youtube.com/watch?v=BC5LSnm9R9k
#include <avr/io.h>
#include <avr/interrupt.h>

volatile int analogResult = 0;

ISR(ADC_vect)
{
	//analog result = 0 - 1023 (0v to 5.0v)
	analogResult = (ADCH<<8)|ADCL;
}

int main(void)
{
	DDRB &= ~(1<<DDB4); // make pin an input
	DDRB |= (1<<DDB3);  // make LED pin an output
	
	// Table 17-3. Voltage Reference Selections for ADC
	// use Vcc as Vref (REFSx)
	// Right adjust (ADLAR)
	ADMUX &= ~( (1<<REFS1)|(1<<REFS0)|(1<<ADLAR) ); //Note: the bits are zero initially

	// Table 17-4. Input Channel Selections
	// Single ended input on ADC2 (MUXx)
	ADMUX |= (1<<MUX1); // ADC2 (PB4)
	
	// Table 17-6. ADC Auto Trigger Source Selections
	ADCSRB &=~( (1<<ADTS2)|(1<<ADTS1)|(1<<ADTS0) ); // ADC Auto Trigger Source = Free running mode
	
	// Digital input disable.  Reduces power consumptions
	// DIDR0 |= (1<<ADC2D);
	
	// ADEN - enable the ADC
	// ADIE - ADC interrupt enable
	// ADATE - When this bit is written to one, Auto Triggering of the ADC is enabled. 
	ADCSRA |= (1<<ADEN)|(1<<ADIE)|(1<<ADATE);

	sei(); // enable interrupts

	// Start conversion
	ADCSRA |= (1<<ADSC);  // In Free Running mode, write this bit to one to start the first conversion      
	while(1)
	{            
	/* while ( ( ADCSRA & (1<<ADSC))); // conversion is going on so wait.
			analogResult = (ADCH<<8)|ADCL; */
			
		if(analogResult<500) // 500 ~= 2.5v
		{
			PORTB |= (1<<PB3);
		}
		else
		{
			PORTB &= ~(1<<PB3);
		}
	}
}
		
	

