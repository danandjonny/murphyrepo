/********************************************************************
  Name    : SensorDetector.c
  Author  : Daniel Murphy
  Date    : March 29, 2016
  Version : 3
  Notes   : The purpose of this program is to detect methane and sound
		  : an alarm (an active piezo and a LED) if the sensor 
		  : exceeds the threshold amount set with the potentiometer.
          : The project displays numerals and characters on a series 
		  : of 3 single character seven segment displays set up in a 
		  : circuit with S8550 PNP transistors for multiplexed
		  : switching. A shift register, 74HC595, is used to shuttle a  
          : value to all of the SS displays, and the transistor for
          : the character position for which the value is intended is
          : switched on.  When the next number comes through the shift
          : register, it's transistor is turned on, and so on.  Values
          : are mapped so as to light the correct segments on the 
		  : display. See sensorDetector.h for the mapping.  The setup 
		  : includes 8 330ohm resistors, 3 single character seven 
		  : segment displays, 3 S8550 transistors, a 74HC595 shift 
		  : register, 4 resistors (brown, brown, black, red, red) for 
		  : the transistor switching, a sensor, and a potentiometer 
		  : for setting the alarm threshold. For this project we're 
		  : using active-lo PNP (emitter follower) transistors as 
		  : "sinking" drivers connected to ground. 
********************************************************************

7 Segment (Common Anode) Display Map: 

    D   E  5V   F   G
 ___|___|___|___|___|____
|                        |
|        F               |
|    E       G           |
|        D               |
|    A       C           |
|        B       H(Dot)  |
|________________________|
    |   |   |   |   |
    A   B  5V   C   H

74HC595 Map:
     _______
Q1  |1 *  16|  Vcc                  PINS 1-7, 15   Q0 - Q7   Output Pins
Q2  |2    15|  Q0                   PIN 8	   GND	     Ground, Vss
Q3  |3    14|  DS                   PIN 9	   Q7"	     Serial Out
Q4  |4    13|  OE                   PIN 10	   MR	     Master Reclear, active low
Q5  |5    12|  ST_CP                PIN 11	   SH_CP     Shift register clock pin
Q6  |6    11|  SH_CP                PIN 12	   ST_CP     Storage register clock pin (latch pin)
Q7  |7    10|  MR                   PIN 13	   OE	     Output enable, active low
GND |8_____9|  Q7"                  PIN 14	   DS	     Serial data input
                                    PIN 16	   Vcc	     Positive supply voltage
           _______
    LED Q1-|1 *  16|-5V
    LED Q2-|2    15|-LED Q0
    LED Q3-|3    14|-PB0
    LED Q4-|4    13|-GND
    LED Q5-|5    12|-PB2
    LED Q6-|6    11|-PB1 ; 1uF TO GND (capacitor omitted)
DECIMAL Q7-|7    10|-5V
       GND-|8_____9|-NILL

The values passed in from the master map to characters as follows:

Array Value      Character
-----------      ---------
0                0
1                1
2                2
...
9                9
10               A
11               B
...
35               Z
(decimal point)  36
(space)          37

This mapping is controlled on the master in sensorDetector.h.

 Pins
 ---------------------------------------
 PB0 - Data pin (DS) pin location for 74HC595 pin 14
 PB1 - Shift Clock (SH_CP) pin location for 74HC595 pin 11
 PB2 - Store Clock (ST_CP) pin location for 74HC595 pin 12
 PD2 - transistor for hundreds place
 PD3 - transistor for tens place
 PD4 - transistor for ones place
 PC1 - switch pin
 PC0 - input for ADC sensor input
 PC2 - input for ADC threshold input
 PB6 - output to sound alarm and light LED

 Reference: https://www.youtube.com/watch?v=BC5LSnm9R9k
 
 TODO: Set the clock speed to 8mHz
 
 Code:																			*/
#include "SensorDetector.h"

void delayus( uint16_t z ) {

	while ( z ) {
		_delay_us( 100 );												// delay 100 microseconds of a second
		z--;
	}
}

//--------------Debug Routines-----------------------------------------
void debug(const char myString[]) {
	if (DEBUG) {
		uint8_t i = 0;
		while (myString[i]) {
			transmitByte(myString[i]);
			i++;
		}
		printString("\r\n");
	}
}

void debugNoLF(const char myString[]) {
	if (DEBUG) {
		uint8_t i = 0;
		while (myString[i]) {
			transmitByte(myString[i]);
			i++;
		}
	}
}

void debugNoLN(const char myString[]) {
	if (DEBUG) {
		uint8_t i = 0;
		while (myString[i]) {
			transmitByte(myString[i]);
			i++;
		}
	}
}

void debugInt(signed int anInt) {
	if (DEBUG) {
		char myInt[4];
		itoa(anInt,myInt,10);
		debug(myInt);
	}
}

//----------------------74HC595 Interface Code--------------------------
#define HC595_PORT		PORTB
#define HC595_DDR		DDRB
#define HC595_DS_POS	PB0												// Data pin (DS) pin location
#define HC595_SH_CP_POS	PB1												// Shift Clock (SH_CP) pin location
#define HC595_ST_CP_POS	PB2												// Store Clock (ST_CP) pin location
																		// END Configure Connections
void initHC595() {														// Initialize HC595 System
	HC595_DDR|=((1<<HC595_SH_CP_POS)|(1<<HC595_ST_CP_POS)|(1<<HC595_DS_POS));
																		// Make the Data(DS), Shift clock (SH_CP), Store Clock (ST_CP) lines output
}

#define HC595DataHigh() (HC595_PORT|=(1<<HC595_DS_POS))					// Low level macro to change data (DS)lines
#define HC595DataLow() (HC595_PORT&=(~(1<<HC595_DS_POS)))				// Low level macro to change data (DS)lines

void HC595Pulse() {														// Sends a clock pulse on SH_CP line
	HC595_PORT|=(1<<HC595_SH_CP_POS);									// Pulse the Shift Clock HIGH
	HC595_PORT&=(~(1<<HC595_SH_CP_POS));								// Pulse the Shift Clock LOW
}

void HC595Latch() {														// Sends a clock pulse on ST_CP line
	HC595_PORT|=(1<<HC595_ST_CP_POS);									// Pulse the Store Clock HIGH
	_delay_loop_1(1);
	HC595_PORT&=(~(1<<HC595_ST_CP_POS));								// Pulse the Store Clock LOW
	_delay_loop_1(1);
}

void HC595Write(uint8_t data) {											// write a single byte to output shift register 74HC595. 
																		// The byte is serially transfered to 74HC595 and then latched. The byte is then available on
																		// output line Q0 to Q7 of the HC595 IC.
	for(uint8_t i=0;i<8;i++) {											// Send each 8 bits serially, order is MSB first
		if(data & 0b10000000) {											// Output the data on DS line according to the value of MSB	
			HC595DataHigh();											// MSB is 1 so output high
		} else {
			HC595DataLow();												// MSB is 0 so output high
		}
		HC595Pulse();													// Pulse the Clock line
		data=data<<1;													// Now bring next bit at MSB position
	}
	HC595Latch();														// Now all 8 bits have been transferred to shift register, move them to output latch at one
}

//----------------------END 74HC595 Interface Code----------------------

static inline void initTimer1(void) {									// initialize timer1
	OCR1A = SS_WRITE_SPEED;												// determines how fast we write to the seven segment displays
	TCCR1A |= (1 << COM1A1) | (1 << COM1A0);							// Set OC1A on Compare Match, clear OC1A at BOTTOM (inverting mode)
	TCCR1B |= (1 << WGM12);												// Mode 4, CTC on OCR1A
	TIMSK1 |= (1 << OCIE1A);											// Set interrupt on compare match
	TCCR1B |= (1 << CS12) | (1 << CS10);								// set prescaler to 1024 and start the timer
}

ISR(TIMER1_COMPA_vect) {												// ISR for Timer 1
	// Moving the switch clause in main() here yields strange behavior.  The correct values make it through
	// to HC595Write, but the same value is output on the shift registers output pins regardless; e.g. 2.
	// That value changes if the MCU is reset!
}

void initInturrupts() {													// Set up the interrupt used for the switch
	PCICR |= (1 << PCIE1);												// Set pin change interrupt for C pins
	PCMSK1 |= (1 << PCINT9);											// Set mask to look for PCINT9 / PC1
	sei();																// enable global interrupts
}

/* Interrupts for buttons and switches */
ISR (PCINT1_vect) {
	_delay_us(DEBOUNCE_TIME);
	if (bit_is_set(PINC, PINC1)) {
		switchPosition = 1;
	}
	_delay_us(DEBOUNCE_TIME);
	if (bit_is_clear(PINC, PINC1)) {            
		switchPosition = 0;
	}
}

void initPins() {
	DDRC &= ~(1 << DDC1);												// Set PC1 to input for switch
	PORTC |= (1 << PORTC1);												// Turn on the pull up
	DDRC &= ~((1 << DDC0) | (1 << DDC2));								// Set PC0 & PC2 to input for ADC
	DDRD |= (1 << DDD2)|(1 << DDD3)|(1 << DDD4);						// transistors for multiplexing seven segment display
	DDRB |= (1 << DDB6);												// make LED pin an output (PB6)
	DDRB |= (1 << DDB0)|(1 << DDB1)|(1 << DDB2);						// Set PB0, PB1, and PB2 as output for shift register (redundant)
}

void initSwitchPosition() {
	if (bit_is_set(PINC, PINC1)) {
		switchPosition = 1;
	}
	if (bit_is_clear(PINC, PINC1)) {            
		switchPosition = 0;
	}
}

void initADC() {
	ADMUX |= (1 << REFS0);												// reference voltage on AVCC 
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0);								// ADC clock prescaler  / 8 
	ADCSRA |= (1 << ADEN);												// enable ADC 
}

uint16_t readADC(int channel) {
	ADMUX &= 0b11110000;												// 0xF0 clear the older channel that was read
	ADMUX |= channel;													// Defines the new ADC channel to be read
																		// first sample after you've changed the
																		// multiplexer will still be from the old
																		// analog source, you need to wait at
																		// least one complete ADC cycle before
																		// getting a value from the new channel
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	ADCSRA |= (1 << ADSC);												// start ADC conversion
	loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
	uint8_t theLowADC = ADCL;
	uint16_t theTenBitResult = ADCH<<8 | theLowADC;
	return(theTenBitResult);
}

int main(void) {
	initUSART();
	initPins();
	initADC();
	initInturrupts();
	initHC595();														// Initialize HC595 system
	initSwitchPosition();												// determines if the switch is on or off
	charToLight = 0;
	while (1) {	

/*		if (bit_is_set(PINC, PINC1)) {
			switchPosition = 1;
		}
		if (bit_is_clear(PINC, PINC1)) {            
			switchPosition = 0;
		}
*/
		sensorDetected  = readADC(0b00000000) * 0.97654;					// ADC0 = PC0, MUX3..0 = 0000
		sensorThreshold = readADC(0b00000011) * 0.97654;					// ADC3 = PC3, MUX3..0 = 0011

 		if (switchPosition) {
			charPosition[2] = (sensorDetected / 100);
			charPosition[1] = ((sensorDetected - ((sensorDetected / 100) * 100)) / 10 );
			charPosition[0] = (sensorDetected - ((sensorDetected / 10) * 10));
		} else {
			charPosition[2] = (sensorThreshold / 100);
			charPosition[1] = ((sensorThreshold - ((sensorThreshold / 100) * 100)) / 10 );
			charPosition[0] = (sensorThreshold - ((sensorThreshold / 10) * 10));
		}
//		debugNoLF("SENSOR THRESHOLD: "); debugInt(sensorThreshold);
//		debugNoLF("SENSOR DETECTED: "); debugInt(sensorDetected);
		if (sensorDetected > sensorThreshold) {
			PORTB |= (1 << PORTB6);										// Sound the alarm
		}
		else {
			PORTB &= ~(1 << PORTB6);									// Silence the alarm
		}
		switch(charToLight) {											// determine which transistor to flip on
			case 0 :
				PORTD |= (1 << PORTD2);									// Turn off the transistor for the hundreds place
				HC595Write(ssCharMap[charPosition[0]]);
				PORTD &= ~(1 << PORTD4);								// Turn on the transistor for the ones place
				charToLight = 1;
				break;
			case 1 :
				PORTD |= (1 << PORTD4);									// Turn off the transistor for the ones place
				HC595Write(ssCharMap[charPosition[1]]);
				PORTD &= ~(1 << PORTD3);								// Turn on the transistor for the tens place
				charToLight = 2;
				break;
			case 2 :
				PORTD |= (1 << PORTD3);									// Turn off the transistor for the tens place
				HC595Write(ssCharMap[charPosition[2]]);
				PORTD &= ~(1 << PORTD2);								// Turn on the transistor for the hundreds place
				charToLight = 0;
				break;
		}
	}																	// End event loop 
	return (0);															// This line is never reached 
}