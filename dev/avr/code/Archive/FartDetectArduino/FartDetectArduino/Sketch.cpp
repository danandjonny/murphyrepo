﻿/*
********************************************************************
  Name    : FartDetector.c
  Author  : Daniel Murphy
  Date    : March 29, 2016
  Version : 3
  Notes   : The purpose of this program is to detect methane and sound
			an alarm (an active piezo and an LED) if the methane
			exceeds the threshold amount set by the potentiometer.
			The project displays numerals and characters on a 
			series of 3 single character seven segment displays set up
			in a circuit with S8550 PNP transistors for multiplexed
			switching. A shift register, 74HC595, is used to shuttle a  
			value to all of the SS displays, and the transistor for
			the character position for which the value is intended is
			switched on.  When the next number comes through the shift
			register, it's transistor is turned on, and so on.  Values
			are mapped so as to light the correct segments on the display.
			See FartDetector.h for the mapping.  The setup includes 8 330ohm
			resistors, 3 single character seven segment displays, 3
			S8550 transistors, a 74HC595 shift register, 4 resistors
			(brown, brown, black, red, red) for the transistor  
			switching, a methane sensor, and a potentiometer for 
			setting the alarm threshold.
********************************************************************

7 Segment (Common Anode) Display Map: 

    D   E  5V   F   G
 ___|___|___|___|___|____
|                        |
|        F               |
|    E       G           |
|        D               |
|    A       C           |
|        B       H(Dot)  |
|________________________|
    |   |   |   |   |
    A   B  5V   C   H

74HC595 Map:
     _______
Q1  |1 *  16|  Vcc                  PINS 1-7, 15   Q0 - Q7   Output Pins
Q2  |2    15|  Q0                   PIN 8    GND    Ground, Vss
Q3  |3    14|  DS                   PIN 9    Q7"    Serial Out
Q4  |4    13|  OE                   PIN 10   MR     Master Reclear, active low
Q5  |5    12|  ST_CP                PIN 11   SH_CP  Shift register clock pin
Q6  |6    11|  SH_CP                PIN 12   ST_CP  Storage register clock pin (latch pin)
Q7  |7    10|  MR                   PIN 13   OE     Output enable, active low
GND |8_____9|  Q7"                  PIN 14   DS     Serial data input
                                    PIN 16   Vcc    Positive supply voltage
           _______
    LED Q1-|1 *  16|-5V
    LED Q2-|2    15|-LED Q0
    LED Q3-|3    14|-PB0
    LED Q4-|4    13|-GND
    LED Q5-|5    12|-PB2
    LED Q6-|6    11|-PB1 ; 1uF TO GND (capacitor omitted)
DECIMAL Q7-|7    10|-5V
       GND-|8_____9|-NILL

The values passed in from the master map to characters as follows:

Array Value      Character
-----------      ---------
0                0
1                1
2                2
...
9                9
10               A
11               B
...
35               Z
(decimal point)  36
(space)          37

This mapping is controlled on the master in FartDetector.h.

 Pins
 ---------------------------------------
 PB0 - Data pin (DS) pin location for 74HC595 pin 14
 PB1 - Shift Clock (SH_CP) pin location for 74HC595 pin 11
 PB2 - Store Clock (ST_CP) pin location for 74HC595 pin 12
 PD2 - transistor for hundreds place
 PD3 - transistor for tens place
 PD4 - transistor for ones place
 PC1 - switch pin
 PC0 - input for ADC sensor input
 PC2 - input for ADC threshold input
 PB6 - output to sound alarm and light LED

 Reference: https://www.youtube.com/watch?v=BC5LSnm9R9k
 
 Clock speed is 8mHz, no external crystal required
 
 Code:                                      */
#include <Arduino.h>
#include "FartDetector.h"

int latchPin = 10;
int dataPin = 8;
int clockPin = 9;

void delayus( uint16_t z ) {

  while ( z ) {
    _delay_us( 100 );													// delay 100 microseconds of a second
    z--;
  }
}

//--------------Debug Routines-----------------------------------------
void debug(const char myString[]) {
  if (DEBUG) {
    uint8_t i = 0;
    while (myString[i]) {
      transmitByte(myString[i]);
      i++;
    }
    printString("\r\n");
  }
}

void debugNoLF(const char myString[]) {
  if (DEBUG) {
    uint8_t i = 0;
    while (myString[i]) {
      transmitByte(myString[i]);
      i++;
    }
  }
}

void debugNoLN(const char myString[]) {
  if (DEBUG) {
    uint8_t i = 0;
    while (myString[i]) {
      transmitByte(myString[i]);
      i++;
    }
  }
}

void debugInt(signed int anInt) {
  if (DEBUG) {
    char myInt[4];
    itoa(anInt,myInt,10);
    debug(myInt);
  }
}

void initPins() {
  DDRC &= ~(1 << DDC1);													// Set PC1 to input for switch
  PORTC |= (1 << PORTC1);												// Turn on the pull up
  DDRC &= ~((1 << DDC0) | (1 << DDC2));									// Set PC0 & PC2 to input for ADC
  DDRD |= (1 << DDD2)|(1 << DDD3)|(1 << DDD4);							// transistors for multiplexing seven segment display
  DDRB |= (1 << DDB6);													// make LED pin an output (PB6)
  DDRB |= (1 << DDB0)|(1 << DDB1)|(1 << DDB2);							// Set PB0, PB1, and PB2 as output for shift register (redundant)
}

void initSwitchPosition() {
  if (bit_is_set(PINC, PINC1)) {
    switchPosition = 1;
  }
  _delay_us(DEBOUNCE_TIME);
  if (bit_is_clear(PINC, PINC1)) {            
    switchPosition = 0;
  }
}

void initADC() {
  ADMUX |= (1 << REFS0);												// reference voltage on AVCC 
  ADCSRA |= (1 << ADPS1) | (1 << ADPS0);								// ADC clock prescaler  / 8 
  ADCSRA |= (1 << ADEN);												// enable ADC 
}

uint16_t readADC(int channel) {
  ADMUX &= 0b11110000;													// 0xF0 clear the older channel that was read
  ADMUX |= channel;														// Defines the new ADC channel to be read
																		// first sample after you've changed the
																		// multiplexer will still be from the old
																		// analog source, you need to wait at
																		// least one complete ADC cycle before
																		// getting a value from the new channel
  ADCSRA |= (1 << ADSC);												// start ADC conversion
  loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
  ADCSRA |= (1 << ADSC);												// start ADC conversion
  loop_until_bit_is_clear(ADCSRA, ADSC);								// wait until done
  uint8_t theLowADC = ADCL;
  uint16_t theTenBitResult = ADCH<<8 | theLowADC;
  return(theTenBitResult);
}

void setup() {
	initUSART();														// REMOVE THIS ONCE DEVELOPMENT IS COMPLETE
	initPins();
	initADC();
	initSwitchPosition();												// determines if the switch is on or off
	charToLight = 0;  
}

void loop(void) {														// determine the location of the switch
	if (bit_is_set(PINC, PINC1)) {
		switchPosition = 1;												// display fart detected
	}
	if (bit_is_clear(PINC, PINC1)) {            
		switchPosition = 0;												// display fart threshold
	}
	
    fartDetected  = readADC(0b00000000) * 0.97654;						// ADC0 = PC0, MUX3..0 = 0000 (999/1023 = 0.97654)
    fartThreshold = readADC(0b00000011) * 0.97654;						// ADC3 = PC3, MUX3..0 = 0011 (999/1023 = 0.97654)
//  debugNoLF("FART THRESHOLD: "); debugInt(fartThreshold);
//  debugNoLF("FART DETECTED: "); debugInt(fartDetected);
    if (switchPosition) {
		charPosition[0]=(fartDetected/100);								// hundreds
		charPosition[1]=((fartDetected-((fartDetected/100)*100))/10);	// tens
		charPosition[2]=(fartDetected-((fartDetected/10)*10));			// ones
    } else {
		charPosition[0]=(fartThreshold/100);							// hundreds
		charPosition[1]=((fartThreshold-((fartThreshold/100)*100))/10);	// tens
		charPosition[2]=(fartThreshold-((fartThreshold/10)*10));		// ones
    }

    if (fartDetected > fartThreshold) {									// Determine if the alarm needs to sound
		PORTB |= (1 << PORTB6);											// Sound the alarm
    }
    else {
		PORTB &= ~(1 << PORTB6);										// Silence the alarm
    }

  switch(charToLight) {													// determine which transistor to flip on
	case 0 :
		digitalWrite(2, HIGH);											// turn off the switch for the hundreds place
		digitalWrite(latchPin, LOW);
		shiftOut(dataPin, clockPin, MSBFIRST, ssCharMap[charPosition[2]]);
		digitalWrite(latchPin, HIGH);									// take the latch pin high so the LEDs will light up:
		digitalWrite(4, LOW);											// turn on the switch for the ones place
		charToLight = 1;
		break;
	case 1 :
		digitalWrite(4, HIGH);											// turn off the switch for the ones place
		digitalWrite(latchPin, LOW);
		shiftOut(dataPin, clockPin, MSBFIRST, ssCharMap[charPosition[1]]);
		digitalWrite(latchPin, HIGH);									// take the latch pin high so the LEDs will light up:
		digitalWrite(3, LOW);											// turn on the switch for the tens place
		charToLight = 2;
		break;
	case 2 :
		digitalWrite(3, HIGH);											// turn off the switch for the tens place
		digitalWrite(latchPin, LOW);
		shiftOut(dataPin, clockPin, MSBFIRST, ssCharMap[charPosition[0]]);
		digitalWrite(latchPin, HIGH);									// take the latch pin high so the LEDs will light up:
		digitalWrite(2, LOW);											// turn on the switch for the hundreds place
		charToLight = 0;
		break;
  }																		// end switch
}
/* REMOVE EVERYTHING FROM HERE DOWN WHEN DEVELOPMENT IS COMPLETE
  Quick and dirty functions that make serial communications work.

  Note that receiveByte() blocks -- it sits and waits _forever_ for
   a byte to come in.  If you're doing anything that's more interesting,
   you'll want to implement this with interrupts.

   initUSART requires BAUDRATE to be defined in order to calculate
     the bit-rate multiplier.  9600 is a reasonable default.

  May not work with some of the older chips:
    Tiny2313, Mega8, Mega16, Mega32 have different pin macros
    If you're using these chips, see (e.g.) iom8.h for how it's done.
    These old chips don't specify UDR0 vs UDR1.
    Correspondingly, the macros will just be defined as UDR.
*/

#include <avr/io.h>
#include "USART.h"
#include <util/setbaud.h>
//Beginning of Auto generated function prototypes by Atmel Studio
void delayus( uint16_t z );
void debug(const char myString[]);
void debugNoLF(const char myString[]);
void debugNoLN(const char myString[]);
void debugInt(signed int anInt);
void initHC595();
void HC595Pulse();
void HC595Latch();
void HC595Write(uint8_t data);
static inline void initTimer1(void);
//*void initTimer1(void);
void initInturrupts();
void initPins();
void initSwitchPosition();
void initADC();
uint16_t readADC(int channel);
void setup();
void loop(void);
void initUSART(void);
void transmitByte(uint8_t data);
uint8_t receiveByte(void);
void printString(const char myString[]);
void readString(char myString[], uint8_t maxLength);
void printByte(uint8_t byte);
void printWord(uint16_t word);
void printBinaryByte(uint8_t byte);
char nibbleToHexCharacter(uint8_t nibble);
void printHexByte(uint8_t byte);
uint8_t getNumber(void);
//End of Auto generated function prototypes by Atmel Studio


void initUSART(void) {                                /* requires BAUD */
  UBRR0H = UBRRH_VALUE;                        /* defined in setbaud.h */
  UBRR0L = UBRRL_VALUE;
#if USE_2X
  UCSR0A |= (1 << U2X0);
#else
  UCSR0A &= ~(1 << U2X0);
#endif
                                  /* Enable USART transmitter/receiver */
  UCSR0B = (1 << TXEN0) | (1 << RXEN0);
  UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);   /* 8 data bits, 1 stop bit */
}


void transmitByte(uint8_t data) {
                                     /* Wait for empty transmit buffer */
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = data;                                            /* send data */
}

uint8_t receiveByte(void) {
  loop_until_bit_is_set(UCSR0A, RXC0);       /* Wait for incoming data */
  return UDR0;                                /* return register value */
}


                       /* Here are a bunch of useful printing commands */

void printString(const char myString[]) {
  uint8_t i = 0;
  while (myString[i]) {
    transmitByte(myString[i]);
    i++;
  }
}

void readString(char myString[], uint8_t maxLength) {
  char response;
  uint8_t i;
  i = 0;
  while (i < (maxLength - 1)) {                   /* prevent over-runs */
    response = receiveByte();
    transmitByte(response);                                    /* echo */
    if (response == '\r') {                     /* enter marks the end */
      break;
    }
    else {
      myString[i] = response;                       /* add in a letter */
      i++;
    }
  }
  myString[i] = 0;                          /* terminal NULL character */
}

void printByte(uint8_t byte) {
              /* Converts a byte to a string of decimal text, sends it */
  transmitByte('0' + (byte / 100));                        /* Hundreds */
  transmitByte('0' + ((byte / 10) % 10));                      /* Tens */
  transmitByte('0' + (byte % 10));                             /* Ones */
}

void printWord(uint16_t word) {
  transmitByte('0' + (word / 10000));                 /* Ten-thousands */
  transmitByte('0' + ((word / 1000) % 10));               /* Thousands */
  transmitByte('0' + ((word / 100) % 10));                 /* Hundreds */
  transmitByte('0' + ((word / 10) % 10));                      /* Tens */
  transmitByte('0' + (word % 10));                             /* Ones */
}

void printBinaryByte(uint8_t byte) {
                       /* Prints out a byte as a series of 1's and 0's */
  uint8_t bit;
  for (bit = 7; bit < 255; bit--) {
    if (bit_is_set(byte, bit))
      transmitByte('1');
    else
      transmitByte('0');
  }
}

char nibbleToHexCharacter(uint8_t nibble) {
                                   /* Converts 4 bits into hexadecimal */
  if (nibble < 10) {
    return ('0' + nibble);
  }
  else {
    return ('A' + nibble - 10);
  }
}

void printHexByte(uint8_t byte) {
                        /* Prints a byte as its hexadecimal equivalent */
  uint8_t nibble;
  nibble = (byte & 0b11110000) >> 4;
  transmitByte(nibbleToHexCharacter(nibble));
  nibble = byte & 0b00001111;
  transmitByte(nibbleToHexCharacter(nibble));
}

uint8_t getNumber(void) {
  // Gets a numerical 0-255 from the serial port.
  // Converts from string to number.
  char hundreds = '0';
  char tens = '0';
  char ones = '0';
  char thisChar = '0';
  do {                                                   /* shift over */
    hundreds = tens;
    tens = ones;
    ones = thisChar;
    thisChar = receiveByte();                   /* get a new character */
    transmitByte(thisChar);                                    /* echo */
  } while (thisChar != '\r');                     /* until type return */
  return (100 * (hundreds - '0') + 10 * (tens - '0') + ones - '0');
}

