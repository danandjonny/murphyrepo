/*-------------------------------------------------------------------------------------
File:			328PXPm_LEDtest1.asm
Date:			1/19/2015 9:09:00 PM	

Target:			ATmega8
Assembler:		Atmel Studio 6.2
Author:			seawarrior 

Hardware:		ATmega8 
				16MHz (Crystal "Part: XC100")	
Summary:		Toggling PB5 (UserLED "Part: D200") with delay of 1 second
---------------------------------------------------------------------------------------*/
.NOLIST										//Turn off to decrease size of list
.INCLUDE "m8def.inc"						//Include device file
.LIST										//Turn on

.CSEG
.ORG	0x0000								//Beginning of Program Memory

MAIN:   LDI		R16, HIGH(RAMEND)			//Initialized the stack
		OUT		SPH, R16
		LDI		R16, LOW(RAMEND)
		OUT		SPL, R16
		CLR		R16

		LDI		R16, 0xFF					//Set portd as output
		OUT		DDRD, R16

REPEAT:	SBI		PORTD, 0					//Sets pin (PD0) high
		RCALL	DELAY
		CBI		PORTD, 0					//Sets pin (PD0) low
		RCALL	DELAY
		RJMP	REPEAT						//Repeat continuously 
//-------------------------------------------------------------------------------------
DELAY:	        LDI		R16, 5				//Create a Delay of 1s
LOOP1:	        LDI		R17, 200
LOOP2:	        LDI		R18, 250
LOOP3:	        NOP		
		DEC		R18
		BRNE	        LOOP3

		DEC		R17
		BRNE	        LOOP2

		DEC		R16
		BRNE	        LOOP1
		RET
