/*
 * SmallRobot.c
 *
 * Created: 7/6/2017 9:19:38 AM
 * Author : djmurphy
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>


/*
Robot version 1
ATtiny85, 2 motors, LED, two sensors

Sensor stuff: 
  both white  : no prior  : random walk
        : prior   : turn in direction of last black
  
  one white : ideal situation, move straight
  
  both black  : no prior  : random walk
        : prior   : turn in direction of last white

This code was written by Eric Heisler (shlonkin)
It is in the public domain, so do what you will with it.
*/
//#include "Arduino.h"

#define INPUT 0
#define OUTPUT 1

// Here are some parameters that you need to adjust for your setup
// Base speeds are the motor pwm values. Adjust them for desired motor speeds.
#define baseLeftSpeed 17
#define baseRightSpeed 17

// This determines sensitivity in detecting black and white
// measurment is considered white if it is > whiteValue*n/4
// where n is the value below. n should satisfy 0<n<4
// A reasonable value is 3. Fractions are acceptable.
#define leftSensitivity 3
#define rightSensitivity 3

// the pin definitions
#define rmotorpin PB0 //0 PB0 pin 5
#define lmotorpin PB1 //1 PB1 pin 6
#define rsensepin PB2 //1 ADC1 pin 7
#define lsensepin PB3 //3 ADC3 pin 2
#define ledpin PB4 //4 PB4 pin 3

// some behavioral numbers
// these are milisecond values
#define steplength 300
#define smallturn 200
#define bigturn 500
#define memtime 1000

// variables
uint8_t lspd, rspd; // motor speeds
uint16_t lsenseval, rsenseval, lwhiteval, rwhiteval; // sensor values

void followEdge();
void move(uint8_t lspeed, uint8_t rspeed);
void stop();
void senseInit();
void flashLED(uint8_t flashes);

// just for convenience and simplicity (HIGH is off)
#define ledoff PORTB &= ~(1<<PB4)
#define ledon PORTB |= (1<<PB4)

void setup();
void loop();
void InitADC0();
void initADC();
void pinMode(uint8_t pin, uint8_t mode);
void analogWrite(uint8_t pin, int value);
uint8_t analogRead(uint8_t channel);
void initTimer0();
void initTimer1();
void StartMillisecondsTimer();
uint32_t millis();
unsigned long micros();
void delayMS(int ticks);


volatile uint32_t milliseconds = 0;

int main() {
	setup();
//	int rightAnalogRead = 0;
//	int leftAnalogRead = 0;
	while (1 == 1) {
/*		leftAnalogRead = analogRead(PB2);
		rightAnalogRead = analogRead(PB3);
		move(leftAnalogRead,rightAnalogRead);
		flashLED(leftAnalogRead);
*/
		followEdge();
	}
	return 0;
}

void setup(){
	initADC();
	DDRB |= 1<<DDB0;					// set the data direction register for the right motor to OUTPUT.
	DDRB |= 1<<DDB1;					// set the data direction register for the left motor to OUTPUT.
	DDRB |= 1<<DDB4;					// set the data direction register for the LED to OUTPUT.
	DDRB &= ~(1<<DDB2);					// set the data direction register for the right sensor to INPUT
	DDRB &= ~(1<<DDB3);					// set the data direction register for the left sensor to INPUT
	initTimer0();
	initTimer1();
	sei();								// enable interrupts
	StartMillisecondsTimer();
    analogWrite(PB0, 0);
    analogWrite(PB1, 0);
	lspd = baseLeftSpeed;
	rspd = baseRightSpeed;
	lsenseval = 6;
	while(lsenseval) {					// give a few second pause to set the thing on a white surface
		lsenseval--;					// the LED will flash during this time
		flashLED(1);
		_delay_ms(989);
	}
	flashLED(4);
	_delay_ms(500);
	senseInit();
}

void initADC()
{
  /* this function initialises the ADC 

        ADC Prescaler Notes:
	--------------------

	   ADC Prescaler needs to be set so that the ADC input frequency is between 50 - 200kHz.
  
           For more information, see table 17.5 "ADC Prescaler Selections" in 
           chapter 17.13.2 "ADCSRA – ADC Control and Status Register A"
          (pages 140 and 141 on the complete ATtiny25/45/85 datasheet, Rev. 2586M–AVR–07/10)

           Valid prescaler values for various clock speeds
	
	     Clock   Available prescaler values
           ---------------------------------------
             1 MHz   8 (125kHz), 16 (62.5kHz)
             4 MHz   32 (125kHz), 64 (62.5kHz)
             8 MHz   64 (125kHz), 128 (62.5kHz)
            16 MHz   128 (125kHz)

           Below example set prescaler to 128 for mcu running at 8MHz
           (check the datasheet for the proper bit values to set the prescaler)
  */

  
/*  ADCSRA = 
            (1 << ADEN)  |     // Enable ADC (62.5kHz)
            (0 << ADPS2) |     // set prescaler to 8, bit 2 
            (1 << ADPS1) |     // set prescaler to 8, bit 1 
            (1 << ADPS0);      // set prescaler to 8, bit 0  
*/
	ADCSRA =
			(1 << ADEN)  |     // Enable ADC (62.5kHz)
			(1 << ADPS2) |     // set prescaler to 16, bit 2
			(0 << ADPS1) |     // set prescaler to 16, bit 1
			(0 << ADPS0);      // set prescaler to 16, bit 0
}

void initTimer0(void) {

	TCCR0A = 1 << COM0A0 | 1 << COM0A1 | 1 << COM0B0 | 1 << COM0B1 | 1 << WGM00 | 1 << WGM01;	// Fast PWM, Set OC0A/OC0B on Compare Match, clear OC0A/OC0B at BOTTOM (inverting mode)
	TCCR0B |= 1<<CS00;																			// no prescaling
}

void move(uint8_t lspeed, uint8_t rspeed){
	analogWrite(lmotorpin, lspeed);
	analogWrite(rmotorpin, rspeed);
}
/*
void stop(){
	analogWrite(lmotorpin, 0);
	analogWrite(rmotorpin, 0);
}
*/
void analogWrite(uint8_t pin, int val)
{
	val = 255 - val;		// light speeds up the motors
	if (pin == PB0) {
		OCR0A = val;		// set the right motor speed
	} else if (pin == PB1) {
		OCR0B = val;		// set the left motor speed
	}
}

void flashLED(uint8_t ticks){
	ledon;
	delayMS(ticks);
	ledoff;
	delayMS(ticks);
}

void delayMS(int ticks) {
	for (int i = 1; i <= ticks; i++) {
		_delay_ms(1);
	}
}

uint8_t  analogRead(uint8_t channel)
{

	switch (channel) {
		case PB2:
		ADMUX =
		// 8-bit resolution
		// set ADLAR to 1 to enable the Left-shift result (only bits ADC9..ADC2 are available)
		// then, only reading ADCH is sufficient for 8-bit results (256 values)
		(1 << ADLAR) |     // left shift result
		(0 << REFS1) |     // Sets ref. voltage to VCC, bit 1
		(0 << REFS0) |     // Sets ref. voltage to VCC, bit 0
		(0 << MUX3)  |     // use ADC2 for input (PB2), MUX bit 3
		(0 << MUX2)  |     // use ADC2 for input (PB2), MUX bit 2
		(0 << MUX1)  |     // use ADC2 for input (PB2), MUX bit 1
		(1 << MUX0);       // use ADC2 for input (PB2), MUX bit 0
		break;
		case PB3:
		ADMUX =
		(1 << ADLAR) |     // left shift result
		(0 << REFS1) |     // Sets ref. voltage to VCC, bit 1
		(0 << REFS0) |     // Sets ref. voltage to VCC, bit 0
		(0 << MUX3)  |     // use ADC2 for input (PB2), MUX bit 3
		(0 << MUX2)  |     // use ADC2 for input (PB2), MUX bit 2
		(1 << MUX1)  |     // use ADC2 for input (PB2), MUX bit 1
		(1 << MUX0);       // use ADC2 for input (PB2), MUX bit 0
		break;
	}

	ADCSRA |= (1 << ADSC);         // start ADC measurement
	while (ADCSRA & (1 << ADSC) ); // wait till conversion complete

	return ADCH;
}

void followEdge(){
  // now look for an edge
  uint8_t lastMove = 1;					// 0=straight, 1=left, 2=right
  unsigned long moveEndTime = 0;		// the millis at which to stop
  unsigned long randomBits = micros();	// for a random walk
  
  unsigned long prior = 0;				// after edge encounter set to millis + memtime
  uint8_t priorDir = 0;					// 0=left, 1=right, 2=both
  uint8_t lastSense = 1;				// 0=edge, 1=both white, 2=both black
  uint8_t i = 0;						// iterator
  
  while(1){
    // only read sensors about once every 20ms
    // it can be done faster, but this makes motion smoother
    _delay_ms(18);
    // read the value 4 times and average
    ledon;
    _delay_ms(1);
    lsenseval = 0;
    rsenseval = 0;
    for(i=0; i<4; i++){
      lsenseval += analogRead(lsensepin);
      rsenseval += analogRead(rsensepin);
    }
    // don't divide by 4 because it is used below
    ledoff;
    
    // refill the random bits if needed
    if(randomBits == 0){ randomBits = micros(); }
    
    // Here is the important part
    // There are four possible states: both white, both black, one of each
    // The behavior will depend on current and previous states
    if((lsenseval > lwhiteval*leftSensitivity) && (rsenseval > rwhiteval*rightSensitivity)){
      // both white - if prior turn toward black, else random walk
      if(lastSense == 2 || millis() < prior){
        // turn toward last black or left
        if(priorDir == 0){
          moveEndTime = millis()+smallturn;
          move(0, rspd); // turn left
          lastMove = 1;
        }else if(priorDir == 1){
          moveEndTime = millis()+smallturn;
          move(lspd, 0); // turn right
          lastMove = 2;
        }else{
          moveEndTime = millis()+bigturn;
          move(0, rspd); // turn left a lot
          lastMove = 1;
        }
      }else{
        // random walk
        if(millis() < moveEndTime){
          // just continue moving
        }else{
          if(lastMove){
            moveEndTime = millis()+steplength;
            move(lspd, rspd); // go straight
            lastMove = 0;
          }else{
            if(randomBits & 1){
              moveEndTime = millis()+smallturn;
              move(0, rspd); // turn left
              lastMove = 1;
            }else{
              moveEndTime = millis()+smallturn;
              move(lspd, 0); // turn right
              lastMove = 2;
            }
            randomBits >>= 1;
          }
        }
      }
      lastSense = 1;
      
    }else if((lsenseval > lwhiteval*leftSensitivity) || (rsenseval > rwhiteval*rightSensitivity)){
      // one white, one black - this is the edge
      // just go straight
      moveEndTime = millis()+steplength;
      move(lspd, rspd); // go straight
      lastMove = 0;
      lastSense = 0;
      prior = millis()+memtime;
      if(lsenseval > lwhiteval*leftSensitivity){
        // the right one is black
        priorDir = 1;
      }else{
        // the left one is black
        priorDir = 0;
      }
      
    }else{
      // both black - if prior turn toward white, else random walk
      if(lastSense == 1 || millis() < prior){
        // turn toward last white or left
        if(priorDir == 0){
          moveEndTime = millis()+smallturn;
          move(lspd, 0); // turn right
          lastMove = 2;
        }else if(priorDir == 1){
          moveEndTime = millis()+smallturn;
          move(0, rspd); // turn left
          lastMove = 1;
        }else{
          moveEndTime = millis()+bigturn;
          move(lspd, 0); // turn right a lot
          lastMove = 2;
        }
      }else{
        // random walk
        if(millis() < moveEndTime){
          // just continue moving
        }else{
          if(lastMove){
            moveEndTime = millis()+steplength;
            move(lspd, rspd); // go straight
            lastMove = 0;
          }else{
            if(randomBits & 1){
              moveEndTime = millis()+smallturn;
              move(0, rspd); // turn left
              lastMove = 1;
            }else{
              moveEndTime = millis()+smallturn;
              move(lspd, 0); // turn right
              lastMove = 2;
            }
            randomBits >>= 1;
          }
        }
      }
      lastSense = 2;
    }
  }
}

// stores the average of 16 readings as a white value
void senseInit(){
  lwhiteval = 0;
  rwhiteval = 0;
  ledon;
  _delay_ms(1);
  for(uint8_t i=0; i<16; i++){
    lwhiteval += analogRead(lsensepin);
    _delay_ms(1);
    rwhiteval += analogRead(rsensepin);
    _delay_ms(9);
  }
  lwhiteval >>= 4;
  rwhiteval >>= 4;
  ledoff;
}

uint32_t millis() {
	return milliseconds;
}

unsigned long micros() {
	return milliseconds * 1000;
}

void initTimer1(void) {														// Measures milliseconds
    //start timer, ctc mode, prescaler clk/16384
    TCCR1 = _BV(CTC1) | _BV(CS10) | _BV(CS11) | _BV(CS13);					// start timer, ctc mode, 1024 prescaler 
	//TIMSK1 |= (1 << OCIE1A);												// Set interrupt on compare match
	TIMSK = _BV(OCIE1A) | _BV(OCIE1B);										// Set interrupt on compare match
	OCR1A = 0;																// Fire interrupt every millisecond
}

ISR (TIMER1_COMPA_vect)	{													// Timer2 interrupt, increment milliseconds
	//DEBUG_STRING("ISR (TIMER2_COMPA_vect)");
	milliseconds+=1;
}

void StartMillisecondsTimer(void) {
	milliseconds = 0;
	TCCR1 |= (1 << CS13) | (1 << CS12) | (1 << CS11);						// set prescaler to 8192 and start the timer
}

void StopMillisecondsTimer(void) {
	TCCR1 &= ~((1 << CS13)|(1 << CS12)|(1 << CS11)|(1 << CS10));
}
