package javanote;

import java.io.*;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * This class has the business objects (Note, NoteSet) and GUI objects (NoteMenu,
 * JavaNotePanel) that are used to manage Notes.  Together these form the
 * application.
 */
public class JavaNote {

    NoteSet noteSet;
    JavaNotePanel javaNotePanel;
    
    // Entry point
    public static void main(String[] args) {
        new JavaNote().run(args);                     
    }
    
    // Entry point for instanciated object
	public void run(String[] args) {
            String location = null;
            if ((args.length == 1) || args.length == 0) {
                if (args.length == 0) {
                    location = null;
                } else {
                    location = args[0];
                }
                load(location);
            } else {
                System.out.println("Supply the directory were notes are located as the only command line argument.");
            } 
        }

            
    public void load(String dir) {
        if (dir == null) {
            NoteSetFinder nsf = new NoteSetFinder();
            dir = nsf.browseForNoteSet(new JPanel(), this);
        }
//      this.manageNoteSet(dir);
        // Create and load the NoteSet given a directory loaded with text files
        noteSet = new NoteSet(dir);
        javaNotePanel = new JavaNotePanel(noteSet, this);
        
    }
                
    /**
    * The NoteSet object holds the collection of Notes.  Should this class
    * extend TreeMap<String, Note> instead of having a seperate object (allNotesMap)?
    */
	private class NoteSet {
		private String directory;
		private TreeMap<String, Note> allNotesMap;
		private File dir;
		private Note currentNote;
		
		NoteSet(String directory) {
			this.directory = directory;
			this.allNotesMap = new TreeMap<String, Note>();
			this.dir = new File(directory);
			String[] children = dir.list();
			buildNoteSet(children);
		}
		
		public Note getCurrentNote() {
		    return currentNote;
		}
		
		public void setCurrentNote(Note note) {
		    this.currentNote = note;
		}

		private void buildNoteSet(String[] fileList) {
			if (fileList == null) {
				System.out.println( "Either dir does not exist or is not a directory");
				return;
			} else { 
				for (int i=0; i < fileList.length; i++) {
					String filename = fileList[i];
					Note note = new Note(directory, filename);
					allNotesMap.put(filename,note);
				}
			}
		}

		public void putNote(Note note) {
			allNotesMap.put(note.getFileName(),note);
			setCurrentNote(note);
			note.save();
		}
		
		public Note getNote(String key) {
			Note aNote = (Note) allNotesMap.get(key);
			setCurrentNote(aNote); 
			return aNote;
		}

		public String[] getAllKeys() {
			String[] allKeys = new String[allNotesMap.size()];
			int k = 0;
			Set noteSet = allNotesMap.entrySet();
			Iterator i = noteSet.iterator();
			while (i.hasNext()) {
				Map.Entry me = (Map.Entry) i.next();
				String key = (String) me.getKey();
				allKeys[k++] = key;
			}
			return allKeys;
		}
		
		public String getDirectory() {
		    return directory;
        }		    
		
		public void printNoteSet() {
			Set noteSet = allNotesMap.entrySet();
			Iterator i = noteSet.iterator();
			while (i.hasNext()) {
				Map.Entry me = (Map.Entry) i.next();
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				System.out.print(me.getKey() + ": ");
				System.out.println(me.getValue());
			}
		}
	}
	
    /**
    * This class defines the Note objects.  Every note is a Note object.
    */
	private class Note {

		private String fileName;
		private String contents;
		private String fullFileName;
		private String directory;
		
		Note(String directory, String fileName) {
		   this.directory = directory;
		   this.fileName = fileName;
                   this.fullFileName = directory + "\\" + fileName;
		   loadContents();
		}

		private void loadContents() {
			try {
				this.contents = new String(Files.readAllBytes(Paths.get(this.fullFileName)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void save() {
			try {
				File f = new File(this.getFullFileName());
				PrintWriter out = new PrintWriter(this.getFullFileName());
				out.println(this.getContents());
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		public String getFileName() {
			return this.fileName;
		}
		
		public String getFullFileName() {
		    return this.fullFileName;
        }
        		
		public String toString() {
			return this.fileName + ":\n" + this.contents;
		}
		
		public String getContents() {
			return this.contents;
		}

		public void setContents(String contents) {
			this.contents = contents;
		}
	}
    
    /**
    * This is the user interface class 
    */
    private class JavaNotePanel extends JPanel {
        String label[];
        
        JList list;
        JTextArea text;
        NoteSet noteSet;
        JavaNote javaNote;

        /**
        * This is the constructor.  We require handles to the NoteSet and the 
        * JavaNote objects so that we can invoke methods on those objects.
        */
        public JavaNotePanel(NoteSet noteSet, JavaNote javaNote) {
            this.noteSet = noteSet;
            this.javaNote = javaNote;
            setList(noteSet.getAllKeys());
            this.setLayout(new BorderLayout( ));
            list = new JList(label);
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            list.addListSelectionListener(new SelectListener());
            JScrollPane paneScroller = new JScrollPane(list);
            JButton printButton = new JButton("Print");
            printButton.addActionListener(new PrintListener( ));
            JButton buttonSave = new JButton("Save");
            buttonSave.addActionListener(new SaveListener());
            text = new JTextArea(10,20);
            JScrollPane textPaneScroller = new JScrollPane(text);
            text.setLineWrap(true);
            textPaneScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            textPaneScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            add(textPaneScroller);
            text.setText("Select a note to edit.");
            add(paneScroller, BorderLayout.WEST);
            JPanel south = new JPanel();
            south.add(printButton);
            south.add(buttonSave);
            add(south, BorderLayout.SOUTH);
            showList(noteSet.getDirectory());
        }

        /**
        * Assembles and renders the UI components.
        */
        public void showList(String noteLocation) {
            JFrame frame = new JFrame("Jonny's Note Manager: " + noteLocation);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setContentPane(this);
            frame.setSize(800, 600);
            frame.setJMenuBar(new NoteMenu(noteSet, this, javaNote));
            frame.pack( );
            frame.setVisible(true);
        }

        // Establishes a list of the filenames
        private void setList(String[] aList){
            label = aList;
        }
        
        // Gets the text that's in JTextArea
        public String getText() {
            return text.getText();
        }
        
        // An inner class to respond to clicks of the Print button
        class PrintListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                int selected[] = list.getSelectedIndices( );
                System.out.println("Selected Elements:  ");

                for (int i=0; i < selected.length; i++) {
                    String element =
                          (String)list.getModel( ).getElementAt(selected[i]);
                    System.out.println("  " + element);
                }
            }
        }
        
        // An inner class to respond to the Save button
        class SaveListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String newText = text.getText();
                String selection = (String) list.getSelectedValue();
                Note aNote = noteSet.getNote(selection);
                aNote.setContents(newText);
                aNote.save();
            }
        }

        // An inner class to respond when a Note's name is selected in the note list.
        class SelectListener implements ListSelectionListener {  // called twice with one mouse click
            public void valueChanged(ListSelectionEvent e) {
                String selection = (String) list.getSelectedValue();
                Note aNote = noteSet.getNote(selection);
                noteSet.setCurrentNote(aNote);
                text.setText(aNote.getContents());
            }
        }
    }
    
    /**
    * This class is for the application's menu.
    */
    private class NoteMenu extends JMenuBar {
    
        String[] fileItems = new String[] { "New", "Open", "Save", "Exit" };
        String[] editItems = new String[] { "Undo", "Cut", "Copy", "Paste" };
        char[] fileShortcuts = { 'N','O','S','X' };
        char[] editShortcuts = { 'Z','X','C','V' };
        
        NoteSet noteSet;
        JavaNotePanel javaNotePanel;
    
        public NoteMenu( NoteSet noteSet, JavaNotePanel javaNotePanel, JavaNote javaNote ) {
            this.javaNotePanel = javaNotePanel;
            this.noteSet = noteSet;
            JMenu fileMenu  = new JMenu("File");
            JMenu editMenu  = new JMenu("Edit");
            JMenu otherMenu = new JMenu("Other");
            JMenu subMenu   = new JMenu("SubMenu");
            JMenu subMenu2  = new JMenu("SubMenu2");
    
            ActionListener menuListener = new ActionListener( ) {
                    public void actionPerformed(ActionEvent event) {
                        System.out.println("Menu item [" + event.getActionCommand( ) +
                                           "] was pressed.");
                        if (event.getActionCommand().equals("Save")) {
                            System.out.println("Saving the note...");
                            Note aNote = noteSet.getCurrentNote();
                            aNote.setContents(javaNotePanel.getText());
                            aNote.save();
                        } else if (event.getActionCommand().equals("Exit")) {
                            System.out.println("Exiting the application...");
                            System.exit(0); // bug: kills all windows if multiple are open
                        } else if (event.getActionCommand().equals("Open")) {
                            System.out.println("Opening another note set...");
                            NoteSetFinder nsf = new NoteSetFinder();
                            String dir = nsf.browseForNoteSet(javaNotePanel, javaNote);
                            if (dir != null) {
                                javaNote.load(dir); 
                            }
                        }
                    }
                };
            // Assemble the File menus with mnemonics.
            for (int i=0; i < fileItems.length; i++) {
                JMenuItem item = new JMenuItem(fileItems[i], fileShortcuts[i]);
                item.addActionListener(menuListener);
                fileMenu.add(item);
            }
    
            // Assemble the File menus with keyboard accelerators.
            for (int i=0; i < editItems.length; i++) {
                JMenuItem item = new JMenuItem(editItems[i]);
                item.setAccelerator(KeyStroke.getKeyStroke(editShortcuts[i],
                    Toolkit.getDefaultToolkit( ).getMenuShortcutKeyMask( ), false));
                item.addActionListener(menuListener);
                editMenu.add(item);
            }
    
            // Insert a separator in the Edit menu in Position 1 after "Undo".
            editMenu.insertSeparator(1);
    
            // Assemble the submenus of the Other menu.
            JMenuItem item;
            subMenu2.add(item = new JMenuItem("Extra 2"));
            item.addActionListener(menuListener);
            subMenu.add(item = new JMenuItem("Extra 1"));
            item.addActionListener(menuListener);
            subMenu.add(subMenu2);
    
            // Assemble the Other menu itself.
            otherMenu.add(subMenu);
            otherMenu.add(item = new JCheckBoxMenuItem("Check Me"));
            item.addActionListener(menuListener);
            otherMenu.addSeparator( );
            ButtonGroup buttonGroup = new ButtonGroup( );
            otherMenu.add(item = new JRadioButtonMenuItem("Radio 1"));
            item.addActionListener(menuListener);
            buttonGroup.add(item);
            otherMenu.add(item = new JRadioButtonMenuItem("Radio 2"));
            item.addActionListener(menuListener);
            buttonGroup.add(item);
            otherMenu.addSeparator( );
            otherMenu.add(item = new JMenuItem("Potted Plant", 
                                 new ImageIcon("image.gif")));
            item.addActionListener(menuListener);
    
            // Finally, add all the menus to the menu bar.
            add(fileMenu);
            add(editMenu);
            add(otherMenu);
        }
    }
    
    /**
    *   This class allows the user to select a directory where notes exist 
    *   by browsing the directory structure.
    */
    private class NoteSetFinder {
        public String browseForNoteSet(JPanel panel, JavaNote javaNote) {
            System.out.println("Opening another note set...");
            JFileChooser chooser = new JFileChooser( );
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int option = chooser.showOpenDialog(panel);
            String dir = null;
            if (option == JFileChooser.APPROVE_OPTION) {
                dir = "C:\\" +     // obviously a hack, volume not available in JFileChooser
                      chooser.getCurrentDirectory().getName() + "\\" +
                      chooser.getSelectedFile( ).getName( );
                javaNote.load(dir);
            }
            else {
                System.out.println("You cancelled");
                dir = null;
            }
            return dir;
        }
    }
    
	/**
	* This method just shows examples of how the Note and NoteSet can be used.
    * You can ignore it 
	*/
	private void manageNoteSet(String noteLocation) {

            // Print all note keys and note values
            noteSet.printNoteSet();

            // Get a list of all of the keys back in a String array and print them
            String[] allKeys = noteSet.getAllKeys();
            for (String x : allKeys) {
                    System.out.println(x);
            }

            // Get the fifth Note in the 
            String aKey = allKeys[5];
            Note aNote = noteSet.getNote(aKey);

            // Print the contents of that Note.
            System.out.println(aNote.getContents());

            // Create a new Note and put it into the NoteSet
            Note myNote = new Note(noteLocation, "myNewNote.txt");
            myNote.setContents("One smart fellow he felt smart; Two smart fellows, they felt smart; Three smart fellows they all felt smart!");
            noteSet.putNote(myNote);
	}
}