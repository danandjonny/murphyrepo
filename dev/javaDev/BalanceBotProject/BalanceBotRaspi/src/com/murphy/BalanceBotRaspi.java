/** BalanceBotRaspi
 *
 * @author dmurphy 2018-03-14
 */
package com.murphy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.ByteBuffer;

import com.murphy.drivers.Mpu6050Controller;
import com.murphy.drivers.Mpu6050Registers;
import com.murphy.drivers.I2CLCDDisplay;

import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import com.pi4j.gpio.extension.base.AdcGpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008GpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerAnalog;
import com.pi4j.io.spi.SpiChannel;

public class BalanceBotRaspi {
    //Define execution of read sensors thread
    private volatile boolean shouldRun = true;
    private ControlLoop controlLoopTask;

    // Create gpio controller
    private final GpioController gpio = GpioFactory.getInstance();

    // Create an instance of BalanceBotRaspi
    private static final BalanceBotRaspi instance = new BalanceBotRaspi();
    
    // Create the LCD display controller
    private final I2CLCDDisplay I2CLCDController = new I2CLCDDisplay();

    private final Mpu6050Registers registers = new Mpu6050Registers();

    //Read MPU6050 with I2C comunication bus
    private final Mpu6050Controller accel = new Mpu6050Controller();;

    //Motor interface using GPIO comunication bus
    private final MotorController motors  = new MotorController();;

    // These values must be adjusted for convenience
    private final float ACCEL_OFFSET = 0.0F; 
    private final float GYRO_GAIN = 131F;       // 131 when MPU6050_RA_GYRO_CONFIG = 0
    private final float GYRO_OFFSET = 0.0F;
    //private final float DESIRED_ANGLE = 0.0F;
    private final float K = 0.0F;
    private final float RADIANS = 57.295779513F;

    private float angle_filtered = 0.0F;        // For angle formula using complimentary filter
    //private final float PI = 3.14159F;
    
    private long lastTime = 0;                  // Last time in PID calculation
    private float integral = 0;                 // PID sum errors
    private float lastErr = 0;                  // PID last error

    // Store accelerometer and gyroscope read data
    private long AccelY;
    private byte AccelY_H;                      // accelerometer Y axis high bit
    private byte AccelY_L;                      // accelerometer Y axis low bit
    
    private long AccelZ;
    private byte AccelZ_H;
    private byte AccelZ_L;

    private long GyroX;
    private byte GyroX_H;
    private byte GyroX_L;

    private long accelY_avg = 0;
    private long accelZ_avg = 0;
    private long gyroX_avg = 0;

    // Set point values used in PID controller.  These are set by potentiometers
    private float Kp = 50.0F;                    // 17F;
    private float Ki = 0.0F;                    // 0.1F;
    private float Kd = 0F;                      // 840F;

    private final int OUTMAX =    3;
    private final int OUTMIN =   -3;
    
    private long preTime = 0;                   // Last time in Filter calculation
    
    private int motorPower;

    private final int POT_MONITOR_INTERVAL = 1000; // milliseconds
    private final int POT_SET_EVENT_THRESHOLD = 2; // a number between 1 and 1023

    float angle_raw;
    float omega;
    float dt = 0.003F;;
    float A;
    
    float derivative;
    float iTerm;
    float timeChange = 0.003F;;
    
    
    //private constructor to avoid client applications to use constructor
    private BalanceBotRaspi(){}

    public static BalanceBotRaspi getInstance(){
        return instance;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BalanceBotRaspi i = BalanceBotRaspi.getInstance();
        i.startApp();
    }

     private void Filter() {

        //Read data from MPU6050 Accelerometer and Gyroscope
        try {
            AccelY_H = accel.readRegister(registers.MPU6050_RA_ACCEL_YOUT_H);
            AccelY_L = accel.readRegister(registers.MPU6050_RA_ACCEL_YOUT_L);
            AccelY = TwoCompl(AccelY_H<<8|AccelY_L);
            AccelZ_H = accel.readRegister(registers.MPU6050_RA_ACCEL_ZOUT_H);
            AccelZ_L = accel.readRegister(registers.MPU6050_RA_ACCEL_ZOUT_L);
            AccelZ = TwoCompl(AccelZ_H<<8|AccelZ_L);
            GyroX_H = accel.readRegister(registers.MPU6050_RA_GYRO_XOUT_H);
            GyroX_L = accel.readRegister(registers.MPU6050_RA_GYRO_XOUT_L);
            GyroX = TwoCompl(GyroX_H<<8|GyroX_L);
        } catch (IOException ioe) {
            System.out.println("IOException: " + ioe.getMessage());
        }
        //Calculate angle and convert from radians to degrees
        angle_raw = (float) (Math.atan2(AccelY - accelY_avg, (AccelZ - (accelZ_avg - 16384))) * RADIANS + ACCEL_OFFSET);
        omega = (float) (GyroX / GYRO_GAIN + GYRO_OFFSET);
        // Filters data to get the real value
//      long now = System.currentTimeMillis();
//      dt = 0.003F;
//      float dt = (float) ((now - preTime) / 1000.00);
//      preTime = now;
        //Calculate error using complimentary filter 
        A = K / (K + dt);
        angle_filtered = A * (angle_filtered + omega * dt) + (1 - A) * angle_raw;
//        System.out.println("af: "+ angle_filtered + " A: " + A + " omega: " + omega + " dt: " + dt + " ar: " + angle_raw);        
    }

    /*
     * Proportional, Integral, Derivative control.
     */
    private void PID() {
//      long now = System.currentTimeMillis();
//      timeChange = 0.003F;
//      float timeChange = (float) (now - lastTime) / 1000.00F;
//      lastTime = now;
        //float error = angle_filtered - DESIRED_ANGLE;  // Proportion
        integral += angle_filtered * timeChange;  // Integration
        derivative = (angle_filtered - lastErr) / timeChange;  // Differentiation
        iTerm = Ki * integral;
        if ((iTerm > OUTMAX) || (iTerm < OUTMIN) ) integral -= angle_filtered * timeChange; // Prevents Windup        
        motorPower = (int) (Kp * angle_filtered + Ki * integral + -Kd * derivative);
        lastErr = angle_filtered;
//      motorPower = (int) Map(motorPower, -255, 255, -100, 100);
//System.out.println(" pwr: " + motorPower + "\tAngl: " + angle_filtered + "\tAcY: " + (AccelY - accelY_avg) + "\tAcZ: " + (AccelZ - (accelZ_avg - 16384)) + "\tGyX: " + GyroX);
//System.out.println("AccelY: " + AccelY + " accelY_avg: " + accelY_avg + " AccelY - accelY_avg: " + (AccelY - accelY_avg));
    
    }

   /*
    * PWM Motor control 
    */
    private void PWMControl() {
 //     System.out.println("motorPower: " + motorPower);
        if (motorPower > 0) {
            motors.moveLeftForward(motorPower);
            motors.moveRightForward(motorPower);
        } else if (motorPower < 0) {
            motors.moveLeftBackward(motorPower * -1);
            motors.moveRightBackward(motorPower * -1);
        } else {
            motors.leftMotorBrake();
            motors.rightMotorBrake();
        }
    }
    
    /*
     * Thread for move and balance robot
     */
    class ControlLoop extends Thread {

        @Override
        public void run() {
            lastTime = System.currentTimeMillis();
            while (shouldRun) {
                Filter();
                // If angle > 180 or < -180 then stop the robot
                if ((angle_filtered < 180.0F) && (angle_filtered > -180.0F)) {
                    PID();
                    PWMControl();
               } else {
                    LogThis("Angle off. " + angle_filtered);
                    motors.rightMotorBrake();
                    motors.leftMotorBrake();
                    
                    // Keep reading accelerometer and gyroscope values after falling down
                    for (int i = 0; i < 100; i++) {
                        Filter();
                    }

                    if (Math.abs(angle_filtered) < 45) // Empty data and restart the robot automaticlly
                    {
                        for (int i = 0; i <= 500; i++) // Reset the robot and delay 2 seconds
                        {
                            angle_filtered = 0;
                            Filter();
                            integral = 0;
                            PID();
                        }
                    }
                }
            }
        }
    }

    public void startApp() {
        LogThis("BalanceBotRaspi starting");
        try {
            // Initialize the LCD display
//            try {
//                I2CLCDController.initialize();
//                LogThis(Level.INFO, "Initializing","Initializing",null);
//            } catch (Exception ex) {
//                LogThis(Level.WARNING, ex.getMessage());
//            }
            
            // Create custom MCP3008 analog gpio provider
            // we must specify which chip select (CS) that that ADC chip is physically connected to.
            LogThis(Level.INFO, "Init MCP3008","", "MCP3008");
            final AdcGpioProvider provider = new MCP3008GpioProvider(SpiChannel.CS0);

            // Provision gpio analog input pins for all channels of the MCP3008.
            // (you don't have to define them all if you only use a subset in your project)
            final GpioPinAnalogInput inputs[] = {
                    gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH0, "Kp-CH0"),
                    gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH1, "Ki-CH1"),
                    gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH2, "Kd-CH2")
            };
            provider.setEventThreshold(POT_SET_EVENT_THRESHOLD, inputs); // all inputs; alternatively you can set thresholds on each input discretely
            provider.setMonitorInterval(POT_MONITOR_INTERVAL); // Set the background monitoring interval timer for the underlying framework to interrogate the ADC chip for input conversion values.  

            // Print current analog input conversion values from each input channel
            for(GpioPinAnalogInput input : inputs){
                float value = (float) input.getValue();
                if        (input.getName() == "Kp-CH0") {
                    value /= 10;
                    Kp = value;
                    LogThis("Initial Kp: " + Kp);
                } else if (input.getName() == "Ki-CH1") {
                    value /= 100;
                    Ki = value;
                    LogThis("Initial Ki: " + Ki);
                } else if (input.getName() == "Kd-CH2") {
                    value /= 10;
                    Kd = value;
                }
                LogThis("<INITIAL VALUE> [" + input.getName() + "] : RAW VALUE = " + value);
           }

            // Create an analog pin value change listener
            GpioPinListenerAnalog listener = new GpioPinListenerAnalog()
            {
                @Override
                public void handleGpioPinAnalogValueChangeEvent(GpioPinAnalogValueChangeEvent event)
                {
                    // get RAW value
                    float value = (float) event.getValue();

                    // display output
                    String pinString = event.getPin().getName();
                    
                    if (        pinString.equalsIgnoreCase("Kp-CH0")) {
                        Kp = value / 10;
                    } else if ( pinString.equalsIgnoreCase("Ki-CH1")) {
                        Ki = value / 100;
                    } else if ( pinString.equalsIgnoreCase("Kd-CH2")) {
                        Kd = value / 10; 
                    }
                    LogThis("Kp: " + Kp + "\tKi: " + Ki + "\tKd: " + Kd);
                }
            };
            // Register the gpio analog input listener for all input pins
            gpio.addListener(listener, inputs);


            //Activate MPU6050 with I2C bus
            LogThis(Level.INFO,"Init MPU6050","","Init MPU6050");
            try {
                accel.initialize();
            } catch (InterruptedException ex) {
                LogThis(Level.WARNING, ex.getMessage());
                destroyApp();
            } catch (UnsupportedBusNumberException ex) {
                LogThis(Level.WARNING, ex.getMessage());
                destroyApp();
            }

            // Set MPU6050 offsets based on callibration exercise.
            try {
                accel.setMPU6050Offsets((short) 114,(short) -29,(short) -30,(short) -3611,(short) -61,(short) 1533);    // MPU6050 #1
//              accel.setMPU6050Offsets((short) 77,(short) -63,(short) 16,(short) -3888,(short) -1594,(short) 1318);    // MPU6050 #2
            } catch (IOException ex) {
                LogThis(Level.WARNING, ex.getMessage());
                destroyApp();
            }
            
            long accelY_sum = 0;
            long accelZ_sum = 0;
            long gyroX_sum = 0;
            // initialize 
            for (int i = 1; i <= 1000; i++) { // calculate average offset.  Robot must start in balanced position. Ideally this should not be necessary.
                Filter();
                accelY_sum += AccelY;
                accelY_avg = accelY_sum / i;
                accelZ_sum += AccelZ;
                accelZ_avg = accelZ_sum / i;
                gyroX_sum += GyroX;
                gyroX_avg = gyroX_sum / i;
            }
            LogThis("accelY_avg: " + accelY_avg + " accelZ_avg: " + accelZ_avg + " gyroX_avg: " + gyroX_avg);
            LogThis("Kp: " + Kp + "\tKi: " + Ki + "\tKd: " + Kd);
            //Start move and balance thread
            controlLoopTask = new ControlLoop();
            controlLoopTask.start();
        } catch (IOException ex) {
            LogThis(Level.WARNING, ex.getMessage());
            destroyApp();
        }
    }

    public void destroyApp() {
        //Stop thread
        shouldRun = false;
        gpio.shutdown(); // When your program is finished, make sure to stop all GPIO activity/threads by shutting down the GPIO controller (this method will forcefully shutdown all GPIO monitoring threads and background scheduled tasks)
        LogThis(Level.INFO, "BalanceBotRaspi done", "Game", "Over");
    }

    private long TwoCompl(long value) {
        return value >= 0x8000 ? -((65535 - value) + 1) : value;
    }
    
    private long Map(long x, long in_min, long in_max, long out_min, long out_max)
    {
      return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
    
    private void LogThis(String message) {
        LogThis(Level.INFO, message, null, null);
    }

    private void LogThis(Level level, String message) {
        LogThis(level, message, null, null);
    }
    
    private void LogThis(Level level, String message, String shortMessage1, String shortMessage2) {
        Logger.getGlobal().log(level, message);
        System.out.println(message);
        if (shortMessage1 != null) {
            I2CLCDController.writeLineOne(shortMessage1);
        } 
        if (shortMessage2 != null) {
            I2CLCDController.writeLineTwo(shortMessage2);
        }
    }
}
