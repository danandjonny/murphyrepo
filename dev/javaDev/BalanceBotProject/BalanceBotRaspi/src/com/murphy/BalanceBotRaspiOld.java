/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dmurphy
 */
package com.murphy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.murphy.drivers.L298Device;
import com.murphy.drivers.Mpu6050Controller;
import com.murphy.drivers.Mpu6050Registers;
//import com.jcruz.demos.i2c.driver.MPU6050Device;
//import com.jcruz.demos.i2c.driver.MPU6050Device.AccelGyroRaw;
import com.jcruz.demos.log.LoggingHandler;

import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JBalancePI v1.3 Raspberry Pi two wheels balance robot firmware
 *
 * @author jcruz
 */
public class BalanceBotRaspi {

    private static final BalanceBotRaspi instance = new BalanceBotRaspi();
    
    private final LoggingHandler loggerHandler = LoggingHandler.getInstance();

    private long AccelX;
    private byte AccelX_H;
    private byte AccelX_L;
    private long AccelY;
    private byte AccelY_H;
    private byte AccelY_L;
    private long AccelZ;
    private byte AccelZ_H;
    private byte AccelZ_L;

    private long GyroX;
    private byte GyroX_H;
    private byte GyroX_L;
    private long GyroY;
    private byte GyroY_H;
    private byte GyroY_L;
    private long GyroZ;
    private byte GyroZ_H;
    private byte GyroZ_L;

        // These values must be adjusted for convenience
    private final float accel_offset = 3.7F;
    private final float gyro_gain = 131F;
    private final float gyro_offset = 0.0F;

    //Set point values used in PID controller
    private final float Kp = 17F;
    private final float Kd = 840F;
    private final float Ki = 0.1F;

    //For angle formula using complimentary filter
    private float angle_filtered = 0.0F;
    private final float pi = 3.14159F;

    //Last time in Filter calculation
    long preTime = 0;
    //Last time in PID calculation
    long lastTime = 0;

    //PID sum errors
    float errSum = 0;
    //PID last error
    float lastErr = 0;

    //For future remote control implementation.
    float Turn_Speed = 0, Turn_Speed_K = 0;
    float Run_Speed = 0, Run_Speed_K = 0, Run_Speed_T = 0;

    //>0 move motor forward, <0 move motor back
    private float LOutput, ROutput;

    private Mpu6050Registers registers = new Mpu6050Registers();
    //Read MPU6050 with I2C comunication bus
    private Mpu6050Controller accel;
    //Motor interface using GPIO comunication bus
//  L298Device motor;
    MotorController motors;
    //Store accelerometer and gyroscope read data
//  AccelGyroRaw data;

    //Define execution of read sensors thread
    private volatile boolean shouldRun = true;
    private ControlLoop controlLoopTask;

    
    //private constructor to avoid client applications to use constructor
    private BalanceBotRaspi(){}

    public static BalanceBotRaspi getInstance(){
        return instance;
    }
    
     /**
     * Read data from accelerometer and gyroscope and apply a complimentary
     * filter
     */
    private void Filter() {
        //Data from MPU6050 Accelerometer and Gyroscope
        try {
            AccelX_H = accel.readRegister(registers.MPU6050_RA_ACCEL_XOUT_H);
            AccelX_L = accel.readRegister(registers.MPU6050_RA_ACCEL_XOUT_L);
            AccelX = TwoCompl(AccelX_H<<8|AccelX_L);

            AccelY_H = accel.readRegister(registers.MPU6050_RA_ACCEL_YOUT_H);
            AccelY_L = accel.readRegister(registers.MPU6050_RA_ACCEL_YOUT_L);
            AccelY = TwoCompl(AccelY_H<<8|AccelY_L);

            AccelZ_H = accel.readRegister(registers.MPU6050_RA_ACCEL_ZOUT_H);
            AccelZ_L = accel.readRegister(registers.MPU6050_RA_ACCEL_ZOUT_L);
            AccelZ = TwoCompl(AccelZ_H<<8|AccelZ_L);


            GyroX_H = accel.readRegister(registers.MPU6050_RA_GYRO_XOUT_H);
            GyroX_L = accel.readRegister(registers.MPU6050_RA_GYRO_XOUT_L);
            GyroX = TwoCompl(GyroX_H<<8|GyroX_L);

            GyroY_H = accel.readRegister(registers.MPU6050_RA_GYRO_YOUT_H);
            GyroY_L = accel.readRegister(registers.MPU6050_RA_GYRO_YOUT_L);
            GyroY = TwoCompl(GyroY_H<<8|GyroY_L);

            GyroZ_H = accel.readRegister(registers.MPU6050_RA_GYRO_ZOUT_H);
            GyroZ_L = accel.readRegister(registers.MPU6050_RA_GYRO_ZOUT_L);
            GyroZ = TwoCompl(GyroZ_H<<8|GyroZ_L);
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        }
        //Calculate angle and convert from radians to degrees
        float angle_raw = (float) (Math.atan2(AccelY, AccelZ) * 180.00 / pi + accel_offset);
        float omega = (float) (GyroX / gyro_gain + gyro_offset);
        // Filters data to get the real value
        long now = System.currentTimeMillis();
        float dt = (float) ((now - preTime) / 1000.00);
        preTime = now;
        //Calculate error using complimentary filter 
        float K = 0.8F;
        float A = K / (K + dt);
        angle_filtered = A * (angle_filtered + omega * dt) + (1 - A) * angle_raw;
    }

    /*
     * Proportional, Integral, Derivative control.
     */
    private void PID() {
        long now = System.currentTimeMillis();
        int timeChange = (int) (now - lastTime);
        lastTime = now;
        float error = angle_filtered;  // Proportion
        errSum += error * timeChange;  // Integration
        float dErr = (error - lastErr) / timeChange;  // Differentiation
        float output = Kp * error + Ki * errSum + Kd * dErr;
        lastErr = error;
        LOutput = output - Turn_Speed + Run_Speed;
        ROutput = output + Turn_Speed + Run_Speed;
    }

    /*
     * PWM Motor control 
     */
    private void PWMControl() {
        if (LOutput > 0) {
            motors.moveLeftForward(100 * (LOutput/256));
        } else if (LOutput < 0) {
            motors.moveLeftBackward(100 * (LOutput/256));
        } else {
            motors.leftMotorBrake();
        }
        
        if (ROutput > 0) {
            motors.moveRightForward(100 * (ROutput/256));
        } else if (ROutput < 0) {
            motors.moveRightBackward(100 * (ROutput/256));
        } else {
            motors.rightMotorBrake();
        }
        // L & ROutput should be between 0 and 256
        // https://cdn-shop.adafruit.com/datasheets/PCA9685.pdf page 17
        Logger.getGlobal().log(Level.INFO, "LOutput: " + LOutput);
        Logger.getGlobal().log(Level.INFO, "ROutput: " + ROutput);
//      motor.motorL_PWM((short) (Math.min(4095, Math.abs(LOutput) * 4095 / 256)));
//      motor.motorR_PWM((short) (Math.min(4095, Math.abs(ROutput) * 4095 / 256)));
    }

    /*
     * Thread for move and balance robot
     */
    class ControlLoop extends Thread {

        @Override
        public void run() {
            
            while (shouldRun) {
                Filter();
                Logger.getGlobal().log(Level.INFO, "Angle = " + angle_filtered);
                // If angle > 45 or < -45 then stop the robot
                if (Math.abs(angle_filtered) < 45) {
                    PID();
                    PWMControl();
                } else {
                    motors.rightMotorBrake();
                    motors.leftMotorBrake();

                    // Keep reading accelerometer and gyroscope values after falling down
                    for (int i = 0; i < 100; i++) {
                        Filter();
                    }

                    if (Math.abs(angle_filtered) < 45) // Empty data and restart the robot automaticlly
                    {
                        for (int i = 0; i <= 500; i++) // Reset the robot and delay 2 seconds
                        {
                            angle_filtered = 0;
                            Filter();
                            errSum = Run_Speed = Turn_Speed = 0;
                            PID();
                        }
                    }

                }
            }
            accel.close();
            motor.close();
        }
    }

    /**
     *
     */
    public void startApp() {

        loggerHandler.start();
        Logger.getGlobal().setLevel(Level.INFO);

        Logger.getGlobal().log(Level.INFO, "***** JBalancePi v1.3 Started *****");
        try {
            //Activate MPU6050 with I2C bus
            accel = new Mpu6050Controller();
            try {
                accel.initialize();
            } catch (InterruptedException ex) {
                Logger.getGlobal().log(Level.WARNING, ex.getMessage());
            } catch (UnsupportedBusNumberException ex) {
                Logger.getGlobal().log(Level.WARNING, ex.getMessage());
            }
            //Activate motor control with GPIO bus In1=27, In2=22, In3=24, In4=25
//          motor = new L298Device(27, 22, 24, 25);
            motors = new MotorController();
            // Looping 200 times to get the real values when starting
            for (int i = 0; i < 200; i++) {
                Filter();
            }
            if (Math.abs(angle_filtered) < 45) // Start the robot after cleaning data
            {
                angle_filtered = 0;
                Filter();
                errSum = Run_Speed = Turn_Speed = 0;
                PID();
            }
            //Start move and balance thread
            controlLoopTask = new ControlLoop();
            controlLoopTask.start();

        } catch (IOException ex) {
            Logger.getGlobal().log(Level.WARNING, ex.getMessage());
        }
    }

    public void destroyApp(boolean unconditional) {
        //Stop thread
        shouldRun = false;
        Logger.getGlobal().log(Level.INFO, "***** JBalancePi v1.3 Stopped *****");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BalanceBotRaspi i = BalanceBotRaspi.getInstance();
        i.startApp();
    }

    private long TwoCompl(long value) {
        return value >= 0x8000 ? -((65535 - value) + 1) : value;
    }
    

}
