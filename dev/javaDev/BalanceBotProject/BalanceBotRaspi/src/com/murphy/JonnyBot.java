/** JonnyBot
 *
 * @author jmurphy 2018-04-14
 */
package com.murphy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import com.pi4j.gpio.extension.base.AdcGpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008GpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerAnalog;
import com.pi4j.io.spi.SpiChannel;

public class JonnyBot {
    //Define execution of read sensors thread
    private volatile boolean shouldRun = true;
    private ControlLoop controlLoopTask;

    // Create gpio controller
    private final GpioController gpio = GpioFactory.getInstance();

    // Create an instance of JonnyBot
    private static final JonnyBot instance = new JonnyBot();
    
    //Motor interface using GPIO comunication bus
    private final MotorController motors  = new MotorController();;

    private int motorPower;
    
    //private constructor to avoid client applications to use constructor
    private JonnyBot(){}

    public static JonnyBot getInstance(){
        return instance;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JonnyBot i = JonnyBot.getInstance();
        i.startApp();
    }

    public void startApp() {
        LogThis("JonnyBot starting");
        controlLoopTask = new ControlLoop();
        controlLoopTask.start();
    }

    /*
    * PWM Motor control 
    */
    private void PWMControl() {
 //     System.out.println("motorPower: " + motorPower);
        if (motorPower > 0) {
            motors.moveLeftForward(motorPower);
            motors.moveRightForward(motorPower);
        } else if (motorPower < 0) {
            motors.moveLeftBackward(motorPower * -1);
            motors.moveRightBackward(motorPower * -1);
        } else {
            motors.leftMotorBrake();
            motors.rightMotorBrake();
        }
    }
    
    /*
     * Thread for move and balance robot
     */
    class ControlLoop extends Thread {

        @Override
        public void run() {
            while (shouldRun) {
                // Add robot control logic here 
                motorPower = 50;
                PWMControl();
            }
        }
    }

    public void destroyApp() {
        //Stop thread
        shouldRun = false;
        gpio.shutdown(); // When your program is finished, make sure to stop all GPIO activity/threads by shutting down the GPIO controller (this method will forcefully shutdown all GPIO monitoring threads and background scheduled tasks)
        LogThis(Level.INFO, "JonnyBot done", "Game", "Over");
    }

    private long Map(long x, long in_min, long in_max, long out_min, long out_max)
    {
      return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    private void LogThis(String message) {
        LogThis(Level.INFO, message, null, null);
    }

    private void LogThis(Level level, String message) {
        LogThis(level, message, null, null);
    }
    
    private void LogThis(Level level, String message, String shortMessage1, String shortMessage2) {
        Logger.getGlobal().log(level, message);
        System.out.println(message);
//        if (shortMessage1 != null) {
//            I2CLCDController.writeLineOne(shortMessage1);
//        } 
//        if (shortMessage2 != null) {
//            I2CLCDController.writeLineTwo(shortMessage2);
//        }
    }
}
