/** JonnyBot
 *
 * @author jmurphy 2018-04-14
 */
package com.murphy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import com.pi4j.gpio.extension.base.AdcGpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008GpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerAnalog;
import com.pi4j.io.spi.SpiChannel;

public class JonnyBot {
    //Define execution of read sensors thread
    private volatile boolean shouldRun = true;
    private ControlLoop controlLoopTask;

    // Create gpio controller
    private final GpioController gpio = GpioFactory.getInstance();

    // Create an instance of JonnyBot
    private static final JonnyBot instance = new JonnyBot();
    
    //Motor interface using GPIO comunication bus
    private final MotorController motors  = new MotorController();

	// The task that will read the SR-04 distance sensor. Call 
	// readSR04Task.getDistanceSR04() to get the distance in centimeters.
	private ReadSR04 readSR04Task = null;

    //private constructor to avoid client applications to use constructor
    private JonnyBot(){}

    public static JonnyBot getInstance(){
        return instance;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JonnyBot i = JonnyBot.getInstance();
        i.startApp();
    }

    public void startApp() {
        LogThis("JonnyBot starting");
        controlLoopTask = new ControlLoop();
        controlLoopTask.start();
        readSR04Task = new ReadSR04();
		try {
			readSR04Task.run();
		} catch (InterruptedException ex) {
			System.out.println(" InterruptedException: " + ex.getMessage());
		} 
    }

    /*
    * PWM Motor control 
    */
    private void PWMControl(int motorPower) {
		// motorPower is a number between 0 and 100
		// You will need to modify this (create new methods) to independently 
		// control the left and right motors so you can turn left and right.
        if (motorPower > 0) {
            motors.moveLeftForward(motorPower);
            motors.moveRightForward(motorPower);
            System.out.println("Forward: " + motorPower);
        } else if (motorPower < 0) {
            motors.moveLeftBackward(motorPower * -1);
            motors.moveRightBackward(motorPower * -1);
            System.out.println("Backwards: " + motorPower);
        } else {
            motors.leftMotorBrake();
            motors.rightMotorBrake();
            System.out.println("Brake");
        }
    }
    
    /*
     * Thread for move and balance robot
     */
    class ControlLoop extends Thread {
        @Override
        public void run() {
			System.out.println("Reached ControlLoop");
            while (shouldRun) {
				
				// Add robot control logic here

				// Optionally create another thread to make the servo pan 
				// back and fourth.
				
				// Periodically read the distance from the SR-04

				double distance = 0;

				// TEST CODE
				
				// Ramp up the speed, stop if there is an obsticle.
				for (byte i = 0; i <= 100; i++) {
					if (readSR04Task != null) {
						distance = readSR04Task.getDistanceSR04();
						System.out.println("Distance: " + distance);
					}
					try
					{
						Thread.sleep(100); // sleep for 1/10th of a second
					}
					catch (InterruptedException ex)
					{
						System.out.println("InterruptedException: " + ex.getMessage());
						destroyApp();
					}

					if (distance > 10) {
						PWMControl(i);
					} else {
						Stop();
					}
				}

				// Ramp down the speed, stop if there is an obstical.
				for (byte i = 100; i >= 0; i--) {
					if (readSR04Task != null) {
						distance = readSR04Task.getDistanceSR04();
						System.out.println("Distance: " + distance);
					}
					try
					{
						Thread.sleep(100); // sleep for 1/10th of a second
					}
					catch (InterruptedException ex)
					{
						System.out.println("InterruptedException: " + ex.getMessage());
						destroyApp();
					}
					
					if (distance > 10) {
						PWMControl(i);
					} else {
						Stop();
					}
				}
				// Finished!
				destroyApp();
            }
        }
    }

	private void Stop() {
		PWMControl(0);
	}

    public void destroyApp() {
        //Stop thread
        shouldRun = false;
        gpio.shutdown(); // When your program is finished, make sure to stop all GPIO activity/threads by shutting down the GPIO controller (this method will forcefully shutdown all GPIO monitoring threads and background scheduled tasks)
        LogThis(Level.INFO, "JonnyBot done", "Game", "Over");
    }

    private long Map(long x, long in_min, long in_max, long out_min, long out_max)
    {
      return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    private void LogThis(String message) {
        LogThis(Level.INFO, message, null, null);
    }

    private void LogThis(Level level, String message) {
        LogThis(level, message, null, null);
    }
    
    private void LogThis(Level level, String message, String shortMessage1, String shortMessage2) {
        Logger.getGlobal().log(level, message);
        System.out.println(message);
//        if (shortMessage1 != null) {
//            I2CLCDController.writeLineOne(shortMessage1);
//        } 
//        if (shortMessage2 != null) {
//            I2CLCDController.writeLineTwo(shortMessage2);
//        }
    }
}
