package com.murphy;

import com.murphy.Motor;

/**
 * Controls the robot's wheels by translating instructions into motor operations
 * 
 * @author (your name) 
 * @version (a version number or a date)
 * 
 * https://robotjava.wordpress.com/2016/09/02/pi4j-and-l298n-motor-controller/
 * 
 */
public class MotorController
{
    // instance variables - replace the example below with your own
    private Motor leftMotor;
    private Motor rightMotor;
    private static final int SPEED_DIFF = Motor.FASTEST - Motor.SLOWEST;

	/*
     * Constructor for objects of class MotorController
     */
    public MotorController()
    {
        leftMotor = new Motor(Motor.MotorType.LEFT);
        rightMotor = new Motor(Motor.MotorType.RIGHT);
    }
    
    /**
     * Moves the motors. Positive speedPct will move the robot forward, negative will move backwards
     */
    public void move(int speedPct, int turn)
    {
        if (speedPct > 0)
        {
            forward(speedPct, turn);
        }
        else if (speedPct < 0)
        {
            backward(-speedPct, turn);
        }
        else if (turn > 0)
        {
            turnRight(turn);
        }
        else if (turn < 0)
        {
            turnLeft(-turn);
        }
        else
        {
            brake();
        }
    }
    
    public void moveWheels(int leftPct, int rightPct)
    {
        //Left wheels
        int leftSpeed = getMotorSpeed(Math.abs(leftPct));
        if (leftPct > 0)
        {
            leftMotor.forward(leftSpeed);
        }
        else if (leftPct < 0)
        {
            leftMotor.backward(leftSpeed);
        }
        else
        {
            leftMotor.brake();
        }
        
        //Right wheels
        int rightSpeed = getMotorSpeed(Math.abs(rightPct));
        if (rightPct > 0)
        {
            rightMotor.forward(rightSpeed);
        }
        else if (rightPct < 0)
        {
            rightMotor.backward(rightSpeed);
        }
        else
        {
            rightMotor.brake();
        }
    }

    /**
     * Moves the robot forward with speed percentage and direction
     */
    public void forward(int speedPct, int turn)
    {
        leftMotor.forward(getLeftMotorSpeed(speedPct, turn));
        rightMotor.forward(getRightMotorSpeed(speedPct, turn));
    }
    
    public void backward(int speedPct, int turn)
    {
       leftMotor.backward(getLeftMotorSpeed(speedPct, -turn));
       rightMotor.backward(getRightMotorSpeed(speedPct, -turn));
    }
    
    public void turnRight(int speedPct)
    {
       int motorSpeed = getMotorSpeed(speedPct);
       leftMotor.forward(motorSpeed);
       rightMotor.backward(motorSpeed);
    }
    
    public void turnLeft(int speedPct)
    {
       int motorSpeed = getMotorSpeed(speedPct);
       leftMotor.backward(motorSpeed);
       rightMotor.forward(motorSpeed);
    }
    
    public void testMotors(int speed) {
        leftMotor.testMotor(speed);
        rightMotor.testMotor(speed);
    }
    
    public void moveLeftForward(int speedPct)
    {
       int motorSpeed = getMotorSpeed(speedPct);    // returns a number between 140 and 500, e.g. 360 for 50%
       leftMotor.forward(motorSpeed);
    }
    
    public void moveRightForward(int speedPct) 
    {
       int motorSpeed = getMotorSpeed(speedPct);
       rightMotor.forward(motorSpeed);
    }
    
    public void moveLeftBackward(int speedPct)
    {
       int motorSpeed = getMotorSpeed(speedPct);
       leftMotor.backward(motorSpeed);
    }
    
    public void moveRightBackward(int speedPct) 
    {
       int motorSpeed = getMotorSpeed(speedPct);
       rightMotor.backward(motorSpeed);
    }

    public void leftMotorBrake()
    {
        leftMotor.brake();
    }

    public void rightMotorBrake()
    {
        rightMotor.brake();
    }

    public void brake()
    {
       leftMotor.brake();
       rightMotor.brake();
    }
    
    // This should return a value between 0 and 1024 given a % between 0 and 100
    private int getMotorSpeed(int speedPct)
    {
        int adjustedSpeed = speedPct;
        if (speedPct > 100)
        {
            adjustedSpeed = 100;
        }
        else if (speedPct < 0)
        {
            adjustedSpeed = 0;
        }
        
        double speed = adjustedSpeed / 100.0;
        return (int)(speed * SPEED_DIFF + Motor.SLOWEST);
    }
    
    /**
     * Returns the right motor speed depending on the speedPct subtracted by
     * the turn ratio. If turn is greater than 0, that means the right motor
     * needs to slow down by that ratio
     */
    private int getRightMotorSpeed(int speedPct, int turn)
    {
        if (turn > 0)
        {
            if (turn > 100)
            {
                turn = 100;
            }
            double turnPct = turn / 100.0;
            int speedReduction = (int)(speedPct * turnPct);
            speedPct = speedPct - speedReduction;
        }
        return getMotorSpeed(speedPct);
    }
    
    /**
     * Returns the left motor speed depending on the speedPct subtracted by
     * the turn ratio. If turn is less than 0, that means the left motor
     * needs to slow down by the absolute number of that ratio
     */
    private int getLeftMotorSpeed(int speedPct, int turn)
    {
        if (turn < 0)
        {
            if (turn < -100)
            {
                turn = -100;
            }
            double turnPct = -turn / 100.0;
            int speedReduction = (int)(speedPct * turnPct);
            speedPct = speedPct - speedReduction;
        }
        return getMotorSpeed(speedPct);
    }
    
}
