import java.io.*;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ListDir {
	public static void main(String[] args) {
		ListDir listDir = new ListDir();
		listDir.manageNoteSet();
	}
	
	/**
	* This method just shows examples of how the Note and NoteSet can be used.
	*/
	private void manageNoteSet() {
		// Create and load the NoteSet given a directory loaded with text files
		NoteSet noteSet = new NoteSet("C:\\temp\\notes");

		// Print all note keys and note values
		noteSet.printNoteSet();

		// Get a list of all of the keys back in a String array and print them
		String[] allKeys = noteSet.getAllKeys();
		for (String x : allKeys) {
			System.out.println(x);
		}

		// Get the tenth Note in the 
		String aKey = allKeys[10];
		Note aNote = noteSet.getNote(aKey);

		// Print the contents of that Note.
		System.out.println(aNote.getContents());
		
		// Create a new Note and put it into the NoteSet
		Note myNote = new Note("C:\\temp\\notes\\myNewNote.txt");
		myNote.setContents("One smart fellow he felt smart; Two smart fellows, they felt smart; Three smart fellows they all felt smart!");
		noteSet.putNote(myNote);
		
	}
	
	private class NoteSet {
		
		private String directory;
		private HashMap allNotesMap;
		private File dir;
		private String[] children;
		
		NoteSet(String directory) {
			this.directory = directory;
			this.allNotesMap = new HashMap();
			this.dir = new File(directory);
			this.children = dir.list();
			buildNoteSet();
		}

		private void buildNoteSet() {

			if (children == null) {
				System.out.println( "Either dir does not exist or is not a directory");
				return;
			} else { 
				for (int i=0; i< children.length; i++) {
					String filename = children[i];
					String fullFileName = directory + "\\" + filename;
					Note note = new Note(fullFileName);
					allNotesMap.put(fullFileName,note);
				}
			}
		}

		public void putNote(Note note) {
			allNotesMap.put(note.getFileName(),note);
			note.save();
		}
		
		public Note getNote(String key) {
			Note aNote = (Note) allNotesMap.get(key); 
			return aNote;
		}

		public String[] getAllKeys() {
			String[] allKeys = new String[allNotesMap.size()];
			int k = 0;
			Set noteSet = allNotesMap.entrySet();
			Iterator i = noteSet.iterator();
			while (i.hasNext()) {
				Map.Entry me = (Map.Entry) i.next();
				String key = (String) me.getKey();
				allKeys[k++] = key;
			}
			return allKeys;
		}
		
		public void printNoteSet() {
			Set noteSet = allNotesMap.entrySet();
			Iterator i = noteSet.iterator();
			while (i.hasNext()) {
				Map.Entry me = (Map.Entry) i.next();
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				System.out.print(me.getKey() + ": ");
				System.out.println(me.getValue());
			}
		}

	}
	
	private class Note {

		private String fileName;
		private String contents;
		
		Note(String fileName) {
		   this.fileName = fileName;
		   LoadContents();
		}

		private void LoadContents() {
			try {
				this.contents = new String(Files.readAllBytes(Paths.get(this.fileName)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void save() {
			try {
				File f = new File(this.getFileName());
				PrintWriter out = new PrintWriter(this.getFileName());
				out.println(this.getContents());
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		
		public String getFileName() {
			return this.fileName;
		}
		
		public String toString() {
			return this.fileName + ":\n" + this.contents;
		}
		
		public String getContents() {
			return this.contents;
		}

		public void setContents(String contents) {
			this.contents = contents;
		}
	}
}