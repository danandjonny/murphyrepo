/*
 * Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */


#include "asm-generic/ioctls.h"
#include <linux/serial.h>
#include <errno.h>

#include "jni.h"

#include "serial.h"
#include "javacall_logging.h"
#include <dio_common.h>

#ifdef __cplusplus
extern "C" {
#endif


JNIEXPORT jboolean JNICALL Java_com_oracle_dio_uart_impl_UARTOptionsHandler_enableRS485
  (JNIEnv* env, jlong def_ref_handle, jboolean rts_on_send) {
    struct serial_rs485 rs485conf;
    device_reference dev_ref = (device_reference)def_ref_handle;
    if (def_ref_handle == -1) {
        JAVACALL_REPORT_ERROR(JC_DIO, "Can't enable RS485 interface. Device is closed");
        return JNI_FALSE;
    }
    int handle = ((serial_handle)getDeviceHandle(dev_ref))->fd;
    memset(&rs485conf, 0x0, sizeof(struct serial_rs485));
    /* Enable RS485 mode */
    rs485conf.flags |= SER_RS485_ENABLED;
    if (rts_on_send) {
        /* Set logical level for RTS pin equal to 1 when sending */
        rs485conf.flags |= SER_RS485_RTS_ON_SEND;
    }
    /* need some field test to detect whether this value is necessary */
    rs485conf.delay_rts_before_send = 0x00000001;
    if (ioctl (handle, TIOCSRS485, &rs485conf) < 0) {
        JAVACALL_REPORT_ERROR1(JC_DIO, "Can't enable RS485 interface. Err %d", errno);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

#ifdef __cplusplus
}
#endif

