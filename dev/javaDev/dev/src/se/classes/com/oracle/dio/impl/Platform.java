/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.dio.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Objects;

import com.oracle.dio.utils.ExceptionMessage;
import com.oracle.dio.utils.Logging;

public final class Platform {
    /** Don't let anyone instantiate this class */
    private Platform() {
    }

    /** Place any platform specific initialization here */
    public static void initialize() {
        // load the main library
        AccessController.doPrivileged(new PrivilegedAction<Void>() {
                @Override
                public Void run() {
                    System.loadLibrary("dio");
                    return null;
                }
            });
    }

    /**
     * Compare two objects by recursive inspection of their fields.
     *
     * @param obj1 first object to compare
     * @param obj2 second object to compare
     *
     * @return true if objects classes are the same and all objects
     *         members are equals.
     *
     * @note unimplemented
     */
    public static boolean equals(Object obj1, Object obj2) {
        if (null == obj1 || null == obj2) {
            return false;
        }

        try {
            return serialize(obj1).equals(serialize(obj2));
        } catch (IOException e) {
            return obj1 == obj2;
        }
    }

    /**
     * Creates a copy of object.
     *
     * @param obj object to be cloned.
     * @return a clone of object.
     */
    public static Object clone(Object obj)  {
        try {
            return deserialize(serialize(obj));
        } catch (IOException e) {
            Logging.reportWarning(ExceptionMessage.format(ExceptionMessage.CLONE_ERROR));
            return obj;
        }
    }

    /**
     * Convert the given byte array into object form.
     *
     * @note The object must be annotated with @SerializeMe tag
     * @note So far ByteArrayInputStream is only useful
     *       InputStream implementation
     *
     * @param in the serialized form of the objects to restore
     *
     * @return the root object
     */
    public static Object deserialize(InputStream in) throws IOException {
        Objects.requireNonNull(in, "Invalid InputStream");
        throw new IOException("Platform.deserialize() is not implemented");
    }



    /**
     *  Converts the given object and everything is refers to
     *       into serialized form.
     * @note The object must be annotated with @SerializeMe tag
     * @param obj root the root object to start serialization from
     * @param os output stream to store serialized data
     *
     * @return a size of serialized data
     */
    public static int serialize(Object obj, OutputStream os) throws IOException {
        Objects.requireNonNull(os, "Invalid OutputStream");
        throw new IOException("Platform.serialize() is not implemented");
    }

    /**
     * Returns the hash code value for given object.
     *
     * @param obj       the target for hash calculation
     * @param initVal hash code initial value
     * @param factor  hash factor
     * @return a hash code value for this object.
     */
    public static int hash(final Object obj, final int initVal, final int factor) {
        try {
            return serialize(obj).hashCode();
        } catch (IOException e) {
            return System.identityHashCode(obj);
        }
    }


    // =========================
    private static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serialize(obj, baos);
        baos.close();
        return baos.toByteArray();
    }

    private static Object deserialize(byte[] ba) throws IOException {
        return deserialize(new ByteArrayInputStream(ba));
    }


}
