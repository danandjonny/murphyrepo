/*
 * Copyright (c) 2012, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.oracle.dio.impl;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;

import jdk.dio.Device;

/**
 * Event queue implementation.
 */
public class EventQueue {
    private final ByteBuffer        buffer;
    private final LinkedList<Event> queue = new LinkedList<>();
    private final ArrayList<Object> listeners = new ArrayList<>();

    private static final int        SHARED_QUEUE_BUFFER_SIZE = 4096;

    private Thread                  nativeEventThread;
    private Thread                  eventDispatchThread;

    public Thread getEventDispatchThread(){
        return eventDispatchThread;
    }

    private static class QueueInstanceHolder {
        private static EventQueue sharedEventQueue = new EventQueue(SHARED_QUEUE_BUFFER_SIZE);
    }

    private EventQueue(int bufferSize) {
        buffer = ByteBuffer.allocateDirect(bufferSize);
        buffer.position(0).limit(0);
        startQueue();
        setNativeEntries(buffer);
    }

    /**
     * This methods returns the reference to the shared event queue. Actual queue
     * is lazily created and started upon first call of this method.
     * @return event queue
     */
    public static EventQueue getSharedEventQueue() {
        return QueueInstanceHolder.sharedEventQueue;
    }

    private class NativeMethodThread implements Runnable {
        @Override
        public void run() {
            synchronized (buffer) {
                try {
                    while (true) {
                        while (buffer.hasRemaining()) {
                            Class<? extends Device> eventType = getEventType(buffer);
                            byte[] payload = null;
                            short len = buffer.getShort();
                            if (len > 0) {
                                payload = new byte[len];
                                buffer.get(payload);
                            }
                            Event e = new Event(eventType, payload);
                            postEvent(e);
                        }
                        buffer.position(0).limit(0);
                        buffer.wait();
                    }
                } catch (InterruptedException ex) {
                    // do nothing on interrupt
                }
            }
        }
    }

    private class EventDispatchThread implements Runnable {
        @Override
        public void run() {
            Event evt = null;
            while (true) {
                synchronized (queue) {
                    try {
                        if (queue.isEmpty()) {
                            queue.wait();
                        }
                        evt = queue.poll();
                    } catch (InterruptedException ex) {
                        // do something
                    }
                }//synchronized queue

                if (evt != null) {
                    dispatch(evt);
                }
            }//while true
        }
    }


    /**
     * This method posts event to be asynchronously dispatched. Can be used by
     * Java components.
     * @param evt event
     */
    public void postEvent(Event evt) {
        if (evt == null) {
            throw new IllegalArgumentException();
        }

        synchronized (queue) {
            queue.add(evt);
            queue.notify();
        }
    }

    /**
     * Registers listener for event with specified class.
     * @param <T> event class
     * @param evtClass class object of event class
     * @param handler listener
     */
    public <T extends Device> void registerForEvent(Class<T> evtClass, EventHandler handler) {
        if (evtClass == null || handler == null) {
            throw new IllegalArgumentException();
        }

        synchronized (listeners) {
            listeners.add(evtClass);
            listeners.add(handler);
        }
    }

    private void dispatch(final Event evt) {
        synchronized (listeners) {
            for (int i = 0; i < listeners.size(); i += 2) {
                if (listeners.get(i).equals(evt.getType())) {
                    EventHandler h = (EventHandler)listeners.get(i+1);
                    h.handleEvent(evt);
                }
            }
        }
    }

    private void startQueue() {
        nativeEventThread = new Thread(new NativeMethodThread());
        nativeEventThread.setDaemon(true);
        nativeEventThread.start();
        eventDispatchThread = new Thread(new EventDispatchThread());
        eventDispatchThread.setDaemon(true);
        eventDispatchThread.start();
    }

    private Class<? extends Device> getEventType(ByteBuffer buffer) {
        char c;
        StringBuilder sb = new StringBuilder();
        while (0 != (c = (char)buffer.get())) {
            sb.append(c);
        }
        try {
            return (Class<? extends Device>)Class.forName(sb.toString());
        } catch (ClassNotFoundException cnfe) {
        }

        return Device.class;
    }

    private static native void setNativeEntries(ByteBuffer buffer);
}
