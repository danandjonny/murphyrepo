/*
 * Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.oracle.dio.uart.impl;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import com.oracle.dio.utils.Logging;

/**
 * This class is workaround for enabling rs485 mode at some
 * serial port drivers.
 *
 */
class UARTOptionsHandler {

    private static final String RS485_ENABLE = "jdk.dio.uart.rs485.enable";
    private static final String RS485_RTS_ON_SEND = "jdk.dio.uart.rs485.rts_on_send";

    static void processOptions(UARTImpl device, String[] options) {
        if (null == options) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (String option: options) {
            sb.append(option.trim()).append('\n');
        }
        try {
            StringReader sr = new StringReader(sb.toString());
            Properties props = new Properties();
            props.load(sr);

            if ("true".equals(props.get(RS485_ENABLE))) {
                final boolean rts_on_send = "true".equals(props.get(RS485_RTS_ON_SEND));
                enableRS485(device.getHandle().getNativeHandle(), rts_on_send);
            }
        } catch (IOException e) {
            Logging.reportError("Can't parse UART options");
        }
    }

    private static native boolean enableRS485(long handle, boolean rts_on_send);
}
