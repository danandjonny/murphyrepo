/*
 * Copyright (c) 2014, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.oracle.dio.impl;
import jdk.dio.Device;

/**
 * Base class for all events.
 */
public class Event {
    private byte[] payload;
    private Class<? extends Device> eventType;
    /**
     * Creates event object with payload.
     * @param payload event payload
     * @param clazz  device class that describes event type
     */
    public Event(Class<? extends Device> clazz, byte[] payload) {
        this.payload = payload;
        eventType = clazz;
    }

    /**
     * Returns event payload. Interpretation of the byte array is left to
     * component implementation.
     * @return event payload
     */
    public byte[] getPayload() {
        return payload;
    }

    /** Returns type of this event. */
    public Class<? extends Device> getType() {
        return eventType;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("Event from ");
        b.append(eventType.toString());

        if (payload != null) {
            b.append(" with payload length = ").append(payload.length).append(" bytes =");
            for (int i = 0; i < payload.length; i++) {
                b.append(" ").append(payload[i]);
            }
        } else {
            b.append(" with empty payload");
        }

        return b.toString();
    }
}
