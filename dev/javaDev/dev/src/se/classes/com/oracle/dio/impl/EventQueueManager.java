/*
 * Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */


package com.oracle.dio.impl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Hashtable;
import java.util.Vector;

import com.oracle.dio.impl.AbstractPeripheral;

import jdk.dio.Device;
import jdk.dio.DeviceEvent;

public final class EventQueueManager implements EventHandler {

    private final Vector<Integer> registeredTypes = new Vector<>();
    private final Hashtable<Integer, AbstractPeripheral> listenerRegistry = new Hashtable();

    private static final EventQueueManager instance = new EventQueueManager();

    private final EventQueue queue = EventQueue.getSharedEventQueue();

    public static EventQueueManager getInstance() {
        return instance;
    }

    private EventQueueManager() {
    }

    private int getHash(int deviceType, int eventType, long nativeHandle) {
        return (int)(deviceType * (deviceType + deviceType * eventType) + nativeHandle);
    }

    public void setEventListener(Class<? extends Device> clazz, int eventSubType, AbstractPeripheral listener) {
        final int eventType = clazz.hashCode();
        final long nativeHandle = listener.getHandle().getNativeHandle();
        if (!registeredTypes.contains(eventType)) {
            // prevent overriding that leads to message losses
            queue.registerForEvent(clazz, this);
            registeredTypes.add(eventType);
        }

        listenerRegistry.put(getHash(eventType, eventSubType, nativeHandle), listener);
    }

    public void removeEventListener(Class<? extends Device> clazz, int eventSubType, AbstractPeripheral listener) {
        final int eventType = clazz.hashCode();
        final long nativeHandle = listener.getHandle().getNativeHandle();
        listenerRegistry.remove(getHash(eventType, eventSubType, nativeHandle));
    }

    public void postEvent(AbstractPeripheral receiver, int subEventType, DeviceEvent event) {
        final Tuple tuple = new Tuple(receiver.getDescriptor().getInterface(), subEventType, receiver, event);
        queue.postEvent(tuple);
    }


    /**
     * This method is called by EventQueue.dispatch(). Each call is made on a
     * separate thread.
     * @param event a previously queued event to handle
     */
    public boolean handleEvent(Event event) {
        if (event instanceof Tuple) {
            Tuple tuple = (Tuple)event;
            tuple.receiver.processDeviceEvent(tuple.eventType, tuple.event);
        } else {
            ByteBuffer payload = ByteBuffer.wrap(event.getPayload());
            // all data are stored in big endian format
            long handle = payload.getLong();
            int subEvent  = payload.getInt();
            int data   = payload.getInt();
            AbstractPeripheral handler = listenerRegistry.get(getHash(event.getType().hashCode(), subEvent, handle));
            if (handler != null) {
                handler.processNativeEvent(subEvent, data);
            }
        }
        return true;
    }

    public boolean isDispatchThread() {
        return queue.getEventDispatchThread().equals(Thread.currentThread());
    }

    /* simple holder for custom data */
    private final class Tuple extends Event {
        private final DeviceEvent event;
        private final int eventType;
        private final AbstractPeripheral receiver;
        private Tuple(Class<? extends Device> type, int eventType, AbstractPeripheral receiver, DeviceEvent event) {
            super(type, null);
            this.eventType = eventType;
            this.receiver = receiver;
            this.event = event;
        }
    }
}
