/*
 * Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved.
 */

package romizer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface Local {
    String[] DontRemoveClasses() default {};
    String[] DontRenameClasses() default {};
    String[] DontRemoveFields() default {};
    String[] DontRenameFields() default {};
    String[] DontRenameMethods() default {};
    String[] WeakDontRenameClasses() default {};
    String[] DontRenameSubtypes() default {};
    String[] WeakDontRenameSubtypes() default {};
    String[] DontRenameNonAbstractSubtypes() default {};
    String[] DontRenameFieldsClasses() default {};
}
