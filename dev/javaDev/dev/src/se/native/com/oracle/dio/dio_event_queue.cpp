/*
 * Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

#include "dio_event_queue.h"
#include <string.h>
#include "javacall_logging.h"

/* Entities required for sending notifications */
static jobject eventBuffer = NULL;

extern "C" {

JNIEXPORT void JNICALL Java_com_oracle_dio_impl_EventQueue_setNativeEntries
  (JNIEnv* env, jobject this_obj, jobject buffer) {
    if (eventBuffer) {
        env->DeleteGlobalRef(eventBuffer);
    }
    eventBuffer = env->NewGlobalRef(buffer);
}

} /* extern "C */

/*
 * Puts an event to the provided NIO buffer and sends a notification to the
 * EventQueue object that handles the buffer. See the implementation
 * of the com.oracle.dio.impl.EventQueue class for details.
 */
void event_queue_put_native_event
  (JavaVM* cachedJVM, const char* device_type, const char *payload, int payload_size) {
    JNIEnv* env;
    if (NULL == eventBuffer) {
        JAVACALL_REPORT_ERROR(JC_DIO, "Event queue is not initialized properly");
        return;
    }
    cachedJVM->AttachCurrentThread((void **)&env, NULL);

    jclass bufferClass = env->GetObjectClass(eventBuffer);
    jmethodID notifyID = bufferClass ? env->GetMethodID(bufferClass, "notify", "()V") :
                                       NULL;
    jmethodID limitID = notifyID ? env->GetMethodID(bufferClass, "limit", "()I") :
                                  NULL;
    jmethodID setLimitID = limitID ? env->GetMethodID(bufferClass, "limit", "(I)Ljava/nio/Buffer;") :
                                     NULL;

    const int device_type_len = strlen(device_type) + 1;
    if (setLimitID) {
        env->MonitorEnter(eventBuffer);

        if (env->ExceptionCheck() != JNI_TRUE) {
            // check enough space in direct buffer
            jlong capacity = env->GetDirectBufferCapacity(eventBuffer);
            jint limit = env->CallIntMethod(eventBuffer, limitID);

            jint newLimit = limit + device_type_len + payload_size + 2;

            if (newLimit < capacity) {
                jbyte* buf = (jbyte*)env->GetDirectBufferAddress(eventBuffer);

                buf += limit;

                memcpy(buf, device_type, device_type_len);
                buf += device_type_len;

                // payload
                *buf++ = (jbyte)((payload_size & 0xFF00) >> 8); // high byte
                *buf++ = (jbyte)(payload_size & 0xFF);          // then low byte
                memcpy(buf, payload, payload_size);

                env->CallObjectMethod(eventBuffer, setLimitID, newLimit);
            }
            env->CallVoidMethod(eventBuffer, notifyID);
        }
        env->MonitorExit(eventBuffer);
    }
    cachedJVM->DetachCurrentThread();
}

static char* serialize_data(char* buffer, size_t buffer_size, char* data, size_t data_size) {
  if (data_size > buffer_size) data_size = buffer_size;

  if (!data_size) return buffer;

  char* to = buffer;
  char* from  = data + (data_size - 1);

  while (data_size-- > 0) {
    *to = *from;
    to++;
    from--;
  }

  return to;
}

char* serialize_pointer(char* buffer, size_t buffer_size, jlong ptr) {
  return serialize_data(buffer, buffer_size, (char*)&ptr, sizeof(jlong));
}

char* serialize_int(char* buffer, size_t buffer_size, jint data) {
  return serialize_data(buffer, buffer_size, (char*)&data, sizeof(jint));
}
