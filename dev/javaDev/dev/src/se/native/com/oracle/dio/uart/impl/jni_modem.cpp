/*
 * Copyright (c) 2013, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

#include <dio_common.h>
#include <dio_exceptions.h>
#include <dio_nio.h>
#include <javacall_serial.h>
#include <javacall_uart.h>
#include <javacall_memory.h>
#include <dio_event_queue.h>

/*
 * Class:     com_oracle_dio_uart_impl_ModemUARTImpl
 * Method:    getDCESignalState0
 */
JNIEXPORT jboolean JNICALL Java_com_oracle_dio_uart_impl_ModemUARTImpl_getDCESignalState0
  (JNIEnv* env, jobject obj, jint signal) {
    device_reference device = getDeviceReferenceFromDeviceObject(env, obj);
    javacall_bool state = JAVACALL_FALSE;
    javacall_result result;
    result = javacall_serial_get_dce_signal(getDeviceHandle(device),
                                            (javacall_serial_signal_type)signal,
                                            &state);
    return (state != JAVACALL_FALSE ? JNI_TRUE : JNI_FALSE);
}

/*
 * Class:     com_oracle_dio_uart_impl_ModemUARTImpl
 * Method:    setDTESignalState0
 */
JNIEXPORT void JNICALL Java_com_oracle_dio_uart_impl_ModemUARTImpl_setDTESignalState0
  (JNIEnv* env, jobject obj, jint signal, jboolean state) {
    device_reference device = getDeviceReferenceFromDeviceObject(env, obj);
    javacall_result result;
    result = javacall_serial_set_dte_signal(getDeviceHandle(device),
                                            (javacall_serial_signal_type)signal,
                                            (state != JNI_FALSE ? JAVACALL_TRUE : JAVACALL_FALSE));
}
