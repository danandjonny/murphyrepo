/*
 * Copyright (c) 2012, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

#include "list_helper.h"
#include "javacall_logging.h"

#include <errno.h>

javacall_result lock_m(pthread_mutex_t *pM){
    if(pthread_mutex_lock(pM)!=0) {
        JAVACALL_REPORT_ERROR2(JC_DIO, "lock mutex errno=%d: %s\n", errno, strerror(errno));
        return JAVACALL_FAIL;
    }
    return JAVACALL_OK;
}

javacall_result unlock_m(pthread_mutex_t *pM){
    if(pthread_mutex_unlock(pM)!=0){
        JAVACALL_REPORT_ERROR2(JC_DIO, "unlock mutex errno=%d: %s\n", errno, strerror(errno));
        return JAVACALL_FAIL;
    }
    return JAVACALL_OK;
}

javacall_result add_dev_to_list(javacall_handle* plistHandle, pthread_mutex_t *mutex, javacall_handle dev){
    javacall_result res = JAVACALL_OK;
    javacall_int32 size;

    if(lock_m(mutex) == JAVACALL_FAIL){
        return JAVACALL_FAIL;
    }

    if(*plistHandle == NULL){
        if(JAVACALL_OK != (res = javautil_list_create(plistHandle))){
            JAVACALL_REPORT_ERROR(JC_DIO, "cannot create device list");
        }
    }
    if(res == JAVACALL_OK){
        res = javautil_list_add(*plistHandle, dev);
    }
    unlock_m(mutex);

    return res;
}

javacall_result remove_dev_from_list(javacall_handle listHandle, pthread_mutex_t *mutex, javacall_handle dev){
    javacall_result res;

    if(lock_m(mutex) == JAVACALL_FAIL){
        return JAVACALL_FAIL;
    }
    res = javautil_list_remove(listHandle, dev);
    unlock_m(mutex);

    return res;
}
