/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.uart;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.oracle.dio.impl.Platform;
import com.oracle.dio.utils.ExceptionMessage;
import com.oracle.dio.utils.Logging;

import jdk.dio.DeviceConfig;
import jdk.dio.DeviceManager;
import jdk.dio.InvalidDeviceConfigException;

import romizer.*;

import serializator.*;

/**
 * The {@code UARTConfig} class encapsulates the hardware addressing information, and static and dynamic configuration
 * parameters of a UART.
 * <p>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} or {@code null} (see
 * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
 * </p><p>
 * An instance of {@code UARTConfig} can be passed to the {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} to open the designated UART with the specified
 * configuration. A {@link InvalidDeviceConfigException} is thrown when attempting to open a device with
 * an invalid or unsupported configuration.
 * </p>
 *
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(DeviceConfig, int)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig, int)
 * @since 1.0
 */
@SerializeMe
@apimarker.API("device-io_1.1_uart")
public final class UARTConfig implements DeviceConfig<UART>, DeviceConfig.HardwareAddressing {

    /**
     * 5 data bit format.
     */
    public static final int DATABITS_5 = 5;
    /**
     * 6 data bit format.
     */
    public static final int DATABITS_6 = 6;
    /**
     * 7 data bit format.
     */
    public static final int DATABITS_7 = 7;
    /**
     * 8 data bit format.
     */
    public static final int DATABITS_8 = 8;
    /**
     * 9 data bit format.
     */
    public static final int DATABITS_9 = 9;
    /**
     * Flow control off.
     */
    public static final int FLOWCONTROL_NONE = 0;
    /**
     * RTS/CTS (hardware) flow control on input.
     * <p>
     * This bit flag can be bitwise-combined (OR) with other output flow control bit flags.
     * </p>
     */
    public static final int FLOWCONTROL_RTSCTS_IN = 1;
    /**
     * RTS/CTS (hardware) flow control on output.
     * <p>
     * This bit flag can be bitwise-combined (OR) with other output flow control bit flags.
     * </p>
     */
    public static final int FLOWCONTROL_RTSCTS_OUT = 2;
    /**
     * XON/XOFF (software) flow control on input.
     * <p>
     * This bit flag can be bitwise-combined (OR) with other input flow control bit flags.
     * The actual XON and XOFF characters used for software flow control are device hardware and driver-dependent.
     * </p>
     */
    public static final int FLOWCONTROL_XONXOFF_IN = 4;
    /**
     * XON/XOFF (software) flow control on output.
     * <p>
     * This bit flag can be bitwise-combined (OR) with other input flow control bit flags.
     * The actual XON and XOFF characters used for software flow control are device hardware and driver-dependent.
     * </p>
     */
    public static final int FLOWCONTROL_XONXOFF_OUT = 8;
    /**
     * EVEN parity scheme.
     */
    public static final int PARITY_EVEN = 2;
    /**
     * MARK parity scheme.
     */
    public static final int PARITY_MARK = 3;
    /**
     * No parity bit.
     */
    public static final int PARITY_NONE = 0;
    /**
     * ODD parity scheme.
     */
    public static final int PARITY_ODD = 1;
    /**
     * SPACE parity scheme.
     */
    public static final int PARITY_SPACE = 4;
    /**
     * Number of STOP bits - 1.
     */
    public static final int STOPBITS_1 = 1;
    /**
     * Number of STOP bits - 1-1/2.
     */
    public static final int STOPBITS_1_5 = 2;
    /**
     * Number of STOP bits - 2.
     */
    public static final int STOPBITS_2 = 3;

    private String controllerName;
    private int channelNumber = UNASSIGNED;
    private int controllerNumber = UNASSIGNED;
    private int baudRate = 9600;
    private int dataBits = DATABITS_8;
    private int flowcontrol; // none
    private int inputBufferSize = UNASSIGNED;
    private int outputBufferSize = UNASSIGNED;
    private int parity; //none
    private int stopBits = STOPBITS_1;

    /**
     * The {@code Builder} class allows for creating and initializing
     * {@code UARTConfig} objects. Calls can be chained in the following manner:
     * <blockquote>
     * <pre>
     *   UARTConfig config = new UARTConfig.Builder()
     *           .setControllerNumber(1)
     *           .setChannelNumber(1)
     *           .setBaudRate(56000)
     *           .setDataBits(DATABITS_9)
     *           .setStopBits(STOPBITS_2)
     *           .setParity(PARITY_EVEN)
     *           .setFlowControlMode(PARITY_EVEN)
     *           .setInputBufferSize(0)
     *           .setOutputBufferSize(0)
     *           .build();
     * </pre>
     * </blockquote>
     *
     * @since 1.1
     */
    @apimarker.API("device-io_1.1_uart")
    public static final class Builder {

        private final UARTConfig instance = new UARTConfig();

        public Builder() {

        }


        /**
         * Creates a new {@code UARTConfig} instance initialized with the
         * values set for each configuration parameters. If a configuration
         * parameter was not explictly set its default value will be used.
         *
         * @return a new initialized {@code UARTConfig} instance.
         */
        public UARTConfig build() {
            return (UARTConfig)Platform.clone(instance);
        }

        /**
         * Sets the controller name (default value is {@code null} if not set).
         *
         * @param controllerName the controller name (such as its <em>device
         * file</em> name on UNIX systems) or {@code null}.
         * @return this {@code Builder} instance.
         */
        public Builder setControllerName(String controllerName) {
            instance.controllerName = controllerName;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the channel number (default value is {@code UNASSIGNED} if not
         * set).
         *
         * @param channelNumber the channel number (a positive or zero integer)
         * or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code channelNumber} is not in
         * the defined range.
         */
        public Builder setChannelNumber(int channelNumber) {
            instance.channelNumber = channelNumber;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the controller number (default value is {@code UNASSIGNED} if
         * not set).
         *
         * @param controllerNumber the hardware converter's number (a positive
         * or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code controllerNumber} is not
         * in the defined range.
         */
        public Builder setControllerNumber(int controllerNumber) {
            instance.controllerNumber = controllerNumber;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the speed in Bauds (default value is {@code 9600} if not set).
         *
         * @param baudRate the speed in Bauds (a positive integer).
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code baudRate} is not in the
         * defined range.
         */
        public Builder setBaudRate(int baudRate) {
            instance.baudRate = baudRate;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the number of bits per character (default value is
         * {@code DATABITS_8} if not set).
         *
         * @param dataBits the number of bits per character, one of: {@link #DATABITS_5}, {@link #DATABITS_6},
         *            {@link #DATABITS_7}, {@link #DATABITS_8} or {@link #DATABITS_9}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code dataBits} is not in the
         * defined range.
         */
        public Builder setDataBits(int dataBits) {
            instance.dataBits = dataBits;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the flow control mode (default value is {@code FLOWCONTROL_NONE}
         * if not set).
         *
         * @param flowcontrol the flow control mode: {@link #FLOWCONTROL_NONE}
         * if flow control is disabled; or a bit-wise OR combination of
         * {@link #FLOWCONTROL_RTSCTS_IN}, {@link #FLOWCONTROL_RTSCTS_OUT}, {@link #FLOWCONTROL_XONXOFF_IN}
         * or {@link #FLOWCONTROL_XONXOFF_OUT}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code flowcontrol} is not in the defined range
         * or if more than one input or more than one output flow control mode is specified.
         */
        public Builder setFlowControlMode(int flowcontrol) {
            instance.flowcontrol = flowcontrol;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the requested input buffer size (default value is
         * {@code UNASSIGNED} if not set).
         *
         * @param inputBufferSize the requested input buffer size in number of
         * samples (a positive or zero integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code inputBufferSize} is not in
         * the defined range.
         */
        public Builder setInputBufferSize(int inputBufferSize) {
            instance.inputBufferSize = inputBufferSize;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the requested output buffer size (default value is
         * {@code UNASSIGNED} if not set).
         *
         * @param outputBufferSize the requested output buffer size in number of
         * samples (a positive or zero integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code outputBufferSize} is not
         * in the defined range.
         */
        public Builder setOutputBufferSize(int outputBufferSize) {
            instance.outputBufferSize = outputBufferSize;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the parity (default value is {@code PARITY_NONE} if not set).
         *
         * @param parity the parity, one of: {@link #PARITY_ODD}, {@link #PARITY_EVEN}, {@link #PARITY_MARK},
         *            {@link #PARITY_SPACE}, or {@link #PARITY_NONE}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code parity} is not in the
         * defined range.
         */
        public Builder setParity(int parity) {
            instance.parity = parity;
            instance.checkParameters();
            return this;
        }

        /**
         * Sets the number of stop bits per character (default value is
         * {@code STOPBITS_1} if not set).
         *
         * @param stopBits the number of stop bits per character, on of:
         * {@link #STOPBITS_1}, {@link #STOPBITS_1_5}, or {@link #STOPBITS_2}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code stopBits} is not in the
         * defined range.
         */
        public Builder setStopBits(int stopBits) {
            instance.stopBits = stopBits;
            instance.checkParameters();
            return this;
        }
    }

    // hidden constructor for serializer
    @DontRenameMethod
    UARTConfig() {}

    /**
     * Creates a new {@code UARTConfig} with the specified hardware addressing information and configuration parameters.
     * <p>
     * The controller name is set to {@code null}.
     * The requested input and output buffer sizes are set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerNumber
     *            the hardware UART controller's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber
     *            the hardware UART channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param baudRate
     *            the speed in Bauds (a positive integer).
     * @param dataBits
     *            the number of bits per character, one of: {@link #DATABITS_5}, {@link #DATABITS_6},
     *            {@link #DATABITS_7}, {@link #DATABITS_8} or {@link #DATABITS_9}.
     * @param parity
     *            the parity, one of: {@link #PARITY_ODD}, {@link #PARITY_EVEN}, {@link #PARITY_MARK},
     *            {@link #PARITY_SPACE}, or {@link #PARITY_NONE}.
     * @param stopBits
     *            the number of stop bits per character, on of: {@link #STOPBITS_1}, {@link #STOPBITS_1_5}, or
     *            {@link #STOPBITS_2}.
     * @param flowcontrol
     *            the flow control mode: {@link #FLOWCONTROL_NONE} if flow control is disabled; or a bit-wise OR combination of
     *            {@link #FLOWCONTROL_RTSCTS_IN}, {@link #FLOWCONTROL_RTSCTS_OUT}, {@link #FLOWCONTROL_XONXOFF_IN} or
     *            {@link #FLOWCONTROL_XONXOFF_OUT}.
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code controllerNumber} is not in the defined range;</li>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code baudRate} is not in the defined range;</li>
     *             <li>{@code dataBits} is not in the defined range;</li>
     *             <li>{@code parity} is not in the defined range;</li>
     *             <li>{@code stopBits} is not in the defined range;</li>
     *             <li>{@code flowcontrol} is not in the defined range or if more than one input or more than one output flow control mode is specified.</li>
     *             </ul>
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public UARTConfig(int controllerNumber, int channelNumber, int baudRate, int dataBits, int parity, int stopBits, int flowcontrol) {
        this(controllerNumber, channelNumber, baudRate, dataBits, parity, stopBits, flowcontrol, DEFAULT, DEFAULT);
    }

    /**
     * Creates a new {@code UARTConfig} with the specified hardware addressing information and configuration parameters.
     * The platform/underlying driver may or may not allocate the requested sizes for the input and output buffers.
     * <p>
     * The controller name is set to {@code null}.
     * The requested input and output buffer sizes are set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerNumber
     *            the hardware UART controller's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber
     *            the hardware UART channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param baudRate
     *            the speed in Bauds (a positive integer).
     * @param dataBits
     *            the number of bits per character, one of: {@link #DATABITS_5}, {@link #DATABITS_6},
     *            {@link #DATABITS_7}, {@link #DATABITS_8} or {@link #DATABITS_9}.
     * @param parity
     *            the parity, one of: {@link #PARITY_ODD}, {@link #PARITY_EVEN}, {@link #PARITY_MARK},
     *            {@link #PARITY_SPACE}, or {@link #PARITY_NONE}.
     * @param stopBits
     *            the number of stop bits per character, on of: {@link #STOPBITS_1}, {@link #STOPBITS_1_5}, or
     *            {@link #STOPBITS_2}.
     * @param flowcontrol
     *            the flow control mode: {@link #FLOWCONTROL_NONE} if flow control is disabled; or a bit-wise OR combination of
     *            {@link #FLOWCONTROL_RTSCTS_IN}, {@link #FLOWCONTROL_RTSCTS_OUT}, {@link #FLOWCONTROL_XONXOFF_IN} or
     *            {@link #FLOWCONTROL_XONXOFF_OUT}.
     * @param inputBufferSize
     *            the input buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED} - (advisory only).
     * @param outputBufferSize
     *            the output buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED} - (advisory only).
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code controllerNumber} is not in the defined range;</li>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code baudRate} is not in the defined range;</li>
     *             <li>{@code dataBits} is not in the defined range;</li>
     *             <li>{@code parity} is not in the defined range;</li>
     *             <li>{@code stopBits} is not in the defined range;</li>
     *             <li>{@code flowcontrol} is not in the defined range or if more than one input or more than one output flow control mode is specified.</li>
     *             <li>{@code inputBufferSize} is not in the defined range;</li>
     *             <li>{@code outputBufferSize} is not in the defined range.</li>
     *             </ul>
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public UARTConfig(int controllerNumber, int channelNumber, int baudRate, int dataBits, int parity, int stopBits, int flowcontrol,
            int inputBufferSize, int outputBufferSize) {
        this.controllerNumber = controllerNumber;
        this.channelNumber = channelNumber;
        this.baudRate = baudRate;
        this.dataBits = dataBits;
        this.stopBits = stopBits;
        this.parity = parity;
        this.flowcontrol = flowcontrol;
        this.inputBufferSize = inputBufferSize;
        this.outputBufferSize = outputBufferSize;
        checkParameters();
    }

    /**
     * Creates a new {@code UARTConfig} with the specified hardware addressing information and configuration parameters.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * The requested input and output buffer sizes are set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param channelNumber
     *            the hardware UART channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param baudRate
     *            the speed in Bauds (a positive integer).
     * @param dataBits
     *            the number of bits per character, one of: {@link #DATABITS_5}, {@link #DATABITS_6},
     *            {@link #DATABITS_7}, {@link #DATABITS_8} or {@link #DATABITS_9}.
     * @param parity
     *            the parity, one of: {@link #PARITY_ODD}, {@link #PARITY_EVEN}, {@link #PARITY_MARK},
     *            {@link #PARITY_SPACE}, or {@link #PARITY_NONE}.
     * @param stopBits
     *            the number of stop bits per character, on of: {@link #STOPBITS_1}, {@link #STOPBITS_1_5}, or
     *            {@link #STOPBITS_2}.
     * @param flowcontrol
     *            the flow control mode: {@link #FLOWCONTROL_NONE} if flow control is disabled; or a bit-wise OR combination of
     *            {@link #FLOWCONTROL_RTSCTS_IN}, {@link #FLOWCONTROL_RTSCTS_OUT}, {@link #FLOWCONTROL_XONXOFF_IN} or
     *            {@link #FLOWCONTROL_XONXOFF_OUT}.
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code baudRate} is not in the defined range;</li>
     *             <li>{@code dataBits} is not in the defined range;</li>
     *             <li>{@code parity} is not in the defined range;</li>
     *             <li>{@code stopBits} is not in the defined range;</li>
     *             <li>{@code flowcontrol} is not in the defined range or if more than one input or more than one output flow control mode is specified.</li>
     *             </ul>
     * @throws NullPointerException
     *             if {@code controllerName} is {@code null}.
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public UARTConfig(String controllerName, int channelNumber, int baudRate, int dataBits, int parity, int stopBits, int flowcontrol) {
        this(controllerName, channelNumber, baudRate, dataBits, parity, stopBits, flowcontrol, DEFAULT, DEFAULT);
    }

    /**
     * Creates a new {@code UARTConfig} with the specified hardware addressing information and configuration parameters.
     * The platform/underlying driver may or may not allocate the requested sizes for the input and output buffers.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * The requested input and output buffer sizes are set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param channelNumber
     *            the hardware UART channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param baudRate
     *            the speed in Bauds (a positive integer).
     * @param dataBits
     *            the number of bits per character, one of: {@link #DATABITS_5}, {@link #DATABITS_6},
     *            {@link #DATABITS_7}, {@link #DATABITS_8} or {@link #DATABITS_9}.
     * @param parity
     *            the parity, one of: {@link #PARITY_ODD}, {@link #PARITY_EVEN}, {@link #PARITY_MARK},
     *            {@link #PARITY_SPACE}, or {@link #PARITY_NONE}.
     * @param stopBits
     *            the number of stop bits per character, on of: {@link #STOPBITS_1}, {@link #STOPBITS_1_5}, or
     *            {@link #STOPBITS_2}.
     * @param flowcontrol
     *            the flow control mode: {@link #FLOWCONTROL_NONE} if flow control is disabled; or a bit-wise OR combination of
     *            {@link #FLOWCONTROL_RTSCTS_IN}, {@link #FLOWCONTROL_RTSCTS_OUT}, {@link #FLOWCONTROL_XONXOFF_IN} or
     *            {@link #FLOWCONTROL_XONXOFF_OUT}.
     * @param inputBufferSize
     *            the input buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED} - (advisory only).
     * @param outputBufferSize
     *            the output buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED} - (advisory only).
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code baudRate} is not in the defined range;</li>
     *             <li>{@code dataBits} is not in the defined range;</li>
     *             <li>{@code parity} is not in the defined range;</li>
     *             <li>{@code stopBits} is not in the defined range;</li>
     *             <li>{@code flowcontrol} is not in the defined range or if more than one input or more than one output flow control mode is specified.</li>
     *             <li>{@code inputBufferSize} is not in the defined range;</li>
     *             <li>{@code outputBufferSize} is not in the defined range.</li>
     *             </ul>
     * @throws NullPointerException
     *             if {@code controllerName} is {@code null}.
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public UARTConfig(String controllerName, int channelNumber, int baudRate, int dataBits, int parity, int stopBits, int flowcontrol,
            int inputBufferSize, int outputBufferSize) {
        this.controllerName = controllerName;
        this.channelNumber = channelNumber;
        this.baudRate = baudRate;
        this.dataBits = dataBits;
        this.stopBits = stopBits;
        this.parity = parity;
        this.flowcontrol = flowcontrol;
        this.inputBufferSize = inputBufferSize;
        this.outputBufferSize = outputBufferSize;
        //checks for null
        controllerName.length();
        checkParameters();
    }

    private void checkParameters(){
        if ((null == controllerName && controllerNumber < UNASSIGNED) ||
            channelNumber < UNASSIGNED ||
            baudRate <= 0 ||
            (dataBits < DATABITS_5 || dataBits > DATABITS_9) ||
            (parity < PARITY_NONE || parity > PARITY_SPACE) ||
            (stopBits < STOPBITS_1 || stopBits > STOPBITS_2) ||
            //check if flowcontrol is bitwise of supported values
            (0 != (flowcontrol & ~(UARTConfig.FLOWCONTROL_NONE | UARTConfig.FLOWCONTROL_RTSCTS_IN |
                               UARTConfig.FLOWCONTROL_RTSCTS_OUT | UARTConfig.FLOWCONTROL_XONXOFF_IN |
                               UARTConfig.FLOWCONTROL_XONXOFF_OUT))) ||
            //check if 1 flaf for input is set
            (0!=(flowcontrol&UARTConfig.FLOWCONTROL_RTSCTS_IN) && 0!=(flowcontrol&UARTConfig.FLOWCONTROL_XONXOFF_IN)) ||
            //check if 1 flaf for output is set
            (0!=(flowcontrol&UARTConfig.FLOWCONTROL_RTSCTS_OUT) && 0!=(flowcontrol&UARTConfig.FLOWCONTROL_XONXOFF_OUT)) ||
            inputBufferSize  < UNASSIGNED ||
            outputBufferSize < UNASSIGNED ) {
           throw new IllegalArgumentException();
        }
    }

    /**
     * Creates a new {@code UARTConfig} whose state is deserialized from the specified {@code InputStream}.
     * This method may be invoked to restore the state of a {@code UARTConfig}
     * object from a persistent store.
     *
     * @param in the stream to read from.
     * @return a new {@code UARTConfig} instance.
     * @throws IOException if an I/O error occurs or if the provided stream does not
     * contain a representation of a {@code UARTConfig} object.
     *
     * @since 1.1
     */
    public static UARTConfig deserialize(InputStream in) throws IOException {
        return (UARTConfig) Platform.deserialize(in);
    }

    /**
     * Serializes the state of this {@code UARTConfig} object to the specified {@code OutputStream}.
     * This method may be invoked by the {@link jdk.dio.DeviceManager DeviceManager}
     * to save the state of this {@code UARTConfig} object to a persistent store.
     *
     * @param out the stream to write to.
     * @throws IOException if an I/O error occurs.
     *
     * @since 1.1
     */
    @Override
    public int serialize(OutputStream out) throws IOException {
      return Platform.serialize(this, out);
    }

    /**
     * Gets the configured initial speed in Bauds.
     *
     * @return the initial speed in Bauds (a positive integer).
     */
    public int getBaudRate() {
        return baudRate;
    }

    /**
     * Gets the configured initial number of bits per character.
     *
     * @return the initial number of bits per character, one of: {@link #DATABITS_5}, {@link #DATABITS_6},
     *         {@link #DATABITS_7}, {@link #DATABITS_8} or {@link #DATABITS_9}.
     */
    public int getDataBits() {
        return dataBits;
    }

    /**
     * Gets the configured initial flow control mode.
     *
     * @return the flow control mode: {@link #FLOWCONTROL_NONE} if flow control is disabled; or a valid bit-wise OR combination of
     *         {@link #FLOWCONTROL_RTSCTS_IN}, {@link #FLOWCONTROL_RTSCTS_OUT}, {@link #FLOWCONTROL_XONXOFF_IN} or
     *         {@link #FLOWCONTROL_XONXOFF_OUT}.
     */
    public int getFlowControlMode() {
        return flowcontrol;
    }

    /**
     * Gets the requested or allocated input buffer size. The platform/underlying
     * driver may or may not allocate the requested size
     * for the input buffer.
     * When querying the configuration of an opened or registered device the
     * allocated buffer size is returned.
     *
     * @return the requested or allocated input buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getInputBufferSize() {
        return inputBufferSize;
    }

    /**
     * Gets the requested or allocated output buffer size. The platform/underlying
     * driver may or may not allocate the requested size
     * for the output buffer.
     * When querying the configuration of an opened or registered device the
     * allocated buffer size is returned.
     *
     * @return the requested or allocated output buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getOutputBufferSize() {
        return outputBufferSize;
    }

    /**
     * Gets the configured initial parity.
     *
     * @return the initial parity, one of: {@link #PARITY_ODD}, {@link #PARITY_EVEN}, {@link #PARITY_MARK},
     *         {@link #PARITY_SPACE}, or {@link #PARITY_NONE}.
     */
    public int getParity() {
        return parity;
    }

    /**
     * Gets the configured initial number of stop bits per character.
     *
     * @return the initial number of stop bits per character, on of: {@link #STOPBITS_1}, {@link #STOPBITS_1_5},
     *         or {@link #STOPBITS_2}.
     */
    public int getStopBits() {
        return stopBits;
    }
    /**
     * Gets the configured UART channel number.
     *
     * @return the hardware channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getChannelNumber() {
        return channelNumber;
    }

    /**
     * Gets the configured UART controller number.
     *
     * @return the hardware controller's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    @Override
    public int getControllerNumber() {
        return controllerNumber;
    }

    /**
     * Gets the configured controller name (such as its <em>device file</em> name on UNIX systems).
     *
     * @return the controller name or {@code null}.
     */
    @Override
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Returns the hash code value for this object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Platform.hash(this, 7, 53);
    }

    /**
     * Checks two {@code UARTConfig} objects for equality.
     *
     * @param obj
     *            the object to test for equality with this object.
     *
     * @return {@code true} if {@code obj} is a {@code UARTConfig} and has
     * the same hardware addressing information and configuration parameter values
     * as this {@code UARTConfig} object; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return Platform.equals(this, obj);
    }
}
