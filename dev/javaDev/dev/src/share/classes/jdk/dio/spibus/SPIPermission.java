/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.spibus;

import jdk.dio.DeviceManager;
import jdk.dio.DevicePermission;
import java.security.Permission;
import java.security.PermissionCollection;
import com.oracle.dio.utils.Utils;

/**
 * The {@code SPIPermission} class defines permissions for SPI slave device access.
 * <p>
 * A {@code SPIPermission} permission has a target name and a list of actions.
 * </p><p>
 * The target name contains hardware addressing information. The format is the one defined for the base {@link DevicePermission} class
 * with the following addition:</p>
 * <blockquote>
 * <dl>
 * <dt><code>{channel-desc}</code></dt>
 * <dd>
 * The <code>{channel-desc}</code> string (described in {@link DevicePermission}) is
 * the hexadecimal string representation of a device address on the SPI bus as may be returned by a call to
 * {@link SPIDeviceConfig#getAddress SPIDeviceConfig.getAddress}. The characters in the string must all be hexadecimal digits.
 * </dd>
 * </dl>
 * </blockquote>
 * <p>
 * The supported actions are {@code open} and {@code powermanage} as defined in {@link DevicePermission}.
 * </p>
 *
 * @see DeviceManager#open DeviceManager.open
 * @see jdk.dio.power.PowerManaged
 * @since 1.0
 */
@SuppressWarnings("serial")
@apimarker.API("device-io_1.1_spibus")
public class SPIPermission extends DevicePermission {

    /**
     * Constructs a new {@code ADCPermission} with the specified target name and the implicit {@code open} action.
     * The target name is normalized so that leading and trailing spaces are removed
     * and each occurrence of <code>{controller-number}</code> and <code>{channel-desc}</code> is respectively represented in its canonical
     * decimal and hexadecimal representation form (without leading zeros).
     *
     * @param name
     *            the target name (as defined above).
     * @throws NullPointerException
     *             if {@code name} is {@code null}.
     * @throws IllegalArgumentException
     *             if {@code name} is not properly formatted.
     *
     * @see #getName getName
     */
    public SPIPermission(String name) {
        super(name);
        Utils.checkDevicePermissionChannelFormat(name, Utils.HEXADECIMAL_DIGITS);
    }

    /**
     * Constructs a new {@code SPIPermission} instance with the specified target name and action list.
     * The target name is normalized so that leading and trailing spaces are removed
     * and each occurrence of <code>{controller-number}</code> and <code>{channel-desc}</code> is respectively represented in its canonical
     * decimal and hexadecimal representation form (without leading zeros).
     *
     * @param name
     *            the target name (as defined above).
     * @param actions
     *            comma-separated list of device operations: {@code open} or {@code powermanage}.
     * @throws NullPointerException
     *             if {@code name} is {@code null}.
     * @throws IllegalArgumentException
     *             <ul>
     *             <li>if {@code actions} is {@code null}, empty or contains an action other than the
     *             specified possible actions,</li>
     *             <li>if {@code name} is not properly formatted.</li>
     *             </ul>
     *
     * @see #getName getName
     */
    public SPIPermission(String name, String actions) {
        super(name, actions);
       Utils.checkDevicePermissionChannelFormat(name, Utils.HEXADECIMAL_DIGITS);
    }
}
