/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.watchdog;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.oracle.dio.impl.Platform;

import jdk.dio.DeviceConfig;
import jdk.dio.DeviceManager;
import jdk.dio.InvalidDeviceConfigException;

import romizer.*;

import serializator.*;

/**
 * The {@code WatchdogTimerConfig} class encapsulates the hardware addressing information, and static and dynamic
 * configuration parameters of a watchdog timer.
 * <p>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} or {@code null} (see
 * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
 * </p><p>
 * An instance of {@code WatchdogTimerConfig} can be passed to the {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} to open the designated watchdog timer with the specified
 * configuration. A {@link InvalidDeviceConfigException} is thrown when attempting to open a device with
 * an invalid or unsupported configuration.
 * </p>
 *
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(DeviceConfig, int)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig, int)
 * @since 1.0
 */
@SerializeMe
@apimarker.API("device-io_1.1_watchdog")
public final class WatchdogTimerConfig implements DeviceConfig<WatchdogTimer>, DeviceConfig.HardwareAddressing {

    private String controllerName;

    private int controllerNumber = UNASSIGNED;

    private int timerNumber = UNASSIGNED;

    // hidden constructor for serializer
    @DontRenameMethod
    WatchdogTimerConfig() {}

    /**
     * Creates a new {@code WatchdogTimerConfig} with the specified hardware addressing information.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param timerNumber
     *            the hardware timer's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @throws IllegalArgumentException
     *             if {@code timerNumber} is not in the defined range.
     * @throws NullPointerException
     *             if {@code controller name} is {@code null}.
     */
    public WatchdogTimerConfig(String controllerName, int timerNumber) {
        this.controllerName = controllerName;
        this.timerNumber = timerNumber;
        // checks for null
        controllerName.length();
        if (UNASSIGNED > timerNumber) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Creates a new {@code WatchdogTimerConfig} with the specified hardware addressing information.
     * <p>
     * The controller name is set to {@code null}.
     * </p>
     *
     * @param controllerNumber
     *            the controller number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param timerNumber
     *            the hardware timer's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @throws IllegalArgumentException
     *             if {@code timerNumber} is not in the defined range.
     */
    public WatchdogTimerConfig(int controllerNumber, int timerNumber) {
        this.controllerNumber = controllerNumber;
        this.timerNumber = timerNumber;
        if (UNASSIGNED > timerNumber || UNASSIGNED > controllerNumber) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Creates a new {@code WatchdogTimerConfig} whose state is deserialized from the specified {@code InputStream}.
     * This method may be invoked to restore the state of a {@code WatchdogTimerConfig}
     * object from a persistent store.
     *
     * @param in the stream to read from.
     * @return a new {@code WatchdogTimerConfig} instance.
     * @throws IOException if an I/O error occurs or if the provided stream does not
     * contain a representation of a {@code WatchdogTimerConfig} object.
     *
     * @since 1.1
     */
    public static WatchdogTimerConfig deserialize(InputStream in) throws IOException {
        return (WatchdogTimerConfig) Platform.deserialize(in);
    }

    /**
     * Serializes the state of this {@code WatchdogTimerConfig} object to the specified {@code OutputStream}.
     * This method may be invoked by the {@link jdk.dio.DeviceManager DeviceManager}
     * to save the state of this {@code WatchdogTimerConfig} object to a persistent store.
     *
     * @param out the stream to write to.
     * @throws IOException if an I/O error occurs.
     *
     * @since 1.1
     */
    @Override
    public int serialize(OutputStream out) throws IOException {
      return Platform.serialize(this, out);
    }

    /**
     * Gets the configured timer number.
     *
     * @return the timer number (a positive or zero integer); or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getTimerNumber() {
        return timerNumber;
    }

    /**
     * Gets the configured controller number.
     *
     * @return the controller number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    @Override
    public int getControllerNumber() {
        return controllerNumber;
    }

    /**
     * Gets the configured controller name (such as its <em>device file</em> name on UNIX systems).
     *
     * @return the controller name or {@code null}.
     */
    @Override
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Returns the hash code value for this object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Platform.hash(this, 3, 79);
    }

    /**
     * Checks two {@code WatchdogTimerConfig} objects for equality.
     *
     * @param obj
     *            the object to test for equality with this object.
     *
     * @return {@code true} if {@code obj} is a {@code WatchdogTimerConfig} and has
     * the same hardware addressing information and configuration parameter values
     * as this {@code WatchdogTimerConfig} object; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return Platform.equals(this, obj);
    }
}
