/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.generic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.oracle.dio.impl.Platform;
import com.oracle.dio.utils.Utils;

import jdk.dio.DeviceConfig;
import jdk.dio.DeviceManager;
import jdk.dio.InvalidDeviceConfigException;

import romizer.DontRenameMethod;

import serializator.*;


/**
 * The {@code GenericDeviceConfig} class encapsulates the hardware addressing information of generic
 * device.
 * It does not encapsulates static or dynamic configuration parameters;
 * configuration parameters should be set using the {@link GenericDevice#setControl
 * GenericDevice.setControl} method.
 * <p>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} or {@code null} (see
 * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
 * </p><p>
 * An instance of {@code GenericDeviceConfig} can be passed to the
 * {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} to open the designated generic
 * device with the specified configuration. A {@link InvalidDeviceConfigException} is thrown
 * when attempting to open a device with an invalid or unsupported configuration.
 * </p>
 *
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(DeviceConfig, int)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig, int)
 * @since 1.0
 */
@SerializeMe
@apimarker.API("device-io_1.1_generic")
public final class GenericDeviceConfig implements DeviceConfig<GenericDevice>, DeviceConfig.HardwareAddressing {
    private String controllerName;
    private int channelNumber = UNASSIGNED;
    private int controllerNumber = UNASSIGNED;
    private int inputBufferSize = UNASSIGNED;
    private int outputBufferSize = UNASSIGNED;


    /**
     * The {@code Builder} class allows for creating and initializing
     * {@code GenericDeviceConfig} objects. Calls can be chained in the following manner:
     * <blockquote>
     * <pre>
     *   GenericDeviceConfig config = new GenericDeviceConfig.Builder()
     *           .setControllerNumber(1)
     *           .setChannelNumber(1)
     *           .setInputBufferSize(0)
     *           .setOutputBufferSize(0)
     *           .build();
     * </pre>
     * </blockquote>
     *
     * @since 1.1
     */
    @apimarker.API("device-io_1.1_generic")
    public static final class Builder {

        private GenericDeviceConfig instance = new GenericDeviceConfig();

        public Builder() {

        }

        /**
         * Creates a new {@code GenericDeviceConfig} instance initialized with the
         * values set for each configuration parameters. If a configuration
         * parameter was not explictly set its default value will be used.
         *
         * @return a new initialized {@code GenericDeviceConfig} instance.
         */
        public GenericDeviceConfig build() {
            return instance;
        }

        /**
         * Sets the controller name (default value is {@code null} if not set).
         *
         * @param controllerName the controller name (such as its <em>device
         * file</em> name on UNIX systems) or {@code null}.
         * @return this {@code Builder} instance.
         */
        public Builder setControllerName(String controllerName) {
            instance.controllerName = controllerName;
            return this;
        }

        /**
         * Sets the channel number (default value is {@code UNASSIGNED} if not
         * set).
         *
         * @param channelNumber the channel number (a positive or zero integer)
         * or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code channelNumber} is not in
         * the defined range.
         */
        public Builder setChannelNumber(int channelNumber) {
            Utils.checkIntValue(channelNumber);
            instance.channelNumber = channelNumber;
            return this;
        }

        /**
         * Sets the controller number (default value is {@code UNASSIGNED} if
         * not set).
         *
         * @param controllerNumber the hardware converter's number (a positive
         * or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code controllerNumber} is not
         * in the defined range.
         */
        public Builder setControllerNumber(int controllerNumber) {
            Utils.checkIntValue(controllerNumber);
            instance.controllerNumber = controllerNumber;
            return this;
        }

        /**
         * Sets the requested input buffer size (default value is
         * {@code UNASSIGNED} if not set).
         *
         * @param inputBufferSize the requested input buffer size in number of
         * samples (a positive or zero integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code inputBufferSize} is not in
         * the defined range.
         */
        public Builder setInputBufferSize(int inputBufferSize) {
            Utils.checkIntValue(inputBufferSize);
            instance.inputBufferSize = inputBufferSize;
            return this;
        }

        /**
         * Sets the requested output buffer size (default value is
         * {@code UNASSIGNED} if not set).
         *
         * @param outputBufferSize the requested output buffer size in number of
         * samples (a positive or zero integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code outputBufferSize} is not
         * in the defined range.
         */
        public Builder setOutputBufferSize(int outputBufferSize) {
            Utils.checkIntValue(outputBufferSize);
            instance.outputBufferSize = outputBufferSize;
            return this;
        }
    }

    // hidden constructor for serializer
    @DontRenameMethod
    GenericDeviceConfig(){}

    /**
     * Creates a new {@code GenericDeviceConfig} with the specified hardware addressing information
     * .
     * <p>
     * The controller name is set to {@code null}.
     * The requested input and output buffer sizes are set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerNumber
     *            the hardware controller's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber
     *            the hardware device's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code controllerNumber} is not in the defined range;</li>
     *             <li>{@code channelNumber} is not in the defined range.</li>
     *             </ul>
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public GenericDeviceConfig(int controllerNumber, int channelNumber) {
        if (controllerNumber < UNASSIGNED || channelNumber < UNASSIGNED) {
            throw new IllegalArgumentException();
        }
        this.controllerNumber = controllerNumber;
        this.channelNumber = channelNumber;
    }

    /**
     * Creates a new {@code GenericDeviceConfig} with the specified hardware addressing information.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * The requested input and output buffer sizes are set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param channelNumber
     *            the hardware device's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @throws IllegalArgumentException
     *             if {@code channelNumber} is not in the defined range.
     * @throws NullPointerException
     *             if {@code controllerName} is {@code null}.
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public GenericDeviceConfig(String controllerName, int channelNumber) {
        this(UNASSIGNED, channelNumber);
        // NPE check
        controllerName.length();
        this.controllerName = controllerName;
    }

    /**
     * Creates a new {@code GenericDeviceConfig} whose state is deserialized from the specified {@code InputStream}.
     * This method may be invoked to restore the state of a {@code GenericDeviceConfig}
     * object from a persistent store.
     *
     * @param in the stream to read from.
     * @return a new {@code GenericDeviceConfig} instance.
     * @throws IOException if an I/O error occurs or if the provided stream does not
     * contain a representation of a {@code GenericDeviceConfig} object.
     *
     * @since 1.1
     */
    public static GenericDeviceConfig deserialize(InputStream in) throws IOException {
        return (GenericDeviceConfig) Platform.deserialize(in);
    }

    /**
     * Serializes the state of this {@code GenericDeviceConfig} object to the specified {@code OutputStream}.
     * This method may be invoked by the {@link jdk.dio.DeviceManager DeviceManager}
     * to save the state of this {@code GenericDeviceConfig} object to a persistent store.
     *
     * @param out the stream to write to.
     * @throws IOException if an I/O error occurs.
     *
     * @since 1.1
     */
    @Override
    public int serialize(OutputStream out) throws IOException {
        return Platform.serialize(this, out);
    }

    /**
     * Gets the configured channel number.
     *
     * @return the hardware device channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}
     *         .
     */
    public int getChannelNumber() {
        return channelNumber;
    }

    /**
     * Gets the configured controller number.
     *
     * @return the hardware device controller's number (a positive or zero integer) or
     *         {@link #UNASSIGNED UNASSIGNED}.
     */
    @Override
    public int getControllerNumber() {
        return controllerNumber;
    }

    /**
     * Gets the configured controller name (such as its <em>device file</em> name on UNIX systems).
     *
     * @return the controller name or {@code null}.
     */
    @Override
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Gets the requested or allocated input buffer size. The platform/underlying
     * driver may or may not allocate the requested size
     * for the input buffer.
     * When querying the configuration of an opened or registered device the
     * allocated buffer size is returned.
     *
     * @return the requested or allocated input buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     *
     * @since 1.1
     */
    public int getInputBufferSize() {
        return inputBufferSize;
    }

    /**
     * Gets the requested or allocated output buffer size. The platform/underlying
     * driver may or may not allocate the requested size
     * for the output buffer.
     * When querying the configuration of an opened or registered device the
     * allocated buffer size is returned.
     *
     * @return the requested or allocated output buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     *
     * @since 1.1
     */
    public int getOutputBufferSize() {
        return outputBufferSize;
    }

    /**
     * Returns the hash code value for this object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Platform.hash(this, 3, 97);
    }

    /**
     * Checks two {@code GenericDeviceConfig} objects for equality.
     *
     * @param obj
     *            the object to test for equality with this object.
     * @return {@code true} if {@code obj} is a {@code GenericDeviceConfig} and has the same hardware
     *         addressing information and configuration parameter values as this
     *         {@code GenericDeviceConfig} object; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return Platform.equals(this, obj);
    }
}
