/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package jdk.dio.dac;

import jdk.dio.DeviceManager;
import jdk.dio.DevicePermission;
import java.security.Permission;
import java.security.PermissionCollection;
import com.oracle.dio.utils.Utils;

/**
 * The {@code DACPermission} class defines permissions for DAC channel access.
 * <p>
 * The target name contains hardware addressing information. The format is the one defined for the
 * base {@link DevicePermission} class with the following addition:
 * </p>
 * <blockquote>
 * <dl>
 * <dt><code>{channel-desc}</code></dt>
 * <dd>The <code>{channel-desc}</code> string (described in {@link DevicePermission}) is the
 * decimal string representation of a channel number as may be returned by a call to
 * {@link DACChannelConfig#getChannelNumber DACChannelConfig.getChannelNumber}. The characters in
 * the string must all be decimal digits.</dd>
 * </dl>
 * </blockquote>
 * <p>
 * The supported actions are {@code open} and {@code powermanage} as defined in {@link DevicePermission}.
 * </p>
 *
 * @see DeviceManager#open DeviceManager.open
 * @see jdk.dio.power.PowerManaged
 * @since 1.0
 */
@apimarker.API("device-io_1.1_dac")
public class DACPermission extends DevicePermission {

    /**
     * Constructs a new {@code DACPermission} with the specified target name and the implicit
     * {@code open} action.
     * The target name is normalized so that leading and trailing spaces are removed
     * and each occurrence of <code>{controller-number}</code> and <code>{channel-desc}</code> is represented in its canonical
     * decimal representation form (without leading zeros).
     *
     * @param name
     *            the target name (as defined above).
     * @throws NullPointerException
     *             if {@code name} is {@code null}.
     * @throws IllegalArgumentException
     *             if {@code name} is not properly formatted.
     * @see #getName getName
     */
    public DACPermission(String name) {
        super(name);
        Utils.checkDevicePermissionChannelFormat(name, Utils.DECIMAL_DIGITS);
    }

    /**
     * Constructs a new {@code DACPermission} instance with the specified target name and action
     * list.
     * The target name is normalized so that leading and trailing spaces are removed
     * and each occurrence of <code>{controller-number}</code> and <code>{channel-desc}</code> is represented in its canonical
     * decimal representation form (without leading zeros).
     *
     * @param name
     *            the target name (as defined above).
     * @param actions
     *            comma-separated list of device operations: {@code open} or {@code powermanage}
     *            .
     * @throws NullPointerException
     *             if {@code name} is {@code null}.
     * @throws IllegalArgumentException
     *             <ul>
     *             <li>if {@code actions} is {@code null}, empty or contains an action other than the
     *             specified possible actions,</li>
     *             <li>if {@code name} is not properly formatted.</li>
     *             </ul>
     * @see #getName getName
     */
    public DACPermission(String name, String actions) {
        super(name, actions);
        Utils.checkDevicePermissionChannelFormat(name, Utils.DECIMAL_DIGITS);
    }
}
