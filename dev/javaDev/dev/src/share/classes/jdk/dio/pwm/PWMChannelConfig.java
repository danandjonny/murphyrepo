/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.pwm;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.oracle.dio.impl.Platform;
import com.oracle.dio.utils.ExceptionMessage;
import com.oracle.dio.utils.Utils;

import jdk.dio.DeviceConfig;
import jdk.dio.DeviceManager;
import jdk.dio.InvalidDeviceConfigException;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;

import romizer.*;

import serializator.*;

/**
 * The {@code PWMChannelConfig} class encapsulates the hardware addressing information, and static and dynamic
 * configuration parameters of a PWM channel.
 * <p>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} or {@code null} (see
 * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
 * </p><p>
 * An instance of {@code PWMChannelConfig} can be passed to the {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} to open the designated PWM channel with the specified
 * configuration. A {@link InvalidDeviceConfigException} is thrown when attempting to open a device with
 * an invalid or unsupported configuration.
 * </p>
 *
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @since 1.0
 */
@SerializeMe
@apimarker.API("device-io_1.1_pwm")
public final class PWMChannelConfig implements DeviceConfig<PWMChannel>, DeviceConfig.HardwareAddressing {

    /**
     * High idle state. The idle state is the state of the PWM output when no pulse is generated.
     */
    public static final int IDLE_STATE_HIGH = 0;
    /**
     * Low idle state. The idle state is the state of the PWM output when no pulse is generated.
     */
    public static final int IDLE_STATE_LOW = 1;
    /**
     * Center alignement. The pulse is centered within the pulse period.
     */
    public static final int ALIGN_CENTER = 0;
    /**
     * Left alignement. The start of the pulse coincides with the start of the pulse period.
     */
    public static final int ALIGN_LEFT = 1;
    /**
     * Right alignement. The end of the pulse coincides with the end of the pulse period.
     */
    public static final int ALIGN_RIGHT = 2;

    private String controllerName;
    private int channelNumber = UNASSIGNED;
    private int controllerNumber = UNASSIGNED;
    private GPIOPinConfig outputConfig;
    private int idleState = UNASSIGNED;
    private int pulsePeriod = UNASSIGNED;

    // keep scale as longbits view of Double to make the class be friendly for serialization and easy comparizon
    private long scaleFactor = 0x3FF0000000000000l;// 1.0

    private int pulseAlignment = UNASSIGNED;
    private int outputBufferSize = UNASSIGNED;

    /**
     * The {@code Builder} class allows for creating and initializing {@code PWMChannelConfig} objects.
     * Calls can be chained in the following manner:
     * <blockquote>
     * <pre>
     *   PWMChannelConfig config = new PWMChannelConfig.Builder()
     *           .setControllerNumber(1)
     *           .setChannelNumber(1)
     *           .setScaleFactor(1.0)
     *           .setPulsePeriod(10)
     *           .setIdleState(IDLE_STATE_HIGH)
     *           .setPulseAlignment(ALIGN_CENTER)
     *           .setOutputBufferSize(0)
     *           .build();
     * </pre>
     * </blockquote>
     *
     * @since 1.1
     */
    @apimarker.API("device-io_1.1_pwm")
    public static final class Builder {

        private final PWMChannelConfig instance = new PWMChannelConfig();

        /**
         * Creates a new {@code Builder} instance.
         */
        public Builder() {

        }

        /**
         * Creates a new {@code PWMChannelConfig} instance initialized with the
         * values set for each configuration parameters. If a configuration parameter
         * was not explictly set its default value will be used.
         *
         * @return a new initialized {@code PWMChannelConfig} instance.
         */
        public PWMChannelConfig build() {
            return (PWMChannelConfig)Platform.clone(instance);
        }

        /**
         * Sets the controller name (default value is {@code null} if not set).
         *
         * @param controllerName the controller name (such as its <em>device
         * file</em> name on UNIX systems) or {@code null}.
         * @return this {@code Builder} instance.
         */
        public Builder setControllerName(String controllerName) {
            instance.controllerName = controllerName;
            return this;
        }

        /**
         * Sets the channel number (default value is {@code UNASSIGNED} if not set).
         *
         * @param channelNumber the channel number (a positive or zero integer)
         * or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code channelNumber} is not in
         * the defined range.
         */
        public Builder setChannelNumber(int channelNumber) {
            Utils.checkIntValue(channelNumber);
            instance.channelNumber = channelNumber;
            return this;
        }

        /**
         * Sets the controller number (default value is {@code UNASSIGNED} if not set).
         *
         * @param controllerNumber the hardware converter's number (a positive
         * or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code controllerNumber} is not
         * in the defined range.
         */
        public Builder setControllerNumber(int controllerNumber) {
            Utils.checkIntValue(controllerNumber);
            instance.controllerNumber = controllerNumber;
            return this;
        }

        /**
         * Sets the configuration of the output (a GPIO output pin) on which the
         * pulses are to be generated (default value is {@code null} if not set).
         *
         * @param outputConfig the configuration of the output (a GPIO output pin);
         * or {@code null} if the output is implicit.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code outputConfig} is not
         * {@code null} and {@code outputConfig}'s direction is not set to
         * {@link GPIOPinConfig#DIR_OUTPUT_ONLY} or {@link GPIOPinConfig#DIR_BOTH_INIT_OUTPUT}.
         */
        public Builder setOutputConfig(GPIOPinConfig outputConfig) {
            if (outputConfig != null) {
                checkOutput(outputConfig);
            }
            instance.outputConfig = outputConfig;
            return this;
        }

        /**
         * Sets the idle output state (default value is {@code UNASSIGNED} if not
         * set).
         *
         * @param idleState the idle output state: : {@link #IDLE_STATE_HIGH}, {@link #IDLE_STATE_LOW} or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code idleState} is not in the
         * defined range.
         */
        public Builder setIdleState(int idleState) {
            checkIdle(idleState);
            instance.idleState = idleState;
            return this;
        }

        /**
         * Sets the initial <em>scaled</em> output sampling interval (default value is {@code UNASSIGNED} if not
         * set).
         *
         * @param pulsePeriod the initial scaled output sampling interval
         * (the amount of time between two samples) in microseconds (a positive
         * integer) or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code pulsePeriod} is not
         * in the defined range.
         *
         * @see #setScaleFactor
         */
        public Builder setPulsePeriod(int pulsePeriod) {
            Utils.checkGreaterThanZero(pulsePeriod);
            instance.pulsePeriod = pulsePeriod;
            return this;
        }

        /**
         * Sets the initial pulse period scale factor (default value is {@code 1.0} if not set).
         *
         * @param scaleFactor the sampling time and sampling interval scale
         * factor (a number greater or equal to {@code 1.0}).
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code scaleFactor} is {@link Double#NaN NaN} or
         * is less than {@code 1.0}.
         */
        public Builder setScaleFactor(double scaleFactor) {
            Utils.checkDoubleGreaterThanZero(scaleFactor);
            instance.scaleFactor = Double.doubleToLongBits(scaleFactor);
            return this;
        }

        /**
         * Sets the pulse alignment - the alignment of the pulse within the pulse period (default value is {@code UNASSIGNED} if not
         * set).
         *
         * @param pulseAlignment the pulse alignment: {@link #ALIGN_CENTER}, {@link #ALIGN_LEFT}, {@link #ALIGN_RIGHT} or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code pulseAlignment} is not in the
         * defined range.
         */
        public Builder setPulseAlignment(int pulseAlignment) {
            checkAligment(pulseAlignment);
            instance.pulseAlignment = pulseAlignment;
            return this;
        }

        /**
         * Sets the requested output buffer size (default value is {@code UNASSIGNED} if not set).
         *
         * @param outputBufferSize the requested output buffer size in number of
         * samples (a positive or zero integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code outputBufferSize} is not in
         * the defined range.
         */
        public Builder setOutputBufferSize(int outputBufferSize) {
            Utils.checkIntValue(outputBufferSize);
            instance.outputBufferSize = outputBufferSize;
            return this;
        }
    }


    // hidden constructor for serializer
    @DontRenameMethod
    PWMChannelConfig() {}

    /**
     * Creates a new {@code PWMChannelConfig} with the specified hardware addressing information and configuration parameters. The output of
     * the PWM channel is implicit (such as a dedicated output pin).
     * <p>
     * The controller name is set to {@code null}.
     * The requested output buffer size is set to {@code UNASSIGNED}.
     * The pulse period time scale factor is set to {@code 1.0}.
     * </p>
     *
     * @param controllerNumber
     *            the hardware PWM controller (or generator)'s number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber
     *            the hardware PWM channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param idleState
     *            the output idle state: {@link #IDLE_STATE_HIGH}, {@link #IDLE_STATE_LOW} or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulsePeriod
     *            the initial pulse period in microseconds (a positive integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulseAlignment
     *            the pulse alignment: {@link #ALIGN_CENTER}, {@link #ALIGN_LEFT}, {@link #ALIGN_RIGHT} or
     *            {@link #UNASSIGNED UNASSIGNED}.
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code controllerNumber} is not in the defined range;</li>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code idleState} is not in the defined range;</li>
     *             <li>{@code pulsePeriod} is not in the defined range.</li>
     *             <li>{@code pulseAlignment} is not in the defined range.</li>
     *             </ul>
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public PWMChannelConfig(int controllerNumber, int channelNumber, int idleState, int pulsePeriod, int pulseAlignment) {
        this.controllerNumber = controllerNumber;
        this.channelNumber = channelNumber;
        this.idleState = idleState;
        this.pulsePeriod = pulsePeriod;
        this.pulseAlignment = pulseAlignment;
        checkParameters();
    }

    /**
     * Creates a new {@code PWMChannelConfig} the specified hardware addressing information, configuration parameters and GPIO pin output.
     * <p>
     * The controller name is set to {@code null}.
     * The requested output buffer size is set to {@code UNASSIGNED}.
     * The pulse period time scale factor is set to {@code 1.0}.
     * </p><p>
     * If the access modes (exclusive or shared) supported by the designated
     * GPIO pin output are incompatible with those required by the underlying {@code PWMChannel}
     * device or device driver, attempting to open
     * the {@code PWMChannel} device using this configuration may result in a
     * {@link InvalidDeviceConfigException} to be thrown.
     * </p>
     *
     * @param controllerNumber
     *            the hardware PWM controller (or generator)'s number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber
     *            the hardware PWM channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param idleState
     *            the output idle state: {@link #IDLE_STATE_HIGH}, {@link #IDLE_STATE_LOW} or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulsePeriod
     *            the initial pulse period in microseconds (a positive integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulseAlignment
     *            the pulse alignment: {@link #ALIGN_CENTER}, {@link #ALIGN_LEFT}, {@link #ALIGN_RIGHT} or
     *            {@link #UNASSIGNED UNASSIGNED}.
     * @param output
     *            the configuration of the output (a GPIO output pin) on which the pulses are to be generated.
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code controllerNumber} is not in the defined range;</li>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code idleState} is not in the defined range;</li>
     *             <li>{@code pulsePeriod} is not in the defined range;</li>
     *             <li>{@code pulseAlignment} is not in the defined range;</li>
     *             <li>if the provided GPIO pin configuration's direction is not set to
     *             {@link GPIOPinConfig#DIR_OUTPUT_ONLY} or {@link GPIOPinConfig#DIR_BOTH_INIT_OUTPUT}.</li>
     *             </ul>
     * @throws NullPointerException
     *             if {@code output} is {@code null}.
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public PWMChannelConfig(int controllerNumber, int channelNumber, int idleState, int pulsePeriod,
            int pulseAlignment, GPIOPinConfig output) {
        this(controllerNumber, channelNumber, idleState, pulsePeriod, pulseAlignment);
        checkOutput(output);
        this.outputConfig = output;
    }

    /**
     * Creates a new {@code PWMChannelConfig} with the specified hardware addressing information and configuration parameters. The output of
     * the PWM channel is implicit (such as a dedicated output pin).
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * The requested output buffer size is set to {@code UNASSIGNED}.
     * The pulse period time scale factor is set to {@code 1.0}.
     * </p>
     *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param channelNumber
     *            the hardware PWM channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param idleState
     *            the output idle state: {@link #IDLE_STATE_HIGH}, {@link #IDLE_STATE_LOW} or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulsePeriod
     *            the initial pulse period in microseconds (a positive integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulseAlignment
     *            the pulse alignment: {@link #ALIGN_CENTER}, {@link #ALIGN_LEFT}, {@link #ALIGN_RIGHT} or
     *            {@link #UNASSIGNED UNASSIGNED}.
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code idleState} is not in the defined range;</li>
     *             <li>{@code pulsePeriod} is not in the defined range;</li>
     *             <li>{@code pulseAlignment} is not in the defined range.</li>
     *             </ul>
     * @throws NullPointerException
     *             if {@code controllerName} is {@code null}.
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public PWMChannelConfig(String controllerName, int channelNumber, int idleState, int pulsePeriod, int pulseAlignment) {
        this(UNASSIGNED, channelNumber, idleState, pulsePeriod, pulseAlignment);
        // null check
        controllerName.length();
        this.controllerName = controllerName;
    }

    /**
     * Creates a new {@code PWMChannelConfig} the specified hardware addressing information, configuration parameters and GPIO pin output.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * The requested output buffer size is set to {@code UNASSIGNED}.
     * The pulse period time scale factor is set to {@code 1.0}.
     * </p><p>
     * If the access modes (exclusive or shared) supported by the designated
     * GPIO pin output are incompatible with those required by the underlying {@code PWMChannel}
     * device or device driver, attempting to open
     * the {@code PWMChannel} device using this configuration may result in a
     * {@link InvalidDeviceConfigException} to be thrown.
     * </p>
      *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param channelNumber
     *            the hardware PWM channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param idleState
     *            the output idle state: {@link #IDLE_STATE_HIGH}, {@link #IDLE_STATE_LOW} or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulsePeriod
     *            the initial pulse period in microseconds (a positive integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param pulseAlignment
     *            the pulse alignment: {@link #ALIGN_CENTER}, {@link #ALIGN_LEFT}, {@link #ALIGN_RIGHT} or
     *            {@link #UNASSIGNED UNASSIGNED}.
     * @param output
     *            the configuration of the output (a GPIO output pin) on which the pulses are to be generated.
     *
     * @throws IllegalArgumentException
     *             if any of the following is true:
     *             <ul>
     *             <li>{@code channelNumber} is not in the defined range;</li>
     *             <li>{@code idleState} is not in the defined range;</li>
     *             <li>{@code pulsePeriod} is not in the defined range;</li>
     *             <li>{@code pulseAlignment} is not in the defined range;</li>
     *             <li>if the provided GPIO pin configuration's direction is not set to
     *             {@link GPIOPinConfig#DIR_OUTPUT_ONLY} or {@link GPIOPinConfig#DIR_BOTH_INIT_OUTPUT}.</li>
     *             </ul>
     * @throws NullPointerException
     *             if {@code controllerName} or {@code output} is {@code null}.
     *
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public PWMChannelConfig(String controllerName, int channelNumber, int idleState, int pulsePeriod,
            int pulseAlignment, GPIOPinConfig output) {
       this(UNASSIGNED, channelNumber, idleState, pulsePeriod, pulseAlignment, output);
       // null check
       controllerName.length();
       this.controllerName = controllerName;
    }

    private static void checkOutput(final GPIOPinConfig output) {
        if (output.getDirection() != GPIOPinConfig.DIR_OUTPUT_ONLY &&
            output.getDirection() != GPIOPinConfig.DIR_BOTH_INIT_OUTPUT) {
            throw new IllegalArgumentException(output.toString());
        }
    }

    private static void checkIdle(final int idleState) {
        if (idleState < UNASSIGNED || idleState > IDLE_STATE_LOW) {
            throw new IllegalArgumentException(Integer.toString(idleState));
        }
    }

    private static void checkAligment(final int pulseAlignment) {
        if ( pulseAlignment < UNASSIGNED || pulseAlignment > ALIGN_RIGHT) {
            throw new IllegalArgumentException(Integer.toString(pulseAlignment));
        }
    }

    private void checkParameters(){
        Utils.checkIntValue(controllerNumber);
        Utils.checkIntValue(channelNumber);
        checkIdle(idleState);
        Utils.checkGreaterThanZero(pulsePeriod);
        checkAligment(pulseAlignment);
    }

    /**
     * Creates a new {@code PWMChannelConfig} whose state is deserialized from the specified {@code InputStream}.
     * This method may be invoked to restore the state of a {@code PWMChannelConfig}
     * object from a persistent store.
     *
     * @param in the stream to read from.
     * @return a new {@code PWMChannelConfig} instance.
     * @throws IOException if an I/O error occurs or if the provided stream does not
     * contain a representation of a {@code PWMChannelConfig} object.
     *
     * @since 1.1
     */
    public static PWMChannelConfig deserialize(InputStream in) throws IOException {
      return (PWMChannelConfig)Platform.deserialize(in);
    }

    /**
     * Serializes the state of this {@code PWMChannelConfig} object to the specified {@code OutputStream}.
     * This method may be invoked by the {@link jdk.dio.DeviceManager DeviceManager}
     * to save the state of this {@code PWMChannelConfig} object to a persistent store.
     *
     * @param out the stream to write to.
     * @throws IOException if an I/O error occurs.
     *
     * @since 1.1
     */
    @Override
    public int serialize(OutputStream  out) throws IOException {
      return Platform.serialize(this, out);
    }

    /**
     * Gets the configured PWM channel number.
     *
     * @return the hardware channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getChannelNumber() {
        return channelNumber;
    }

    /**
     * Gets the configured controller number (the PWM controller or generator number).
     *
     * @return the controller number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    @Override
    public int getControllerNumber() {
        return controllerNumber;
    }

    /**
     * Gets the configured controller name (such as its <em>device file</em> name on UNIX systems).
     *
     * @return the controller name or {@code null}.
     */
    @Override
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Gets the requested or allocated output buffer size. The platform/underlying
     * driver may or may not allocate the requested size
     * for the output buffer.
     * When querying the configuration of an opened or registered device the
     * allocated buffer size is returned.
     *
     * @return the requested or allocated output buffer size (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     *
     * @since 1.1
     */
    public int getOutputBufferSize() {
        return outputBufferSize;
    }

    /**
     * Gets the configured output configuration on which the pulses are to be generated.
     *
     * @return the output on which the pulses are to be generated; or {@code null} if the output is implicit.
     */
    public GPIOPinConfig getOutputConfig() {
        return outputConfig;
    }

    /**
     * Gets the output on which the pulses are to be generated.
     * <p>
     * A concurrent runtime change of the
     * dynamic configuration parameters of the output (such as of its direction) may result in
     * {@code IOException} being thrown by PWM operations.
     * </p>
     *
     * @return the output on which the pulses are to be generated; or {@code null} if the output is implicit
     * or if this {@code PWMChannelConfig} instance is not associated to an actual {@code PWMChannel} instance.
     *
     * @deprecated As of 1.1, replaced by {@link PWMChannel#getOutput PWMChannel.getOutput}.
     */
    @Deprecated
    public GPIOPin getOutput() {
        return null;
    }

    /**
     * Gets the configured initial <em>scaled</em> pulse period (in microseconds).
     *
     * @return the initial pulse period in microseconds (a positive integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getPulsePeriod() {
        return pulsePeriod;
    }

    /**
     * Gets the configured idle output state.
     *
     * @return the idle output state: : {@link #IDLE_STATE_HIGH}, {@link #IDLE_STATE_LOW} or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getIdleState() {
        return idleState;
    }

    /**
     * Gets the configured pulse alignment. The alignment of the pulse within the pulse period.
     *
     * @return the pulse alignment: {@link #ALIGN_CENTER}, {@link #ALIGN_LEFT}, {@link #ALIGN_RIGHT} or {@link #UNASSIGNED UNASSIGNED}
     */
    public int getPulseAlignment() {
        return pulseAlignment;
    }

    /**
     * Gets the configured initial pulse period scale factor.
     *
     * @return the initial pulse period scale factor (a number greater or equal to {@code 1.0}).
     *
     * @since 1.1
     */
    public double getScaleFactor() {
        return Double.longBitsToDouble(scaleFactor);
    }

    /**
     * Returns the hash code value for this object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Platform.hash(this, 5, 79);
    }

    /**
     * Checks two {@code PWMChannelConfig} objects for equality.
     *
     * @param obj
     *            the object to test for equality with this object.
     *
     * @return {@code true} if {@code obj} is a {@code PWMChannelConfig} and has
     * the same hardware addressing information and configuration parameter values
     * as this {@code PWMChannelConfig} object; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return Platform.equals(this, obj);
    }
}
