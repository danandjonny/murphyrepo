/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.adc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.oracle.dio.impl.Platform;
import com.oracle.dio.utils.Utils;

import jdk.dio.DeviceConfig;
import jdk.dio.DeviceManager;
import jdk.dio.InvalidDeviceConfigException;

import romizer.DontRenameMethod;

import serializator.*;

/**
 * The {@code ADCChannelConfig} class encapsulates the hardware addressing
 * information, and static and dynamic configuration parameters of an ADC
 * channel.
 * <p>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} or {@code null} (see
 * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned,
 * Default or Unused Parameter Values</a>).
 * </p><p>
 * An instance of {@code ADCChannelConfig} can be passed to the
 * {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} to open the designated ADC channel with
 * the specified configuration. A {@link InvalidDeviceConfigException} is thrown
 * when attempting to open a device with an invalid or unsupported
 * configuration.
 * </p>
 *
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(DeviceConfig, int)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig, int)
 * @since 1.0
 */
@SerializeMe
@apimarker.API("device-io_1.1_adc")
public final class ADCChannelConfig implements DeviceConfig<ADCChannel>, DeviceConfig.HardwareAddressing {
    private String controllerName;
    private int channelNumber = UNASSIGNED;
    private int controllerNumber = UNASSIGNED;
    private int resolution = UNASSIGNED;
    private int samplingInterval = UNASSIGNED;
    private int samplingTime = UNASSIGNED;
    // keep scale as longbits view of Double to make the class be friendly for serialization and easy comparizon
    private long scaleFactor = 0x3FF0000000000000l;// 1.0
    private int inputBufferSize = UNASSIGNED;

    /**
     * The {@code Builder} class allows for creating and initializing {@code ADCChannelConfig} objects.
     * Calls can be chained in the following manner:
     * <blockquote>
     * <pre>
     *   ADCChannelConfig config = new ADCChannelConfig.Builder()
     *           .setControllerNumber(1)
     *           .setChannelNumber(1)
     *           .setResolution(256)
     *           .setScaleFactor(1.0)
     *           .setSamplingTime(1)
     *           .setSamplingInterval(10)
     *           .setInputBufferSize(0)
     *           .build();
     * </pre>
     * </blockquote>
     *
     * @since 1.1
     */
    @apimarker.API("device-io_1.1_adc")
    public static final class Builder {
        private  final ADCChannelConfig instance = new ADCChannelConfig();

        /**
         * Creates a new {@code Builder} instance.
         */
        public Builder() {

        }

        /**
         * Creates a new {@code ADCChannelConfig} instance initialized with the
         * values set for each configuration parameters. If a configuration parameter
         * was not explictly set its default value will be used.
         *
         * @return a new initialized {@code ADCChannelConfig} instance.
         */
        public ADCChannelConfig build() {
            return (ADCChannelConfig)Platform.clone(instance);
        }

        /**
         * Sets the controller name (default value is {@code null} if not set).
         *
         * @param controllerName the controller name (such as its <em>device
         * file</em> name on UNIX systems) or {@code null}.
         * @return this {@code Builder} instance.
         */
        public Builder setControllerName(String controllerName) {
            instance.controllerName = controllerName;
            return this;
        }

        /**
         * Sets the channel number (default value is {@code UNASSIGNED} if not set).
         *
         * @param channelNumber the channel number (a positive or zero integer)
         * or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code channelNumber} is not in
         * the defined range.
         */
        public Builder setChannelNumber(int channelNumber) {
            Utils.checkIntValue(channelNumber);
            instance.channelNumber = channelNumber;
            return this;
        }

        /**
         * Sets the controller number (default value is {@code UNASSIGNED} if not set).
         *
         * @param controllerNumber the hardware converter's number (a positive
         * or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code controllerNumber} is not
         * in the defined range.
         */
        public Builder setControllerNumber(int controllerNumber) {
            Utils.checkIntValue(controllerNumber);
            instance.controllerNumber = controllerNumber;
            return this;
        }

        /**
         * Sets the resolution (default value is {@code UNASSIGNED} if not set).
         *
         * @param resolution the resolution in bits (a positive integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code resolution} is not in the
         * defined range.
         */
        public Builder setResolution(int resolution) {
            Utils.checkGreaterThanZero(resolution);
            instance.resolution = resolution;
            return this;
        }

        /**
         * Sets the initial <em>scaled</em> input sampling interval (default value is {@code UNASSIGNED} if not
         * set).
         *
         * @param samplingInterval the initial scaled input sampling interval
         * (the amount of time between two samples) in microseconds (a positive
         * integer) or {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code samplingInterval} is not
         * in the defined range.
         *
         * @see #setScaleFactor
         */
        public Builder setSamplingInterval(int samplingInterval) {
            Utils.checkGreaterThanZero(samplingInterval);
            instance.samplingInterval = samplingInterval;
            return this;
        }

        /**
         * Sets the <em>scaled</em> input sampling time (default value is {@code UNASSIGNED} if not set).
         *
         * @param samplingTime the scaled sampling time (the amount of time to
         * take the sample) in microseconds (a positive integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code samplingTime} is not in
         * the defined range.
         *
         * @see #setScaleFactor
         */
        public Builder setSamplingTime(int samplingTime) {
            Utils.checkGreaterThanZero(samplingTime);
            instance.samplingTime = samplingTime;
            return this;
        }

        /**
         * Sets the initial input sampling interval and input
         * sampling time scale factor (default value is {@code 1.0} if not set).
         *
         * @param scaleFactor the sampling time and sampling interval scale
         * factor (a number greater or equal to {@code 1.0}).
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code scaleFactor} is {@link Double#NaN NaN} or
         * is less than {@code 1.0}.
         */
        public Builder setScaleFactor(double scaleFactor) {
            Utils.checkDoubleGreaterThanZero(scaleFactor);
            instance.scaleFactor = Double.doubleToLongBits(scaleFactor);
            return this;
        }

        /**
         * Sets the requested input buffer size (default value is {@code UNASSIGNED} if not set).
         *
         * @param inputBufferSize the requested input buffer size in number of
         * samples (a positive or zero integer) or
         * {@link #UNASSIGNED UNASSIGNED}.
         * @return this {@code Builder} instance.
         * @throws IllegalArgumentException if {@code inputBufferSize} is not in
         * the defined range.
         */
        public Builder setInputBufferSize(int inputBufferSize) {
            Utils.checkIntValue(inputBufferSize);
            instance.inputBufferSize = inputBufferSize;
            return this;
        }
    }

        // hidden constructor for serializer
    @DontRenameMethod
    ADCChannelConfig(){}

    /**
     * Creates a new {@code ADCChannelConfig} with the specified hardware
     * addressing information and configuration parameters.
     * <p>
     * The controller name is set to {@code null}.
     * The requested input buffer size is set to {@code UNASSIGNED}.
     * The sampling interval and sampling time scale factor is set to
     * {@code 1.0}.
     * </p>
     *
     * @param controllerNumber the hardware converter's number (a positive or
     * zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber the hardware channel's number (a positive or zero
     * integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param resolution the resolution in bits (a positive integer) or
     * {@code UNASSIGNED}.
     * @param samplingInterval the initial input sampling interval (the amount
     * of time between two samples) in microseconds (a positive integer) or
     * {@code UNASSIGNED}.
     * @param samplingTime the sampling time (the amount of time to take the
     * sample) in microseconds (a positive integer) or
     * {@code UNASSIGNED}.
     * @throws IllegalArgumentException if any of the following is true:
     * <ul>
     * <li>{@code controllerNumber} is not in the defined range;</li>
     * <li>{@code channelNumber} is not in the defined range;</li>
     * <li>{@code resolution} is not in the defined range;</li>
     * <li>{@code samplingInterval} is not in the defined range;</li>
     * <li>{@code samplingTime} is not in the defined range.</li>
     * </ul>
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public ADCChannelConfig(int controllerNumber, int channelNumber, int resolution, int samplingInterval, int samplingTime) {
        this.controllerNumber = controllerNumber;
        this.channelNumber = channelNumber;
        this.resolution = resolution;
        this.samplingInterval = samplingInterval;
        this.samplingTime = samplingTime;
        checkValues();
    }

    /**
     * Creates a new {@code ADCChannelConfig} with the specified hardware
     * addressing information and configuration parameters.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * The requested input buffer size is set to {@code UNASSIGNED}.
     * The sampling interval and sampling time scale factor is set to
     * {@code 1.0}.
     * </p>
     *
     * @param controllerName the hardware controller's name (such as its
     * <em>device file</em> name on UNIX systems).
     * @param channelNumber the hardware channel's number (a positive or zero
     * integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param resolution the resolution in bits (a positive integer) or
     * {@code UNASSIGNED}.
     * @param samplingInterval the initial input sampling interval (the amount
     * of time between two samples) in microseconds (a positive integer) or
     * {@code UNASSIGNED}.
     * @param samplingTime the sampling time (the amount of time to take the
     * sample) in microseconds (a positive integer) or
     * {@code UNASSIGNED}.
     * @throws IllegalArgumentException if any of the following is true:
     * <ul>
     * <li>{@code channelNumber} is not in the defined range;</li>
     * <li>{@code resolution} is not in the defined range;</li>
     * <li>{@code samplingInterval} is not in the defined range;</li>
     * <li>{@code samplingTime} is not in the defined range.</li>
     * </ul>
     * @throws NullPointerException if {@code controllerName} is {@code null}.
     * @deprecated As of 1.1, use {@link Builder} instead.
     */
    @Deprecated
    public ADCChannelConfig(String controllerName, int channelNumber, int resolution, int samplingInterval, int samplingTime) {
        this.controllerName = controllerName;
        this.channelNumber = channelNumber;
        this.resolution = resolution;
        this.samplingInterval = samplingInterval;
        this.samplingTime = samplingTime;
        // checks for null
        controllerName.length();
        checkValues();
    }

    /**
     * Creates a new {@code ADCChannelConfig} whose state is deserialized from
     * the specified {@code InputStream}. This method may be invoked to restore
     * the state of a {@code ADCChannelConfig} object from a persistent store.
     *
     * @param in the stream to read from.
     * @return a new {@code ADCChannelConfig} instance.
     * @throws IOException if an I/O error occurs or if the provided stream does
     * not contain a representation of a {@code ADCChannelConfig} object.
     *
     * @since 1.1
     */
    public static ADCChannelConfig deserialize(InputStream in) throws IOException {
        return (ADCChannelConfig)Platform.deserialize(in);
    }

    /**
     * Serializes the state of this {@code ADCChannelConfig} object to the
     * specified {@code OutputStream}. This method may be invoked by the
     * {@link jdk.dio.DeviceManager DeviceManager} to save the state of this
     * {@code ADCChannelConfig} object to a persistent store.
     *
     * @param out the stream to write to.
     * @throws IOException if an I/O error occurs.
     *
     * @since 1.1
     */
    @Override
    public int serialize(OutputStream out) throws IOException {
        return Platform.serialize(this, out);
    }

    /**
     * Gets the configured channel number.
     *
     * @return the hardware channel's number (a positive or zero integer) or
     * {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getChannelNumber() {
        return channelNumber;
    }

    /**
     * Gets the configured device number (such as the ADC converter number).
     *
     * @return the device number (a positive or zero integer) or
     * {@link #UNASSIGNED UNASSIGNED}.
     */
    @Override
    public int getControllerNumber() {
        return controllerNumber;
    }

    /**
     * Gets the configured controller name (such as its <em>device file</em>
     * name on UNIX systems).
     *
     * @return the controller name or {@code null}.
     */
    @Override
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Gets the requested or allocated input buffer size. The
     * platform/underlying driver may or may not allocate the requested size for
     * the input buffer. When querying the configuration of an opened or
     * registered device the allocated buffer size is returned.
     *
     * @return the requested or allocated input buffer size in number of samples
     * (a positive or zero integer).
     *
     * @since 1.1
     */
    public int getInputBufferSize() {
        return inputBufferSize;
    }

    /**
     * Gets the configured resolution.
     *
     * @return the resolution in bits (a positive integer) or
     * {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getResolution() {
        return resolution;
    }

    /**
     * Gets the configured initial <em>scaled</em> input sampling
     * interval - the amount of time between two samples (in microseconds).
     *
     * @return the initial input sampling interval in microseconds (a
     * positive integer) or {@link #UNASSIGNED UNASSIGNED}.
     *
     * @see #getScaleFactor
     */
    public int getSamplingInterval() {
        return samplingInterval;
    }

    /**
     * Gets the configured <em>scaled</em> input sampling time - the amount of
     * time to take the sample (in microseconds).
     *
     * @return the input sampling time in microseconds (a positive integer) or
     * {@link #UNASSIGNED UNASSIGNED}.
     *
     * @see #getScaleFactor
     */
    public int getSamplingTime() {
        return samplingTime;
    }

    /**
     * Gets the configured initial input sampling interval and input
     * sampling time scale factor.
     *
     * @return the initial input sampling interval scale factor (a number greater or equal to {@code 1.0}).
     *
     * @since 1.1
     */
    public double getScaleFactor() {
        return Double.longBitsToDouble(scaleFactor);
    }

    /**
     * Returns the hash code value for this object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Platform.hash(this, 7, 97);
    }

    /**
     * Checks two {@code ADCChannelConfig} objects for equality.
     *
     * @param obj the object to test for equality with this object.
     * @return {@code true} if {@code obj} is a {@code ADCChannelConfig} and has
     * the same hardware addressing information and configuration parameter
     * values as this {@code ADCChannelConfig} object; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return Platform.equals(this, obj);
    }


    private void checkValues() throws IllegalArgumentException {
        Utils.checkIntValue(controllerNumber);
        Utils.checkIntValue(channelNumber);
        Utils.checkGreaterThanZero(resolution);
        Utils.checkGreaterThanZero(samplingInterval);
        Utils.checkGreaterThanZero(samplingTime);
    }
}

