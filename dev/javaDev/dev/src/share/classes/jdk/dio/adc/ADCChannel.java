/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.adc;

import jdk.dio.BufferAccess;
import jdk.dio.Device;
import jdk.dio.DeviceManager;
import jdk.dio.ClosedDeviceException;
import jdk.dio.UnavailableDeviceException;
import jdk.dio.UnsupportedByteOrderException;
import java.io.IOException;
import java.nio.*;
import romizer.WeakDontRenameClass;

/**
 * The {@code ADCChannel} interface provides methods for controlling an ADC (Analog to Digital
 * Converter) channel.
 * <p>
 * One ADC device can have several channels. Analog input are sampled and converted according to the
 * ADC device resolution to raw digital values between {@link #getMinValue getMinValue} and
 * {@link #getMaxValue getMaxValue}. Actual input voltage values can be calculated from raw digital
 * values and the <em>Reference Voltage</em> value as returned by {@link #getVRefValue getVRefValue}.
 * </p><p>
 * An ADC channel may be identified by the numeric ID and by the name (if any defined) that
 * correspond to its registered configuration. An {@code ADCChannel} instance can be opened by a
 * call to one of the {@link DeviceManager#open(int) DeviceManager.open(id,...)} methods
 * using its ID or by a call to one of the
 * {@link DeviceManager#open(java.lang.String, java.lang.Class, java.lang.String[])
 * DeviceManager.open(name,...)} methods using its name. When a {@code ADCChannel} instance is
 * opened with an ad-hoc {@link ADCChannelConfig} configuration (which includes its hardware
 * addressing information) using one of the
 * {@link DeviceManager#open(jdk.dio.DeviceConfig)
 * DeviceManager.open(config,...)} methods it is not assigned any ID nor name.
 * </p><p>
 * Once opened, an application can read the current sampled input value of an ADC channel by calling
 * the {@link #acquire() acquire()} method or can acquire the input values sampled over a period of time by
 * calling the {@link #acquire(IntBuffer)} method.
 * </p><p>
 * An application can also asynchronously acquire the input values sampled at a certain rate by
 * calling the {@link #startAcquisition(IntBuffer, AcquisitionRoundListener) startAcquisition}
 * methods with a {@link AcquisitionRoundListener} instance which will get cyclicly and
 * asynchronously notified when the desired number of samples have been acquired. The analog input
 * acquisition can be stopped by calling the {@link #stopAcquisition() stopAcquisition} method.
 * </p><p>
 * An application can monitor the input value by calling the
 * {@link #startMonitoring(int, int, MonitoringListener) startMonitoring} method with a low and a
 * high threshold value and {@link MonitoringListener} instance which will get asynchronously
 * notified when the input value gets out of or back in the defined range. The monitoring can be
 * stopped by calling the {@link #stopMonitoring() stopMonitoring} method.
 * </p><p>
 * At most one acquisition (synchronous or asynchronous) and/or (depending on the platform) at most
 * one monitoring can be going on at any time. If an acquisition and a monitoring can be performed
 * concurrently, they will be performed at the same sampling rate (see
 * {@link #getSamplingInterval() getSamplingInterval}).
 * They therefore respectively acquire and
 * monitor the same sampled input values.
 * </p><p>
 * When an application is no longer using an ADC channel it should call the {@link #close() close}
 * method to close the ADC channel. Any further attempt to set or get the value of a ADC channel
 * which has been closed will result in a {@link ClosedDeviceException} been thrown.
 * </p><p>
 * Asynchronous notification of range conditions or input acquisition is only loosely tied
 * to hardware-level interrupt requests. The platform does not guarantee notification in a
 * deterministic/timely manner.</p>
 * <h3><a name="iomodes">Buffered I/O and Direct I/O Transfers</a></h3>
 * A ADC channel may support buffered I/O or direct I/O operations depending on
 * the capabilities of the underlying device hardware and driver. <br>
 * Buffered input - input in buffering mode - may be requested by setting the
 * input buffer size parameter of the {@link ADCChannelConfig} configuration to
 * a value greater than {@code 0} ; whether or not the channel will indeed work
 * in buffering mode and will use an internal input buffer of the size requested
 * is up to the device driver. An application may check whether a channel is
 * working in buffering mode by calling the
 * {@link ADCChannelConfig#getInputBufferSize ADCChannelConfig.getInputBufferSize} method. <br>
 * When a ADC channel is not working in buffering mode, direct I/O may be
 * enabled by providing direct {@code Buffer}s to the input methods; whether
 * efficient direct input transfers will be used depends on the underlying
 * hardware and driver capabilities and on whether the provided buffers are
 * suitable for such operations (see
 * {@link BufferAccess#prepareBuffer BufferAccess.prepareBuffer}). Input methods
 * using double buffering may only support efficient direct operations if both
 * buffers are suitable for such operations.
 *
 * @see AcquisitionRoundListener
 * @see MonitoringListener
 * @see ADCPermission
 * @since 1.0
 */
@apimarker.API("device-io_1.1_adc")
@WeakDontRenameClass
public interface ADCChannel extends Device<ADCChannel>, BufferAccess<IntBuffer> {

    /**
     * Returns the maximum raw value this channel can convert to. If the ADC device resolution is
     * {@code n} then the {@code min} value returned by {@link #getMinValue() getMinValue} and the
     * {@code max} value returned by {@link #getMaxValue() getMaxValue} are such that: <blockquote>
     *
     * <pre>
     * {@code (max - min) == (2^n - 1)}.
     * </pre>
     *
     * </blockquote>
     *
     * @return the maximum raw value this channel can convert to.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    int getMaxValue() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Gets the minimum <em>scaled</em> input sampling interval (in microseconds) that can be set using
     * {@link #setSamplingInterval setSamplingInterval}. The minimum <em>effective</em> sampling interval
     * can be calculated from the currently set <em>scale factor</em> as can be retrieved using
     * {@link #getScaleFactor getScaleFactor}.
     *
     * @return the minimum scaled input sampling interval (in microseconds).
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    int getMinSamplingInterval() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Gets the maximum <em>scaled</em> input sampling interval (in microseconds) that can be set using
     * {@link #setSamplingInterval setSamplingInterval}. The maximum <em>effective</em> sampling interval
     * can be calculated from the currently set <em>scale factor</em> as can be retrieved using
     * {@link #getScaleFactor getScaleFactor}.
     *
     * @return the maximum scaled input sampling interval (in microseconds).
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @since 1.1
     */
    int getMaxSamplingInterval() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Returns the minimum raw value this channel can convert to. If the ADC device resolution is
     * {@code n} then the {@code min} value returned by {@link #getMinValue getMinValue} and the
     * {@code max} value returned by {@link #getMaxValue getMaxValue} are such that: <blockquote>
     *
     * <pre>
     * {@code (max - min) == (2^n - 1)}.
     * </pre>
     *
     * </blockquote>
     *
     * @return the minimum raw value this channel can convert to.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    int getMinValue() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Gets the <em>scaled</em> input sampling interval (in microseconds). The
     * <em>effective</em> sampling interval can then be calculated from the
     * currently set <em>scale factor</em> as can be retrieved using {@link #getScaleFactor getScaleFactor}.
     * If the sampling interval was not
     * set previously using {@link #setSamplingInterval setSamplingInterval}, the device
     * configuration-specific default value is returned. Additionally, the value returned may differ
     * from the previously set or configured value as it may have been adjusted to account
     * for the timer resolution or discrete sampling interval values supported by the underlying platform or driver.
     *
     * @return the scaled input sampling interval (in microseconds).
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    int getSamplingInterval() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Sets the sampling interval scale factor of this ADC channel. The current
     * scaled sampling interval value is reset to the new resulting minimum <em>scaled</em> sampling interval
     * value. A call to this method should be immediately followed by a call to
     * the {@link #setSamplingInterval setSamplingInterval} method in order to set
     * an appropriate <em>scaled</em> sampling interval.
     *
     * @param factor the scale factor (a number greater or equal to {@code 1.0}).
     * @throws IllegalArgumentException if {@code factor} is {@link Double#NaN NaN}, is less than {@code 1.0} or,
     * if the setting of the scale factor would result
     * in the <em>scaled</em> sampling interval range to be outside the range {@code [1 - }{@link Integer#MAX_VALUE}{@code ]}.
     * @throws IOException if some other I/O error occurs.
     * @throws UnavailableDeviceException if this device is not currently
     * available - such as it is locked by another application.
     * @throws ClosedDeviceException if the device has been closed.
     *
     * @see #getScaleFactor
     * @see #setSamplingInterval
     */
    void setScaleFactor(double factor) throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Gets the sampling interval scale factor of this ADC channel. If the scale
     * factor is {@code scale} and the scaled sampling interval value as
     * returned by {@link #getSamplingInterval getSamplingInterval} is {@code sInterval} then the
     * effective sampling interval is calculated as follows: <blockquote>
     * <pre>
     * {@code eInterval = (sInterval / scale)}
     * </pre>
     * </blockquote>
     * Conversely, the scaled sampling interval value to set using
     * {@link #setSamplingInterval} to obtain the effective sampling
     * interval {@code eInterval} is calculated as follows: <blockquote>
     * <pre>
     * {@code sInterval = (eInterval * scale)}
     * </pre>
     * </blockquote>
     * The scale factor also applies to the minimum and maximum scaled sampling intervals
     * as respectively returned by {@link #getMinSamplingInterval getMinSamplingInterval}
     * and {@link #getMaxSamplingInterval getMaxSamplingInterval}.
     * <p>
     * If the sampling interval scale factor was not set previously using
     * {@link #setScaleFactor setScaleFactor}, the device configuration-specific
     * default value is returned.
     * </p>
     *
     * @return the scale factor.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    double getScaleFactor() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Reads the current raw sampled input value of this channel.
     * <p>
     * This method may be invoked at any time. If another thread has already initiated a synchronous
     * input acquisition upon this channel, however, then an invocation of this method will block
     * until the first operation is complete.
     * </p><p>
     * Only one acquisition (synchronous or asynchronous) can be going on at any time.
     * </p>
     *
     * @return this channel's current raw sampled input value.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @throws IllegalStateException
     *             if an asynchronous acquisition is already active.
     */
    int acquire() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Reads a sequence of raw sampled input values from this channel and copies them into the
     * provided buffer.
     * <p>
     * The input will be sampled according to the current effective input sampling interval
     * as determined by the current scaled sampling interval (see {@link #getSamplingInterval getSamplingInterval})
     * and the current scale factor {@link #getScaleFactor getScaleFactor}.
     * </p><p>
     * <i>r</i> {@code int} integers will be read from this channel, where <i>r</i> is the number of
     * integers remaining in the buffer, that is, {@code dst.remaining()}, at the moment this method
     * is invoked.
     * </p><p>
     * Suppose that an integer sequence of length <i>n</i> is read, where <i>{@code 0 <= n <= r}
     * </i>. This integer sequence will be transferred into the buffer so that the first integer in
     * the sequence is at index <i>p</i> and the last integer is at index <i>{@code p + n - 1}</i>,
     * where <i>p</i> is the buffer's position at the moment this method is invoked. Upon return the
     * buffer's position will be equal to <i>{@code p + n}</i>; its limit will not have changed.
     * <br>
     * This operation will block until the requested <i>r</i> raw input values
     * have been read or otherwise transferred from the driver/hardware. If this
     * channel uses an internal input buffer and is therefore
     * working in <a href="#iomodes">buffering mode</a> this method will block
     * until all the <i>r</i> raw input values have been copied from the internal
     * input buffer.
     * </p><p>
     * This method may be invoked at any time. If another thread has already initiated a synchronous
     * input acquisition upon this channel, however, then an invocation of this method will block
     * until the first operation is complete.
     * </p><p>
     * Only one acquisition (synchronous or asynchronous) can be going on at any time.
     * </p>
     *
     * @param dst
     *            The buffer into which integer raw sampled input values are to be transferred
     * @throws NullPointerException
     *             If {@code dst} is {@code null}.
     * @throws IllegalStateException
     *             if an asynchronous acquisition is already active.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws UnsupportedByteOrderException
     *             if the byte ordering of the provided buffer is not supported (see <a href="{@docRoot}/jdk/dio/Device.html#byte_order">Device Byte Order</a>).
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnsupportedOperationException
     *             if an asynchronous monitoring is already active and acquisition and monitoring
     *             cannot be performed concurrently.
     */
    void acquire(IntBuffer dst) throws IOException, UnavailableDeviceException, UnsupportedByteOrderException, ClosedDeviceException;

    /**
     * Returns the <em>Reference Voltage</em> value of this ADC channel. If the reference voltage is
     * {@code vRef} and the ADC device resolution is {@code n} then the actual input voltage value
     * corresponding to a raw sampled value {@code value} read on this channel can be calculated as
     * follows: <blockquote>
     * <pre>
     * {@code vInput = (value * vRef) / (2^n)}
     * </pre>
     * </blockquote>
     *
     * @return the <em>Reference Voltage</em> value of this channel.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    double getVRefValue() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Sets the <em>scaled</em> input sampling interval (in microseconds). The
     * <em>effective</em> sampling interval is calculated from the
     * currently set <em>scale factor</em> as can be retrieved using {@link #getScaleFactor getScaleFactor}.
     * Whether changing the sampling interval
     * has an immediate effect or not on an active (synchronous or asynchronous) generation is
     * device- as well as platform-dependent.
     * <p>
     * If the underlying platform or driver does not support the requested interval value
     * then {@code interval} will be aligned to the closest lower supported discrete interval value. The resulting, actual
     * scaled sampling interval can be retrieved by a call to {@link #getSamplingInterval getSamplingInterval}.
     * If the current scale factor as returned by
     * {@link #getScaleFactor getScaleFactor} is {@code scale} and the current scaled
     * sampling interval value - after alignment - is {@code sInterval}
     * then the effective sampling interval can be calculated as follows:</p>
     * <blockquote>
     * <pre>
     * {@code eInterval = (sInterval / scale)}
     * </pre>
     * </blockquote>
     *
     * @param interval
     *            the scaled input sampling interval (in microseconds).
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws InvalidInputSamplingRateException
     *             if {@code interval} is greater than the maximum sampling interval (see {@link #getMaxSamplingInterval getMaxSamplingInterval})
     *             or lower than the minimum sampling interval (see {@link #getMinSamplingInterval getMinSamplingInterval}).
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @throws IllegalArgumentException
     *             if {@code interval} is negative or zero.
     */
    void setSamplingInterval(int interval) throws IOException, UnavailableDeviceException,
            ClosedDeviceException;

    /**
     * Starts asynchronous analog input acquisition on this channel and reads a series of raw
     * sampled input values. The provided {@link AcquisitionRoundListener} instance is cyclicly
     * notified when the provided buffer has been filled with raw sampled input values. The read
     * values are copied into the provided buffer. Once the requested number of raw sampled input
     * values has been read, reading will be suspended and in the event of continuous sampling,
     * subsequent sampled input values may be lost. Reading into the buffer and notification will
     * only resume once the event has been handled. Reading and notification will immediately start
     * and will repeat until it is stopped by a call to {@link #stopAcquisition() stopAcquisition}.
     * <p>
     * <i>r</i> {@code int} integers will be read from this channel, where <i>r</i> is the number of
     * integers remaining in the buffer (possibly {@code 0}), that is, {@code dst.remaining()}, at
     * the moment this method is initially invoked and then subsequently when the listener is
     * returning.
     * </p><p>
     * Suppose that an integer sequence of length <i>n</i> is read, where <i>{@code 0 <= n <= r}
     * </i>. This integer sequence will be transferred into the buffer so that the first integer in
     * the sequence is at index <i>p</i> and the last integer is at index <i>{@code p + n - 1}</i>,
     * where <i>p</i> is the buffer's position at the moment this method is invoked and then
     * subsequently when the listener is returning. Upon invocation of the listener to fetch more
     * values to convert the buffer's position will be equal to <i>{@code p + n}</i>; its limit will
     * not have changed. <br>
     * If this channel
     * uses an internal input buffer and is therefore working in <a href="#iomodes">buffering mode</a> the listener will only be
     * invoked after all the <i>r</i> raw input values have been copied from the
     * internal input buffer; otherwise the listener will only be invoked after all the
     * <i>r</i> raw input values have been transferred from the driver/hardware.<br>
     * The buffer's position upon stopping this asynchronous operation by a call to
     * {@link #stopAcquisition stopAcquisition} is not predictable unless called from within the
     * listener.
     * </p><p>
     * Upon notification of the provided {@code AcquisitionRoundListener}
     * the reference to the provided {@code dst} buffer can be retrieved from the
     * {@code RoundCompletionEvent} using the {@link jdk.dio.RoundCompletionEvent#getBuffer() getBuffer} method.
     * <br>
     * A buffer with {@code 0} integers remaining to be read (that is a buffer already
     * full) at the moment this method is initially invoked or then subsequently when the listener
     * is returning will not stop the asynchronous operation; the listener is guaranteed to be
     * called back again at the latest as soon as all other events pending at the time of
     * notification have been dispatched. The overrun condition resulting from the listener notification
     * returning with an already-full buffer will be reported on the subsequent notifications through
     * the {@link jdk.dio.RoundCompletionEvent#isOnError() RoundCompletionEvent.isOnError} method.
     * </p><p>
     * The input will be sampled according to the current effective input sampling interval
     * as determined by the current scaled sampling interval (see {@link #getSamplingInterval getSamplingInterval})
     * and the current scale factor {@link #getScaleFactor getScaleFactor}.
     * </p><p>
     * Only one acquisition (synchronous or asynchronous) can be going on at any time.
     * </p><p>
     * Buffers are not safe for use by multiple concurrent threads so care should
     * be taken to not access the provided buffer until the operation (or a round thereof) has completed.
     * Interfering with the asynchronous operation by accessing and modifying the provided buffer
     * concurrently may yield unpredictable results.
     * </p>
     *
     * @param dst
     *            The buffer into which integer raw sampled input values are to be transferred.
     * @param listener
     *            the {@link AcquisitionRoundListener} instance to be notified when all the sampled
     *            input values have been read.
     * @throws NullPointerException
     *             If {@code dst} or {@code listener} is {@code null}.
     * @throws IllegalArgumentException
     *             if the provided buffer {@code dst} has a zero-capacity.
     * @throws IllegalStateException
     *             if another synchronous or asynchronous acquisition is already active.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws UnsupportedByteOrderException
     *             if the byte ordering of the provided buffer is not supported (see <a href="{@docRoot}/jdk/dio/Device.html#byte_order">Device Byte Order</a>).
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnsupportedOperationException
     *             if an asynchronous monitoring is already active and acquisition and monitoring
     *             cannot be performed concurrently.
     */
    void startAcquisition(IntBuffer dst, AcquisitionRoundListener listener) throws IOException,
            UnavailableDeviceException, UnsupportedByteOrderException, ClosedDeviceException;

    /**
     * Starts asynchronous analog input acquisition on this channel and reads a series of raw
     * sampled input values.
     * <p>
     * This method behaves identically to
     * {@link #startAcquisition(IntBuffer, AcquisitionRoundListener)} excepts that it uses
     * double-buffering - the provided buffers must not have a zero-capacity and must not overlap
     * - that is the backing array sections or memory regions they refer to must not overlap.
     * Notification will happen when the current working buffer (initially
     * {@code dst1}) has been filled with raw sampled input values and reading will asynchronously
     * proceed with the alternate buffer (which becomes the current working buffer). Reading will
     * only be suspended if the previous event has not yet been handled (this may result in the case
     * of continuous sampling in subsequent sampled input values to be lost). Also,
     * the position of the current working buffer upon stopping this asynchronous operation by a call to
     * {@link #stopAcquisition stopAcquisition} is not predictable even if called from within the
     * listener.
     * </p><p>
     * Upon notification of the provided {@code AcquisitionRoundListener}
     * the reference to the  current working buffer (initially {@code dst1}) can be retrieved from the
     * {@code RoundCompletionEvent} using the {@link jdk.dio.RoundCompletionEvent#getBuffer() getBuffer} method.
     * <br>
     * A working buffer with {@code 0} integers remaining to be read (that is a buffer
     * already full) at the moment this method is initially invoked or then subsequently when the
     * listener is returning will not stop the asynchronous operation; the listener is guaranteed to
     * be called back again at the latest as soon as all other events pending at the time of
     * notification have been dispatched. The overrun condition resulting from the listener notification
     * returning with an already-full buffer will be reported on the subsequent notifications through
     * the {@link jdk.dio.RoundCompletionEvent#isOnError() RoundCompletionEvent.isOnError} method.
     * </p><p>
     * Only one acquisition (synchronous or asynchronous) can be going on at any time.
     * </p><p>
     * Buffers are not safe for use by multiple concurrent threads so care should
     * be taken to not access the provided buffers until the operation (or a round thereof) has completed.
     * Interfering with the asynchronous operation by accessing and modifying the provided buffers
     * concurrently may yield unpredictable results.
     * </p>
     *
     * @param dst1
     *            The first buffer into which integer raw sampled input values are to be
     *            transferred.
     * @param dst2
     *            The second buffer into which integer raw sampled input values are to be
     *            transferred.
     * @param listener
     *            the {@link AcquisitionRoundListener} instance to be notified when all the sampled
     *            input values have been read.
     * @throws NullPointerException
     *             If {@code dst1}, {@code dst2} or {@code listener} is {@code null}.
     * @throws IllegalStateException
     *             if another synchronous or asynchronous acquisition is already active.
     * @throws IllegalArgumentException
     *             if any of the buffers {@code dst1} and {@code dst2} has a zero-capacity or
     *             if they are the same or overlap.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws UnsupportedByteOrderException
     *             if the byte ordering of the any of the provided buffers is not supported (see <a href="{@docRoot}/jdk/dio/Device.html#byte_order">Device Byte Order</a>).
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnsupportedOperationException
     *             if an asynchronous monitoring is already active and acquisition and monitoring
     *             cannot be performed concurrently.
     */
    void startAcquisition(IntBuffer dst1, IntBuffer dst2, AcquisitionRoundListener listener) throws IOException,
            UnavailableDeviceException, UnsupportedByteOrderException, ClosedDeviceException;

    /**
     * Starts monitoring this channel analog input and asynchronously notifies the provided
     * {@link MonitoringListener} instance when this channel's raw sampled input value gets out of
     * or back in the specified range (as defined by a low and a high threshold value). Monitoring
     * and notification will immediately start and will repeat until it is stopped by a call to
     * {@link #stopMonitoring stopMonitoring}. Range notification operates in toggle mode: once
     * notified of an out-of-range condition the application will next only get notified of a
     * back-in-range condition and so on...
     * <p>
     * The input will be sampled according to the current effective input sampling interval
     * as determined by the current scaled sampling interval (see {@link #getSamplingInterval getSamplingInterval})
     * and the current scale factor {@link #getScaleFactor getScaleFactor}.
     * </p><p>
     * To only get notified when the input value gets over some threshold one may call this method
     * with the {@code low} parameter set to the value of {@link #getMinValue getMinValue}.
     * Conversely, to only get notified when the input value gets under some threshold one may call
     * this method with the {@code high} parameter set to the value of {@link #getMaxValue
     * getMaxValue}.
     * </p><p>
     * If {@code low} is lower than the minimum value returned by {@link #getMinValue getMinValue}
     * then the minimum value is assumed. If {@code high} is higher the maximum value returned by
     * {@link #getMaxValue getMaxValue}, then the maximum value is assumed.
     * </p><p>
     * Only one monitoring can be going on at any time.
     * </p>
     *
     * @param listener
     *            the {@link MonitoringListener} instance to be notified when a range condition
     *            occurs.
     * @param low
     *            the low raw threshold value.
     * @param high
     *            the high raw threshold value.
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws IllegalArgumentException
     *             if {@code low} is greater than {@code high}.
     * @throws NullPointerException
     *             if {@code listener} is {@code null}.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     * @throws IllegalStateException
     *             if monitoring is already active.
     * @throws UnsupportedOperationException
     *             if an asynchronous acquisition is already active and monitoring and acquisition
     *             cannot be performed concurrently.
     */
    void startMonitoring(int low, int high, MonitoringListener listener) throws IOException,
            UnavailableDeviceException, ClosedDeviceException;

    /**
     * Stops the asynchronous analog input acquisition on this channel as started by a call to one
     * of the {@link #startAcquisition startAcquisition} methods.
     * <p>
     * This method return silently if no asynchronous analog input acquisition is currently active.
     * </p>
     *
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    void stopAcquisition() throws IOException, UnavailableDeviceException, ClosedDeviceException;

    /**
     * Stops the range monitoring of this channel analog input as started by a call to the
     * {@link #startMonitoring startMonitoring} method.
     * <p>
     * This method return silently if no range monitoring is currently active.
     * </p>
     *
     * @throws IOException
     *             if some other I/O error occurs.
     * @throws UnavailableDeviceException
     *             if this device is not currently available - such as it is locked by another
     *             application.
     * @throws ClosedDeviceException
     *             if the device has been closed.
     */
    void stopMonitoring() throws IOException, UnavailableDeviceException, ClosedDeviceException;
}
