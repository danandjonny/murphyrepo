/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package jdk.dio.atcmd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.oracle.dio.impl.Platform;

import jdk.dio.DeviceConfig;
import jdk.dio.DeviceManager;
import jdk.dio.InvalidDeviceConfigException;

import romizer.DontRenameMethod;

import serializator.*;

/**
 * The {@code ATDeviceConfig} class encapsulates the hardware addressing information, and static and
 * dynamic configuration parameters of an AT device.
 * <p>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} or {@code null} (see
 * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
 * </p><p>
 * An instance of {@code ATDeviceConfig} can be passed to the
 * {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} to open the designated AT device
 * with the specified configuration. A {@link InvalidDeviceConfigException} is thrown when
 * attempting to open a device with an invalid or unsupported configuration.
 * </p>
 *
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(DeviceConfig, int)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig, int)
 * @since 1.0
 */
@SerializeMe
@apimarker.API("device-io_1.1_atcmd")
public final class ATDeviceConfig implements DeviceConfig<ATDevice>, DeviceConfig.HardwareAddressing {

    private String controllerName;

    private int controllerNumber = UNASSIGNED;

    private int channelNumber = UNASSIGNED;

    // hidden constructor for serializer
    @DontRenameMethod
    ATDeviceConfig(){}

    /**
     * Creates a new {@code ATDeviceConfig} with the specified hardware addressing information.
     * <p>
     * The controller number is set to {@code UNASSIGNED}.
     * </p>
     *
     * @param controllerName
     *            the controller name (such as its <em>device file</em> name on UNIX systems).
     * @param channelNumber
     *            the hardware channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @throws IllegalArgumentException
     *             if {@code channelNumber} is not in the defined range.
     * @throws NullPointerException
     *             if {@code controller name} is {@code null}.
     */
    public ATDeviceConfig(String controllerName, int channelNumber) {
        this(UNASSIGNED, channelNumber);
        // checks for NPE
        controllerName.length();
        this.controllerName = controllerName;
    }

    /**
     * Creates a new {@code ATDeviceConfig} with the specified hardware addressing information.
     * <p>
     * The controller name is set to {@code null}.
     * </p>
     *
     * @param controllerNumber
     *            the controller number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @param channelNumber
     *            the hardware channel's number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     * @throws IllegalArgumentException
     *             if {@code channelNumber} is not in the defined range.
     */
    public ATDeviceConfig(int controllerNumber, int channelNumber) {
        if (UNASSIGNED > channelNumber || UNASSIGNED > controllerNumber) {
            throw new  IllegalArgumentException();
        }
        this.controllerNumber = controllerNumber;
        this.channelNumber = channelNumber;
    }

    /**
     * Creates a new {@code ATDeviceConfig} whose state is deserialized from the specified {@code InputStream}.
     * This method may be invoked to restore the state of a {@code ATDeviceConfig}
     * object from a persistent store.
     *
     * @param in the stream to read from.
     * @return a new {@code ATDeviceConfig} instance.
     * @throws IOException if an I/O error occurs or if the provided stream does not
     * contain a representation of a {@code ATDeviceConfig} object.
     *
     * @since 1.1
     */
    public static ATDeviceConfig deserialize(InputStream in) throws IOException {
        return (ATDeviceConfig) Platform.deserialize(in);
    }


    /**
     * Serializes the state of this {@code ATDeviceConfig} object to the specified {@code OutputStream}.
     * This method may be invoked by the {@link jdk.dio.DeviceManager DeviceManager}
     * to save the state of this {@code ATDeviceConfig} object to a persistent store.
     *
     * @param out the stream to write to.
     * @throws IOException if an I/O error occurs.
     *
     * @since 1.1
     */
    @Override
    public int serialize(OutputStream out) throws IOException {
        return Platform.serialize(this, out);
    }

    /**
     * Gets the configured channel number.
     *
     * @return the channel number (a positive or zero integer); or {@link #UNASSIGNED UNASSIGNED}.
     */
    public int getChannelNumber() {
        return channelNumber;
    }

    /**
     * Gets the configured controller number.
     *
     * @return the controller number (a positive or zero integer) or {@link #UNASSIGNED UNASSIGNED}.
     */
    @Override
    public int getControllerNumber() {
        return controllerNumber;
    }

    /**
     * Gets the configured controller name (such as its <em>device file</em> name on UNIX systems).
     *
     * @return the controller name or {@code null}.
     */
    @Override
    public String getControllerName() {
        return controllerName;
    }

    /**
     * Returns the hash code value for this object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return Platform.hash(this, 3, 79);
    }

    /**
     * Checks two {@code ATDeviceConfig} objects for equality.
     *
     * @param obj
     *            the object to test for equality with this object.
     * @return {@code true} if {@code obj} is a {@code ATDeviceConfig} and has the same hardware
     *         addressing information and configuration parameter values as this
     *         {@code ATDeviceConfig} object; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return Platform.equals(this, obj);
    }
}
