/*
 * Copyright (c) 2013, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package jdk.dio;
import java.io.IOException;
import java.io.OutputStream;

/**
 * The {@code DeviceConfig} class is the base interface for all device
 * configuration classes.
 * <p>
 * A device's configuration consists of the following elements:
 * </p>
 * <blockquote>
 * <dl>
 * <dt><b>Hardware Addressing Information</b></dt>
 * <dd>Information required to address the device. Examples are an I2C bus
 * number and an I2C slave device address or a GPIO controller number and pin
 * index.</dd>
 * <dt><b>Static Configuration Parameters</b></dt>
 * <dd>Configuration parameters that must be set upon opening the device and
 * which may not be changed afterwards. Examples are an SPI slave device clock
 * mode or word length.</dd>
 * <dt><b>Dynamic Configuration Parameters</b></dt>
 * <dd>Configuration parameters for which an initial value is set upon opening the
 * device and that an application may change while the device is still open.
 * Dynamic configuration parameters can be changed after the device is open
 * through methods of {@link Device} sub-interfaces. Examples are a UART baud
 * rate or the current direction of a bidirectional GPIO pin.
 * The initial values
 * of dynamic configuration parameters are initial <em>default</em> values from
 * the point of view of the application accessing the device. Runtime changes
 * to the values of dynamic configuration parameters are not reflected in the
 * {@code DeviceConfig} object of that device, which retains its initial values.
 * </dd>
 * </dl>
 * </blockquote>
 * The {@link DeviceManager#open(DeviceConfig) open(DeviceConfig, ...)} and
 * {@link DeviceManager#open(Class, DeviceConfig) open(Class, DeviceConfig, ...)}
 * methods of the {@link DeviceManager} can be used to open a device and obtain
 * a {@code Device} instance to access it. The device to open is designated by
 * the hardware addressing information encapsulated in the provided {@code DeviceConfig} object;
 * it is then configured according to the static and runtime configuration parameters
 * encapsulated in the same provided {@code DeviceConfig} object. If the device
 * designated by the provided hardware addressing information cannot be found a
 * {@link DeviceNotFoundException} is thrown. If a static or runtime configuration
 * parameter or a combination thereof is not valid or not supported a
 * {@link InvalidDeviceConfigException}.
 * <h3><a name="immutability">Immutability of {@code DeviceConfig} Objects</a></h3>
 * {@code DeviceConfig} objects should be immutable. If a
 * {@code DeviceConfig}-implementing class is not immutable it should implement
 * the {@link java.lang.Cloneable} interface. A compliant implementation of this
 * specification MUST do its best to ensure that information encapsulated in a
 * {@code DeviceConfig} instance cannot be altered while it is handling it and
 * SHOULD, when necessary (that is when an instance is not considered
 * immutable), create its own private copy either of the instance (such as by
 * cloning it) or of the information it contains. If a
 * {@code DeviceConfig}-implementing class does not implement the
 * {@code Cloneable} interface then it SHALL - for that purpose - be considered
 * immutable.<br>
 * As per the above requirements, a compliant implementation of this specification
 * may return the same (immutable) {@code DeviceConfig} instance over several calls;
 * an application must therefore not synchronize on {@code DeviceConfig} objects.
 * <h3><a name="persistency">Persistency of {@code DeviceConfig} Objects</a></h3>
 * A compliant implementation of this specification MUST support the persistence
 * of the device configuration registry across platform reboots; it MUST guarantee
 * in particular that a device configuration registered by an application remains
 * registered after the application has terminated and across platform reboots
 * until it is explictly unregistered by an application (possibly the same)
 * that has been granted the necessary permissions.<br>
 * To allow for persistency of the device configuration registry, {@code DeviceConfig}
 * objects must support saving their states to an {@code OutputStream}
 * and restoring their states from an {@code InputStream}. The
 * {@code DeviceManager} saves the state of an {@code DeviceConfig} object by invoking
 * its {@link #serialize(java.io.OutputStream) serialize} method. The
 * {@code DeviceManager} restores the state of an {@code DeviceConfig} object by invoking
 * the {@link jdk.dio.spi.DeviceProvider#deserialize(java.io.InputStream) deserialize}
 * on its associated {@link jdk.dio.spi.DeviceProvider DeviceProvider}.<br>
 * On the Java Platform Standard Edition, these methods may delegate to the Object Serialization
 * framework.
 * In order to facilitate the implementation of compound or layered device abstractions,
 * on the Java Platform Micro Edition, which does not support Object Serialization,
 * {@code DeviceConfig}-implementing classes should provide methods for creating new instances
 * initialized from an {@code InputStream}; see example below:
 * <blockquote>
 *   <pre>
 * public class RealTimeClockConfig implements DeviceConfig&lt;RealTimeClock&gt;, HardwareAddressing {
 *
 *   private MMIODeviceConfig mmioConfig;
 *
 *   public RealTimeClockConfig(MMIODeviceConfig mmioConfig) {
 *     this.mmioConfig = mmioConfig;
 *   }
 *
 *   <em>public static RealTimeClockConfig deserialize(InputStream in) throws IOException {
 *     return new RealTimeClockConfig(MMIODeviceConfig.deserialize(in));
 *   }</em>
 *
 *   ...
 *
 *   public MMIODeviceConfig getMmioConfig() {
 *     return mmioConfig;
 *   }
 *
 *   &#64;Override
 *   public int serialize(OutputStream out) throws IOException {
 *     return mmioConfig.serialize(out);
 *   }
 * }
 *   </pre>
 * </blockquote>
 * <h3><a name="default_unassigned">Unassigned, Default or Unused Parameter Values</a></h3>
 * Some hardware addressing, static or dynamic configuration parameters may be
 * set to {@link #UNASSIGNED UNASSIGNED} (or {@code null}) when opening or registering a
 * device configuration. Device drivers may substitute hardware addressing parameters
 * and static and dynamic configuration parameters
 * set to {@link #UNASSIGNED UNASSIGNED} (or {@code null}) with actual default values;
 * whether such default settings are supported is platform- as well as device driver-dependent;
 * if a required parameter is set to {@link #UNASSIGNED UNASSIGNED} (or {@code null}) the device driver
 * MUST reject the configuration and throw an {@link InvalidDeviceConfigException}.
 * When querying the configuration of an open device using the
 * {@link DeviceDescriptor#getConfiguration DeviceDescriptor.getConfiguration}
 * method the actual settings are returned; parameters that are neither supported
 * nor required by the underlying hardware or driver are still set to {@link #UNASSIGNED UNASSIGNED}
 * (or {@code null}); whether or not this is the case when listing registered
 * device configuration using the {@link DeviceManager#list DeviceManager.list}
 * methods depends on whether the device could be probed upon registration
 * of the configuration (see <a href="{@docRoot}/jdk/dio/DeviceManager.html#probing">Device Probing</a>).
 *
 * @param <P> the device type the configuration is defined for.
 * @see DeviceManager#open(DeviceConfig)
 * @see DeviceManager#open(DeviceConfig, int)
 * @see DeviceManager#open(Class, DeviceConfig)
 * @see DeviceManager#open(Class, DeviceConfig, int)
 * @see DeviceManager#list
 * @see DeviceDescriptor#getConfiguration
 * @see InvalidDeviceConfigException
 * @since 1.0
 */
@apimarker.API("device-io_1.1")
public interface DeviceConfig<P extends Device<? super P>> {

  /**
   * Used to indicate that the default value of a hardware addressing or
   * configuration parameter is requested upon opening or registering a device
   * configuration or to indicate that a configuration parameter is unassigned
   * (such as when it is not supported).
   * @deprecated As of 1.1 replaced by {@link #UNASSIGNED UNASSIGNED}.
   */
    @Deprecated
  int DEFAULT = -1;
  /**
   * Used to indicate that the default value of a hardware addressing or
   * configuration parameter is requested upon opening or registering a device
   * configuration or to indicate that a configuration parameter is unused
   * (such as when it is neither supported nor required by the underlying hardware or driver).
   */
  int UNASSIGNED = -1;

  /**
   * The {@code HardwareAddressing} interface defines an abstraction of an
   * hardware addressing information common on different platforms.
   * <p>
   * When a device's {@code HardwareAddressing} contains both a specific controller number
   * (see {@link #getControllerNumber getControllerNumber}) and a specific controller name (see {@link #getControllerName getControllerName})
   * a compliant implementation of this specification MUST only use the controller number
   * for addressing the device and for permission checks.
   * </p>
   *
   * @since 1.0
   */
  @apimarker.API("device-io_1.1")
  public interface HardwareAddressing {

    /**
     * Gets the controller number.
     *
     * @return the controller number (a positive or zero integer) or
     * {@link #UNASSIGNED UNASSIGNED} if no specific value is requested or no actual value is
     * assigned (see
     * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
     */
    int getControllerNumber();

    /**
     * Gets the controller name (such as a <em>device file</em> name on UNIX
     * systems).
     *
     * @return the controller name or {@code null} if no specific value is
     * requested or no actual value is assigned (see
     * <a href="{@docRoot}/jdk/dio/DeviceConfig.html#default_unassigned">Unassigned, Default or Unused Parameter Values</a>).
     */
    String getControllerName();
  }

  /**
   * Serializes the state of this {@code DeviceConfig} object to the specified
   * {@code OutputStream}. This method may be invoked by the
   * {@link jdk.dio.DeviceManager DeviceManager} to save the state of a
   * {@code DeviceConfig} object to a persistent store in order to support
   * persistence of device configuration registration.
   *
   * @param out the stream to write to.
   * @return the number of bytes written to the stream.
   * @throws IOException if an I/O error occurs.
   *
   * @see jdk.dio.spi.DeviceProvider#deserialize(java.io.InputStream)
   * @since 1.1
   */
  int serialize(OutputStream out) throws IOException;
}
