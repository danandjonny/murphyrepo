/*
 * Copyright (c) 2012, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.oracle.dio.impl;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

import com.oracle.dio.utils.Constants;
import com.oracle.dio.utils.Logging;
import com.oracle.dio.impl.Platform;

import jdk.dio.Device;
import jdk.dio.DeviceConfig;
import jdk.dio.DeviceDescriptor;
import jdk.dio.spi.DeviceProvider;

import romizer.*;

import serializator.*;

/**
 * Default implementation of {@link DeviceDescriptor}
 *
 */
@SerializeMe
@DontRenameClass
public final class PeripheralDescriptorImpl<T extends Device<? super T>> implements DeviceDescriptor<T> {

    private Object config;
    private String clazz;
    private int id = DeviceDescriptor.UNDEFINED_ID;
    private String name;
    private String[] props;
    private String providerClazz;

    // need for serializator
    public PeripheralDescriptorImpl() {
    }

    public PeripheralDescriptorImpl(int id, String name, DeviceConfig config, Class<T> intf, String[] props) {
        Objects.requireNonNull(config);
        this.config = config;
        // see DeviceManager.openWithConfig where intf may be null
        // however register/open-with-name/open-with-id always provides correct intf
        if (null != intf) {
            this.clazz = intf.getName();
        }
        this.id = id;
        this.name = name;
        this.props = (props == null) ? props : props.clone();

    }

    @Override
    public <C extends DeviceConfig<? super T>> C getConfiguration() {
        if (config instanceof DeviceConfig) {
            return (C)config;
        }
        return null;
    }

    @Override
    public Class<T> getInterface() {
        if (null != clazz) {
            try {
                return (Class<T>)Class.forName(clazz);
            } catch (ClassNotFoundException | RuntimeException e) {
                Logging.reportError("Can't restore class at PeripheralDescriptorImpl");
            }
        }
        return null;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String[] getProperties() {
        return (props == null) ? props : props.clone();
    }

    public void setDeviceProvider(DeviceProvider provider) {
        providerClazz = provider.getClass().getName();
    }

    public void prepareForSerilization() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (! (config instanceof byte[]) ) {
            if (null != providerClazz) {
                ((DeviceConfig)config).serialize(baos);
            } else {
                Platform.serialize(config, baos);
            }
            baos.close();
            config = baos.toByteArray();
        }
    }

    public void recoverFromSerialization() {
        if (config instanceof byte[]) {
            ByteArrayInputStream bais = new ByteArrayInputStream((byte[])config);
            try {
                config = Platform.deserialize(bais);
            } catch (IOException e1) {
                Logging.reportInformation("Config is custom DeviceConfig child");
                try {
                    bais.reset();
                    DeviceProvider<T> provider = (DeviceProvider)Class.forName(providerClazz).newInstance();
                    config = provider.deserialize(bais);
                } catch (Exception e2) {
                    Logging.reportError("Can't restore config object");
                }
            }
        }
    }

    public void setNewID(int new_id) {
        id = new_id;
    }

}

