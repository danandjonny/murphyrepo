//testing if statements

import java.util.Scanner;

public class snow{
    public static void main(String[]args){
        Scanner info = new Scanner(System.in);

        //variables
        int rnum = (int)(Math.random() * 10 + 1);
        int guess;

        System.out.println("I'm thinking of a number between 1 and 10, can you guess what it is? ");
        System.out.println("Enter your guess below.");
        while (true) {
            guess = info.nextInt();

            if (guess > rnum) {
                System.out.println("Too high guess again.");
            } else if (guess < rnum) {
                System.out.println("Too low guess again.");
            } else if (guess == rnum) {
                System.out.println("CORRECT!!! good job :)");
                break;
            }


        }
    }
}