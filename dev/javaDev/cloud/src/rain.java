//tutorial for switch statements

public class rain{
    public static void main(String[]args){
        int age;
        age = 4;

        switch (age){
            case 1:
                System.out.println("You can crawl");
                break;
            case 2:
                System.out.println("You can walk");
                break;
            case 3:
                System.out.println("You can get in trouble");
                break;
            default:
                System.out.println("I don't know how old you are");
                break;

        }
    }
}