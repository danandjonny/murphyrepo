/*
Programmer: Jonathan Murphy
Date: 3/20/17
Filename: looping.java
Purpose: Explore looping and while loops
*/

public class looping{
	public static void main(String[]args){
	
	int counter = 0;
	
	while (counter < 10){
		System.out.println(counter);
		counter++;
		}
	}
}