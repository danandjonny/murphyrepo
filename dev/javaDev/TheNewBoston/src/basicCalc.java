/*
Programmer: Jonathan Murphy
Date: 3/20/17
Filename: basicCalc.java
Purpose: Create a basic calculator
*/

import java.util.Scanner;
import java.util.scanner;

public class basicCalc{
	public static void main(String[]args){
		Scanner jonny = new Scanner(System.in);
		double num1, num2, answer;
		System.out.println("Enter first number: ");
		num1 = jonny.nextDouble();
		System.out.println("Enter second number: ");
		num2 = jonny.nextDouble();
		answer = num1 + num2;
		System.out.println(answer);
}
}