/*
Programmer: Jonathan Murphy
Date: 10/27/17
Filename: test1.java
Purpose: make a smart computer guess
*/

import java.util.Scanner;

public class test1{
	public static void main(String[]args){
		Scanner vReader = new Scanner(System.in);
		
		int iPicked = 0, iGuess = 50, iTries = 1;
		
		System.out.print("Pick a number between 1-100: ");
		iPicked = vReader.nextInt();
		
		int v1 = 1;
		int v2 = 100;
		
		System.out.println(v1 + " - " + v2);
		
		
		while(iPicked != v1 && iPicked != v2 && iPicked != iGuess){
			if(iGuess < iPicked){
				v1 = iGuess;
				iGuess = ((v2 - v1) / 2) + v1;
				System.out.println("iguess < ipicked " + iGuess + "::" + v1 +"-"+ v2);
				iTries++;
				}
			if(iGuess > iPicked){
				v2 = iGuess;
				iGuess = ((v2 - v1) / 2) + v1;
				System.out.println("iguess > ipicked " + iGuess + "::" + v1 +"-"+ v2);
				iTries++;
				}
			
			}
		System.out.println("Your number was " + iGuess + " it took me " + iTries + " tries");
		
		

		
		}
	}
