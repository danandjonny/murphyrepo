/*
By: Jonathan Murphy
12/27/17
Simple number guessing game
*/

import java.util.Scanner;
import java.util.Random;

public class GuessingGame{
	public static void main(String[]args){
		Guess g1 = new Guess();
		Random Generator = new Random();
		
		g1.Parameters();
		g1.Target();
		g1.Find();
		System.out.println(g1);
	}
}
