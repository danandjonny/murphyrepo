/*
By: Jonathan Murphy
12/27/17
reusable class for a binary search
*/

import java.util.Scanner;

public class Guess{
	Scanner vReader = new Scanner(System.in);
	int iP1, iP2, iTarget3, x;

	public void Parameters(){
		System.out.println("Please enter your first parameter");
		iP1 = vReader.nextInt();
		System.out.println("Please enter your second parameter");
		iP2 = vReader.nextInt();		
	}
	
	public void Target(){
		System.out.println("Enter target number");
		iTarget3 = vReader.nextInt();		
	}
	
	public int Find(){
		x = iP2/2;
		while (iTarget3 != iP1 && iTarget3 != iP2 && iTarget3 != x){
			if (x < iTarget3){
				iP1 = x;
				x = ((iP2-iP1) / 2) + iP1;
			}
			if (x > iTarget3){
				iP2 = x;
				x = ((iP2-iP1) / 2) + iP1;
			}
		}
		return x;
	}
	
	public String toString(){
		String str = ("computer guess " + x);
		return str;
	}
}